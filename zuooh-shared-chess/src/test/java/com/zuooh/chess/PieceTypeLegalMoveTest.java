package com.zuooh.chess;

import java.util.List;

import junit.framework.TestCase;

import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class PieceTypeLegalMoveTest extends TestCase {

   private static final String[][] BOARD = {
    // A     B     C     D     E     F     G    H
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 8
   { "wR", "  ", "wQ", "  ", "bP", "  ", "  ", "  " },  // 7
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 6
   { "bR", "  ", "  ", "wP", "wP", "wP", "  ", "  " },  // 5
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 4
   { "  ", "  ", "bQ", "  ", "  ", "  ", "  ", "  " },  // 3
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 2
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }}; // 1

   public void testSimpleMoves() {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);

      ChessPiece whiteQueen = chessBoard.getPiece(ChessBoardPosition.C7);

      List<ChessBoardPosition> moves = ChessPieceType.QUEEN.moveNormal(chessBoard, whiteQueen);

      assertEquals(moves.size(), 13);
      assertTrue(moves.contains(ChessBoardPosition.B7));
      assertTrue(moves.contains(ChessBoardPosition.D7));
      assertTrue(moves.contains(ChessBoardPosition.E7));
      assertTrue(moves.contains(ChessBoardPosition.C8));
      assertTrue(moves.contains(ChessBoardPosition.C6));
      assertTrue(moves.contains(ChessBoardPosition.C5));
      assertTrue(moves.contains(ChessBoardPosition.C4));
      assertTrue(moves.contains(ChessBoardPosition.C3));
      assertTrue(moves.contains(ChessBoardPosition.B8));
      assertTrue(moves.contains(ChessBoardPosition.D6));
      assertTrue(moves.contains(ChessBoardPosition.D8));
      assertTrue(moves.contains(ChessBoardPosition.B6));
      assertTrue(moves.contains(ChessBoardPosition.A5));

      moves = ChessPieceType.QUEEN.moveCapture(chessBoard, whiteQueen);

      assertEquals(moves.size(), 3);
      assertTrue(moves.contains(ChessBoardPosition.A5));
      assertTrue(moves.contains(ChessBoardPosition.E7));
      assertTrue(moves.contains(ChessBoardPosition.C3));

      ChessPiece blackQueen = chessBoard.getPiece(ChessBoardPosition.C3);

      moves = ChessPieceType.QUEEN.moveNormal(chessBoard, blackQueen);

      assertEquals(moves.size(), 20);
      assertTrue(moves.contains(ChessBoardPosition.C1));
      assertTrue(moves.contains(ChessBoardPosition.C2));
      assertTrue(moves.contains(ChessBoardPosition.C4));
      assertTrue(moves.contains(ChessBoardPosition.C5));
      assertTrue(moves.contains(ChessBoardPosition.C6));
      assertTrue(moves.contains(ChessBoardPosition.C7));
      assertTrue(moves.contains(ChessBoardPosition.A3));
      assertTrue(moves.contains(ChessBoardPosition.B3));
      assertTrue(moves.contains(ChessBoardPosition.D3));
      assertTrue(moves.contains(ChessBoardPosition.E3));
      assertTrue(moves.contains(ChessBoardPosition.F3));
      assertTrue(moves.contains(ChessBoardPosition.G3));
      assertTrue(moves.contains(ChessBoardPosition.H3));
      assertTrue(moves.contains(ChessBoardPosition.D4));
      assertTrue(moves.contains(ChessBoardPosition.E5));
      assertTrue(moves.contains(ChessBoardPosition.B2));
      assertTrue(moves.contains(ChessBoardPosition.A1));
      assertTrue(moves.contains(ChessBoardPosition.B4));
      assertTrue(moves.contains(ChessBoardPosition.D2));
      assertTrue(moves.contains(ChessBoardPosition.E1));

      moves = ChessPieceType.QUEEN.moveCapture(chessBoard, blackQueen);

      assertEquals(moves.size(), 2);
      assertTrue(moves.contains(ChessBoardPosition.C7));
      assertTrue(moves.contains(ChessBoardPosition.E5));

      ChessPiece whiteRook = chessBoard.getPiece(ChessBoardPosition.A7);
      ChessPiece blackRook = chessBoard.getPiece(ChessBoardPosition.A5);

      moves = ChessPieceType.ROOK.moveNormal(chessBoard, whiteRook);

      assertEquals(moves.size(), 4);
      assertTrue(moves.contains(ChessBoardPosition.A8));
      assertTrue(moves.contains(ChessBoardPosition.A6));
      assertTrue(moves.contains(ChessBoardPosition.A5));
      assertTrue(moves.contains(ChessBoardPosition.B7));

      moves = ChessPieceType.ROOK.moveNormal(chessBoard, blackRook);

      assertEquals(moves.size(), 9);
      assertTrue(moves.contains(ChessBoardPosition.A7));
      assertTrue(moves.contains(ChessBoardPosition.A6));
      assertTrue(moves.contains(ChessBoardPosition.A4));
      assertTrue(moves.contains(ChessBoardPosition.A3));
      assertTrue(moves.contains(ChessBoardPosition.A2));
      assertTrue(moves.contains(ChessBoardPosition.A1));
      assertTrue(moves.contains(ChessBoardPosition.B5));
      assertTrue(moves.contains(ChessBoardPosition.C5));
      assertTrue(moves.contains(ChessBoardPosition.D5));

      ChessPiece blackPawn = chessBoard.getPiece(ChessBoardPosition.E7);
      ChessPiece whitePawn = chessBoard.getPiece(ChessBoardPosition.E5);

      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
      moves = ChessPieceType.PAWN.moveNormal(chessBoard, blackPawn);

      assertEquals(moves.size(), 1);
      assertTrue(moves.contains(ChessBoardPosition.E6));

      ChessMove whiteMove1 = ChessSide.WHITE.createMove(ChessBoardPosition.E5, ChessBoardPosition.E4);
      ChessBoardMove whiteBoardMove1 = chessBoard.getBoardMove(whiteMove1);
      
      whiteBoardMove1.makeMove(false);

      moves = ChessPieceType.PAWN.moveNormal(chessBoard, blackPawn);

      assertEquals(moves.size(), 2);
      assertTrue(moves.contains(ChessBoardPosition.E6));
      assertTrue(moves.contains(ChessBoardPosition.E5));

      whitePawn = chessBoard.getPiece(ChessBoardPosition.E4);

      whiteBoardMove1.revertMove();
      
      ChessMove blackMove1 = ChessSide.BLACK.createMove(ChessBoardPosition.E7, ChessBoardPosition.E6);
      ChessBoardMove blackBoardMove1 = chessBoard.getBoardMove(blackMove1);
      
      blackBoardMove1.makeMove(false);

      moves = ChessPieceType.PAWN.moveNormal(chessBoard, blackPawn);

      assertEquals(moves.size(), 2);
      assertTrue(moves.contains(ChessBoardPosition.D5));
      assertTrue(moves.contains(ChessBoardPosition.F5));
   }
}
