package com.zuooh.chess.analyze;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextMoveEvaluator;
import com.zuooh.chess.board.text.ChessTextPieceSet;

public class AttackAnalyzerTest extends TestCase {

   private static final String[][] BOARD = {
    // A     B     C     D     E     F     G    H      
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },//8
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },//7
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//6
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//5
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//4
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//3
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },//2
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};//1

   public void testAttackAnalyzer() throws Exception {
      assertUnderAttack(ChessBoardPosition.D7, ChessSide.BLACK, "w:e2->e3", "b:g7->g6", "w:d1->g4");
   }
   
   private void assertUnderAttack(ChessBoardPosition position, ChessSide side, String... playGame) throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet); 
      
      for(String move : playGame) {
         chessBoard = ChessTextMoveEvaluator.makeMove(chessBoard, move);         
         System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
         Thread.sleep(2000);
      }
      assertTrue("Position " + position + " not under attack", ChessAttackAnalyzer.underAttack(chessBoard, position, side) != 0);
   }
}
