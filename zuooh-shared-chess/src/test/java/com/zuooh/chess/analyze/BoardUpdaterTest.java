package com.zuooh.chess.analyze;

import static com.zuooh.chess.ChessBoardPosition.A2;
import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class BoardUpdaterTest extends TestCase {

   private static final String[][] BOARD = {
   // A     B     C     D     E     F     G    H
  { "wQ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 8
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 7
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 6
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 5
  { "  ", "bP", "  ", "  ", "  ", "  ", "  ", "  " },  // 4
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "bP" },  // 3
  { "wP", "  ", "  ", "  ", "  ", "  ", "wP", "  " },  // 2
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }}; // 1

  public void testEnPassant() throws Exception {
     ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
     ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
     ChessPiece whitePawn = chessBoard.getPiece(A2);
     
     System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));     
     
     int pawnMoveCount = chessBoard.getChangeCount();
     ChessMove pawnMove = new ChessMove(ChessBoardPosition.A2, ChessBoardPosition.A4, ChessSide.WHITE, pawnMoveCount);
     
     ChessBoardMove pawnBoardMove = chessBoard.getBoardMove(pawnMove);
     pawnBoardMove.makeMove();
     
     assertEquals(chessBoard.getMoveCount(whitePawn), 1);

     System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
     
     int passentMoveCount = chessBoard.getChangeCount();
     ChessMove passentMove = new ChessMove(ChessBoardPosition.B4, ChessBoardPosition.A3, ChessSide.BLACK, passentMoveCount);
     
     ChessBoardMove passentBoardMove = chessBoard.getBoardMove(passentMove);
     passentBoardMove.makeMove();
     
     System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
  }
}
