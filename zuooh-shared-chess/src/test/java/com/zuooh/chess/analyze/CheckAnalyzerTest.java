package com.zuooh.chess.analyze;

import junit.framework.TestCase;

import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextMoveEvaluator;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class CheckAnalyzerTest extends TestCase {
   
   private static final String[][] BOARD = {
   // A     B     C     D     E     F     G    H      
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },//8
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },//7
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//6
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//5
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//4
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//3
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },//2
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};//1
   
   public void testCheckMate() throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      String[] movesToMake = new String[]{
            "w:e2->e3",
            "b:g7->g6",
            "w:d1->g4",  
            "b:b8->a6",
            "w:f1->c4",
            "b:h7->h6",
            "w:g4->f4",
            "b:a6->b4",
            "w:f4->f7"
      };
      for(String moveToMake : movesToMake) {
         ChessMove move = ChessTextMoveEvaluator.parseMove(chessBoard, moveToMake);
         ChessBoardMove boardMove = chessBoard.getBoardMove(move);
         
         boardMove.makeMove();
         
         if(moveToMake.equals("w:f4->f7")) {
            assertTrue(ChessCheckAnalyzer.check(chessBoard));
            assertTrue(ChessCheckAnalyzer.checkMate(chessBoard));
         } else {
            assertFalse(ChessCheckAnalyzer.check(chessBoard));
            assertFalse(ChessCheckAnalyzer.checkMate(chessBoard));            
         }
         System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
         Thread.sleep(1000);
      }
   }
}
