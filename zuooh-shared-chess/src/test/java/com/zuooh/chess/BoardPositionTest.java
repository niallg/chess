package com.zuooh.chess;

import com.zuooh.chess.ChessBoardPosition;

import junit.framework.TestCase;

public class BoardPositionTest extends TestCase {
   
   public void testPositions() throws Exception {
     ChessBoardPosition a8 = ChessBoardPosition.at("A8");     
     System.err.printf("x=%s, y=%s, letter=%s digit=%s%n", a8.x, a8.y, a8.code, a8.digit);
     ChessBoardPosition b8 = ChessBoardPosition.at("B8");     
     System.err.printf("x=%s, y=%s, letter=%s digit=%s%n", b8.x, b8.y, b8.code, b8.digit); 
     ChessBoardPosition c8 = ChessBoardPosition.at("C8");     
     System.err.printf("x=%s, y=%s, letter=%s digit=%s%n", c8.x, c8.y, c8.code, c8.digit); 
     ChessBoardPosition d8 = ChessBoardPosition.at("D8");     
     System.err.printf("x=%s, y=%s, letter=%s digit=%s%n", d8.x, d8.y, d8.code, d8.digit);
     
     ChessBoardPosition a7 = ChessBoardPosition.at("A7");     
     System.err.printf("x=%s, y=%s, letter=%s digit=%s%n", a7.x, a7.y, a7.code, a7.digit);
     ChessBoardPosition b7 = ChessBoardPosition.at("B7");     
     System.err.printf("x=%s, y=%s, letter=%s digit=%s%n", b7.x, b7.y, b7.code, b7.digit); 
     ChessBoardPosition c7 = ChessBoardPosition.at("C7");     
     System.err.printf("x=%s, y=%s, letter=%s digit=%s%n", c7.x, c7.y, c7.code, c7.digit); 
     ChessBoardPosition d7 = ChessBoardPosition.at("D7");     
     System.err.printf("x=%s, y=%s, letter=%s digit=%s%n", d7.x, d7.y, d7.code, d7.digit);      
   }
   
   public void testAdvanceCount() throws Exception {
      assertEquals(ChessBoardPosition.A8.digit, 8);
      assertEquals(ChessBoardPosition.A7.digit, 7);
      assertEquals(ChessBoardPosition.A6.digit, 6);
      assertEquals(ChessBoardPosition.A5.digit, 5);  
   }
   
   public void testIndexPosition() throws Exception {
      assertEquals(ChessBoardPosition.at(0), ChessBoardPosition.A1);
      assertEquals(ChessBoardPosition.at(1), ChessBoardPosition.A2);
      assertEquals(ChessBoardPosition.at(2), ChessBoardPosition.A3);
      assertEquals(ChessBoardPosition.at(3), ChessBoardPosition.A4);  
      assertEquals(ChessBoardPosition.at(4), ChessBoardPosition.A5);
      assertEquals(ChessBoardPosition.at(5), ChessBoardPosition.A6);  
      assertEquals(ChessBoardPosition.at(6), ChessBoardPosition.A7);
      assertEquals(ChessBoardPosition.at(7), ChessBoardPosition.A8);  
      assertEquals(ChessBoardPosition.at(8), ChessBoardPosition.B1);
      assertEquals(ChessBoardPosition.at(9), ChessBoardPosition.B2);  
      assertEquals(ChessBoardPosition.at(10), ChessBoardPosition.B3);
      assertEquals(ChessBoardPosition.at(11), ChessBoardPosition.B4);  
   }

   public void testPosition() {
      assertEquals(ChessBoardPosition.at("A8").x, 0);
      assertEquals(ChessBoardPosition.at("A8").y, 0);
      assertEquals(ChessBoardPosition.at("A7").x, 0);
      assertEquals(ChessBoardPosition.at("A7").y, 1);
      assertEquals(ChessBoardPosition.at("A6").x, 0);
      assertEquals(ChessBoardPosition.at("A6").y, 2);
      assertEquals(ChessBoardPosition.at("A5").x, 0);
      assertEquals(ChessBoardPosition.at("A5").y, 3);
      assertEquals(ChessBoardPosition.at("A4").x, 0);
      assertEquals(ChessBoardPosition.at("A4").y, 4);
      assertEquals(ChessBoardPosition.at("A3").x, 0);
      assertEquals(ChessBoardPosition.at("A3").y, 5);
      assertEquals(ChessBoardPosition.at("A2").x, 0);
      assertEquals(ChessBoardPosition.at("A2").y, 6);
      assertEquals(ChessBoardPosition.at("A1").x, 0);
      assertEquals(ChessBoardPosition.at("A1").y, 7);

      assertEquals(ChessBoardPosition.at("B8").x, 1);
      assertEquals(ChessBoardPosition.at("B8").y, 0);
      assertEquals(ChessBoardPosition.at("B7").x, 1);
      assertEquals(ChessBoardPosition.at("B7").y, 1);
      assertEquals(ChessBoardPosition.at("B6").x, 1);
      assertEquals(ChessBoardPosition.at("B6").y, 2);
      assertEquals(ChessBoardPosition.at("B5").x, 1);
      assertEquals(ChessBoardPosition.at("B5").y, 3);
      assertEquals(ChessBoardPosition.at("B4").x, 1);
      assertEquals(ChessBoardPosition.at("B4").y, 4);
      assertEquals(ChessBoardPosition.at("B3").x, 1);
      assertEquals(ChessBoardPosition.at("B3").y, 5);
      assertEquals(ChessBoardPosition.at("B2").x, 1);
      assertEquals(ChessBoardPosition.at("B2").y, 6);
      assertEquals(ChessBoardPosition.at("B1").x, 1);
      assertEquals(ChessBoardPosition.at("B1").y, 7);

      assertEquals(ChessBoardPosition.at("C8").x, 2);
      assertEquals(ChessBoardPosition.at("C8").y, 0);
      assertEquals(ChessBoardPosition.at("C7").x, 2);
      assertEquals(ChessBoardPosition.at("C7").y, 1);
      assertEquals(ChessBoardPosition.at("C6").x, 2);
      assertEquals(ChessBoardPosition.at("C6").y, 2);
      assertEquals(ChessBoardPosition.at("C5").x, 2);
      assertEquals(ChessBoardPosition.at("C5").y, 3);
      assertEquals(ChessBoardPosition.at("C4").x, 2);
      assertEquals(ChessBoardPosition.at("C4").y, 4);
      assertEquals(ChessBoardPosition.at("C3").x, 2);
      assertEquals(ChessBoardPosition.at("C3").y, 5);
      assertEquals(ChessBoardPosition.at("C2").x, 2);
      assertEquals(ChessBoardPosition.at("C2").y, 6);
      assertEquals(ChessBoardPosition.at("C1").x, 2);
      assertEquals(ChessBoardPosition.at("C1").y, 7);

      assertEquals(ChessBoardPosition.at("H8").x, 7);
      assertEquals(ChessBoardPosition.at("H8").y, 0);
      assertEquals(ChessBoardPosition.at("H7").x, 7);
      assertEquals(ChessBoardPosition.at("H7").y, 1);
      assertEquals(ChessBoardPosition.at("H6").x, 7);
      assertEquals(ChessBoardPosition.at("H6").y, 2);
      assertEquals(ChessBoardPosition.at("H5").x, 7);
      assertEquals(ChessBoardPosition.at("H5").y, 3);
      assertEquals(ChessBoardPosition.at("H4").x, 7);
      assertEquals(ChessBoardPosition.at("H4").y, 4);
      assertEquals(ChessBoardPosition.at("H3").x, 7);
      assertEquals(ChessBoardPosition.at("H3").y, 5);
      assertEquals(ChessBoardPosition.at("H2").x, 7);
      assertEquals(ChessBoardPosition.at("H2").y, 6);
      assertEquals(ChessBoardPosition.at("H1").x, 7);
      assertEquals(ChessBoardPosition.at("H1").y, 7);
   }
}
