package com.zuooh.chess.board;

import static com.zuooh.chess.ChessBoardPosition.A2;
import static com.zuooh.chess.ChessBoardPosition.A4;
import static com.zuooh.chess.ChessBoardPosition.B4;
import static com.zuooh.chess.ChessBoardPosition.G2;
import static com.zuooh.chess.ChessBoardPosition.G3;
import static com.zuooh.chess.ChessBoardPosition.H3;

import java.util.List;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class EnPassantTest extends TestCase {

   private static final String[][] BOARD = {
    // A     B     C     D     E     F     G    H
   { "wQ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 8
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 7
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 6
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 5
   { "  ", "bP", "  ", "  ", "  ", "  ", "  ", "  " },  // 4
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "bP" },  // 3
   { "wP", "  ", "  ", "  ", "  ", "  ", "wP", "  " },  // 2
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }}; // 1

   public void testEnPassant() throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessMove move = new ChessMove(A2, A4, ChessSide.WHITE, 0);
      
      ChessPiece whitePawn = chessBoard.getPiece(A2);
      ChessPiece blackPawn = chessBoard.getPiece(B4);
      ChessBoardMove boardMove = chessBoard.getBoardMove(move);
      
      boardMove.makeMove(false);

      assertEquals(chessBoard.getMoveCount(whitePawn), 1);

      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
      List<ChessBoardPosition> moves = ChessPieceType.PAWN.moveNormal(chessBoard, blackPawn);
      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
      
      assertEquals(moves.size(), 2);
      assertTrue(moves.contains(ChessBoardPosition.A3));
      assertTrue(moves.contains(ChessBoardPosition.B3));

      whitePawn = chessBoard.getPiece(G2);
      blackPawn = chessBoard.getPiece(H3);      
      
      ChessMove move2 = new ChessMove(G2, G3, ChessSide.WHITE, 0);
      ChessBoardMove boardMove2 = chessBoard.getBoardMove(move2);
      
      boardMove2.makeMove(false);

      assertEquals(chessBoard.getMoveCount(whitePawn), 1);

      moves = ChessPieceType.PAWN.moveNormal(chessBoard, blackPawn);

      assertEquals(moves.size(), 1);
      assertTrue(moves.contains(ChessBoardPosition.H2));
   }
}
