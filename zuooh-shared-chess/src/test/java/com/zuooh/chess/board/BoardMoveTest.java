package com.zuooh.chess.board;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class BoardMoveTest extends TestCase {

   private static final String[][] BOARD = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" },
   };

   public void testBoardCopy() throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);      
      
      ChessBoardMove move1 = chessBoard.getBoardMove(new ChessMove(ChessBoardPosition.A2, ChessBoardPosition.A4, ChessSide.WHITE, chessBoard.getChangeCount()));
      
      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
      
      ChessMove lastWhiteMove1 = chessBoard.getLastMove(ChessSide.WHITE);
      ChessMove lastBlackMove1 = chessBoard.getLastMove(ChessSide.BLACK);
      int changeCount1 = chessBoard.getChangeCount();
      int moveCount1 = chessBoard.getMoveCount(chessBoard.getPiece(ChessBoardPosition.A2));
      
      assertNull(lastWhiteMove1);
      assertNull(lastBlackMove1);
      assertEquals(moveCount1, 0); // make sure it was the first move 
      
      move1.makeMove(true);
      
      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));      
      
      ChessMove lastWhiteMove2 = chessBoard.getLastMove(ChessSide.WHITE);
      ChessMove lastBlackMove2 = chessBoard.getLastMove(ChessSide.BLACK);
      int changeCount2 = chessBoard.getChangeCount();
      int moveCount2 = chessBoard.getMoveCount(chessBoard.getPiece(ChessBoardPosition.A4));
      
      assertNotNull(lastWhiteMove2);
      assertEquals(lastWhiteMove2.from, ChessBoardPosition.A2);
      assertEquals(lastWhiteMove2.to, ChessBoardPosition.A4);
      assertEquals(lastWhiteMove2.side, ChessSide.WHITE);
      assertNull(lastBlackMove2);
      assertEquals(moveCount2, 1); // make sure it was the first move      
      assertEquals(changeCount1 + 1, changeCount2);
      
      move1.revertMove();
      
      ChessMove lastWhiteMove3 = chessBoard.getLastMove(ChessSide.WHITE);
      ChessMove lastBlackMove3 = chessBoard.getLastMove(ChessSide.BLACK);
      int changeCount3 = chessBoard.getChangeCount();
      int moveCount3 = chessBoard.getMoveCount(chessBoard.getPiece(ChessBoardPosition.A2));
      
      assertNull(lastWhiteMove3);
      assertNull(lastBlackMove3);
      assertEquals(moveCount3, 0); // make sure it was the first move      
      assertEquals(changeCount3, changeCount1);
      
      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
      
     /* ChessBoardUpdater.makeMove(originalBoard, );
      ChessBoardUpdater.makeMove(originalBoard, new ChessMove(ChessBoardPosition.B7, ChessBoardPosition.B5, ChessSide.BLACK, originalBoard.getChangeCount()));
      ChessBoardUpdater.makeMove(originalBoard, new ChessMove(ChessBoardPosition.G1, ChessBoardPosition.H3, ChessSide.WHITE, originalBoard.getChangeCount()));
      ChessBoardUpdater.makeMove(originalBoard, new ChessMove(ChessBoardPosition.G8, ChessBoardPosition.F6, ChessSide.BLACK, originalBoard.getChangeCount()));*/
   }
}
