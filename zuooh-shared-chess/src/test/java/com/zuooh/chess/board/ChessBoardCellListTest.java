package com.zuooh.chess.board;

import java.util.List;

import junit.framework.TestCase;

import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.text.ChessTextPieceSet;

public class ChessBoardCellListTest extends TestCase {

   private static final String[][] BOARD = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" },
   };

   public void testCellList() throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessBoardCellIterator whiteCells = chessBoard.getBoardCells(ChessSide.WHITE);
      ChessBoardCellIterator blackCells = chessBoard.getBoardCells(ChessSide.BLACK);
      int whiteCount = 0;
      int blackCount = 0;
      
      while(whiteCells.hasNext()) {
         ChessBoardCell boardCell = whiteCells.next();      
         ChessPiece chessPiece = boardCell.getPiece();
         assertEquals(chessPiece.side, ChessSide.WHITE);
         whiteCount++;
      }
      while(blackCells.hasNext()) {
         ChessBoardCell boardCell = blackCells.next(); 
         ChessPiece chessPiece = boardCell.getPiece();
         assertEquals(chessPiece.side, ChessSide.BLACK);
         blackCount++;
      }
      assertEquals(16, whiteCount);
      assertEquals(16, blackCount);
   }   
}
