package com.zuooh.chess.board;

import static com.zuooh.chess.ChessBoardPosition.A1;
import static com.zuooh.chess.ChessBoardPosition.A2;
import junit.framework.TestCase;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class PawnPromotionTest extends TestCase {

   private static final String[][] BOARD = {
      // A     B     C     D     E     F     G    H
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 8
   { "wR", "  ", "wQ", "  ", "bP", "  ", "  ", "  " },  // 7
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 6
   { "bR", "  ", "  ", "wP", "wP", "wP", "  ", "  " },  // 5
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 4
   { "  ", "  ", "bQ", "  ", "  ", "  ", "  ", "  " },  // 3
   { "bP", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 2
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }}; // 1

   public void testPromotion() throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessMove move = new ChessMove(A2, A1, ChessSide.BLACK, 0);
      ChessBoardMove boardMove = chessBoard.getBoardMove(move);
      
      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
      boardMove.makeMove(false);
      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
   }
}
