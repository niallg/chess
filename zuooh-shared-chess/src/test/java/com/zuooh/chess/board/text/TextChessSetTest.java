package com.zuooh.chess.board.text;

import static com.zuooh.chess.ChessBoardPosition.C7;
import static com.zuooh.chess.ChessBoardPosition.D7;
import static com.zuooh.chess.ChessBoardPosition.E8;
import junit.framework.TestCase;

import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;

public class TextChessSetTest extends TestCase {

   private static final String[][] BOARD = {
   { "  ", "  ", "  ", "  ", "b#", "  ", "  ", "  " },
   { "  ", "  ", "bP", "bP", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   };

   public void testChessSet() {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);

      assertEquals(chessBoard.getPiece(C7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(C7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(D7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(D7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(E8).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(E8).getPieceType(), ChessPieceType.KING);
   }
}
