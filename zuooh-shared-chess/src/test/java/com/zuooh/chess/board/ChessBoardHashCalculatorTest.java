package com.zuooh.chess.board;

import java.util.HashMap;
import java.util.Map;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;

public class ChessBoardHashCalculatorTest extends TestCase {

   public void testBoardHashStrength() throws Exception{
      Map<Long, String> alreadyDone = new HashMap<Long, String>();
      ChessPieceType[] types = new ChessPieceType[]{
            ChessPieceType.PAWN,
            ChessPieceType.KNIGHT,
            ChessPieceType.BISHOP,
            ChessPieceType.ROOK,
            ChessPieceType.QUEEN,
            ChessPieceType.KING
      };
      ChessSide[] directions = new ChessSide[]{
            ChessSide.WHITE,
            ChessSide.BLACK
      };
      for(int i = 0; i < 64; i++) {
         for(int j = 0; j < 6; j++) {
            for(int k = 0; k < 2; k++) {
               String text = ""+types[j]+"/"+directions[k]+"/"+ChessBoardPosition.at(i);
               long hash = ChessBoardHashCalculator.calculatePieceHash(types[j], directions[k], ChessBoardPosition.at(i));
               String done = alreadyDone.put(hash, text);
               
               if(done != null) {
                  ChessBoardHashCalculator.calculatePieceHash(types[j], directions[k], ChessBoardPosition.at(i));
                  ChessBoardHashCalculator.calculatePieceHash(ChessPieceType.PAWN, ChessSide.WHITE, ChessBoardPosition.A2);
                  ChessBoardHashCalculator.calculatePieceHash(ChessPieceType.KNIGHT, ChessSide.WHITE, ChessBoardPosition.A1);                  
                  throw new IllegalStateException("Already done prev="+done+" curr="+text);
               }
            }
         }
      }
   }
}
