package com.zuooh.chess.board;

import static com.zuooh.chess.ChessBoardPosition.A5;
import static com.zuooh.chess.ChessBoardPosition.A7;
import static com.zuooh.chess.ChessBoardPosition.A8;
import static com.zuooh.chess.ChessBoardPosition.B7;
import static com.zuooh.chess.ChessBoardPosition.C7;
import static com.zuooh.chess.ChessBoardPosition.D7;
import static com.zuooh.chess.ChessBoardPosition.E7;
import static com.zuooh.chess.ChessBoardPosition.F7;
import static com.zuooh.chess.ChessBoardPosition.G7;
import static com.zuooh.chess.ChessBoardPosition.H7;

import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class PiecePositionTest extends TestCase {

   private static final String[][] BOARD = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" },
   };

   public void testChessBoard() throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessBoardCell fromCell = chessBoard.getBoardCell(A8);
      ChessBoardCell toCell = chessBoard.getBoardCell(A5);
      ChessPiece chessPiece = fromCell.getPiece();

      assertNotNull(chessBoard.getPiece(A7));
      assertNotNull(chessBoard.getPiece(B7));
      assertNotNull(chessBoard.getPiece(C7));
      assertNotNull(chessBoard.getPiece(D7));
      assertNotNull(chessBoard.getPiece(E7));
      assertNotNull(chessBoard.getPiece(F7));
      assertNotNull(chessBoard.getPiece(G7));
      assertNotNull(chessBoard.getPiece(H7));

      assertEquals(chessBoard.getPiece(A7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(B7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(C7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(D7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(E7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(F7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(G7).getPieceType(), ChessPieceType.PAWN);
      assertEquals(chessBoard.getPiece(H7).getPieceType(), ChessPieceType.PAWN);

      assertEquals(chessBoard.getPiece(A7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(B7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(C7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(D7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(E7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(F7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(G7).getSide(), ChessSide.BLACK);
      assertEquals(chessBoard.getPiece(H7).getSide(), ChessSide.BLACK);

      ChessMove move = new ChessMove(A8, A5, ChessSide.BLACK, 0);
      ChessBoardMove boardMove = chessBoard.getBoardMove(move);
      boardMove.makeMove(false);

      assertNull(chessBoard.getPiece(A8));
      assertNotNull(chessBoard.getPiece(A5));
      assertEquals(ChessPieceType.ROOK, chessBoard.getPiece(A5).getPieceType());

      List<ChessPiece> blackPieces = new LinkedList<ChessPiece>();
      ChessBoardCellIterator boardCells = chessBoard.getBoardCells(ChessSide.BLACK);
      
      while(boardCells.hasNext()) {
         ChessBoardCell boardCell = boardCells.next();
         ChessPiece nextPiece = boardCell.getPiece();
         blackPieces.add(nextPiece);         
      }
      assertEquals(blackPieces.size(), 16);
   }
}
