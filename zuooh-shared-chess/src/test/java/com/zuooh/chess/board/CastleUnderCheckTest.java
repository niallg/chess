package com.zuooh.chess.board;

import java.util.List;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.text.ChessTextPieceSet;

public class CastleUnderCheckTest extends TestCase {

   private static final String[][] BOARD = {
      // A     B     C     D     E     F     G    H
     { "bR", "bK", "  ", "  ", "b#", "  ", "  ", "bR" },  // 8
     { "bP", "  ", "wK", "  ", "bK", "  ", "  ", "bP" },  // 7
     { "  ", "bP", "  ", "  ", "  ", "bP", "  ", "  " },  // 6
     { "  ", "wP", "  ", "  ", "  ", "  ", "bP", "  " },  // 5
     { "  ", "wP", "  ", "bP", "  ", "  ", "  ", "  " },  // 4
     { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "wP" },  // 3
     { "wP", "  ", "  ", "  ", "  ", "wP", "  ", "wP" },  // 2
     { "wR", "  ", "wB", "  ", "w#", "  ", "wK", "wR" }}; // 1
   
   
   public void testCastleUnderCheck() throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessPiece blackKing = chessBoard.getPiece(ChessBoardPosition.E8);
      
      assertNotNull(blackKing);
      assertEquals(blackKing.side, ChessSide.BLACK);
      assertEquals(blackKing.key, ChessPieceKey.BLACK_KING);
      
      List<ChessBoardPosition> possibleMoves = blackKing.moveNormal(chessBoard);
      
      assertTrue("The black king should be in check", ChessCheckAnalyzer.check(chessBoard));
      assertTrue("The black king should be in check", ChessCheckAnalyzer.check(chessBoard, ChessSide.BLACK));
      assertFalse("The white king is not in check", ChessCheckAnalyzer.check(chessBoard, ChessSide.WHITE));        
      
      for(ChessBoardPosition possibleMove : possibleMoves) {
         assertFalse("Should not be able to castle under check '" + possibleMoves+ "'", possibleMove == ChessBoardPosition.G8);
      }
   }
}
