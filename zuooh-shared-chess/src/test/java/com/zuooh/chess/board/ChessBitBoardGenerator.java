package com.zuooh.chess.board;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessMoveDirection;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.binary.ChessBitBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class ChessBitBoardGenerator {

   private static final String[][] BOARD = {
   // A     B     C     D     E     F     G    H
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 8
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 7
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 6
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 5
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 4
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 3
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 2
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }}; // 1

   public static void main(String[] list) throws Exception {
      //drawAttackBoards(ChessPieceType.PAWN, true);
      //drawAttackBoards(ChessPieceType.PAWN, false);
      //drawAttackBoards(null, true);
      //drawAttackBoards(null, false);
      //drawEnPassentCriteriaBoards(true);
      //drawEnPassentCriteriaBoards(false);  
      //drawMoveBoards(ChessPieceType.PAWN, true);
      //drawMoveBoards(ChessPieceType.PAWN, false);
      //drawMoveBoards(null, true);
      //drawMoveBoards(null, false);
      //directionMoveBoard(false);
      directionMoveOneBoard(false);      
   }
   
   public static void directionMoveOneBoard(boolean debug)throws Exception{
      for(ChessMoveDirection dir : ChessMoveDirection.values()){
         drawDirectionOneMoveBoard(dir, debug);
      }
   }
   
   public static void drawDirectionOneMoveBoard(ChessMoveDirection direction, boolean debug) throws Exception {    
      Map<ChessBoardPosition, ChessBoardPosition> mapPositions = new LinkedHashMap<ChessBoardPosition, ChessBoardPosition>();
      String name = direction + "_ADJACENT_TO";
      
      for(int i = 0; i < 64; i++) {       
         ChessBoardPosition original = ChessBoardPosition.at(i);  
         ChessBoardPosition pos = ChessBoardPosition.at(i);  
         
         pos = direction.adjacent(pos); // just the next move, which you can and
         mapPositions.put(original, pos);
      }
      String resultBoard =  drawPositions(mapPositions, name, name, debug);      
      System.err.println(resultBoard);
   }
   
   public static void directionMoveBoard(boolean debug)throws Exception{
      for(ChessMoveDirection dir : ChessMoveDirection.values()){
         drawDirectionMoveBoard(dir, debug);
      }
   }
   
   public static void drawDirectionMoveBoard(ChessMoveDirection direction, boolean debug) throws Exception {    
      Map<ChessBoardPosition, ChessBitBoard> positionBoards = new LinkedHashMap<ChessBoardPosition, ChessBitBoard>();
      String name = direction + "_LINE_FROM";
      
      for(int i = 0; i < 64; i++) {       
         ChessBoardPosition original = ChessBoardPosition.at(i);  
         ChessBoardPosition pos = ChessBoardPosition.at(i);        
         long boardMask = 0L;
         
         while(pos != null) {
            pos = direction.adjacent(pos);
            if(pos != null){
               boardMask |= pos.mask;
            }
         }
         ChessBitBoard bitBoard = new ChessBitBoard(boardMask); 
         positionBoards.put(original, bitBoard);
      }
      String resultBoard =  drawBitBoard(positionBoards, name, name, debug);      
      System.err.println(resultBoard);
   }
   
   public static void drawEnPassentCriteriaBoards(boolean debug) throws Exception {
      Map<String, Map<ChessBoardPosition, ChessBitBoard>> bitBoards = new LinkedHashMap<String, Map<ChessBoardPosition, ChessBitBoard>>();
      
      for(ChessSide side : ChessSide.values()) {        
         String name = "PAWN_" + side + "_PASSENT_ATTACKS_FROM"; 
         Map<ChessBoardPosition, ChessBitBoard> positionBoards = new LinkedHashMap<ChessBoardPosition, ChessBitBoard>();
         bitBoards.put(name, positionBoards);
         
         for(int i = 0; i < 64; i++) {    
            ChessBoardPosition pos = ChessBoardPosition.at(i);   
            positionBoards.put(pos, new ChessBitBoard(ChessBitBoard.OCCUPY_NONE));
         }         
         for(int i = 0; i < 8; i++) {            
            ChessPiece piece = new ChessPiece("E1", ChessPieceType.PAWN, side);
            ChessPieceSet set = new ChessTextPieceSet(BOARD);
            ChessBoard board = new ChessPieceSetBoard(set);
            ChessBoardPosition pos = null;
            
            if(side == ChessSide.WHITE) {
               pos = ChessBoardPosition.at(i, 3);         
            } else {
               pos = ChessBoardPosition.at(i, 4);
            }
            ChessBoardCell cell = board.getBoardCell(pos);
            ChessBoardPosition destination = cell.getPosition();
            ChessBoardPosition source = board.getPosition(piece);
            ChessMove moveToMake = new ChessMove(source, destination, piece.side, 0);
            ChessBoardMove boardMove = board.getBoardMove(moveToMake);
            
            boardMove.makeMove(false); 
            
            List<ChessBoardPosition> moves = new ArrayList<ChessBoardPosition>();                  
            ChessBoardPosition opponentEast = ChessBoardPosition.at(pos.x + 1, pos.y);
            ChessBoardPosition opponentWest = ChessBoardPosition.at(pos.x - 1, pos.y);

            if(opponentEast != null) {
               moves.add(opponentEast);
            }
            if(opponentWest != null) {
               moves.add(opponentWest);
            }
            long boardMask = 0L;         
            
            for(ChessBoardPosition move : moves) {
               boardMask |= move.mask;
            }                 
            ChessBitBoard bitBoard = new ChessBitBoard(boardMask); 
            positionBoards.put(pos, bitBoard);
         }
      }
      Map<String, String> sortedResults = new TreeMap<String, String>();      
      Set<String> names = bitBoards.keySet();
      
      for(String name : names) {
         Map<ChessBoardPosition, ChessBitBoard> positionBoards = bitBoards.get(name);
        String resultBoard =  drawBitBoard(positionBoards, name, name, debug);
        sortedResults.put(name, resultBoard);        
      }
      if(!debug){
         Set<String> resultNames = sortedResults.keySet();
         for(String resultName : resultNames){
            System.err.println(sortedResults.get(resultName));
         }      
      }
   }
   
   public static void drawMoveBoards(ChessPieceType filterType, boolean debug) throws Exception {
      Map<String, Map<ChessBoardPosition, ChessBitBoard>> bitBoards = new LinkedHashMap<String, Map<ChessBoardPosition, ChessBitBoard>>();
      Map<String, String> sameBoards = new LinkedHashMap<String, String>();
      Set<String> boardsDone = new LinkedHashSet<String>();
      
      for(ChessSide side : ChessSide.values()) {
         for(ChessPieceType type : ChessPieceType.values()) {
            Map<ChessBoardPosition, ChessBitBoard> positionBoards = new LinkedHashMap<ChessBoardPosition, ChessBitBoard>();
            
            if(type != ChessPieceType.SHADOW && (filterType == null || filterType == type)) {
               String name = type + "_" + side + "_MOVES_TO";
               String same = name;
               
               if(type != ChessPieceType.PAWN) {
                  same = type + "_MOVES_TO";
               }
               bitBoards.put(name, positionBoards);
               sameBoards.put(name, same);
               
               for(int i = 0; i < 64; i++) {       
                  ChessPiece piece = new ChessPiece("E1",type,side);
                  ChessPieceSet set = new ChessTextPieceSet(BOARD);
                  ChessBoard board = new ChessPieceSetBoard(set);
                  ChessBoardPosition currentPosition = ChessBoardPosition.at(i);         
                  ChessBoardCell currentCell = board.getBoardCell(currentPosition);
                  ChessBoardPosition position = board.getPosition(piece);
                  ChessBoardPosition destination = currentCell.getPosition();
                  ChessMove moveToMake = new ChessMove(position, destination, piece.side, 0);
                  ChessBoardMove boardMove = board.getBoardMove(moveToMake);
                  
                  boardMove.makeMove(false);
                  
                  List<ChessBoardPosition> moves = new ArrayList<ChessBoardPosition>();
                 
                  if(type != ChessPieceType.PAWN) {
                     moves = piece.moveNormal(board);
                     
                     if(type == ChessPieceType.KING) {
                        if(side == ChessSide.WHITE && currentPosition == ChessBoardPosition.E1) {
                           moves.add(ChessBoardPosition.G1);
                           moves.add(ChessBoardPosition.C1);                       
                        } else if(side == ChessSide.BLACK && currentPosition == ChessBoardPosition.E8) {
                           moves.add(ChessBoardPosition.G8);
                           moves.add(ChessBoardPosition.C8);
                        }
                     }
                  } else {
                     if(side == ChessSide.WHITE) { 
                        ChessBoardPosition whitePawnPosition = currentPosition;
                        
                        if(whitePawnPosition.digit >= 2 && whitePawnPosition.digit <= 7) {
                           ChessBoardPosition northEast = ChessBoardPosition.at(whitePawnPosition.x + 1, whitePawnPosition.y - 1);
                           ChessBoardPosition northWest = ChessBoardPosition.at(whitePawnPosition.x - 1, whitePawnPosition.y - 1);
                           ChessBoardPosition north = ChessBoardPosition.at(whitePawnPosition.x, whitePawnPosition.y - 1);                          
                           
                           if(whitePawnPosition.digit == 2){
                              ChessBoardPosition northNorth = ChessBoardPosition.at(whitePawnPosition.x, whitePawnPosition.y - 2);
                              
                              if(northNorth != null) {
                                 moves.add(northNorth);
                              }
                           }
                           if(northEast != null) {
                              moves.add(northEast);
                           }
                           if(northWest != null) {
                              moves.add(northWest);
                           }
                           if(north != null) {
                              moves.add(north);
                           }
                        }
                     } else {
                        if(currentPosition==ChessBoardPosition.H7){
                           System.err.println();
                        }
                        ChessBoardPosition blackPawnPosition = currentPosition;
                        
                        if(blackPawnPosition.digit <= 7 && blackPawnPosition.digit >= 1) {
                           ChessBoardPosition southEast = ChessBoardPosition.at(blackPawnPosition.x + 1, blackPawnPosition.y + 1);
                           ChessBoardPosition southWest = ChessBoardPosition.at(blackPawnPosition.x - 1, blackPawnPosition.y + 1);
                           ChessBoardPosition south = ChessBoardPosition.at(blackPawnPosition.x, blackPawnPosition.y + 1);                            
                           
                           if(blackPawnPosition.digit == 7){
                              ChessBoardPosition southSouth = ChessBoardPosition.at(blackPawnPosition.x, blackPawnPosition.y + 2);
                              
                              if(southSouth != null) {
                                 moves.add(southSouth);
                              }
                           }
                           if(southEast != null) {
                              moves.add(southEast);
                           }
                           if(southWest != null) {
                              moves.add(southWest);
                           }
                           if(south != null) {
                              moves.add(south);
                           }
                        }
                     }
                  }
                  long boardMask = 0L;         
                  
                  for(ChessBoardPosition move : moves) {
                     boardMask |= move.mask;
                  }                 
                  ChessBitBoard bitBoard = new ChessBitBoard(boardMask);                  
                  ChessBitBoard alreadyDone = positionBoards.put(currentPosition, bitBoard);
                  
                  if(alreadyDone != null) {
                     throw new IllegalStateException("Position " + currentPosition + " already evaluated");
                  }
               }
            }
         }
      }
      Map<String, String> sortedResults = new TreeMap<String, String>();      
      Set<String> names = bitBoards.keySet();
      
      for(String name : names) {
         Map<ChessBoardPosition, ChessBitBoard> positionBoards = bitBoards.get(name);
         String realName = sameBoards.get(name);
         
         if(boardsDone.add(realName)) { // all boards except pawns share the same move types
            String resultBoard =  drawBitBoard(positionBoards, name, realName, debug);
            sortedResults.put(realName, resultBoard);
         }         
      }
      if(!debug){
         Set<String> resultNames = sortedResults.keySet();
         for(String resultName : resultNames){
            System.err.println(sortedResults.get(resultName));
         }      
      }
   } 
   
   
   public static void drawAttackBoards(ChessPieceType filterType, boolean debug) throws Exception {
      Map<String, Map<ChessBoardPosition, ChessBitBoard>> bitBoards = new LinkedHashMap<String, Map<ChessBoardPosition, ChessBitBoard>>();
      Map<String, String> sameBoards = new LinkedHashMap<String, String>();
      Set<String> boardsDone = new LinkedHashSet<String>();
      
      for(ChessSide underAttackSide : ChessSide.values()) {
         for(ChessPieceType type : ChessPieceType.values()) {
            Map<ChessBoardPosition, ChessBitBoard> positionBoards = new LinkedHashMap<ChessBoardPosition, ChessBitBoard>();
            
            if(type != ChessPieceType.SHADOW && (filterType == null || filterType == type)) {
               String name = type + "_" + underAttackSide.oppositeSide() + "_ATTACKS_FROM";
               String same = name;
               
               if(type != ChessPieceType.PAWN) {
                  same = type + "_ATTACKS_FROM";
               }
               bitBoards.put(name, positionBoards);
               sameBoards.put(name, same);
               
               for(int i = 0; i < 64; i++) {       
                  ChessPiece underAttackPiece = new ChessPiece("E1",type,underAttackSide);
                  ChessPieceSet set = new ChessTextPieceSet(BOARD);
                  ChessBoard board = new ChessPieceSetBoard(set);
                  ChessBoardPosition underAttackPosition = ChessBoardPosition.at(i);         
                  ChessBoardCell underAttackCell = board.getBoardCell(underAttackPosition);
                  ChessBoardPosition fromPos = board.getPosition(underAttackPiece);
                  ChessBoardPosition toPos = underAttackCell.getPosition();
                  ChessMove attackMove = new ChessMove(fromPos, toPos, underAttackPiece.side, 0);
                  ChessBoardMove attackBoardMove = board.getBoardMove(attackMove);
                  
                  attackBoardMove.makeMove();
       
                  List<ChessBoardPosition> moves = new ArrayList<ChessBoardPosition>();
                 
                  if(underAttackPosition == ChessBoardPosition.H7) {
                     System.err.println();
                  }
                  if(type != ChessPieceType.PAWN) {
                     moves = underAttackPiece.moveNormal(board);         
                  } else {
                     if(underAttackSide == ChessSide.WHITE) { 
                        ChessBoardPosition anyWhitePiece = underAttackPosition;
                        
                        if(anyWhitePiece.digit <= 6) { // white is being attacked by a black pawn
                           ChessBoardPosition northEast = ChessBoardPosition.at(anyWhitePiece.x + 1, anyWhitePiece.y - 1); // black attacks white from
                           ChessBoardPosition northWest = ChessBoardPosition.at(anyWhitePiece.x - 1, anyWhitePiece.y - 1); // black attacks white from
                           
                           if(northEast != null) {
                              moves.add(northEast);
                           }
                           if(northWest != null) {
                              moves.add(northWest);
                           }                           

                        }
                     } else {
                        ChessBoardPosition anyBlackPiece = underAttackPosition;//could be any piece attacked by a pawn
                                                
                        if(anyBlackPiece.digit >= 3) {
                           ChessBoardPosition southEast = ChessBoardPosition.at(anyBlackPiece.x + 1, anyBlackPiece.y + 1); // white attacks black from
                           ChessBoardPosition southWest = ChessBoardPosition.at(anyBlackPiece.x - 1, anyBlackPiece.y + 1); // white attacks black from
                           
                           if(southEast != null) {
                              moves.add(southEast);
                           }
                           if(southWest != null) {
                              moves.add(southWest);
                           }
                        }
                     }
                  }
                  long boardMask = 0L;         
                  
                  for(ChessBoardPosition move : moves) {
                     boardMask |= move.mask;
                  }                 
                  ChessBitBoard bitBoard = new ChessBitBoard(boardMask); 
                  ChessBitBoard alreadyDone = positionBoards.put(underAttackPosition, bitBoard);
                  
                  if(alreadyDone != null) {
                     throw new IllegalStateException("Position " + underAttackPosition + " already evaluated");
                  }
               }
            }
         }
      }
      Map<String, String> sortedResults = new TreeMap<String, String>();      
      Set<String> names = bitBoards.keySet();
      
      for(String name : names) {
         Map<ChessBoardPosition, ChessBitBoard> positionBoards = bitBoards.get(name);
         String realName = sameBoards.get(name);
         
         if(boardsDone.add(realName)) { // all boards except pawns share the same move types
            String resultBoard =  drawBitBoard(positionBoards, name, realName, debug);
            sortedResults.put(realName, resultBoard);
         }         
      }
      if(!debug){
         Set<String> resultNames = sortedResults.keySet();
         for(String resultName : resultNames){
            System.err.println(sortedResults.get(resultName));
         }      
      }
   }
   
   private static String drawPositions(Map<ChessBoardPosition, ChessBoardPosition> mapPositions, String name, String realName, boolean debug) {
      Set<ChessBoardPosition> positions = mapPositions.keySet();
      ChessBoardPosition[] positionList = new ChessBoardPosition[64];
      
      for(ChessBoardPosition position : positions) {
         ChessBoardPosition nextPos = mapPositions.get(position);
         ChessBitBoard bitBoard = new ChessBitBoard();
         
         if(nextPos != null) {
            bitBoard = new ChessBitBoard(nextPos.mask);
         }         
         if(debug) {
            System.err.println();
            System.err.println();                 
            System.err.println("###### " + position + " (" + name + ") I am " + (name.indexOf("WHITE") != -1 ? "BLACK" : "WHITE")+ " ######");    
            System.err.println(bitBoard);               
         }
         positionList[position.index] = nextPos;
      }
      StringBuilder builder = new StringBuilder();
                                        
      builder.append("   private static final int[][] " + realName + " = {");
      for(int i = 0; i < 8; i++) {
         builder.append("\n   {");               
      
         for(int j = 0; j < 8; j++) {
            ChessBoardPosition pos = ChessBoardPosition.at(i, j);
            ChessBoardPosition next = positionList[pos.index];
            int index = -1;
            
            if(next != null){
               index = next.index;
            }
            String value = String.valueOf(index);
            int length = value.length();
            
            if(length < 2){
               value = " " + value;
            }
            builder.append(value);
            
            if(j < 7) {
               builder.append(", ");
            }
         }
         if(i < 7) {
            builder.append("},");
         } else {
            builder.append("}");
         }
      }
      builder.append("};\n");
      return builder.toString();
   }   
   
   private static String drawBitBoard(Map<ChessBoardPosition, ChessBitBoard> positionBoards, String name, String realName, boolean debug) {
      Set<ChessBoardPosition> positions = positionBoards.keySet();
      long[] positionMasks = new long[64];
      
      for(ChessBoardPosition position : positions) {
         ChessBitBoard bitBoard = positionBoards.get(position);
         
         if(debug) {
            System.err.println();
            System.err.println();                 
            System.err.println("###### " + position + " (" + name + ") I am " + (name.indexOf("WHITE") != -1 ? "BLACK" : "WHITE")+ " ######");    
            System.err.println(bitBoard);               
         }
         positionMasks[position.index] = bitBoard.value();
      }
      StringBuilder builder = new StringBuilder();
                                        
      builder.append("   private static final long[][] " + realName + " = {");
      for(int i = 0; i < 8; i++) {
         builder.append("\n   {");               
      
         for(int j = 0; j < 8; j++) {
            ChessBoardPosition pos = ChessBoardPosition.at(i, j);
            builder.append(toProperHex(positionMasks[pos.index]));
            
            if(j < 7) {
               builder.append(", ");
            }
         }
         if(i < 7) {
            builder.append("},");
         } else {
            builder.append("}");
         }
      }
      builder.append("};\n");
      return builder.toString();
   }
   
   private static String toProperHex(long value) {
      String hex = Long.toHexString(value);
      for(int i = 0; i < 64; i++) {
         int length = hex.length();
         
         if(length < 16) {
            hex = "0" + hex;
         }
      }
      return String.format("0x%sL", hex);
   }
}
