package com.zuooh.chess.engine;

import com.zuooh.chess.ChessSide;

public class ChessNullMove extends ChessMove {
   
   private static final long serialVersionUID = 1L;

   public ChessNullMove(ChessSide side, int changeCount) {
      super(null, null, side, changeCount);
   }

   public String toString() {
      return String.format("NULL (%s)", side);
   }

}
