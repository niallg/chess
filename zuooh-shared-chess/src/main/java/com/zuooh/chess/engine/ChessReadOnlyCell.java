package com.zuooh.chess.engine;

import static com.zuooh.chess.ChessBoardPosition.A1;
import static com.zuooh.chess.ChessBoardPosition.A2;
import static com.zuooh.chess.ChessBoardPosition.A3;
import static com.zuooh.chess.ChessBoardPosition.A4;
import static com.zuooh.chess.ChessBoardPosition.A5;
import static com.zuooh.chess.ChessBoardPosition.A6;
import static com.zuooh.chess.ChessBoardPosition.A7;
import static com.zuooh.chess.ChessBoardPosition.A8;
import static com.zuooh.chess.ChessBoardPosition.B1;
import static com.zuooh.chess.ChessBoardPosition.B2;
import static com.zuooh.chess.ChessBoardPosition.B3;
import static com.zuooh.chess.ChessBoardPosition.B4;
import static com.zuooh.chess.ChessBoardPosition.B5;
import static com.zuooh.chess.ChessBoardPosition.B6;
import static com.zuooh.chess.ChessBoardPosition.B7;
import static com.zuooh.chess.ChessBoardPosition.B8;
import static com.zuooh.chess.ChessBoardPosition.C1;
import static com.zuooh.chess.ChessBoardPosition.C2;
import static com.zuooh.chess.ChessBoardPosition.C3;
import static com.zuooh.chess.ChessBoardPosition.C4;
import static com.zuooh.chess.ChessBoardPosition.C5;
import static com.zuooh.chess.ChessBoardPosition.C6;
import static com.zuooh.chess.ChessBoardPosition.C7;
import static com.zuooh.chess.ChessBoardPosition.C8;
import static com.zuooh.chess.ChessBoardPosition.D1;
import static com.zuooh.chess.ChessBoardPosition.D2;
import static com.zuooh.chess.ChessBoardPosition.D3;
import static com.zuooh.chess.ChessBoardPosition.D4;
import static com.zuooh.chess.ChessBoardPosition.D5;
import static com.zuooh.chess.ChessBoardPosition.D6;
import static com.zuooh.chess.ChessBoardPosition.D7;
import static com.zuooh.chess.ChessBoardPosition.D8;
import static com.zuooh.chess.ChessBoardPosition.E1;
import static com.zuooh.chess.ChessBoardPosition.E2;
import static com.zuooh.chess.ChessBoardPosition.E3;
import static com.zuooh.chess.ChessBoardPosition.E4;
import static com.zuooh.chess.ChessBoardPosition.E5;
import static com.zuooh.chess.ChessBoardPosition.E6;
import static com.zuooh.chess.ChessBoardPosition.E7;
import static com.zuooh.chess.ChessBoardPosition.E8;
import static com.zuooh.chess.ChessBoardPosition.F1;
import static com.zuooh.chess.ChessBoardPosition.F2;
import static com.zuooh.chess.ChessBoardPosition.F3;
import static com.zuooh.chess.ChessBoardPosition.F4;
import static com.zuooh.chess.ChessBoardPosition.F5;
import static com.zuooh.chess.ChessBoardPosition.F6;
import static com.zuooh.chess.ChessBoardPosition.F7;
import static com.zuooh.chess.ChessBoardPosition.F8;
import static com.zuooh.chess.ChessBoardPosition.G1;
import static com.zuooh.chess.ChessBoardPosition.G2;
import static com.zuooh.chess.ChessBoardPosition.G3;
import static com.zuooh.chess.ChessBoardPosition.G4;
import static com.zuooh.chess.ChessBoardPosition.G5;
import static com.zuooh.chess.ChessBoardPosition.G6;
import static com.zuooh.chess.ChessBoardPosition.G7;
import static com.zuooh.chess.ChessBoardPosition.G8;
import static com.zuooh.chess.ChessBoardPosition.H1;
import static com.zuooh.chess.ChessBoardPosition.H2;
import static com.zuooh.chess.ChessBoardPosition.H3;
import static com.zuooh.chess.ChessBoardPosition.H4;
import static com.zuooh.chess.ChessBoardPosition.H5;
import static com.zuooh.chess.ChessBoardPosition.H6;
import static com.zuooh.chess.ChessBoardPosition.H7;
import static com.zuooh.chess.ChessBoardPosition.H8;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardCellList;

public class ChessReadOnlyCell implements ChessBoardCell {
   
   public static final ChessBoardCellList EMPTY = new ChessBoardCellList(
      new ChessReadOnlyCell(null, A1),
      new ChessReadOnlyCell(null, A2),
      new ChessReadOnlyCell(null, A3),
      new ChessReadOnlyCell(null, A4),
      new ChessReadOnlyCell(null, A5),
      new ChessReadOnlyCell(null, A6),
      new ChessReadOnlyCell(null, A7),
      new ChessReadOnlyCell(null, A8),
      new ChessReadOnlyCell(null, B1),
      new ChessReadOnlyCell(null, B2),
      new ChessReadOnlyCell(null, B3),
      new ChessReadOnlyCell(null, B4),
      new ChessReadOnlyCell(null, B5),
      new ChessReadOnlyCell(null, B6),
      new ChessReadOnlyCell(null, B7),
      new ChessReadOnlyCell(null, B8),
      new ChessReadOnlyCell(null, C1),
      new ChessReadOnlyCell(null, C2),
      new ChessReadOnlyCell(null, C3),
      new ChessReadOnlyCell(null, C4),
      new ChessReadOnlyCell(null, C5),
      new ChessReadOnlyCell(null, C6),
      new ChessReadOnlyCell(null, C7),
      new ChessReadOnlyCell(null, C8),
      new ChessReadOnlyCell(null, D1),
      new ChessReadOnlyCell(null, D2),
      new ChessReadOnlyCell(null, D3),
      new ChessReadOnlyCell(null, D4),
      new ChessReadOnlyCell(null, D5),
      new ChessReadOnlyCell(null, D6),
      new ChessReadOnlyCell(null, D7),
      new ChessReadOnlyCell(null, D8),
      new ChessReadOnlyCell(null, E1),
      new ChessReadOnlyCell(null, E2),
      new ChessReadOnlyCell(null, E3),
      new ChessReadOnlyCell(null, E4),
      new ChessReadOnlyCell(null, E5),
      new ChessReadOnlyCell(null, E6),
      new ChessReadOnlyCell(null, E7),
      new ChessReadOnlyCell(null, E8),
      new ChessReadOnlyCell(null, F1),
      new ChessReadOnlyCell(null, F2),
      new ChessReadOnlyCell(null, F3),
      new ChessReadOnlyCell(null, F4),
      new ChessReadOnlyCell(null, F5),
      new ChessReadOnlyCell(null, F6),
      new ChessReadOnlyCell(null, F7),
      new ChessReadOnlyCell(null, F8),
      new ChessReadOnlyCell(null, G1),
      new ChessReadOnlyCell(null, G2),
      new ChessReadOnlyCell(null, G3),
      new ChessReadOnlyCell(null, G4),
      new ChessReadOnlyCell(null, G5),
      new ChessReadOnlyCell(null, G6),
      new ChessReadOnlyCell(null, G7),
      new ChessReadOnlyCell(null, G8),
      new ChessReadOnlyCell(null, H1),
      new ChessReadOnlyCell(null, H2),
      new ChessReadOnlyCell(null, H3),
      new ChessReadOnlyCell(null, H4),
      new ChessReadOnlyCell(null, H5),
      new ChessReadOnlyCell(null, H6),
      new ChessReadOnlyCell(null, H7),
      new ChessReadOnlyCell(null, H8)
   );   

   private final ChessBoardPosition position;
   private final ChessPiece piece;

   public ChessReadOnlyCell(ChessPiece piece, ChessBoardPosition position) {
      this.position = position;
      this.piece = piece;
   }

   @Override
   public boolean isCellEmpty() {
      return piece == null;
   }

   @Override
   public ChessPiece getPiece() {
      return piece;
   }

   @Override
   public ChessBoardPosition getPosition() {
      return position;
   }
   
   @Override
   public String toString() {
      return piece + " " + position;
   }
}