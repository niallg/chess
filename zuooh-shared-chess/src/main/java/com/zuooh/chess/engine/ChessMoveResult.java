package com.zuooh.chess.engine;

import static com.zuooh.chess.engine.ChessOpponentStatus.NONE;

import java.io.Serializable;

public class ChessMoveResult implements Serializable {

   private static final long serialVersionUID = 1L;
   
   public final ChessOpponentStatus opponentStatus;
   public final ChessMove opponentMove;

   public ChessMoveResult() {
      this(NONE);
   }

   public ChessMoveResult(ChessOpponentStatus opponentStatus) {
      this(opponentStatus, null);
   }

   public ChessMoveResult(ChessOpponentStatus opponentStatus, ChessMove opponentMove) {
      this.opponentStatus = opponentStatus;
      this.opponentMove = opponentMove;
   }

   public ChessOpponentStatus getStatus() {
      return opponentStatus;
   }

   public ChessMove getMove() {
      return opponentMove;
   }
}
