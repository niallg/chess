package com.zuooh.chess.engine;

import java.io.Serializable;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;

public class ChessMove implements Serializable {
   
   private static final long serialVersionUID = 1L;

   public final ChessBoardPosition from;
   public final ChessBoardPosition to;
   public final ChessSide side;
   public final int change;

   public ChessMove(ChessBoardPosition from, ChessBoardPosition to, ChessSide side, int change) {
      this.side = side;
      this.change = change;
      this.from = from;
      this.to = to;
   }     
   
   public ChessBoardPosition getTo() {
      return to;
   }

   public ChessBoardPosition getFrom() {
      return from;
   }   

   public ChessSide getSide() {
      return side;
   }
   
   public int getChange() {
      return change;
   }

   public String toString() {
      return String.format("%s -> %s (%s)", from, to, side);
   }

}
