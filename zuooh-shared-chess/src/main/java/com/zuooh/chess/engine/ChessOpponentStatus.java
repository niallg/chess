package com.zuooh.chess.engine;

public enum ChessOpponentStatus {
   ONLINE,
   OFFLINE,
   RESIGN,
   DRAW,
   WIN,
   LOSE,
   THINKING,
   ERROR,
   NONE;

   public boolean isError() {
      return this == ERROR;
   }

   public boolean isWin() {
      return this == WIN;
   }
   
   public boolean isLose() {
      return this == LOSE;
   }

   public boolean isThinking() {
      return this == THINKING;
   }

   public boolean isOnline() {
      return this == ONLINE;
   }

   public boolean isOffline() {
      return this == OFFLINE;
   }

   public boolean isResign() {
      return this == RESIGN;
   }
   
   public boolean isDraw() {
      return this == DRAW;
   }   
   
   public boolean isGameOver() {
      return this == DRAW || this == LOSE || this == WIN || this == RESIGN;
   }

   public static ChessOpponentStatus resolveStatus(String token) {
      if(token != null) {
         for(ChessOpponentStatus status : values()) {
            if(status.name().equalsIgnoreCase(token)) {
               return status;
            }
         }
      }
      return null;
   }
}

