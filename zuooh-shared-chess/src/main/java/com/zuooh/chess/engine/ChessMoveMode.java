package com.zuooh.chess.engine;

public enum ChessMoveMode {
   BASIC,
   VALIDATION,
   SEARCH;
   
   public boolean isValidation() {
      return this == VALIDATION;
   }
}
