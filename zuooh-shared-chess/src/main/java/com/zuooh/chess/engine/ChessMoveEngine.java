package com.zuooh.chess.engine;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;

public interface ChessMoveEngine {
   String getOpponent();
   ChessSide getSide();
   ChessMove getLastMove();
   ChessOpponentStatus getEngineStatus(); 
   void makeMove(ChessBoard chessBoard, ChessMove myLastMove);
   void drawGame();
   void resignGame();
   void stopSearch();
}
