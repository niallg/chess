package com.zuooh.chess.engine;

import com.zuooh.chess.ChessSide;

public interface ChessMoveEngineProvider {
   ChessMoveEngine createEngine(ChessSide engineSide, int skillLevel);
}
