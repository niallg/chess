package com.zuooh.chess;

import java.util.Collections;
import java.util.List;

import com.zuooh.chess.board.ChessBoard;

public enum ChessPieceType {
   PAWN(0, 0x01) {
      @Override
      protected List<ChessBoardPosition> move(ChessBoard board, ChessPiece piece, ChessMoveType moveType) {
         return moveType.possiblePositions(board, piece);
      }
   },
   KNIGHT(1, 0x02) {
      @Override
      protected List<ChessBoardPosition> move(ChessBoard board, ChessPiece piece, ChessMoveType moveType) {
         return moveType.possiblePositions(board, piece);
      }
   },
   BISHOP(2, 0x04) {
      @Override
      protected List<ChessBoardPosition> move(ChessBoard board, ChessPiece piece, ChessMoveType moveType) {
         return moveType.possiblePositions(board, piece);
      }
   },   
   ROOK(3, 0x08) {
      @Override
      protected List<ChessBoardPosition> move(ChessBoard board, ChessPiece piece, ChessMoveType moveType) {
         return moveType.possiblePositions(board, piece);
      }
   },
   QUEEN(4, 0x10) {
      @Override
      protected List<ChessBoardPosition> move(ChessBoard board, ChessPiece piece, ChessMoveType moveType) {
         return moveType.possiblePositions(board, piece);
      }
   },
   KING(5, 0x020) {
      @Override
      protected List<ChessBoardPosition> move(ChessBoard board, ChessPiece piece, ChessMoveType moveType) {
         return moveType.possiblePositions(board, piece);
      }
   },
   SHADOW(-1, 0x40) {
      @Override
      protected List<ChessBoardPosition> move(ChessBoard board, ChessPiece piece, ChessMoveType moveType) {
         return Collections.emptyList();
      }
   };   
   
   public final int index;
   public final int mask;
   
   private ChessPieceType(int index, int mask) {
      this.index = index;
      this.mask = mask;
   }

   public final List<ChessBoardPosition> moveCapture(ChessBoard board, ChessPiece piece) {
      return move(board, piece, ChessMoveType.CAPTURE);
   }

   public final List<ChessBoardPosition> moveNormal(ChessBoard board, ChessPiece piece) {
      return move(board, piece, ChessMoveType.NORMAL);
   }

   protected abstract List<ChessBoardPosition> move(ChessBoard board, ChessPiece piece, ChessMoveType moveType);
   
   public static ChessPieceType resolveType(String token) {
      ChessPieceType[] types = ChessPieceType.values();
      
      for(ChessPieceType type : types) {
         String name = type.name();
         
         if(name.equalsIgnoreCase(token)) {
            return type;
         }
      }
      return null;
   }
}
