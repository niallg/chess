package com.zuooh.chess;

public enum ChessCellColor {
   DARK,
   LIGHT;
}
