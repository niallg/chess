package com.zuooh.chess;

import com.zuooh.chess.ChessBoardPosition;

public enum ChessMoveDirection {
   NORTH {
      @Override
      public ChessBoardPosition adjacent(ChessBoardPosition position) {
         return ChessBoardPosition.at(position.x, position.y - 1);
      }
   },
   SOUTH {
      @Override
      public ChessBoardPosition adjacent(ChessBoardPosition position) {
         return ChessBoardPosition.at(position.x, position.y + 1);
      }
   },
   EAST {
      @Override
      public ChessBoardPosition adjacent(ChessBoardPosition position) {
         return ChessBoardPosition.at(position.x + 1, position.y);
      }
   },
   WEST {
      @Override
      public ChessBoardPosition adjacent(ChessBoardPosition position) {
         return ChessBoardPosition.at(position.x - 1, position.y);
      }
   },
   NORTH_EAST {
      @Override
      public ChessBoardPosition adjacent(ChessBoardPosition position) {
         return ChessBoardPosition.at(position.x + 1, position.y - 1);
      }
   },
   NORTH_WEST {
      @Override
      public ChessBoardPosition adjacent(ChessBoardPosition position) {
         return ChessBoardPosition.at(position.x - 1, position.y - 1);
      }
   },
   SOUTH_EAST {
      @Override
      public ChessBoardPosition adjacent(ChessBoardPosition position) {
         return ChessBoardPosition.at(position.x + 1, position.y + 1);
      }
   },
   SOUTH_WEST {
      @Override
      public ChessBoardPosition adjacent(ChessBoardPosition position) {
         return ChessBoardPosition.at(position.x - 1, position.y + 1);
      }
   };
   
   public abstract ChessBoardPosition adjacent(ChessBoardPosition position);
}
