package com.zuooh.chess.analyze;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;

public final class ChessCaptureAnalyzer {
   
   public static long captures(ChessBoard board, ChessPiece piece) {
      long moves = ChessMoveAnalyzer.move(board, piece);
      
      if(moves != 0) {
         ChessSide side = piece.side;
         ChessSide opposite = side.oppositeSide();
         long occupation = board.getBoardOccupation(opposite);
         
         return moves & occupation;
      }
      return 0;
   }

   public static long captures(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      long moves = ChessMoveAnalyzer.move(board, position, side);
      
      if(moves != 0) {
         ChessSide opposite = side.oppositeSide();
         long occupation = board.getBoardOccupation(opposite);
         
         return moves & occupation;
      }
      return 0;
   }
}
