package com.zuooh.chess.analyze;

import static com.zuooh.chess.ChessPieceType.KING;
import static com.zuooh.chess.ChessPieceType.KNIGHT;
import static com.zuooh.chess.ChessPieceType.PAWN;
import static com.zuooh.chess.ChessSide.BLACK;
import static com.zuooh.chess.ChessSide.WHITE;
import static com.zuooh.chess.board.binary.ChessBitBoard.ALL_ADJACENT_TO;
import static com.zuooh.chess.board.binary.ChessBitBoard.ALL_LINES_FROM;
import static com.zuooh.chess.board.binary.ChessBitBoard.ALL_TYPES;
import static com.zuooh.chess.board.binary.ChessBitBoard.KING_ATTACKS_FROM;
import static com.zuooh.chess.board.binary.ChessBitBoard.KING_MOVE_OFFSETS;
import static com.zuooh.chess.board.binary.ChessBitBoard.KNIGHT_ATTACKS_FROM;
import static com.zuooh.chess.board.binary.ChessBitBoard.KNIGHT_JUMP_OFFSETS;
import static com.zuooh.chess.board.binary.ChessBitBoard.PAWN_BLACK_ATTACKS_FROM;
import static com.zuooh.chess.board.binary.ChessBitBoard.PAWN_BLACK_PASSENT_ATTACKS_FROM;
import static com.zuooh.chess.board.binary.ChessBitBoard.PAWN_WHITE_ATTACKS_FROM;
import static com.zuooh.chess.board.binary.ChessBitBoard.PAWN_WHITE_PASSENT_ATTACKS_FROM;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;

public final class ChessAttackAnalyzer {
   
   public static boolean safe(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      long[] occupation = new long[2];      
      
      occupation[WHITE.index] = board.getBoardOccupation(WHITE);
      occupation[BLACK.index] = board.getBoardOccupation(BLACK);
      
      if(underAttackJump(board, position, side, occupation) != 0) {
         return false;
      }
      if(underAttackAdjacent(board, position, side, occupation) != 0) {
         return false;
      }
      if(underAttackStep(board, position, side, occupation) != 0) {
         return false;
      }
      if(underAttackEnPassent(board, position, side, occupation) != 0) {
         return false;
      }
      
      for(int i = 0; i < ALL_LINES_FROM.length; i++) {
         long[][] line = ALL_LINES_FROM[i];
         int[][] moves = ALL_ADJACENT_TO[i];
         int types = ALL_TYPES[i];
         
         if(underAttackLine(board, position, side, line, moves, occupation, types) != 0) {
            return false;
         }
      }
      return true;
   }
   
   public static boolean safeIf(ChessBoard board, ChessBoardPosition from, ChessBoardPosition to, ChessSide side) {
      ChessMove move = side.createMove(from, to);
      ChessBoardMove boardMove = board.getBoardMove(move);
      
      if(boardMove == null) {
         throw new IllegalStateException("Move " + move + " could not be created on " + board);
      }
      boardMove.makeMove(false);
      
      try {
         return safe(board, to, side);
      } finally {
         boardMove.revertMove();
      }      
   }    
   
   public static long underAttack(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      long[] occupation = new long[2];
      long positions = 0L;
      
      occupation[WHITE.index] = board.getBoardOccupation(WHITE);
      occupation[BLACK.index] = board.getBoardOccupation(BLACK);
      
      for(int i = 0; i < ALL_LINES_FROM.length; i++) {
         long[][] line = ALL_LINES_FROM[i];
         int[][] moves = ALL_ADJACENT_TO[i];
         int types = ALL_TYPES[i];
         
         positions |= underAttackLine(board, position, side, line, moves, occupation, types);
      }
      positions |= underAttackJump(board, position, side, occupation);
      positions |= underAttackAdjacent(board, position, side, occupation);
      positions |= underAttackStep(board, position, side, occupation);
      positions |= underAttackEnPassent(board, position, side, occupation);        
      
      return positions;
   }
   
   public static long underAttackIf(ChessBoard board, ChessBoardPosition from, ChessBoardPosition to, ChessSide side) {
      ChessMove move = side.createMove(from, to);
      ChessBoardMove boardMove = board.getBoardMove(move);
      
      if(boardMove == null) {
         throw new IllegalStateException("Move " + move + " could not be created on " + board);
      }
      boardMove.makeMove(false);
      
      try {
         return underAttack(board, to, side);
      } finally {
         boardMove.revertMove();
      }          
   }    
   
   private static long underAttackJump(ChessBoard board, ChessBoardPosition position, ChessSide side, long[] occupation) {
      ChessSide opposite = side.oppositeSide();
      long attacks = KNIGHT_ATTACKS_FROM[position.x][position.y]; // knights
      long opponentPositions = attacks & occupation[opposite.index];
      long attackPositions = 0L;
         
      if(opponentPositions != 0L) {
         for(int i = 0; i < KNIGHT_JUMP_OFFSETS.length; i++) {
            int x = position.x + KNIGHT_JUMP_OFFSETS[i][0];
            int y = position.y + KNIGHT_JUMP_OFFSETS[i][1];
            ChessBoardPosition jump = ChessBoardPosition.at(x, y);
            
            if(jump != null) {
               if((opponentPositions & jump.mask) == jump.mask) {
                  ChessPiece piece = board.getPiece(jump);
                  
                  if(piece == null) {
                     throw new IllegalStateException("Piece at " + jump + " is null which does not match occupation mask");
                  }
                  ChessPieceKey pieceKey = piece.key;
                  
                  if(pieceKey.type == KNIGHT) { // opponent knight attack
                     attackPositions |= jump.mask;
                  }
               }
            }
         }   
      }
      return attackPositions;
   }     
   
   private static long underAttackEnPassent(ChessBoard board, ChessBoardPosition position, ChessSide side, long[] occupation) {
      ChessSide opposite = side.oppositeSide();
      long attackPositions = 0L;      
      long attacks = 0L;
      
      if(side == BLACK) {
         attacks = PAWN_WHITE_PASSENT_ATTACKS_FROM[position.x][position.y];
      } else {
         attacks = PAWN_BLACK_PASSENT_ATTACKS_FROM[position.x][position.y];
      }
      long opponentPositions = attacks & occupation[opposite.index];            
      
      if(opponentPositions != 0L) {
         ChessPiece currentPiece = board.getPiece(position);            
         ChessPieceKey currentKey = currentPiece.key;
         
         if(currentKey.type == PAWN) {
            int moves = board.getMoveCount(currentPiece);
            
            if(moves == 1) {
               ChessBoardPosition previous = board.getLastPosition(side);
            
               if(previous == position) {
                  ChessBoardPosition stepWest = ChessBoardPosition.at(position.x - 1, position.y);
                  ChessBoardPosition stepEast = ChessBoardPosition.at(position.x + 1, position.y);
                  ChessBoardPosition[] steps = {stepWest, stepEast};
                  
                  for(ChessBoardPosition step : steps) {
                     if(step != null) {
                        if((opponentPositions & step.mask) == step.mask) {
                           ChessPiece opponent = board.getPiece(step);
                           
                           if(opponent == null) {
                              throw new IllegalStateException("Piece at " + step + " is null which does not match occupation mask");
                           }
                           ChessPieceKey pieceKey = opponent.key;
                           
                           if(pieceKey.type == PAWN) { // opponent pawn attack
                              attackPositions |= step.mask;
                           }
                        }
                     }
                  }  
               }
            }
         } 
      }               
      return attackPositions;
   } 
   
   private static long underAttackStep(ChessBoard board, ChessBoardPosition position, ChessSide side, long[] occupation) {
      ChessSide opposite = side.oppositeSide();
      long attackPositions = 0L;      
      
      if(side == BLACK) {
         long attacks = PAWN_WHITE_ATTACKS_FROM[position.x][position.y];
         long opponentPositions = attacks & occupation[opposite.index];            
         
         if(opponentPositions != 0L) {
            ChessBoardPosition stepWest = ChessBoardPosition.at(position.x - 1, position.y + 1);
            ChessBoardPosition stepEast = ChessBoardPosition.at(position.x + 1, position.y + 1);
            ChessBoardPosition[] steps = {stepWest, stepEast};
            
            for(ChessBoardPosition step : steps) {
               if(step != null) {
                  if((opponentPositions & step.mask) == step.mask) {
                     ChessPiece opponent = board.getPiece(step);
                     
                     if(opponent == null) {
                        throw new IllegalStateException("Piece at " + step + " is null which does not match occupation mask");
                     }
                     ChessPieceKey pieceKey = opponent.key;
                     
                     if(pieceKey.type == PAWN) { // opponent pawn attack
                        attackPositions |= step.mask;
                     }
                  }
               }
            } 
         }
      } else {        
         long attacks = PAWN_BLACK_ATTACKS_FROM[position.x][position.y];
         long opponentPositions = attacks & occupation[opposite.index];            
         
         if(opponentPositions != 0L) {
            ChessBoardPosition stepWest = ChessBoardPosition.at(position.x - 1, position.y - 1);
            ChessBoardPosition stepEast = ChessBoardPosition.at(position.x + 1, position.y - 1);
            ChessBoardPosition[] steps = {stepWest, stepEast};
            
            for(ChessBoardPosition step : steps) {
               if(step != null) {
                  if((opponentPositions & step.mask) == step.mask) {
                     ChessPiece opponent = board.getPiece(step);
                     
                     if(opponent == null) {
                        throw new IllegalStateException("Piece at " + step + " is null which does not match occupation mask");
                     }
                     ChessPieceKey pieceKey = opponent.key;
                     
                     if(pieceKey.type == PAWN) { // opponent pawn attack
                        attackPositions |= step.mask;
                     }
                  }
               }
            } 
         }
      }          
      return attackPositions;
   }     
   
   private static long underAttackAdjacent(ChessBoard board, ChessBoardPosition position, ChessSide side, long[] occupation) {
      ChessSide opposite = side.oppositeSide();
      long attacks = KING_ATTACKS_FROM[position.x][position.y]; // kings
      long opponentPositions = attacks & occupation[opposite.index];
         
      if(opponentPositions != 0L) {
         for(int i = 0; i < KING_MOVE_OFFSETS.length; i++) {
            int x = position.x + KING_MOVE_OFFSETS[i][0];
            int y = position.y + KING_MOVE_OFFSETS[i][1];
            ChessBoardPosition adjacent = ChessBoardPosition.at(x, y);
            
            if(adjacent != null) {
               if((opponentPositions & adjacent.mask) == adjacent.mask) {
                  ChessPiece opponent = board.getPiece(adjacent);
                  
                  if(opponent == null) {
                     throw new IllegalStateException("Piece at " + adjacent + " is null which does not match occupation mask");
                  }
                  ChessPieceKey pieceKey = opponent.key;
                  
                  if(pieceKey.type == KING) { // opponent king attack
                     return adjacent.mask;
                  }
               }
            }
         }   
      }
      return 0L;
   }     
   
   private static long underAttackLine(ChessBoard board, ChessBoardPosition position, ChessSide side, long[][] line, int[][] moves, long[] occupation, int types) {
      ChessSide opposite = side.oppositeSide();
      long attackLine = line[position.x][position.y];
      long playerOccupation = occupation[side.index];
      long opponentOccupation = occupation[opposite.index];      
      long opponentPieces = attackLine & occupation[opposite.index];
      
      if(opponentPieces != 0) {
         int adjacent = moves[position.x][position.y];
         
         while(adjacent != -1) {
            ChessBoardPosition next = ChessBoardPosition.at(adjacent);
            
            if((opponentOccupation & next.mask) != 0) {
               ChessPiece piece = board.getPiece(next);
               
               if(piece == null) {
                  throw new IllegalStateException("Piece at " + next + " is null which does not match occupation mask");
               }
               ChessPieceKey pieceKey = piece.key;
               
               if((pieceKey.type.mask & types) != 0) { // clear line to viable piece
                  return next.mask;
               }
               return 0L;
            } else {
               if((playerOccupation & next.mask) != 0) { // hit own piece
                  return 0L;
               }
            }
            adjacent = moves[next.x][next.y]; // advance along line
         }         
         throw new IllegalStateException("End of board reached from " + position + " without a piece");         
      }
      return 0L;
   }
}
