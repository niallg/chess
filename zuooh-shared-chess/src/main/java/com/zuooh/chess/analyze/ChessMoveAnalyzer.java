package com.zuooh.chess.analyze;

import static com.zuooh.chess.ChessBoardPosition.A1;
import static com.zuooh.chess.ChessBoardPosition.A8;
import static com.zuooh.chess.ChessBoardPosition.B1;
import static com.zuooh.chess.ChessBoardPosition.B8;
import static com.zuooh.chess.ChessBoardPosition.C1;
import static com.zuooh.chess.ChessBoardPosition.C8;
import static com.zuooh.chess.ChessBoardPosition.D1;
import static com.zuooh.chess.ChessBoardPosition.D8;
import static com.zuooh.chess.ChessBoardPosition.E1;
import static com.zuooh.chess.ChessBoardPosition.E8;
import static com.zuooh.chess.ChessBoardPosition.F1;
import static com.zuooh.chess.ChessBoardPosition.F8;
import static com.zuooh.chess.ChessBoardPosition.G1;
import static com.zuooh.chess.ChessBoardPosition.G8;
import static com.zuooh.chess.ChessBoardPosition.H1;
import static com.zuooh.chess.ChessBoardPosition.H8;
import static com.zuooh.chess.ChessMoveDirection.EAST;
import static com.zuooh.chess.ChessMoveDirection.NORTH_EAST;
import static com.zuooh.chess.ChessMoveDirection.NORTH_WEST;
import static com.zuooh.chess.ChessMoveDirection.SOUTH_EAST;
import static com.zuooh.chess.ChessMoveDirection.SOUTH_WEST;
import static com.zuooh.chess.ChessMoveDirection.WEST;
import static com.zuooh.chess.ChessPieceType.BISHOP;
import static com.zuooh.chess.ChessPieceType.KING;
import static com.zuooh.chess.ChessPieceType.KNIGHT;
import static com.zuooh.chess.ChessPieceType.PAWN;
import static com.zuooh.chess.ChessPieceType.QUEEN;
import static com.zuooh.chess.ChessPieceType.ROOK;
import static com.zuooh.chess.ChessSide.BLACK;
import static com.zuooh.chess.ChessSide.WHITE;
import static com.zuooh.chess.board.binary.ChessBitBoard.ALL_ADJACENT_TO;
import static com.zuooh.chess.board.binary.ChessBitBoard.DIAGONAL_ADJACENT_TO;
import static com.zuooh.chess.board.binary.ChessBitBoard.KING_MOVES_TO;
import static com.zuooh.chess.board.binary.ChessBitBoard.KING_MOVE_OFFSETS;
import static com.zuooh.chess.board.binary.ChessBitBoard.KNIGHT_JUMP_OFFSETS;
import static com.zuooh.chess.board.binary.ChessBitBoard.KNIGHT_MOVES_TO;
import static com.zuooh.chess.board.binary.ChessBitBoard.PAWN_BLACK_MOVES_TO;
import static com.zuooh.chess.board.binary.ChessBitBoard.PAWN_WHITE_MOVES_TO;
import static com.zuooh.chess.board.binary.ChessBitBoard.STRAIGHT_ADJACENT_TO;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessMoveDirection;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;

public final class ChessMoveAnalyzer {

   public static long move(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);
      
      if(position == null) {
         throw new IllegalArgumentException("Piece " + piece + " is not on the board");
      }
      ChessPieceKey key = piece.key;
      ChessPieceType type = key.type;
      ChessSide side = piece.side;

      if(type == PAWN) {
         return movePawn(board, position, side);
      }
      if(type == KNIGHT) {
         return moveKnight(board, position, side);
      }
      if(type == BISHOP) {
         return moveBishop(board, position, side);
      }
      if(type == ROOK) {
         return moveRook(board, position, side);
      }
      if(type == QUEEN) {
         return moveQueen(board, position, side);
      }
      if(type == KING) {
         return moveKing(board, position, side);
      }
      throw new IllegalArgumentException("Piece type " + type + " should not be moved");      
   }
   
   public static long move(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      ChessPiece piece = board.getPiece(position);
      
      if(piece == null) {
         throw new IllegalArgumentException("No piece on " + position);
      }
      ChessPieceKey key = piece.key;
      ChessPieceType type = key.type;
      
      if(piece.side != side) {
         throw new IllegalArgumentException("Piece on " + position + " is on side " + piece.side + " not " + side);
      }
      if(type == PAWN) {
         return movePawn(board, position, side);
      }
      if(type == KNIGHT) {
         return moveKnight(board, position, side);
      }
      if(type == BISHOP) {
         return moveBishop(board, position, side);
      }
      if(type == ROOK) {
         return moveRook(board, position, side);
      }
      if(type == QUEEN) {
         return moveQueen(board, position, side);
      }
      if(type == KING) {
         return moveKing(board, position, side);
      }
      throw new IllegalArgumentException("Piece type " + type + " should not be moved");
   }
   
   private static long movePawn(ChessBoard board, ChessBoardPosition position, ChessSide side) { // this could be made much faster with better bit boards
      long[] occupation = new long[2];
      long moves = 0L;
      
      occupation[WHITE.index] = board.getBoardOccupation(WHITE);
      occupation[BLACK.index] = board.getBoardOccupation(BLACK);
      
      if(side == BLACK) {
         moves = PAWN_BLACK_MOVES_TO[position.x][position.y];
      } else {
         moves = PAWN_WHITE_MOVES_TO[position.x][position.y];
      }
      ChessSide opposide = side.oppositeSide();
      long playerOccupation = occupation[side.index];
      long opponentOccupation = occupation[opposide.index];
      long boardOccupation = playerOccupation | opponentOccupation;
      long positions = moves & ~playerOccupation; // remove your own pieces
      long possible = 0L;
      
      if(positions != 0L) {
         if(side == BLACK) {
            ChessBoardPosition stepSouthWest = ChessBoardPosition.at(position.x - 1, position.y + 1);
            ChessBoardPosition stepSouthEast = ChessBoardPosition.at(position.x + 1, position.y + 1);
            ChessBoardPosition stepSouth = ChessBoardPosition.at(position.x, position.y + 1);
            ChessBoardPosition[] steps = {stepSouthWest, stepSouthEast, stepSouth};
            long[] stepBoards = {~opponentOccupation, ~opponentOccupation, boardOccupation};            
            
            for(int i = 0; i < steps.length; i++) {
               ChessBoardPosition step = steps[i];
            
               if(step != null) {
                  long stepBoard = stepBoards[i];
                  
                  if((positions & step.mask) == step.mask && (stepBoard & step.mask) == 0L) {
                     possible |= step.mask;
                  }
               }
            }    
            if(position.digit == 7) {
               ChessBoardPosition stepDoubleSouth = ChessBoardPosition.at(position.x, position.y + 2);              
               
               if((possible & stepSouth.mask) == stepSouth.mask && (boardOccupation & stepDoubleSouth.mask) == 0L) {
                  possible |= stepDoubleSouth.mask;
               }
            }
         } else {
            ChessBoardPosition stepNorthWest = ChessBoardPosition.at(position.x - 1, position.y - 1);
            ChessBoardPosition stepNorthEast = ChessBoardPosition.at(position.x + 1, position.y - 1);
            ChessBoardPosition stepNorth = ChessBoardPosition.at(position.x, position.y - 1);
            ChessBoardPosition[] steps = {stepNorthWest, stepNorthEast, stepNorth};
            long[] stepBoards = {~opponentOccupation, ~opponentOccupation, boardOccupation};
            
            for(int i = 0; i < steps.length; i++) {
               ChessBoardPosition step = steps[i];
            
               if(step != null) {
                  long stepBoard = stepBoards[i];
                  
                  if((positions & step.mask) == step.mask && (stepBoard & step.mask) == 0L) {
                     possible |= step.mask;
                  }
               }
            }  
            if(position.digit == 2) {
               ChessBoardPosition stepDoubleNorth = ChessBoardPosition.at(position.x, position.y - 2);              
               
               if((possible & stepNorth.mask) == stepNorth.mask && (boardOccupation & stepDoubleNorth.mask) == 0L) {
                  possible |= stepDoubleNorth.mask;
               }
            }
         }
      }
      if(side == WHITE) {
         if(position.digit == 5) {
            ChessBoardPosition west = ChessBoardPosition.at(position.x  -1, position.y);
            ChessBoardPosition east = ChessBoardPosition.at(position.x  +1, position.y);
            
            if(west != null && (opponentOccupation & west.mask) == west.mask) {
               possible |= movePawnPassent(board, position, west, NORTH_WEST, playerOccupation);
            }
            if(east != null && (opponentOccupation & east.mask) == east.mask) {
               possible |= movePawnPassent(board, position, east, NORTH_EAST, playerOccupation);               
            }            
         }
      } else {
         if(position.digit == 4) {
            ChessBoardPosition west = ChessBoardPosition.at(position.x  -1, position.y);
            ChessBoardPosition east = ChessBoardPosition.at(position.x  +1, position.y);
            
            if(west != null && (opponentOccupation & west.mask) == west.mask) {
               possible |= movePawnPassent(board, position, west, SOUTH_WEST, playerOccupation);
            }
            if(east != null && (opponentOccupation & east.mask) == east.mask) {
               possible |= movePawnPassent(board, position, east, SOUTH_EAST, playerOccupation);               
            }            
         }
      }
      return possible; 
   }

   private static long movePawnPassent(ChessBoard board, ChessBoardPosition position, ChessBoardPosition attack, ChessMoveDirection direction, long occupation) {
      ChessPiece pawnPiece = board.getPiece(attack);
      int pawnMoves = board.getMoveCount(pawnPiece);
      
      if(pawnMoves == 1) {
         ChessBoardPosition lastMove = board.getLastPosition(pawnPiece.side);
         
         if(lastMove == attack) {
            ChessBoardPosition destination = direction.adjacent(position);
            
            if(destination != null && (occupation & destination.mask) == 0L) {
               return destination.mask;
            }
         }
      }
      return 0L;
   }
   
   private static long moveKnight(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      long[] occupation = new long[2];
      
      occupation[WHITE.index] = board.getBoardOccupation(WHITE);
      occupation[BLACK.index] = board.getBoardOccupation(BLACK);
      
      long moves = KNIGHT_MOVES_TO[position.x][position.y]; // knights
      long playerOccupation = occupation[side.index];
      long positions = moves & ~playerOccupation; // remove your own pieces
      long possible = 0L;
      
      if(positions != 0L) {
         for(int i = 0; i < KNIGHT_JUMP_OFFSETS.length; i++) {
            int x = position.x + KNIGHT_JUMP_OFFSETS[i][0];
            int y = position.y + KNIGHT_JUMP_OFFSETS[i][1];
            ChessBoardPosition jump = ChessBoardPosition.at(x, y);
            
            if(jump != null) {
               if((positions & jump.mask) == jump.mask) {
                  possible |= jump.mask;                  
               }
            }
         }   
      }
      return possible;
   }   
   
   private static long moveKing(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      long[] occupation = new long[2];
      
      occupation[WHITE.index] = board.getBoardOccupation(WHITE);
      occupation[BLACK.index] = board.getBoardOccupation(BLACK);
      
      long moves = KING_MOVES_TO[position.x][position.y]; // knights
      long playerOccupation = occupation[side.index];
      long boardOccupation = occupation[WHITE.index] | occupation[BLACK.index];      
      long positions = moves & ~playerOccupation; // remove your own pieces
      long possible = 0L;
      
      if(positions != 0L) {
         for(int i = 0; i < KING_MOVE_OFFSETS.length; i++) {
            int x = position.x + KING_MOVE_OFFSETS[i][0];
            int y = position.y + KING_MOVE_OFFSETS[i][1];
            ChessBoardPosition adjacent = ChessBoardPosition.at(x, y);
            
            if(adjacent != null) {
               if((positions & adjacent.mask) == adjacent.mask) {
                  if(ChessAttackAnalyzer.safeIf(board, position, adjacent, side)) {
                     possible |= adjacent.mask;
                  }
               }
            }
         }   
      }
      if(side == WHITE) {
         if(position == E1) {
            if((playerOccupation & A1.mask) == A1.mask && (boardOccupation & (D1.mask|C1.mask|B1.mask)) == 0L) { // make sure we occupy A1 unobstructed
               possible |= moveKingCastle(board, C1, position, A1, EAST, occupation);
            }
            if((playerOccupation & H1.mask) == H1.mask && (boardOccupation & (F1.mask|G1.mask)) == 0L) {
               possible |= moveKingCastle(board, G1, position, H1, WEST, occupation);
            }            
         }
      } else {
         if(position == E8) {
            if((playerOccupation & A8.mask) == A8.mask && (boardOccupation & (D8.mask|C8.mask|B8.mask)) == 0L) {
               possible |= moveKingCastle(board, C8, position, A8, EAST, occupation);
            }
            if((playerOccupation & H8.mask) == H8.mask && (boardOccupation & (F8.mask|G8.mask)) == 0L) {
               possible |= moveKingCastle(board, G8, position, H8, WEST, occupation);
            } 
         }
      }
      return possible; // once here we check for castle
   } 
   
   private static long moveKingCastle(ChessBoard board, ChessBoardPosition position, ChessBoardPosition king, ChessBoardPosition castle, ChessMoveDirection direction, long[] occupation) {
      ChessPiece kingPiece = board.getPiece(king);
      int kingMoves = board.getMoveCount(kingPiece);
      
      if(kingMoves == 0) {
         ChessPiece castlePiece = board.getPiece(castle);
         
         if(castlePiece == null) {
            throw new IllegalStateException("Piece at " + castle + " is null but should be occupied");
         }
         if(castlePiece.key.type == ROOK) {
            int castleMoves = board.getMoveCount(castlePiece);
         
            if(castleMoves == 0) {
               ChessBoardPosition next = direction.adjacent(castle);               
               ChessBoardPosition current = castle;
               
               while(next != king) {
                  if(!ChessAttackAnalyzer.safeIf(board, current, next, castlePiece.side)) { // ensure there is no threat
                     return 0L;
                  }
                  next = direction.adjacent(next); // validate all positions for attack
               }
               if(next == king) {
                  if(ChessAttackAnalyzer.safe(board, king, kingPiece.side)) {
                     return position.mask;
                  }
               }
            }
         }
      }
      return 0;
   } 

   private static long moveBishop(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      long[] occupation = new long[2];
      long possible = 0L;
      
      occupation[WHITE.index] = board.getBoardOccupation(WHITE);
      occupation[BLACK.index] = board.getBoardOccupation(BLACK);
      
      for(int i = 0; i < DIAGONAL_ADJACENT_TO.length; i++) {
         possible |= moveLine(board, position, side, DIAGONAL_ADJACENT_TO[i], occupation);
      }
      return possible;
   }   
   
   private static long moveQueen(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      long[] occupation = new long[2];
      long possible = 0L;
      
      occupation[WHITE.index] = board.getBoardOccupation(WHITE);
      occupation[BLACK.index] = board.getBoardOccupation(BLACK);
      
      for(int i = 0; i < ALL_ADJACENT_TO.length; i++) {
         possible |= moveLine(board, position, side, ALL_ADJACENT_TO[i], occupation);
      }
      return possible;
   }
   
   private static long moveRook(ChessBoard board, ChessBoardPosition position, ChessSide side) {
      long[] occupation = new long[2];
      long possible = 0L;
      
      occupation[WHITE.index] = board.getBoardOccupation(WHITE);
      occupation[BLACK.index] = board.getBoardOccupation(BLACK);
      
      for(int i = 0; i < STRAIGHT_ADJACENT_TO.length; i++) {
         possible |= moveLine(board, position, side, STRAIGHT_ADJACENT_TO[i], occupation);
      }
      return possible;
   }   
   
   private static long moveLine(ChessBoard board, ChessBoardPosition position, ChessSide side, int[][] moves, long[] occupation) {
      ChessSide opposite = side.oppositeSide();
      long boardOccupation = occupation[opposite.index] | occupation[side.index];
      long opponentOccupation = occupation[opposite.index];      
      int adjacent = moves[position.x][position.y];
      long possible = 0L;
      
      while(adjacent != -1) {
         ChessBoardPosition next = ChessBoardPosition.at(adjacent);
         
         if((boardOccupation & next.mask) == 0) {
            possible |= next.mask;
         } else {
            if((opponentOccupation & next.mask) == next.mask) {
               ChessPiece piece = board.getPiece(next);
               
               if(piece == null) {
                  throw new IllegalStateException("Piece at " + next + " is null which does not match occupation mask");
               }
               possible |= next.mask;
            }
            break;
         }
         adjacent = moves[next.x][next.y]; // advance along line
      }
      return possible;
   }   
}
