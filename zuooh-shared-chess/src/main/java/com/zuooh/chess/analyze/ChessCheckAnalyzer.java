package com.zuooh.chess.analyze;

import static com.zuooh.chess.ChessPieceType.BISHOP;
import static com.zuooh.chess.ChessPieceType.KING;
import static com.zuooh.chess.ChessPieceType.KNIGHT;
import static com.zuooh.chess.ChessSide.BLACK;
import static com.zuooh.chess.ChessSide.WHITE;

import java.util.List;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardCellIterator;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;

public final class ChessCheckAnalyzer {

   public static boolean check(ChessBoard board) {
      if(!check(board,WHITE)) {
         return check(board,BLACK);
      }
      return true;      
   }
   
   public static boolean check(ChessBoard board, ChessSide side) {
      ChessBoardPosition position = board.getKingPosition(side);
      ChessPiece piece = board.getPiece(position);

      if(piece.key.type != KING) {
         throw new IllegalStateException("Piece is not a king");
      }
      return !ChessAttackAnalyzer.safe(board, position, side);
   }
   
   public static boolean checkIf(ChessBoard board, ChessBoardPosition from, ChessBoardPosition to, ChessSide side) {
      ChessMove move = side.createMove(from, to);
      
      if(move != null) {
         return checkIf(board, move);
      }
      return false;      
   }

   public static boolean checkIf(ChessBoard board, ChessMove move) {
      ChessBoardMove boardMove = board.getBoardMove(move);
      
      if(boardMove == null) {
         throw new IllegalStateException("Move " + move + " could not be created on " + board);
      }
      boardMove.makeMove(false);
      
      try {
         return check(board, move.side);
      } finally {
         boardMove.revertMove();
      } 
   }
   
   public static boolean staleMate(ChessBoard board) {
      if(!staleMate(board,WHITE)) {
         return staleMate(board,BLACK);
      }
      return true;      
   }

   public static boolean staleMate(ChessBoard board, ChessSide side) {
      ChessBoardCellIterator cells = board.getBoardCells(side);
      
      while(cells.hasNext()) {
         ChessBoardCell cell = cells.next();      
         ChessPiece piece = cell.getPiece();
         ChessPieceType pieceType = piece.key.type;
         List<ChessBoardPosition> positions = pieceType.moveNormal(board, piece);

         if(!positions.isEmpty()) {
            return false;
         }           
      }
      return true;
   }

   public static boolean checkMate(ChessBoard board) {
      if(!checkMate(board,WHITE)) {
         return checkMate(board,BLACK);
      }
      return true;      
   }
   
   public static boolean checkMate(ChessBoard board, ChessSide side) {
      if(check(board, side)) {
         ChessBoardCellIterator cells = board.getBoardCells(side);
         
         while(cells.hasNext()) {
            ChessBoardCell cell = cells.next(); 
            ChessPiece piece = cell.getPiece();
            ChessBoardPosition current = cell.getPosition();
            ChessPieceType pieceType = piece.key.type;
            List<ChessBoardPosition> positions = pieceType.moveNormal(board, piece);
            
            for(ChessBoardPosition next : positions) {
               ChessMove move = side.createMove(current, next);
               ChessBoardMove boardMove = board.getBoardMove(move);
               
               if(boardMove == null) {
                  throw new IllegalStateException("Move " + move + " could not be created on " + board);
               }
               try {
                  boardMove.makeMove(false);
                  
                  if(!check(board)) {
                     return false;
                  }
               } finally {
                  boardMove.revertMove();
               }
            }              
         }
         return true;
      }
      return false;
   }
   
   public static boolean checkMatePossible(ChessBoard board) {
      ChessSide[] sides = ChessSide.values();
      
      for(ChessSide side : sides) {
         ChessBoardCellIterator cells = board.getBoardCells(side);        
         int count = 0;
         
         while(cells.hasNext()) {
            ChessBoardCell cell = cells.next(); 
            ChessPiece piece = cell.getPiece();
            ChessPieceType pieceType = piece.key.type;
            
            if(pieceType != KING) {
               if(pieceType != KNIGHT && pieceType != BISHOP) {
                  return true;
               } 
               if(count++ > 1) {
                  return true;
               }         
            }
         }
      }
      return false;
   }   
}
