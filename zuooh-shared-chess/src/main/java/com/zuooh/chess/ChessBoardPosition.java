package com.zuooh.chess;

import static com.zuooh.chess.ChessCellColor.DARK;
import static com.zuooh.chess.ChessCellColor.LIGHT;

public enum ChessBoardPosition {
   A1("A1", DARK, 0x0000000000000001L, 'A', 1, 0, 0, 7), // 8-bits[A1-A8] 8-bits[B] 8-bits[C] 8-bits[D]
   A2("A2", LIGHT, 0x0000000000000002L, 'A', 2, 1, 0, 6),
   A3("A3", DARK, 0x0000000000000004L, 'A', 3, 2, 0, 5),
   A4("A4", LIGHT, 0x0000000000000008L, 'A', 4, 3, 0, 4),
   A5("A5", DARK, 0x0000000000000010L, 'A', 5, 4, 0, 3),
   A6("A6", LIGHT, 0x0000000000000020L, 'A', 6, 5, 0, 2),
   A7("A7", DARK, 0x0000000000000040L, 'A', 7, 6, 0, 1),
   A8("A8", LIGHT, 0x0000000000000080L, 'A', 8, 7, 0, 0),
   B1("B1", DARK, 0x0000000000000100L, 'B', 1, 8, 1, 7),
   B2("B2", LIGHT, 0x0000000000000200L, 'B', 2, 9, 1, 6),
   B3("B3", DARK, 0x0000000000000400L, 'B', 3, 10, 1, 5),
   B4("B4", LIGHT, 0x0000000000000800L, 'B', 4, 11, 1, 4),
   B5("B5", DARK, 0x0000000000001000L, 'B', 5, 12, 1, 3),
   B6("B6", LIGHT, 0x0000000000002000L, 'B', 6, 13, 1, 2),
   B7("B7", DARK, 0x0000000000004000L, 'B', 7, 14, 1, 1),
   B8("B8", LIGHT, 0x0000000000008000L, 'B', 8, 15, 1, 0),
   C1("C1", DARK, 0x0000000000010000L, 'C', 1, 16, 2, 7),
   C2("C2", LIGHT, 0x0000000000020000L, 'C', 2, 17, 2, 6),
   C3("C3", DARK, 0x0000000000040000L, 'C', 3, 18, 2, 5),
   C4("C4", LIGHT, 0x0000000000080000L, 'C', 4, 19, 2, 4),
   C5("C5", DARK, 0x0000000000100000L, 'C', 5, 20, 2, 3),
   C6("C6", LIGHT, 0x0000000000200000L, 'C', 6, 21, 2, 2),
   C7("C7", DARK, 0x0000000000400000L, 'C', 7, 22, 2, 1),
   C8("C8", LIGHT, 0x0000000000800000L, 'C', 8, 23, 2, 0),
   D1("D1", DARK, 0x0000000001000000L, 'D', 1, 24, 3, 7),
   D2("D2", LIGHT, 0x0000000002000000L, 'D', 2, 25, 3, 6),
   D3("D3", DARK, 0x0000000004000000L, 'D', 3, 26, 3, 5),
   D4("D4", LIGHT, 0x0000000008000000L, 'D', 4, 27, 3, 4),
   D5("D5", DARK, 0x0000000010000000L, 'D', 5, 28, 3, 3),
   D6("D6", LIGHT, 0x0000000020000000L, 'D', 6, 29, 3, 2),
   D7("D7", DARK, 0x0000000040000000L, 'D', 7, 30, 3, 1),
   D8("D8", LIGHT, 0x0000000080000000L, 'D', 8, 31, 3, 0),
   E1("E1", DARK, 0x0000000100000000L, 'E', 1, 32, 4, 7),
   E2("E2", LIGHT, 0x0000000200000000L, 'E', 2, 33, 4, 6),
   E3("E3", DARK, 0x0000000400000000L, 'E', 3, 34, 4, 5),
   E4("E4", LIGHT, 0x0000000800000000L, 'E', 4, 35, 4, 4),
   E5("E5", DARK, 0x0000001000000000L, 'E', 5, 36, 4, 3),
   E6("E6", LIGHT, 0x0000002000000000L, 'E', 6, 37, 4, 2),
   E7("E7", DARK, 0x0000004000000000L, 'E', 7, 38, 4, 1),
   E8("E8", LIGHT, 0x0000008000000000L, 'E', 8, 39, 4, 0),
   F1("F1", DARK, 0x0000010000000000L, 'F', 1, 40, 5, 7),
   F2("F2", LIGHT, 0x0000020000000000L, 'F', 2, 41, 5, 6),
   F3("F3", DARK, 0x0000040000000000L, 'F', 3, 42, 5, 5),
   F4("F4", LIGHT, 0x0000080000000000L, 'F', 4, 43, 5, 4),
   F5("F5", DARK, 0x0000100000000000L, 'F', 5, 44, 5, 3),
   F6("F6", LIGHT, 0x0000200000000000L, 'F', 6, 45, 5, 2),
   F7("F7", DARK, 0x0000400000000000L, 'F', 7, 46, 5, 1),
   F8("F8", LIGHT, 0x0000800000000000L, 'F', 8, 47, 5, 0),
   G1("G1", DARK, 0x0001000000000000L, 'G', 1, 48, 6, 7),
   G2("G2", LIGHT, 0x0002000000000000L, 'G', 2, 49, 6, 6),
   G3("G3", DARK, 0x0004000000000000L, 'G', 3, 50, 6, 5),
   G4("G4", LIGHT, 0x0008000000000000L, 'G', 4, 51, 6, 4),
   G5("G5", DARK, 0x0010000000000000L, 'G', 5, 52, 6, 3),
   G6("G6", LIGHT, 0x0020000000000000L, 'G', 6, 53, 6, 2),
   G7("G7", DARK, 0x0040000000000000L, 'G', 7, 54, 6, 1),
   G8("G8", LIGHT, 0x0080000000000000L, 'G', 8, 55, 6, 0),
   H1("H1", DARK, 0x0100000000000000L, 'H', 1, 56, 7, 7),
   H2("H2", LIGHT, 0x0200000000000000L, 'H', 2, 57, 7, 6),
   H3("H3", DARK, 0x0400000000000000L, 'H', 3, 58, 7, 5),
   H4("H4", LIGHT, 0x0800000000000000L, 'H', 4, 59, 7, 4),
   H5("H5", DARK, 0x1000000000000000L, 'H', 5, 60, 7, 3),
   H6("H6", LIGHT, 0x2000000000000000L, 'H', 6, 61, 7, 2),
   H7("H7", DARK, 0x4000000000000000L, 'H', 7, 62, 7, 1),
   H8("H8", LIGHT, 0x8000000000000000L, 'H', 8, 63, 7, 0);

   public final ChessCellColor color;
   public final String code;
   public final long mask;
   public final char letter;   
   public final int digit;
   public final int index;
   public final int x;
   public final int y;

   private ChessBoardPosition(String code, ChessCellColor color, long mask, char letter, int digit, int index, int x, int y) {      
      this.letter = letter;
      this.digit = digit;
      this.color = color;
      this.index = index;
      this.code = code;
      this.mask = mask;       
      this.x = x;
      this.y = y;
   }
   
   public static ChessBoardPosition at(int index) {
      if(index < 0 || index >= LINE.length) {
         return null;
      }
      return LINE[index];
   }   

   public static ChessBoardPosition at(int x, int y) { // graphics x y coordinates
      if(x < 0 || x >= SQUARE.length) {
         return null;
      }
      if(y < 0 || y >= SQUARE.length) {
         return null;
      }
      return SQUARE[x][y];
   }

   public static ChessBoardPosition at(String code) {
      int letter = code.charAt(0);
      int digit = code.charAt(1);
      int x = LETTERS.indexOf(letter);
      int y = DIGITS.indexOf(digit);

      if(x == -1 || y == -1) {
         throw new IllegalArgumentException("No position found for " + code);
      }
      return SQUARE[x][y];
   }

   private static final String LETTERS = "ABCDEFGH";
   private static final String DIGITS = "87654321";
   private static final ChessBoardPosition[] LINE =
   {
      A1, A2, A3, A4, A5, A6, A7, A8,
      B1, B2, B3, B4, B5, B6, B7, B8,
      C1, C2, C3, C4, C5, C6, C7, C8,
      D1, D2, D3, D4, D5, D6, D7, D8,
      E1, E2, E3, E4, E5, E6, E7, E8,
      F1, F2, F3, F4, F5, F6, F7, F8,
      G1, G2, G3, G4, G5, G6, G7, G8,
      H1, H2, H3, H4, H5, H6, H7, H8,
   };
   private static final ChessBoardPosition[][] SQUARE =
   {
      {A8, A7, A6, A5, A4, A3, A2, A1},
      {B8, B7, B6, B5, B4, B3, B2, B1},
      {C8, C7, C6, C5, C4, C3, C2, C1},
      {D8, D7, D6, D5, D4, D3, D2, D1},
      {E8, E7, E6, E5, E4, E3, E2, E1},
      {F8, F7, F6, F5, F4, F3, F2, F1},
      {G8, G7, G6, G5, G4, G3, G2, G1},
      {H8, H7, H6, H5, H4, H3, H2, H1},
   };
}
