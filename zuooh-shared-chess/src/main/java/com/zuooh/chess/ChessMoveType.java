package com.zuooh.chess;

import java.util.Collections;
import java.util.List;

import com.zuooh.chess.analyze.ChessCaptureAnalyzer;
import com.zuooh.chess.analyze.ChessMoveAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.binary.ChessBitBoard;

public enum ChessMoveType {
   NORMAL {
      @Override
      public List<ChessBoardPosition> possiblePositions(ChessBoard board, ChessPiece piece) {
         long moves = ChessMoveAnalyzer.move(board, piece);
         
         if(moves != 0) {
            ChessBitBoard result = new ChessBitBoard(moves);
            return result.listOccupied();
         }
         return Collections.emptyList();
      }
   },
   CAPTURE {
      @Override
      public List<ChessBoardPosition> possiblePositions(ChessBoard board, ChessPiece piece) {
         long captures = ChessCaptureAnalyzer.captures(board, piece);
         
         if(captures != 0) {
            ChessBitBoard result = new ChessBitBoard(captures);
            return result.listOccupied();
         }
         return Collections.emptyList();
      }
   };  
   
   public abstract List<ChessBoardPosition> possiblePositions(ChessBoard board, ChessPiece piece);
}
