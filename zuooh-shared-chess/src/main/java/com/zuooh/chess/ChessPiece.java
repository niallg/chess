package com.zuooh.chess;

import static com.zuooh.chess.ChessBoardPosition.A1;
import static com.zuooh.chess.ChessBoardPosition.A8;
import static com.zuooh.chess.ChessBoardPosition.C1;
import static com.zuooh.chess.ChessBoardPosition.C8;
import static com.zuooh.chess.ChessBoardPosition.E1;
import static com.zuooh.chess.ChessBoardPosition.E8;
import static com.zuooh.chess.ChessBoardPosition.G1;
import static com.zuooh.chess.ChessBoardPosition.G8;
import static com.zuooh.chess.ChessBoardPosition.H1;
import static com.zuooh.chess.ChessBoardPosition.H8;
import static com.zuooh.chess.ChessPieceType.KING;
import static com.zuooh.chess.ChessPieceType.PAWN;
import static com.zuooh.chess.ChessPieceType.ROOK;
import static com.zuooh.chess.ChessSide.BLACK;
import static com.zuooh.chess.ChessSide.WHITE;

import java.io.Serializable;
import java.util.List;

import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;

public class ChessPiece implements Serializable {

   private static final long serialVersionUID = 1L;
   
   public final ChessPieceKey key;
   public final ChessSide side;

   public ChessPiece(String originalPosition, ChessPieceType type, ChessSide side) {
      this(ChessBoardPosition.at(originalPosition), type, side);
   }

   public ChessPiece(ChessBoardPosition originalPosition, ChessPieceType type, ChessSide side) {
      this(ChessPieceKey.resolveKey(originalPosition, type, side), side);
   }

   public ChessPiece(ChessPieceKey key, ChessSide side) {
      if(side != key.side) {
         throw new IllegalStateException("Attempting to create piece " + key + " to play in direction " + side);
      }
      this.side = side;
      this.key = key;
   }

   public boolean check(ChessBoard board) {
      if(key.type == KING) {
         return ChessCheckAnalyzer.check(board, side);
      }
      return false;
   }
   
   public boolean checkIf(ChessBoard board, ChessBoardPosition to) {
      ChessBoardPosition from = board.getPosition(this);
      
      if(from != null) {
         return ChessCheckAnalyzer.checkIf(board, from, to, side);
      }
      return false;
   }

   public List<ChessBoardPosition> moveNormal(ChessBoard board) {
      return key.type.moveNormal(board, this);
   }

   public List<ChessBoardPosition> moveCapture(ChessBoard board) {
      return key.type.moveCapture(board, this);
   }  
   
   public boolean moveWasPromotion(ChessBoard board, ChessBoardPosition position) {
      if(key.type == PAWN) {
         return position.digit == 8 || position.digit == 1;
      }
      return false;
   }
   
   public boolean moveWasEnPassent(ChessBoard board, ChessBoardPosition position) {
      if(key.type == PAWN) {
         if(position.digit == 3 || position.digit == 6) {
            ChessBoardPosition attackPosition = null;
            
            if(side == WHITE) {
               attackPosition = ChessBoardPosition.at(position.x, position.y + 1);
            } else {
               attackPosition = ChessBoardPosition.at(position.x, position.y - 1);
            }
            if(attackPosition != null) {
               ChessPiece opponentPiece = board.getPiece(attackPosition);
               
               if(opponentPiece != null && opponentPiece.key.type == PAWN) {
                  if(opponentPiece.side != side) {
                     ChessBoardPosition lastMove = board.getLastPosition(opponentPiece.side);
                     int moveCount = board.getMoveCount(opponentPiece);
                     
                     if(moveCount == 1 && attackPosition == lastMove) { // opponent pawn with only one move made
                        if(side == WHITE) {
                           return attackPosition.digit == 5;
                        }
                        return attackPosition.digit == 4;                        
                     }
                  }                    
               }
            }
         }         
      }
      return false;
   }

   
   public boolean moveWasCastle(ChessBoard board, ChessBoardPosition position) {
      if(key.type == KING) {
         int moveCount = board.getMoveCount(this);

         if(moveCount == 0) {
            ChessBoardPosition kingPosition = board.getKingPosition(side);

            if(side == BLACK) {
               if(kingPosition == E8) {
                  if(position == C8) {
                     if(canCastleWith(board, A8)) {
                        return true;
                     }
                  } else if(position == G8) {
                     if(canCastleWith(board, H8)) {
                        return true;
                     }
                  }
               }
            } else {
               if(kingPosition == E1) {
                  if(position == C1) {
                     if(canCastleWith(board, A1)) {
                        return true;
                     }
                  } else if(position == G1) {
                     if(canCastleWith(board, H1)) {
                        return true;
                     }
                  }
               }
            }
         }
      }
      return false;
   }   
   
   public boolean canTakeEnPassent(ChessBoard board, ChessBoardPosition targetPosition) {
      if(key.type == PAWN) {
         ChessPiece opponentPiece = board.getPiece(targetPosition);
         ChessBoardPosition currentPosition = board.getPosition(this);
         
         if(opponentPiece != null && opponentPiece.key.type == PAWN) {
            int moveCount = board.getMoveCount(opponentPiece);            
            
            if(moveCount == 1 && opponentPiece.side != side) { // opponent pawn with only one move made
               if(side == WHITE) {
                  return targetPosition.digit == 5 && currentPosition.digit == 5;
               }
               return targetPosition.digit == 4 && currentPosition.digit == 4;               
            }
         }         
      }
      return false;
   }

   public boolean canCastleWith(ChessBoard board, ChessBoardPosition rookPosition) {
      ChessPiece chessPiece = board.getPiece(rookPosition);

      if(chessPiece != null) {
         if(chessPiece.key.type == ROOK) {      
            int rookCount = board.getMoveCount(chessPiece);  

            if(rookCount == 0) {
               return chessPiece.side == side;
            }
         } else if(chessPiece.key.type == KING) {
            int kingCount = board.getMoveCount(chessPiece);  

            if(kingCount == 0) {
               return chessPiece.side == side;
            }
         }
      }
      return false;
   }

   public boolean canTake(ChessPiece otherPiece) {
      return otherPiece != null && otherPiece.getSide() != side;
   }

   public ChessSide getSide() {
      return side;
   }

   public ChessPieceType getPieceType() {
      return key.type;
   }

   @Override
   public boolean equals(Object otherValue) {
      if(otherValue == this) {
         return true;
      }
      if(otherValue instanceof ChessPiece) {
         ChessPiece chessPiece = (ChessPiece) otherValue;
         return chessPiece.key == key;
      }
      return false;
   }

   @Override
   public int hashCode() {
      return key.hashCode();
   }

   @Override
   public String toString() {
      return String.format("%s->%s", key.type, side);
   }
}
