package com.zuooh.chess.board;

import java.util.Iterator;

import com.zuooh.chess.ChessBoardPosition;

public class ChessBoardCellIterator implements Iterator<ChessBoardCell> {
   
   private ChessBoardCell current;
   private ChessBoard board;
   private long occupation;
   private int count;
   
   public ChessBoardCellIterator(ChessBoard board, long occupation) {
      this.occupation = occupation;
      this.board = board;
   }

   @Override
   public boolean hasNext() {
      if(current != null) {
         return true;
      }
      while(count < 64) {
         long positionMask = 1L << count++;
         
         if((occupation & positionMask) == positionMask) {
            ChessBoardPosition position = ChessBoardPosition.at(count - 1);
            current = board.getBoardCell(position);              
            return true;
         }         
      }
      return false;
   }

   @Override
   public ChessBoardCell next() {
      ChessBoardCell next = current;
      
      if(next == null) {
         while(count < 64) {
            long positionMask = 1L << count++;
            
            if((occupation & positionMask) == positionMask) {
               ChessBoardPosition position = ChessBoardPosition.at(count - 1);
               return board.getBoardCell(position);              
            }         
         }
      }                     
      current = null;
      return next;         
   }

   @Override
   public void remove() {
      throw new UnsupportedOperationException("Chess board cell iterator is immutable"); 
   }
   
}