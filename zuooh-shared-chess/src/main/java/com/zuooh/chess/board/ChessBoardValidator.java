package com.zuooh.chess.board;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;

public class ChessBoardValidator {
   
   public static void validateBoardHash(ChessBoard board, ChessSide side, long boardHash) {
      long currentHash = ChessBoardHashCalculator.calculateBoardHash(board, side);
      
      if(currentHash != boardHash) {
         throw new IllegalStateException("Board hash is " + currentHash + " but expected " + boardHash);
      }
   }

   public static void validateBoardOccupation(ChessBoard board) {
      validateBoardOccupation(board, null);
      validateBoardOccupation(board, ChessSide.WHITE);
      validateBoardOccupation(board, ChessSide.BLACK);
   }
   
   public static void validateBoardOccupation(ChessBoard board, ChessSide side) {
      ChessBoardCellIterator boardCells = board.getBoardCells(side);
      long occupationMask = board.getBoardOccupation(side);
      long expectedMask = 0L;
      
      while(boardCells.hasNext()) {
         ChessBoardCell boardCell = boardCells.next(); 
         ChessBoardPosition position = boardCell.getPosition();
         ChessPiece piece = boardCell.getPiece();
         
         if(piece != null) {
            if((expectedMask & position.mask) == position.mask) {
               throw new IllegalStateException("Board produced position " + position + " already");
            }
            expectedMask |= position.mask;
         }
      }
      if(expectedMask != occupationMask) {
         throw new IllegalStateException("Expected " + expectedMask + " for " + side + " but was " + occupationMask);
      }
   }
   
   public static void validateBoardCells(ChessBoard board) {
      validateBoardCells(board, null);
      validateBoardCells(board, ChessSide.WHITE);
      validateBoardCells(board, ChessSide.BLACK);
   }
   
   public static void validateBoardCells(ChessBoard board, ChessSide side) {
      ChessBoardCellIterator boardCells = board.getBoardCells(side);
      
      while(boardCells.hasNext()) {
         ChessBoardCell boardCell = boardCells.next(); 
         ChessBoardPosition position = boardCell.getPosition();
         ChessPiece piece = boardCell.getPiece();
         
         if(side != null && piece == null) {
            throw new IllegalStateException("Board cell piece at " + position + " is null for " + side);
         }
         if(side != null && piece.side != side) {
            throw new IllegalStateException("Board cell piece " + piece + " is not on side " + side + " at " + position);
         }         
         ChessPiece boardPiece = board.getPiece(position);         
         
         if(side != null && boardPiece == null) {
            throw new IllegalStateException("Board piece at " + position + " is null for " + side);
         }
         if(side != null && boardPiece.side != side) {
            throw new IllegalStateException("Board piece " + boardPiece + " is not on side " + side + " at " + position);
         }         
         if(side != null && piece.side != boardPiece.side) {
            throw new IllegalStateException("Board piece is " + boardPiece + " but cell piece is " + piece + " at " + position);
         }
      }
   }

}
