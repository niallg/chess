package com.zuooh.chess.board;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;

public interface ChessBoard {
   long getBoardKey();
   int getChangeCount();   
   int getMoveCount(ChessPiece piece);
   long getBoardOccupation(ChessSide side);
   ChessMove getLastMove(ChessSide side);
   ChessSide getSide(ChessBoardPosition position);
   ChessBoardPosition getLastPosition(ChessSide side);
   ChessBoardPosition getKingPosition(ChessSide side);
   ChessBoardPosition getPosition(ChessPiece piece);
   ChessPiece getPiece(ChessBoardPosition position);
   ChessBoardCell getBoardCell(ChessBoardPosition position);
   ChessBoardCellIterator getBoardCells(ChessSide side);
   ChessBoardMove getBoardMove(ChessMove move);
   ChessBoardMoveIterator getBoardMoves();
}


