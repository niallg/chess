package com.zuooh.chess.board;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;

public interface ChessBoardCell {
   boolean isCellEmpty();
   ChessPiece getPiece();
   ChessBoardPosition getPosition();

}
