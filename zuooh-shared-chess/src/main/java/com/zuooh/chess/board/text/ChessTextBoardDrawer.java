package com.zuooh.chess.board.text;

import com.zuooh.chess.board.ChessBoard;

public class ChessTextBoardDrawer {

   public static String drawBoard(ChessBoard board) {
      return new ChessTextPieceSet(board).toString();
   }
}
