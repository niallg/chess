package com.zuooh.chess.board;

import com.zuooh.chess.ChessPiece;

public interface ChessBoardModelCell extends ChessBoardCell {
   void clearCell();
   ChessPiece placePiece(ChessPiece piece); 
   ChessPiece takePiece();
}
