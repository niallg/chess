package com.zuooh.chess.board;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;

public interface ChessBoardModelTransaction {
   void beforeNormalMoveMade();
   void beforeNormalMoveMade(ChessPiece takePiece);
   void beforePromotionMoveMade();
   void beforePromotionMoveMade(ChessPiece takePiece);   
   void beforeCastleMoveMade(ChessBoardPosition castlePosition);
   void beforeEnPassentMoveMade(ChessBoardPosition pawnTakePosition);
   void afterMoveRevert();
}
