package com.zuooh.chess.board;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;

public class ChessBoardModelUpdater { 
   
   public static void makeNormalMove(ChessBoardModelTransaction state, ChessBoardModel board, ChessMove move) {
      ChessBoardModelCell fromCell = board.getBoardCell(move.from);
      ChessBoardModelCell toCell = board.getBoardCell(move.to);
      ChessPiece chessPiece = fromCell.getPiece();
      
      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " + fromCell);
      }
      ChessPiece takePiece = toCell.getPiece();
      
      if(takePiece != null) {
         state.beforeNormalMoveMade(takePiece);
         toCell.takePiece();
      } else {
         state.beforeNormalMoveMade();
      }
      toCell.placePiece(chessPiece);
      fromCell.clearCell();
   }   
   
   public static void revertNormalMove(ChessBoardModelTransaction state, ChessBoardModel board, ChessMove move, ChessPiece pieceTaken) {
      ChessBoardModelCell fromCell = board.getBoardCell(move.from);
      ChessBoardModelCell toCell = board.getBoardCell(move.to);
      ChessPiece chessPiece = toCell.getPiece();
      
      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " + fromCell);
      }
      fromCell.placePiece(chessPiece);
      
      if(pieceTaken != null) {        
         toCell.placePiece(pieceTaken);
      } else {
         toCell.clearCell();
      }
      state.afterMoveRevert();
   }   
   
   public static void makePromotionMove(ChessBoardModelTransaction state, ChessBoardModel board, ChessMove move) {
      ChessBoardModelCell fromCell = board.getBoardCell(move.from);
      ChessBoardModelCell toCell = board.getBoardCell(move.to);
      ChessPiece chessPiece = fromCell.getPiece();
      
      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " + fromCell);
      }
      ChessPiece takePiece = toCell.getPiece();
      
      if(takePiece != null) {
         state.beforePromotionMoveMade(takePiece);
         toCell.takePiece();
      } else {
         state.beforePromotionMoveMade();
      }
      toCell.placePiece(chessPiece);
      fromCell.clearCell();
   }   
   
   public static void revertPromotionMove(ChessBoardModelTransaction state, ChessBoardModel board, ChessMove move, ChessPiece pawnMoved, ChessPiece pieceTaken) {
      ChessBoardModelCell fromCell = board.getBoardCell(move.from);
      ChessBoardModelCell toCell = board.getBoardCell(move.to);
      ChessPiece promotedPiece = toCell.getPiece();
      
      if(promotedPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " + fromCell);
      }
      fromCell.placePiece(pawnMoved); // place back original pawn
      toCell.clearCell();
      
      if(pieceTaken != null) {        
         toCell.placePiece(pieceTaken);     
      }
      state.afterMoveRevert();
   }     
   
   public static void makeCastleMove(ChessBoardModelTransaction state, ChessBoardModel board, ChessMove move) {
      ChessBoardModelCell fromCell = board.getBoardCell(move.from);
      ChessBoardModelCell toCell = board.getBoardCell(move.to);
      ChessPiece chessPiece = fromCell.getPiece();
      ChessBoardPosition toPosition = toCell.getPosition();
      
      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " + fromCell);
      }
      ChessPiece castlePiece = null;
      ChessBoardModelCell castleFromCell = null;
      ChessBoardModelCell castleToCell = null;

      if(chessPiece.side == ChessSide.BLACK) {
         if(toPosition == ChessBoardPosition.G8) {
            castlePiece = board.getPiece(ChessBoardPosition.H8);
            castleFromCell = board.getBoardCell(ChessBoardPosition.H8);
            castleToCell = board.getBoardCell(ChessBoardPosition.F8);
         } else if(toPosition == ChessBoardPosition.C8) {
            castlePiece = board.getPiece(ChessBoardPosition.A8);
            castleFromCell = board.getBoardCell(ChessBoardPosition.A8);
            castleToCell = board.getBoardCell(ChessBoardPosition.D8);
         }
      } else {
         if(toPosition == ChessBoardPosition.G1) {
            castlePiece = board.getPiece(ChessBoardPosition.H1);
            castleFromCell = board.getBoardCell(ChessBoardPosition.H1);
            castleToCell = board.getBoardCell(ChessBoardPosition.F1);
         } else if(toPosition == ChessBoardPosition.C1) {
            castlePiece = board.getPiece(ChessBoardPosition.A1);
            castleFromCell = board.getBoardCell(ChessBoardPosition.A1);
            castleToCell = board.getBoardCell(ChessBoardPosition.D1);
         }
      }
      ChessBoardPosition castleFromPosition = castleFromCell.getPosition();
      
      state.beforeCastleMoveMade(castleFromPosition);
      castleToCell.placePiece(castlePiece);
      castleFromCell.clearCell();
      toCell.placePiece(chessPiece);
      fromCell.clearCell();      
   }
   
   public static void revertCastleMove(ChessBoardModelTransaction state, ChessBoardModel board, ChessMove move) {
      ChessBoardModelCell fromCell = board.getBoardCell(move.from);
      ChessBoardModelCell toCell = board.getBoardCell(move.to);      
      ChessPiece chessPiece = toCell.getPiece();
      ChessBoardPosition toPosition = toCell.getPosition();
      
      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " + toPosition);
      }
      ChessPiece castlePiece = null;
      ChessBoardModelCell castleFromCell = null;
      ChessBoardModelCell castleToCell = null;

      if(chessPiece.side == ChessSide.BLACK) {
         if(toPosition == ChessBoardPosition.G8) {
            castleFromCell = board.getBoardCell(ChessBoardPosition.H8);
            castleToCell = board.getBoardCell(ChessBoardPosition.F8);
            castlePiece = board.getPiece(ChessBoardPosition.F8);
         } else if(toPosition == ChessBoardPosition.C8) {
            castleFromCell = board.getBoardCell(ChessBoardPosition.A8);
            castleToCell = board.getBoardCell(ChessBoardPosition.D8);
            castlePiece = board.getPiece(ChessBoardPosition.D8);
         }
      } else {
         if(toPosition == ChessBoardPosition.G1) {
            castleFromCell = board.getBoardCell(ChessBoardPosition.H1);
            castleToCell = board.getBoardCell(ChessBoardPosition.F1);
            castlePiece = board.getPiece(ChessBoardPosition.F1);
         } else if(toPosition == ChessBoardPosition.C1) {
            castleFromCell = board.getBoardCell(ChessBoardPosition.A1);
            castleToCell = board.getBoardCell(ChessBoardPosition.D1);
            castlePiece = board.getPiece(ChessBoardPosition.D1);
         }
      }          
      castleFromCell.placePiece(castlePiece);
      castleToCell.clearCell();
      fromCell.placePiece(chessPiece);
      toCell.clearCell();
      state.afterMoveRevert();      
   }    
   
   public static void makeEnPassentMove(ChessBoardModelTransaction state, ChessBoardModel board, ChessMove move) {
      ChessBoardModelCell fromCell = board.getBoardCell(move.from);
      ChessBoardModelCell toCell = board.getBoardCell(move.to);
      ChessPiece chessPiece = fromCell.getPiece();
      ChessBoardPosition toPosition = toCell.getPosition();
      
      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " + fromCell);
      }
      ChessBoardPosition pawnTakePosition = null;
      
      if(chessPiece.side == ChessSide.WHITE) {
         pawnTakePosition = ChessBoardPosition.at(toPosition.x, toPosition.y + 1);
      } else {
         pawnTakePosition = ChessBoardPosition.at(toPosition.x, toPosition.y - 1);
      }
      ChessBoardModelCell pawnTakeCell = board.getBoardCell(pawnTakePosition);

      state.beforeEnPassentMoveMade(pawnTakePosition);
      pawnTakeCell.takePiece();
      toCell.placePiece(chessPiece);
      fromCell.clearCell();         
   }
   
   public static void revertEnPassentMove(ChessBoardModelTransaction state, ChessBoardModel board, ChessMove move, ChessPiece pawnTaken) {
      ChessBoardModelCell fromCell = board.getBoardCell(move.from);
      ChessBoardModelCell toCell = board.getBoardCell(move.to);      
      ChessPiece chessPiece = toCell.getPiece();
      ChessBoardPosition toPosition = toCell.getPosition();     
      
      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " + fromCell);
      }
      ChessBoardPosition pawnTakePosition = null;
      
      if(chessPiece.side == ChessSide.WHITE) {
         pawnTakePosition = ChessBoardPosition.at(toPosition.x, toPosition.y + 1);
      } else {
         pawnTakePosition = ChessBoardPosition.at(toPosition.x, toPosition.y - 1);
      }
      ChessBoardModelCell pawnTakeCell = board.getBoardCell(pawnTakePosition);
      
      fromCell.placePiece(chessPiece);
      pawnTakeCell.placePiece(pawnTaken); 
      state.afterMoveRevert();
   }   
}

