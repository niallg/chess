package com.zuooh.chess.board;

public enum ChessBoardStatus {
   CHECK,
   CHECK_MATE,
   STALE_MATE,
   RESIGN,
   REJECT,
   DRAW,
   NORMAL,
   TIME_OUT;
   
   public boolean isCheck() {
      return this == CHECK;
   }
   
   public boolean isTimeOut() {
      return this == TIME_OUT;
   }
   
   public boolean isGameOver() {
      return this == CHECK_MATE || this == STALE_MATE || this == TIME_OUT || this == RESIGN || this == REJECT || this == DRAW;
   }
}
