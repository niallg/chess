package com.zuooh.chess.board;

import java.util.AbstractList;

import com.zuooh.chess.ChessBoardPosition;

public final class ChessBoardCellList extends AbstractList<ChessBoardCell> {

   private final ChessBoardCell[] boardCells;

   public ChessBoardCellList(ChessBoardCell... boardCells) {  
      this.boardCells = boardCells;
   }

   @Override
   public int size() {
      return boardCells.length;
   }

   @Override
   public ChessBoardCell get(int index) {
      return boardCells[index];
   }
  
   public ChessBoardCell get(ChessBoardPosition position) {
      return boardCells[position.index];
   }   

   @Override
   public int indexOf(Object value) {
      if (value == null) {
         for (int i = 0; i < boardCells.length; i++) {
            if (boardCells[i] == null) {
               return i;
            }
         }
      } else {
         for (int i = 0; i < boardCells.length; i++) {
            if (value.equals(boardCells[i])) {
               return i;               
            }
         }
      }
      return -1;
   }

   @Override
   public boolean contains(Object value) {
      return indexOf(value) != -1;
   }   

   @Override
   public ChessBoardCell set(int index, ChessBoardCell element) {
      throw new UnsupportedOperationException("Chess board cell list is immutable");
   }   
}