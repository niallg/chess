package com.zuooh.chess.board.binary;

import static com.zuooh.chess.ChessPieceType.BISHOP;
import static com.zuooh.chess.ChessPieceType.KING;
import static com.zuooh.chess.ChessPieceType.KNIGHT;
import static com.zuooh.chess.ChessPieceType.PAWN;
import static com.zuooh.chess.ChessPieceType.QUEEN;
import static com.zuooh.chess.ChessPieceType.ROOK;
import static com.zuooh.chess.ChessSide.BLACK;
import static com.zuooh.chess.ChessSide.WHITE;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;

public class ChessBitBoardAnalyzer {
   
   private static final long[][] PAWN_WHITE_PASSENT_ATTACKS_FROM = {
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000001000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000100010L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000010001000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000001000100000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000100010000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0010001000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x1000100000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0010000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L}};   
   
   private static final long[][] PAWN_BLACK_PASSENT_ATTACKS_FROM = {
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000800L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000080008L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000008000800L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000800080000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000080008000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0008000800000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0800080000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0008000000000000L, 0x0000000000000000L, 0x0000000000000000L, 0x0000000000000000L}};
   
   private static final long[][] PAWN_WHITE_ATTACKS_FROM = {
   {0x0000000000004000L, 0x0000000000002000L, 0x0000000000001000L, 0x0000000000000800L, 0x0000000000000400L, 0x0000000000000200L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000000400040L, 0x0000000000200020L, 0x0000000000100010L, 0x0000000000080008L, 0x0000000000040004L, 0x0000000000020002L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000000040004000L, 0x0000000020002000L, 0x0000000010001000L, 0x0000000008000800L, 0x0000000004000400L, 0x0000000002000200L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000004000400000L, 0x0000002000200000L, 0x0000001000100000L, 0x0000000800080000L, 0x0000000400040000L, 0x0000000200020000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0000400040000000L, 0x0000200020000000L, 0x0000100010000000L, 0x0000080008000000L, 0x0000040004000000L, 0x0000020002000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0040004000000000L, 0x0020002000000000L, 0x0010001000000000L, 0x0008000800000000L, 0x0004000400000000L, 0x0002000200000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x4000400000000000L, 0x2000200000000000L, 0x1000100000000000L, 0x0800080000000000L, 0x0400040000000000L, 0x0200020000000000L, 0x0000000000000000L, 0x0000000000000000L},
   {0x0040000000000000L, 0x0020000000000000L, 0x0010000000000000L, 0x0008000000000000L, 0x0004000000000000L, 0x0002000000000000L, 0x0000000000000000L, 0x0000000000000000L}};   
   
   private static final long[][] PAWN_BLACK_ATTACKS_FROM = {
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000004000L, 0x0000000000002000L, 0x0000000000001000L, 0x0000000000000800L, 0x0000000000000400L, 0x0000000000000200L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000000400040L, 0x0000000000200020L, 0x0000000000100010L, 0x0000000000080008L, 0x0000000000040004L, 0x0000000000020002L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000000040004000L, 0x0000000020002000L, 0x0000000010001000L, 0x0000000008000800L, 0x0000000004000400L, 0x0000000002000200L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000004000400000L, 0x0000002000200000L, 0x0000001000100000L, 0x0000000800080000L, 0x0000000400040000L, 0x0000000200020000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0000400040000000L, 0x0000200020000000L, 0x0000100010000000L, 0x0000080008000000L, 0x0000040004000000L, 0x0000020002000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0040004000000000L, 0x0020002000000000L, 0x0010001000000000L, 0x0008000800000000L, 0x0004000400000000L, 0x0002000200000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x4000400000000000L, 0x2000200000000000L, 0x1000100000000000L, 0x0800080000000000L, 0x0400040000000000L, 0x0200020000000000L},
   {0x0000000000000000L, 0x0000000000000000L, 0x0040000000000000L, 0x0020000000000000L, 0x0010000000000000L, 0x0008000000000000L, 0x0004000000000000L, 0x0002000000000000L}};
   
   private static final long[][] BISHOP_ATTACKS_FROM = {
   {0x0102040810204000L, 0x000102040810a000L, 0x0000010204885000L, 0x0000000182442800L, 0x0000008041221400L, 0x0000804020110a00L, 0x0080402010080500L, 0x8040201008040200L},
   {0x0204081020400040L, 0x0102040810a000a0L, 0x0001020488500050L, 0x0000018244280028L, 0x0000804122140014L, 0x00804020110a000aL, 0x8040201008050005L, 0x4020100804020002L},
   {0x0408102040004020L, 0x02040810a000a010L, 0x0102048850005088L, 0x0001824428002844L, 0x0080412214001422L, 0x804020110a000a11L, 0x4020100805000508L, 0x2010080402000204L},
   {0x0810204000402010L, 0x040810a000a01008L, 0x0204885000508804L, 0x0182442800284482L, 0x8041221400142241L, 0x4020110a000a1120L, 0x2010080500050810L, 0x1008040200020408L},
   {0x1020400040201008L, 0x0810a000a0100804L, 0x0488500050880402L, 0x8244280028448201L, 0x4122140014224180L, 0x20110a000a112040L, 0x1008050005081020L, 0x0804020002040810L},
   {0x2040004020100804L, 0x10a000a010080402L, 0x8850005088040201L, 0x4428002844820100L, 0x2214001422418000L, 0x110a000a11204080L, 0x0805000508102040L, 0x0402000204081020L},
   {0x4000402010080402L, 0xa000a01008040201L, 0x5000508804020100L, 0x2800284482010000L, 0x1400142241800000L, 0x0a000a1120408000L, 0x0500050810204080L, 0x0200020408102040L},
   {0x0040201008040201L, 0x00a0100804020100L, 0x0050880402010000L, 0x0028448201000000L, 0x0014224180000000L, 0x000a112040800000L, 0x0005081020408000L, 0x0002040810204080L}};

   private static final long[][] KING_ATTACKS_FROM = {
   {0x000000000000c040L, 0x000000000000e0a0L, 0x0000000000007050L, 0x0000000000003828L, 0x0000000000001c14L, 0x0000000000000e0aL, 0x0000000000000705L, 0x0000000000000302L},
   {0x0000000000c040c0L, 0x0000000000e0a0e0L, 0x0000000000705070L, 0x0000000000382838L, 0x00000000001c141cL, 0x00000000000e0a0eL, 0x0000000000070507L, 0x0000000000030203L},
   {0x00000000c040c000L, 0x00000000e0a0e000L, 0x0000000070507000L, 0x0000000038283800L, 0x000000001c141c00L, 0x000000000e0a0e00L, 0x0000000007050700L, 0x0000000003020300L},
   {0x000000c040c00000L, 0x000000e0a0e00000L, 0x0000007050700000L, 0x0000003828380000L, 0x0000001c141c0000L, 0x0000000e0a0e0000L, 0x0000000705070000L, 0x0000000302030000L},
   {0x0000c040c0000000L, 0x0000e0a0e0000000L, 0x0000705070000000L, 0x0000382838000000L, 0x00001c141c000000L, 0x00000e0a0e000000L, 0x0000070507000000L, 0x0000030203000000L},
   {0x00c040c000000000L, 0x00e0a0e000000000L, 0x0070507000000000L, 0x0038283800000000L, 0x001c141c00000000L, 0x000e0a0e00000000L, 0x0007050700000000L, 0x0003020300000000L},
   {0xc040c00000000000L, 0xe0a0e00000000000L, 0x7050700000000000L, 0x3828380000000000L, 0x1c141c0000000000L, 0x0e0a0e0000000000L, 0x0705070000000000L, 0x0302030000000000L},
   {0x40c0000000000000L, 0xa0e0000000000000L, 0x5070000000000000L, 0x2838000000000000L, 0x141c000000000000L, 0x0a0e000000000000L, 0x0507000000000000L, 0x0203000000000000L}};

   private static final long[][] KNIGHT_ATTACKS_FROM = {
   {0x0000000000402000L, 0x0000000000a01000L, 0x0000000000508800L, 0x0000000000284400L, 0x0000000000142200L, 0x00000000000a1100L, 0x0000000000050800L, 0x0000000000020400L},
   {0x0000000040200020L, 0x00000000a0100010L, 0x0000000050880088L, 0x0000000028440044L, 0x0000000014220022L, 0x000000000a110011L, 0x0000000005080008L, 0x0000000002040004L},
   {0x0000004020002040L, 0x000000a0100010a0L, 0x0000005088008850L, 0x0000002844004428L, 0x0000001422002214L, 0x0000000a1100110aL, 0x0000000508000805L, 0x0000000204000402L},
   {0x0000402000204000L, 0x0000a0100010a000L, 0x0000508800885000L, 0x0000284400442800L, 0x0000142200221400L, 0x00000a1100110a00L, 0x0000050800080500L, 0x0000020400040200L},
   {0x0040200020400000L, 0x00a0100010a00000L, 0x0050880088500000L, 0x0028440044280000L, 0x0014220022140000L, 0x000a1100110a0000L, 0x0005080008050000L, 0x0002040004020000L},
   {0x4020002040000000L, 0xa0100010a0000000L, 0x5088008850000000L, 0x2844004428000000L, 0x1422002214000000L, 0x0a1100110a000000L, 0x0508000805000000L, 0x0204000402000000L},
   {0x2000204000000000L, 0x100010a000000000L, 0x8800885000000000L, 0x4400442800000000L, 0x2200221400000000L, 0x1100110a00000000L, 0x0800080500000000L, 0x0400040200000000L},
   {0x0020400000000000L, 0x0010a00000000000L, 0x0088500000000000L, 0x0044280000000000L, 0x0022140000000000L, 0x00110a0000000000L, 0x0008050000000000L, 0x0004020000000000L}};

   private static final long[][] QUEEN_ATTACKS_FROM = {
   {0x8182848890a0c07fL, 0x404142444850e0bfL, 0x2020212224a870dfL, 0x10101011925438efL, 0x08080888492a1cf7L, 0x0404844424150efbL, 0x02824222120a07fdL, 0x81412111090503feL},
   {0x82848890a0c07fc0L, 0x4142444850e0bfe0L, 0x20212224a870df70L, 0x101011925438ef38L, 0x080888492a1cf71cL, 0x04844424150efb0eL, 0x824222120a07fd07L, 0x412111090503fe03L},
   {0x848890a0c07fc0a0L, 0x42444850e0bfe050L, 0x212224a870df70a8L, 0x1011925438ef3854L, 0x0888492a1cf71c2aL, 0x844424150efb0e15L, 0x4222120a07fd070aL, 0x2111090503fe0305L},
   {0x8890a0c07fc0a090L, 0x444850e0bfe05048L, 0x2224a870df70a824L, 0x11925438ef385492L, 0x88492a1cf71c2a49L, 0x4424150efb0e1524L, 0x22120a07fd070a12L, 0x11090503fe030509L},
   {0x90a0c07fc0a09088L, 0x4850e0bfe0504844L, 0x24a870df70a82422L, 0x925438ef38549211L, 0x492a1cf71c2a4988L, 0x24150efb0e152444L, 0x120a07fd070a1222L, 0x090503fe03050911L},
   {0xa0c07fc0a0908884L, 0x50e0bfe050484442L, 0xa870df70a8242221L, 0x5438ef3854921110L, 0x2a1cf71c2a498808L, 0x150efb0e15244484L, 0x0a07fd070a122242L, 0x0503fe0305091121L},
   {0xc07fc0a090888482L, 0xe0bfe05048444241L, 0x70df70a824222120L, 0x38ef385492111010L, 0x1cf71c2a49880808L, 0x0efb0e1524448404L, 0x07fd070a12224282L, 0x03fe030509112141L},
   {0x7fc0a09088848281L, 0xbfe0504844424140L, 0xdf70a82422212020L, 0xef38549211101010L, 0xf71c2a4988080808L, 0xfb0e152444840404L, 0xfd070a1222428202L, 0xfe03050911214181L}};

   private static final long[][] ROOK_ATTACKS_FROM = {
   {0x808080808080807fL, 0x40404040404040bfL, 0x20202020202020dfL, 0x10101010101010efL, 0x08080808080808f7L, 0x04040404040404fbL, 0x02020202020202fdL, 0x01010101010101feL},
   {0x8080808080807f80L, 0x404040404040bf40L, 0x202020202020df20L, 0x101010101010ef10L, 0x080808080808f708L, 0x040404040404fb04L, 0x020202020202fd02L, 0x010101010101fe01L},
   {0x80808080807f8080L, 0x4040404040bf4040L, 0x2020202020df2020L, 0x1010101010ef1010L, 0x0808080808f70808L, 0x0404040404fb0404L, 0x0202020202fd0202L, 0x0101010101fe0101L},
   {0x808080807f808080L, 0x40404040bf404040L, 0x20202020df202020L, 0x10101010ef101010L, 0x08080808f7080808L, 0x04040404fb040404L, 0x02020202fd020202L, 0x01010101fe010101L},
   {0x8080807f80808080L, 0x404040bf40404040L, 0x202020df20202020L, 0x101010ef10101010L, 0x080808f708080808L, 0x040404fb04040404L, 0x020202fd02020202L, 0x010101fe01010101L},
   {0x80807f8080808080L, 0x4040bf4040404040L, 0x2020df2020202020L, 0x1010ef1010101010L, 0x0808f70808080808L, 0x0404fb0404040404L, 0x0202fd0202020202L, 0x0101fe0101010101L},
   {0x807f808080808080L, 0x40bf404040404040L, 0x20df202020202020L, 0x10ef101010101010L, 0x08f7080808080808L, 0x04fb040404040404L, 0x02fd020202020202L, 0x01fe010101010101L},
   {0x7f80808080808080L, 0xbf40404040404040L, 0xdf20202020202020L, 0xef10101010101010L, 0xf708080808080808L, 0xfb04040404040404L, 0xfd02020202020202L, 0xfe01010101010101L}};
   
   private static final long[][] PAWN_WHITE_MOVES_TO = {
   {0x0000000000000000L, 0x0000000000008080L, 0x0000000000004040L, 0x0000000000002020L, 0x0000000000001010L, 0x0000000000000808L, 0x000000000000040cL, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000808080L, 0x0000000000404040L, 0x0000000000202020L, 0x0000000000101010L, 0x0000000000080808L, 0x0000000000040c04L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000080808000L, 0x0000000040404000L, 0x0000000020202000L, 0x0000000010101000L, 0x0000000008080800L, 0x00000000040c0400L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000008080800000L, 0x0000004040400000L, 0x0000002020200000L, 0x0000001010100000L, 0x0000000808080000L, 0x000000040c040000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000808080000000L, 0x0000404040000000L, 0x0000202020000000L, 0x0000101010000000L, 0x0000080808000000L, 0x0000040c04000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0080808000000000L, 0x0040404000000000L, 0x0020202000000000L, 0x0010101000000000L, 0x0008080800000000L, 0x00040c0400000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x8080800000000000L, 0x4040400000000000L, 0x2020200000000000L, 0x1010100000000000L, 0x0808080000000000L, 0x040c040000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x8080000000000000L, 0x4040000000000000L, 0x2020000000000000L, 0x1010000000000000L, 0x0808000000000000L, 0x0c04000000000000L, 0x0000000000000000L}};
   
   private static final long[][] PAWN_BLACK_MOVES_TO = {
   {0x0000000000000000L, 0x0000000000002030L, 0x0000000000001010L, 0x0000000000000808L, 0x0000000000000404L, 0x0000000000000202L, 0x0000000000000101L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000000203020L, 0x0000000000101010L, 0x0000000000080808L, 0x0000000000040404L, 0x0000000000020202L, 0x0000000000010101L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000000020302000L, 0x0000000010101000L, 0x0000000008080800L, 0x0000000004040400L, 0x0000000002020200L, 0x0000000001010100L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000002030200000L, 0x0000001010100000L, 0x0000000808080000L, 0x0000000404040000L, 0x0000000202020000L, 0x0000000101010000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0000203020000000L, 0x0000101010000000L, 0x0000080808000000L, 0x0000040404000000L, 0x0000020202000000L, 0x0000010101000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x0020302000000000L, 0x0010101000000000L, 0x0008080800000000L, 0x0004040400000000L, 0x0002020200000000L, 0x0001010100000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x2030200000000000L, 0x1010100000000000L, 0x0808080000000000L, 0x0404040000000000L, 0x0202020000000000L, 0x0101010000000000L, 0x0000000000000000L},
   {0x0000000000000000L, 0x3020000000000000L, 0x1010000000000000L, 0x0808000000000000L, 0x0404000000000000L, 0x0202000000000000L, 0x0101000000000000L, 0x0000000000000000L}};   
   
   private static final long[][] BISHOP_MOVES_TO = {
   {0x0102040810204000L, 0x000102040810a000L, 0x0000010204885000L, 0x0000000182442800L, 0x0000008041221400L, 0x0000804020110a00L, 0x0080402010080500L, 0x8040201008040200L},
   {0x0204081020400040L, 0x0102040810a000a0L, 0x0001020488500050L, 0x0000018244280028L, 0x0000804122140014L, 0x00804020110a000aL, 0x8040201008050005L, 0x4020100804020002L},
   {0x0408102040004020L, 0x02040810a000a010L, 0x0102048850005088L, 0x0001824428002844L, 0x0080412214001422L, 0x804020110a000a11L, 0x4020100805000508L, 0x2010080402000204L},
   {0x0810204000402010L, 0x040810a000a01008L, 0x0204885000508804L, 0x0182442800284482L, 0x8041221400142241L, 0x4020110a000a1120L, 0x2010080500050810L, 0x1008040200020408L},
   {0x1020400040201008L, 0x0810a000a0100804L, 0x0488500050880402L, 0x8244280028448201L, 0x4122140014224180L, 0x20110a000a112040L, 0x1008050005081020L, 0x0804020002040810L},
   {0x2040004020100804L, 0x10a000a010080402L, 0x8850005088040201L, 0x4428002844820100L, 0x2214001422418000L, 0x110a000a11204080L, 0x0805000508102040L, 0x0402000204081020L},
   {0x4000402010080402L, 0xa000a01008040201L, 0x5000508804020100L, 0x2800284482010000L, 0x1400142241800000L, 0x0a000a1120408000L, 0x0500050810204080L, 0x0200020408102040L},
   {0x0040201008040201L, 0x00a0100804020100L, 0x0050880402010000L, 0x0028448201000000L, 0x0014224180000000L, 0x000a112040800000L, 0x0005081020408000L, 0x0002040810204080L}};

   private static final long[][] KING_MOVES_TO = {
   {0x000000000000c040L, 0x000000000000e0a0L, 0x0000000000007050L, 0x0000000000003828L, 0x0000000000001c14L, 0x0000000000000e0aL, 0x0000000000000705L, 0x0000000000000302L},
   {0x0000000000c040c0L, 0x0000000000e0a0e0L, 0x0000000000705070L, 0x0000000000382838L, 0x00000000001c141cL, 0x00000000000e0a0eL, 0x0000000000070507L, 0x0000000000030203L},
   {0x00000000c040c000L, 0x00000000e0a0e000L, 0x0000000070507000L, 0x0000000038283800L, 0x000000001c141c00L, 0x000000000e0a0e00L, 0x0000000007050700L, 0x0000000003020300L},
   {0x000000c040c00000L, 0x000000e0a0e00000L, 0x0000007050700000L, 0x0000003828380000L, 0x0000001c141c0000L, 0x0000000e0a0e0000L, 0x0000000705070000L, 0x0000000302030000L},
   {0x0000c040c0000000L, 0x0000e0a0e0000000L, 0x0000705070000000L, 0x0000382838000000L, 0x00001c141c000000L, 0x00000e0a0e000000L, 0x0000070507000000L, 0x0001030203010000L},
   {0x00c040c000000000L, 0x00e0a0e000000000L, 0x0070507000000000L, 0x0038283800000000L, 0x001c141c00000000L, 0x000e0a0e00000000L, 0x0007050700000000L, 0x0003020300000000L},
   {0xc040c00000000000L, 0xe0a0e00000000000L, 0x7050700000000000L, 0x3828380000000000L, 0x1c141c0000000000L, 0x0e0a0e0000000000L, 0x0705070000000000L, 0x0302030000000000L},
   {0x40c0000000000000L, 0xa0e0000000000000L, 0x5070000000000000L, 0x2838000000000000L, 0x141c000000000000L, 0x0a0e000000000000L, 0x0507000000000000L, 0x0203000000000000L}};

   private static final long[][] KNIGHT_MOVES_TO = {
   {0x0000000000402000L, 0x0000000000a01000L, 0x0000000000508800L, 0x0000000000284400L, 0x0000000000142200L, 0x00000000000a1100L, 0x0000000000050800L, 0x0000000000020400L},
   {0x0000000040200020L, 0x00000000a0100010L, 0x0000000050880088L, 0x0000000028440044L, 0x0000000014220022L, 0x000000000a110011L, 0x0000000005080008L, 0x0000000002040004L},
   {0x0000004020002040L, 0x000000a0100010a0L, 0x0000005088008850L, 0x0000002844004428L, 0x0000001422002214L, 0x0000000a1100110aL, 0x0000000508000805L, 0x0000000204000402L},
   {0x0000402000204000L, 0x0000a0100010a000L, 0x0000508800885000L, 0x0000284400442800L, 0x0000142200221400L, 0x00000a1100110a00L, 0x0000050800080500L, 0x0000020400040200L},
   {0x0040200020400000L, 0x00a0100010a00000L, 0x0050880088500000L, 0x0028440044280000L, 0x0014220022140000L, 0x000a1100110a0000L, 0x0005080008050000L, 0x0002040004020000L},
   {0x4020002040000000L, 0xa0100010a0000000L, 0x5088008850000000L, 0x2844004428000000L, 0x1422002214000000L, 0x0a1100110a000000L, 0x0508000805000000L, 0x0204000402000000L},
   {0x2000204000000000L, 0x100010a000000000L, 0x8800885000000000L, 0x4400442800000000L, 0x2200221400000000L, 0x1100110a00000000L, 0x0800080500000000L, 0x0400040200000000L},
   {0x0020400000000000L, 0x0010a00000000000L, 0x0088500000000000L, 0x0044280000000000L, 0x0022140000000000L, 0x00110a0000000000L, 0x0008050000000000L, 0x0004020000000000L}};

   private static final long[][] QUEEN_MOVES_TO = {
   {0x8182848890a0c07fL, 0x404142444850e0bfL, 0x2020212224a870dfL, 0x10101011925438efL, 0x08080888492a1cf7L, 0x0404844424150efbL, 0x02824222120a07fdL, 0x81412111090503feL},
   {0x82848890a0c07fc0L, 0x4142444850e0bfe0L, 0x20212224a870df70L, 0x101011925438ef38L, 0x080888492a1cf71cL, 0x04844424150efb0eL, 0x824222120a07fd07L, 0x412111090503fe03L},
   {0x848890a0c07fc0a0L, 0x42444850e0bfe050L, 0x212224a870df70a8L, 0x1011925438ef3854L, 0x0888492a1cf71c2aL, 0x844424150efb0e15L, 0x4222120a07fd070aL, 0x2111090503fe0305L},
   {0x8890a0c07fc0a090L, 0x444850e0bfe05048L, 0x2224a870df70a824L, 0x11925438ef385492L, 0x88492a1cf71c2a49L, 0x4424150efb0e1524L, 0x22120a07fd070a12L, 0x11090503fe030509L},
   {0x90a0c07fc0a09088L, 0x4850e0bfe0504844L, 0x24a870df70a82422L, 0x925438ef38549211L, 0x492a1cf71c2a4988L, 0x24150efb0e152444L, 0x120a07fd070a1222L, 0x090503fe03050911L},
   {0xa0c07fc0a0908884L, 0x50e0bfe050484442L, 0xa870df70a8242221L, 0x5438ef3854921110L, 0x2a1cf71c2a498808L, 0x150efb0e15244484L, 0x0a07fd070a122242L, 0x0503fe0305091121L},
   {0xc07fc0a090888482L, 0xe0bfe05048444241L, 0x70df70a824222120L, 0x38ef385492111010L, 0x1cf71c2a49880808L, 0x0efb0e1524448404L, 0x07fd070a12224282L, 0x03fe030509112141L},
   {0x7fc0a09088848281L, 0xbfe0504844424140L, 0xdf70a82422212020L, 0xef38549211101010L, 0xf71c2a4988080808L, 0xfb0e152444840404L, 0xfd070a1222428202L, 0xfe03050911214181L}};

   private static final long[][] ROOK_MOVES_TO = {
   {0x808080808080807fL, 0x40404040404040bfL, 0x20202020202020dfL, 0x10101010101010efL, 0x08080808080808f7L, 0x04040404040404fbL, 0x02020202020202fdL, 0x01010101010101feL},
   {0x8080808080807f80L, 0x404040404040bf40L, 0x202020202020df20L, 0x101010101010ef10L, 0x080808080808f708L, 0x040404040404fb04L, 0x020202020202fd02L, 0x010101010101fe01L},
   {0x80808080807f8080L, 0x4040404040bf4040L, 0x2020202020df2020L, 0x1010101010ef1010L, 0x0808080808f70808L, 0x0404040404fb0404L, 0x0202020202fd0202L, 0x0101010101fe0101L},
   {0x808080807f808080L, 0x40404040bf404040L, 0x20202020df202020L, 0x10101010ef101010L, 0x08080808f7080808L, 0x04040404fb040404L, 0x02020202fd020202L, 0x01010101fe010101L},
   {0x8080807f80808080L, 0x404040bf40404040L, 0x202020df20202020L, 0x101010ef10101010L, 0x080808f708080808L, 0x040404fb04040404L, 0x020202fd02020202L, 0x010101fe01010101L},
   {0x80807f8080808080L, 0x4040bf4040404040L, 0x2020df2020202020L, 0x1010ef1010101010L, 0x0808f70808080808L, 0x0404fb0404040404L, 0x0202fd0202020202L, 0x0101fe0101010101L},
   {0x807f808080808080L, 0x40bf404040404040L, 0x20df202020202020L, 0x10ef101010101010L, 0x08f7080808080808L, 0x04fb040404040404L, 0x02fd020202020202L, 0x01fe010101010101L},
   {0x7f80808080808080L, 0xbf40404040404040L, 0xdf20202020202020L, 0xef10101010101010L, 0xf708080808080808L, 0xfb04040404040404L, 0xfd02020202020202L, 0xfe01010101010101L}}; 
   
   
   public static long possibleCapturesByPawn(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position == null) {
         throw new IllegalArgumentException("Piece " + piece + " is not on the board");
      }
      if(piece.side == WHITE) {
         long normalMoves = PAWN_WHITE_MOVES_TO[position.x][position.y];
         
         if(normalMoves != 0L) {
            long blackOccupation = board.getBoardOccupation(BLACK);   
            long capturePositions = normalMoves & blackOccupation;
            
            if(position.digit == 5) {
               ChessBoardPosition passentEast = ChessBoardPosition.at(position.x + 1, position.y);
               ChessBoardPosition passentWest = ChessBoardPosition.at(position.x + 1, position.y);
               
               if(passentEast != null && (passentEast.mask | blackOccupation) == passentEast.mask) {
                  capturePositions |= passentEast.mask;
               }
               if(passentWest != null && (passentWest.mask | blackOccupation) == passentWest.mask) {
                  capturePositions |= passentWest.mask;
               }               
            }
            return capturePositions;
         }
      } else {        
         long normalMoves = PAWN_BLACK_MOVES_TO[position.x][position.y];
         
         if(normalMoves != 0L) {
            long whiteOccupation = board.getBoardOccupation(WHITE);   
            long capturePositions = normalMoves & whiteOccupation;
            
            if(position.digit == 4) {
               ChessBoardPosition passentEast = ChessBoardPosition.at(position.x + 1, position.y);
               ChessBoardPosition passentWest = ChessBoardPosition.at(position.x + 1, position.y);
               
               if(passentEast != null && (passentEast.mask | whiteOccupation) == passentEast.mask) {
                  capturePositions |= passentEast.mask;
               }
               if(passentWest != null && (passentWest.mask | whiteOccupation) == passentWest.mask) {
                  capturePositions |= passentWest.mask;
               }               
            }
            return capturePositions;
         }
      }          
      return 0L;
   }
   
   public static long possibleCapturesByBishop(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position == null) {
         throw new IllegalArgumentException("Piece " + piece + " is not on the board");
      }
      ChessSide opposite = piece.side.oppositeSide();
      long bishopMoves = BISHOP_MOVES_TO[position.x][position.y];
      long occupation = board.getBoardOccupation(opposite);
      
      return bishopMoves & occupation;
   }
   
   public static long possibleCapturesByKnight(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position == null) {
         throw new IllegalArgumentException("Piece " + piece + " is not on the board");
      }
      ChessSide opposite = piece.side.oppositeSide();
      long knightMoves = KNIGHT_MOVES_TO[position.x][position.y];
      long occupation = board.getBoardOccupation(opposite);
      
      return knightMoves & occupation;
   }   
   
   public static long possibleCapturesByRook(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position == null) {
         throw new IllegalArgumentException("Piece " + piece + " is not on the board");
      }
      ChessSide opposite = piece.side.oppositeSide();
      long rookMoves = ROOK_MOVES_TO[position.x][position.y];
      long occupation = board.getBoardOccupation(opposite);
      
      return rookMoves & occupation;
   }    
   
   public static long possibleCapturesByQueen(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position == null) {
         throw new IllegalArgumentException("Piece " + piece + " is not on the board");
      }
      ChessSide opposite = piece.side.oppositeSide();
      long queenMoves = QUEEN_MOVES_TO[position.x][position.y];
      long occupation = board.getBoardOccupation(opposite);
      
      return queenMoves & occupation;
   }
   
   public static long possibleCapturesByKing(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position == null) {
         throw new IllegalArgumentException("Piece " + piece + " is not on the board");
      }
      ChessSide opposite = piece.side.oppositeSide();
      long kingMoves = KING_MOVES_TO[position.x][position.y];
      long occupation = board.getBoardOccupation(opposite);
      
      return kingMoves & occupation;
   }  
   
   public static long possiblePawnAttacksOn(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         if(piece.side == BLACK) {
            long normalAttacks = PAWN_WHITE_ATTACKS_FROM[position.x][position.y];
            
            if(normalAttacks != 0L) {
               long whiteOccupation = board.getBoardOccupation(WHITE);   
               long positions = normalAttacks & whiteOccupation;
               
               if(piece.key.type == PAWN) {
                  long passentAttacks = PAWN_WHITE_PASSENT_ATTACKS_FROM[position.x][position.y];
                  long passentPositions = passentAttacks & whiteOccupation;
                  
                  if(passentPositions != 0L) {
                     positions |= passentPositions;
                  }
               }
               if(positions != 0L) {
                  return possibleAttacksOn(board, PAWN, positions);
               } 
            }
         } else {        
            long normalAttacks = PAWN_BLACK_ATTACKS_FROM[position.x][position.y];
            
            if(normalAttacks != 0L) {
               long blackOccupation = board.getBoardOccupation(BLACK);   
               long positions = normalAttacks & blackOccupation;

               if(piece.key.type == PAWN) {
                  long passentAttacks = PAWN_BLACK_PASSENT_ATTACKS_FROM[position.x][position.y];
                  long passentPositions = passentAttacks & blackOccupation;
                  
                  if(passentPositions != 0L) {
                     positions |= passentPositions;
                  }
               }
               if(positions != 0L) {
                  return possibleAttacksOn(board, PAWN, positions);
               }   
            }
         }           
      }
      return 0L;
   }
   
   public static long possibleBishopAttacksOn(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         ChessSide opposite = piece.side.oppositeSide();
         long bishopAttacks = BISHOP_ATTACKS_FROM[position.x][position.y];
         long occupation = board.getBoardOccupation(opposite);
         long positions = bishopAttacks & occupation;
         
         if(positions != 0L) {
            return possibleAttacksOn(board, BISHOP, positions);
         }                    
      }
      return 0L;
   }
   
   public static long possibleKnightAttacksOn(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         ChessSide opposite = piece.side.oppositeSide();
         long knightAttacks = KNIGHT_ATTACKS_FROM[position.x][position.y]; // black knights
         long occupation = board.getBoardOccupation(opposite);  
         long positions = knightAttacks & occupation;           
         
         if(positions != 0L) {
            return possibleAttacksOn(board, KNIGHT, positions);
         }
      }
      return 0L;
   }   
   
   public static long possibleRookAttacksOn(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         ChessSide opposite = piece.side.oppositeSide();
         long rookAttacks = ROOK_ATTACKS_FROM[position.x][position.y];
         long occupation = board.getBoardOccupation(opposite);  
         long positions = rookAttacks & occupation;             
         
         if(positions != 0L) {
            return possibleAttacksOn(board, ROOK, positions);
         }
      }
      return 0L;
   }    
   
   public static long possibleQueenAttacksOn(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         ChessSide opposite = piece.side.oppositeSide();
         long queenAttacks = QUEEN_ATTACKS_FROM[position.x][position.y];
         long occupation = board.getBoardOccupation(opposite);  
         long positions = queenAttacks & occupation;            
         
         if(positions != 0L) {
            return possibleAttacksOn(board, QUEEN, positions);
         }         
      }
      return 0L;
   }
   
   public static long possibleKingAttacksOn(ChessBoard board, ChessPiece piece) {
      if(piece.key.type != KING) {
         ChessBoardPosition position = board.getPosition(piece);
   
         if(position != null) {
            ChessSide opposite = piece.side.oppositeSide();
            long kingAttacks = KING_ATTACKS_FROM[position.x][position.y];
            long occupation = board.getBoardOccupation(opposite);  
            long positions = kingAttacks & occupation;             
            
            if(positions != 0L) {
               return possibleAttacksOn(board, KING, positions);
            } 
         }
      }
      return 0L;
   }

   private static long possibleAttacksOn(ChessBoard board, ChessPieceType type, long positions) {
      if(positions != 0L) {
         for(int i = 0; i < 64; i++) {
            long nextPosition = positions >>> i;
            
            if((nextPosition & 0x1L) == 0x1L) {
               ChessBoardPosition attackPosition = ChessBoardPosition.at(i);
               ChessPiece attackPiece = board.getPiece(attackPosition);
               ChessPieceKey attackKey = attackPiece.key;
               
               if(attackKey.type != type) {
                  positions &= ~attackPosition.mask;
               }
            }           
         }
      }   
      return positions;
   }
   
   public static long possiblePawnDefence(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         if(piece.side == WHITE) {
            long pawnAttacks = PAWN_WHITE_ATTACKS_FROM[position.x][position.y];
            long occupation = board.getBoardOccupation(piece.side);  
            long positions = pawnAttacks & occupation;
            
            if(positions != 0L) {
               return possibleDefence(board, PAWN, positions);
            }            
         } else {
            long pawnAttacks = PAWN_BLACK_ATTACKS_FROM[position.x][position.y];
            long occupation = board.getBoardOccupation(piece.side);  
            long positions = pawnAttacks & occupation;  
            
            if(positions != 0L) {
               return possibleDefence(board, PAWN, positions);
            }
         }
      }
      return 0L;
   }
   
   public static long possibleBishopDefence(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long bishopAttacks = BISHOP_ATTACKS_FROM[position.x][position.y];
         long occupation = board.getBoardOccupation(piece.side);  
         long positions = bishopAttacks & occupation;                  
         
         if(positions != 0L) {
            return possibleDefence(board, BISHOP, positions);
         }         
      }
      return 0L;
   }
   
   public static long possibleKnightDefence(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long knightAttacks = KNIGHT_ATTACKS_FROM[position.x][position.y]; // black knights
         long occupation = board.getBoardOccupation(piece.side);  
         long positions = knightAttacks & occupation;                  
         
         if(positions != 0L) {
            return possibleDefence(board, KNIGHT, positions);
         }          
      }
      return 0L;
   }   
   
   public static long possibleRookDefence(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long rookAttacks = ROOK_ATTACKS_FROM[position.x][position.y];
         long occupation = board.getBoardOccupation(piece.side);  
         long positions = rookAttacks & occupation;                    
         
         if(positions != 0L) {
            return possibleDefence(board, ROOK, positions);
         }          
      }
      return 0L;
   }    
   
   public static long possibleQueenDefence(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long queenAttacks = QUEEN_ATTACKS_FROM[position.x][position.y];
         long occupation = board.getBoardOccupation(piece.side);  
         long positions = queenAttacks & occupation;                   
         
         if(positions != 0L) {
            return possibleDefence(board, QUEEN, positions);
         } 
      }
      return 0L;
   }
   
   private static long possibleDefence(ChessBoard board, ChessPieceType type, long positions) {
      if(positions != 0L) {
         for(int i = 0; i < 64; i++) {
            if((positions >>>= i & 0x1L) == 0x1L) {
               ChessBoardPosition defencePosition = ChessBoardPosition.at(i);
               ChessPiece defencePiece = board.getPiece(defencePosition);
               ChessPieceKey defenceKey = defencePiece.key;
               
               if(defenceKey.type != type) {
                  positions &= ~defencePosition.mask;
               }
            }           
         }
      }   
      return positions;
   }   
   
   public static long possiblePawnMove(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         if(piece.side == WHITE) {
            long normalMoves = PAWN_WHITE_MOVES_TO[position.x][position.y];
            
            if(normalMoves != 0L) {
               long whiteOccupation = board.getBoardOccupation(WHITE);   
               long normalPositions = normalMoves & ~whiteOccupation;
               
               if(normalPositions != 0L) {
                  return normalPositions;
               }
            }
         } else {        
            long normalMoves = PAWN_BLACK_MOVES_TO[position.x][position.y];
            
            if(normalMoves != 0L) {
               long blackOccupation = board.getBoardOccupation(BLACK);   
               long normalPositions = normalMoves & ~blackOccupation;
               
               if(normalPositions != 0L) {
                  return normalPositions;
               }
            }
         }           
      }
      return 0L;
   }
   
   public static long possibleBishopMove(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long bishopMoves = BISHOP_MOVES_TO[position.x][position.y];
         long occupation = board.getBoardOccupation(piece.side);   
            
         return bishopMoves & ~occupation;                     
      }
      return 0L;
   }
   
   public static long possibleKnightMove(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long knightMoves = KNIGHT_MOVES_TO[position.x][position.y];
         long occupation = board.getBoardOccupation(piece.side);   
            
         return knightMoves & ~occupation;                     
      }
      return 0L;
   }   
   
   public static long possibleRookMove(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long rookMoves = ROOK_MOVES_TO[position.x][position.y];
         long occupation = board.getBoardOccupation(piece.side);   
            
         return rookMoves & ~occupation;                     
      }
      return 0L;
   }    
   
   public static long possibleQueenMove(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long queenMoves = QUEEN_MOVES_TO[position.x][position.y];
         long occupation = board.getBoardOccupation(piece.side);   
            
         return queenMoves & ~occupation;                     
      }
      return 0L;
   }
   
   public static long possibleKingMove(ChessBoard board, ChessPiece piece) {
      ChessBoardPosition position = board.getPosition(piece);

      if(position != null) {
         long kingMoves = KING_MOVES_TO[position.x][position.y];
         long occupation = board.getBoardOccupation(piece.side);   
            
         return kingMoves & ~occupation;                     
      }
      return 0L;
   } 
}
