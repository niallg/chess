package com.zuooh.chess.board;

import com.zuooh.chess.ChessBoardPosition;

public interface ChessBoardModel extends ChessBoard {
   ChessBoardModelCell getBoardCell(ChessBoardPosition position);
}
