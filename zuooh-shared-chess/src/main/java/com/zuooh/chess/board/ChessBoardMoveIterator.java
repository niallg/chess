package com.zuooh.chess.board;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ChessBoardMoveIterator implements Iterator<ChessBoardMove> {
   
   private ChessBoardMove[] moves;
   private int index;
   
   public ChessBoardMoveIterator(ChessBoardMove[] moves, int count) {
      this.index = count - 1;
      this.moves = moves;
   }

   @Override
   public boolean hasNext() {
      return index >= 0;
   }

   @Override
   public ChessBoardMove next() {
      if(index < 0) {
         throw new NoSuchElementException("No more moves available");
      }         
      return moves[index--];
   }

   @Override
   public void remove() {
      throw new UnsupportedOperationException("Chess move iterator is immutable"); 
   }
   
}