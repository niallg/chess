package com.zuooh.chess.board.text;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;

public enum ChessTextPiece {
   BLACK_PAWN("bP", ChessPieceType.PAWN, ChessSide.BLACK) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   BLACK_ROOK("bR", ChessPieceType.ROOK, ChessSide.BLACK) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   BLACK_KNIGHT("bK", ChessPieceType.KNIGHT, ChessSide.BLACK) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   BLACK_BISHOP("bB", ChessPieceType.BISHOP, ChessSide.BLACK) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   BLACK_QUEEN("bQ", ChessPieceType.QUEEN, ChessSide.BLACK) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   BLACK_KING("b#", ChessPieceType.KING, ChessSide.BLACK) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   WHITE_PAWN("wP", ChessPieceType.PAWN, ChessSide.WHITE) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   WHITE_ROOK("wR", ChessPieceType.ROOK, ChessSide.WHITE) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   WHITE_KNIGHT("wK", ChessPieceType.KNIGHT, ChessSide.WHITE) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   WHITE_BISHOP("wB", ChessPieceType.BISHOP, ChessSide.WHITE) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   WHITE_QUEEN("wQ", ChessPieceType.QUEEN, ChessSide.WHITE) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   WHITE_KING("w#", ChessPieceType.KING, ChessSide.WHITE) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return new ChessPiece(boardPosition, pieceType, pieceSide);
      }
   },
   EMPTY("  ", null, null) {
      @Override
      public ChessPiece getPiece(ChessBoardPosition boardPosition) {
         return null;
      }
   };
   
   public final ChessPieceType pieceType;
   public final ChessSide pieceSide;
   public final String code;

   private ChessTextPiece(String code, ChessPieceType pieceType, ChessSide pieceSide) {
      this.pieceSide = pieceSide;
      this.pieceType = pieceType;
      this.code = code;
   }

   public abstract ChessPiece getPiece(ChessBoardPosition boardPosition);

   public static ChessTextPiece resolvePiece(String pieceCode) {      
      if(pieceCode != null) {
         ChessTextPiece[] textPieces = values();
         
         for(ChessTextPiece textPiece : textPieces) {
            String pieceToken = pieceCode.trim();
            
            if(pieceToken.equals(textPiece.code)) {
               return textPiece;
            }
         }
      }
      return EMPTY;
   }

   public static ChessTextPiece resolvePiece(ChessPiece chessPiece) {
      if(chessPiece != null) {
         ChessTextPiece[] textPieces = values();
         
         for(ChessTextPiece textPiece : textPieces) {
            ChessPieceType pieceType = chessPiece.getPieceType();
            ChessSide side = chessPiece.getSide();
            
            if(textPiece.pieceSide.equals(side) && textPiece.pieceType.equals(pieceType)) {
               return textPiece;
            }
         }
      }
      return EMPTY;
   }
}