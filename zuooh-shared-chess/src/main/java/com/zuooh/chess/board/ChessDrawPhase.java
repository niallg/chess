package com.zuooh.chess.board;

public enum ChessDrawPhase {
   NOT_POSSIBLE("No request for draw made"),
   REQUEST_SENT("Player requests a draw"),
   REQUEST_RECEIVED("Draw request is received"),
   REQUEST_ACKNOWLEDGED("Draw request acknowledged");
   
   public final String description;
   
   private ChessDrawPhase(String description){
      this.description = description;
   }
   
   public boolean isDrawRequestSent() {
      return this == REQUEST_SENT;
   }
   
   public boolean isDrawRequestReceived() {
      return this == REQUEST_RECEIVED;
   }
   
   public boolean isDrawPossible() {
      return REQUEST_RECEIVED == this || REQUEST_ACKNOWLEDGED == this; 
   }
}
