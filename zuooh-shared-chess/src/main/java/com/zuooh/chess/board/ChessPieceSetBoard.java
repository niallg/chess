package com.zuooh.chess.board;

import static com.zuooh.chess.ChessPieceKey.sparePiece;
import static com.zuooh.chess.ChessPieceType.KING;
import static com.zuooh.chess.ChessPieceType.PAWN;
import static com.zuooh.chess.ChessPieceType.QUEEN;
import static com.zuooh.chess.ChessSide.BLACK;
import static com.zuooh.chess.ChessSide.WHITE;
import static com.zuooh.chess.ChessSide.values;

import java.util.Arrays;
import java.util.Map;
import java.util.Set;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.engine.ChessMove;

public final class ChessPieceSetBoard implements ChessBoardModel {
      
   private static final int OFFSET_MOVE_COUNT = 0;
   private static final int OFFSET_MOVE_PIECE_TYPE = 1;
   private static final int OFFSET_OTHER_COUNT = 2;
   private static final int OFFSET_OTHER_PIECE_TYPE = 3;
   private static final int OFFSET_OTHER_POSITION = 4;          
   private static final int OFFSET_CHANGE_TYPE = 7;
   private static final int OFFSET_BOARD_BEFORE_COUNT = 5;
   private static final int OFFSET_BOARD_AFTER_COUNT = 6;    
   private static final int OFFSET_BOARD_MOVE_COUNT = 8;     
   private static final int TYPE_NORMAL =  0;
   private static final int TYPE_EN_PASSENT = 1;
   private static final int TYPE_CASTLE = 2;
   private static final int TYPE_PROMOTION = 3;
   
   private ChessBoardCellUpdater[] boardCells;
   private ChessBoardMoveState[] boardMoves; // history of all moves
   private ChessBoardPosition[] kingPositions;
   private ChessBoardPosition[] boardPositions;
   private ChessPiece[][] sidePieces; // pieces by color/side
   private ChessPiece[] boardPieces;
   private long[] occupationMask;  
   private int[] pieceMoves;
   private int changeCount;
   private int moveCount;

   public ChessPieceSetBoard(ChessPieceSet pieceSet) {
      this.kingPositions = new ChessBoardPosition[2];
      this.boardPieces = new ChessPiece[64];
      this.boardCells = new ChessBoardCellUpdater[64];
      this.sidePieces = new ChessPiece[2][100];
      this.boardPositions = new ChessBoardPosition[100];
      this.boardMoves = new ChessBoardMoveState[100];
      this.occupationMask = new long[2];
      this.pieceMoves = new int[100];     
      
      for (int i = 0; i < boardCells.length; i++) {
         ChessBoardPosition boardPosition = ChessBoardPosition.at(i);
         ChessBoardCellUpdater boardCell = new ChessBoardCellUpdater(boardPosition, boardPieces);

         boardCells[boardPosition.index] = boardCell;         
      }      
      ChessSide[] sides = values();

      for(ChessSide side : sides) {
         Map<ChessBoardPosition, ChessPiece> pieces = pieceSet.getPieces(side);
         Set<ChessBoardPosition> boardPositions = pieces.keySet();

         for(ChessBoardPosition boardPosition : boardPositions){
            ChessBoardModelCell boardCell = getBoardCell(boardPosition);
            ChessPiece piece = pieces.get(boardPosition);
            
            boardCell.placePiece(piece);
         }
      }
      for(int i = 0; i < pieceMoves.length; i++){
         pieceMoves[i] = 0;
      }            
   }
   
   @Override
   public int getChangeCount() {
      return changeCount;
   }

   @Override
   public int getMoveCount(ChessPiece piece) {
      return pieceMoves[piece.key.index];
   }   

   @Override
   public ChessBoardPosition getKingPosition(ChessSide side) {
      return kingPositions[side.index];
   }

   @Override
   public ChessBoardPosition getPosition(ChessPiece piece) {
      return boardPositions[piece.key.index];
   }

   @Override
   public ChessBoardModelCell getBoardCell(ChessBoardPosition boardPosition) {
      if(boardPosition == null) {
         throw new IllegalStateException("Board position provided is null");
      }
      return boardCells[boardPosition.index];
   }

   @Override
   public ChessPiece getPiece(ChessBoardPosition boardPosition) {
      return boardPieces[boardPosition.index];
   }

   @Override
   public ChessMove getLastMove(ChessSide side) {
      for(int i = moveCount - 1; i >= 0; i--) {
         ChessBoardMoveState boardMove = boardMoves[i];

         if(boardMove != null && boardMove.move != null) {
            if(boardMove.move.side == side) {
               return boardMove.move;
            }
         }
      }
      return null;
   }   

   @Override
   public ChessBoardPosition getLastPosition(ChessSide side) {
      for(int i = moveCount - 1; i >= 0; i--) {
         ChessBoardMoveState boardMove = boardMoves[i];
         
         if(boardMove != null && boardMove.move != null) {
            if(boardMove.move.side == side) {
               return boardMove.move.to;
            }
         }
      }
      return null;
   }   

   @Override
   public ChessBoardCellIterator getBoardCells(ChessSide side) {      
      if(side == null) {
         long whiteOccupation = occupationMask[WHITE.index];         
         long blackOccupation = occupationMask[BLACK.index];
         
         return new ChessBoardCellIterator(this, whiteOccupation | blackOccupation);
      }
      return new ChessBoardCellIterator(this, occupationMask[side.index]);
   }   
   
   public ChessBoardMoveIterator getBoardMoves() {
      return new ChessBoardMoveIterator(boardMoves, moveCount);
   }
   
   @Override
   public ChessSide getSide(ChessBoardPosition boardPosition){
      long blackOccupation = occupationMask[BLACK.index];
      long whiteOccupation = occupationMask[WHITE.index];
      
      if((blackOccupation & boardPosition.mask) != 0) {
         return BLACK;
      }
      if((whiteOccupation & boardPosition.mask) != 0) {
         return WHITE;
      }
      return null;
   }  
   
   @Override
   public long getBoardOccupation(ChessSide side){
      if(side == null) {
         long blackOccupation = occupationMask[BLACK.index];
         long whiteOccupation = occupationMask[WHITE.index];
         
         return blackOccupation | whiteOccupation;
      }
      return occupationMask[side.index];
   } 
   
   @Override
   public ChessBoardMove getBoardMove(ChessMove move) {
      int[] changes = new int[9];
      
      for(int i = 0; i < changes.length; i++){
         changes[i] = -1;
      }
      return new ChessBoardMoveState(this, move, changes);      
   }
   
   @Override
   public long getBoardKey() {
      return System.identityHashCode(this);
   }

   @Override
   public String toString() {
      return ChessTextBoardDrawer.drawBoard(this);
   }

   public class ChessBoardMoveState implements ChessBoardMove, ChessBoardModelTransaction {
      
      private ChessBoardModel moveBoard;
      private ChessMove move;
      private int[] changes;

      public ChessBoardMoveState(ChessBoardModel moveBoard, ChessMove move, int[] changes) {
         this.changes = changes;
         this.moveBoard = moveBoard;
         this.move = move;
      }
      
      @Override
      public void makeMove() {
         makeMove(true);
      }
      
      @Override
      public void makeMove(boolean validate) {
         ChessBoardCell fromCell = moveBoard.getBoardCell(move.from);
         ChessBoardCell toCell = moveBoard.getBoardCell(move.to);
         ChessPiece chessPiece = fromCell.getPiece();

         if(chessPiece == null) {
            throw new NullPointerException("Cell does not have a piece " + fromCell);
         }
         int currentChange = changeCount;
         
         if(move.change != currentChange && validate) {
            throw new IllegalStateException("Move " + move + " has change " + move.change + " but board is " + currentChange);
         }         
         ChessBoardPosition to = toCell.getPosition();

         if(chessPiece.moveWasEnPassent(moveBoard, to)) {
            ChessBoardModelUpdater.makeEnPassentMove(this, moveBoard, move);
         } else if(chessPiece.moveWasCastle(moveBoard, to)) {
            ChessBoardModelUpdater.makeCastleMove(this, moveBoard, move);
         } else if(chessPiece.moveWasPromotion(moveBoard, move.to)) {
            ChessBoardModelUpdater.makePromotionMove(this, moveBoard, move);
         } else {
            ChessBoardModelUpdater.makeNormalMove(this, moveBoard, move);
         }
         if(moveCount >= boardMoves.length) {
            boardMoves = Arrays.copyOf(boardMoves, boardMoves.length + 100);
         }
         changes[OFFSET_BOARD_AFTER_COUNT] = changeCount;
         changes[OFFSET_BOARD_MOVE_COUNT] = moveCount;
         boardMoves[moveCount++] = this;
      }
      
      @Override
      public void revertMove() {
         int changeType = changes[OFFSET_CHANGE_TYPE];
         int expectChange = changes[OFFSET_BOARD_AFTER_COUNT]; // what was the change after the move
         int expectMove = changes[OFFSET_BOARD_MOVE_COUNT] + 1;
         
         if(expectChange != changeCount) {
            throw new IllegalStateException("Move " + move + " expects change " + expectChange + " but board is " + changeCount);
         }
         if(expectMove != moveCount) {
            throw new IllegalStateException("Move " + move + " is " + expectMove + " but board has " + moveCount + " moves");
         }          
         ChessBoardMove lastMove = boardMoves[moveCount - 1];
         
         if(lastMove != this) {
            throw new IllegalStateException("Move " + move + " was not the last move made");
         }
         ChessBoardCell toCell = moveBoard.getBoardCell(move.to);
         ChessPiece chessPiece = toCell.getPiece();
         
         if(chessPiece == null) {
            throw new NullPointerException("Cell does not have a piece " + toCell);
         }         
         if(changeType == TYPE_EN_PASSENT) {
            int takeIndex = changes[OFFSET_OTHER_PIECE_TYPE];
            ChessSide takeSide = move.side.oppositeSide();
            ChessPieceKey takeKey = ChessPieceKey.resolveKey(takeIndex);
            ChessPiece pawnTaken = takeKey.createPiece(takeSide);
            
            ChessBoardModelUpdater.revertEnPassentMove(this, moveBoard, move, pawnTaken);
         } else if(changeType == TYPE_CASTLE) {
            ChessBoardModelUpdater.revertCastleMove(this, moveBoard, move);
         } else if(changeType == TYPE_PROMOTION){
            int pawnIndex = changes[OFFSET_MOVE_PIECE_TYPE];
            int takeIndex = changes[OFFSET_OTHER_PIECE_TYPE];
            ChessPieceKey pawnKey = ChessPieceKey.resolveKey(pawnIndex);
            ChessPiece pawnMoved = pawnKey.createPiece(move.side);
            
            if(takeIndex != -1) {
               ChessSide takeSide = move.side.oppositeSide();
               ChessPieceKey takeKey = ChessPieceKey.resolveKey(takeIndex);
               ChessPiece pieceTaken = takeKey.createPiece(takeSide);
            
               ChessBoardModelUpdater.revertPromotionMove(this, moveBoard, move, pawnMoved, pieceTaken);
            } else {
               ChessBoardModelUpdater.revertPromotionMove(this, moveBoard, move, pawnMoved, null);
            }
         } else {
            int takeIndex = changes[OFFSET_OTHER_PIECE_TYPE];
            
            if(takeIndex != -1) {
               ChessSide takeSide = move.side.oppositeSide();
               ChessPieceKey takeKey = ChessPieceKey.resolveKey(takeIndex);
               ChessPiece pieceTaken = takeKey.createPiece(takeSide);
            
               ChessBoardModelUpdater.revertNormalMove(this, moveBoard, move, pieceTaken);
            } else {
               ChessBoardModelUpdater.revertNormalMove(this, moveBoard, move, null);
            }            
         }
         changeCount = changes[OFFSET_BOARD_BEFORE_COUNT];
         moveCount = changes[OFFSET_BOARD_MOVE_COUNT];
         boardMoves[moveCount] = null;
      }      
      
      @Override
      public void beforeNormalMoveMade() {
         ChessBoardCell fromCell = getBoardCell(move.from);
         ChessPiece chessPiece = fromCell.getPiece();

         changes[OFFSET_CHANGE_TYPE] = TYPE_NORMAL;
         changes[OFFSET_BOARD_BEFORE_COUNT] = changeCount;
         changes[OFFSET_MOVE_COUNT] = pieceMoves[chessPiece.key.index];
         changes[OFFSET_MOVE_PIECE_TYPE] = chessPiece.key.index;
         changes[OFFSET_OTHER_COUNT] = -1;
         changes[OFFSET_OTHER_PIECE_TYPE] = -1; 
         changes[OFFSET_OTHER_POSITION] = -1;
      }    
      
      @Override
      public void beforeNormalMoveMade(ChessPiece takePiece) {
         ChessBoardCell fromCell = getBoardCell(move.from);
         ChessPiece chessPiece = fromCell.getPiece();
         
         changes[OFFSET_CHANGE_TYPE] = TYPE_NORMAL;
         changes[OFFSET_BOARD_BEFORE_COUNT] = changeCount;
         changes[OFFSET_MOVE_COUNT] = pieceMoves[chessPiece.key.index];
         changes[OFFSET_MOVE_PIECE_TYPE] = chessPiece.key.index;            
         changes[OFFSET_OTHER_COUNT] = pieceMoves[takePiece.key.index];
         changes[OFFSET_OTHER_PIECE_TYPE] = takePiece.key.index;              
         changes[OFFSET_OTHER_POSITION] = move.to.index;     
         
      }      
      
      @Override
      public void beforeCastleMoveMade(ChessBoardPosition castlePosition) {
         ChessBoardCell kingCell = getBoardCell(move.from);
         ChessBoardCell castleCell = getBoardCell(castlePosition);
         ChessPiece kingPiece = kingCell.getPiece();
         ChessPiece castlePiece = castleCell.getPiece();         

         changes[OFFSET_CHANGE_TYPE] = TYPE_CASTLE;
         changes[OFFSET_BOARD_BEFORE_COUNT] = changeCount;
         changes[OFFSET_MOVE_COUNT] = pieceMoves[kingPiece.key.index];
         changes[OFFSET_MOVE_PIECE_TYPE] = kingPiece.key.index;        
         changes[OFFSET_OTHER_COUNT] = pieceMoves[castlePiece.key.index];
         changes[OFFSET_OTHER_PIECE_TYPE] = castlePiece.key.index; 
         changes[OFFSET_OTHER_POSITION] = castlePosition.index;        
      }
      
      @Override
      public void beforeEnPassentMoveMade(ChessBoardPosition pawnTakePosition) {
         ChessBoardCell pawnMoveCell = getBoardCell(move.from);
         ChessBoardCell pawnTakeCell = getBoardCell(pawnTakePosition);
         ChessPiece pawnMoved = pawnMoveCell.getPiece();
         ChessPiece pawnTaken = pawnTakeCell.getPiece();
         
         changes[OFFSET_CHANGE_TYPE] = TYPE_EN_PASSENT;
         changes[OFFSET_BOARD_BEFORE_COUNT] = changeCount;
         changes[OFFSET_MOVE_COUNT] = pieceMoves[pawnMoved.key.index];
         changes[OFFSET_MOVE_PIECE_TYPE] = pawnMoved.key.index;         
         changes[OFFSET_OTHER_COUNT] = pieceMoves[pawnTaken.key.index];
         changes[OFFSET_OTHER_PIECE_TYPE] = pawnTaken.key.index;         
         changes[OFFSET_OTHER_POSITION] = pawnTakePosition.index;         
      }
      
      @Override
      public void beforePromotionMoveMade() {
         ChessBoardCell fromCell = getBoardCell(move.from);
         ChessPiece chessPiece = fromCell.getPiece();
         
         changes[OFFSET_CHANGE_TYPE] = TYPE_PROMOTION;
         changes[OFFSET_BOARD_BEFORE_COUNT] = changeCount;
         changes[OFFSET_MOVE_COUNT] = pieceMoves[chessPiece.key.index];
         changes[OFFSET_MOVE_PIECE_TYPE] = chessPiece.key.index;            
         changes[OFFSET_OTHER_COUNT] = -1;
         changes[OFFSET_OTHER_PIECE_TYPE] = -1; 
         changes[OFFSET_OTHER_POSITION] = -1;          
      }      
      
      @Override
      public void beforePromotionMoveMade(ChessPiece takePiece) {
         ChessBoardCell fromCell = getBoardCell(move.from);
         ChessPiece chessPiece = fromCell.getPiece();
         
         changes[OFFSET_CHANGE_TYPE] = TYPE_PROMOTION;
         changes[OFFSET_BOARD_BEFORE_COUNT] = changeCount;
         changes[OFFSET_MOVE_COUNT] = pieceMoves[chessPiece.key.index];
         changes[OFFSET_MOVE_PIECE_TYPE] = chessPiece.key.index;            
         changes[OFFSET_OTHER_COUNT] = pieceMoves[takePiece.key.index];
         changes[OFFSET_OTHER_PIECE_TYPE] = takePiece.key.index;              
         changes[OFFSET_OTHER_POSITION] = move.to.index;          
      }      
      
      @Override
      public void afterMoveRevert() {
         ChessBoardCell fromCell = getBoardCell(move.from);
         ChessPiece chessPiece = fromCell.getPiece();

         if(chessPiece == null) {
            throw new IllegalStateException("Move revert of " + move + " has no piece at " + move.from);
         }
         int otherType = changes[OFFSET_OTHER_PIECE_TYPE];
         
         if(otherType != -1) {
            int otherIndex = changes[OFFSET_OTHER_POSITION];
            ChessBoardPosition otherPosition = ChessBoardPosition.at(otherIndex);
            ChessBoardCell otherCell = getBoardCell(otherPosition);
            ChessPieceKey otherKey = ChessPieceKey.resolveKey(otherType);
            ChessPiece otherPiece = otherCell.getPiece();
            
            if(otherPiece.key.index != otherType) {
               throw new IllegalStateException("Piece at " + otherPosition + " should be " + otherKey + " but is " + otherPiece.key);
            }
            pieceMoves[otherType] = changes[OFFSET_OTHER_COUNT];
         } 
         pieceMoves[chessPiece.key.index] = changes[OFFSET_MOVE_COUNT];              
      }     
   }   

   private final class ChessBoardCellUpdater implements ChessBoardModelCell {

      private final ChessBoardPosition boardPosition;
      private final ChessPiece[] localPieces; // just to avoid an invocation

      public ChessBoardCellUpdater(ChessBoardPosition boardPosition, ChessPiece[] localPieces) {
         this.boardPosition = boardPosition;
         this.localPieces = localPieces;
      }

      @Override
      public boolean isCellEmpty() {
         return boardPieces[boardPosition.index] == null;
      }

      @Override
      public void clearCell() {
         ChessPiece currentPiece = boardPieces[boardPosition.index];

         if(currentPiece != null) {
            ChessSide side = currentPiece.side;
            occupationMask[side.index] &= ~boardPosition.mask;
         }
         boardPieces[boardPosition.index] = null;
      }

      @Override
      public ChessPiece takePiece() {
         ChessPiece piece = boardPieces[boardPosition.index];

         if(piece == null) {
            throw new IllegalStateException("There is no piece on cell " + boardPosition);
         }
         ChessSide side = piece.side;
         ChessPieceType pieceType = piece.key.type;

         if(piece != null) {
            occupationMask[side.index] &= ~boardPosition.mask;
            boardPieces[boardPosition.index] = null;
         }
         if(pieceType == KING) {
            kingPositions[side.index] = null;
         }
         sidePieces[side.index][piece.key.index] = null;
         boardPositions[piece.key.index] = null;
         return piece;
      }
      
      @Override
      public ChessPiece placePiece(ChessPiece piece) {
         if(piece == null) {
            throw new NullPointerException("Can not place null on position " + boardPosition);
         }
         ChessPiece currentPiece = boardPieces[boardPosition.index];

         if(currentPiece != null) {
            throw new IllegalStateException("Can not place a piece on "+boardPosition+" it is occupied by "+currentPiece);
         }
         ChessBoardPosition previousPosition = boardPositions[piece.key.index];
         ChessSide side = piece.side;
         ChessPieceType pieceType = piece.key.type;

         if(pieceType == PAWN) {
            if(boardPosition.digit == 8 || boardPosition.digit == 1) {
               ChessPieceKey pieceKey = sparePiece(piece.key, QUEEN);

               sidePieces[side.index][piece.key.index] = null;
               boardPositions[piece.key.index] = null;
               piece = new ChessPiece(pieceKey, side);
            }
         }
         if(pieceType == KING) {
            kingPositions[side.index] = boardPosition;
         }
         if(previousPosition != null) {
            occupationMask[side.index] &= ~previousPosition.mask;
            boardPieces[previousPosition.index] = null;
         }
         sidePieces[side.index][piece.key.index] = piece;
         boardPieces[boardPosition.index] = piece;
         occupationMask[side.index] |= boardPosition.mask;
         boardPositions[piece.key.index] = boardPosition;
         pieceMoves[piece.key.index]++;
         changeCount++;

         return piece;
      }

      @Override
      public ChessPiece getPiece() {
         return localPieces[boardPosition.index]; // faster lookup as it references same thing
      }

      @Override
      public ChessBoardPosition getPosition() {
         return boardPosition;
      }

      @Override
      public String toString() {
         return String.format("%s=%s", boardPosition, getPiece());
      }
   }
}

