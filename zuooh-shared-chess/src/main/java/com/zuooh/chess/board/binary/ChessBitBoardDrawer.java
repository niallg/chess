package com.zuooh.chess.board.binary;

public class ChessBitBoardDrawer {

   public static String drawBoard(long occupationMask) {
      return new ChessBitBoard(occupationMask).toString();
   }
}
