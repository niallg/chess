package com.zuooh.chess.board;

public enum ChessBoardResult {
   WIN,
   LOSE,
   DRAW,
   REJECT,
   NONE;
   
   public boolean isWin() {
      return this == WIN;
   }
   
   public boolean isLose() {
      return this == LOSE;
   }
   
   public boolean isReject() {
      return this == REJECT;
   }   
   
   public boolean isFinal() {
      return this != NONE;
   }
}
