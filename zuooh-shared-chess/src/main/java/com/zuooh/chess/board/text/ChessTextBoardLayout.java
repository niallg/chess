package com.zuooh.chess.board.text;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;

public class ChessTextBoardLayout {
   
   private final String[][] chessSet;
   
   public ChessTextBoardLayout(String[][] chessSet) {
      if(chessSet.length != 8) {
         throw new IllegalStateException("Text set needs to be 8x8 pieces");
      }
      for(int y = 0; y < 8; y++) {
         if(chessSet[y].length != 8) {
            throw new IllegalStateException("Text set needs to be 8x8 pieces");
         }
      }
      this.chessSet = chessSet;
   }
   
   public Map<ChessBoardPosition, ChessPiece> getPieces(ChessSide side) {
      Map<ChessBoardPosition, ChessPiece> boardPieces = new HashMap<ChessBoardPosition, ChessPiece>();
      Set<ChessPieceKey> takenKeys = new HashSet<ChessPieceKey>();
      
      for(int y = 0; y < 8; y++) {
         if(chessSet[y].length != 8) {
            throw new IllegalStateException("Text set needs to be 8x8 pieces");
         }
         for(int x = 0; x < 8; x++) {
            ChessBoardPosition boardPosition = ChessBoardPosition.at(x, y);
            ChessTextPiece textPiece = ChessTextPiece.resolvePiece(chessSet[y][x]);
            
            if(textPiece != ChessTextPiece.EMPTY && side == textPiece.pieceSide) {
               ChessPieceKey pieceKey = getPieceKey(Collections.EMPTY_SET, textPiece, boardPosition);
            
               if(pieceKey != null) {
                  ChessPiece chessPiece = new ChessPiece(pieceKey, textPiece.pieceSide);
                  
                  if(takenKeys.add(pieceKey)) {
                     boardPieces.put(boardPosition, chessPiece);
                  }
               }
            }
         }
      }    
      for(int y = 0; y < 8; y++) {
         for(int x = 0; x < 8; x++) {
            ChessBoardPosition boardPosition = ChessBoardPosition.at(x, y);
            
            if(!boardPieces.containsKey(boardPosition)) {
               ChessTextPiece textPiece = ChessTextPiece.resolvePiece(chessSet[y][x]);
               
               if(textPiece != ChessTextPiece.EMPTY && side == textPiece.pieceSide) {
                  ChessPieceKey pieceKey = getPieceKey(takenKeys, textPiece, boardPosition);
                  
                  if(pieceKey == null) {
                     throw new IllegalStateException("Unable to match " + textPiece + " at " + boardPosition);
                  }            
                  ChessPiece chessPiece = new ChessPiece(pieceKey, textPiece.pieceSide);

                  boardPieces.put(boardPosition, chessPiece);                  
                  takenKeys.add(pieceKey);
               }
            }
         }         
      }
      return boardPieces;
   }
   
   private ChessPieceKey getPieceKey(Set<ChessPieceKey> takenKeys, ChessTextPiece textPiece, ChessBoardPosition boardPosition) {
      ChessPieceKey[] pieceKeys = ChessPieceKey.values();
     
      for(ChessPieceKey pieceKey : pieceKeys) {
         if(pieceKey.type == textPiece.pieceType) {
            if(pieceKey.side == textPiece.pieceSide) {
               if(pieceKey.code == boardPosition.letter) { // exact match
                  if(!takenKeys.contains(pieceKey)) {
                     return pieceKey;                
                  }
               }
            }
         }
      } 
      for(ChessPieceKey pieceKey : pieceKeys) {
         if(pieceKey.type == textPiece.pieceType) {
            if(pieceKey.side == textPiece.pieceSide) { // close match
               if(!takenKeys.contains(pieceKey)) {
                  return pieceKey;
               }
            }
         }
      }
      return null;
   }
}
