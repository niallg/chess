package com.zuooh.chess.board;

public interface ChessBoardMove {
   void makeMove();
   void makeMove(boolean validate);
   void revertMove();
}
