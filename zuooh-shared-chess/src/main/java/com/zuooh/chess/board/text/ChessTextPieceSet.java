package com.zuooh.chess.board.text;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;

public class ChessTextPieceSet implements ChessPieceSet {

   private final Map<ChessBoardPosition, ChessPiece> topToBottom;
   private final Map<ChessBoardPosition, ChessPiece> bottomToTop;

   public ChessTextPieceSet(ChessBoard chessBoard) {
      Map<ChessBoardPosition, ChessPiece> topToBottom = new HashMap<ChessBoardPosition, ChessPiece>();
      Map<ChessBoardPosition, ChessPiece> bottomToTop = new HashMap<ChessBoardPosition, ChessPiece>();

      for(int y = 0; y < 8; y++) {
         for(int x = 0; x < 8; x++) {
            ChessBoardPosition position = ChessBoardPosition.at(x, y);
            ChessPiece piece = chessBoard.getPiece(position);

            if(piece != null) {
               ChessSide playDirection = piece.getSide();

               if(playDirection == ChessSide.BLACK) {
                  topToBottom.put(position, piece);
               }else {
                  bottomToTop.put(position, piece);
               }
            }
         }
      }
      this.topToBottom = topToBottom;
      this.bottomToTop = bottomToTop;
   }

   public ChessTextPieceSet(String[][] chessSet) {
      ChessTextBoardLayout boardLayout = new ChessTextBoardLayout(chessSet);
      
      if(chessSet.length != 8) {
         throw new IllegalStateException("Text set needs to be 8x8 pieces");
      }
      this.topToBottom = boardLayout.getPieces(ChessSide.BLACK);
      this.bottomToTop = boardLayout.getPieces(ChessSide.WHITE);
   }

   @Override
   public Map<ChessBoardPosition, ChessPiece> getPieces(ChessSide playDirection) {
      if(playDirection == ChessSide.BLACK) {
         return topToBottom;
      }
      return bottomToTop;
   }

   public String drawBoard() {
      StringWriter writer = new StringWriter();
      PrintWriter printer = new PrintWriter(writer);

      drawBoard(printer);

      return writer.toString();
   }

   private void drawBoard(PrintWriter writer) {
      drawLetters(writer);
      drawRow(writer);

      for(int y = 0; y < 8; y++) {
         writer.print(8 - y);
         writer.print(" | ");

         for(int x = 0; x < 8; x++) {
            ChessBoardPosition boardPosition = ChessBoardPosition.at(x, y);
            drawCell(writer, boardPosition);
         }
         drawRow(writer);
      }
      drawBottom(writer);
   }

   private void drawCell(PrintWriter writer, ChessBoardPosition position) {
      ChessPiece chessPiece = topToBottom.get(position);

      if(chessPiece == null) {
         chessPiece = bottomToTop.get(position);
      }
      ChessTextPiece textPiece = ChessTextPiece.resolvePiece(chessPiece);

      writer.print(textPiece.code);
      writer.print(" | ");
   }

   private void drawLetters(PrintWriter writer) {
      writer.println();
      writer.print("  ");

      for(int i = 0; i < 8; i++) {
         int letter = 'A' + i;
         writer.print("  ");
         writer.print((char)letter);
         writer.print("  ");
      }
      writer.flush();
   }

   private void drawRow(PrintWriter writer) {
      writer.println();
      writer.print("  +");

      for(int i = 0; i < 8; i++) {
         writer.print("----+");
      }
      writer.println();
      writer.flush();
   }
   
   private void drawBottom(PrintWriter writer) {
      writer.println();
   }

   @Override
   public String toString() {
      return drawBoard();
   }
}
