package com.zuooh.chess.board;

import java.util.Map;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;

public interface ChessPieceSet {
   Map<ChessBoardPosition, ChessPiece> getPieces(ChessSide side);
}
