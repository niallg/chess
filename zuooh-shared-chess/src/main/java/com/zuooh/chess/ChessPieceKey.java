package com.zuooh.chess;

import static com.zuooh.chess.ChessPieceType.BISHOP;
import static com.zuooh.chess.ChessPieceType.KING;
import static com.zuooh.chess.ChessPieceType.KNIGHT;
import static com.zuooh.chess.ChessPieceType.PAWN;
import static com.zuooh.chess.ChessPieceType.QUEEN;
import static com.zuooh.chess.ChessPieceType.ROOK;
import static com.zuooh.chess.ChessSide.BLACK;
import static com.zuooh.chess.ChessSide.WHITE;

public enum ChessPieceKey {
   BLACK_PAWN_A('A', 0, false, PAWN, BLACK),
   BLACK_PAWN_B('B', 1, false, PAWN, BLACK),
   BLACK_PAWN_C('C', 2, false, PAWN, BLACK),
   BLACK_PAWN_D('D', 3, false, PAWN, BLACK),
   BLACK_PAWN_E('E', 4, false, PAWN, BLACK),
   BLACK_PAWN_F('F', 5, false, PAWN, BLACK),
   BLACK_PAWN_G('G', 6, false, PAWN, BLACK),
   BLACK_PAWN_H('H', 7, false, PAWN, BLACK),
   BLACK_ROOK_A('A', 8, false, ROOK, BLACK),
   BLACK_ROOK_H('H', 9, false, ROOK, BLACK),
   BLACK_KNIGHT_B('B', 10, false, KNIGHT, BLACK),
   BLACK_KNIGHT_G('G', 11, false, KNIGHT, BLACK),
   BLACK_BISHOP_C('C', 12, false, BISHOP, BLACK),
   BLACK_BISHOP_F('F', 13, false, BISHOP, BLACK),
   BLACK_QUEEN('D', 14, false, QUEEN, BLACK),
   BLACK_KING('E', 15, false, KING, BLACK),
   WHITE_PAWN_A('A', 16, false, PAWN, WHITE),
   WHITE_PAWN_B('B', 17, false, PAWN, WHITE),
   WHITE_PAWN_C('C', 18, false, PAWN, WHITE),
   WHITE_PAWN_D('D', 19, false, PAWN, WHITE),
   WHITE_PAWN_E('E', 20, false, PAWN, WHITE),
   WHITE_PAWN_F('F', 21, false, PAWN, WHITE),
   WHITE_PAWN_G('G', 22, false, PAWN, WHITE),
   WHITE_PAWN_H('H', 23, false, PAWN, WHITE),
   WHITE_ROOK_A('A', 24, false, ROOK, WHITE),
   WHITE_ROOK_H('H', 25, false, ROOK, WHITE),
   WHITE_KNIGHT_B('B', 26, false, KNIGHT, WHITE),
   WHITE_KNIGHT_G('G', 27, false, KNIGHT, WHITE),
   WHITE_BISHOP_C('C', 28, false, BISHOP, WHITE),
   WHITE_BISHOP_F('F', 29, false, BISHOP, WHITE),
   WHITE_QUEEN('D', 30, false, QUEEN, WHITE),
   WHITE_KING('E', 31, false, KING, WHITE),
   BLACK_SPARE_QUEEN_A('A', 32, true, QUEEN, BLACK),
   BLACK_SPARE_QUEEN_B('B', 33, true, QUEEN, BLACK),
   BLACK_SPARE_QUEEN_C('C', 34, true, QUEEN, BLACK),
   BLACK_SPARE_QUEEN_D('D', 35, true, QUEEN, BLACK),
   BLACK_SPARE_QUEEN_E('E', 36, true, QUEEN, BLACK),
   BLACK_SPARE_QUEEN_F('F', 37, true, QUEEN, BLACK),
   BLACK_SPARE_QUEEN_G('G', 38, true, QUEEN, BLACK),
   BLACK_SPARE_QUEEN_H('H', 39, true, QUEEN, BLACK),
   BLACK_SPARE_ROOK_A('A', 40, true, ROOK, BLACK),
   BLACK_SPARE_ROOK_B('B', 41, true, ROOK, BLACK),
   BLACK_SPARE_ROOK_C('C', 42, true, ROOK, BLACK),
   BLACK_SPARE_ROOK_D('D', 43, true, ROOK, BLACK),
   BLACK_SPARE_ROOK_E('E', 44, true, ROOK, BLACK),
   BLACK_SPARE_ROOK_F('F', 45, true, ROOK, BLACK),
   BLACK_SPARE_ROOK_G('G', 46, true, ROOK, BLACK),
   BLACK_SPARE_ROOK_H('H', 47, true, ROOK, BLACK),
   BLACK_SPARE_KNIGHT_A('A', 48, true, KNIGHT, BLACK),
   BLACK_SPARE_KNIGHT_B('B', 49, true, KNIGHT, BLACK),
   BLACK_SPARE_KNIGHT_C('C', 50, true, KNIGHT, BLACK),
   BLACK_SPARE_KNIGHT_D('D', 51, true, KNIGHT, BLACK),
   BLACK_SPARE_KNIGHT_E('E', 52, true, KNIGHT, BLACK),
   BLACK_SPARE_KNIGHT_F('F', 53, true, KNIGHT, BLACK),
   BLACK_SPARE_KNIGHT_G('G', 54, true, KNIGHT, BLACK),
   BLACK_SPARE_KNIGHT_H('H', 55, true, KNIGHT, BLACK),
   BLACK_SPARE_BISHOP_A('A', 56, true, BISHOP, BLACK),
   BLACK_SPARE_BISHOP_B('B', 57, true, BISHOP, BLACK),
   BLACK_SPARE_BISHOP_C('C', 58, true, BISHOP, BLACK),
   BLACK_SPARE_BISHOP_D('D', 59, true, BISHOP, BLACK),
   BLACK_SPARE_BISHOP_E('E', 60, true, BISHOP, BLACK),
   BLACK_SPARE_BISHOP_F('F', 61, true, BISHOP, BLACK),
   BLACK_SPARE_BISHOP_G('G', 62, true, BISHOP, BLACK),
   BLACK_SPARE_BISHOP_H('H', 63, true, BISHOP, BLACK),
   WHITE_SPARE_QUEEN_A('A', 64, true, QUEEN, WHITE),
   WHITE_SPARE_QUEEN_B('B', 65, true, QUEEN, WHITE),
   WHITE_SPARE_QUEEN_C('C', 66, true, QUEEN, WHITE),
   WHITE_SPARE_QUEEN_D('D', 67, true, QUEEN, WHITE),
   WHITE_SPARE_QUEEN_E('E', 68, true, QUEEN, WHITE),
   WHITE_SPARE_QUEEN_F('F', 69, true, QUEEN, WHITE),
   WHITE_SPARE_QUEEN_G('G', 70, true, QUEEN, WHITE),
   WHITE_SPARE_QUEEN_H('H', 71, true, QUEEN, WHITE),
   WHITE_SPARE_ROOK_A('A', 72, true, ROOK, WHITE),
   WHITE_SPARE_ROOK_B('B', 73, true, ROOK, WHITE),
   WHITE_SPARE_ROOK_C('C', 74, true, ROOK, WHITE),
   WHITE_SPARE_ROOK_D('D', 75, true, ROOK, WHITE),
   WHITE_SPARE_ROOK_E('E', 76, true, ROOK, WHITE),
   WHITE_SPARE_ROOK_F('F', 77, true, ROOK, WHITE),
   WHITE_SPARE_ROOK_G('G', 78, true, ROOK, WHITE),
   WHITE_SPARE_ROOK_H('H', 79, true, ROOK, WHITE),
   WHITE_SPARE_KNIGHT_A('A', 80, true, KNIGHT, WHITE),
   WHITE_SPARE_KNIGHT_B('B', 81, true, KNIGHT, WHITE),
   WHITE_SPARE_KNIGHT_C('C', 82, true, KNIGHT, WHITE),
   WHITE_SPARE_KNIGHT_D('D', 83, true, KNIGHT, WHITE),
   WHITE_SPARE_KNIGHT_E('E', 84, true, KNIGHT, WHITE),
   WHITE_SPARE_KNIGHT_F('F', 85, true, KNIGHT, WHITE),
   WHITE_SPARE_KNIGHT_G('G', 86, true, KNIGHT, WHITE),
   WHITE_SPARE_KNIGHT_H('H', 87, true, KNIGHT, WHITE),
   WHITE_SPARE_BISHOP_A('A', 88, true, BISHOP, WHITE),
   WHITE_SPARE_BISHOP_B('B', 89, true, BISHOP, WHITE),
   WHITE_SPARE_BISHOP_C('C', 90, true, BISHOP, WHITE),
   WHITE_SPARE_BISHOP_D('D', 91, true, BISHOP, WHITE),
   WHITE_SPARE_BISHOP_E('E', 92, true, BISHOP, WHITE),
   WHITE_SPARE_BISHOP_F('F', 93, true, BISHOP, WHITE),
   WHITE_SPARE_BISHOP_G('G', 94, true, BISHOP, WHITE),
   WHITE_SPARE_BISHOP_H('H', 95, true, BISHOP, WHITE);

   public final ChessPieceType type;
   public final ChessSide side;
   public final boolean spare;
   public final char code;
   public final int index;

   private ChessPieceKey(char code, int index, boolean spare, ChessPieceType type, ChessSide side) {
      this.side = side;
      this.type = type;
      this.spare = spare;
      this.index = index;
      this.code = code;
   }
   
   public ChessPiece createPiece(ChessSide side) {
      return new ChessPiece(this, side);
   }

   public static ChessPieceKey spareQueen(ChessPieceKey pawnKey) {
      return sparePiece(pawnKey, QUEEN);
   }

   public static ChessPieceKey spareKnight(ChessPieceKey pawnKey) {
      return sparePiece(pawnKey, KNIGHT);
   }

   public static ChessPieceKey spareBishop(ChessPieceKey pawnKey) {
      return sparePiece(pawnKey, BISHOP);
   }

   public static ChessPieceKey spareRook(ChessPieceKey pawnKey) {
      return sparePiece(pawnKey, ROOK);
   }

   public static ChessPieceKey sparePiece(ChessPieceKey pawnKey, ChessPieceType spareType) {
      if(pawnKey.type != PAWN) {
         throw new IllegalArgumentException("Can no convert type " + pawnKey.type + " to a queen");
      }
      ChessPieceKey[] pieceKeys = values();
      
      for(ChessPieceKey pieceKey : pieceKeys) {
         if(pieceKey.spare) {
            if(pieceKey.type == spareType) {
               if(pieceKey.code == pawnKey.code) {
                  if(pawnKey.side == pieceKey.side) {
                     return pieceKey;
                  }
               }
            }
         }
      }
      return null;
   }
   
   public static ChessPieceKey resolveKey(int index) {
      if(index < 0 || index >= KEYS.length) {
         return null;
      }
      return KEYS[index];
   }

   public static ChessPieceKey resolveKey(ChessBoardPosition boardPosition, ChessPieceType pieceType, ChessSide pieceSide) {
      ChessPieceKey[] pieceKeys = values();
      
      for(ChessPieceKey pieceKey : pieceKeys) {
         if(pieceKey.type == pieceType) {
            if(pieceKey.side == pieceSide) {
               if(pieceKey.code == boardPosition.letter) { // is this needed??
                  return pieceKey;
               }
            }
         }
      }
      throw new IllegalStateException("No piece could be resolved for " + boardPosition + " of type " + pieceType);
   }
   
   private static final ChessPieceKey[] KEYS = {
      BLACK_PAWN_A,
      BLACK_PAWN_B,
      BLACK_PAWN_C,
      BLACK_PAWN_D,
      BLACK_PAWN_E,
      BLACK_PAWN_F,
      BLACK_PAWN_G,
      BLACK_PAWN_H,
      BLACK_ROOK_A,
      BLACK_ROOK_H,
      BLACK_KNIGHT_B,
      BLACK_KNIGHT_G,
      BLACK_BISHOP_C,
      BLACK_BISHOP_F,
      BLACK_QUEEN,
      BLACK_KING,
      WHITE_PAWN_A,
      WHITE_PAWN_B,
      WHITE_PAWN_C,
      WHITE_PAWN_D,
      WHITE_PAWN_E,
      WHITE_PAWN_F,
      WHITE_PAWN_G,
      WHITE_PAWN_H,
      WHITE_ROOK_A,
      WHITE_ROOK_H,
      WHITE_KNIGHT_B,
      WHITE_KNIGHT_G,
      WHITE_BISHOP_C,
      WHITE_BISHOP_F,
      WHITE_QUEEN,
      WHITE_KING,
      BLACK_SPARE_QUEEN_A,
      BLACK_SPARE_QUEEN_B,
      BLACK_SPARE_QUEEN_C,
      BLACK_SPARE_QUEEN_D,
      BLACK_SPARE_QUEEN_E,
      BLACK_SPARE_QUEEN_F,
      BLACK_SPARE_QUEEN_G,
      BLACK_SPARE_QUEEN_H,
      BLACK_SPARE_ROOK_A,
      BLACK_SPARE_ROOK_B,
      BLACK_SPARE_ROOK_C,
      BLACK_SPARE_ROOK_D,
      BLACK_SPARE_ROOK_E,
      BLACK_SPARE_ROOK_F,
      BLACK_SPARE_ROOK_G,
      BLACK_SPARE_ROOK_H,
      BLACK_SPARE_KNIGHT_A,
      BLACK_SPARE_KNIGHT_B,
      BLACK_SPARE_KNIGHT_C,
      BLACK_SPARE_KNIGHT_D,
      BLACK_SPARE_KNIGHT_E,
      BLACK_SPARE_KNIGHT_F,
      BLACK_SPARE_KNIGHT_G,
      BLACK_SPARE_KNIGHT_H,
      BLACK_SPARE_BISHOP_A,
      BLACK_SPARE_BISHOP_B,
      BLACK_SPARE_BISHOP_C,
      BLACK_SPARE_BISHOP_D,
      BLACK_SPARE_BISHOP_E,
      BLACK_SPARE_BISHOP_F,
      BLACK_SPARE_BISHOP_G,
      BLACK_SPARE_BISHOP_H,
      WHITE_SPARE_QUEEN_A,
      WHITE_SPARE_QUEEN_B,
      WHITE_SPARE_QUEEN_C,
      WHITE_SPARE_QUEEN_D,
      WHITE_SPARE_QUEEN_E,
      WHITE_SPARE_QUEEN_F,
      WHITE_SPARE_QUEEN_G,
      WHITE_SPARE_QUEEN_H,
      WHITE_SPARE_ROOK_A,
      WHITE_SPARE_ROOK_B,
      WHITE_SPARE_ROOK_C,
      WHITE_SPARE_ROOK_D,
      WHITE_SPARE_ROOK_E,
      WHITE_SPARE_ROOK_F,
      WHITE_SPARE_ROOK_G,
      WHITE_SPARE_ROOK_H,
      WHITE_SPARE_KNIGHT_A,
      WHITE_SPARE_KNIGHT_B,
      WHITE_SPARE_KNIGHT_C,
      WHITE_SPARE_KNIGHT_D,
      WHITE_SPARE_KNIGHT_E,
      WHITE_SPARE_KNIGHT_F,
      WHITE_SPARE_KNIGHT_G,
      WHITE_SPARE_KNIGHT_H,
      WHITE_SPARE_BISHOP_A,
      WHITE_SPARE_BISHOP_B,
      WHITE_SPARE_BISHOP_C,
      WHITE_SPARE_BISHOP_D,
      WHITE_SPARE_BISHOP_E,
      WHITE_SPARE_BISHOP_F,
      WHITE_SPARE_BISHOP_G,
      WHITE_SPARE_BISHOP_H      
   };
}
