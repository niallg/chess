package com.zuooh.chess;

public enum ChessCellStatus {
   SHADOW,
   UNDER_ATTACK,
   SELECTED,
   TAKEN,
   NORMAL;
}
