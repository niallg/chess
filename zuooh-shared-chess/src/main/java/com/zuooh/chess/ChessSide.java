package com.zuooh.chess;

import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessNullMove;

public enum ChessSide {
   WHITE("white", 0),
   BLACK("black", 1);

   public final String name;
   public final int index;

   private ChessSide(String name, int index) {
      this.index = index;
      this.name = name;
   }   

   public ChessSide oppositeSide() {
      if(this == WHITE) {
         return BLACK;
      }
      return WHITE;
   }
   
   public ChessMove createMove() {
      return new ChessNullMove(this, 0);
   }
   
   public ChessMove createMove(int change) {
      return new ChessNullMove(this, change);
   }    
   
   public ChessMove createMove(ChessBoardPosition from, ChessBoardPosition to) {
      return new ChessMove(from, to, this, 0);
   }
   
   public ChessMove createMove(ChessBoardPosition from, ChessBoardPosition to, int change) {
      return new ChessMove(from, to, this, change);
   }

   public static ChessSide resolveSide(String token) {       
      if(WHITE.name.equalsIgnoreCase(token)) {
         return WHITE;
      }
      if(BLACK.name.equalsIgnoreCase(token)) {
         return BLACK;
      }
      return null;
   }
}
