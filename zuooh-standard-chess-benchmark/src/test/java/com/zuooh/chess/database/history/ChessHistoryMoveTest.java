package com.zuooh.chess.database.history;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.database.SingleTableDatabaseBinder;
import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.bind.DatabaseBinder;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.bind.table.statement.CreateStatement;
import com.zuooh.database.bind.table.statement.DropStatement;
import com.zuooh.database.build.dom.element.TableBinderElement;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;
import com.zuooh.database.standard.StandardDatabase;

public class ChessHistoryMoveTest extends TestCase {

   private static final String SOURCE =
   "<table name='gameHistory' type='com.zuooh.chess.database.history.ChessHistoryItem'>\r\n"+
   "  <schema>\r\n"+
   "    <key>\r\n"+
   "      <column name='gameId' />\r\n"+
   "      <column name='chessMoveChangeCount' />\r\n"+
   "    </key>\r\n"+
   "    <column name='status' constraint='required' />\r\n"+
   "    <column name='timeStamp' constraint='required' />\r\n"+
   "  </schema>\r\n"+
   "</table>\r\n";
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }   
   
   public void testChessHistory() throws Exception {
      Persister persister = new Persister();
      TableBinderBuilder builder = persister.read(TableBinderBuilder.class, SOURCE);
      Database database = new StandardDatabase("c:\\work\\temp\\database",  "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      TableBuilder binder = new AttributeTableBuilder(database);
      List<String> script = new LinkedList<String>();      

      TableBinderElement tableElement = builder.createElement();
      TableDefinition tableDefinition = tableElement.createTable(binder);
      TableBinder table = tableDefinition.createBinder(binder, script);
      DropStatement drop = table.dropIfExists();      
      CreateStatement create = table.create();
      
      drop.execute();
      create.execute();
      
      for(String line : script) {
         DatabaseConnection connection = database.getConnection();
         
         try {
            connection.executeStatement(line);
         } finally {
            connection.closeConnection();
         }
      }

      DatabaseBinder databaseBinder = new SingleTableDatabaseBinder(database, table, ChessHistoryItem.class);
      ChessHistory history = new ChessHistoryDatabase(databaseBinder);
      
      ChessHistoryMove move1 = new ChessHistoryMove(ChessBoardPosition.E2, ChessBoardPosition.E4, null, ChessPieceKey.WHITE_PAWN_E, null, ChessSide.WHITE, 1);
      ChessHistoryItem item1 = new ChessHistoryItem(move1, ChessBoardStatus.NORMAL, "game.1", 0);
      
      ChessHistoryMove move2 = new ChessHistoryMove(ChessBoardPosition.H7, ChessBoardPosition.H6, null, ChessPieceKey.BLACK_PAWN_H, null, ChessSide.BLACK, 2);
      ChessHistoryItem item2 = new ChessHistoryItem(move2, ChessBoardStatus.NORMAL, "game.1", 1000); // advance one move and 1 second
      
      history.saveHistoryItem(item1);
      history.saveHistoryItem(item2);
      
      ChessHistoryItem lastItem = history.loadLastHistoryItem("game.1");
      
      assertEquals(lastItem.status, item2.status);
      assertEquals(lastItem.chessMove.fromPiece, item2.chessMove.fromPiece);
      
      ChessBoard chessBoard = history.loadHistoryAsBoard("game.1");
      ChessTextPieceSet textPieceSet = new ChessTextPieceSet(chessBoard);      
      String board = textPieceSet.drawBoard();
      
      System.err.println(board);
   }
}
