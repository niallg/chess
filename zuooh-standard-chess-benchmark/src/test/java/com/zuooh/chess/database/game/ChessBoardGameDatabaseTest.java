package com.zuooh.chess.database.game;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.SingleTableDatabaseBinder;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.bind.DatabaseBinder;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.bind.table.statement.CreateStatement;
import com.zuooh.database.bind.table.statement.DropStatement;
import com.zuooh.database.build.dom.element.TableBinderElement;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;
import com.zuooh.database.standard.StandardDatabase;

public class ChessBoardGameDatabaseTest extends TestCase {
   
   private static final String SOURCE =
   "<table name='boardGame' type='com.zuooh.chess.database.board.ChessBoardGame'>\r\n"+
   "  <schema>\r\n"+
   "    <key>\r\n"+
   "      <column name='gameId' />\r\n"+
   "    </key>\r\n"+
   "    <column name='userId' constraint='required'/>\r\n"+
   "    <column name='boardStatus' constraint='required'/>\r\n"+
   "    <column name='boardResult' constraint='required'/>\r\n"+
   "    <column name='timeStamp' default='time'/>\r\n"+     
   "  </schema>\r\n"+
   "  <insert>\r\n"+
   "     <row>\r\n"+
   "        <value column=\"gameId\">game.1</value>\r\n"+
   "        <value column=\"userId\">gallagher_niall@yahoo.com</value>\r\n"+
   "        <value column=\"boardStatus\">NORMAL</value>\r\n"+
   "        <value column=\"boardResult\">NONE</value>\r\n"+
   "        <value column=\"selectedSide\">BLACK</value>\r\n"+
   "        <value column=\"gameDuration\">60000</value>\r\n"+
   "        <value column=\"themeColor\">blue</value>\r\n"+
   "        <value column=\"opponentId\">jdoe@gmail.com</value>\r\n"+
   "        <value column=\"onlineGame\">true</value>\r\n"+   
   "     </row>\r\n"+
   "  </insert>\r\n"+
   "</table>\r\n";
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");   
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }   
   
   public void testProfileDatabase() throws Exception {
      Persister persister = new Persister();
      TableBinderBuilder builder = persister.read(TableBinderBuilder.class, SOURCE);
      Database database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      TableBuilder binder = new AttributeTableBuilder(database);
      List<String> script = new LinkedList<String>();
      
      TableBinderElement tableElement = builder.createElement();
      TableDefinition tableDefinition = tableElement.createTable(binder);
      TableBinder table = tableDefinition.createBinder(binder, script);
      DropStatement drop = table.dropIfExists();
      String dropStatement = drop.compile();
      
      System.err.println(dropStatement); 
      
      CreateStatement create = table.create();
      String createStatement = create.compile();
      
      System.err.println(createStatement);
      
      drop.execute();
      create.execute();      
     
      System.err.println(SOURCE);
      System.err.println();
      
      for(String line : script) {
         DatabaseConnection connection = database.getConnection();
         
         try {
            connection.executeStatement(line);
         } finally {
            connection.closeConnection();
         }
      }
      
      DatabaseBinder databaseBinder = new SingleTableDatabaseBinder(database, table, ChessBoardGame.class);
      ChessBoardGameDatabase gameDatabase = new ChessBoardGameDatabase(databaseBinder);      
      
      ChessBoardGame game = gameDatabase.loadBoardGame("game.1");
      
      assertNotNull(game);
      assertEquals(game.getGameId(), "game.1");
      assertEquals(game.getOpponentId(), "jdoe@gmail.com");
      assertEquals(game.getUserId(), "gallagher_niall@yahoo.com");
      assertEquals(game.getGameDuration(), 60000);
      assertEquals(game.getUserSide(), ChessSide.BLACK);
      
      game.setGameDuration(15000);
      game.setUserSide(ChessSide.WHITE);
      game.setOpponentId("tom@hotmail.com");
      gameDatabase.saveBoardGame(game);

      
      game = gameDatabase.loadBoardGame("game.1");
      
      assertNotNull(game);
      assertEquals(game.getGameId(), "game.1");
      assertEquals(game.getOpponentId(), "tom@hotmail.com");
      assertEquals(game.getUserId(), "gallagher_niall@yahoo.com");
      assertEquals(game.getGameDuration(), 15000);
      assertEquals(game.getUserSide(), ChessSide.WHITE);
      
   }
}
