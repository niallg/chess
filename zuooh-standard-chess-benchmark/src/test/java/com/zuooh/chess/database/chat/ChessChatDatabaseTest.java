package com.zuooh.chess.database.chat;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.chess.database.SingleTableDatabaseBinder;
import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.bind.DatabaseBinder;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.bind.table.statement.CreateStatement;
import com.zuooh.database.bind.table.statement.DropStatement;
import com.zuooh.database.build.dom.element.TableBinderElement;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;
import com.zuooh.database.standard.StandardDatabase;

public class ChessChatDatabaseTest extends TestCase {

   private static final String SOURCE =
   "<table name='chat' type='com.zuooh.chess.database.chat.ChessChat'>\n"+
   "  <schema>\n"+
   "    <key>\n"+
   "      <column name='chatId' />\n"+
   "    </key>\n"+
   "    <column name='gameId' />\n"+
   "    <column name='fromId' />\n"+
   "    <column name='toId' />\n"+
   "    <column name='status' constraint='required' />\n"+
   "    <column name='message' constraint='required' />\n"+
   "  </schema>\n"+
   "</table>";
   
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }
   
   public void testChatDatabase() throws Exception {
      Persister persister = new Persister();
      TableBinderBuilder builder = persister.read(TableBinderBuilder.class, SOURCE);
      Database database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");;
      TableBuilder binder = new AttributeTableBuilder(database);
      List<String> script = new LinkedList<String>();
      TableBinderElement tableElement = builder.createElement();
      TableDefinition tableDefinition = tableElement.createTable(binder);      
      TableBinder table = tableDefinition.createBinder(binder, script);  
      
      System.err.println(SOURCE);
      System.err.println();
      
      DropStatement drop = table.dropIfExists();
      String dropStatement = drop.compile();
      
      System.err.println(dropStatement); 
      
      CreateStatement create = table.create();
      String createStatement = create.compile();
      
      System.err.println(createStatement);
      
      drop.execute();
      create.execute();      
     
      System.err.println(SOURCE);
      System.err.println();
      
      for(String line : script) {
         DatabaseConnection connection = database.getConnection();
         
         try {
            connection.executeStatement(line);
         } finally {
            connection.closeConnection();
         }
      }
      
      DatabaseBinder databaseBinder = new SingleTableDatabaseBinder(database, table, ChessChat.class);
      ChessChatDatabase chatDatabase = new ChessChatDatabase(databaseBinder);
      ChessChat chat1 = new ChessChat(ChessChatStatus.UNREAD, "1", "game1", "tom", "pat", "Hi pat it is tom", -1);
      ChessChat chat2 = new ChessChat(ChessChatStatus.UNREAD, "2", "game1", "pat", "tom", "Hey tom", -1);
      ChessChat chat3 = new ChessChat(ChessChatStatus.UNREAD, "3", "game1", "tom", "pat", "Whats happening??", -1);
      
      chatDatabase.saveMessage(chat1);
      chatDatabase.saveMessage(chat2);
      chatDatabase.saveMessage(chat3);
      
      List<ChessChat> messages = chatDatabase.listNewMessages("game1");
      
      assertFalse(messages.isEmpty());
      assertEquals(messages.size(), 3);
      assertEquals(messages.get(0).getFrom(), "tom");
      assertEquals(messages.get(0).getTo(), "pat");
      assertEquals(messages.get(0).getChatId(), "1");
      assertEquals(messages.get(1).getFrom(), "pat");
      assertEquals(messages.get(1).getTo(), "tom");
      assertEquals(messages.get(1).getChatId(), "2");
      assertEquals(messages.get(2).getFrom(), "tom");
      assertEquals(messages.get(2).getTo(), "pat");
      assertEquals(messages.get(2).getChatId(), "3");      
      
      chatDatabase.markMessageAsRead("1");
      
      messages = chatDatabase.listNewMessages("game1");
      
      assertFalse(messages.isEmpty());
      assertEquals(messages.size(), 2);
      assertEquals(messages.get(0).getFrom(), "pat");
      assertEquals(messages.get(0).getTo(), "tom");
      assertEquals(messages.get(0).getChatId(), "2");
      assertEquals(messages.get(1).getFrom(), "tom");
      assertEquals(messages.get(1).getTo(), "pat");
      assertEquals(messages.get(1).getChatId(), "3");  
      
      messages = chatDatabase.listAllMessages("game1");
      
      assertFalse(messages.isEmpty());
      assertEquals(messages.size(), 3);
      assertEquals(messages.get(0).getFrom(), "tom");
      assertEquals(messages.get(0).getTo(), "pat");
      assertEquals(messages.get(0).getChatId(), "1");
      assertEquals(messages.get(1).getFrom(), "pat");
      assertEquals(messages.get(1).getTo(), "tom");
      assertEquals(messages.get(1).getChatId(), "2");
      assertEquals(messages.get(2).getFrom(), "tom");
      assertEquals(messages.get(2).getTo(), "pat");
      assertEquals(messages.get(2).getChatId(), "3");  
      
      chatDatabase.deleteMessage("1");
      chatDatabase.deleteMessage("2");
      
      messages = chatDatabase.listAllMessages("game1");
      
      assertFalse(messages.isEmpty());
      assertEquals(messages.size(), 1);
      assertEquals(messages.get(0).getFrom(), "tom");
      assertEquals(messages.get(0).getTo(), "pat");
      assertEquals(messages.get(0).getChatId(), "3"); 
      
   }
}
