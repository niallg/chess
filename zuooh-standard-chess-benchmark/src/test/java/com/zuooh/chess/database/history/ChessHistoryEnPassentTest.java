package com.zuooh.chess.database.history;

import java.io.File;
import java.io.FileNotFoundException;

import junit.framework.TestCase;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class ChessHistoryEnPassentTest extends TestCase {
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }   
   
   public void testEnPassentInHistory() throws Exception {
      ChessHistory history = ChessHistoryDatabaseTest.createEmptyDatabase();
      int counter = 0;
      
      dumpHistory(history, "game.X");
      makeMove(history, "E2", "E4", "WHITE", "game.X", counter++);
      makeMove(history, "D7", "D5", "BLACK", "game.X", counter++);
      
      makeMove(history, "E4", "E5", "WHITE", "game.X", counter++);
      makeMove(history, "F7", "F5", "BLACK", "game.X", counter++);
      
      makeMove(history, "E5", "F6", "WHITE", "game.X", counter++);
      makeMove(history, "A7", "A6", "BLACK", "game.X", counter++);
   
      ChessHistoryScroller scroller = new ChessHistoryScroller(history);
      System.err.println("BEFORE");
      dumpHistory(history, "game.X"); 
      scroller.scrollBack("game.X", ChessSide.WHITE);
      System.err.println("AFTER");
      dumpHistory(history, "game.X");
      scroller.scrollBack("game.X", ChessSide.BLACK);
      System.err.println("AFTER");
      dumpHistory(history, "game.X");
   }
   
   public void makeMove(ChessHistory history, String from, String to, String color, String gameId, int count) throws Exception {
      ChessMove move = new ChessMove(ChessBoardPosition.at(from), ChessBoardPosition.at(to), ChessSide.resolveSide(color), count);
      
      history.saveHistoryItem("game.X", move);
      dumpHistory(history, "game.X");   
   }
   
   public void dumpHistory(ChessHistory history, String gameId) throws Exception {
      ChessBoard currentBoard = history.loadHistoryAsBoard(gameId);
      ChessTextPieceSet textPieceSet = new ChessTextPieceSet(currentBoard);
      
      System.err.println(textPieceSet.drawBoard());
      
      if(history.containsHistory(gameId)) {
         System.err.println("NORTH "+history.loadLastHistoryItem(gameId, ChessSide.WHITE));
         System.err.println("SOUTH "+history.loadLastHistoryItem(gameId, ChessSide.BLACK));
         System.err.println(currentBoard.getKingPosition(history.loadLastHistoryItem(gameId).chessMove.side));
         System.err.println(currentBoard.getKingPosition(history.loadLastHistoryItem(gameId).chessMove.side.oppositeSide()));
      }
   }
}
