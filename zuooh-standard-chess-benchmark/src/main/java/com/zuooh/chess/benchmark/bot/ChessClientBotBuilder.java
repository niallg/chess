package com.zuooh.chess.benchmark.bot;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.client.engine.ChessGameObserver;
import com.zuooh.chess.client.engine.ChessMoveListener;
import com.zuooh.chess.client.engine.ChessRemoteGameManager;
import com.zuooh.chess.client.engine.ChessRemoteGameMoveEngine;
import com.zuooh.chess.client.engine.ChessRemotePlayer;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessMoveEngineProvider;
import com.zuooh.chess.engine.ChessMoveResult;

public class ChessClientBotBuilder {

   private static final String[][] SET = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};

   private final ChessMoveEngineProvider engineProvider;
   private final ChessRemoteGameManager gameManager;

   public ChessClientBotBuilder(ChessRemoteGameManager gameManager, ChessMoveEngineProvider engineProvider) {
      this.engineProvider = engineProvider;
      this.gameManager = gameManager;
   }

   public ChessClientBot createNewBot(ChessClientBotListener botListener, ChessClientBotCriteria botCriteria) throws Exception {
      long gameDuration = botCriteria.getGameDuration();
      int botSkill = botCriteria.getBotSkill();
      String gameId = botCriteria.getGameId();
      String boardSet = botCriteria.getBoardSet();
      String botName = botCriteria.getBotId();      
      ChessSide botSide = botCriteria.getBotSide();
      ChessRemotePlayer opponent = null;
      
      if(botCriteria.isCreate()) {
         opponent = gameManager.createNewGame(gameId, null, botSide, boardSet, gameDuration);
      } else {
         opponent = gameManager.acceptGame(gameId); // game was matched
      }
      ChessPieceSet chessSet = createChessSet(botName, gameId, botSide);
      ChessMoveEngine computerEngine = engineProvider.createEngine(botSide, botSkill);
      ChessRemoteGameMoveEngine opponentEngine = opponent.getMoveEngine();
      
      return createClientBot(botListener, opponentEngine, computerEngine, chessSet, gameId);      
   }

   private ChessClientBot createClientBot(ChessClientBotListener botListener, ChessRemoteGameMoveEngine opponentEngine, ChessMoveEngine computerEngine, ChessPieceSet chessSet, String gameId) {
      ChessClientBot clientBot = new ChessClientBot(botListener, opponentEngine, computerEngine, chessSet, gameId);
      ChessBotMoveNotifier botNotifier = new ChessBotMoveNotifier(clientBot);
      
      opponentEngine.setMoveListener(botNotifier);
      gameManager.setGameObserver(gameId, botNotifier);
      
      return clientBot;
   }  

   private ChessPieceSet createChessSet(String botName, String gameId, ChessSide playDirection) {
      return new ChessTextPieceSet(SET);
   }
   
   private class ChessBotMoveNotifier implements ChessMoveListener, ChessGameObserver {
      
      private final ChessClientBot bot;
      
      public ChessBotMoveNotifier(ChessClientBot bot) {
         this.bot = bot;
      }

      @Override
      public void onMoveMade(ChessMove moveMade) {
         bot.moveConfirmed(moveMade);
      }

      @Override
      public void onMoveResult(ChessMoveResult moveResult) {
         bot.moveResponse(moveResult);
      }

      @Override
      public void onGameUpdate(ChessGame game) {        
         bot.gameUpdate(game);
      }      
   }
}
