package com.zuooh.chess.benchmark;

import java.util.Arrays;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.event.operation.ChessSignInCommand;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.common.task.Task;
import com.zuooh.common.task.TaskScheduler;
import com.zuooh.message.client.ResponseStatus;

public class ChessBenchmarkTrigger {

   private static final Logger LOG = LoggerFactory.getLogger(ChessBenchmarkTrigger.class);

   private final ChessBenchmarkRunner benchmarkRunner;
   private final ChessResponseChecker responseChecker;
   private final ChessSignInCommand signInCommand;
   private final ChessUserSource userSource;
   private final TaskScheduler taskScheduler;
   private final String botPassword;
   private final String botId;

   public ChessBenchmarkTrigger(ChessBenchmarkRunner benchmarkRunner, ChessUserSource userSource, ChessSignInCommand signInCommand, TaskScheduler taskScheduler, String botId, String botPassword) {
      this.responseChecker = new ChessResponseChecker();
      this.benchmarkRunner = benchmarkRunner;
      this.signInCommand = signInCommand;
      this.taskScheduler = taskScheduler;
      this.userSource = userSource;
      this.botPassword = botPassword;
      this.botId = botId;
   }

   public void startBenchmark() {
      taskScheduler.scheduleTask(responseChecker, 5000);
   }

   private class ChessResponseChecker implements Task {

      private final List<ResponseStatus> retryStatus;

      public ChessResponseChecker() {
         this.retryStatus = Arrays.asList(ResponseStatus.NONE, ResponseStatus.OVERFLOW, ResponseStatus.TIMEOUT, ResponseStatus.NOT_CONNECTED);
      }

      @Override
      public long executeTask() {
         ResponseStatus responseStatus = signInCommand.getStatus();

         LOG.info("Sign in progress for " + botId + " is " + responseStatus);

         if (retryStatus.contains(responseStatus)) {
            LOG.info("Attemting to sign in for " + botId + " is " + responseStatus);
            signInCommand.execute(botId, botPassword);
         } else if (responseStatus == ResponseStatus.SUCCESS) {
            ChessUserValidationEvent responseEvent = signInCommand.getValidationResponse();
            ChessUserValidation responseValidation = responseEvent.getValidation();
            ChessUser chessUser = responseEvent.getUser();
            
            if(responseValidation != ChessUserValidation.PASSWORD_VERIFIED) {
               LOG.info("Failed to sign in for " + botId + " as response validation was " + responseValidation);
               return -1;
            }
            ChessBenchmarkExecutor task = new ChessBenchmarkExecutor(chessUser);
            
            LOG.info("Starting benchmark for " + botId + " in 5 seconds");
            userSource.loginUser(chessUser);
            taskScheduler.scheduleRunnable(task, 5000);
            return -1;
         } else if(responseStatus == ResponseStatus.SENDING) {
            LOG.info("Sign in request is sending");
         } else {
            LOG.info("Unable to sign in with " + botId + " as response is " + responseStatus);
            return -1;
         }
         return 5000;
      }
   }

   private class ChessBenchmarkExecutor implements Runnable {

      private final ChessUser chessUser;

      public ChessBenchmarkExecutor(ChessUser chessUser) {
         this.chessUser = chessUser;
      }

      @Override
      public void run() {
         LOG.info("Starting benchmark for " + botId);
         userSource.loginUser(chessUser); // be double sure!!
         benchmarkRunner.runBenchmark(chessUser);
      }

   }
}
