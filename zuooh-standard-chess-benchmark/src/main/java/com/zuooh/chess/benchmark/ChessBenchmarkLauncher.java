package com.zuooh.chess.benchmark;

import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import com.zuooh.application.ProcessContainer;
import com.zuooh.application.standard.file.StandardFileSystemContainer;
import com.zuooh.application.standard.process.StandardProcessMonitor;
import com.zuooh.common.FileSystem;
import com.zuooh.common.FileSystemDirectory;

public class ChessBenchmarkLauncher {  
   
   public static final String WORKSPACE_PATH = "c:\\Work\\development\\bitbucket\\";
   public static final String CONTAINER = "Container";
   public static final String FILE_SYSTEM = "FileSystem";
   public static final String PROCESS_MONITOR = "ProcessMonitor";   
   public static final String DATABASE = "Database";   

   private final Properties globalProperties;
   private final String[] propertiesFiles;
   private final String configFile;
   private final FileSystem fileSystem;

   public ChessBenchmarkLauncher(String rootPath, String configFile, String[] propertiesFiles) throws Exception {
      this.fileSystem = new FileSystemDirectory(rootPath, "temp", false);    
      this.globalProperties = new Properties();
      this.configFile = configFile;
      this.propertiesFiles = propertiesFiles;
   }

   public void launchBenchmark() throws Exception {      
      for(String propertiesFile : propertiesFiles) {
         InputStream attributes = fileSystem.openFile(propertiesFile);   
         globalProperties.load(attributes);
      }
      String name = getName();
      
      ProcessContainer container = new StandardFileSystemContainer(fileSystem, globalProperties);  
      StandardProcessMonitor monitor = new StandardProcessMonitor(name);
      
      container.setProcessMonitor(monitor);
      container.setContainer(container);
      container.setFileSystem(fileSystem);
      container.loadFrom(configFile);
      container.loadAll();
      container.startAll();
   } 
   
   public String getName() {
      return UUID.randomUUID().toString();
   }

   public boolean isHeadless() {
      return false;
   }
  
   public String getDatabaseSuffix() {
      String name = System.getProperty("user.namespace");
      
      if(name != null) {
         return "." + name + ".db";
      }
      return ".db";
   }
   
   public String getDatabaseLibrary() {
      return WORKSPACE_PATH + "chess\\zuooh-standard-chess-game\\lib";
   }    
      
   public String getDatabasePath() {
      return WORKSPACE_PATH + "chess\\zuooh-standard-chess-game\\resources\\temp\\database";
   }     

   public static void main(String[] list) throws Exception {
      ChessBenchmarkLauncher launcher = new ChessBenchmarkLauncher(
            WORKSPACE_PATH + "chess\\zuooh-standard-chess-benchmark\\resources", 
             "spring.xml", 
             new String[]{"common.properties", list[0]});
       
      launcher.launchBenchmark();             
   }
}

