package com.zuooh.chess.benchmark;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.benchmark.bot.ChessClientBot;
import com.zuooh.chess.benchmark.bot.ChessClientBotBuilder;
import com.zuooh.chess.benchmark.bot.ChessClientBotCriteria;
import com.zuooh.chess.benchmark.bot.ChessClientBotListener;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.client.engine.ChessRemoteGameManager;
import com.zuooh.chess.client.event.operation.ChessMatchGameCommand;
import com.zuooh.chess.client.event.response.ChessNewGameEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessOpponentStatus;
import com.zuooh.common.html.TableDrawer;
import com.zuooh.common.html.TableRowDrawer;
import com.zuooh.common.task.Task;
import com.zuooh.common.task.TaskScheduler;
import com.zuooh.common.time.DateTime;
import com.zuooh.container.annotation.ManagedOperation;
import com.zuooh.container.annotation.ManagedResource;
import com.zuooh.message.client.ResponseStatus;

@ManagedResource("Runs a series of games")
public class ChessBenchmarkRunner {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessBenchmarkRunner.class);
   
   private final Map<String, ChessBenchmarkProgress> gameProgress;
   private final Map<String, ChessOpponentStatus> gameResults; 
   private final Map<String, ChessClientBotCriteria> gameBots;;
   private final ChessMatchGameCommand matchCommand;
   private final ChessRemoteGameManager gameManager;
   private final ChessClientBotListener botListener;
   private final ChessClientBotBuilder botBuilder; 
   private final TaskScheduler taskScheduler;
   private final Random random;
   private final String boardSet;
   
   public ChessBenchmarkRunner(ChessClientBotBuilder botBuilder, ChessRemoteGameManager gameManager, ChessMatchGameCommand matchCommand, TaskScheduler taskScheduler, String boardSet, int repeats) {
      this.gameProgress = new ConcurrentHashMap<String, ChessBenchmarkProgress>();
      this.gameResults = new ConcurrentHashMap<String, ChessOpponentStatus>();
      this.gameBots = new ConcurrentHashMap<String, ChessClientBotCriteria>();
      this.botListener = new ChessBenchmarkListener(repeats);
      this.random = new SecureRandom();
      this.taskScheduler = taskScheduler;
      this.matchCommand = matchCommand;
      this.gameManager = gameManager;
      this.botBuilder = botBuilder;      
      this.boardSet = boardSet;       
   }
   
   @ManagedOperation("Show benchmark progress")
   public String showBenchmark() {
      TableDrawer drawer = new TableDrawer("gameId", "botId", "side", "skill", "result", "changes", "lastMove", "lastUpdate", "board");
      
      if(!gameProgress.isEmpty()) {
         Set<String> gameIds = gameProgress.keySet();
         
         for(String gameId : gameIds) {
            ChessBenchmarkProgress progress = gameProgress.get(gameId);
            ChessClientBotCriteria botCriteria = gameBots.get(gameId);
            ChessOpponentStatus botStatus = progress.getBotStatus();
            ChessMove lastMove = progress.getLastMove();
            ChessBoard chessBoard = progress.getGameBoard();
            DateTime lastUpdate = progress.getTime();
            String text = ChessTextBoardDrawer.drawBoard(chessBoard);
            TableRowDrawer gameRow = drawer.newRow();
            ChessSide botSide = botCriteria.getBotSide();
            ChessUser botUser = gameManager.currentUser();
            String botId = botUser.getKey();
            int changes = chessBoard.getChangeCount();
            int botSkill = botCriteria.getBotSkill();
            
            if(botStatus.isGameOver()) {
               gameRow.setColor("#ff0000");
            } else {
               gameRow.setColor("#00ff00");
            }
            gameRow.setNormal("gameId", gameId);            
            gameRow.setNormal("botId", botId);
            gameRow.setNormal("side", botSide);
            gameRow.setNormal("skill", botSkill);            
            gameRow.setNormal("result", botStatus);
            gameRow.setNormal("changes", changes);
            gameRow.setNormal("lastMove", lastMove);
            gameRow.setNormal("lastUpdate", lastUpdate);
            gameRow.setCode("board", text);                        
         }
      }
      return drawer.drawTable();
   }
   
   public void runBenchmark(ChessUser botUser) {
      ChessUser currentUser = gameManager.currentUser();
      String currentId = currentUser.getKey();
      String botId = botUser.getKey();
      
      if(!botId.equals(currentId)) {
         throw new IllegalArgumentException("Bot user " + botId + " is not the current user " + currentId);
      }
      executeChecker();     
   }

   private void executeBenchmark(ChessClientBotCriteria botCriteria) {
      try {
         ChessClientBot botClient = botBuilder.createNewBot(botListener, botCriteria);
         String gameId = botCriteria.getGameId();
         
         gameBots.put(gameId, botCriteria);
         botClient.start();
      } catch(Exception e) {
         LOG.info("Could not run benchmark", e);
      }
   }
   
   private void executeChecker() {
      int botSkill = random.nextInt(5) + 1;
      int randomSide = random.nextInt(100);
      ChessSide botSide = ChessSide.WHITE;
      ChessUser botUser = gameManager.currentUser();
      String botId = botUser.getKey();      
      
      if(randomSide % 2 == 0) {
         botSide = ChessSide.BLACK;
      }      
      ChessMatchChecker checker = new ChessMatchChecker(botId, botSide, botSkill, 60 * 60 * 4 * 1000L);
      taskScheduler.scheduleTask(checker, 1000);
   }
  
   
   private class ChessMatchChecker implements Task {

      private final List<ResponseStatus> retryStatus;
      private final ChessSide botSide;
      private final String botId;
      private final int botSkill;
      private final long gameDuration;
      
      public ChessMatchChecker(String botId, ChessSide botSide, int botSkill, long gameDuration) {
         this.retryStatus = Arrays.asList(ResponseStatus.NONE, ResponseStatus.OVERFLOW, ResponseStatus.TIMEOUT, ResponseStatus.NOT_CONNECTED, ResponseStatus.FINISHED);
         this.gameDuration = gameDuration;
         this.botId = botId;
         this.botSide = botSide;
         this.botSkill = botSkill;
      }

      @Override
      public long executeTask() {
         ResponseStatus responseStatus = matchCommand.getStatus();

         LOG.info("Searching for games for " + botId + " resulted in " + responseStatus);

         if (retryStatus.contains(responseStatus)) {
            LOG.info("Attemting to match games for " + botId + " resulted in " + responseStatus);
            matchCommand.execute(botSide, gameDuration, botId, botSkill);
         } else if (responseStatus == ResponseStatus.SUCCESS) {
            ChessNewGameEvent responseEvent = matchCommand.getGameResponse();
            
            if(responseEvent == null) {
               String gameId = createGameId(botSide, botId);
               ChessClientBotCriteria criteria = new ChessClientBotCriteria(gameId, botId, botSide, boardSet, botSkill, gameDuration, true);
               
               LOG.info("Creating a new game " + gameId + " for " + botId);
               
               executeBenchmark(criteria);
               matchCommand.reset();
               return -1;
            } else {
               ChessGame game = responseEvent.getGame();
               String gameId = game.getGameId();
               ChessClientBotCriteria criteria = new ChessClientBotCriteria(gameId, botId, botSide, boardSet, botSkill, gameDuration, false);
               
               LOG.info("Accepting existing game " + gameId + " for " + botId);
               
               executeBenchmark(criteria);
               matchCommand.reset();
               return -1;
            }           
         } else if(responseStatus != ResponseStatus.SENDING){ // no longer sending!!
            String gameId = createGameId(botSide, botId);
            ChessClientBotCriteria criteria = new ChessClientBotCriteria(gameId, botId, botSide, boardSet, botSkill, gameDuration, true);
            
            LOG.info("Unable to search for a new game for " + botId + " as response is " + responseStatus);
            
            executeBenchmark(criteria);
            matchCommand.reset();
            return -1;
         }
         return 5000;
      }
      
      private String createGameId(ChessSide side, String user) {
         StringBuilder builder = new StringBuilder();
         
         try {
            MessageDigest digest = MessageDigest.getInstance("MD5");
            long nanoTime = System.nanoTime();
            long hash = System.identityHashCode(this);
            
            builder.append(nanoTime);
            builder.append(side);
            builder.append(user);
            builder.append(hash);
            
            String key = builder.toString();
            byte[] octets = key.getBytes("UTF-8");
            
            builder.setLength(0);
            digest.update(octets);
            
            byte[] result = digest.digest();
            
            for(int i = 0; i < result.length;i++) {
               int value = 0xff & result[i];
               
               if(value <= 0xf) {
                  builder.append("0");
               }
               builder.append(Integer.toHexString(value));
            }
            return builder.toString();         
         }catch(Exception e) {
            return builder.toString();
         }
      } 
   }
   
   private class ChessBenchmarkProgress {

      private final ChessOpponentStatus botStatus;
      private final ChessBoard gameBoard;
      private final ChessMove lastMove;
      private final String gameId;
      private final DateTime time;
      
      public ChessBenchmarkProgress(ChessOpponentStatus botStatus, ChessBoard gameBoard, ChessMove lastMove, String gameId) {
         this.time = DateTime.now();
         this.botStatus = botStatus;
         this.gameBoard = gameBoard;
         this.lastMove = lastMove;
         this.gameId = gameId;                       
      }      
      
      public ChessOpponentStatus getBotStatus() {
         return botStatus;
      }

      public ChessBoard getGameBoard() {
         return gameBoard;
      }

      public ChessMove getLastMove() {
         return lastMove;
      }
      
      public DateTime getTime() {
         return time;
      }

      public String getGameId() {
         return gameId;
      }
      
      @Override
      public String toString() {
         return  lastMove + ChessTextBoardDrawer.drawBoard(gameBoard);
      }
   }
   
   private class ChessBenchmarkListener implements ChessClientBotListener {
      
      private final Map<String, ChessOpponentStatus> finalResults;
      private final AtomicInteger repeats;
      
      public ChessBenchmarkListener(int repeats) {
         this.finalResults = new ConcurrentHashMap<String, ChessOpponentStatus>();
         this.repeats = new AtomicInteger(repeats);
      }      

      @Override
      public void onMove(ChessOpponentStatus botStatus, ChessBoard gameBoard, ChessMove lastMove, String gameId) {        
         ChessBenchmarkProgress  progress = new ChessBenchmarkProgress(botStatus, gameBoard, lastMove, gameId);
         
         gameResults.put(gameId, botStatus);
         gameProgress.put(gameId, progress);
      }      

      @Override
      public void onGameOver(ChessOpponentStatus botStatus, ChessBoard gameBoard, ChessMove lastMove, String gameId) {
         ChessBenchmarkProgress  progress = new ChessBenchmarkProgress(botStatus, gameBoard, lastMove, gameId);

         if(!finalResults.containsKey(gameId)) { // only register result once
            finalResults.put(gameId, botStatus);
            gameResults.put(gameId, botStatus);
            gameProgress.put(gameId, progress);
            gameManager.removeGameObserver(gameId);
            gameManager.removeActionObserver(gameId);
         
            LOG.info("gameId=[" + gameId + "] botStatus=[" + botStatus + "] lastMove=["+lastMove+"]");
         }
         int remaining = repeats.getAndDecrement();         
         
         if(remaining > 0) {
            executeChecker();
         }      
      }
   }
}
