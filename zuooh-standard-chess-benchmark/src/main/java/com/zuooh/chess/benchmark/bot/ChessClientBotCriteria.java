package com.zuooh.chess.benchmark.bot;

import com.zuooh.chess.ChessSide;

public class ChessClientBotCriteria {
   
   private final String gameId;
   private final String  boardSet;
   private final ChessSide botSide;
   private final String botId;
   private final int botSkill;
   private final long gameDuration;
   private final boolean create;
   
   public ChessClientBotCriteria(String gameId, String botId, ChessSide botSide, String boardSet, int botSkill, long gameDuration, boolean create) {
      this.gameDuration = gameDuration;
      this.boardSet = boardSet;      
      this.gameId = gameId;
      this.botId = botId;
      this.botSide = botSide;
      this.botSkill = botSkill;
      this.create = create;
   }      
   
   public boolean isCreate() {
      return create;
   }
   
   public String getGameId() {
      return gameId;
   }
   
   public String getBoardSet() {
      return boardSet;
   }

   public ChessSide getBotSide() {
      return botSide;
   }

   public String getBotId() {
      return botId;
   }

   public int getBotSkill() {
      return botSkill;
   }   
   
   public long getGameDuration() {
      return gameDuration;
   }
      
}
