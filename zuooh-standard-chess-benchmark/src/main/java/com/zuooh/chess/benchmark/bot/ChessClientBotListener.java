package com.zuooh.chess.benchmark.bot;

import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessOpponentStatus;

public interface ChessClientBotListener {
   void onMove(ChessOpponentStatus botStatus, ChessBoard gameBoard, ChessMove lastMove, String gameId);
   void onGameOver(ChessOpponentStatus botStatus, ChessBoard gameBoard, ChessMove lastMove, String gameId);
}
