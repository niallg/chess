package com.zuooh.chess.benchmark.bot;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessMoveResult;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessClientBot {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessClientBot.class);

   private final ConcurrentMap<ChessSide, ChessMove> lastMoves;
   private final ChessClientBotListener botListener;
   private final ChessMoveEngine opponentEngine;
   private final ChessMoveEngine computerEngine;
   private final AtomicInteger noProgressCount;
   private final ChessBoard chessBoard;
   private final String gameId;

   public ChessClientBot(ChessClientBotListener botListener, ChessMoveEngine opponentEngine, ChessMoveEngine computerEngine, ChessPieceSet chessSet, String gameId) { 
      this.lastMoves = new ConcurrentHashMap<ChessSide, ChessMove>();
      this.chessBoard = new ChessPieceSetBoard(chessSet);
      this.noProgressCount = new AtomicInteger();
      this.opponentEngine = opponentEngine;
      this.computerEngine = computerEngine;
      this.botListener = botListener;
      this.gameId = gameId;
   }
   
   public void start() {
      ChessSide side = computerEngine.getSide();
      
      if(side == ChessSide.WHITE) {
         computerEngine.makeMove(chessBoard, null);             
         
         ChessOpponentStatus engineStatus = computerEngine.getEngineStatus();
         ChessMove computerMove = computerEngine.getLastMove();         
         
         if(engineStatus == ChessOpponentStatus.THINKING) {
            throw new IllegalStateException("Chess engine is still thinking after first move was made");
         }
         if(computerMove != null) {
            updateBoard(computerMove); 
            opponentEngine.makeMove(chessBoard, computerMove);
         }
      } else {
         opponentEngine.makeMove(chessBoard, null); // this just subscribes!!
      }
   }
   
   public void gameUpdate(ChessGame chessGame) {
      ChessGameStatus gameStatus = chessGame.getStatus();
      String updateId = chessGame.getGameId();
      
      if(updateId.equals(gameId)) {
         LOG.info("[" + gameId + "] Game status is " + gameStatus);
         
         if(gameStatus.isGameOver()) {
            ChessMove lastMove = chessGame.getLastMove();
            
            if(lastMove != null) {
               updateBoard(lastMove); // make sure board has last move
               determineWinner(chessGame);
            } else {
               LOG.info("[" + gameId + "] Game over with no last move made, timeout? " + gameStatus);
               botListener.onGameOver(ChessOpponentStatus.DRAW, chessBoard, lastMove, gameId);
            }
         }
      }
   }
   
   private void determineWinner(ChessGame chessGame) {
      ChessGameStatus gameStatus = chessGame.getStatus();
      ChessMove lastMove = chessGame.getLastMove();
      ChessSide botSide = computerEngine.getSide();
      
      LOG.info("[" + gameId + "] Last move was " + lastMove + " for result of " + gameStatus);           
      
      if(ChessCheckAnalyzer.staleMate(chessBoard)) {
         LOG.info("[" + gameId + "] Draw as a result of stale mate");
         botListener.onGameOver(ChessOpponentStatus.DRAW, chessBoard, lastMove, gameId);
      } else {
         if(!ChessCheckAnalyzer.checkMatePossible(chessBoard)) {
            LOG.info("[" + gameId + "] Draw as a result of insufficient material");
            botListener.onGameOver(ChessOpponentStatus.DRAW, chessBoard, lastMove, gameId);
         } else {
            if(lastMove.side == botSide) {
               LOG.info("[" + gameId + "] Win as last move was made by " + botSide);
               botListener.onGameOver(ChessOpponentStatus.WIN, chessBoard, lastMove, gameId);
            } else {
               LOG.info("[" + gameId + "] Lose as last move was made by " + botSide);
               botListener.onGameOver(ChessOpponentStatus.LOSE, chessBoard, lastMove, gameId); // not really true, could be stale mate!!
            }
         }
      }
   }

   public void moveConfirmed(ChessMove myMove) {
      updateBoard(myMove);
   }

   public void moveResponse(ChessMoveResult opponentMoveResult) {
      ChessMove opponentMove = opponentMoveResult.getMove();
      ChessOpponentStatus opponentStatus = opponentMoveResult.getStatus();
      
      LOG.info("[" + gameId + "] Opponent made move " + opponentMove + " with status " + opponentStatus);
      
      if(opponentMove != null) {                  
         updateBoard(opponentMove); 
         
         int movesWithNoProgress = noProgressCount.get();
         
         // http://en.wikipedia.org/wiki/Fifty-move_rule
         if(movesWithNoProgress >= 50) {
            LOG.info("[" + gameId + "] Resigning as no progress in " + movesWithNoProgress + " moves");
            opponentEngine.resignGame();
            botListener.onGameOver(ChessOpponentStatus.WIN, chessBoard, opponentMove, gameId);            
         } else {         
            computerEngine.makeMove(chessBoard, opponentMove);             
            
            ChessOpponentStatus engineStatus = computerEngine.getEngineStatus();
            ChessMove computerMove = computerEngine.getLastMove();            
            
            if(engineStatus == ChessOpponentStatus.THINKING) {
               throw new IllegalStateException("Chess engine is still thinking after " + opponentMove + " was made");
            }
            if(engineStatus.isGameOver()) {
               botListener.onGameOver(engineStatus, chessBoard, computerMove, gameId);
            } else if(computerMove != null) {
               updateBoard(computerMove); 
               opponentEngine.makeMove(chessBoard, computerMove);
            }
         }
      }
   }

   private void updateBoard(ChessMove moveMade) {
      ChessSide moveDirection = moveMade.getSide();
      ChessMove lastMove = lastMoves.put(moveDirection, moveMade);

      if(lastMove == null || moveMade.from != lastMove.from || moveMade.to != lastMove.to) {
         ChessBoardCell fromCell = chessBoard.getBoardCell(moveMade.from);
         ChessBoardCell toCell = chessBoard.getBoardCell(moveMade.to);
         ChessOpponentStatus engineStatus = computerEngine.getEngineStatus();         
         ChessPiece movedPiece = fromCell.getPiece();
         
         botListener.onMove(engineStatus, chessBoard, moveMade, gameId);
         
         ChessBoardMove boardMove = chessBoard.getBoardMove(moveMade);

         if(toCell.isCellEmpty()) {
            boardMove.makeMove();
            
            // http://en.wikipedia.org/wiki/Fifty-move_rule
            if(movedPiece.key.type != ChessPieceType.PAWN) {
               noProgressCount.getAndIncrement();
            } else {
               noProgressCount.set(0);
            }
         } else {
            boardMove.makeMove();
            noProgressCount.set(0);
         }
         System.err.println(moveMade + ": " + ChessTextBoardDrawer.drawBoard(chessBoard));
      }
   }
}
