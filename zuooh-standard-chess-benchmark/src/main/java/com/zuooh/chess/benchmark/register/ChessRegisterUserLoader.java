package com.zuooh.chess.benchmark.register;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.benchmark.ChessBenchmarkRunner;
import com.zuooh.chess.client.event.operation.ChessRegisterUserCommand;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.common.task.Task;
import com.zuooh.common.task.TaskScheduler;
import com.zuooh.message.client.ResponseStatus;

public class ChessRegisterUserLoader {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessRegisterUserLoader.class);

   private final ChessRegistrationExecutor executor;
   private final ChessRegisterUserCommand command;
   private final TaskScheduler scheduler;
   private final boolean enable;
   
   public ChessRegisterUserLoader(ChessRegisterUserCommand command, TaskScheduler scheduler, int users) {
      this(command, scheduler, users, false);
   }      
   
   public ChessRegisterUserLoader(ChessRegisterUserCommand command, TaskScheduler scheduler, int users, boolean enable) {
      this.executor = new ChessRegistrationExecutor(users);
      this.scheduler = scheduler;
      this.command = command;
      this.enable = enable;
   }
   
   public void registerUsers() {
      if(enable) {
         scheduler.scheduleTask(executor);
      }
   }
   
   private class ChessRegistrationExecutor implements Task {

      private final List<ResponseStatus> retryStatus;
      private final AtomicInteger count;
      private final int limit;

      public ChessRegistrationExecutor(int limit) {
         this.retryStatus = Arrays.asList(ResponseStatus.NONE, ResponseStatus.OVERFLOW, ResponseStatus.TIMEOUT, ResponseStatus.NOT_CONNECTED, ResponseStatus.FINISHED);
         this.count = new AtomicInteger();
         this.limit = limit;
      }

      @Override
      public long executeTask() {
         int currentCount = count.get();
         String userName = String.format("user-%s", currentCount);     
         ResponseStatus responseStatus = command.getStatus();

         LOG.info("Sign in progress for " + userName + " is " + responseStatus);

         if (retryStatus.contains(responseStatus)) {                
            LOG.info("Attemting to register " + userName + " is " + responseStatus);
            command.execute(userName, "password12");
         } else if (responseStatus == ResponseStatus.SUCCESS) {
            ChessUserValidationEvent responseEvent = command.getValidationResponse();
            ChessUserValidation responseValidation = responseEvent.getValidation();
            
            if(responseValidation == ChessUserValidation.USER_NAME_REGISTERED || responseValidation == ChessUserValidation.USER_REGISTERED) {
               if(responseValidation == ChessUserValidation.USER_REGISTERED) {
                  ChessUser chessUser = responseEvent.getUser();
                  String userId = chessUser.getKey();
                  
                  LOG.info("Success registering user " + userName + " with key " + userId);
               } else {
                  LOG.info("User name " + userName + " is already registered");  
               }
               int nextCount = count.getAndIncrement();
               
               if(nextCount >= limit) {
                  LOG.info("Limit of " + limit + " reached");
                  return -1;
               }
               command.reset();
               return 1;                                    
            }
            LOG.info("Signed in  for " + userName + " failed with " + responseValidation);            
            return -1;            
         } else if(responseStatus != ResponseStatus.SENDING){
            LOG.info("Unable to register " + userName + " as response is " + responseStatus);
            return -1;
         }
         return 100;
      }
   }
}
