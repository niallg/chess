package com.zuooh.chess.server.notify;

import java.util.List;

import com.zuooh.chess.database.notify.ChessNotification;

public interface ChessNotificationListener {
   void onNotification(List<ChessNotification> notifications);
   void onException(Exception cause);
}
