package com.zuooh.chess.server.game.bot;

import java.security.SecureRandom;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.event.ChessMoveEvent;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.server.game.ChessGameChangeObserver;
import com.zuooh.chess.server.game.ChessGameCreateObserver;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.common.thread.ThreadPool;
import com.zuooh.common.thread.ThreadPoolFactory;

@ManagedResource(description="Controls bot behaviour")
public class ChessServerBotObserver implements ChessGameChangeObserver, ChessGameCreateObserver {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessServerBotObserver.class);
   
   private final Set<ChessServerBotGroup> botPlayerGroups;
   private final Cache<String, ChessServerBot> botPlayers;
   private final Cache<String, ChessUser> botUsers;
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;  
   private final AtomicLong matchDelay;
   private final AtomicLong acceptDelay;
   private final ThreadFactory threadFactory;
   private final ThreadPool threadPool;
   private final ThreadPool gamePool;
   private final Random randomGenerator;
   private final String observerName;

   public ChessServerBotObserver(ChessServerController mainController, ChessServerPublisher serverPublisher, ThreadPool gamePool, String observerName) throws Exception {
      this(mainController, serverPublisher, gamePool, observerName, 10000, 5 * 60000);
   }
   
   public ChessServerBotObserver(ChessServerController mainController, ChessServerPublisher serverPublisher, ThreadPool gamePool, String observerName, long matchDelay, long acceptDelay) throws Exception {
      this.botPlayers = new LeastRecentlyUsedCache<String, ChessServerBot>(10000);
      this.botUsers = new LeastRecentlyUsedCache<String, ChessUser>(1000);
      this.botPlayerGroups = new CopyOnWriteArraySet<ChessServerBotGroup>();
      this.threadFactory = new ThreadPoolFactory(ChessServerBot.class);
      this.threadPool = new ThreadPool(threadFactory, 10);
      this.matchDelay = new AtomicLong(matchDelay);
      this.acceptDelay = new AtomicLong(acceptDelay);
      this.randomGenerator = new SecureRandom();
      this.mainController = mainController;   
      this.serverPublisher = serverPublisher;
      this.observerName = observerName;
      this.gamePool = gamePool;
   }
   
   @ManagedAttribute(description="Delay before match")
   public long getDelayBeforeMatch() {
      return matchDelay.get();
   }      
   
   @ManagedAttribute(description="Delay before accepting challenge")
   public long getDelayBeforeAccept() {
      return acceptDelay.get();
   }
   
   @ManagedOperation(description="Set delay before matching")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="minutes", description="Delay before match")
   })
   public void setDelayBeforeMatch(long minutes) {
      matchDelay.set(minutes);
   }   
   
   @ManagedOperation(description="Set delay before accepting challenge")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="minutes", description="Delay before accept")
   })
   public void setDelayBeforeAccept(long minutes) {
      acceptDelay.set(minutes);
   }
   
   public void registerObserver() { // should run on start up
      try {
         mainController.registerObserver(observerName, (ChessGameCreateObserver)this); // one observer to rule them all
         mainController.registerObserver(observerName, (ChessGameChangeObserver)this); // one observer to rule them all
      } catch(Exception e) {
         LOG.info("Could not register notification observer '" + observerName + "'", e);
      }
   }
   
   public void registerBotGroup(ChessServerBotGroup botGroup){
      botPlayerGroups.add(botGroup);
   } 
   
   public void registerBotPlayer(ChessServerBot bot, String gameId){
      botPlayers.cache(gameId, bot);
   }
   
   public void registerBotUser(ChessUser bot){
      String userId = bot.getKey();
      botUsers.cache(userId, bot);
   } 

   @Override
   public void onCreate(ChessGameState event) {
      ChessGameStatus status = event.getGameStatus();
      
      if(status == ChessGameStatus.AWAITING_ACCEPTENCE)  {
         NewGameNotification gameNotification = new NewGameNotification(event);
         long delay = acceptDelay.get();
         long randomDelay = randomGenerator.nextInt((int)delay) + 5000;
         
         gamePool.schedule(gameNotification, randomDelay, TimeUnit.MILLISECONDS); 
      } else if(status == ChessGameStatus.NEW)  {
         NewGameNotification gameNotification = new NewGameNotification(event);
         long delay = matchDelay.get();
         long randomDelay = randomGenerator.nextInt((int)delay) + 5000;
         
         gamePool.schedule(gameNotification, randomDelay, TimeUnit.MILLISECONDS); 
      }
   }
   
   @Override
   public void onStart(ChessGameState gameState) {
      String gameId = gameState.getGameId();
      ChessServerBot chessBot = botPlayers.fetch(gameId);
      
      if(chessBot != null) {
         StartNotification notification = new StartNotification(chessBot, gameState);
         threadPool.execute(notification);
      }
   }   

   @Override
   public void onMove(ChessGameState gameState, ChessMoveState chessMove) {
      String gameId = chessMove.getGameId();
      String userId = chessMove.getUserId();
      
      try {
         if(botUsers.contains(userId)) { // was it a bot move?
            ChessGameCriteria criteria = mainController.gameDetails(gameId, userId);
            
            if(criteria != null) {
               ChessUser challenger = criteria.getUser();
               
               if(challenger != null) {
                  String challengerId = challenger.getKey();
                  ChessBoardPosition from = chessMove.getFromPosition();
                  ChessBoardPosition to = chessMove.getToPosition();
                  ChessSide side = chessMove.getUserSide();
                  int change = chessMove.getChangeCount();
                  
                  if(!botUsers.contains(challengerId)) { // bot vs bot!!?
                     ChessMove move = new ChessMove(from, to, side, change);
                     ChessMoveEvent event = new ChessMoveEvent(move, challengerId, userId, gameId);
                  
                     serverPublisher.publishNotification(event, challengerId);
                  }
               }
            }
         } else {
            ChessServerBot chessBot = botPlayers.fetch(gameId); // is the opponent a bot!!
            
            if(chessBot != null) {
               MoveNotification notification = new MoveNotification(chessBot, chessMove);
               threadPool.execute(notification);
            }
         }
      } catch(Exception e) {
         LOG.info("Could not dispatch notification", e);
      }      
   }

   @Override
   public void onFinish(ChessGameState gameState) {
      String gameId = gameState.getGameId();
      ChessServerBot chessBot = botPlayers.take(gameId);
      
      if(chessBot != null) {
         GameOverNotification notification = new GameOverNotification(chessBot, gameState);
         threadPool.execute(notification);
      }
   }
   
   private class NewGameNotification implements Runnable {

      private final ChessGameState game;

      public NewGameNotification(ChessGameState game) {
         this.game = game;
      }

      @Override
      public void run() {
         try {
            String gameId = game.getGameId();
            
            if(mainController.isNew(gameId)) { // ensure its not been accepted
               for(ChessServerBotGroup botPlayerGroup : botPlayerGroups) {
                  botPlayerGroup.newGame(game);
               }
            }
         } catch (Exception e) {
            LOG.info("Error on bot notification", e);
         }
      }
   }
   
   private class StartNotification implements Runnable {

      private final ChessServerBot bot;
      private final ChessGameState game;

      public StartNotification(ChessServerBot bot, ChessGameState game) {
         this.game = game;
         this.bot = bot;
      }

      @Override
      public void run() {
         try {
            bot.startGame(game);
         } catch (Exception e) {
            LOG.info("Error on bot notification", e);
         }
      }
   }   

   private class MoveNotification implements Runnable {

      private final ChessServerBot bot;
      private final ChessMoveState move;

      public MoveNotification(ChessServerBot bot, ChessMoveState move) {
         this.move = move;
         this.bot = bot;
      }

      @Override
      public void run() {
         try {
            bot.moveMade(move);
         } catch (Exception e) {
            LOG.info("Error on bot notification", e);
         }
      }
   }

   private class GameOverNotification implements Runnable {

      private final ChessServerBot bot;
      private final ChessGameState game;

      public GameOverNotification(ChessServerBot bot, ChessGameState game) {
         this.game = game;
         this.bot = bot;
      }

      @Override
      public void run() {
         try {
            bot.finishGame(game);
         } catch (Exception e) {
            LOG.info("Error on bot notification", e);
         }
      }
   }
}
