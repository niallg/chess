package com.zuooh.chess.server.game.message;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.event.request.ChessChangePasswordEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserType;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.mail.ChessServerMailHandler;
import com.zuooh.message.client.RequestHandler;

public class ChessChangePasswordRequestHandler implements RequestHandler<ChessChangePasswordEvent> {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessChangePasswordRequestHandler.class);
   
   private final ChessServerMailHandler mailHandler;
   private final ChessServerController mainController;
   
   public ChessChangePasswordRequestHandler(ChessServerController mainController, ChessServerMailHandler mailHandler) throws Exception { 
      this.mainController = mainController;   
      this.mailHandler = mailHandler;
   }

   public ChessUserValidationEvent execute(ChessChangePasswordEvent event) throws Exception {
      String userId = event.getUserId();
      String password = event.getPassword();
      ChessUser user = mainController.loadUser(userId);
      
      if(user != null)  {       
         ChessUserData userData = user.getData();
         ChessUserType userType = user.getType();
         String userMail = user.getMail();
         String userName = user.getName();
         
         try {  
            ChessUser resetProfile = new ChessUser(userData, userType, userId, userName, userMail, password);
            mainController.saveUser(resetProfile);
            return new ChessUserValidationEvent(ChessUserValidation.PASSWORD_CHANGED, resetProfile);
         } catch(Exception e)  {
            LOG.info("Error sending password to " + userId);
         }
      }
      return new ChessUserValidationEvent(ChessUserValidation.USER_NAME_NOT_REGISTERED, user);
   }
}
