package com.zuooh.chess.server.game.account;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserStatus;
import com.zuooh.chess.database.user.ChessUserType;

public class ChessAccountUserBuilder {
   
   public static final String ANONYMOUS_NAME = "Anonymous";
   public static final String INVALID_NAME = "Invalid";    

   public ChessUser createUser(ChessAccount account) {
      ChessAccountType accountType = account.getType();
      ChessUserData userData = createData(account);
      String userId = account.getKey();
      String userName = account.getName();
      String userMail = account.getMail();
      String password = account.getPassword();
      
      if(accountType.isInvalid()) {
         return new ChessUser(userData, ChessUserType.INVALID, userId, userName, userMail, password);
      }
      if(accountType.isAnonymous()) {
         return new ChessUser(userData, ChessUserType.ANONYMOUS, userId, userName, userMail, password);
      }
      return new ChessUser(userData, ChessUserType.NORMAL, userId, userName, userMail, password);
   }
   
   public ChessUserData createData(ChessAccount account) {
      int rank = account.getRank();
      int percentile = account.getPercentile();
      int blackGames = account.getBlackGames();
      int whiteGames = account.getWhiteGames();
      ChessUserStatus status = ChessUserStatus.calculateStatus(blackGames + whiteGames, percentile);
      int skill = ChessUserStatus.calculateSkill(blackGames + whiteGames, percentile);
      long lastSeenOnline = account.getLastSeenOnline();
      
      return new ChessUserData(status, rank, skill, blackGames, whiteGames, lastSeenOnline);
   }
   
   public ChessAccount createAccount(ChessUser user) {
      ChessUserType userType = user.getType();
      String userId = user.getKey();
      String userName = user.getName();
      String userMail = user.getMail();      
      String password = user.getPassword();
      
      if(userType.isAnonymous() && userName.equalsIgnoreCase(ANONYMOUS_NAME)) {
         return new ChessAccount(ChessAccountType.ANONYMOUS, userId, ANONYMOUS_NAME, userMail, null); // keep user mail 
      }
      return new ChessAccount(ChessAccountType.ACTIVE, userId, userName, userMail, password);     
   }
   
   public ChessAccount createInvalidAccount(String playerId) {
      return new ChessAccount(ChessAccountType.INVALID, playerId, INVALID_NAME, null, null);  
   }   

   public ChessAccount createAnonymousAccount(String playerId) {
      return new ChessAccount(ChessAccountType.ANONYMOUS, playerId, ANONYMOUS_NAME, null, null);  
   }
}
