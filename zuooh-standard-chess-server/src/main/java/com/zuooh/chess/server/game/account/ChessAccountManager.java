package com.zuooh.chess.server.game.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.user.ChessUser;

public class ChessAccountManager {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessAccountManager.class);
   
   private final ChessAccountUserBuilder builder;
   private final ChessAccountDatabase database;
   
   public ChessAccountManager(ChessAccountDatabase database) {
      this.builder = new ChessAccountUserBuilder();
      this.database = database;
   }

   public synchronized void updateAccount(ChessUser profile) {
      ChessAccount account = builder.createAccount(profile);      
      long time = System.currentTimeMillis();
         
      if(account != null) {
         try {
            String playerId = account.getKey();
            ChessAccount current = database.loadAccount(playerId);
            
            if(current != null) {
               String password = account.getPassword();
               String mail = account.getMail();
               String name = account.getName();
               
               current.setPassword(password);
               current.setMail(mail); // update the mail!! important for anonymous players
               current.setName(name);
               account.setLastSeenOnline(time);
               database.saveAccount(current);
            } else {
               account.setLastSeenOnline(time);
               database.saveAccount(account); // update if exists
            }
         } catch(Exception e) {
            LOG.info("Could not save account", e);
         }
      }
   }
}
