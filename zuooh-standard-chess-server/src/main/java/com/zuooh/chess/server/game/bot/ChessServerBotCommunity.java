package com.zuooh.chess.server.game.bot;

import java.security.SecureRandom;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessGameStateDatabase;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngineProvider;

public class ChessServerBotCommunity implements ChessServerBotGroup {

   private final ChessUserDatabase profileDatabase;
   private final ChessServerBotPublisher botPublisher;
   private final ChessServerBotBuilder botBuilder;
   private final ChessServerBotObserver botObserver;
   private final ChessGameStateDatabase gameDatabase;
   private final Set<String> botNames;
   private final String observerName;
   private final Random randomSkill;

   public ChessServerBotCommunity(ChessServerBotPublisher botPublisher, ChessServerBotObserver botObserver, ChessUserDatabase profileDatabase, ChessGameStateDatabase gameDatabase, ChessMoveEngineProvider engineProvider, String observerName) {
      this.botBuilder = new ChessServerBotBuilder(botPublisher, engineProvider);
      this.botNames = new CopyOnWriteArraySet<String>();
      this.randomSkill = new SecureRandom();
      this.gameDatabase = gameDatabase;
      this.profileDatabase = profileDatabase;
      this.botPublisher = botPublisher;   
      this.observerName = observerName;   
      this.botObserver = botObserver;
   }

   public void registerBots() {
      try {
         List<ChessUser> botProfiles = profileDatabase.loadUsersMatching(observerName, true);
         
         for(ChessUser botProfile : botProfiles) {
            String botId = botProfile.getKey();
            
            botObserver.registerBotUser(botProfile);
            botNames.add(botId);
         }
         botObserver.registerBotGroup(this);
         
         for(ChessUser botProfile : botProfiles) {
            String botId = botProfile.getKey();
            List<ChessGameState> currentGames = gameDatabase.listGamesPlayedBy(botId);
            
            for(ChessGameState gameState : currentGames) {
               try {
                  playGame(gameState, botProfile);
               } catch(Exception e){
                  gameDatabase.deleteGame(gameState); // stop bad games ruining things
               }
            }
         }         
      } catch(Exception e) {
         throw new IllegalStateException("Problem registering observer", e);
      }
   }
   
   @Override
   public void newGame(ChessGameState game) {
      ChessGameStatus status = game.getGameStatus();
      
      if(status == ChessGameStatus.AWAITING_ACCEPTENCE) {
         try {
            String challengerId = game.getChallengerId();
            
            if(botNames.contains(challengerId)) {
               ChessUser botProfile = profileDatabase.loadUser(challengerId);
               
               if(botProfile != null) {
                  playGame(game, botProfile);  
               }
            }
         }catch(Exception e) {
            throw new IllegalStateException("Error accepting challenge", e);
         }         
      }
   }

   private void playGame(ChessGameState game, ChessUser botProfile) throws Exception {
      String gameId = game.getGameId();
      String playerId = game.getOwnerId(); // a server bot will never own a game!!
      String botId = botProfile.getKey();
      ChessGameStatus status = game.getGameStatus();   
      ChessUserData playerData = botProfile.getData();
      int botSkill = playerData.getSkill();
      
      if(botSkill <= 1) {
         botSkill = randomSkill.nextInt(3) + 3; // at least 2 in skill but less than 5 
      }
      ChessSide pieceColor = game.getOwnerSide();
      ChessSide botDirection = pieceColor.oppositeSide();        
      ChessServerBot chessBot = botBuilder.createNewBot(botProfile, gameId, playerId, botDirection, botSkill);
      
      if(status == ChessGameStatus.AWAITING_ACCEPTENCE) {
         chessBot.start();
         botObserver.registerBotPlayer(chessBot, gameId);
         botPublisher.acceptGame(gameId, botId, botSkill); // ensure its done in correct thread         
      } 
      if(status == ChessGameStatus.IN_PROGRESS) {         
         List<ChessMoveState> listMoves = gameDatabase.listMoves(gameId);
         Iterator<ChessMoveState> moveIterator = listMoves.iterator();
         int size = listMoves.size();
         int lastIndex = size - 1;
         int index = 0;
         
         botObserver.registerBotPlayer(chessBot, gameId);
         
         while(moveIterator.hasNext()) {
            ChessMoveState gameMove = moveIterator.next();
            ChessMove moveMade = gameMove.getMove();           
            
            if(index++ == lastIndex) {     
               String moveUser = gameMove.getUserId();
               
               if(!botId.equals(moveUser)) {
                  //chessBot.moveMade(gameMove); // kick engine in to gear from calling thread
                  botObserver.onMove(game, gameMove); // kick engine in to gear to play ASYNCHRONOUSLY
               } else {
                  chessBot.updateBoard(moveMade);
               }
            } else {
               chessBot.updateBoard(moveMade);
            }
         }
      }      
   }
}

