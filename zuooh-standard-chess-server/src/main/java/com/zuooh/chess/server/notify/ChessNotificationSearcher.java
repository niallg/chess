package com.zuooh.chess.server.notify;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.database.notify.ChessNotification;
import com.zuooh.chess.database.notify.ChessNotificationDatabase;
import com.zuooh.chess.server.game.account.ChessAccount;
import com.zuooh.chess.server.game.account.ChessAccountDatabase;
import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.common.html.TableDrawer;
import com.zuooh.common.time.DateTime;

@ManagedResource(description="Search for notifications")
public class ChessNotificationSearcher { 
   
   private final Cache<String, ChessNotificationQuery> recentQueries;
   private final ChessNotificationQueryComparator queryComparator;
   private final ChessNotificationDatabase notificationDatabase;
   private final ChessAccountDatabase accountDatabase;
   private final AtomicBoolean vibrateEnabled;
   private final AtomicLong retryInterval;

   public ChessNotificationSearcher(ChessNotificationDatabase notificationDatabase, ChessAccountDatabase accountDatabase, int retryInterval) {
      this(notificationDatabase, accountDatabase, retryInterval, true);
   }
   
   public ChessNotificationSearcher(ChessNotificationDatabase notificationDatabase, ChessAccountDatabase accountDatabase, int retryInterval, boolean vibrateEnabled) {
      this.recentQueries = new LeastRecentlyUsedCache<String, ChessNotificationQuery>(300);
      this.queryComparator = new ChessNotificationQueryComparator();
      this.vibrateEnabled = new AtomicBoolean(vibrateEnabled);
      this.retryInterval = new AtomicLong(retryInterval);
      this.notificationDatabase = notificationDatabase;   
      this.accountDatabase = accountDatabase;
   }
   
   @ManagedAttribute(description="Determine if vibrate is enabled")
   public synchronized boolean isVibrateEnabled() {
      return vibrateEnabled.get();
   }   
   
   @ManagedOperation(description="Enable or disable vibrate")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="enabled", description="Enable or disable vibrate")
   })
   public synchronized void setVibrateEnabled(boolean enabled) {
      vibrateEnabled.set(enabled);
   }
   
   @ManagedAttribute(description="Determine the retry interval")
   public synchronized long getRetryInterval() {
      return retryInterval.get();
   }   
   
   @ManagedOperation(description="Set the retry interval")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="duration", description="Interval in milliseconds")
   })
   public synchronized void setRetryInterval(long duration) {
      retryInterval.set(duration);
   }    
   
   @ManagedOperation(description="Show recent queries")
   public synchronized String showRecentQueries() throws Exception {
      TableDrawer tableDrawer = new TableDrawer("device", "user", "name", "protocol", "platform", "version", "time");
      List<ChessNotificationQuery> queries = new ArrayList<ChessNotificationQuery>();      
      
      if(!recentQueries.isEmpty()) {
         Set<String> deviceKeys = recentQueries.keySet();
         
         for(String deviceKey : deviceKeys) {            
            ChessNotificationQuery recentQuery = recentQueries.fetch(deviceKey);

            if(recentQuery != null) {
               queries.add(recentQuery);
            }
         }
         Collections.sort(queries, queryComparator);
         
         for(ChessNotificationQuery query : queries) {
            long time = query.getTime();
            String device = query.getDevice();
            String user = query.getUser();
            String protocol = query.getProtocol();
            String platform = query.getPlatform();
            String version = query.getVersion();
            ChessAccount account = accountDatabase.loadAccount(user);
            String name = null;
            
            if(account != null) {
               name = account.getName();
            }               
            tableDrawer.newRow(
                  device, 
                  user, 
                  name, 
                  protocol, 
                  platform, 
                  version, 
                  DateTime.at(time));                                              
         }
      }
      return tableDrawer.drawTable();
   }
   
   public synchronized List<ChessNotification> search(ChessNotificationQuery query) throws Exception {
      long currentTime = System.currentTimeMillis();
      String userId = query.getUser();
      String deviceId = query.getDevice();
      List<ChessNotification>  notifications = notificationDatabase.listUserNotifications(userId, currentTime); // ensure the notifications are due to be sent!!
      boolean vibrate = vibrateEnabled.get();
      long retry = retryInterval.get();
      
      for(ChessNotification notification : notifications) {
         notification.setVibrate(vibrate);
         notification.setRetry(retry);
         notification.setReceived(true);
         notificationDatabase.saveNotification(notification);
      }
      recentQueries.cache(deviceId, query);
      return notifications;      
   }
}
