package com.zuooh.chess.server.game.message;

import com.zuooh.chess.client.event.request.ChessLastMoveQueryEvent;
import com.zuooh.chess.client.event.response.ChessLastMoveEvent;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.message.client.RequestHandler;

public class ChessLastMoveQueryRequestHandler implements RequestHandler<ChessLastMoveQueryEvent> {
   
   private final ChessServerController mainController;

   public ChessLastMoveQueryRequestHandler(ChessServerController mainController) throws Exception { 
      this.mainController = mainController;   
   }

   @Override
   public ChessLastMoveEvent execute(ChessLastMoveQueryEvent event) throws Exception {
      String playerId = event.getUserId();
      String gameId = event.getGameId();
      ChessMove move = mainController.lastMoveMadeBy(gameId, playerId);

      if(playerId != null)  {
         return new ChessLastMoveEvent(move, playerId, gameId);
      }
      return null;
   }
}
