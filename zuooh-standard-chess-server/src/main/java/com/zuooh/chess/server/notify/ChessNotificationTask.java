package com.zuooh.chess.server.notify;

import java.util.List;

import com.zuooh.chess.database.notify.ChessNotification;

public class ChessNotificationTask implements Runnable {   

   private final ChessNotificationSearcher searcher;
   private final ChessNotificationListener listener;
   private final ChessNotificationQuery query;
   
   public ChessNotificationTask(ChessNotificationSearcher searcher, ChessNotificationQuery query, ChessNotificationListener listener) {
      this.searcher = searcher;
      this.listener = listener;
      this.query = query;      
   }
   
   @Override
   public void run() {
      try {
         List<ChessNotification> notification = searcher.search(query);         
         listener.onNotification(notification);
      } catch(Exception cause) {
         listener.onException(cause);
      }
   }
}
