package com.zuooh.chess.server.notify;

import static com.zuooh.chess.server.notify.ChessNotificationKey.FROM_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.FUNCTION_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.GAME_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.ICON_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.KEY_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.MESSAGE_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.PAGE_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.RETRY_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.*;
import static com.zuooh.chess.server.notify.ChessNotificationKey.TITLE_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.TYPE_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.USER_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.VIBRATE_KEY;

import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.notify.ChessNotification;
import com.zuooh.chess.database.notify.ChessNotificationType;

public class ChessNotificationConverter {
   
   public static final Logger LOG = LoggerFactory.getLogger(ChessNotificationConverter.class);
   
   private final ChessNotificationQuery query;
   private final List<String> smallIcons;
   
   public ChessNotificationConverter(ChessNotificationQuery query) {
      this.smallIcons = Arrays.<String>asList("1", "2", "3", "4", "5", "6", "7", "8", "9", "10");
      this.query = query;
   }   

   public Map<String, Object> convert(ChessNotification notification) {         
      Map<String, Object> values = new LinkedHashMap<String, Object>();
      StringBuilder builder = new StringBuilder();
               
      try {              
         if(notification != null) {
            ChessNotificationType type = notification.getType();
            String normalIcon = notification.getNormalIcon();
            String smallIcon = notification.getSmallIcon();
            String text = notification.getText();
            String gameId = notification.getGameId();
            String userId = notification.getUserId();
            String version = query.getVersion();
            boolean vibrate = notification.isVibrate();
            int sequence = notification.getSequence();
            long retry = notification.getRetry();
            
            if(smallIcons.contains(version)) {
               values.put(ICON_KEY, smallIcon);
            } else {
               values.put(ICON_KEY, normalIcon);                        
            }
            builder.append(type);
            builder.append("_");
            builder.append(gameId);
            builder.append("_");
            builder.append(userId);
            builder.append("_");
            builder.append(sequence);
            
            values.put(FROM_KEY, "Chess For Everyone");
            values.put(KEY_KEY, builder); 
            values.put(TYPE_KEY, type);
            values.put(TEXT_KEY, text);                 
            values.put(GAME_KEY, gameId);
            values.put(USER_KEY, userId);             
            values.put(VIBRATE_KEY, vibrate);
            values.put(RETRY_KEY, retry);          
            values.put(ID_KEY, type.type);  
            
            String title = interpolate(type.title, values);
            String page = interpolate(type.page, values);
            String function = interpolate(type.function, values);
            String message = interpolate(type.template, values);
            String home = interpolate(type.home, values);            
            
            values.put(TITLE_KEY, title);
            values.put(PAGE_KEY, page);
            values.put(HOME_KEY, home);               
            values.put(FUNCTION_KEY, function);
            values.put(MESSAGE_KEY, message);       
            
         }
      } catch(Exception e) {
         LOG.info("Error converting notification", e);
      }
      return values;
   }
   
   private String interpolate(String template, Map<String, Object> values) {
      try {            
         if(template != null) {
            Set<String> keys = values.keySet();
            
            for(String key : keys) {
               int index = template.indexOf('%');
               
               if(index < 0) {
                  return template;
               }
               Object value = values.get(key);            
               String token = String.valueOf(value);
           
               template = template.replace("%{" + key + "}", token);
               template = template.replace("%" + key + "", token);               
            }
         }
      } catch(Exception e) {
         LOG.info("Error interpolating value", e);
      }
      return template;
   } 
}
