package com.zuooh.chess.server.notify;

import java.util.Comparator;

public class ChessNotificationQueryComparator implements Comparator<ChessNotificationQuery> {

   @Override
   public int compare(ChessNotificationQuery left, ChessNotificationQuery right) {
      Long leftTime = left.getTime();
      Long rightTime = right.getTime();
      
      return rightTime.compareTo(leftTime);
   }
}
