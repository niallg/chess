package com.zuooh.chess.server.game.message;

import java.util.List;

import com.zuooh.chess.client.event.request.ChessRankQueryEvent;
import com.zuooh.chess.client.event.response.ChessRankResultEvent;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.message.client.RequestHandler;

public class ChessRankQueryRequestHandler implements RequestHandler<ChessRankQueryEvent> {

   private final ChessServerController mainController;

   public ChessRankQueryRequestHandler(ChessServerController mainController) throws Exception { 
      this.mainController = mainController;   
   }

   @Override
   public ChessRankResultEvent execute(ChessRankQueryEvent event) throws Exception {
      List<ChessUser> profiles = mainController.findBestUsers(40);

      if(profiles != null)  {
         ChessRankResultEvent resultEvent = new ChessRankResultEvent();
         
         for(ChessUser profile : profiles){
            resultEvent.addUser(profile);
         }
         return resultEvent;
      }
      return new ChessRankResultEvent();
   }
}
