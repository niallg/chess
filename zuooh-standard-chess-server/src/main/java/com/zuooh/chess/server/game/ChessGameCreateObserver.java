package com.zuooh.chess.server.game;

import com.zuooh.chess.database.state.ChessGameState;

public interface ChessGameCreateObserver {
   void onCreate(ChessGameState gameState);
}
