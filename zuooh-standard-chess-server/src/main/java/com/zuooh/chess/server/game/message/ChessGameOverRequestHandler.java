package com.zuooh.chess.server.game.message;

import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.client.event.ChessGameOverEvent;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.message.client.RequestHandler;

public class ChessGameOverRequestHandler implements RequestHandler<ChessGameOverEvent> {
   
   private final ChessServerController mainController;

   public ChessGameOverRequestHandler(ChessServerController mainController) throws Exception { 
      this.mainController = mainController;   
   }

   @Override
   public ChessGameOverEvent execute(ChessGameOverEvent event) throws Exception {
      ChessBoardResult result = event.getResult();
      String playerId = event.getPlayerId();
      String gameId = event.getGameId();
      
      if(result.isLose()) {
         ChessGameCriteria criteria = mainController.gameDetails(gameId, playerId);
      
         if(criteria != null) {
            ChessUser opponentProfile = criteria.getUser();
            String opponentId = opponentProfile.getKey();
            
            mainController.gameOver(gameId, playerId, opponentId);
         }
      } else if(result.isWin()) {
         mainController.gameOver(gameId, playerId, playerId);
      } else {
         mainController.gameOver(gameId, playerId);
      }
      return event;
   }
}
