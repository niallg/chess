package com.zuooh.chess.server.game.bot;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessServerBot {   
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessServerBot.class);

   private final ChessServerBotPublisher botPublisher;
   private final ChessMoveEngine computerEngine;
   private final ChessUser botProfile;
   private final ChessBoard chessBoard;
   private final String opponentId;
   private final String gameId;

   public ChessServerBot(ChessServerBotPublisher botPublisher, ChessBoard chessBoard, ChessMoveEngine computerEngine, ChessUser botProfile, String gameId, String opponentId) {
      this.botPublisher = botPublisher;
      this.computerEngine = computerEngine;
      this.botProfile = botProfile;
      this.chessBoard = chessBoard; 
      this.opponentId = opponentId;
      this.gameId = gameId;
   }
   
   public void start() {
      ChessSide computerSide = computerEngine.getSide();
      
      if(computerSide == ChessSide.WHITE) {
         opponentMoveMade(null);
      }
   }
   
   public void moveMade(ChessMoveState gameMove) {
      String moveGameId = gameMove.getGameId();
      
      if(gameId.equals(moveGameId)) {
         String playerThatMadeMove = gameMove.getUserId();
         String botName = botProfile.getKey();
                
         if(!playerThatMadeMove.equals(botName)) { 
            ChessBoardPosition fromPosition = gameMove.getFromPosition();
            ChessBoardPosition toPosition = gameMove.getToPosition();
            ChessSide pieceColor = gameMove.getUserSide();           
            int boardCount = chessBoard.getChangeCount();
            int opponentCount = gameMove.getChangeCount(); // change for the move!

            if(opponentCount != boardCount) {
               throw new IllegalStateException("Move from " + playerThatMadeMove + " has a change of " + opponentCount + " but board is " + boardCount);
            }
            ChessMove chessMove = new ChessMove(fromPosition, toPosition, pieceColor, opponentCount);
            
            updateBoard(chessMove); // make move on board
            opponentMoveMade(chessMove); // process move for engine
         }
      }
   }
   
   public void updateBoard(ChessMove moveMade) {
      int boardCount = chessBoard.getChangeCount();
      int moveCount = moveMade.getChange();
      
      if(boardCount != moveCount) {
         throw new IllegalStateException("Move " + moveMade + " has a count of " + moveCount + " not " + boardCount);
      }
      ChessBoardMove boardMove = chessBoard.getBoardMove(moveMade);      
      boardMove.makeMove();
   }
   
   public void startGame(ChessGameState gameState) {
      String challengerId = gameState.getChallengerId();
      String botId = botProfile.getKey();
      
      if(botId.equals(challengerId)) {
         LOG.info("[" + gameId + "] Start notification");
      }
   }     

   public void finishGame(ChessGameState gameState) {
      String winnerId = gameState.getWinnerId();
      String botId = botProfile.getKey();
      
      if(winnerId == null) {
         LOG.info("[" + gameId + "] Draw notification");
      } else if(winnerId.equals(opponentId)) {
         LOG.info("[" + gameId + "] Loss notification");
      } else if(winnerId.equals(botId)) {
         LOG.info("[" + gameId + "] Win notification");
      } else {
         LOG.info("[" + gameId + "] Invalid notification");
      }
   }   
   
   private void opponentMoveMade(ChessMove opponentMove) {
      computerEngine.makeMove(chessBoard, opponentMove);             
      
      ChessOpponentStatus engineStatus = computerEngine.getEngineStatus();
      String botId = botProfile.getKey();
      
      if(engineStatus == ChessOpponentStatus.THINKING) {
         throw new IllegalStateException("Chess engine is still thinking after " + opponentMove + " was made");
      }
      if(engineStatus == ChessOpponentStatus.DRAW) {
         staleMate();
      } else if(engineStatus == ChessOpponentStatus.LOSE) {
         checkMate(opponentId);
      } else if(engineStatus == ChessOpponentStatus.RESIGN) {
         checkMate(opponentId);         
      } else if(engineStatus == ChessOpponentStatus.WIN) {
         checkMate(botId);
      } else {
         ChessMove computerMove = computerEngine.getLastMove();
   
         if(computerMove == null) {
            throw new IllegalStateException("Chess engine has no valid move with status of " + engineStatus);
         }          
         int boardCount = chessBoard.getChangeCount();
         int computerCount = computerMove.getChange();

         if(computerCount != boardCount) {
            throw new IllegalStateException("Computer move " + computerMove + " has " + computerCount + " but board has " + boardCount + " with " + engineStatus);
         }
         updateBoard(computerMove);               
         makeMove(computerMove);     
      }      
   }     
   
   private void makeMove(ChessMove moveToMake) {
      String botId = botProfile.getKey();
      ChessSide computerSide = computerEngine.getSide();
      ChessSide playerSide = computerSide.oppositeSide();
      
      if(moveToMake.side == playerSide) {
         throw new IllegalStateException("Move can not be made for opponent");
      }
      try {
         if(ChessCheckAnalyzer.checkMatePossible(chessBoard)) {
            botPublisher.makeMove(gameId, moveToMake, botId);
            
            if(ChessCheckAnalyzer.checkMate(chessBoard, playerSide)) {
               checkMate(botId);
            }
         } else {
            insufficientMaterial();  // this might confuse some players!
         }
      } catch(Exception e) {
         throw new IllegalStateException("Problem making move " + moveToMake);
      }
   }
   
   private void insufficientMaterial() {
      String botId = botProfile.getKey();
      
      try {
         LOG.info("[" + gameId + "] Draw due to insufficient material");
         botPublisher.gameOver(gameId, botId);
      } catch(Exception e) {
         throw new IllegalStateException("Could not report draw for " + gameId);
      }
      gameOver();
   }
   
   private void staleMate() {
      String botId = botProfile.getKey();
      
      try {
         LOG.info("[" + gameId + "] Draw due to stale mate");
         botPublisher.gameOver(gameId, botId);
      } catch(Exception e) {
         throw new IllegalStateException("Could not report check mate for " + gameId);
      }
      gameOver();
   }
   
   private void checkMate(String winnerId) {
      String botId = botProfile.getKey();
      
      try {
         if(winnerId.equals(opponentId)) {
            LOG.info("[" + gameId + "] Loss due to check mate");
         } else {
            LOG.info("[" + gameId + "] Win due to check mate");
         }
         botPublisher.gameOver(gameId, botId, winnerId);
      } catch(Exception e) {
         throw new IllegalStateException("Could not report check mate for " + gameId);
      }
      gameOver();
   }

   private void gameOver() {
      String changeText = ChessTextBoardDrawer.drawBoard(chessBoard);

      LOG.info("[" + gameId + "] Game over!!!");
      LOG.info(changeText);
   }
}
