package com.zuooh.chess.server.notify;

public class ChessNotificationQuery {    

   private final String user;
   private final String protocol;
   private final String version;
   private final String platform;
   private final String device;
   private final long time;
   
   public ChessNotificationQuery(String user, String protocol, String platform, String device, String version, long time) {
      this.user = user;
      this.protocol = protocol;
      this.platform = platform;
      this.device = device;
      this.version = version;
      this.time = time;
   }
   
   public long getTime() {
      return time;
   }

   public String getUser() {
      return user;
   }

   public String getProtocol() {
      return protocol;
   }

   public String getVersion() {
      return version;
   }

   public String getPlatform() {
      return platform;
   }

   public String getDevice() {
      return device;
   }
}
