package com.zuooh.chess.server.mail;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserType;
import com.zuooh.common.mail.ImageAttachment;
import com.zuooh.common.mail.MailMessage;
import com.zuooh.common.mail.MailRecipient;
import com.zuooh.common.mail.MailRecipientType;
import com.zuooh.common.mail.MailSender;
import com.zuooh.common.mail.MailType;
import com.zuooh.common.template.StringTemplateEngine;
import com.zuooh.common.template.TemplateModel;

@ManagedResource(description="Used for mail correspondence")
public class ChessServerMailHandler {  

   private static final Logger LOG = LoggerFactory.getLogger(ChessServerMailHandler.class);
   
   private final StringTemplateEngine templateEngine;
   private final ChessServerMailResource resource;
   private final MailMessage mailMessage;
   private final MailSender mailSender;
   private final String verifyFile;
   private final String passwordFile;
   private final String logoFile;
   
   public ChessServerMailHandler(ChessServerMailResource resource, MailSender mailSender, MailMessage mailMessage, String verifyFile, String passwordFile, String logoFile) {
      this.templateEngine = new StringTemplateEngine();
      this.passwordFile = passwordFile;
      this.mailMessage = mailMessage;
      this.mailSender = mailSender;
      this.verifyFile = verifyFile;
      this.resource = resource;
      this.logoFile = logoFile;
   }   
   
   @ManagedOperation(description="Send test verification mail")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="playerId", description="Test player mail"),
      @ManagedOperationParameter(name="playerName", description="Test player name"),
      @ManagedOperationParameter(name="playerMail", description="Test player mail"),      
      @ManagedOperationParameter(name="password", description="Test password")        
   })
   public void sendTestVerificationMail(String playerId, String playerName, String playerMail, String password) {
      ChessUserData data = new ChessUserData();
      ChessUser profile = new ChessUser(data, ChessUserType.NORMAL, playerId, playerName, playerMail, password);
      
      sendVerificationMail(profile);      
   }
   
   @ManagedOperation(description="Send test password reset mail")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="playerId", description="Test player mail"),
      @ManagedOperationParameter(name="playerName", description="Test player name"),
      @ManagedOperationParameter(name="playerMail", description="Test player mail"),         
      @ManagedOperationParameter(name="password", description="Test password")        
   })
   public void sendTestPasswordResetMail(String playerId, String playerName, String playerMail, String password) {
      ChessUserData data = new ChessUserData();
      ChessUser profile = new ChessUser(data, ChessUserType.NORMAL, playerId, playerName, playerMail, password);
      
      sendResetPasswordMail(profile, password);      
   }
   
   public void sendResetPasswordMail(ChessUser profile, String password) {   
      String address = profile.getMail();
      
      if(address != null) {
         try {         
            MailMessage mailCopy = mailMessage.getMessage();         
            Path path = Paths.get(passwordFile);
            byte[] content = Files.readAllBytes(path);
            String text = new String(content, "UTF-8");
            Map<String, Object> attributes = new LinkedHashMap<String, Object>();
            TemplateModel model = new TemplateModel(attributes);
            MailRecipient recipient = new MailRecipient(MailRecipientType.TO, address);         
            String token = UUID.randomUUID().toString();
            
            resource.register(token, profile);
            model.setAttribute("password", password);
            model.setAttribute("token", token);         
            model.setAttribute("profile", profile);
            
            String result = templateEngine.renderTemplate(model,text);
            
            if(result.contains("logo.png")) {
               File file = new File(logoFile);
               ImageAttachment attachment = new ImageAttachment(file, "logo.png");
               
               mailCopy.setAttachment(attachment);
            }
            mailCopy.setRecipient(recipient);
            
            if(passwordFile.endsWith(".html")) {
               mailCopy.setType(MailType.HTML);
            } else {
               mailCopy.setType(MailType.TEXT);
            }
            mailCopy.setSubject("Password Reset");
            mailCopy.setBody(result);
            mailSender.sendMessage(mailCopy);
         } catch(Exception e) {
            LOG.info("Error sending mail to " + address, e);
         }
      }
   }   
   
   public void sendVerificationMail(ChessUser profile) {   
      String address = profile.getMail();
      
      if(address != null) {
         try {         
            MailMessage mailCopy = mailMessage.getMessage();         
            Path path = Paths.get(verifyFile);
            byte[] content = Files.readAllBytes(path);
            String text = new String(content, "UTF-8");
            Map<String, Object> attributes = new LinkedHashMap<String, Object>();
            TemplateModel model = new TemplateModel(attributes);
            MailRecipient recipient = new MailRecipient(MailRecipientType.TO, address);
            String token = UUID.randomUUID().toString();
            
            resource.register(token, profile);
            model.setAttribute("token", token);
            model.setAttribute("profile", profile);
            
            String result = templateEngine.renderTemplate(model,text);         
            
            if(result.contains("logo.png")) {
               File file = new File(logoFile);
               ImageAttachment attachment = new ImageAttachment(file, "logo.png");
               
               mailCopy.setAttachment(attachment);
            }
            mailCopy.setRecipient(recipient);
            
            if(verifyFile.endsWith(".html")) {
               mailCopy.setType(MailType.HTML);
            } else {
               mailCopy.setType(MailType.TEXT);
            }
            mailCopy.setSubject("Account Verification");
            mailCopy.setBody(result);
            mailSender.sendMessage(mailCopy);
         } catch(Exception e) {
            LOG.info("Error sending mail to " + address, e);
         }
      }
   }
      
}
