package com.zuooh.chess.server.game;

import java.util.List;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessGameStateDatabase;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.server.game.account.ChessAccount;
import com.zuooh.chess.server.game.account.ChessAccountDatabase;
import com.zuooh.chess.server.game.rank.ChessRatingUpdater;
import com.zuooh.common.task.Task;
import com.zuooh.common.time.DateTime;

@ManagedResource(description="Clean expired games")
public class ChessGameStateCleaner implements Task {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessRatingUpdater.class);
   
   private final ChessAccountDatabase accountDatabase;
   private final ChessGameStateDatabase gameDatabase;
   private final StaleGameReaper gameReaper;
   private final AtomicLong lastUpdate;
   private final Executor executor;
   private final long cleanFrequency;

   public ChessGameStateCleaner(ChessGameStateDatabase gameDatabase, ChessAccountDatabase accountDatabase, Executor executor) {
      this(gameDatabase, accountDatabase, executor, 1 * 60 * 60 * 1000);
   }
   
   public ChessGameStateCleaner(ChessGameStateDatabase gameDatabase, ChessAccountDatabase accountDatabase, Executor executor, long cleanFrequency) {
      this.gameReaper = new StaleGameReaper(3 * 60 * 60 * 1000);
      this.lastUpdate = new AtomicLong(); 
      this.accountDatabase = accountDatabase;
      this.gameDatabase = gameDatabase;    
      this.cleanFrequency = cleanFrequency;
      this.executor = executor;;
   }
   
   @ManagedAttribute(description="Frequency of clean intervals")
   public long getCleanFrequency() {
      return cleanFrequency;
   }   
   
   @ManagedAttribute(description="Time last clean occured")
   public String getLastClean() {
      long rankTime = lastUpdate.get();
      DateTime rankDate = DateTime.at(rankTime);
      
      return rankDate.toString();
   }   
   
   @Override
   public long executeTask() {
      long currentTime = System.currentTimeMillis();
      
      try {
         executor.execute(gameReaper); // execute on game thread
      } catch(Exception e) {
         LOG.info("Could not clean games", e);
      } finally {
         lastUpdate.set(currentTime);
      }
      return cleanFrequency;
   }   
   
   private void cleanGames(long gracePeriod) {
      try {
         List<ChessGameState> gameStates = gameDatabase.listGames();
         
         for(ChessGameState gameState : gameStates) {
            ChessGameStatus gameStatus = gameState.getGameStatus();
            
            if(!gameStatus.isGameOver()) { // let the rank updater delete concluded games
               long creationTime = gameState.getCreationTime();
               long gameDuration = gameState.getGameDuration();
               long maximumDuration = gameDuration * 2; // white duration + black duration
               long expiryTime = creationTime + maximumDuration;
               long purgeTime = expiryTime + gracePeriod;
               long currentTime = System.currentTimeMillis();             
               
               if(purgeTime < currentTime) {
                  String gameId = gameState.getGameId();
                  String ownerId = gameState.getOwnerId();
                  String challengerId = gameState.getChallengerId();
                  ChessSide ownerSide = gameState.getOwnerSide();
                  
                  if(challengerId != null) {                                       
                     ChessMoveState moveState = gameDatabase.lastMove(gameId);
                     
                     if(moveState == null) { // no moves made
                        if(ownerSide == ChessSide.WHITE) {
                           LOG.info("Clean gameId=[" + gameId + "] ownerId=["+ownerId+"] ownerSide=["+ownerSide+"] gameStatus=["+gameStatus+"] winnerId=["+challengerId+"] moveCount=[0]");
                           cleanGame(gameState, challengerId); 
                        } else {
                           if(gameStatus.isAccepted()) {
                              LOG.info("Clean gameId=[" + gameId + "] ownerId=["+ownerId+"] ownerSide=["+ownerSide+"] gameStatus=["+gameStatus+"] winnerId=["+ownerId+"]");
                              cleanGame(gameState, ownerId); 
                           } else {
                              LOG.info("Purge gameId=[" + gameId + "] ownerId=["+ownerId+"] ownerSide=["+ownerSide+"] gameStatus=["+gameStatus+"]");
                              gameDatabase.deleteGame(gameState); // challenger did not accept game
                           }
                        }                      
                     } else {
                        ChessMove lastMove = moveState.getMove();
                        ChessSide moveSide = lastMove.getSide();
                        
                        if(moveSide == ownerSide) { // last to move wins due to timeout
                           LOG.info("Clean gameId=[" + gameId + "] ownerId=["+ownerId+"] ownerSide=["+ownerSide+"] lastMove=["+lastMove+"] winnerId=["+ownerId+"]");
                           cleanGame(gameState, ownerId); ; // owner was last to move
                        } else {
                           LOG.info("Clean gameId=[" + gameId + "] ownerId=["+ownerId+"] ownerSide=["+ownerSide+"] lastMove=["+lastMove+"] winnerId=["+challengerId+"]");
                           cleanGame(gameState, challengerId); 
                        }
                     }
                  } else {
                     LOG.info("Purge gameId=[" + gameId + "] ownerId=["+ownerId+"] ownerSide=["+ownerSide+"] gameStatus=["+gameStatus+"]");                     
                     gameDatabase.deleteGame(gameState); // no challenger
                  }
               }
            }
         }
      } catch(Exception e) {
         LOG.info("Could not clean games", e);
      }
   }   
   
   private void cleanGame(ChessGameState game, String winnerName) throws Exception {
      String ownerName = game.getOwnerId();
      String challengerName = game.getChallengerId();
      ChessAccount ownerAccount = accountDatabase.loadAccount(ownerName);
      ChessAccount challengerAccount = accountDatabase.loadAccount(challengerName);
      ChessSide ownerColor = game.getOwnerSide();      
  
      if(ownerAccount != null) { 
         int ownerBlackGames = ownerAccount.getBlackGames();
         int ownerWhiteGames = ownerAccount.getWhiteGames();
         int challengerBlackGames = challengerAccount.getBlackGames();
         int challengerWhiteGames = challengerAccount.getWhiteGames();
         long currentTime = System.currentTimeMillis();
         
         if(ownerColor == ChessSide.WHITE) {
            ownerAccount.setWhiteGames(ownerWhiteGames + 1);
            challengerAccount.setBlackGames(challengerBlackGames + 1);
         } else {
            ownerAccount.setBlackGames(ownerBlackGames + 1);
            challengerAccount.setWhiteGames(challengerWhiteGames + 1);
         }      
         ownerAccount.setLastSeenOnline(currentTime);
         challengerAccount.setLastSeenOnline(currentTime);;
      }
      game.setWinnerId(winnerName);
      game.setGameStatus(ChessGameStatus.GAME_OVER);
      gameDatabase.saveGame(game);      
   }   
   
   private class StaleGameReaper implements Runnable {

      private final long gracePeriod;
      
      public StaleGameReaper(long gracePeriod) {
         this.gracePeriod = gracePeriod;
      }

      public void run() {
         try {
            cleanGames(gracePeriod);
         } catch(Exception e) {
            LOG.info("Could not clean games", e);
         }
      }
   }
}
