package com.zuooh.chess.server.game.bot;

import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.server.game.ChessGameChangeObserver;

public class ChessServerBotNotifier implements ChessGameChangeObserver {

   private static final Logger LOG = LoggerFactory.getLogger(ChessServerBotNotifier.class);

   private final ChessServerBot bot;
   private final Executor executor;

   public ChessServerBotNotifier(ChessServerBot bot, Executor executor) {
      this.executor = executor;
      this.bot = bot;
   }
   
   @Override
   public void onStart(ChessGameState gameState) {
      StartNotification notification = new StartNotification(gameState);
      executor.execute(notification);

   }   

   @Override
   public void onMove(ChessGameState gameState, ChessMoveState chessMove) {
      MoveNotification notification = new MoveNotification(chessMove);
      executor.execute(notification);

   }

   @Override
   public void onFinish(ChessGameState gameState) {
      GameOverNotification notification = new GameOverNotification(gameState);
      executor.execute(notification);

   }
   
   private class StartNotification implements Runnable {

      private final ChessGameState game;

      public StartNotification(ChessGameState game) {
         this.game = game;
      }

      @Override
      public void run() {
         try {
            bot.startGame(game);
         } catch (Exception e) {
            LOG.info("Error on bot notification", e);
         }
      }
   }   

   private class MoveNotification implements Runnable {

      private final ChessMoveState move;

      public MoveNotification(ChessMoveState move) {
         this.move = move;
      }

      @Override
      public void run() {
         try {
            bot.moveMade(move);
         } catch (Exception e) {
            LOG.info("Error on bot notification", e);
         }
      }
   }

   public class GameOverNotification implements Runnable {

      private final ChessGameState game;

      public GameOverNotification(ChessGameState game) {
         this.game = game;
      }

      @Override
      public void run() {
         try {
            bot.finishGame(game);
         } catch (Exception e) {
            LOG.info("Error on bot notification", e);
         }
      }
   }
}
