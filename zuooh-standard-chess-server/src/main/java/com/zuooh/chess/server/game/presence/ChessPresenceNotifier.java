package com.zuooh.chess.server.game.presence;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.Executor;

import org.jfree.util.Log;

public class ChessPresenceNotifier {
   
   private final Set<ChessPresenceListener> listeners;
   private final Executor executor;
   
   public ChessPresenceNotifier(Executor executor) {
      this.listeners = new CopyOnWriteArraySet<ChessPresenceListener>();
      this.executor = executor;
   }
   
   public void registerListener(ChessPresenceListener listener) {
      listeners.add(listener);
   }
   
   public void notifyUpdate(ChessPresence presence) {
      PresenceDistributor distributor = new PresenceDistributor(presence, false);
      executor.execute(distributor);
   }
   
   public void notifyExpiry(ChessPresence presence) {
      PresenceDistributor distributor = new PresenceDistributor(presence, true);
      executor.execute(distributor);
   }
   
   private class PresenceDistributor implements Runnable {
      
      private final ChessPresence presence;
      private final boolean expiry;
      
      public PresenceDistributor(ChessPresence presence, boolean expiry) {
         this.presence = presence;
         this.expiry = expiry;
      }
      
      @Override
      public void run() {
         for(ChessPresenceListener listener : listeners) {
            try {
               if(expiry) {
                  listener.onPresenceExpiry(presence);
               } else {
                  listener.onPresenceUpdate(presence);
               }
            }catch(Exception e){
               Log.info("Error sending presence notification", e);
            }
         }
      } 
   }
}
