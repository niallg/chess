package com.zuooh.chess.server.notify;

import static com.zuooh.chess.server.notify.ChessNotificationKey.DEVICE_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.PLATFORM_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.PROTOCOL_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.USER_KEY;
import static com.zuooh.chess.server.notify.ChessNotificationKey.VERSION_KEY;

import org.simpleframework.http.Request;

public class ChessNotificationReader {    

   public ChessNotificationQuery readNotification(Request request) {
      String user = request.getParameter(USER_KEY);
      String protocol = request.getParameter(PROTOCOL_KEY);
      String version = request.getParameter(VERSION_KEY);
      String platform = request.getParameter(PLATFORM_KEY);
      String device = request.getParameter(DEVICE_KEY);
      long time = System.currentTimeMillis();
      
      return new ChessNotificationQuery(user, protocol, platform, device, version, time);
   }
}
