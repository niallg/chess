package com.zuooh.chess.server.game;

import java.util.List;

import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.database.state.ChessGameStateDatabase;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.engine.ChessMove;

public class ChessGameStateReader {   
   
   private static final String[][] SET = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};
   
   private final ChessGameStateDatabase gameDatabase;
   
   public ChessGameStateReader(ChessGameStateDatabase gameDatabase) {
      this.gameDatabase = gameDatabase;
   }
   
   public ChessBoard readGame(String gameId) throws Exception {
      return readGame(gameId, Integer.MAX_VALUE);
   }
   
   public ChessBoard readGame(String gameId, int limit) throws Exception {
      ChessTextPieceSet chessSet = new ChessTextPieceSet(SET);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      
      if(gameId != null) {
         List<ChessMoveState> moveStates = gameDatabase.listMoves(gameId, limit);

         for(ChessMoveState moveState : moveStates) {
            ChessMove move = moveState.getMove();
            ChessBoardMove boardMove = chessBoard.getBoardMove(move);
            
            boardMove.makeMove();
         }         
      }
      return chessBoard;      
   }
   
  
}
