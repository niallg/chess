package com.zuooh.chess.server.game.account;

import java.util.List;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.common.html.TableDrawer;
import com.zuooh.common.time.DateTime;

@ManagedResource(description="Monitor existing accounts")
public class ChessAccountMonitor {

   private final ChessAccountDatabase database;
   private final int limit;
   
   public ChessAccountMonitor(ChessAccountDatabase database) {
      this(database, 2000);
   }
   
   public ChessAccountMonitor(ChessAccountDatabase database, int limit) {
      this.database = database;
      this.limit = limit;
   }
   
   @ManagedOperation(description="Show current accounts")
   public String showAccounts() throws Exception {
      return showAccounts(limit);
   }
   
   @ManagedOperation(description="Show current accounts")
   @ManagedOperationParameters({     
      @ManagedOperationParameter(name="limit", description="The number of accounts to show")
   })
   public String showAccounts(int limit) throws Exception {
      List<ChessAccount> list = database.listAccounts(limit);
      
      if(!list.isEmpty()) {
         TableDrawer drawer = new TableDrawer("playerId", "playerName", "type", "password", "whiteGames", "blackGames", "lastSeen");         
         
         for(ChessAccount account : list) {
            ChessAccountType type = account.getType();
            String key = account.getKey();
            String playerName = account.getName();
            String password = account.getPassword();
            int whiteGames = account.getWhiteGames();
            int blackGames = account.getBlackGames();
            long lastSeen = account.getLastSeenOnline();
            DateTime lastDate = DateTime.at(lastSeen);                  
            
            drawer.newRow(key, playerName, type, password, whiteGames, blackGames, lastDate);
         }
         return drawer.drawTable();
      }
      return null;      
   }
}
