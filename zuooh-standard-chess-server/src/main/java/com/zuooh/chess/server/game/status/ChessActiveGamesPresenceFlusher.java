package com.zuooh.chess.server.game.status;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.chess.server.game.presence.ChessPresence;
import com.zuooh.chess.server.game.presence.ChessPresenceAdapter;

public class ChessActiveGamesPresenceFlusher extends ChessPresenceAdapter {

   private static final Logger LOG = LoggerFactory.getLogger(ChessActiveGamesPresenceFlusher.class);
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;

   public ChessActiveGamesPresenceFlusher(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.mainController = mainController;
   }

   @Override
   public void onPresenceUpdate(ChessPresence presence) {
      String playerId = presence.getPresenceId();
      
      try {
         List<ChessGame> chessGames = mainController.statusUpdate(playerId);
         
         if(!chessGames.isEmpty()) {
            ChessListOfGamesEvent event = new ChessListOfGamesEvent(chessGames);
            serverPublisher.publishNotification(event, playerId);
         }
      }catch(Exception e) {
         LOG.info("Unable to flush available games for " + playerId, e);
      }
   }

}
