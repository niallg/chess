package com.zuooh.chess.server.game.account;

import java.util.Comparator;

public class ChessAccountRankComparator implements Comparator<ChessAccount> {
   
   private final boolean bestFirst;
   
   public ChessAccountRankComparator() {
      this(true);
   }
   
   public ChessAccountRankComparator(boolean bestFirst) {
      this.bestFirst = bestFirst;
   }
   
   public int compare(ChessAccount left, ChessAccount right) {
      if(bestFirst) {
         return compareBestFirst(left, right);
      }
      return compareWorstFirst(left, right);
   }
   
   public static int compareWorstFirst(ChessAccount left, ChessAccount right) {
      return compareBestFirst(right, left);
   }
   
   public static int compareBestFirst(ChessAccount left, ChessAccount right) {
      Integer leftRank = left.getRank();
      Integer rightRank = right.getRank();
      
      if(leftRank == 0) { // 0 is unranked so give it worst value
         leftRank = Integer.MAX_VALUE;
      }
      if(rightRank == 0) {
         rightRank = Integer.MAX_VALUE;
      }
      return leftRank.compareTo(rightRank);
   }

}
