package com.zuooh.chess.server.game;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.event.ChessEvent;
import com.zuooh.chess.server.game.presence.ChessPresenceCache;
import com.zuooh.message.client.publish.NotificationPublisher;

public class ChessServerPublisher {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessServerPublisher.class);
   
   private final NotificationPublisher notificationPublisher;
   private final ChessPresenceCache presenceCache;
   private final boolean enable;
   
   public ChessServerPublisher(ChessPresenceCache presenceCache, NotificationPublisher notificationPublisher) {
      this(presenceCache, notificationPublisher, true);
   }
   
   public ChessServerPublisher(ChessPresenceCache presenceCache, NotificationPublisher notificationPublisher, boolean enable) {
      this.notificationPublisher = notificationPublisher;
      this.presenceCache = presenceCache; 
      this.enable = enable;
   }
   
   public void publishNotification(ChessEvent event, String playerId) {
      try {
         if(enable) {
            if(presenceCache.currentlyPresent(playerId)) {
               notificationPublisher.publish(event, playerId);
            }
         }
      } catch(Exception e){
         LOG.info("Could not publihs event to '" + playerId + "'", e);
      }
   }
}
