package com.zuooh.chess.server.game.status;

import java.util.List;

import com.zuooh.chess.client.event.request.ChessRejectGameEvent;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.RequestHandler;

public class ChessRejectGameRequestHandler implements RequestHandler<ChessRejectGameEvent> {
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;
   
   public ChessRejectGameRequestHandler(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.mainController = mainController;   
   }
   
   @Override
   public ChessRejectGameEvent execute(ChessRejectGameEvent event) throws Exception {
      String gameId = event.getGameId();
      ChessGame game = mainController.rejectGame(gameId);
      
      if(game != null) {
         String ownerId = game.getOwnerId();
         List<ChessGame> chessGames = mainController.statusUpdate(ownerId);
         
         if(!chessGames.isEmpty()) {
            ChessListOfGamesEvent listEvent = new ChessListOfGamesEvent(chessGames);
            serverPublisher.publishNotification(listEvent, ownerId); // if they are online tell them straight away
         }
      }
      return event; // perhaps we should tell the game creator here!
   }
}
