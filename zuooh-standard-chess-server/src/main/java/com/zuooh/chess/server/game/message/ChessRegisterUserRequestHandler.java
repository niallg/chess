package com.zuooh.chess.server.game.message;

import java.util.List;

import com.zuooh.chess.client.event.request.ChessRegisterUserEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.mail.ChessServerMailHandler;
import com.zuooh.message.client.RequestHandler;

public class ChessRegisterUserRequestHandler implements RequestHandler<ChessRegisterUserEvent> {

   private final ChessServerMailHandler mailHandler;
   private final ChessServerController mainController;

   public ChessRegisterUserRequestHandler(ChessServerController mainController, ChessServerMailHandler mailHandler) throws Exception { 
      this.mainController = mainController;   
      this.mailHandler = mailHandler;     
   }

   @Override
   public ChessUserValidationEvent execute(ChessRegisterUserEvent event) throws Exception {
      ChessUser user = event.getUser();
      String userName = user.getName();
      List<ChessUser> existingUsers = mainController.findMatchingUsers(userName);
      
      for(ChessUser existingUser : existingUsers) {
         String profileName = existingUser.getName();
         
         if(profileName.equalsIgnoreCase(userName)) {
            return new ChessUserValidationEvent(ChessUserValidation.USER_NAME_REGISTERED, null); 
         }         
      }       
      mainController.saveUser(user);            
      return new ChessUserValidationEvent(ChessUserValidation.USER_REGISTERED, user);        
   }
}
