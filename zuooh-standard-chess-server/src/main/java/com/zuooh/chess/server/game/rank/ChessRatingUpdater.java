package com.zuooh.chess.server.game.rank;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.concurrent.atomic.AtomicLong;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessGameStateDatabase;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.server.game.account.ChessAccount;
import com.zuooh.chess.server.game.account.ChessAccountDatabase;
import com.zuooh.chess.server.game.account.ChessAccountType;
import com.zuooh.common.task.Task;
import com.zuooh.common.time.DateTime;

@ManagedResource(description="Task for updating player ranks")
public class ChessRatingUpdater implements Task {

   private static final Logger LOG = LoggerFactory.getLogger(ChessRatingUpdater.class);
   
   private final ChessAccountDatabase accountDatabase;
   private final ChessGameStateDatabase gameDatabase;
   private final AtomicLong lastUpdate;
   private final long frequency;
   
   public ChessRatingUpdater(ChessGameStateDatabase gameDatabase, ChessAccountDatabase accountDatabase, long frequency) {
      this.lastUpdate = new AtomicLong();
      this.accountDatabase = accountDatabase;  
      this.gameDatabase = gameDatabase;    
      this.frequency = frequency;
   }
   
   @ManagedAttribute(description="Time last ranking occured")
   public long getRankUpdateFrequency() {
      return frequency;
   }   
   
   @ManagedAttribute(description="Time last ranking occured")
   public String getLaskRankUpdate() {
      long rankTime = lastUpdate.get();
      DateTime rankDate = DateTime.at(rankTime);
      
      return rankDate.toString();
   }   
   
   @Override
   public long executeTask() {
      long currentTime = System.currentTimeMillis();
      
      try {
         updateRanks();
      } catch(Exception e) {
         LOG.info("Could not update ranks", e);
      } finally {
         lastUpdate.set(currentTime);
      }
      return frequency;
   }   
   
   @ManagedOperation(description="Update player ranks")
   public void updateRanks() throws Exception {
      List<ChessGameState> games = gameDatabase.listGames();
      List<ChessAccount> accounts = accountDatabase.listAccounts();
      
      if(!games.isEmpty()) {
         Queue<ChessRating> orderedRatings = new PriorityQueue<ChessRating>();
         Map<String, ChessRating> accountRatings = new LinkedHashMap<String, ChessRating>();
         Map<String, ChessAccount> activeAccounts = new LinkedHashMap<String, ChessAccount>();
         ChessRatingPeriodResults ratingResults = new ChessRatingPeriodResults();
         ChessRatingCalculator ratingCalculator = new ChessRatingCalculator();
         List<String> playerIds = new ArrayList<String>();
         
         for(ChessAccount account : accounts) {
            ChessAccountType accountType = account.getType();
            
            if(accountType != ChessAccountType.INACTIVE) {
               String playerId = account.getKey();           
               double rank = account.getRating();
               double volatility = account.getVolatility();
               double deviation = account.getDeviation();
               ChessRating rating = null;
               
               if(rank <= 0) {
                  rating = new ChessRating(playerId, ratingCalculator); // use default values
               } else {
                  rating = new ChessRating(playerId, ratingCalculator, rank, deviation, volatility);
               }    
               playerIds.add(playerId);
               activeAccounts.put(playerId, account);
               accountRatings.put(playerId, rating);
               ratingResults.addParticipants(rating);
            }   
         }         
         for(ChessGameState game : games) {
            if(gameIsFinished(game)) {
               gameDatabase.deleteGame(game); // delete all games that are over     
               
               if(gameIsRankable(game)) {
                  String ownerId = game.getOwnerId();
                  String challengerId = game.getChallengerId();
                  
                  if(!ownerId.equals(challengerId)) {
                     String winnerId = game.getWinnerId();
                     ChessRating ownerRating = accountRatings.get(ownerId);
                     ChessRating challengerRating = accountRatings.get(challengerId);
                     
                     if(winnerId != null) {
                        if(winnerId.equals(ownerId)) {
                           ratingResults.addResult(ownerRating, challengerRating);
                        } else {
                           ratingResults.addResult(challengerRating, ownerRating);
                        }
                     } else {
                        ratingResults.addDraw(ownerRating, challengerRating);
                     }
                  }
               }          
            }            
         }
         ratingCalculator.updateRatings(ratingResults);
         
         for(String playerId : playerIds) {
            ChessAccount account = activeAccounts.get(playerId);
            ChessRating rating = accountRatings.get(playerId);
            double result = rating.getRating();
            double volatility = rating.getVolatility();
            double deviation = rating.getRatingDeviation();
            
            account.setRating(result);
            account.setVolatility(volatility);
            account.setDeviation(deviation);
            orderedRatings.add(rating);
         }
         float total = orderedRatings.size();
         float index = 1;
         
         while(!orderedRatings.isEmpty()) {
            ChessRating rating = orderedRatings.poll();
            
            if(rating != null) {
               String okayerId = rating.getUserId();
               ChessAccount account = activeAccounts.get(okayerId);
               int position = Math.round(index); // starts at 1 and ends at player count
               float factor = index++ / total;
               int percentile = Math.round(100.0f - (factor * 100.0f)); // higher is better e.g 99th percentile
               
               account.setPercentile(percentile);
               account.setRank(position);
            }
         }
         accountDatabase.saveAccounts(accounts); // transactional save
      }
   }
   
   private boolean gameIsFinished(ChessGameState state) {
      ChessGameStatus status = state.getGameStatus();   
      
      if(status.isGameOver()) {
         long creationTime = state.getCreationTime();
         long gameDuration = state.getGameDuration();
         long maximumDuration = creationTime + gameDuration + gameDuration;
         long additionalTime = 24 * 60 * 60 * 1000;
         long expiryTime = maximumDuration + additionalTime;
         long currentTime = System.currentTimeMillis();
         
         return expiryTime < currentTime;
      }
      return false;
   }
   
   private boolean gameIsRankable(ChessGameState game) {
      try {
         ChessGameStatus status = game.getGameStatus();
         
         if(status == ChessGameStatus.GAME_OVER || status == ChessGameStatus.RESIGNED) {
            String ownerId = game.getOwnerId();
            String challengerId = game.getChallengerId();
            String gameId = game.getGameId();
            
            if(ownerId != null && challengerId != null) {
               if(!ownerId.equals(challengerId)) {
                  List<ChessMoveState> gameMoves = gameDatabase.listMoves(gameId);
                  int count = gameMoves.size();                  
                  
                  if(count >= 2) { // black moves, and white moved
                     return true;
                  }
               }
            }
         }
      } catch(Exception e) {
         LOG.info("Could not determine game status", e);
      }
      return false;
      
   }
}
