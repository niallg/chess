package com.zuooh.chess.server.game.bot;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.client.event.response.ChessNewGameEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessGameStateDatabase;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngineProvider;

public class ChessServerBotChallenger implements ChessServerBotGroup {

   private final ChessUserDatabase profileDatabase;
   private final ChessServerBotPublisher botPublisher;
   private final ChessServerBotBuilder botBuilder;
   private final ChessServerBotObserver botObserver;
   private final ChessGameStateDatabase gameDatabase;
   private final BlockingQueue<String> botQueue;  
   private final Random randomSkill;
   private final String observerName;

   public ChessServerBotChallenger(ChessServerBotPublisher botPublisher, ChessServerBotObserver botObserver, ChessUserDatabase profileDatabase, ChessGameStateDatabase gameDatabase, ChessMoveEngineProvider engineProvider, String observerName) {
      this.botQueue = new LinkedBlockingQueue<String>();
      this.botBuilder = new ChessServerBotBuilder(botPublisher, engineProvider);
      this.randomSkill = new SecureRandom();
      this.gameDatabase = gameDatabase;
      this.profileDatabase = profileDatabase;
      this.botPublisher = botPublisher;
      this.botObserver = botObserver;
      this.observerName = observerName;
   }

   public void registerBots() {
      try {
         List<ChessUser> botProfiles = profileDatabase.loadUsersMatching(observerName, true);
         
         if(!botProfiles.isEmpty()) {
            List<ChessUser> randomProfiles = new ArrayList<ChessUser>(botProfiles);
            
            Collections.shuffle(randomProfiles);
            
            for(ChessUser botProfile : randomProfiles) {
               String botId = botProfile.getKey();
               
               botObserver.registerBotUser(botProfile);
               botQueue.offer(botId);
            }
         }
         botObserver.registerBotGroup(this);
      } catch(Exception e) {
         throw new IllegalStateException("Problem registering observer", e);
      }
   }

   @Override
   public void newGame(ChessGameState event) {
      ChessGameStatus status = event.getGameStatus();

      if(status == ChessGameStatus.NEW) {
         ChessSide pieceColor = event.getOwnerSide();
         
         if(!botQueue.isEmpty()) {
            String botName = botQueue.poll();
            
            try {
               ChessUser botProfile = profileDatabase.loadUser(botName);
               ChessSide botDirection = pieceColor.oppositeSide();
               String botId = botProfile.getKey();
            
               if(!botId.equals(botName)) {
                  throw new IllegalStateException("Bot name '" + botName + "' does not match both mail '" + botId + "'");
               }             
               String gameId = event.getGameId();
               String playerId = event.getOwnerId();
               ChessUserData botData = botProfile.getData();               
               int botSkill = botData.getSkill();
               
               if(botSkill <= 1) {
                  botSkill = randomSkill.nextInt(3) + 3; // at least 2 in skill but less than 5 
               }
               ChessServerBot chessBot = botBuilder.createNewBot(botProfile, gameId, playerId, botDirection, botSkill);
               
               botObserver.registerBotPlayer(chessBot, gameId);
               botPublisher.acceptGame(gameId, botId, botSkill);
               
               startGame(chessBot, botId, gameId);
            } catch(Exception e) {
               throw new IllegalStateException("Problem registering observer", e);
            }
            botQueue.offer(botName);            
         }
      } 
   }
   
   private void startGame(ChessServerBot bot, String botId, String gameId) {
      try {
         List<ChessMoveState> listMoves = gameDatabase.listMoves(gameId); // check for previous moves, maybe player moved white
         Iterator<ChessMoveState> moveIterator = listMoves.iterator();      
         int size = listMoves.size();
         int lastIndex = size - 1;
         int index = 0;
         
         botObserver.registerBotPlayer(bot, gameId);
         
         if(size > 0) {
            while(moveIterator.hasNext()) {
               ChessMoveState gameMove = moveIterator.next();
               ChessMove moveMade = gameMove.getMove();           
               
               if(index++ == lastIndex) {     
                  String moveUser = gameMove.getUserId();
                  
                  if(!botId.equals(moveUser)) {
                     bot.moveMade(gameMove); // kick engine in to gear to play
                  } else {
                     bot.updateBoard(moveMade);
                  }
               } else {
                  bot.updateBoard(moveMade);
               }
            }
         } else {
            bot.start(); // make first move
         }
      } catch(Exception e) {
         throw new IllegalStateException("Problem starting game '" + gameId + "'", e);
      }
   }
}
