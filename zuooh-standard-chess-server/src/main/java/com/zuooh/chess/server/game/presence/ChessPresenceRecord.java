package com.zuooh.chess.server.game.presence;

import java.util.concurrent.atomic.AtomicLong;

import com.zuooh.common.time.DateTime;
import com.zuooh.common.time.SampleAverager;

class ChessPresenceRecord implements ChessPresence {

   private final SampleAverager updateAverager;
   private final AtomicLong updateCount;
   private final AtomicLong lastUpdate;
   private final String playerId;
   private final long firstPresent;

   public ChessPresenceRecord(String playerId, long firstPresent) {
      this.lastUpdate = new AtomicLong(firstPresent);
      this.updateAverager = new SampleAverager();
      this.updateCount = new AtomicLong();
      this.firstPresent = firstPresent;
      this.playerId = playerId;
   }

   @Override
   public String getPresenceId() {
      return playerId;
   }

   @Override
   public DateTime getFirstPresent() {
      return DateTime.at(firstPresent);
   }

   @Override
   public DateTime getLastPresent() {
      long lastPresent = lastUpdate.get();
      return DateTime.at(lastPresent);
   }

   @Override
   public long getPresenceDuration() {
      long lastPresent = lastUpdate.get();
      return lastPresent - firstPresent;
   }

   @Override
   public long getAverageUpdateDuration() {
      return updateAverager.average();
   }

   @Override
   public long getMinimumUpdateDuration() {
      return updateAverager.minimum();
   }

   @Override
   public long getMaximumUpdateDuration() {
      return updateAverager.maximum();
   }

   public long getCountPresenceUpdates() {
      return updateCount.get();
   }

   public void updatePresence() {
      long currentTime = System.currentTimeMillis();
      long lastPresent = lastUpdate.get();
      long sampleCount = updateAverager.count();

      if (sampleCount > 100) {
         long currentAverage = updateAverager.average();

         updateAverager.reset();
         updateAverager.sample(currentAverage); // retain running average
      }
      if (lastPresent != 0) {
         updateAverager.sample(currentTime - lastPresent);
      }
      updateCount.getAndIncrement();
   }
}
