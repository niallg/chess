package com.zuooh.chess.server.notify;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.notify.ChessNotification;

public class ChessNotificationPersister {

   public static final Logger LOG = LoggerFactory.getLogger(ChessNotificationPersister.class);
   
   private final ChessNotificationConverter converter;
   
   public ChessNotificationPersister(ChessNotificationQuery query) {
      this.converter = new ChessNotificationConverter(query);
   }
   
   public String persist(List<ChessNotification> notifications) throws Exception {
      StringBuilder builder = new StringBuilder();
               
      try {         
         if(notifications != null) {
            int count = notifications.size();
            
            for(int i = 0; i < count; i++) {
               ChessNotification notification = notifications.get(i);
               Map<String, Object> values = converter.convert(notification);
               Set<String> keys = values.keySet();

               if(i == 0) { // for legacy reasons only!!
                  for(String key : keys) {       
                     Object value = values.get(key);
                     
                     builder.append(key);
                     builder.append("=");
                     builder.append(value);
                     builder.append("\n");
                  }
               }
               for(String key : keys) {       
                  Object value = values.get(key);   
                  
                  builder.append(key);
                  builder.append(".");
                  builder.append(i);
                  builder.append("=");
                  builder.append(value);
                  builder.append("\n");
               }
            } 
         }
      } catch(Exception e) {
         LOG.info("Error marshalling response", e);
      }
      return builder.toString();
   }
}
