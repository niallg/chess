package com.zuooh.chess.server.game.account;

import java.util.Comparator;

public class ChessAccountPresenceComparator implements Comparator<ChessAccount> {
   
   private final boolean mostRecentFirst;
   
   public ChessAccountPresenceComparator() {
      this(true);
   }
   
   public ChessAccountPresenceComparator(boolean mostRecentFirst) {
      this.mostRecentFirst = mostRecentFirst;
   }
   
   public int compare(ChessAccount left, ChessAccount right) {
      if(mostRecentFirst) {
         return compareMostRecentFirst(left, right);
      }
      return compareLeastRecentFirst(left, right);
   }
   
   public static int compareLeastRecentFirst(ChessAccount left, ChessAccount right) {
      return compareMostRecentFirst(right, left);
   }
   
   public static int compareMostRecentFirst(ChessAccount left, ChessAccount right) {
      Long leftPresence = left.getLastSeenOnline();
      Long rightPresence = right.getLastSeenOnline();

      return rightPresence.compareTo(leftPresence);
   }

}
