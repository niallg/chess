package com.zuooh.chess.server.game.presence;

import com.zuooh.common.time.DateTime;

public interface ChessPresence {   
   String getPresenceId();
   DateTime getFirstPresent();
   DateTime getLastPresent();
   long getPresenceDuration();
   long getAverageUpdateDuration();
   long getMinimumUpdateDuration();
   long getMaximumUpdateDuration();
   long getCountPresenceUpdates();
}
