package com.zuooh.chess.server.game;

import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessMoveState;

public interface ChessGameChangeObserver {
   void onStart(ChessGameState gameState);
   void onMove(ChessGameState gameState, ChessMoveState moveState);
   void onFinish(ChessGameState gameState);
}
