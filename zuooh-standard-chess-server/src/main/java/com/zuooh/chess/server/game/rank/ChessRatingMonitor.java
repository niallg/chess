package com.zuooh.chess.server.game.rank;

import java.util.List;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserStatus;
import com.zuooh.chess.server.game.account.ChessAccount;
import com.zuooh.chess.server.game.account.ChessAccountDatabase;
import com.zuooh.chess.server.game.account.ChessAccountUserBuilder;
import com.zuooh.chess.server.game.account.ChessAccountSearcher;
import com.zuooh.common.html.TableDrawer;
import com.zuooh.common.time.DateTime;

@ManagedResource(description="Monitor current player ratings")
public class ChessRatingMonitor {
   
   private final ChessAccountUserBuilder accountBuilder;
   private final ChessAccountSearcher accountSearcher;
   
   public ChessRatingMonitor(ChessAccountDatabase accountDatabase) {
      this.accountSearcher = new ChessAccountSearcher(accountDatabase); 
      this.accountBuilder = new ChessAccountUserBuilder();
   }
   
   @ManagedOperation(description="Show highest ranks")
   public String showHighestRankedPlayers() throws Exception {
      return showHighestRankedPlayers(100);
   }
   
   @ManagedOperation(description="Show highest ranks")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="limit", description="The number of players to show")
   })
   public String showHighestRankedPlayers(int limit) throws Exception {
      TableDrawer drawer = new TableDrawer("playerId", "status", "rank", "rating", "percentile", "skill", "blackGames", "whiteGames", "lastOnline");
      
      if(limit > 0) {
         List<ChessAccount> orderedAccounts = accountSearcher.findHighestRatedAccounts();
         
         for(ChessAccount account : orderedAccounts) {
            ChessUserData playerData = accountBuilder.createData(account);
            ChessUserStatus status = playerData.getStatus();
            String playerId = account.getKey();
            double rating = account.getRating();
            int skill = playerData.getSkill();
            int rank = playerData.getRank();
            int percentile = account.getPercentile(); 
            int blackGames = account.getBlackGames();
            int whiteGames = account.getWhiteGames();
            long lastOnline = account.getLastSeenOnline();
            DateTime lastOnlineDate = DateTime.at(lastOnline);
            
            if(limit-- <= 0) {
               return drawer.drawTable();
            }
            drawer.newRow(playerId, 
                  status,                  
                  rank, 
                  rating,
                  percentile,
                  skill,
                  blackGames, 
                  whiteGames, 
                  lastOnlineDate);
         }         
      }
      return drawer.drawTable();
   }
}
