package com.zuooh.chess.server.game.bot;

import com.zuooh.chess.database.state.ChessGameState;

public interface ChessServerBotGroup {
   void newGame(ChessGameState game);
}
