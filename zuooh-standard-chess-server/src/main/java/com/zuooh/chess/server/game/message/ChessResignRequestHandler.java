package com.zuooh.chess.server.game.message;

import com.zuooh.chess.client.event.ChessResignEvent;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.RequestHandler;

public class ChessResignRequestHandler implements RequestHandler<ChessResignEvent> {
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;

   public ChessResignRequestHandler(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception { 
      this.mainController = mainController;   
      this.serverPublisher = serverPublisher;
   }

   @Override
   public ChessResignEvent execute(ChessResignEvent event) throws Exception {  
      String playerId = event.getUserId();
      String gameId = event.getGameId();
      String opponentId = event.getOpponentId();

      mainController.resignGame(gameId, playerId);
      
      if(opponentId != null) {
         serverPublisher.publishNotification(event, opponentId);
      }
      return event;
   }
}
