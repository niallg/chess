package com.zuooh.chess.server.game.presence;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.server.game.account.ChessAccount;
import com.zuooh.chess.server.game.account.ChessAccountDatabase;
import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.common.html.TableDrawer;
import com.zuooh.common.time.DateTime;

@ManagedResource(description="Cache player presence")
public class ChessPresenceMonitor implements ChessPresenceListener {
   
   private final Cache<String, ChessPresence> presenceCache;
   private final ChessPresenceComparator presenceComparator;
   private final ChessAccountDatabase accountDatabase;

   public ChessPresenceMonitor(ChessAccountDatabase accountDatabase) {
      this(accountDatabase, 10000);
   }
   
   public ChessPresenceMonitor(ChessAccountDatabase accountDatabase, int capacity) {   
      this.presenceCache = new LeastRecentlyUsedCache<String, ChessPresence>(capacity);
      this.presenceComparator = new ChessPresenceComparator();
      this.accountDatabase = accountDatabase;
   }
   
   @ManagedOperation(description="Show currently present users")
   public synchronized String showPresence() throws Exception {
      TableDrawer drawer = new TableDrawer("playerId", "playerName", "lastPresent", "firstPresent", "averageUpdate", "maximumUpdate", "minimumUpdate", "updateCount");
      DecimalFormat format = new DecimalFormat("###,###,###");      
      List<ChessPresence> list = new ArrayList<ChessPresence>();
      
      if(!presenceCache.isEmpty()) {
         Set<String> playerIds = presenceCache.keySet();

         for(String playerId : playerIds) {
            ChessPresence presence = presenceCache.fetch(playerId);
            
            if(presence != null) {
               list.add(presence);
            }
         }
         Collections.sort(list, presenceComparator);
         
         for(ChessPresence presence : list) {
            String playerId = presence.getPresenceId();
            String playerName = null;
            
            if(presence != null) {
               ChessAccount userAccount = accountDatabase.loadAccount(playerId);
               DateTime lastPresent = presence.getLastPresent();
               DateTime firstPresent = presence.getFirstPresent();
               long averageUpdate = presence.getAverageUpdateDuration();
               long maximumUpdate = presence.getMaximumUpdateDuration();
               long minimumUpdate = presence.getMinimumUpdateDuration();
               long updateCount = presence.getCountPresenceUpdates();
               
               if(userAccount != null) {
                  playerName = userAccount.getName();
               }                              
               drawer.newRow(
                     playerId,
                     playerName,
                     lastPresent, 
                     firstPresent, 
                     format.format(averageUpdate), 
                     format.format(maximumUpdate), 
                     format.format(minimumUpdate), 
                     format.format(updateCount));
            }
         }
      }
      return drawer.drawTable();
   }   

   @Override
   public synchronized void onPresenceUpdate(ChessPresence presence) {
      String playerId = presence.getPresenceId();
      
      if(playerId != null) {
         presenceCache.cache(playerId, presence);
      }
   }

   @Override
   public synchronized void onPresenceExpiry(ChessPresence presence) {
      String playerId = presence.getPresenceId();
      
      if(playerId != null) {
         presenceCache.take(playerId);
      }
   }

}
