package com.zuooh.chess.server.game.presence;

public interface ChessPresenceListener {
   void onPresenceUpdate(ChessPresence presence);
   void onPresenceExpiry(ChessPresence presence);
}
