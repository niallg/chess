package com.zuooh.chess.server.game.status;

import java.util.List;

import com.zuooh.chess.client.event.request.ChessCreateGameEvent;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.client.event.response.ChessNewGameEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.RequestHandler;

public class ChessCreateGameRequestHandler implements RequestHandler<ChessCreateGameEvent> {
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;

   public ChessCreateGameRequestHandler(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.mainController = mainController; 
   }

   @Override
   public ChessNewGameEvent execute(ChessCreateGameEvent event) throws Exception {
      String gameOpponentId = event.getChallengeUserId();
      ChessGameCriteria gameChallenge = event.getChallenge();
      String gameId = event.getGameId();
      ChessGame game = mainController.createGame(gameChallenge, gameId, gameOpponentId);

      if(gameOpponentId != null)  {
         List<ChessGame> chessGames = mainController.statusUpdate(gameOpponentId); // tell user they were challenged
         
         if(!chessGames.isEmpty()) {
            ChessListOfGamesEvent listEvent = new ChessListOfGamesEvent(chessGames);
            serverPublisher.publishNotification(listEvent, gameOpponentId); // if they are online tell them straight away
         }
      }
      return new ChessNewGameEvent(game); // always respond with the game details
   }
}
