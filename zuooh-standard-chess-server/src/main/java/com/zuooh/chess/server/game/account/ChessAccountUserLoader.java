package com.zuooh.chess.server.game.account;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;

public class ChessAccountUserLoader {

   private static final Logger LOG = LoggerFactory.getLogger(ChessAccountUserLoader.class);
   
   private final ChessAccountUserBuilder accountBuilder;
   private final ChessAccountDatabase accountDatabase;
   private final ChessUserDatabase userDatabase;
   
   public ChessAccountUserLoader(ChessAccountDatabase accountDatabase, ChessUserDatabase userDatabase) {
      this.accountBuilder = new ChessAccountUserBuilder();
      this.accountDatabase = accountDatabase;
      this.userDatabase = userDatabase;
   }
   
   public void loadUsers() {
      try {
         List<ChessUser> users = userDatabase.loadUsers();
         
         for(ChessUser user : users) {
            String userId = user.getKey();
            ChessAccount account = accountDatabase.loadAccount(userId);
            
            if(account == null) {
               account = accountBuilder.createAccount(user);
               
               if(account != null) {
                  accountDatabase.saveAccount(account);
               }
            }
         }
      } catch(Exception e) {
         LOG.info("Could not restore database users", e);
      }
   }
}
