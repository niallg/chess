package com.zuooh.chess.server.game.status;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.event.request.ChessMatchGameEvent;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.client.event.response.ChessNewGameEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.RequestHandler;

public class ChessMatchGameRequestHandler implements RequestHandler<ChessMatchGameEvent> {
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;

   public ChessMatchGameRequestHandler(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.mainController = mainController; 
   }

   @Override
   public ChessNewGameEvent execute(ChessMatchGameEvent event) throws Exception {
      long requestGameDuration = event.getGameDuration();
      int requestUserSkill = event.getUserSkill();
      String requestUserId = event.getUserId();
      ChessSide requestSide = event.getGameSide();
      List<ChessGame> matchedGames = matches(requestSide, requestGameDuration, requestUserId, requestUserSkill);
      
      for(ChessGame availableGame : matchedGames){
         String gameId = availableGame.getGameId();
         ChessGame acceptedGame = mainController.acceptGame(gameId, requestUserId, requestUserSkill);
                  
         if(acceptedGame != null) {
            String ownerId = availableGame.getOwnerId();

            if(ownerId != null)  {
               List<ChessGame> chessGames = mainController.statusUpdate(ownerId);
            
               if(!chessGames.isEmpty()) {
                  ChessListOfGamesEvent listEvent = new ChessListOfGamesEvent(chessGames);
                  serverPublisher.publishNotification(listEvent, ownerId); // if they are online tell them straight away
               }
            }
            return new ChessNewGameEvent(acceptedGame);
         }
      }
      return null;
   }
   
   private List<ChessGame> matches(ChessSide requestSide, long requestGameDuration, String requestUserId, int requestUserSkill) throws Exception {
      List<ChessGame> availableGames = mainController.availableGames(requestUserId); // possible games!!
      
      if(!availableGames.isEmpty()) {
         List<ChessGame> matches = new ArrayList<ChessGame>();
         
         for(ChessGame availableGame : availableGames){
            ChessGameStatus gameStatus = availableGame.getStatus();
   
            if(gameStatus == ChessGameStatus.NEW) {
               ChessGameCriteria gameCriteria = availableGame.getCriteria();
               ChessSide gameSide = gameCriteria.getUserSide();
               
               if(gameSide != requestSide) {
                  long gameDuration = gameCriteria.getGameDuration();
                  
                  if(gameDuration == requestGameDuration) {
                     matches.add(availableGame);                     
                  }
               }
            }
         }
         return matches;
      }
      return Collections.emptyList();
   }
}
