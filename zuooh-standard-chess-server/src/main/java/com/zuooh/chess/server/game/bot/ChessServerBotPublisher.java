package com.zuooh.chess.server.game.bot;

import java.util.concurrent.Executor;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.presence.ChessPresenceCache;

public class ChessServerBotPublisher {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessServerBotPublisher.class);
   
   private final ChessServerController controller;
   private final ChessPresenceCache cache;
   private final Executor executor;
   
   public ChessServerBotPublisher(ChessServerController controller, ChessPresenceCache cache, Executor executor) {// provide the database thread pool
      this.controller = controller;
      this.executor = executor;
      this.cache = cache;
   }
   
   public void saveUser(ChessUser user) {
      PublishSaveUser saveUser = new PublishSaveUser(user);            
      executor.execute(saveUser);
   }
   
   public void acceptGame(String gameId, String userId, int userSkill) {
      PublishAcceptGame acceptGame = new PublishAcceptGame(gameId, userId, userSkill);            
      executor.execute(acceptGame);
   }

   public void makeMove(String gameId, ChessMove chessMove, String userId) {
      PublishMakeMove botMove = new PublishMakeMove(gameId, chessMove, userId);            
      executor.execute(botMove);
   }   
   
   public void gameOver(String gameId, String declareId) {
      PublishGameOver botMove = new PublishGameOver(gameId, declareId);            
      executor.execute(botMove);      
   }   
   
   public void gameOver(String gameId, String declareId, String winnerId) {
      PublishGameOver botMove = new PublishGameOver(gameId, declareId, winnerId);            
      executor.execute(botMove);      
   }
   
   private class PublishAcceptGame implements Runnable {
      
      private final String gameId;
      private final String userId;
      private final int userSkill;

      public PublishAcceptGame(String gameId, String userId, int userSkill) {
         this.gameId = gameId;
         this.userId = userId;
         this.userSkill = userSkill;
      }
      
      @Override
      public void run() {
         try {
            controller.acceptGame(gameId, userId, userSkill);
            cache.updatePresence(userId, false);            
         } catch(Exception e){
            LOG.info("Error on bot publication", e);
         }
      }      
   }
   
   private class PublishSaveUser implements Runnable {
      
      private final ChessUser user;

      public PublishSaveUser(ChessUser user) {
         this.user = user;
      }
      
      @Override
      public void run() {
         try {
            controller.saveUser(user);
         } catch(Exception e){
            LOG.info("Error on bot publication", e);
         }
      }      
   }
   
   private class PublishMakeMove implements Runnable {
      
      private final ChessMove chessMove;
      private final String gameId;
      private final String userId;

      public PublishMakeMove(String gameId, ChessMove chessMove, String userId) {
         this.gameId = gameId;
         this.chessMove = chessMove;
         this.userId = userId;
      }
      
      @Override
      public void run() {
         try {
            controller.makeMove(gameId, chessMove, userId);
            cache.updatePresence(userId, false);            
         } catch(Exception e){
            LOG.info("Error on bot publication", e);
         }
      }      
   }
   
   public class PublishGameOver implements Runnable {

      private final String gameId;
      private final String winnerId;
      private final String declareId;

      public PublishGameOver(String gameId, String declareId) {
         this(gameId, declareId, null);
      }
      
      public PublishGameOver(String gameId, String declareId, String winnerId) {
         this.declareId = declareId;
         this.winnerId = winnerId;
         this.gameId = gameId;
      }
      
      @Override
      public void run() {
         try {
            if(winnerId != null) {
               controller.gameOver(gameId, declareId, winnerId);
            } else {
               controller.gameOver(gameId, declareId);
            }
         } catch(Exception e){
            LOG.info("Error on bot publication", e);
         }
      }      
   }
}
