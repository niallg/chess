package com.zuooh.chess.server.notify;

import static com.zuooh.chess.server.notify.ChessNotificationKey.ERROR_KEY;

import java.io.PrintStream;
import java.util.List;
import java.util.concurrent.Executor;

import org.simpleframework.http.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.notify.ChessNotification;

public class ChessNotificationResponder implements ChessNotificationListener {   

   public static final Logger LOG = LoggerFactory.getLogger(ChessNotificationResponder.class);
   
   private final ChessNotificationPersister persister;
   private final ChessNotificationQuery query;
   private final Executor executor;
   private final Response response;
   private final boolean debug;
   
   public ChessNotificationResponder(ChessNotificationQuery query, Response response, Executor executor) {
      this(query, response, executor, true);
   }
   
   public ChessNotificationResponder(ChessNotificationQuery query, Response response, Executor executor, boolean debug) {
      this.persister = new ChessNotificationPersister(query);
      this.response = response;
      this.executor = executor;
      this.query = query;
      this.debug = debug;
   }

   @Override
   public void onNotification(List<ChessNotification> notifications) {
      ResponseMarshaller marshaller = new ResponseMarshaller(notifications);
      executor.execute(marshaller);
   }
   
   @Override
   public void onException(Exception exception) {
      ErrorMarshaller marshaller = new ErrorMarshaller(exception);
      executor.execute(marshaller);
   }
   
   private class ErrorMarshaller implements Runnable {
      
      private final Exception exception;
      
      public ErrorMarshaller(Exception exception) {
         this.exception = exception;
      }
      
      public void run() {
         try {
            PrintStream out = response.getPrintStream();
            String message = exception.getMessage();
            String data = ERROR_KEY + "=" + message;
            String user = query.getUser();
            String device = query.getDevice();           
 
            response.setContentType("text/plain");                
            out.println("# Notification Error");
            out.println(data);
            out.close();
            
            if(debug) {
               LOG.info("Notification error user=[" +user + "] device=["+device+"] response=["+ response + data + "]", exception);
            }
            response.close();
         } catch(Exception e) {
            LOG.info("Error marshalling error", e);
         }
         
      }
   }
   
   private class ResponseMarshaller implements Runnable {
      
      private final List<ChessNotification> notifications;
      
      public ResponseMarshaller(List<ChessNotification> notifications) {        
         this.notifications = notifications;
      }
      
      public void run() {
         try {
            PrintStream out = response.getPrintStream();
            String data = persister.persist(notifications);
            String user = query.getUser();
            String device = query.getDevice();

            response.setContentType("text/plain");
            out.println("# Notification Response");
            out.println(data);
            out.close();            
            
            if(debug) {
               LOG.info("Notification response user=[" +user + "] device=["+device+"] response=[" + response + data + "]");
            }
            response.close();
         } catch(Exception e) {
            LOG.info("Error marshalling response", e);
         }
      }      
   }
}
