package com.zuooh.chess.server.notify;

public interface ChessNotificationKey {
   String USER_KEY = "user";
   String FUNCTION_KEY = "function";
   String PROTOCOL_KEY = "protocol";
   String VERSION_KEY = "version";
   String PLATFORM_KEY = "platform";
   String DEVICE_KEY = "device";
   String ERROR_KEY = "error";
   String TITLE_KEY = "title";
   String TYPE_KEY = "type";
   String GAME_KEY = "game";   
   String TEXT_KEY = "text";   
   String FROM_KEY = "from";
   String MESSAGE_KEY = "message";
   String PAGE_KEY = "page";
   String HOME_KEY = "home";      
   String ICON_KEY = "icon";
   String VIBRATE_KEY = "vibrate";
   String RETRY_KEY = "retry";
   String KEY_KEY = "key";
   String ID_KEY = "id";
}
