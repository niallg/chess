package com.zuooh.chess.server.game.account;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.event.ChessUserEvent;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.chess.server.game.presence.ChessPresence;
import com.zuooh.chess.server.game.presence.ChessPresenceAdapter;

public class ChessAccountPresenceFlusher extends ChessPresenceAdapter {

   private static final Logger LOG = LoggerFactory.getLogger(ChessAccountPresenceFlusher.class);
   
   private final ChessAccountUserBuilder accountBuilder;
   private final ChessServerPublisher serverPublisher;
   private final ChessAccountDatabase accountDatabase;

   public ChessAccountPresenceFlusher(ChessServerPublisher serverPublisher, ChessAccountDatabase accountDatabase) throws Exception {
      this.accountBuilder = new ChessAccountUserBuilder();
      this.serverPublisher = serverPublisher;
      this.accountDatabase = accountDatabase;
   }

   @Override
   public void onPresenceUpdate(ChessPresence presence) {
      String playerId = presence.getPresenceId();
      
      try {
         ChessAccount account = accountDatabase.loadAccount(playerId);
         ChessUser user = accountBuilder.createUser(account);
         
         if(user != null) {
            ChessUserEvent event = new ChessUserEvent(user);
            serverPublisher.publishNotification(event, playerId);
         }
      }catch(Exception e) {
         LOG.info("Unable to flush messages for " + playerId, e);
      }
   }
}
