package com.zuooh.chess.server.game.presence;

import java.util.Collections;
import java.util.Set;

public class ChessPresenceListenerRegistry {

   private final Set<ChessPresenceListener> listeners;
   private final ChessPresenceNotifier notifier;

   public ChessPresenceListenerRegistry(ChessPresenceNotifier notifier) {
      this(notifier, Collections.EMPTY_SET);
   }
   
   public ChessPresenceListenerRegistry(ChessPresenceNotifier notifier, Set<ChessPresenceListener> listeners) {
      this.listeners = listeners;
      this.notifier = notifier;
   }
   
   public void register() {
      for(ChessPresenceListener listener : listeners) {
         notifier.registerListener(listener);
      }
   }
}
