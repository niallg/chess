package com.zuooh.chess.server.game.status;

import java.util.List;

import com.zuooh.chess.client.event.request.ChessAcceptGameEvent;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.client.event.response.ChessNewGameEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.RequestHandler;

public class ChessAcceptGameRequestHandler implements RequestHandler<ChessAcceptGameEvent> {
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;
   
   public ChessAcceptGameRequestHandler(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.mainController = mainController;   
   }
   
   @Override
   public ChessNewGameEvent execute(ChessAcceptGameEvent event) throws Exception {   
      String playerName = event.getUserId();
      int playerSkill = event.getUserSkill();
      String gameId = event.getGameId();
      ChessGame game = mainController.acceptGame(gameId, playerName, playerSkill);

      if(game != null)  {
         ChessGameCriteria challenge = game.getCriteria();
         ChessUser profile = challenge.getUser();          
         String ownerName = profile.getKey();

         if(ownerName != null)  {
            List<ChessGame> chessGames = mainController.statusUpdate(ownerName);
            
            if(!chessGames.isEmpty()) {
               ChessListOfGamesEvent listEvent = new ChessListOfGamesEvent(chessGames);
               serverPublisher.publishNotification(listEvent, ownerName); // if they are online tell them straight away
            }
            return new ChessNewGameEvent(game);
         }
      }
      return null;
   }
}
