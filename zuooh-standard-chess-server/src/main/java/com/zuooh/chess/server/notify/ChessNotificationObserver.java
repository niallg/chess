package com.zuooh.chess.server.notify;

import static com.zuooh.chess.database.notify.ChessNotificationType.GAME_INVITE;
import static com.zuooh.chess.database.notify.ChessNotificationType.GAME_OVER_DRAW;
import static com.zuooh.chess.database.notify.ChessNotificationType.GAME_OVER_LOSE;
import static com.zuooh.chess.database.notify.ChessNotificationType.GAME_OVER_WIN;
import static com.zuooh.chess.database.notify.ChessNotificationType.GAME_START;
import static com.zuooh.chess.database.notify.ChessNotificationType.MOVE_MADE;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.notify.ChessNotification;
import com.zuooh.chess.database.notify.ChessNotificationBuilder;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserFormatter;
import com.zuooh.chess.server.game.ChessGameChangeObserver;
import com.zuooh.chess.server.game.ChessGameCreateObserver;
import com.zuooh.chess.server.game.ChessServerController;

public class ChessNotificationObserver implements ChessGameCreateObserver, ChessGameChangeObserver {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessNotificationObserver.class);
   
   private final ChessNotificationPublisher notificationPublisher;
   private final ChessNotificationBuilder notificationBuilder;
   private final ChessServerController mainController;
   private final String observerName;
   
   public ChessNotificationObserver(ChessNotificationPublisher notificationPublisher, ChessNotificationBuilder notificationBuilder, ChessServerController mainController, String observerName) {
      this.notificationPublisher = notificationPublisher;
      this.notificationBuilder = notificationBuilder;
      this.mainController = mainController;
      this.observerName = observerName;
   }
   
   public void registerObserver() {
      try {
         mainController.registerObserver(observerName, (ChessGameChangeObserver)this);
         mainController.registerObserver(observerName, (ChessGameCreateObserver)this);
      } catch(Exception e) {
         LOG.info("Coult not register for notifications", e);
      }
   }

   @Override
   public void onCreate(ChessGameState gameState) {
      try {
         ChessGameStatus gameStatus = gameState.getGameStatus();
         
         if(gameStatus.isInvitation()) {
            String gameId = gameState.getGameId();
            String challengerId = gameState.getChallengerId();
            ChessNotification inviteNotification = notificationBuilder.createNotification(GAME_INVITE, gameId, challengerId);
            ChessGame game = mainController.loadGame(gameId, challengerId);
            ChessGameCriteria gameCriteria = game.getCriteria();            
            ChessUser ownerUser = gameCriteria.getUser();
            String ownerName =  ChessUserFormatter.formatUser(ownerUser);
            
            inviteNotification.setText(ownerName);
            notificationPublisher.publishNotification(inviteNotification, game);
         }
      } catch(Exception e) {
         LOG.info("Error creating notification", e);
      }
   }   
   
   @Override
   public void onStart(ChessGameState gameState) {
      try {
         ChessGameStatus gameStatus = gameState.getGameStatus();
         
         if(gameStatus.isAccepted()) {
            String gameId = gameState.getGameId();
            String ownerId = gameState.getOwnerId();
            ChessNotification startNotification = notificationBuilder.createNotification(GAME_START, gameId, ownerId);            
            ChessGame game = mainController.loadGame(gameId, ownerId);
            ChessGameCriteria gameCriteria = game.getCriteria();            
            ChessUser challengerUser = gameCriteria.getUser();
            String challengerName =  ChessUserFormatter.formatUser(challengerUser);
            
            startNotification.setText(challengerName);
            notificationPublisher.publishNotification(startNotification, game);            
         }
      } catch(Exception e) {
         LOG.info("Error creating notification", e);
      }
   }   

   @Override
   public void onMove(ChessGameState gameState, ChessMoveState moveState) { // maybe check how long since last move!
      try {
         ChessGameStatus gameStatus = gameState.getGameStatus();
         
         if(gameStatus.isAccepted()) {
            String moveId = moveState.getUserId();  
            String challengerId = gameState.getChallengerId();
            
            if(moveId.equals(challengerId)) {
               onMoveByChallenger(gameState, moveState);
            } else {
               onMoveByOwner(gameState, moveState);
            }
         }
      } catch(Exception e) {
         LOG.info("Error creating notification", e);
      }
   }   

   @Override
   public void onFinish(ChessGameState gameState) {
      try {
         ChessGameStatus gameStatus = gameState.getGameStatus();
         
         if(gameStatus.isGameOver()) {
            String ownerId = gameState.getOwnerId();
            String challengerId = gameState.getChallengerId();
            String winnerId = gameState.getWinnerId();
            
            if(winnerId == null) {
               onFinishedDraw(gameState);
            } else if(challengerId.equals(winnerId)) {
               onFinishedOwnerLose(gameState);
            } else if(ownerId.equals(winnerId)) {
               onFinishedOwnerWin(gameState);
            }
         }
      } catch(Exception e) {
         LOG.info("Error creating notification", e);
      }      
   }    
   
   private void onMoveByOwner(ChessGameState gameState, ChessMoveState moveState) { // maybe check how long since last move!
      try {
         long currentTime = System.currentTimeMillis();
         int changeCount = moveState.getChangeCount();
         String gameId = moveState.getGameId();
         String challengerId = gameState.getChallengerId();
         ChessNotification moveNotification = notificationBuilder.createNotification(MOVE_MADE, gameId, challengerId);            
         ChessGame game = mainController.loadGame(gameId, challengerId);
         ChessGameCriteria gameCriteria = game.getCriteria();            
         ChessUser ownerUser = gameCriteria.getUser();
         String ownerName =  ChessUserFormatter.formatUser(ownerUser);
         
         moveNotification.setSendTime(currentTime + 120000); // ensure we delay move notifications
         moveNotification.setSequence(changeCount);
         moveNotification.setText(ownerName);
         notificationPublisher.publishNotification(moveNotification, game);                     
      } catch(Exception e) {
         LOG.info("Error creating notification", e);
      }
   }
   
   private void onMoveByChallenger(ChessGameState gameState, ChessMoveState moveState) { // maybe check how long since last move!
      try {
         long currentTime = System.currentTimeMillis();
         int changeCount = moveState.getChangeCount();         
         String gameId = moveState.getGameId();                     
         String ownerId = gameState.getOwnerId();
         ChessNotification moveNotification = notificationBuilder.createNotification(MOVE_MADE, gameId, ownerId);           
         ChessGame game = mainController.loadGame(gameId, ownerId);
         ChessGameCriteria gameCriteria = game.getCriteria();            
         ChessUser challengerUser = gameCriteria.getUser();
         String challengerName =  ChessUserFormatter.formatUser(challengerUser);
         
         moveNotification.setSendTime(currentTime + 120000); // ensure we delay move notifications
         moveNotification.setSequence(changeCount);
         moveNotification.setText(challengerName);
         notificationPublisher.publishNotification(moveNotification, game);                    
      } catch(Exception e) {
         LOG.info("Error creating notification", e);
      }
   }  
   
   private void onFinishedOwnerWin(ChessGameState gameState) {
      try {
         String gameId = gameState.getGameId();
         String ownerId = gameState.getOwnerId();
         String challengerId = gameState.getChallengerId();
         ChessNotification winNotification = notificationBuilder.createNotification(GAME_OVER_WIN, gameId, ownerId); 
         ChessNotification loseNotification = notificationBuilder.createNotification(GAME_OVER_LOSE, gameId, challengerId);            
         ChessGame challengerGame = mainController.loadGame(gameId, challengerId);
         ChessGameCriteria challengerGameCriteria = challengerGame.getCriteria();            
         ChessUser ownerUser = challengerGameCriteria.getUser();               
         ChessGame ownerGame = mainController.loadGame(gameId, ownerId);
         ChessGameCriteria ownerGameCriteria = ownerGame.getCriteria();            
         ChessUser challengerUser = ownerGameCriteria.getUser();
         String ownerName =  ChessUserFormatter.formatUser(ownerUser);
         String challengerName =  ChessUserFormatter.formatUser(challengerUser); 
         
         winNotification.setText(challengerName);
         loseNotification.setText(ownerName);  
         notificationPublisher.publishNotification(winNotification, ownerGame);     
         notificationPublisher.publishNotification(loseNotification, challengerGame);                               
      } catch(Exception e) {
         LOG.info("Error creating win notifications", e);
      }      
   }   
   
   private void onFinishedOwnerLose(ChessGameState gameState) {
      try {
         String gameId = gameState.getGameId();
         String ownerId = gameState.getOwnerId();
         String challengerId = gameState.getChallengerId();
         ChessNotification winNotification = notificationBuilder.createNotification(GAME_OVER_WIN, gameId, challengerId); 
         ChessNotification loseNotification = notificationBuilder.createNotification(GAME_OVER_LOSE, gameId, ownerId);            
         ChessGame challengerGame = mainController.loadGame(gameId, challengerId);
         ChessGameCriteria challengerGameCriteria = challengerGame.getCriteria();            
         ChessUser ownerUser = challengerGameCriteria.getUser();               
         ChessGame ownerGame = mainController.loadGame(gameId, ownerId);
         ChessGameCriteria ownerGameCriteria = ownerGame.getCriteria();            
         ChessUser challengerUser = ownerGameCriteria.getUser();
         String ownerName =  ChessUserFormatter.formatUser(ownerUser);
         String challengerName =  ChessUserFormatter.formatUser(challengerUser);  
         
         winNotification.setText(ownerName); // set opponents name
         loseNotification.setText(challengerName); // set opponents name
         notificationPublisher.publishNotification(winNotification, challengerGame);     
         notificationPublisher.publishNotification(loseNotification, ownerGame);                        
      } catch(Exception e) {
         LOG.info("Error creating lose notifications", e);
      }      
   } 
   
   private void onFinishedDraw(ChessGameState gameState) {
      try {
         String gameId = gameState.getGameId();
         String ownerId = gameState.getOwnerId();
         String challengerId = gameState.getChallengerId();
         ChessNotification ownerNotification = notificationBuilder.createNotification(GAME_OVER_DRAW, gameId, ownerId); 
         ChessNotification challengerNotification = notificationBuilder.createNotification(GAME_OVER_DRAW, gameId, challengerId);            
         ChessGame challengerGame = mainController.loadGame(gameId, challengerId);
         ChessGameCriteria challengerGameCriteria = challengerGame.getCriteria();            
         ChessUser ownerUser = challengerGameCriteria.getUser();               
         ChessGame ownerGame = mainController.loadGame(gameId, ownerId);
         ChessGameCriteria ownerGameCriteria = ownerGame.getCriteria();            
         ChessUser challengerUser = ownerGameCriteria.getUser();               
         String ownerName =  ChessUserFormatter.formatUser(ownerUser);
         String challengerName =  ChessUserFormatter.formatUser(challengerUser);
         
         ownerNotification.setText(challengerName); // opponents anme
         challengerNotification.setText(ownerName);
         notificationPublisher.publishNotification(ownerNotification, ownerGame);     
         notificationPublisher.publishNotification(challengerNotification, challengerGame);                            
      } catch(Exception e) {
         LOG.info("Error creating draw notifications", e);
      }      
   }    
}
