package com.zuooh.chess.server.game.chat;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.event.chat.ChessChatListEvent;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.chess.server.game.presence.ChessPresence;
import com.zuooh.chess.server.game.presence.ChessPresenceListener;

public class ChessChatPresenceFlusher implements ChessPresenceListener {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessChatPresenceFlusher.class);
   
   private final ChessServerPublisher serverPublisher;
   private final ChessChatDatabase chatDatabase;

   public ChessChatPresenceFlusher(ChessChatDatabase chatDatabase, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.chatDatabase = chatDatabase;
   }

   @Override
   public void onPresenceUpdate(ChessPresence presence) {
      String playerId = presence.getPresenceId();
      
      try {
         List<ChessChat> messages = chatDatabase.listNewMessagesTo(playerId);
         
         if(!messages.isEmpty()) {
            ChessChatListEvent event = new ChessChatListEvent(playerId, messages);
            serverPublisher.publishNotification(event, playerId);
         }
      }catch(Exception e) {
         LOG.info("Unable to flush messages for " + playerId, e);
      }
   }

   @Override
   public void onPresenceExpiry(ChessPresence presence) {
      String playerId = presence.getPresenceId();
      
      try {
         //chatCache.saveMessages(); // XXX should be specific to player
      }catch(Exception e) {
         LOG.info("Unable to flush messages for " + playerId, e);
      }
   }

}
