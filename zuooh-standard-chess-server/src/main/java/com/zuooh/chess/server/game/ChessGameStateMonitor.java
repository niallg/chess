package com.zuooh.chess.server.game;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.List;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessGameStateDatabase;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.server.game.account.ChessAccountDatabase;
import com.zuooh.common.html.TableDrawer;
import com.zuooh.common.html.TableRowDrawer;
import com.zuooh.common.time.DateTime;

@ManagedResource(description="Monitor current games")
public class ChessGameStateMonitor {
   
   private final ChessGameStateDatabase gameDatabase;
   private final ChessAccountDatabase accountDatabase;
   private final ChessGameStateReader gameReader;
   
   public ChessGameStateMonitor(ChessGameStateDatabase database, ChessAccountDatabase accountDatabase) {
      this.gameReader = new ChessGameStateReader(database);
      this.accountDatabase = accountDatabase;
      this.gameDatabase = database;
   }
   
   @ManagedOperation(description="Show currently present users")
   public String showGames() throws Exception {
      return showGames(".*");
   }
   
   @ManagedOperation(description="Show currently present users")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="users", description="Match specific users")
   })
   public String showGames(String users) throws Exception {
      return showGames(users, 1000);
   }
   
   @ManagedOperation(description="Show currently present users")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="users", description="Match specific users"),      
      @ManagedOperationParameter(name="limit", description="The number of games to show")
   })
   public String showGames(String users, int limit) throws Exception {
      TableDrawer drawer = new TableDrawer("gameId", "owner", "challenger", "winner", "declaredBy", "status", "lastMove", "updateTime", "creationTime");
      
      if(limit > 0) {
         List<ChessGameState> listGames = gameDatabase.listGames();
   
         for(ChessGameState gameState : listGames) {
            String gameId = gameState.getGameId();
            String ownerId = gameState.getOwnerId();
            String challengerId = gameState.getChallengerId(); 
            String winnerId = gameState.getWinnerId();
            String declareId = gameState.getDeclareId();            
            
            if(ownerId.contains(users) || ownerId.matches(users) || challengerId.contains(users) || challengerId.matches(users)) {             
               ChessGameStatus gameStatus = gameState.getGameStatus();
               long creationTime = gameState.getCreationTime();              
               DateTime creationDate = DateTime.at(creationTime);
               
               if(limit-- <= 0) {
                  return drawer.drawTable();
               }
               String ownerName = null;
               String challengerName = null;
               String winnerName = null;
               String declareName = null;               
               
               if(ownerId != null) {
                  ownerName = accountDatabase.resolveAccountName(ownerId);
               }
               if(challengerId != null) {
                  challengerName = accountDatabase.resolveAccountName(challengerId);
               }
               if(winnerId != null) {
                  winnerName = accountDatabase.resolveAccountName(winnerId);
               }
               if(declareId != null) {
                  declareName = accountDatabase.resolveAccountName(declareId);
               } else {
                  declareName = declareId;
               }
               ChessMoveState lastMove = gameDatabase.lastMove(gameId);
               DateTime lastUpdate = null;
               
               if(lastMove != null) {
                  long lastMoveTime = lastMove.getTimeMoveMade();
                  
                  if(lastMoveTime > 0) {
                     lastUpdate = DateTime.at(lastMoveTime);
                  }
               }
               TableRowDrawer row = drawer.newRow(
                     "<a href='action=showBoard?action=showBoard&gameId%2Bjava.lang.String=" + gameId + "'>" + gameId + "</a>",                     
                     ownerName, 
                     challengerName,         
                     winnerName,
                     declareName,                     
                     gameStatus,
                     lastMove,
                     lastUpdate, 
                     creationDate);
               
               if(gameStatus == ChessGameStatus.GAME_OVER || gameStatus == ChessGameStatus.RESIGNED) {
                  row.setColor("#ff0000");
               } else if(gameStatus == ChessGameStatus.IN_PROGRESS) {
                  row.setColor("#00ff00");
               }
            }
         }
      }
      return drawer.drawTable();
   }  
   
   @ManagedOperation(description="Draw the specified board")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="gameId", description="Draw specific board")
   })
   public String showBoard(String gameId) throws Exception {
      return showBoard(gameId, 200);
   }
   
   @ManagedOperation(description="Draw the specified board")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="gameId", description="Draw specific board"),
      @ManagedOperationParameter(name="limit", description="The number of moves to show")      
   })
   public String showBoard(String gameId, int limit) throws Exception {
      TableDrawer drawer = new TableDrawer("gameId", "owner", "challenger", "ownerSide", "challengerSide", "update", "moves", "board");
      StringWriter buffer = new StringWriter();
      PrintWriter writer = new PrintWriter(buffer);
      
      if(limit > 0) {
         List<ChessMoveState> moveStates = gameDatabase.listMovesLatestFirst(gameId, limit);
         ChessGameState gameState = gameDatabase.loadGame(gameId);
         ChessBoard chessBoard = gameReader.readGame(gameId);
         TableRowDrawer gameRow = drawer.newRow();        
         String ownerId = gameState.getOwnerId();
         String challengerId = gameState.getChallengerId();
         ChessSide ownerColor = gameState.getOwnerSide();
         ChessSide challengerColor = ownerColor.oppositeSide();                
         DateTime lastUpdate = null;
         
         for(ChessMoveState moveState : moveStates) {
            long timeMoveMade = moveState.getTimeMoveMade();
            
            if(lastUpdate == null) {
               lastUpdate = DateTime.at(timeMoveMade);
            }
            writer.println(moveState);
            writer.flush();
         }         
         String ownerName = null;
         String challengerName = null;
         
         if(ownerId != null) {
            ownerName = accountDatabase.resolveAccountName(ownerId);
         }
         if(challengerId != null) {
            challengerName = accountDatabase.resolveAccountName(challengerId);
         }         
         String board = ChessTextBoardDrawer.drawBoard(chessBoard); 
         String recentMoves = buffer.toString();
         
         gameRow.setNormal("gameId", gameId);
         gameRow.setNormal("owner", ownerName);
         gameRow.setNormal("challenger", challengerName);
         gameRow.setNormal("ownerSide", ownerColor);
         gameRow.setNormal("challengerSide", challengerColor);
         gameRow.setNormal("update", lastUpdate);            
         gameRow.setCode("moves", recentMoves);
         gameRow.setCode("board", board);        
      }
      return drawer.drawTable();
   }    
}
