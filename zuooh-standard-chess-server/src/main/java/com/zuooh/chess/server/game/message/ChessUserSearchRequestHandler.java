package com.zuooh.chess.server.game.message;

import java.util.List;

import com.zuooh.chess.client.event.request.ChessUserSearchEvent;
import com.zuooh.chess.client.event.response.ChessUserSearchResultEvent;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.message.client.RequestHandler;

public class ChessUserSearchRequestHandler implements RequestHandler<ChessUserSearchEvent> {
   
   private final ChessServerController mainController;

   public ChessUserSearchRequestHandler(ChessServerController mainController) throws Exception { 
      this.mainController = mainController; 
   }

   @Override
   public ChessUserSearchResultEvent execute(ChessUserSearchEvent event) throws Exception { 
      String searchTerm = event.getTerm();
      List<ChessUser> users = mainController.findMatchingUsers(searchTerm);

      if(users != null)  {
         ChessUserSearchResultEvent resultEvent = new ChessUserSearchResultEvent(searchTerm);
         
         for(ChessUser user : users){
            resultEvent.addUser(user);
         }
         return resultEvent;
      }
      return new ChessUserSearchResultEvent();
   }
}
