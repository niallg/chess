package com.zuooh.chess.server.game.rank;

/**
 * Represents the result of a match between two players.
 * 
 * @author Jeremy Gooch
 */
public class ChessResult {
   private static final double POINTS_FOR_WIN = 1.0;
   private static final double POINTS_FOR_LOSS = 0.0;
   private static final double POINTS_FOR_DRAW = 0.5;
   
   private boolean isDraw = false;
   private ChessRating winner;
   private ChessRating loser;
   
   
   /**
    * Record a new result from a match between two players.
    * 
    * @param winner
    * @param loser
    */
   public ChessResult(ChessRating winner, ChessRating loser) {
      if ( ! validPlayers(winner, loser) ) {
         throw new IllegalArgumentException();
      }

      this.winner = winner;
      this.loser = loser;
   }
   
   
   /**
    * Record a draw between two players.
    * 
    * @param player1
    * @param player2
    * @param isDraw (must be set to "true")
    */
   public ChessResult(ChessRating player1, ChessRating player2, boolean isDraw) {
      if (! isDraw || ! validPlayers(player1, player2) ) {
         throw new IllegalArgumentException();
      }
      
      this.winner = player1;
      this.loser = player2;
      this.isDraw = true;
   }

   
   /**
    * Check that we're not doing anything silly like recording a match with only one player.
    * 
    * @param player1
    * @param player2
    * @return
    */
   private boolean validPlayers(ChessRating player1, ChessRating player2) {
      if (player1.equals(player2)) {
         return false;
      } 
      return true;      
   }
   
   
   /**
    * Test whether a particular player participated in the match represented by this result.
    * 
    * @param player
    * @return boolean (true if player participated in the match)
    */
   public boolean participated(ChessRating player) {
      if ( winner != null && winner.equals(player) ){
         return true;
      }
      if( loser != null && loser.equals(player) ) {
         return true;
      }     
      return false;      
   }
   
   
   /**
    * Returns the "score" for a match.
    * 
    * @param player
    * @return 1 for a win, 0.5 for a draw and 0 for a loss
    * @throws IllegalArgumentException
    */
   public double getScore(ChessRating player) throws IllegalArgumentException {
      double score;
      
      if ( winner.equals(player) ) {
         score = POINTS_FOR_WIN;
      } else if ( loser.equals(player) ) {
         score = POINTS_FOR_LOSS;         
      } else {
         throw new IllegalArgumentException("Player " + player.getUserId() + " did not participate in match");
      }
      
      if ( isDraw ) {
         score = POINTS_FOR_DRAW;
      }
      
      return score;
   }
   
   
   /**
    * Given a particular player, returns the opponent.
    * 
    * @param player
    * @return opponent
    */
   public ChessRating getOpponent(ChessRating player) {
      ChessRating opponent;
      
      if ( winner.equals(player) ) {
         opponent = loser;
      } else if ( loser.equals(player) ) {
         opponent = winner;         
      } else {
         throw new IllegalArgumentException("Player " + player.getUserId() + " did not participate in match");
      }
      
      return opponent;
   }
   
   
   public ChessRating getWinner() {
      return this.winner;
   }

   
   public ChessRating getLoser() {
      return this.loser;
   }
}
