package com.zuooh.chess.server.game;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.state.ChessGameState;
import com.zuooh.chess.database.state.ChessGameStateDatabase;
import com.zuooh.chess.database.state.ChessMoveState;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.server.game.account.ChessAccount;
import com.zuooh.chess.server.game.account.ChessAccountManager;
import com.zuooh.chess.server.game.account.ChessAccountDatabase;
import com.zuooh.chess.server.game.account.ChessAccountUserBuilder;
import com.zuooh.chess.server.game.account.ChessAccountSearcher;

public class ChessGameStateController implements ChessServerController {

   private final Map<String, ChessGameChangeObserver> chessObservers;
   private final Map<String, ChessGameCreateObserver> gameObservers;
   private final ChessAccountUserBuilder userBuilder;
   private final ChessAccountDatabase  accountDatabase;
   private final ChessAccountSearcher accountSearcher;
   private final ChessAccountManager accountAllocator;
   private final ChessGameStateDatabase gameStore;

   public ChessGameStateController(ChessGameStateDatabase gameStore, ChessAccountDatabase accountDatabase) throws Exception {
      this.chessObservers = new ConcurrentHashMap<String, ChessGameChangeObserver>();
      this.gameObservers = new ConcurrentHashMap<String, ChessGameCreateObserver>();
      this.accountSearcher = new ChessAccountSearcher(accountDatabase);
      this.accountAllocator = new ChessAccountManager(accountDatabase);
      this.userBuilder = new ChessAccountUserBuilder();
      this.accountDatabase = accountDatabase;
      this.gameStore = gameStore;
   }

   public void registerObserver(String observerName, ChessGameChangeObserver chessObserver) throws Exception {
      chessObservers.put(observerName, chessObserver);
   }

   public void registerObserver(String observerName, ChessGameCreateObserver gameObserver) throws Exception {
      gameObservers.put(observerName, gameObserver);
   }

   private ChessGameStatus gameStatus(String gameId) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null)  {
         return game.getGameStatus();
      }
      return null;
   }

   @Override
   public boolean isGameOver(String gameId) throws Exception {
      ChessGameStatus gameState = gameStatus(gameId);
      return gameState == ChessGameStatus.RESIGNED || gameState == ChessGameStatus.GAME_OVER;
   }

   @Override
   public boolean isNew(String gameId) throws Exception {
      ChessGameStatus gameState = gameStatus(gameId);
      return gameState == ChessGameStatus.NEW || gameState == ChessGameStatus.AWAITING_ACCEPTENCE;
   }

   @Override
   public boolean isAwaitingAcceptance(String gameId) throws Exception {
      return gameStatus(gameId) == ChessGameStatus.AWAITING_ACCEPTENCE;
   }

   @Override
   public boolean isResigned(String gameId) throws Exception {
      return gameStatus(gameId) == ChessGameStatus.RESIGNED;
   }

   @Override
   public boolean isInProgress(String gameId) throws Exception {
      return gameStatus(gameId) == ChessGameStatus.IN_PROGRESS;
   }

   @Override
   public ChessSide nextPlayerToMove(String gameId) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game == null)  {
         throw new IllegalStateException("No such game " + game);
      }
      ChessSide lastMoveMadeBy = game.getLastMoveMadeBy();

      if(lastMoveMadeBy != null)  {
         return lastMoveMadeBy.oppositeSide();
      }
      return ChessSide.WHITE;
   }

   @Override
   public List<ChessGame> statusUpdate(String playerId) throws Exception {
      List<ChessGame> startedGames = new LinkedList<ChessGame>();
      List<ChessGameState> gamesCreated = gameStore.listGamesCreatedBy(playerId);

      for(ChessGameState game : gamesCreated)  {
         ChessGame challengeGame = createChallengeGame(game, playerId);
         startedGames.add(challengeGame);         
      }
      List<ChessGameState> gamesAccepted = gameStore.listGamesInvitedTo(playerId);

      for(ChessGameState game : gamesAccepted)  {
         ChessGameStatus gameStatus = game.getGameStatus();

         if(gameStatus == ChessGameStatus.IN_PROGRESS || gameStatus == ChessGameStatus.AWAITING_ACCEPTENCE || gameStatus == ChessGameStatus.RESIGNED || gameStatus == ChessGameStatus.GAME_OVER)  {
            ChessGame challengeGame = createChallengeGame(game, playerId);
            startedGames.add(challengeGame);
         }
      }
      return startedGames;
   }

   @Override
   public List<ChessGame> availableGames(String playerId) throws Exception {
      List<ChessGame> availableGames = new LinkedList<ChessGame>();
      List<ChessGameState> games = gameStore.listGames();
      
      for(ChessGameState game : games)  {
         ChessGameStatus gameStatus = game.getGameStatus();
         String challengerId = game.getChallengerId();
         String ownerName = game.getOwnerId();

         if(gameStatus == ChessGameStatus.NEW && !ownerName.equals(playerId))  {
            ChessGame challengeGame = createChallengeGame(game, playerId);
            availableGames.add(challengeGame);
         } else if(gameStatus == ChessGameStatus.AWAITING_ACCEPTENCE && challengerId.equals(playerId))  {
            ChessGame challengeGame = createChallengeGame(game, playerId);
            availableGames.add(challengeGame);
         }
      }
      return availableGames;
   }

   private ChessGame createChallengeGame(ChessGameState game, String playerId) throws Exception {
      long gameDuration = game.getGameDuration();
      long creationTime = game.getCreationTime();
      String boardSet = game.getBoardSet();
      String gameId = game.getGameId();
      String ownerId = game.getOwnerId();
      ChessSide lastMoveMadeBy = game.getLastMoveMadeBy();
      ChessSide nextToMove = ChessSide.WHITE;
      ChessGameStatus status = game.getGameStatus();
      ChessMove lastMove = lastMove(gameId);
      
      if(lastMoveMadeBy != null)  {
         nextToMove = lastMoveMadeBy.oppositeSide();
      }
      if(ownerId.equals(playerId))  {
         String opponentId = game.getChallengerId();
         ChessSide playerColor = game.getOwnerSide();
         ChessSide opponentColor = playerColor.oppositeSide();
         ChessUser opponentUser = loadUser(opponentId);
         
         ChessGameCriteria opponentChallenge = new ChessGameCriteria(opponentUser, opponentColor, boardSet, gameDuration, creationTime);
         ChessGame challengeGame = new ChessGame(gameId, status, opponentChallenge, lastMove, nextToMove, ownerId);

         return challengeGame;
      } else {
         ChessSide ownerColor = game.getOwnerSide();
         ChessUser ownerUser = loadUser(ownerId);

         ChessGameCriteria ownerChallenge = new ChessGameCriteria(ownerUser, ownerColor, boardSet, gameDuration, creationTime);
         ChessGame challengeGame = new ChessGame(gameId, status, ownerChallenge, lastMove, nextToMove, ownerId);

         return challengeGame;
      }
   }
   
   @Override
   public ChessGame rejectGame(String gameId) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null)  {
         ChessGameStatus gameState = game.getGameStatus();

         if(gameState == ChessGameStatus.AWAITING_ACCEPTENCE) {
            String ownerId = game.getOwnerId();
            
            game.setGameStatus(ChessGameStatus.REJECTED);
            gameStore.saveGame(game);
            return createChallengeGame(game, ownerId);
         }
      }
      return null;
   }
   
   @Override
   public ChessGame loadGame(String gameId, String playerId) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null)  {
         return createChallengeGame(game, playerId);
      }
      return null;
   }   

   @Override
   public ChessGame acceptGame(String gameId, String playerId, int playerSkill) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null)  {
         ChessGameStatus gameState = game.getGameStatus();
         String gameOwner = game.getOwnerId();

         if(!gameOwner.equals(playerId))  {            
            if(gameState == ChessGameStatus.NEW) {
               game.setChallengerId(playerId);
               game.setGameStatus(ChessGameStatus.IN_PROGRESS);
               gameStore.saveGame(game);
               notifyGameStart(game);
               return createChallengeGame(game, playerId);
            }
            if(gameState == ChessGameStatus.AWAITING_ACCEPTENCE) {
               String challengerId = game.getChallengerId();
               
               if(playerId.equals(challengerId)) { // only accept if specifically challenged               
                  game.setGameStatus(ChessGameStatus.IN_PROGRESS);
                  gameStore.saveGame(game);
                  notifyGameStart(game);
                  return createChallengeGame(game, playerId);
               }
            }            
         }
      }
      return null;
   }

   @Override
   public ChessGame createGame(ChessGameCriteria challenge, String gameId, String challengePlayer) throws Exception {
      ChessGameState existingGame = gameStore.loadGame(gameId);
      
      if(existingGame == null)  {
         ChessUser ownerUser = challenge.getUser();
         ChessSide ownerColor = challenge.getUserSide();
         String ownerName = ownerUser.getKey();
         String boardSet = challenge.getThemeColor();
         long gameDuration = challenge.getGameDuration();
         long creationTime = challenge.getCreationTime();
         ChessGameState game = new ChessGameState(gameId, ownerName, ownerColor, boardSet, gameDuration, creationTime);
   
         if(challengePlayer == null)  {
            game.setGameStatus(ChessGameStatus.NEW);
         } else {
            game.setGameStatus(ChessGameStatus.AWAITING_ACCEPTENCE);
            game.setChallengerId(challengePlayer);
         }
         gameStore.saveGame(game);
         saveUser(ownerUser); // chance to save anonymous user user, which may have mail address
         notifyNewGame(game);
         return createChallengeGame(game, ownerName);
      }
      String ownerName = existingGame.getOwnerId();
      return createChallengeGame(existingGame, ownerName);
   }

   @Override
   public ChessGameCriteria gameDetails(String gameId, String playerId) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null)  {
         ChessGame challengeGame = createChallengeGame(game, playerId);
         return challengeGame.getCriteria();
      }
      return null;
   }

   @Override
   public ChessMove lastMoveMadeBy(String gameId, String playerId) throws Exception {
      ChessMoveState lastMove = gameStore.lastMoveMadeBy(gameId, playerId);

      if(lastMove != null)  {
         ChessBoardPosition from = lastMove.getFromPosition();
         ChessBoardPosition to = lastMove.getToPosition();
         ChessSide playerColor = lastMove.getUserSide();
         int changeCount = lastMove.getChangeCount();
         
         return new ChessMove(from, to, playerColor, changeCount);
      }
      return null;
   }

   public ChessMove nextMove(String gameId, String playerId) throws Exception {
      ChessMoveState nextMove = gameStore.nextMove(gameId, playerId);

      if(nextMove != null)  {
         ChessBoardPosition from = nextMove.getFromPosition();
         ChessBoardPosition to = nextMove.getToPosition();
         ChessSide playerColor = nextMove.getUserSide();
         int changeCount = nextMove.getChangeCount();

         return new ChessMove(from, to, playerColor, changeCount);
      }
      return null;
   }
   
   public ChessMove lastMove(String gameId) throws Exception {
      ChessMoveState nextMove = gameStore.lastMove(gameId);

      if(nextMove != null)  {
         ChessBoardPosition from = nextMove.getFromPosition();
         ChessBoardPosition to = nextMove.getToPosition();
         ChessSide playerColor = nextMove.getUserSide();
         int changeCount = nextMove.getChangeCount();

         return new ChessMove(from, to, playerColor, changeCount);
      }
      return null;
   }

   @Override
   public boolean makeMove(String gameId, ChessMove chessMove, String userId) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null && chessMove != null)  {
         long moveTime = System.currentTimeMillis();
         ChessSide playerSide = chessMove.side;
         ChessSide lastMoveMadeBy = game.getLastMoveMadeBy();
         ChessSide nextToMove = ChessSide.WHITE;

         if(lastMoveMadeBy != null)  {
            nextToMove = lastMoveMadeBy.oppositeSide();
         }
         if(isRepeatMove(gameId, chessMove, userId))  {
            return false;
         }
         if(playerSide != nextToMove)  {
            //throw new IllegalStateException("Player '" +playerId+ "' is attempting to move out of turn");
            return false;
         }
         ChessMoveState newMove = new ChessMoveState(gameId, userId, playerSide, chessMove.from, chessMove.to, moveTime, chessMove.change);
         ChessMoveState lastMoveByOpponent = gameStore.lastMoveMadeByOpponent(gameId, userId);

         if(lastMoveByOpponent != null)  {
            lastMoveByOpponent.setAcknowledged(true);
            gameStore.saveMove(lastMoveByOpponent);
         }
         gameStore.saveMove(newMove);
         game.setLastMoveMadeBy(playerSide);
         gameStore.saveGame(game);
         notifyGameMove(game, newMove);
         return true;
      }
      return false;
   }
   
   private void notifyGameStart(ChessGameState gameState) throws Exception {
      Set<String> observerNames = chessObservers.keySet();

      for(String observerName : observerNames)  {
         ChessGameChangeObserver chessObserver = chessObservers.get(observerName);

         if(chessObserver != null)  {
            chessObserver.onStart(gameState);
         }
      }
   }   

   private void notifyGameMove(ChessGameState gameState, ChessMoveState newMove) throws Exception {
      Set<String> observerNames = chessObservers.keySet();

      for(String observerName : observerNames)  {
         ChessGameChangeObserver chessObserver = chessObservers.get(observerName);

         if(chessObserver != null)  {
            chessObserver.onMove(gameState, newMove);
         }
      }
   }

   private void notifyGameOver(ChessGameState gameState) throws Exception {
      Set<String> observerNames = chessObservers.keySet();

      for(String observerName : observerNames)  {
         ChessGameChangeObserver chessObserver = chessObservers.get(observerName);

         if(chessObserver != null) {
            chessObserver.onFinish(gameState);
         }
      }
   }

   private void notifyNewGame(ChessGameState gameState) throws Exception {
      Set<String> observerNames = gameObservers.keySet();

      for(String observerName : observerNames) {
         ChessGameCreateObserver chessObserver = gameObservers.get(observerName);

         if(chessObserver != null) {
            chessObserver.onCreate(gameState);
         }
      }
   }

   private boolean isRepeatMove(String gameId, ChessMove chessMove, String playerId) throws Exception {
      ChessMove lastMove = lastMoveMadeBy(gameId, playerId);

      if(lastMove == null) {
         return false;
      }
      if(chessMove.from != lastMove.from){
         return false;
      }
      if(chessMove.to != lastMove.to) {
         return false;
      }
      return true;
   }

   @Override
   public void resignGame(String gameId, String playerId) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null) {
         String ownerId = game.getOwnerId();
         String challengerId = game.getChallengerId();
         
         if(playerId.equals(ownerId)) {
            gameOver(game, ChessGameStatus.RESIGNED, playerId, challengerId);
         } else {
            gameOver(game, ChessGameStatus.RESIGNED, playerId, ownerId);
         }
         notifyGameOver(game);
      }
   }

   @Override
   public void gameOver(String gameId, String declareId) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null) {
         gameOver(game, ChessGameStatus.GAME_OVER, declareId, null);
         notifyGameOver(game);
      }
   }   
   
   @Override
   public void gameOver(String gameId, String declareId, String winnerName) throws Exception {
      ChessGameState game = gameStore.loadGame(gameId);

      if(game != null) {
         gameOver(game, ChessGameStatus.GAME_OVER, declareId, winnerName);
         notifyGameOver(game);
      }
   }
   
   private void gameOver(ChessGameState game, ChessGameStatus status, String declareId, String winnerName) throws Exception {
      String ownerId = game.getOwnerId();
      String challengerId = game.getChallengerId();
      ChessAccount ownerAccount = accountDatabase.loadAccount(ownerId);
      
      if(challengerId != null) {
         ChessAccount challengerAccount = accountDatabase.loadAccount(challengerId);
         ChessSide ownerColor = game.getOwnerSide();      
     
         if(ownerAccount != null && challengerAccount != null) { // anonymous games do not get ranked... should prevent someone calling themselves Anonymous and stealing points
            int ownerBlackGames = ownerAccount.getBlackGames();
            int ownerWhiteGames = ownerAccount.getWhiteGames();
            int challengerBlackGames = challengerAccount.getBlackGames();
            int challengerWhiteGames = challengerAccount.getWhiteGames();
            long currentTime = System.currentTimeMillis();
            
            if(ownerColor == ChessSide.WHITE) {
               ownerAccount.setWhiteGames(ownerWhiteGames + 1);
               challengerAccount.setBlackGames(challengerBlackGames + 1);
            } else {
               ownerAccount.setBlackGames(ownerBlackGames + 1);
               challengerAccount.setWhiteGames(challengerWhiteGames + 1);
            }      
            ownerAccount.setLastSeenOnline(currentTime);
            challengerAccount.setLastSeenOnline(currentTime);;
            accountDatabase.saveAccount(ownerAccount);
            accountDatabase.saveAccount(challengerAccount);
         }
         game.setWinnerId(winnerName);
      }
      game.setDeclareId(declareId); // who declared it a game over?
      game.setGameStatus(status);
      gameStore.saveGame(game);      
   }
   
   @Override
   public List<ChessUser> findBestUsers(int count) throws Exception {
      List<ChessAccount> accounts = accountSearcher.findHighestRatedAccounts();
      
      if(!accounts.isEmpty()) {
         List<ChessUser> bestUsers = new ArrayList<ChessUser>();
         
         for(ChessAccount account : accounts) {
            ChessUser user = userBuilder.createUser(account);
            
            if(user != null) {
               bestUsers.add(user);
            }
            int size = bestUsers.size();
            
            if(size >= count) {
               return bestUsers;
            }
         }
         return bestUsers;
      }
      return new ArrayList<ChessUser>();
   }   

   @Override
   public List<ChessUser> findMatchingUsers(String searchTerm) throws Exception {
      List<ChessAccount> accounts = accountSearcher.findMostRecentlyActiveAccounts(searchTerm);
      
      if(!accounts.isEmpty()) {
         List<ChessUser> matchingUsers = new ArrayList<ChessUser>();
         
         for(ChessAccount account : accounts) {
            ChessUser user = userBuilder.createUser(account);
            
            if(user != null) {
               matchingUsers.add(user);
            }
            int size = matchingUsers.size();
            
            if(size >= 10) {
               return matchingUsers;
            }
         }
         return matchingUsers;
      }
      return new ArrayList<ChessUser>();
   }
   
   @Override
   public ChessUser loadUser(String playerId) throws Exception {
      if(playerId != null) {
         ChessAccount account = accountDatabase.loadAccount(playerId);
         
         if(account != null) {
            return userBuilder.createUser(account);
         }
      }
      return null;
   }

   @Override
   public void saveUser(ChessUser user) throws Exception {
      accountAllocator.updateAccount(user);
   }
}
