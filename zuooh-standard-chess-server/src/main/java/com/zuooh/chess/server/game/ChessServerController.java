package com.zuooh.chess.server.game;

import java.util.List;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;

public interface ChessServerController {
   boolean isGameOver(String gameId) throws Exception;
   boolean isNew(String gameId) throws Exception;
   boolean isResigned(String gameId) throws Exception;
   boolean isAwaitingAcceptance(String gameId) throws Exception;
   boolean isInProgress(String gameId) throws Exception;
   ChessSide nextPlayerToMove(String gameId) throws Exception;
   List<ChessGame> statusUpdate(String userId) throws Exception;
   List<ChessGame> availableGames(String userId) throws Exception;
   ChessGame loadGame(String gameId, String userId) throws Exception;
   ChessGame rejectGame(String gameId) throws Exception; 
   ChessGame acceptGame(String gameId, String userId, int userSkill) throws Exception;
   ChessGame createGame(ChessGameCriteria challenge, String gameId, String challengerId) throws Exception;
   ChessGameCriteria gameDetails(String gameId, String userId) throws Exception;
   ChessMove lastMove(String gameId) throws Exception;
   ChessMove lastMoveMadeBy(String gameId, String userId) throws Exception;
   ChessMove nextMove(String gameId, String userId) throws Exception;
   List<ChessUser> findBestUsers(int count) throws Exception;   
   List<ChessUser> findMatchingUsers(String searchTerm) throws Exception;
   ChessUser loadUser(String userId) throws Exception;   
   void saveUser(ChessUser user) throws Exception;
   void registerObserver(String observerName, ChessGameChangeObserver chessObserver) throws Exception;
   void registerObserver(String observerName, ChessGameCreateObserver chessObserver) throws Exception;
   boolean makeMove(String gameId, ChessMove chessMove, String userId) throws Exception;  
   void resignGame(String gameId, String userId) throws Exception;
   void gameOver(String gameId, String declareId, String winnerId) throws Exception;
   void gameOver(String gameId, String declareId) throws Exception;
}
