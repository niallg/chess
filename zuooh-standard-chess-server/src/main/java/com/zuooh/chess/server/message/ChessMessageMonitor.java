package com.zuooh.chess.server.message;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.io.Serializable;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.common.html.TableDrawer;
import com.zuooh.common.time.SampleAverager;
import com.zuooh.message.client.receive.ExecutionTimer;

@ManagedResource(description="Monitor execution performance")
public class ChessMessageMonitor implements ExecutionTimer {
   
   private final Map<Class, SampleAverager> averagers;
   private final TimeUnit measurement;

   public ChessMessageMonitor() {
      this(MILLISECONDS);
   }
   
   public ChessMessageMonitor(TimeUnit measurement) {
      this.averagers = new ConcurrentHashMap<Class, SampleAverager>();
      this.measurement = measurement;
   }
   
   @ManagedOperation(description="Show execution measurements")
   public String showMeasurements() {
      TableDrawer drawer = new TableDrawer("type", "count", "average", "maximum", "minimum");
      
      if(!averagers.isEmpty()) {
         Set<Class> types = averagers.keySet();
         
         for(Class type : types) {
            SampleAverager averager = averagers.get(type);
            long count = averager.count();
            long average = averager.average();
            long maximum = averager.maximum();
            long minimum = averager.minimum();
            
            drawer.newRow(type, count, average, maximum, minimum);            
         }
      }
      return drawer.drawTable();
   }   
   
   @ManagedOperation(description="Clear measurements")
   public void clearMeasurements() {
      averagers.clear();
   }    

   @Override
   public void update(Serializable event, long duration, TimeUnit unit) {
      Class type = event.getClass();
      SampleAverager averager = averagers.get(type);
      long time = measurement.convert(duration, unit);
      
      if(averager == null) {
         averager = new SampleAverager();
         averagers.put(type, averager);
      }
      averager.sample(time);      
   }  
}
