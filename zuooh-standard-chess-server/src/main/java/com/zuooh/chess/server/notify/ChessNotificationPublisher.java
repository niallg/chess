package com.zuooh.chess.server.notify;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.event.ChessNotificationEvent;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.notify.ChessNotification;
import com.zuooh.chess.database.notify.ChessNotificationDatabase;
import com.zuooh.chess.database.notify.ChessNotificationType;
import com.zuooh.chess.server.game.ChessServerPublisher;

public class ChessNotificationPublisher {

   private static final Logger LOG = LoggerFactory.getLogger(ChessNotificationPublisher.class);
   
   private final ChessNotificationDatabase notificationDatabase;
   private final ChessServerPublisher serverPublisher;

   public ChessNotificationPublisher(ChessNotificationDatabase notificationDatabase, ChessServerPublisher serverPublisher) {
      this.notificationDatabase = notificationDatabase;
      this.serverPublisher = serverPublisher;
   }
  
   public void publishNotification(ChessNotification notification, ChessGame game) {
      try {
         ChessNotificationType type = notification.getType();
         String gameId = notification.getGameId();
         String userId = notification.getUserId();                       
         
         if(!type.isMove()) { // don't publish moves straight away
            ChessNotificationEvent notificationEvent = new ChessNotificationEvent(type, gameId, userId);                   
            
            if(type.isFinish()) { // when finished all move notifications should be cleared               
               notificationDatabase.markUserNotificationsAsReceived(userId, gameId);               
            }
            if(game != null) {
               List<ChessGame> gameList = new ArrayList<ChessGame>();
               ChessListOfGamesEvent listGamesEvent = new ChessListOfGamesEvent(gameList);     
               
               gameList.add(game);
               serverPublisher.publishNotification(listGamesEvent, userId); // make sure user gets the game update also
            }
            serverPublisher.publishNotification(notificationEvent, userId);            
         } else { // opponent just made the move so is up to date on the current status of game
            notificationDatabase.markOpponentNotificationsAsReceived(userId, gameId);
         }
         notificationDatabase.saveNotification(notification);
      }catch(Exception e) {
         LOG.info("Unable to publish notification", e);
      }
   }

}
