package com.zuooh.chess.server.game.chat;

import com.zuooh.chess.client.event.chat.ChessChatEvent;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.NotificationHandler;

public class ChessChatNotificationHandler implements NotificationHandler<ChessChatEvent> {
   
   private final ChessServerPublisher serverPublisher;
   private final ChessChatDatabase chatDatabase;

   public ChessChatNotificationHandler(ChessChatDatabase chatDatabase, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.chatDatabase = chatDatabase;
   }

   @Override
   public void handle(ChessChatEvent event) throws Exception {
      ChessChat chat = event.getChat();

      if(chat != null)  {
         String toId = chat.getTo();
         
         chatDatabase.saveMessage(chat);
         serverPublisher.publishNotification(event, toId);
      }
   }
}
