package com.zuooh.chess.server.game.account;

import java.util.List;

import com.zuooh.database.bind.DatabaseBinder;

public class ChessAccountDatabase {

   private final DatabaseBinder binder;

   public ChessAccountDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   public synchronized List<ChessAccount> listAccounts() throws Exception {
      return binder.withTable(ChessAccount.class)
            .select()
            .orderBy("lastSeenOnline desc")
            .execute()
            .fetchAll(); 
   }
   
   public synchronized List<ChessAccount> listAccounts(int limit) throws Exception {
      return binder.withTable(ChessAccount.class)
            .select()
            .orderBy("lastSeenOnline desc")
            .limit(limit)
            .execute()
            .fetchAll(); 
   }   

   public synchronized List<ChessAccount> listRecentlyActiveAccounts(String term) throws Exception {
      return binder.withTable(ChessAccount.class)
            .select()
            .where("name like :term or mail == :term")
            .set("term", "%" + term + "%")
            .orderBy("lastSeenOnline desc")
            .execute()
            .fetchNext(40);  
   }
   
   public synchronized List<ChessAccount> listHighestRatedAccounts() throws Exception {
      return binder.withTable(ChessAccount.class)
            .select()
            .where("rank > 0")
            .orderBy("rank asc")
            .execute()
            .fetchNext(40);  
   }   
   
   public synchronized List<ChessAccount> listHighestRatedAccounts(String term) throws Exception {
      return binder.withTable(ChessAccount.class)
            .select()
            .where("name like :term and rank > 0")
            .set("term", "%" + term + "%")
            .orderBy("rank asc")
            .execute()
            .fetchNext(40);  
   }   

   public synchronized ChessAccount loadAccount(String playerId) throws Exception {
      return binder.withTable(ChessAccount.class)
            .select()
            .where("key == :key")
            .set("key", playerId)               
            .execute()
            .fetchFirst();  
   }
   
   public synchronized String resolveAccountName(String playerId) throws Exception {
      ChessAccount account = binder.withTable(ChessAccount.class)
            .select()
            .where("key == :key")
            .set("key", playerId)               
            .execute()
            .fetchFirst();  
      
      if(account != null) {
         return account.getName();
      }
      return null;
   }
   
   public synchronized boolean containsAccount(String playerId) throws Exception {
      int count = binder.withTable(ChessAccount.class)
            .selectCount()
            .where("key == :key")
            .set("key", playerId)
            .execute();
      
      return count != 0;
   }

   public synchronized void saveAccount(ChessAccount account) throws Exception {
      binder.withTable(ChessAccount.class)
         .updateOrInsert()
         .execute(account);
   }
   
   public synchronized void saveAccounts(List<ChessAccount> accounts) throws Exception {
      binder.withTable(ChessAccount.class)
         .begin()
         .execute();
      
      try {
         for(ChessAccount account : accounts) {
            binder.withTable(ChessAccount.class)
               .updateOrInsert()
               .execute(account);
         }
      } finally {
         binder.withTable(ChessAccount.class)
            .commit()
            .execute();
      }
   }

   public synchronized void removeAccount(String playerId) throws Exception {
      ChessAccount account = loadAccount(playerId);
      
      if(account != null) {
         binder.withTable(ChessAccount.class)
         .delete() 
         .execute(account); 
      }
   }

   public synchronized void clearAccounts() throws Exception {
      binder.withTable(ChessAccount.class)
      .truncate() 
      .execute();      
   }
}
