package com.zuooh.chess.server.game.presence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.server.game.account.ChessAccount;
import com.zuooh.chess.server.game.account.ChessAccountDatabase;
import com.zuooh.chess.server.game.account.ChessAccountUserBuilder;
import com.zuooh.common.task.Task;

public class ChessPresenceCache implements Task {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessPresenceCache.class);
   
   private final Map<String, ChessPresenceRecord> presenceCache;
   private final ChessAccountUserBuilder builder;
   private final ChessPresenceNotifier notifier;
   private final ChessAccountDatabase database;
   private final long expiry;
   private final long frequency;
   
   public ChessPresenceCache(ChessPresenceNotifier notifier, ChessAccountDatabase database, long frequency, long expiry) {
      this.presenceCache = new HashMap<String, ChessPresenceRecord>();
      this.builder = new ChessAccountUserBuilder();
      this.frequency = frequency;      
      this.notifier = notifier;
      this.database = database;
      this.expiry = expiry;
   }  
   
   @Override
   public synchronized long executeTask() {
      try {
         clearExpiredPresence();
      }catch(Exception e) {
         LOG.info("Error maintaining presence cache", e);
      }
      return frequency;
   }
   
   private synchronized void clearExpiredPresence() throws Exception {
      Set<String> playerIds = presenceCache.keySet();
      
      if(!playerIds.isEmpty()) {
         List<String> expiredPresence = new ArrayList<String>();
         
         for(String playerId : playerIds) {
            ChessAccount account = database.loadAccount(playerId);
            long currentTime = System.currentTimeMillis();
            long lastPresent = account.getLastSeenOnline();
            long timeElapsed = currentTime - lastPresent;
            
            if(timeElapsed > expiry) {
               try {
                  expiredPresence.add(playerId);
               }catch(Exception e) {
                  LOG.info("Could not persist profile", e);
               }
            }
         }
         for(String playerId : expiredPresence) {
            ChessPresenceRecord presence = presenceCache.remove(playerId);
            
            if(presence != null) {
               notifier.notifyExpiry(presence);
            }
         }
      }
   }
   
   public synchronized boolean currentlyPresent(String playerId) throws Exception {
      return presenceCache.containsKey(playerId);
   }
   
   public synchronized void updatePresence(String playerId, boolean anonymous) throws Exception {
      ChessPresenceRecord presence = presenceCache.get(playerId);
      
      if(presence == null) {         
         presence = createPresence(playerId, anonymous);    
         presenceCache.put(playerId,  presence);
      }
      ChessAccount account = database.loadAccount(playerId);      
      long currentTime = System.currentTimeMillis();
      
      account.setLastSeenOnline(currentTime);
      database.saveAccount(account);
      presence.updatePresence();   
      notifier.notifyUpdate(presence);
   }
   
   private synchronized ChessPresenceRecord createPresence(String playerId, boolean anonymous) throws Exception {      
      ChessAccount account = database.loadAccount(playerId);
      long time = System.currentTimeMillis();
      
      if(account == null) { // only anonymous accounts are dynamically allocated!!
         if(anonymous) {
            account = builder.createAnonymousAccount(playerId);
         } else {
            account = builder.createInvalidAccount(playerId);
         }         
         if(account != null) {
            database.saveAccount(account);
         }
      }
      return new ChessPresenceRecord(playerId, time);
   }
}
