package com.zuooh.chess.server.game.bot;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessMoveEngineProvider;

public class ChessServerBotBuilder {

   private static final String[][] SET = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};

   private final ChessMoveEngineProvider engineProvider;
   private final ChessServerBotPublisher botPublisher;
   private final ChessPieceSet chessSet;  

   public ChessServerBotBuilder(ChessServerBotPublisher botPublisher, ChessMoveEngineProvider engineProvider) {
      this.chessSet = new ChessTextPieceSet(SET);
      this.engineProvider = engineProvider;
      this.botPublisher = botPublisher;
   }

   public ChessServerBot createNewBot(ChessUser botProfile, String gameId, String opponentId, ChessSide side, int botSkill) throws Exception {
      String botName = botProfile.getKey();
      ChessBoard chessBoard = createChessBoard(botName, gameId, side);
      ChessMoveEngine chessEngine = createChessEngine(side, botSkill);
      ChessServerBot chessBot = createChessBot(chessBoard, chessEngine, botProfile, gameId, opponentId);
      
      botPublisher.saveUser(botProfile);

      return chessBot;
   }

   private ChessMoveEngine createChessEngine(ChessSide side, int botSkill) throws Exception {     
      return engineProvider.createEngine(side, botSkill);
   }
   
   private ChessServerBot createChessBot(ChessBoard chessBoard, ChessMoveEngine chessEngine, ChessUser botProfile, String gameId, String opponentId) throws Exception {
      return new ChessServerBot(botPublisher, chessBoard, chessEngine, botProfile, gameId, opponentId);
   }

   private ChessBoard createChessBoard(String botName, String gameId, ChessSide side) throws Exception {
      return new ChessPieceSetBoard(chessSet);
   }
}
