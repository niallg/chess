package com.zuooh.chess.server.game.presence;

import java.util.Comparator;

import com.zuooh.common.time.DateTime;

public class ChessPresenceComparator implements Comparator<ChessPresence> {

   @Override
   public int compare(ChessPresence left, ChessPresence right) {
      DateTime leftTime = left.getLastPresent();
      DateTime rightTime = right.getLastPresent();
      
      return rightTime.compareTo(leftTime);
   }
}
