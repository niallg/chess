package com.zuooh.chess.server.game.presence;

import com.zuooh.chess.client.event.presence.ChessPresenceEvent;
import com.zuooh.message.client.NotificationHandler;

public class ChessPresenceNotificationHandler implements NotificationHandler<ChessPresenceEvent> {
   
   private final ChessPresenceCache cache;
   
   public ChessPresenceNotificationHandler(ChessPresenceCache cache) {
      this.cache = cache;
   }

   @Override
   public void handle(ChessPresenceEvent value) throws Exception {
      String userId = value.getUserId();
      boolean anonymous = value.isAnonymous();
      
      if(userId != null) {
         cache.updatePresence(userId, anonymous);
      }
   }

}
