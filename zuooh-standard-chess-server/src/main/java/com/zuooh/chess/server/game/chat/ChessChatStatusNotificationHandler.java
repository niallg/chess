package com.zuooh.chess.server.game.chat;

import com.zuooh.chess.client.event.chat.ChessChatStatusEvent;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.database.chat.ChessChatStatus;
import com.zuooh.message.client.NotificationHandler;

public class ChessChatStatusNotificationHandler implements NotificationHandler<ChessChatStatusEvent> {
   
   private final ChessChatDatabase chatDatabase;

   public ChessChatStatusNotificationHandler(ChessChatDatabase chatDatabase) throws Exception {
      this.chatDatabase = chatDatabase;
   }

   @Override
   public void handle(ChessChatStatusEvent event) throws Exception {
      ChessChatStatus status = event.getStatus();
      String chatId = event.getChatId();

      if(status.isRead())  {
         chatDatabase.deleteMessage(chatId);
      }
   }
}
