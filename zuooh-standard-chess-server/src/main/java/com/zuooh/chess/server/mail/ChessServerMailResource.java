package com.zuooh.chess.server.mail;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.http.resource.Resource;

@ManagedResource(description="Verify player profiles")
public class ChessServerMailResource implements Resource {
   
   private final Cache<String, ChessUser> profiles;
   private final ChessServerController mainController;
   private final Resource success;
   private final Resource failure;
   private final String attribute;
   
   public ChessServerMailResource(ChessServerController mainController, Resource success, Resource failure, String attribute) {
      this.profiles = new LeastRecentlyUsedCache<String, ChessUser>(1000);
      this.mainController = mainController;
      this.failure = failure;
      this.success = success;
      this.attribute = attribute;
   }
   
   public void register(String token, ChessUser profile) {
      profiles.cache(token, profile);
   }

   @Override
   public void handle(Request request, Response response) throws Throwable {
      String value = request.getParameter(attribute);
      
      if(value != null) {
         ChessUser profile = profiles.take(value);
         
         if(profile != null) {
            mainController.saveUser(profile);
            success.handle(request, response);
         } else {
            failure.handle(request, response);
         }
      } else {
         failure.handle(request, response);
      }
   }
}
