package com.zuooh.chess.server.game.message;

import com.zuooh.chess.client.event.ChessDrawEvent;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.RequestHandler;

public class ChessDrawRequestHandler implements RequestHandler<ChessDrawEvent> {
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;

   public ChessDrawRequestHandler(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception { 
      this.mainController = mainController;   
      this.serverPublisher = serverPublisher;
   }

   @Override
   public ChessDrawEvent execute(ChessDrawEvent event) throws Exception {  
      String opponentId = event.getOpponentId();
      
      if(opponentId != null) {
         serverPublisher.publishNotification(event, opponentId);
      }
      return event;
   }
}
