package com.zuooh.chess.server.game.account;

import java.io.Serializable;

public class ChessAccount implements Serializable {
   
   private static final long serialVersionUID = 1L;

   private ChessAccountType type;
   private String key;
   private String name;
   private String mail;
   private String password;
   private int rank;
   private int percentile;   
   private double rating;
   private double deviation;
   private double volatility;
   private int blackGames;
   private int whiteGames;
   private long lastSeenOnline;

   public ChessAccount(ChessAccountType type, String key, String name, String mail, String password) {
      this.key = key.toLowerCase();
      this.password = password;      
      this.type = type;
      this.name = name;
      this.mail = mail;
   }
   
   public ChessAccountType getType() {
      return type;
   }
   
   public void setType(ChessAccountType type) {
      this.type = type;
   }
   
   public String getMail() {
      return mail;
   }

   public void setMail(String mail) {
      this.mail = mail;
   }   

   public String getName() {
      return name;
   }

   public void setName(String name) {
      this.name = name;
   }

   public String getPassword() {
      return password;
   }

   public void setPassword(String password) {
      this.password = password;
   }

   public String getKey() {
      return key;
   }

   public void setKey(String userId) {
      this.key = userId;
   }
   
   public int getRank() {
      return rank;
   }
   
   public void setRank(int rank) {
      this.rank = rank;
   }
   
   public int getPercentile() {
      return percentile;
   }
   
   public void setPercentile(int percentile) {
      this.percentile = percentile;
   }

   public double getRating() {
      return rating;
   }

   public void setRating(double rating) {
      this.rating = rating;
   }
   
   public double getVolatility() {
      return volatility;
   }
   
   public void setVolatility(double volatility) {
      this.volatility = volatility;      
   }
   
   public double getDeviation() {
      return deviation;
   }
   
   public void setDeviation(double deviation) {
      this.deviation = deviation;
   }

   public int getBlackGames() {
      return blackGames;
   }

   public void setBlackGames(int blackGames) {
      this.blackGames = blackGames;
   }

   public int getWhiteGames() {
      return whiteGames;
   }

   public void setWhiteGames(int whiteGames) {
      this.whiteGames = whiteGames;
   }

   public long getLastSeenOnline() {
      return lastSeenOnline;
   }

   public void setLastSeenOnline(long lastSeenOnline) {
      this.lastSeenOnline = lastSeenOnline;
   }
   
   @Override
   public String toString() {
      return String.format("name=%s id=%s", name, key);
   }
}
