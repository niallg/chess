package com.zuooh.chess.server.game.message;

import com.zuooh.chess.client.event.ChessMoveEvent;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.RequestHandler;

public class ChessMoveRequestHandler implements RequestHandler<ChessMoveEvent> {
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;

   public ChessMoveRequestHandler(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.mainController = mainController;   
   }

   @Override
   public ChessMoveEvent execute(ChessMoveEvent event) throws Exception { 
     ChessMove move = event.getMove();
     String playerName = event.getUserId();
     String gameId = event.getGameId();

     if(mainController.makeMove(gameId, move, playerName))  {
        ChessGameCriteria challenge = mainController.gameDetails(gameId, playerName);
        ChessUser profile = challenge.getUser();
        String challengerId = null;
        
        if(profile != null) { // has a user accepted!!
           challengerId = profile.getKey();
           serverPublisher.publishNotification(event, challengerId); // tell the player straight away if present!!
        }
        return new ChessMoveEvent(move, playerName, challengerId, gameId);        
     }        
     return null;
   }
}
