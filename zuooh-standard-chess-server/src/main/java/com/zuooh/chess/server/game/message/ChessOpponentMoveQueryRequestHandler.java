package com.zuooh.chess.server.game.message;

import com.zuooh.chess.client.event.ChessMoveEvent;
import com.zuooh.chess.client.event.request.ChessOpponentMoveQueryEvent;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.message.client.RequestHandler;

public class ChessOpponentMoveQueryRequestHandler implements RequestHandler<ChessOpponentMoveQueryEvent> {
   
   private final ChessServerController mainController;

   public ChessOpponentMoveQueryRequestHandler(ChessServerController mainController) throws Exception { 
      this.mainController = mainController;  
   }

   @Override
   public ChessMoveEvent execute(ChessOpponentMoveQueryEvent event) throws Exception {
      String playerId = event.getUserId();
      String gameId = event.getGameId();
      ChessMove move = mainController.nextMove(gameId, playerId);

      if(move != null)  {
         ChessGameCriteria challenge = mainController.gameDetails(gameId, playerId);
         ChessUser profile = challenge.getUser();
         
         if(profile != null)  { // will be null if nobody has accepted yet!!
            String challengerName = profile.getKey();
   
            if(challengerName != null)  {
               return new ChessMoveEvent(move, challengerName, playerId, gameId);
            }
         }
      } 
      return null;
   }
}
