package com.zuooh.chess.server.game.message;

import java.util.List;

import com.zuooh.chess.client.event.request.ChessSignInEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.message.client.RequestHandler;

public class ChessSignInRequestHandler implements RequestHandler<ChessSignInEvent> {

   private final ChessServerController mainController;

   public ChessSignInRequestHandler(ChessServerController mainController) throws Exception { 
      this.mainController = mainController;  
   } 

   @Override
   public ChessUserValidationEvent execute(ChessSignInEvent event) throws Exception {
      String userName = event.getUserName();
      String userPassword = event.getPassword();
      List<ChessUser> existingUsers = mainController.findMatchingUsers(userName);
      
      for(ChessUser existingUser : existingUsers) {
         String profileName = existingUser.getName();
         
         if(profileName.equalsIgnoreCase(userName)) {
            String existingPassword = existingUser.getPassword();
            
            if(existingPassword != null && !existingPassword.equals(userPassword))  {
               return new ChessUserValidationEvent(ChessUserValidation.PASSWORD_IS_INCORRECT, existingUser);                
            }
            return new ChessUserValidationEvent(ChessUserValidation.PASSWORD_VERIFIED, existingUser);  
         }         
      }             
      return new ChessUserValidationEvent(ChessUserValidation.USER_NAME_NOT_REGISTERED, null);         
   }
}
