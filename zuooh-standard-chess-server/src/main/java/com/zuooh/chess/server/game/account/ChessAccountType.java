package com.zuooh.chess.server.game.account;

public enum ChessAccountType {
   ANONYMOUS,
   INVALID,
   PREMIUM,
   ACTIVE,
   INACTIVE;
   
   public boolean isAnonymous() {
      return this == ANONYMOUS;
   }
   
   public boolean isInvalid() {
      return this == INVALID;
   }   
}
