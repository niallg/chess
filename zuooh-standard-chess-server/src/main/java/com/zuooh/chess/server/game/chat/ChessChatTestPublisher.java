package com.zuooh.chess.server.game.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.chess.client.event.chat.ChessChatEvent;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.chat.ChessChatStatus;

@ManagedResource(description="Send a test chat message")
public class ChessChatTestPublisher {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessChatTestPublisher.class);

   private final ChessChatNotificationHandler handler;
   
   public ChessChatTestPublisher(ChessChatNotificationHandler handler) {
      this.handler = handler;
   }
   
   @ManagedOperation(description="Send a test chat message")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="chatId", description="Test chat identifier"),
      @ManagedOperationParameter(name="gameId", description="Test game identifier"),
      @ManagedOperationParameter(name="fromId", description="Test from identifier"),
      @ManagedOperationParameter(name="toId", description="Test to identifier"),
      @ManagedOperationParameter(name="message", description="Test chat message")      
   })
   public void sendChat(String chatId, String gameId, String fromId, String toId, String message) {
      ChessChat chat = new ChessChat(ChessChatStatus.UNREAD, chatId, gameId, fromId, toId, message, -1);
      ChessChatEvent event = new ChessChatEvent(chat);
      
      try {
         handler.handle(event);
      } catch(Exception e) {
         LOG.info("Could not send chat to " + toId, e);
      }
   }
   
}
