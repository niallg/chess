package com.zuooh.chess.server.notify;

import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;

import org.simpleframework.http.Request;
import org.simpleframework.http.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedOperation;
import org.springframework.jmx.export.annotation.ManagedOperationParameter;
import org.springframework.jmx.export.annotation.ManagedOperationParameters;
import org.springframework.jmx.export.annotation.ManagedResource;

import com.zuooh.common.task.TaskScheduler;
import com.zuooh.http.resource.Resource;

@ManagedResource(description="Handles all notification requests")
public class ChessNotificationResource implements Resource {   

   public static final Logger LOG = LoggerFactory.getLogger(ChessNotificationResource.class);
   
   private final ChessNotificationSearcher searcher;
   private final ChessNotificationReader reader;
   private final TaskScheduler scheduler;
   private final Executor executor; // game thread
   private final AtomicBoolean debug;
   
   public ChessNotificationResource(ChessNotificationSearcher searcher, TaskScheduler scheduler, Executor executor) {
      this(searcher, scheduler, executor, true);
   }
   
   public ChessNotificationResource(ChessNotificationSearcher searcher, TaskScheduler scheduler, Executor executor, boolean debug) {
      this.reader = new ChessNotificationReader();
      this.debug = new AtomicBoolean(debug);
      this.searcher = searcher;
      this.scheduler = scheduler;
      this.executor = executor; 
   }
   
   @ManagedAttribute(description="Determine if debug is enabled")
   public boolean isDebugEnabled() {
      return debug.get();
   }   
   
   @ManagedOperation(description="Enable or disable debug")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="enabled", description="Enable or disable debug")
   })
   public void setDebugEnabled(boolean enabled) {
      debug.set(enabled);
   }   

   @Override
   public void handle(Request request, Response response) throws Throwable {
      boolean verbose = debug.get();
      ChessNotificationQuery query = reader.readNotification(request);
      ChessNotificationListener listener = new ChessNotificationResponder(query, response, scheduler, verbose); // async response off the game thread
      ChessNotificationTask task = new ChessNotificationTask(searcher, query, listener);
      
      if(verbose) {              
         String header = request.toString();
         String content = request.getContent();
         
         LOG.info("Notification request=[" + header + content + "]");
      }
      executor.execute(task); // execute in the game thread
   }
}
