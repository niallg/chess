package com.zuooh.chess.server.game.account;

import java.util.Collections;
import java.util.List;

public class ChessAccountSearcher {

   private final ChessAccountPresenceComparator presenceComparator;
   private final ChessAccountRankComparator rankComparator;
   private final ChessAccountDatabase database;

   public ChessAccountSearcher(ChessAccountDatabase database) {
      this.presenceComparator = new ChessAccountPresenceComparator();
      this.rankComparator = new ChessAccountRankComparator();
      this.database = database;
   }

   public List<ChessAccount> findMostRecentlyActiveAccounts() throws Exception {
      return findMostRecentlyActiveAccounts("");
   }

   public List<ChessAccount> findMostRecentlyActiveAccounts(String term) throws Exception {
      List<ChessAccount> matchedAccounts = database.listRecentlyActiveAccounts(term);

      if (!matchedAccounts.isEmpty()) {
         Collections.sort(matchedAccounts, presenceComparator);
      }
      return matchedAccounts;
   }

   public List<ChessAccount> findHighestRatedAccounts() throws Exception {
      List<ChessAccount> matchedAccounts = database.listHighestRatedAccounts();

      if (!matchedAccounts.isEmpty()) {
         Collections.sort(matchedAccounts, rankComparator);
      }
      return matchedAccounts;
   }

   public List<ChessAccount> findHighestRatedAccounts(String term) throws Exception {
      List<ChessAccount> matchedAccounts = database.listHighestRatedAccounts(term);

      if (!matchedAccounts.isEmpty()) {
         Collections.sort(matchedAccounts, rankComparator);
      }
      return matchedAccounts;
   }
}
