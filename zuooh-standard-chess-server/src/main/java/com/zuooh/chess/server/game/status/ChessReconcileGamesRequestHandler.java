package com.zuooh.chess.server.game.status;

import java.util.List;

import com.zuooh.chess.client.event.request.ChessReconcileGamesEvent;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.server.game.ChessServerController;
import com.zuooh.chess.server.game.ChessServerPublisher;
import com.zuooh.message.client.RequestHandler;

public class ChessReconcileGamesRequestHandler implements RequestHandler<ChessReconcileGamesEvent> {
   
   private final ChessServerController mainController;
   private final ChessServerPublisher serverPublisher;
   
   public ChessReconcileGamesRequestHandler(ChessServerController mainController, ChessServerPublisher serverPublisher) throws Exception {
      this.serverPublisher = serverPublisher;
      this.mainController = mainController;   
   }
   
   @Override
   public ChessListOfGamesEvent execute(ChessReconcileGamesEvent event) throws Exception {   
      String userId = event.getUserId();
      List<ChessGame> chessGames = mainController.statusUpdate(userId);
      
      if(!chessGames.isEmpty()) {
         ChessListOfGamesEvent listEvent = new ChessListOfGamesEvent(chessGames);
         serverPublisher.publishNotification(listEvent, userId); // if they are online tell them straight away
         return listEvent;
      }
      return null;
   }
}
