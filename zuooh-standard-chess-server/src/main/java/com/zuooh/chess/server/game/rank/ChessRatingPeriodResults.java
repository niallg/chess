package com.zuooh.chess.server.game.rank;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This class holds the results accumulated over a rating period.
 * 
 * @author Jeremy Gooch
 */
public class ChessRatingPeriodResults {

   private List<ChessResult> results = new ArrayList<ChessResult>();
   private Set<ChessRating> participants = new HashSet<ChessRating>();

   /**
    * Create an empty resultset.
    */
   public ChessRatingPeriodResults() {
   }

   /**
    * Constructor that allows you to initialise the list of participants.
    * 
    * @param participants
    *           (Set of ChessRating objects)
    */
   public ChessRatingPeriodResults(Set<ChessRating> participants) {
      this.participants = participants;
   }

   /**
    * Add a result to the set.
    * 
    * @param winner
    * @param loser
    */
   public void addResult(ChessRating winner, ChessRating loser) {
      ChessResult result = new ChessResult(winner, loser);

      results.add(result);
   }

   /**
    * Record a draw between two players and add to the set.
    * 
    * @param player1
    * @param player2
    */
   public void addDraw(ChessRating player1, ChessRating player2) {
      ChessResult result = new ChessResult(player1, player2, true);

      results.add(result);
   }

   /**
    * Get a list of the results for a given player.
    * 
    * @param player
    * @return List of results
    */
   public List<ChessResult> getResults(ChessRating player) {
      List<ChessResult> filteredResults = new ArrayList<ChessResult>();

      for (ChessResult result : results) {
         if (result.participated(player)) {
            filteredResults.add(result);
         }
      }

      return filteredResults;
   }

   /**
    * Get all the participants whose results are being tracked.
    * 
    * @return set of all participants covered by the resultset.
    */
   public Set<ChessRating> getParticipants() {
      // Run through the results and make sure all players have been pushed into
      // the participants set.
      for (ChessResult result : results) {
         participants.add(result.getWinner());
         participants.add(result.getLoser());
      }

      return participants;
   }

   /**
    * Add a participant to the rating period, e.g. so that their rating will
    * still be calculated even if they don't actually compete.
    * 
    * @param rating
    */
   public void addParticipants(ChessRating rating) {
      participants.add(rating);
   }

   /**
    * Clear the resultset.
    */
   public void clear() {
      results.clear();
   }
}
