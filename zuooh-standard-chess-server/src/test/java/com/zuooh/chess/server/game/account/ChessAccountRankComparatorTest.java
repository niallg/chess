package com.zuooh.chess.server.game.account;

import java.security.SecureRandom;
import java.util.TreeSet;

import junit.framework.TestCase;

public class ChessAccountRankComparatorTest extends TestCase {
   
   public void testAccountRankSorting() throws Exception {
      ChessAccountRankComparator bestFirst = new ChessAccountRankComparator(true);
      ChessAccountRankComparator worstFirst = new ChessAccountRankComparator(false);
      TreeSet<ChessAccount> bestFirstAccounts = new TreeSet<ChessAccount>(bestFirst);
      TreeSet<ChessAccount> worstFirstAccounts = new TreeSet<ChessAccount>(worstFirst);
      SecureRandom random = new SecureRandom();
      
      for(int i = 0; i < 1000; i++) {
         ChessAccount account = new ChessAccount(ChessAccountType.ACTIVE, "playerId" + i, "playerName" + i, "playerMail" +i, "password"+1);
         int value = random.nextInt(10000);
         
         if(value % 2 == 0) {
            value = 0;
         }
         account.setRank(value);
         bestFirstAccounts.add(account);
         worstFirstAccounts.add(account);
      }
      for(ChessAccount account : bestFirstAccounts) {
         System.err.println("BEST FIRST: " + account.getRank() + "->" + account.getKey());
      }
      for(ChessAccount account : worstFirstAccounts) {
         System.err.println("WORST FIRST: " + account.getRank() + "->" + account.getKey());
      }
   }

}
