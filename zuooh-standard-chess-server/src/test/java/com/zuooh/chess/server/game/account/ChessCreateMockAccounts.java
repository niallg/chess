package com.zuooh.chess.server.game.account;

import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

public class ChessCreateMockAccounts {
   public static void main(String[] a)throws Exception{
      Path p = Paths.get("C:\\Work\\development\\bitbucket\\chess\\zuooh-standard-chess-server\\database\\bots.txt");
      List<String> list = Files.readAllLines(p, Charset.forName("UTF-8"));
      Random r = new SecureRandom();
      Collections.shuffle(list);
      int size = list.size();
      int half = size / 2;
      Iterator<String> i = list.iterator();
      while(i.hasNext()){
         String suffix = ".community.bot.zuooh.com";
         if(half-- <= 0) {
            suffix = ".challenge.bot.zuooh.com";
         }
         String name = i.next().trim();
         String key = name.toLowerCase().replace(" ", "");
         String value = "    <row>\n" +
               "      <value column='key'>" +  key + suffix + "</value>\n" +
               "      <value column='type'>NORMAL</value>\n" +
               "      <value column='name'>" + name + "</value>\n" +
               "      <value column='password'>password12</value>\n" +
               "      <value column='dataStatus'>BEGINNER</value>\n" +
               "      <value column='dataSkill'>0</value>\n" +
               "      <value column='dataRank'>0</value>\n" +
               "      <value column='dataBlackGames'>0</value>\n" +
               "      <value column='dataWhiteGames'>0</value>\n" +
               "      <value column='dataLastSeenOnline'>0</value>\n" +
               "    </row>";
         System.out.println(value);
      }
   }
   
   
}
