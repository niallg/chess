package com.zuooh.chess;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.control.ChessBoardPlayerProvider;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;

public class ChessGamePlayerManager {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessGamePlayerManager.class);

   private final ChessBoardPlayerProvider playerProvider;
   private final ChessDatabase gameDatabase;

   public ChessGamePlayerManager(ChessDatabase gameDatabase, ChessBoardPlayerProvider playerProvider) {
      this.playerProvider = playerProvider;
      this.gameDatabase = gameDatabase;
   }   

   public ChessPlayer loadChessPlayer(String gameId) {
      try {
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
         ChessPlayer chessPlayer = playerProvider.createPlayer(gameId);
         
         if(boardGame.isOnlineGame()) {
            ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
            ChessSide myColor = boardGame.getUserSide();
            ChessMoveEngine moveEngine = chessPlayer.getMoveEngine();
            ChessHistoryItem historyItem = historyDatabase.loadLastHistoryItem(gameId, myColor);
            
            if(historyItem != null) {
               ChessMove lastChessMove = historyItem.getMove();
               ChessBoard chessBoard = historyDatabase.loadHistoryAsBoard(gameId);
               
               LOG.warn("Loaded existing game " + gameId + " with a last move of " + lastChessMove);
               
               moveEngine.makeMove(chessBoard, lastChessMove); // here we hook up for a subscription
            }
         }
         return chessPlayer;
      } catch(Exception e) {
         LOG.info("Could not load chess player for " + gameId, e);
      }
      return null;
   }
}
