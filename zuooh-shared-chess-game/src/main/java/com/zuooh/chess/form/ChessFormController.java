package com.zuooh.chess.form;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import com.zuooh.application.Dialog;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.ResourceState;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.bind.annotation.Required;
import com.zuooh.chess.client.event.ChessRequestEvent;
import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.client.event.operation.form.ChessFormCommand;
import com.zuooh.chess.client.event.operation.form.ChessFormAction;
import com.zuooh.chess.task.ChessTaskController;
import com.zuooh.message.client.ResponseStatus;

@Component
public class ChessFormController extends ChessTaskController {   
   
   @Required
   @Inject("formCommand")
   private ChessFormCommand<ChessRequestEvent, ChessResponseEvent> command;   
   
   @Required
   @Inject("application")
   private MobileApplication application;
   
   @Required
   @Inject("screen")
   private Screen screen;
   
   @Override
   public void onBeforeStop() throws Exception {
      command.reset();
   }     

   @Override
   public long onInterval() throws Exception {
      ResponseStatus status = command.getStatus();
      
      if(status != ResponseStatus.NONE && status != ResponseStatus.SENDING) {
         if(status == ResponseStatus.TIMEOUT) {
            showWarning("Error", "Unable to complete this action due to network delays!", null);                
         } else if(status == ResponseStatus.NOT_CONNECTED) {
            showWarning("Error", "Connection to internet is currently not available!", null);             
         } else if(status == ResponseStatus.ERROR) {
            showWarning("Error", "Error occured during this action, please try again!", null);            
         } else if(status == ResponseStatus.SUCCESS) {
            ChessResponseEvent event = command.getResponse();
            ChessFormAction action = command.getAction();
            String error = action.validateResponse(event);            
            
            if(error != null) {
               String redirectPage = action.requiresRedirect(event); // XXX this is really really bad!!
               showWarning("Error", error, redirectPage);
            } else {
               String page = action.executeResponse(event);            
               screen.showPage(page);            
            }
         }
         command.reset();   
      }
      return 1000;
   }
   
   @OnCall
   public void submitForm(String formName) throws Exception {
      ResourceState state = application.getResourceState(formName);
      Set<String> names = state.listAttributes();
      Map<String, String> values = new LinkedHashMap<String, String>();
      
      for(String name : names){
         Object value = state.getAttribute(name);
         String text = null;
         
         if(value != null) {
            text = String.valueOf(value);
         }
         values.put(name, text);
      }
      ChessFormAction action = command.getAction();
      String error = action.validateRequest(values);
      
      if(error != null) {
         showWarning("Error", error, null);
      } else {
         command.execute(values);
      }
   }
   
   protected void showWarning(String title, String message, String redirect) {
      Dialog dialog = screen.openDialog("warningDialog");
      
      dialog.setAttribute("warningTitle", title);
      dialog.setAttribute("warningMessage", message);
      
      if(redirect == null) {
         dialog.setAttribute("warningFunction", "dialog.hideDialog()");
      } else {
         dialog.setAttribute("warningFunction", "screen.showPage('" + redirect + "')");
      }
      dialog.setAttribute("warningButton", "Okay");
      dialog.showDialog();
   }   
}
