package com.zuooh.chess.learn;

import java.util.List;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.canvas.Card;
import com.zuooh.application.canvas.Deck;
import com.zuooh.application.canvas.Layer;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.Slide;
import com.zuooh.application.canvas.SlideShow;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.application.canvas.info.Bubble;
import com.zuooh.application.canvas.info.speech.SpeechBubbleCreator;
import com.zuooh.application.canvas.info.speech.SpeechBubbleDrawer;
import com.zuooh.application.canvas.info.speech.SpeechPointerSize;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardCellIterator;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessBoardStateModel;
import com.zuooh.chess.control.state.ChessBoardStateSet;
import com.zuooh.chess.control.state.ChessGameQuota;
import com.zuooh.chess.control.state.ChessUpgradeProduct;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.view.ChessBoardCellView;
import com.zuooh.chess.view.ChessBoardPanel;
import com.zuooh.style.Color;
import com.zuooh.style.Font;
import com.zuooh.style.Quality;
import com.zuooh.style.Rectangle;
import com.zuooh.style.Shadow;
import com.zuooh.style.Style;

@Component
public class ChessLearnGameController {

   private static final String[][] BOARD = {
    // A     B     C     D     E     F     G    H
   { "bR", "bK", "bB", "  ", "b#", "bB", "bK", "bR" },  // 8
   { "  ", "bP", "  ", "  ", "bQ", "  ", "bP", "bP" },  // 7
   { "bP", "  ", "  ", "bP", "  ", "  ", "  ", "  " },  // 6
   { "  ", "  ", "bP", "  ", "  ", "bP", "  ", "  " },  // 5
   { "  ", "  ", "  ", "  ", "  ", "wP", "  ", "  " },  // 4
   { "  ", "wQ", "wP", "  ", "  ", "  ", "  ", "  " },  // 3
   { "wP", "wP", "  ", "wP", "wB", "  ", "wP", "wP" },  // 2
   { "wR", "wK", "wB", "  ", "w#", "  ", "wK", "wR" }}; // 1

   private final SpeechBubbleCreator speechCreator;
   private final ChessUserSource userSource;
   private final ChessDatabase gameDatabase;
   private final Document document;
   private final SlideShow slideShow;
   private final Deck<Card> nextArrows;    
   private final Deck<Card> backArrows;     
   private final Window window;
   private final Screen screen;
   private final String themeColor;

   public ChessLearnGameController(
         @Inject("application") MobileApplication application,
         @Inject("document") Document document,        
         @Inject("userSource") ChessUserSource userSource,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("themeColor") String themeColor,
         @Inject("speechBubble") SpeechBubbleCreator speechCreator,
         @Inject("backArrows") Deck<Card> backArrows,
         @Inject("nextArrows") Deck<Card> nextArrows,         
         @Inject("slideShow") SlideShow slideShow) 
   {
      this.screen = application.getScreen();
      this.window = application.getWindow();
      this.userSource = userSource;
      this.speechCreator = speechCreator;
      this.gameDatabase = gameDatabase;
      this.nextArrows = nextArrows;
      this.backArrows = backArrows;
      this.themeColor = themeColor;
      this.document = document;
      this.slideShow = slideShow;
   }

   @OnCreate
   public void startController() throws Exception {
      //window.resetForeground();
      Slide slide = slideShow.start();
      Panel footerPanel = slide.getCanvas("footer");
      String name = slide.getName();
      int index = slideShow.getIndex(name);
      int count = slideShow.getCount();
            
      if(footerPanel != null) {    
         footerPanel.setText((index + 1) + " of " + count);
      }       
      drawNavigation(index, count);
      window.invalidate();
   }

   @OnCall
   public void nextSlide(Toggle toggle) throws Exception {
     // window.resetForeground();
      toggle.toggle();
      
      if(slideShow.isEnd()) {
         screen.showPage("home"); // SHOULD NEVER HAPPEN!!
      } else {
         Slide slide = slideShow.next();
         Panel footerPanel = slide.getCanvas("footer");
         String name = slide.getName();
         int index = slideShow.getIndex(name);
         int count = slideShow.getCount();
         
         if(footerPanel != null) {    
            footerPanel.setText((index + 1) + " of " + count);
         }     
         drawNavigation(index, count);         
         window.invalidate();
      }
   }

   @OnCall
   public void previousSlide(Toggle toggle) throws Exception {
      //window.resetForeground();
      toggle.toggle();
      
      if(slideShow.isStart()) {
         screen.showPage("home"); // SHOULD NEVER HAPPEN!!
      } else {
         Slide slide = slideShow.back();
         Panel footerPanel = slide.getCanvas("footer");
         String name = slide.getName();
         int index = slideShow.getIndex(name);
         int count = slideShow.getCount();
         
         if(footerPanel != null) {   
            footerPanel.setText((index + 1) + " of " + count);
         }           
         drawNavigation(index, count);         
         window.invalidate();
      }
   }
   
   private void drawNavigation(int index, int count) {
      if(count > 0) { 
         if(index + 1 >= count) {
            nextArrows.setTop("hide");         
         } else {
            nextArrows.setTop("enable");
         }
         if(index > 0) {
            backArrows.setTop("enable");
         } else {
            backArrows.setTop("hide");
         }
      } else {
         nextArrows.setTop("hide");
         backArrows.setTop("hide");      
      }
   }   

   @OnCall
   public void learnChat(Context context, Style style) throws Exception {
      Layer layer = context.getCanvas("boardLayer");

      if(layer != null) {
         ChessBoardPanel boardPanel = createBoardPanel(context, BOARD);
         ChessBoardCellView cellView = boardPanel.getKingCell(ChessSide.BLACK);
         Rectangle rectangle = cellView.getRectangle();
         float scale = document.getScale();
         float width = rectangle.floatWidth;
         float pad = (width / 2.5f) - (1f * scale);
         int lineThickness = Math.round(2 * scale);
         int rectPadding = Math.round(15 * scale); 
         int screenPadding = Math.round(1 * scale);
         int shadowDistance = Math.round(5 * scale);
         Rectangle centerOfSpeech = rectangle.createInnerRectangle(pad);
         Style scaledStyle = style.getScaled(scale);
         Font font = scaledStyle.getFont();
         Rectangle screenBounds = slideShow.getRectangle();
         Color shadowColor = new Color("#99000000"); 
         Shadow shadow = new Shadow(shadowColor, Quality.NORMAL, shadowDistance);                                 
         SpeechBubbleDrawer speechDrawer = speechCreator.createDrawer(SpeechPointerSize.SHORT, shadow, lineThickness, rectPadding, screenPadding);
         Bubble bubble = speechDrawer.drawBubble(screenBounds, "Great move! It looks like I am in real trouble here. I was not paying attention to your queen.", font, centerOfSpeech);

         layer.addCanvas("bubble", bubble);    
      }
   }

   @OnCall
   public void learnChatDismiss(Context context) throws Exception {
      createBoardPanel(context, BOARD);
   }

   @OnCall
   public void learnSwitchPerspective(Context context) throws Exception {
      ChessBoardPanel boardPanel = createBoardPanelFlip(context, BOARD);
      ChessBoard board = boardPanel.getChessBoard();
      ChessBoardPosition queenPosition = findPosition(board, ChessPieceType.QUEEN, ChessSide.WHITE);
      ChessBoardCellView boardCell = boardPanel.getCell(queenPosition);
      ChessPiece piece = boardCell.getChessPiece();
      List<ChessBoardPosition> boardPositions = piece.moveNormal(board);

      for(ChessBoardPosition boardPosition : boardPositions) {
         ChessBoardCellView thisCell = boardPanel.getCell(boardPosition);

         if(thisCell.isCellEmpty()) {
            thisCell.shadowPiece(piece);
         } else {
            thisCell.underAttackPiece();
         }
      }
      boardCell.selectedPiece();
   }

   @OnCall
   public void learnThink(Context context) throws Exception {
      ChessBoardPanel boardPanel = createBoardPanel(context, BOARD);
      ChessBoard board = boardPanel.getChessBoard();
      ChessBoardPosition queenPosition = findPosition(board, ChessPieceType.QUEEN, ChessSide.BLACK);
      ChessBoardCellView boardCell = boardPanel.getCell(queenPosition);
      ChessPiece piece = boardCell.getChessPiece();
      List<ChessBoardPosition> boardPositions = piece.moveNormal(board);

      for(ChessBoardPosition boardPosition : boardPositions) {
         ChessBoardCellView thisCell = boardPanel.getCell(boardPosition);

         if(thisCell.isCellEmpty()) {
            thisCell.shadowPiece(piece);
         } else {
            thisCell.underAttackPiece();
         }
      }
      boardCell.selectedPiece();
   }

   @OnCall
   public void learnBoardHighlight(Context context) throws Exception {
      ChessBoardPanel boardPanel = createBoardPanel(context, BOARD);
      ChessBoard board = boardPanel.getChessBoard();
      ChessBoardPosition queenPosition = findPosition(board, ChessPieceType.QUEEN, ChessSide.WHITE);
      ChessBoardCellView boardCell = boardPanel.getCell(queenPosition);
      ChessPiece piece = boardCell.getChessPiece();
      List<ChessBoardPosition> boardPositions = piece.moveNormal(board);

      for(ChessBoardPosition boardPosition : boardPositions) {
         ChessBoardCellView thisCell = boardPanel.getCell(boardPosition);

         if(thisCell.isCellEmpty()) {
            thisCell.shadowPiece(piece);
         } else {
            thisCell.underAttackPiece();
         }
      }
      boardCell.selectedPiece();
   }

   @OnCall
   public void learnFinished(Context board) throws Exception {
      createBoardPanel(board);
   }

   private ChessBoardPosition findPosition(ChessBoard board, ChessPieceType type, ChessSide side) {
      ChessBoardCellIterator boardCells = board.getBoardCells(side);
      
      while(boardCells.hasNext()) {            
         ChessBoardCell boardCell = boardCells.next();
         ChessPiece chessPiece = boardCell.getPiece();
         ChessPieceType currentType = chessPiece.getPieceType();

         if(currentType == type) {
            return board.getPosition(chessPiece);
         }         
      }
      return null;
   }

   private ChessBoardPanel createBoardPanelFlip(Context board, String[][] chessSet) throws Exception {
      String[][] flipSet = new String[chessSet.length][];

      for(int i = 0; i < flipSet.length; i++) {
         int flipIndex = chessSet.length - (i + 1);
         String[] boardRow = new String[chessSet[flipIndex].length];

         for(int j = 0; j < boardRow.length; j++) {
            boardRow[j] = chessSet[flipIndex][j];
         }
         flipSet[i] = boardRow;
      }
      return createBoardPanel(board, flipSet);
   }
   
   private ChessBoardPanel createBoardPanel(Context board) throws Exception {
      ChessGameQuota quota = new ChessGameQuota(0, 0, 0);
      ChessUpgradeProduct product = new ChessUpgradeProduct(quota, quota, null);      
      ChessBoardContext boardContext = new ChessBoardContext(userSource, product, "learnGame", themeColor);
      ChessBoardStateModel boardModel = new ChessBoardStateModel(boardContext, gameDatabase);
      ChessBoardStateSet previewStateSet = new ChessBoardStateSet(boardModel, document);
      
      return new ChessBoardPanel(previewStateSet, null, board);
   }
   
   private ChessBoardPanel createBoardPanel(Context board, String[][] chessSet) throws Exception {
      ChessPieceSet pieceSet = new ChessTextPieceSet(chessSet);
      ChessBoard chessBoard = new ChessPieceSetBoard(pieceSet);
      ChessGameQuota quota = new ChessGameQuota(0, 0, 0);
      ChessUpgradeProduct product = new ChessUpgradeProduct(quota, quota, null);      
      ChessBoardContext boardContext = new ChessBoardContext(userSource, product, "learnGame", themeColor);
      ChessBoardStateModel boardModel = new ChessBoardStateModel(boardContext, gameDatabase, chessBoard);
      ChessBoardStateSet previewStateSet = new ChessBoardStateSet(boardModel, document);
      
      return new ChessBoardPanel(previewStateSet, null, board);
   }   
}
