package com.zuooh.chess.learn;

import java.util.Collection;
import java.util.List;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.canvas.Card;
import com.zuooh.application.canvas.Deck;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.Slide;
import com.zuooh.application.canvas.SlideShow;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCellIterator;
import com.zuooh.chess.board.ChessBoardModel;
import com.zuooh.chess.board.ChessBoardModelCell;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessBoardStateModel;
import com.zuooh.chess.control.state.ChessBoardStateSet;
import com.zuooh.chess.control.state.ChessGameQuota;
import com.zuooh.chess.control.state.ChessUpgradeProduct;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.view.ChessBoardCellView;
import com.zuooh.chess.view.ChessBoardPanel;

@Component
public class ChessLearnRulesController {
   
   private static final String[][] BOARD = {
   // A     B     C     D     E     F     G    H      
  { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },//8
  { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },//7
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//6
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//5
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//4
  { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },//3
  { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },//2
  { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};//1

   private final ChessUserSource userSource;
   private final ChessDatabase gameDatabase;  
   private final String themeColor;
   private final SlideShow slideShow;
   private final Deck<Card> nextArrows;    
   private final Deck<Card> backArrows;     
   private final Document document;
   private final Window window;
   private final Screen screen;

   public ChessLearnRulesController(
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("userSource") ChessUserSource userSource,
         @Inject("themeColor") String themeColor,
         @Inject("application") MobileApplication application,
         @Inject("document") Document document,
         @Inject("backArrows") Deck<Card> backArrows,
         @Inject("nextArrows") Deck<Card> nextArrows,            
         @Inject("slideShow") SlideShow slideShow)
   {
      this.screen = application.getScreen();
      this.window = application.getWindow();
      this.userSource = userSource;
      this.gameDatabase = gameDatabase;
      this.themeColor = themeColor;
      this.backArrows = backArrows;
      this.nextArrows = nextArrows;
      this.document = document;
      this.slideShow = slideShow;
   }

   @OnCreate
   public void startController() throws Exception {
      Slide slide = slideShow.start();      
      Panel footerPanel = slide.getCanvas("footer");
      String name = slide.getName();
      int index = slideShow.getIndex(name);
      int count = slideShow.getCount();      
      
      if(footerPanel != null) {    
         footerPanel.setText((index + 1) + " of " + count);
      } 
      drawNavigation(index, count);      
      window.invalidate();
   }

   @OnCall
   public void nextSlide(Toggle toggle) throws Exception {
      toggle.toggle();
      
      if(slideShow.isEnd()) {
         screen.showPage("home");
      } else {
         Slide slide = slideShow.next();
         Panel footerPanel = slide.getCanvas("footer");
         String name = slide.getName();
         int index = slideShow.getIndex(name);
         int count = slideShow.getCount();                  
         
         if(footerPanel != null) {    
            footerPanel.setText((index + 1) + " of " + count);
         }      
         drawNavigation(index, count);         
         window.invalidate();
      }
   }

   @OnCall
   public void previousSlide(Toggle toggle) throws Exception {
      toggle.toggle();
      
      if(slideShow.isStart()) {
         screen.showPage("home");
      } else {
         Slide slide = slideShow.back();
         Panel footerPanel = slide.getCanvas("footer");
         String name = slide.getName();
         int index = slideShow.getIndex(name);
         int count = slideShow.getCount();                  
         
         if(footerPanel != null) {    
            footerPanel.setText((index + 1) + " of " + count);
         }    
         drawNavigation(index, count);
         window.invalidate();
      }
   }   
   
   private void drawNavigation(int index, int count) {
      if(count > 0) { 
         if(index + 1 >= count) {
            nextArrows.setTop("hide");         
         } else {
            nextArrows.setTop("enable");
         }
         if(index > 0) {
            backArrows.setTop("enable");
         } else {
            backArrows.setTop("hide");
         }
      } else {
         nextArrows.setTop("hide");
         backArrows.setTop("hide");      
      }
   }      

   @OnCall
   public void learnAboutStart(Context board) throws Exception {
      createBoardPanel(board);
   }

   @OnCall
   public void learnAboutWhite(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();     
      ChessBoardModelCell boardCellFrom = boardModel.getBoardCell(ChessBoardPosition.C2);
      ChessBoardModelCell boardCellTo = boardModel.getBoardCell(ChessBoardPosition.C3);
      ChessPiece piece = boardCellFrom.takePiece();

      boardCellTo.placePiece(piece);      
      createBoardPanel(board, boardModel);
   }

   @OnCall
   public void learnAboutBlack(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardModelCell boardCellWhiteFrom = boardModel.getBoardCell(ChessBoardPosition.C2);
      ChessBoardModelCell boardCellWhiteTo = boardModel.getBoardCell(ChessBoardPosition.C3);
      ChessPiece whitePiece = boardCellWhiteFrom.takePiece();

      boardCellWhiteTo.placePiece(whitePiece);

      ChessBoardModelCell boardCellBlackFrom = boardModel.getBoardCell(ChessBoardPosition.E7);
      ChessBoardModelCell boardCellBlackTo = boardModel.getBoardCell(ChessBoardPosition.E6);
      ChessPiece blackPiece = boardCellBlackFrom.takePiece();

      boardCellBlackTo.placePiece(blackPiece);
      createBoardPanel(board, boardModel);
   }

   @OnCall
   public void learnAboutTurn(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardModelCell boardCellWhiteFrom = boardModel.getBoardCell(ChessBoardPosition.C2);
      ChessBoardModelCell boardCellWhiteTo = boardModel.getBoardCell(ChessBoardPosition.C3);
      ChessPiece whitePiece = boardCellWhiteFrom.takePiece();

      boardCellWhiteTo.placePiece(whitePiece);

      ChessBoardModelCell boardCellBlackFrom = boardModel.getBoardCell(ChessBoardPosition.E7);
      ChessBoardModelCell boardCellBlackTo = boardModel.getBoardCell(ChessBoardPosition.E6);
      ChessPiece blackPiece = boardCellBlackFrom.takePiece();

      boardCellBlackTo.placePiece(blackPiece);

      ChessBoardModelCell boardCellSecondWhiteFrom = boardModel.getBoardCell(ChessBoardPosition.F2);
      ChessBoardModelCell boardCellSecondWhiteTo = boardModel.getBoardCell(ChessBoardPosition.F4);
      ChessPiece secondWhitePiece = boardCellSecondWhiteFrom.takePiece();

      boardCellSecondWhiteTo.placePiece(secondWhitePiece);
      createBoardPanel(board, boardModel);
   }

   @OnCall
   public void learnAboutPawn(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell c2 = boardModel.getBoardCell(ChessBoardPosition.C2);
      ChessPiece pawnC2 = c2.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      c2.placePiece(pawnC2);
      
      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoardCellView boardCellC3 = boardPanel.getCell(ChessBoardPosition.C3);
      
      boardCellC3.shadowPiece(pawnC2);
   }

   @OnCall
   public void learnAboutPawnFirstMove(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell c2 = boardModel.getBoardCell(ChessBoardPosition.C2);
      ChessPiece pawnC2 = c2.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      c2.placePiece(pawnC2);
      
      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoardCellView boardCellC3 = boardPanel.getCell(ChessBoardPosition.C3);
      ChessBoardCellView boardCellC4 = boardPanel.getCell(ChessBoardPosition.C4);
      
      boardCellC3.shadowPiece(pawnC2);
      boardCellC4.shadowPiece(pawnC2);
   }

   @OnCall
   public void learnAboutPawnAttack(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell c2 = boardModel.getBoardCell(ChessBoardPosition.C2);
      ChessBoardModelCell d2 = boardModel.getBoardCell(ChessBoardPosition.D2);
      ChessBoardModelCell d3 = boardModel.getBoardCell(ChessBoardPosition.D3);
      ChessPiece pawnC2 = c2.takePiece();
      ChessPiece pawnD2 = d2.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      c2.placePiece(pawnC2);
      d3.placePiece(pawnD2);
      
      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoardCellView boardCellD3 = boardPanel.getCell(ChessBoardPosition.D3);
      
      boardCellD3.underAttackPiece();      
   }

   @OnCall
   public void learnAboutBishop(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell c1 = boardModel.getBoardCell(ChessBoardPosition.C1);
      ChessBoardModelCell d4 = boardModel.getBoardCell(ChessBoardPosition.D4);
      ChessPiece bishopC1 = c1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      d4.placePiece(bishopC1);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> positions = bishopC1.moveNormal(chessBoard);

      for(ChessBoardPosition position : positions) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(bishopC1);
      }
   }

   @OnCall
   public void learnAboutKnight(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell b1 = boardModel.getBoardCell(ChessBoardPosition.B1);
      ChessBoardModelCell d5 = boardModel.getBoardCell(ChessBoardPosition.D5);
      ChessPiece knightB1 = b1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      d5.placePiece(knightB1);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> positions = knightB1.moveNormal(chessBoard);

      for(ChessBoardPosition position : positions) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(knightB1);
      }
   }

   @OnCall
   public void learnAboutRook(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell b1 = boardModel.getBoardCell(ChessBoardPosition.A1);
      ChessBoardModelCell d4 = boardModel.getBoardCell(ChessBoardPosition.D4);
      ChessPiece rookA1 = b1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      d4.placePiece(rookA1);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> positions = rookA1.moveNormal(chessBoard);

      for(ChessBoardPosition position : positions) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(rookA1);
      }
   }

   @OnCall
   public void learnAboutQueen(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell d1 = boardModel.getBoardCell(ChessBoardPosition.D1);
      ChessBoardModelCell d4 = boardModel.getBoardCell(ChessBoardPosition.D4);
      ChessPiece queenD1 = d1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      d4.placePiece(queenD1);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> positions = queenD1.moveNormal(chessBoard);

      for(ChessBoardPosition position : positions) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(queenD1);
      }
   }

   @OnCall
   public void learnAboutKing(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell e1 = boardModel.getBoardCell(ChessBoardPosition.E1);
      ChessBoardModelCell d4 = boardModel.getBoardCell(ChessBoardPosition.D4);
      ChessPiece kingE1 = e1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      d4.placePiece(kingE1);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> positions = kingE1.moveNormal(chessBoard);

      for(ChessBoardPosition position : positions) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(kingE1);
      }
   }

   @OnCall
   public void learnAboutAttack(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell d1 = boardModel.getBoardCell(ChessBoardPosition.D1);
      ChessBoardModelCell d7 = boardModel.getBoardCell(ChessBoardPosition.D7);
      ChessBoardModelCell c8 = boardModel.getBoardCell(ChessBoardPosition.C8);
      ChessBoardModelCell f6 = boardModel.getBoardCell(ChessBoardPosition.F6);
      ChessBoardModelCell d4 = boardModel.getBoardCell(ChessBoardPosition.D4);
      ChessPiece pawnD7 = d7.takePiece();
      ChessPiece bishopC8 = c8.takePiece();
      ChessPiece queenD1 = d1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      d4.placePiece(queenD1);
      f6.placePiece(bishopC8);
      d7.placePiece(pawnD7);
      createBoardPanel(board, boardModel);
   }

   @OnCall
   public void learnAboutAttackMove(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell d1 = boardModel.getBoardCell(ChessBoardPosition.D1);
      ChessBoardModelCell d7 = boardModel.getBoardCell(ChessBoardPosition.D7);
      ChessBoardModelCell c8 = boardModel.getBoardCell(ChessBoardPosition.C8);
      ChessBoardModelCell f6 = boardModel.getBoardCell(ChessBoardPosition.F6);
      ChessBoardModelCell d4 = boardModel.getBoardCell(ChessBoardPosition.D4);
      ChessPiece pawnD7 = d7.takePiece();
      ChessPiece bishopC8 = c8.takePiece();
      ChessPiece queenD1 = d1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      d4.placePiece(queenD1);
      f6.placePiece(bishopC8);
      d7.placePiece(pawnD7);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> normalPositions = queenD1.moveNormal(chessBoard);

      for(ChessBoardPosition position : normalPositions) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(queenD1);
      }
      List<ChessBoardPosition> queenAttacks = queenD1.moveCapture(chessBoard);

      for(ChessBoardPosition position : queenAttacks) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.underAttackPiece();
      }
   }

   @OnCall
   public void learnAboutAttackTake(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell d1 = boardModel.getBoardCell(ChessBoardPosition.D1);
      ChessBoardModelCell d7 = boardModel.getBoardCell(ChessBoardPosition.D7);
      ChessBoardModelCell f6 = boardModel.getBoardCell(ChessBoardPosition.F6);
      ChessPiece pawnD7 = d7.takePiece();
      ChessPiece queenD1 = d1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      f6.placePiece(queenD1);
      d7.placePiece(pawnD7);
      createBoardPanel(board, boardModel);
   }

   @OnCall
   public void learnAboutCheck(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell d1 = boardModel.getBoardCell(ChessBoardPosition.D1);
      ChessBoardModelCell e8 = boardModel.getBoardCell(ChessBoardPosition.E8);
      ChessBoardModelCell e5 = boardModel.getBoardCell(ChessBoardPosition.E5);
      ChessPiece kingE1 = e8.takePiece();
      ChessPiece queenD1 = d1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      e5.placePiece(queenD1);
      e8.placePiece(kingE1);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> queenMoves = queenD1.moveNormal(chessBoard);

      for(ChessBoardPosition position : queenMoves) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(queenD1);
      }
      List<ChessBoardPosition> queenAttacks = queenD1.moveCapture(chessBoard);

      for(ChessBoardPosition position : queenAttacks) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.underAttackPiece();
      }
   }

   @OnCall
   public void learnAboutMoveOutOfCheck(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell d1 = boardModel.getBoardCell(ChessBoardPosition.D1);
      ChessBoardModelCell d8 = boardModel.getBoardCell(ChessBoardPosition.D8);
      ChessBoardModelCell e8 = boardModel.getBoardCell(ChessBoardPosition.E8);
      ChessBoardModelCell e5 = boardModel.getBoardCell(ChessBoardPosition.E5);
      ChessPiece kingE1 = e8.takePiece();
      ChessPiece queenD1 = d1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      e5.placePiece(queenD1);
      d8.placePiece(kingE1);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> normalPositions = queenD1.moveNormal(chessBoard);

      for(ChessBoardPosition position : normalPositions) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(queenD1);
      }
   }

   @OnCall
   public void learnAboutCheckMate(Context board) throws Exception {
      ChessBoardModel boardModel = createBoardModel();
      ChessBoardCellIterator boardCells = boardModel.getBoardCells(null);
      ChessBoardModelCell d1 = boardModel.getBoardCell(ChessBoardPosition.D1);
      ChessBoardModelCell b1 = boardModel.getBoardCell(ChessBoardPosition.B1);
      ChessBoardModelCell a8 = boardModel.getBoardCell(ChessBoardPosition.A8);
      ChessBoardModelCell e8 = boardModel.getBoardCell(ChessBoardPosition.E8);
      ChessBoardModelCell b7 = boardModel.getBoardCell(ChessBoardPosition.B7);
      ChessBoardModelCell d6 = boardModel.getBoardCell(ChessBoardPosition.D6);
      ChessPiece kingE1 = e8.takePiece();
      ChessPiece queenD1 = d1.takePiece();
      ChessPiece knightB1 = b1.takePiece();

      while(boardCells.hasNext()) {
         ChessBoardModelCell boardCell = (ChessBoardModelCell)boardCells.next();
         
         if(!boardCell.isCellEmpty()) {
            boardCell.takePiece();
         }         
      }
      d6.placePiece(knightB1);
      b7.placePiece(queenD1);
      a8.placePiece(kingE1);

      ChessBoardPanel boardPanel = createBoardPanel(board, boardModel);
      ChessBoard chessBoard = boardPanel.getChessBoard();
      List<ChessBoardPosition> queenMoves = queenD1.moveNormal(chessBoard);

      for(ChessBoardPosition position : queenMoves) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.shadowPiece(queenD1);
      }
      List<ChessBoardPosition> queenAttacks = queenD1.moveCapture(chessBoard);

      for(ChessBoardPosition position : queenAttacks) {
         ChessBoardCellView boardCell = boardPanel.getCell(position);
         boardCell.underAttackPiece();
      }
   }
   
   public ChessBoardModel createBoardModel() throws Exception {
      ChessTextPieceSet pieceSet = new ChessTextPieceSet(BOARD);
      return new ChessPieceSetBoard(pieceSet);
   }
  
   private ChessBoardPanel createBoardPanel(Context board) throws Exception {
      ChessGameQuota quota = new ChessGameQuota(0, 0, 0);
      ChessUpgradeProduct product = new ChessUpgradeProduct(quota, quota, null);       
      ChessBoardContext boardContext = new ChessBoardContext(userSource, product, "learnRules", themeColor);
      ChessBoardStateModel boardModel = new ChessBoardStateModel(boardContext, gameDatabase);
      ChessBoardStateSet previewStateSet = new ChessBoardStateSet(boardModel, document);
      
      return new ChessBoardPanel(previewStateSet, null, board);
   }
   
   private ChessBoardPanel createBoardPanel(Context board, ChessBoard chessBoard) throws Exception {
      ChessGameQuota quota = new ChessGameQuota(0, 0, 0);
      ChessUpgradeProduct product = new ChessUpgradeProduct(quota, quota, null);       
      ChessBoardContext boardContext = new ChessBoardContext(userSource, product, "learnRules", themeColor);
      ChessBoardStateModel boardModel = new ChessBoardStateModel(boardContext, gameDatabase, chessBoard);
      ChessBoardStateSet previewStateSet = new ChessBoardStateSet(boardModel, document);
      
      return new ChessBoardPanel(previewStateSet, null, board);
   }   
}
