package com.zuooh.chess.score;

import java.util.LinkedHashMap;

import com.zuooh.application.canvas.Deck;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessCellStatus;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.theme.ChessThemePiece;
import com.zuooh.style.image.Image;

@Component
public class ChessScorePanel {

   private final LinkedHashMap<ChessPieceKey, ChessThemePiece> blackList;
   private final LinkedHashMap<ChessPieceKey, ChessThemePiece> whiteList;
   private final Context blackScoreSmall;
   private final Context whiteScoreSmall;
   private final Context blackScore;
   private final Context whiteScore;
   private final Deck scoreDeck;

   public ChessScorePanel(
         @Inject("blackScore") Context blackScore,
         @Inject("whiteScore") Context whiteScore)
   {
      this(blackScore, whiteScore, blackScore, whiteScore, null);
   }

   public ChessScorePanel(
         @Inject("blackScore") Context blackScore,
         @Inject("whiteScore") Context whiteScore,
         @Inject("blackScoreSmall") Context blackScoreSmall,
         @Inject("whiteScoreSmall") Context whiteScoreSmall,
         @Inject("scoreDeck") Deck scoreDeck)
   {
      this.blackList = new LinkedHashMap<ChessPieceKey, ChessThemePiece>();
      this.whiteList = new LinkedHashMap<ChessPieceKey, ChessThemePiece>();
      this.blackScoreSmall = blackScoreSmall;
      this.whiteScoreSmall = whiteScoreSmall;
      this.blackScore = blackScore;
      this.whiteScore = whiteScore;
      this.scoreDeck = scoreDeck;
   }

   public void pieceRecovered(ChessPieceKey pieceKey) {
      if(pieceKey.side == ChessSide.BLACK) {
         String key = "black" + blackList.size();
         Panel area = blackScore.getCanvas(key);
         Panel areaSmall = blackScoreSmall.getCanvas(key);

         if(blackList.remove(pieceKey) == null) {
            throw new IllegalStateException("Could not recover "+pieceKey);
         }
         area.setImage(null);
         areaSmall.setImage(null);
      } else {
         String key = "white" + whiteList.size();
         Panel area = whiteScore.getCanvas(key);
         Panel areaSmall = whiteScoreSmall.getCanvas(key);

         if(whiteList.remove(pieceKey) == null) {
            throw new IllegalStateException("Could not recover "+pieceKey);
         }
         area.setImage(null);
         areaSmall.setImage(null);
      }
      flipDeck();
   }

   public void pieceTaken(ChessPieceKey pieceKey, ChessThemePiece pieceView) {
      if(pieceView.pieceSide == ChessSide.BLACK) {
         String key = "black" + (blackList.size() + 1);
         Panel area = blackScore.getCanvas(key);
         Panel areaSmall = blackScoreSmall.getCanvas(key);

         if(area == null) {
            throw new IllegalStateException("Could not find area for " + key);
         }
         Image image = pieceView.getImage(ChessCellStatus.TAKEN, null);

         area.setImage(image);
         areaSmall.setImage(image);
         blackList.put(pieceKey, pieceView);
      } else {
         String key = "white" + (whiteList.size() + 1);
         Panel area = whiteScore.getCanvas(key);
         Panel areaSmall = whiteScoreSmall.getCanvas(key);

         if(area == null) {
            throw new IllegalStateException("Could not find area for " + key);
         }
         Image image = pieceView.getImage(ChessCellStatus.TAKEN, null);

         area.setImage(image);
         areaSmall.setImage(image);
         whiteList.put(pieceKey, pieceView);
      }
      flipDeck();
   }

   private void flipDeck() {
      int blackSize = blackList.size();
      int whiteSize = whiteList.size();
      int maxSize = Math.max(whiteSize, blackSize);

      if(scoreDeck != null) {
         if(maxSize > 10) {
            scoreDeck.setTop("small");
         } else {
            scoreDeck.setTop("large");
         }
      }
   }
}
