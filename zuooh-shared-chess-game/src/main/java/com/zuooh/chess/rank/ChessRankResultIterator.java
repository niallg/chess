package com.zuooh.chess.rank;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import com.zuooh.chess.database.user.ChessUser;

public abstract class ChessRankResultIterator {
   
   private final List<ChessUser> cache;
   private final AtomicInteger position;
   private final AtomicInteger index;
   
   protected ChessRankResultIterator() {
      this.cache = new ArrayList<ChessUser>();
      this.position = new AtomicInteger(-1);
      this.index = new AtomicInteger(0);
   } 
   
   public synchronized ChessRankResult nextResult() {    
      int cacheSize = cache.size();
      int nextIndex = index.incrementAndGet();
      int nextPosition = position.incrementAndGet();
      
      if(nextIndex >= cacheSize) {
         int fetchSize = fetchBatchSize();
         List<ChessUser> range = nextRange(nextPosition, fetchSize);
         
         if(range.isEmpty()) {
            int lastIndex = index.decrementAndGet();
            int lastPosition = position.decrementAndGet();
            ChessUser result = cache.get(lastIndex);
            
            return new ChessRankResult(result, lastPosition + 1, true);            
         } else {
            cache.clear();
            cache.addAll(range);
            index.set(0);
         }
      }
      int currentIndex = index.get();
      int currentPosition = position.get();
      ChessUser result = cache.get(currentIndex);
      
      return new ChessRankResult(result, currentPosition + 1);
   } 
   
   public synchronized ChessRankResult previousResult() {  
      int startPosition = position.get();  
      int previousIndex = index.decrementAndGet();
      int previousPosition = position.decrementAndGet();
      
      if(previousIndex < 0) {
         if(previousPosition > 0) {
            int fetchSize = fetchBatchSize();            
            List<ChessUser> range = nextRange(startPosition - fetchSize, fetchSize);
            int size = range.size();
            
            if(size > 0) {
               cache.clear();
               cache.addAll(range);               
               index.set(size - 1);
            }
         } else {
            ChessUser user = currentUser();            
            int firstIndex = index.getAndSet(-1);
            int firstPosition = position.getAndSet(-1);
            
            return new ChessRankResult(user, 0);
         }     
      }
      int currentIndex = index.get();
      int currentPosition = position.get();
      ChessUser result = cache.get(currentIndex);
      
      return new ChessRankResult(result, currentPosition + 1);
   }   
   
   protected synchronized int fetchBatchSize() {
      return 100;
   }
   
   protected abstract ChessUser currentUser();
   protected abstract List<ChessUser> nextRange(int from, int size);

}
