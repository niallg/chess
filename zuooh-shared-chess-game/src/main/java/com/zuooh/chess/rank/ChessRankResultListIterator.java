package com.zuooh.chess.rank;

import java.util.Collections;
import java.util.List;

import com.zuooh.chess.database.user.ChessUser;

public class ChessRankResultListIterator extends ChessRankResultIterator  {

   private final List<ChessUser> users;
   private final ChessUser user;
   private final int fetchSize;

   public ChessRankResultListIterator(ChessUser profile, List<ChessUser> users) {
      this(profile, users, 100);
   }
   
   public ChessRankResultListIterator(ChessUser user, List<ChessUser> users, int fetchSize) {
      this.fetchSize = fetchSize;
      this.users = users;
      this.user = user;
   }
   
   @Override
   protected int fetchBatchSize() {
      return fetchSize;
   }
   
   @Override
   protected ChessUser currentUser() {
      return user;
   }

   @Override
   protected List<ChessUser> nextRange(int from, int size) {
      int count = users.size();
      int start = Math.max(0, from);
      int stop = Math.min(count, size + start);
      
      if(start < stop) {
         return users.subList(start, stop);
      }
      return Collections.emptyList();
   }

}
