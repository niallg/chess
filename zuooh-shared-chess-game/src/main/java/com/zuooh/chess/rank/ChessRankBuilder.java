package com.zuooh.chess.rank;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserStatus;
import com.zuooh.chess.timer.ChessTimeDrawer;
import com.zuooh.style.Style;

@Component
public class ChessRankBuilder {

   private final Document document;
   private final Screen screen;
   private final Style star;

   public ChessRankBuilder(
         @Inject("appliation") MobileApplication application,
         @Inject("document") Document document,
         @Inject("star") Style star)
   {
      this.screen = application.getScreen();
      this.document = document;
      this.star = star;
   }
   

   public void createProfile(ChessUser profile, Context context) {
      if(context != null) {
         Panel creationTime = context.getCanvas("lastSeenOnline");
         Panel playerScore = context.getCanvas("playerRank");
         Context skillContext = context.getContext("skillPanel");
         
         drawPlayerName(context, profile);
         
         if(skillContext != null) {
            drawSkill(skillContext, profile);
         }
         drawPlayerStatus(context, profile);
         drawPlayerPoints(context, profile);
         //drawPlayerHistory(context, profile);
         drawPlayerDescription(context, profile);
         drawPlayerIcon(context, profile);
         
         if(creationTime != null) {
            drawCreationTime(context, profile, creationTime);
         }
         if(playerScore != null) {
            drawPlayerScore(context, profile, playerScore);
         }
         context.addAttribute("player", profile);
      }
   }
   
   private void drawPlayerDescription(Context context, ChessUser profile) {
      Panel playerDescription = context.getCanvas("playerDescription");
      
      if(playerDescription != null) {
         playerDescription.setText("Champion Player");
      }
   }

   private void drawPlayerIcon(Context context, ChessUser profile) {
      Panel playerIcon = context.getCanvas("playerIcon");
      
      if(playerIcon != null) {
         
      }
   }
   
   private void drawCreationTime(Context context, ChessUser profile, Panel creationTime) {
      ChessUserData playerData = profile.getData();
      long timeStamp = playerData.getLastSeenOnline();
      long currentTime = System.currentTimeMillis();
      long timeSinceActive = currentTime - timeStamp;
      long oneDay = 1000 * 60 * 60 * 24;
      
      if(timeSinceActive >= oneDay) {
         creationTime.setText("Not active today");
      } else if(timeSinceActive > 1000) {
         String lastSeenTime = ChessTimeDrawer.convertToTextTime(timeSinceActive);
         String timeTrimmed = lastSeenTime.trim();

         creationTime.setText("Active " + timeTrimmed + " ago");
      } else {
         creationTime.setText("Currently active");
      }
   }

   private void drawPlayerScore(Context context, ChessUser profile, Panel playerRank) {
      ChessUserData playerData = profile.getData();
      double rank = playerData.getRank();
      long rounded = Math.round(rank);
      
      playerRank.setText("Rank " + rounded);
   }
   
   private void drawPlayerStatus(Context context, ChessUser profile) {
      ChessUserData playerData = profile.getData();
      ChessUserStatus profileStatus = playerData.getStatus();
      Panel logoImage = context.getCanvas("logoImage");
      
      if(logoImage != null) {
         Style style = context.getStyle(ChessUserStatus.EXPERIENCED_EXCELLENT.statusName);
         logoImage.setStyle(style);
      }
      // Panel playerPoints = context.getCanvas("playerPoints");
     //  int opponentPoints = profile.getPlayerPoints();

       //playerPoints.setText(String.valueOf(opponentPoints));
    }

   
   private void drawPlayerPoints(Context context, ChessUser profile) {
     // Panel playerPoints = context.getCanvas("playerPoints");
    //  int opponentPoints = profile.getPlayerPoints();

      //playerPoints.setText(String.valueOf(opponentPoints));
   }

   private void drawPlayerName(Context context,  ChessUser profile) {      
      Panel person = context.getCanvas("playerName");
      String playerId = profile.getKey();
      String playerName = profile.getName();
 
      if(playerName != null) {
         playerName = playerName.trim();
         person.setText(playerName);
      } else {
         person.setText(playerId);
      }
   }

   private void drawSkill(Context context,  ChessUser profile) {
      ChessUserData playerData = profile.getData();
      int opponentSkillLevel = playerData.getSkill();

      for(int i = 1; i <= opponentSkillLevel; i++) {
         Panel starPanel = context.getCanvas("star" + i);
         
         if(starPanel != null) {
            starPanel.setStyle(star);
         }
      }
   }
}
