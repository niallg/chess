package com.zuooh.chess.rank;

import com.zuooh.chess.database.user.ChessUser;

public class ChessRankResult {
   
   private final ChessUser profile;
   private final int position;
   private final boolean last;
   
   public ChessRankResult(ChessUser profile, int position) {
      this(profile, position, false);
   }
   
   public ChessRankResult(ChessUser profile, int position, boolean last) {
      this.profile = profile;
      this.position = position;
      this.last = last;
   }
   
   public boolean isFirst() {
      return position == 0;
   }   
   
   public boolean isLast() {
      return last;
   }
   
   public ChessUser getProfile(){
      return profile;
   }
   
   public int getPosition(){
      return position;
   }
}
