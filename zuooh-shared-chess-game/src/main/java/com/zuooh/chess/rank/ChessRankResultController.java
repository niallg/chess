package com.zuooh.chess.rank;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.canvas.Fade;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.client.event.operation.ChessRankQueryCommand;
import com.zuooh.chess.client.event.response.ChessRankResultEvent;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserComparator;
import com.zuooh.chess.task.ChessTaskController;

@Component
public class ChessRankResultController extends ChessTaskController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessRankResultController.class);   

   private ChessRankResultIterator resultIterator;
   private ChessRankQueryCommand rankCommand;
   private ChessGameManager gameManager;
   private Panel description;
   private Window window;
   private Screen screen;
   private Fade splash;
   
   public ChessRankResultController(
         @Inject("application") MobileApplication application,
         @Inject("rankCommand") ChessRankQueryCommand rankCommand,
         @Inject("gameManager") ChessGameManager gameManager,
         @Inject("description") Panel description,
         @Inject("splash") Fade splash)        
   {
      this.screen = application.getScreen();
      this.window = application.getWindow();
      this.rankCommand = rankCommand;
      this.gameManager = gameManager;
      this.description = description;
      this.splash = splash;
   }

   @OnStart
   public long onInterval() throws Exception {
      if(splash == null || !splash.isVisible()) {
         if(resultIterator == null) {
            resultIterator = createIterator();
         }               
         if(resultIterator != null) {
            ChessUser profile = resultIterator.currentUser(); // assumption you are first!!!
            
            if(description != null) {
               description.setText("Your ranking");
            }
            ChessRankDialogBuilder.showProfile(screen, profile, null, null, true, true, false, true);
         }     
         return -1;
      }
      return 200;
   }
   
   private ChessRankResultIterator createIterator() {      
      try {
         ChessRankResultEvent result = rankCommand.getRankResponse();
         List<ChessUser> results = result.getUsers();
         ChessUser currentProfile = gameManager.currentUser();               
         
         if(!results.isEmpty()) {
            List<ChessUser> profiles = new ArrayList<ChessUser>();
            ChessUserComparator comparator = new ChessUserComparator();
            
            for(ChessUser profile : results) {
               if(profile != null) {
                  profiles.add(profile);
               }
            }     
            Collections.sort(profiles, comparator);
      
            return new ChessRankResultListIterator(currentProfile, profiles);
         }               
      } catch(Exception e) {
         LOG.info("Error getting profile iterator", e);
      }
      return new ChessRankResultListIterator(null, Collections.EMPTY_LIST);
   }
   
   @OnCall
   public void nextSlide(Toggle toggle) throws Exception {
      if(resultIterator != null) {
         ChessRankResult next = resultIterator.nextResult();         
         ChessUser profile = next.getProfile();
         boolean first = next.isFirst();
         boolean last = next.isLast();        
         
         showProfile(profile, first, last);         
      }
      toggle.toggle();
      window.invalidate();            
   }

   @OnCall
   public void previousSlide(Toggle toggle) throws Exception {
      if(resultIterator != null) {
         ChessRankResult previous = resultIterator.previousResult();
         ChessUser profile = previous.getProfile();
         boolean first = previous.isFirst();
         boolean last = previous.isLast();        
         
         showProfile(profile, first, last);    
      }  
      toggle.toggle();
      window.invalidate();      
   }
   
   private void showProfile(ChessUser profile, boolean first, boolean last) {
      ChessUser current = resultIterator.currentUser();
      
      if(description != null) {
         String profileId = profile.getKey();
         String currentId = current.getKey();

         if(currentId.equals(profileId)) {            
            description.setText("Your ranking");
            ChessRankDialogBuilder.showProfile(screen, profile, null, null, true, true, false, true); 
         } else {
            description.setText("Player ranking");
            ChessRankDialogBuilder.showProfile(screen, profile, null, null, true, false, !first, !last);             
         }
      } else {
         ChessRankDialogBuilder.showProfile(screen, profile, null, null, true, false, !first, !last);
      }
   }


}
