package com.zuooh.chess.rank;

import java.util.Random;

import com.zuooh.application.Dialog;
import com.zuooh.application.Screen;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserFormatter;
import com.zuooh.chess.database.user.ChessUserStatus;
import com.zuooh.chess.database.user.ChessUserType;

public class ChessRankDialogBuilder {
   
   public static void showProfile(Screen screen, ChessUser profile, String color, String function, boolean backArrow) {
      showProfile(screen, profile, color, function, backArrow, false);         
   }
   
   public static void showProfile(Screen screen, ChessUser profile, String color, String function, boolean navigation, boolean me) {
      showProfile(screen, profile, color, function, navigation, me, false, false);
   }
   
   public static void showProfile(Screen screen, ChessUser profile, String color, String function, boolean navigation, boolean me, boolean backArrow, boolean nextArrow) {
      String playerName = ChessUserFormatter.formatUser(profile, me);
      long time = System.currentTimeMillis();
      Random random = new Random(System.identityHashCode(playerName + time));
      ChessUserData playerData = profile.getData();
      ChessUserStatus status = playerData.getStatus();
      ChessUserType playerType = profile.getType();
      int angle = random.nextInt(12) - 6;
      int rank = playerData.getRank();
      int skill = playerData.getSkill();
      Dialog dialog = screen.openDialog("rankDialog");
      dialog.hideDialog();

      if(playerType.isComputer()) {
         dialog.setAttribute("award", "Computer");
      } else {
         dialog.setAttribute("award", status.statusDescription);
      }
      dialog.setAttribute("user", playerName);
      
      if(playerType.isComputer()) {
         dialog.setAttribute("logo", "computerSkill" + skill);
      } else {
         dialog.setAttribute("logo", status.statusName);
      }
      dialog.setAttribute("color", "#35a5d4");
      
      if(playerType.isComputer() || rank <= 0) {
         dialog.setAttribute("rank", "No Rank");
      } else {
         dialog.setAttribute("rank", "Rank # " + rank);
      }
      dialog.setAttribute("angle", angle);
      dialog.setAttribute("navigation", navigation ? "visible" : "hidden");
      dialog.setAttribute("enableBackArrow", backArrow ? "visible" : "hidden");
      dialog.setAttribute("disableBackArrow",  "hidden");  
      dialog.setAttribute("enableNextArrow", nextArrow ? "visible" : "hidden");
      dialog.setAttribute("disableNextArrow", "hidden");        
      
      if(function!=null){
         dialog.setAttribute("function",function);
      }else {
         dialog.setAttribute("function","");
      }
      if(color !=null){
         dialog.setAttribute("background", color);
      }else{
         dialog.setAttribute("background", "#00000000");
      }
      
      for(int i = 1; i <= skill;i++) {
         dialog.setAttribute("star"+i, "star");
      }
      for(int i = skill+1; i <= 5;i++) {
         dialog.setAttribute("star"+i, "disabledStar");
      }
      dialog.showDialog();     
   }
}
