package com.zuooh.chess.rank;

import java.util.List;

import com.zuooh.application.Dialog;
import com.zuooh.application.Screen;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.bind.annotation.Required;
import com.zuooh.chess.client.event.operation.ChessRankQueryCommand;
import com.zuooh.chess.client.event.response.ChessRankResultEvent;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.task.ChessTaskController;
import com.zuooh.message.client.ResponseStatus;

@Component
public class ChessRankResultChecker extends ChessTaskController {

   @Required
   @Inject("rankCommand")
   private ChessRankQueryCommand rankCommand;
   
   @Required
   @Inject("screen")
   private Screen screen;
   
   @Override
   public void onBeforeStart() throws Exception {
      rankCommand.execute();
   }

   @Override
   public long onInterval() throws Exception {
      ResponseStatus status = rankCommand.getStatus();
      
      if(status != ResponseStatus.NONE && status != ResponseStatus.SENDING) {
         if(status == ResponseStatus.TIMEOUT) {
            showWarning("Error", "Unable to get player rankings due to network delays!");              
         } else if(status == ResponseStatus.NOT_CONNECTED) {
            showWarning("Error", "Connection to internet is currently not available!");               
         } else if(status == ResponseStatus.ERROR) {
            showWarning("Error", "Error occured downloading rankings!");           
         } else if(status == ResponseStatus.SUCCESS) {
            ChessRankResultEvent result = rankCommand.getRankResponse();
            List<ChessUser> results = result.getUsers();
            
            if(!results.isEmpty()) {
               screen.showPage("rankResults");
            } else {
               showWarning("Not Found", "Unable to determine rankings at this moment!");
            }
         }
         rankCommand.reset();   
      }
      return 1000;
   }     
   
   @Override
   public void onBeforeStop() {
      rankCommand.reset();
   }
   
   public void showWarning(String title, String message) {
      Dialog dialog = screen.openDialog("warningDialog");
      
      dialog.setAttribute("warningTitle", title);
      dialog.setAttribute("warningMessage", message);
      dialog.setAttribute("warningFunction", "screen.showPage('home')");
      dialog.setAttribute("warningButton", "Okay");
      dialog.showDialog();
   }
}
