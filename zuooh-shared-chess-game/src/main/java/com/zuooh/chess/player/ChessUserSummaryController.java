package com.zuooh.chess.player;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnBack;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnMenu;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.database.user.ChessUser;

@Component
public class ChessUserSummaryController {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessUserSummaryController.class);

   private final ChessUserSource userSource;
   private final ChessUserSummaryContext summaryContext;
   private final ChessUserSummaryBuilder summaryBuilder;
   private final Window window;
   private final Context context;
   private final Screen screen;

   public ChessUserSummaryController(
         @Inject("userSource") ChessUserSource userSource,          
         @Inject("summaryContext") ChessUserSummaryContext summaryContext,
         @Inject("summaryBuilder") ChessUserSummaryBuilder summaryBuilder,
         @Inject("appliation") MobileApplication application,         
         @Inject("playerProfile") Context context)
   {
      this.window = application.getWindow();
      this.screen = application.getScreen();
      this.summaryContext = summaryContext;
      this.summaryBuilder = summaryBuilder;
      this.userSource = userSource;
      this.context = context;
   }
   
   @OnStart
   public void populateForm() {
      ChessUser user = summaryContext.getUser();

      if(user == null) {
         user = userSource.currentUser(); // XXX bad!
      }
      if(context != null) {
         summaryBuilder.createProfile(user, context);
      }
      window.invalidate();
   }

   @OnMenu
   public void backToMenu() {
      screen.showPage("home");
   }
   
   @OnBack
   public void backToInput(){      
      screen.showPage("home");
   }
   
   @OnCall
   public void upgradeProfile(Toggle toggle) {
      toggle.toggle();
      screen.showPage("upgradeScreen");
   }

   @OnCall
   public void signOutProfile(Toggle toggle) {
      try {
         toggle.toggle();
         window.resetForeground();
         window.invalidate();
         userSource.resetUser();
         screen.showPage("accountSignIn");
      } catch(Exception e) {
         LOG.info("Could not register profile", e);
      }
   }   
   
}
