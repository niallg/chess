package com.zuooh.chess.player;

import java.util.concurrent.atomic.AtomicReference;

import com.zuooh.bind.annotation.Component;
import com.zuooh.chess.database.user.ChessUser;

@Component
public class ChessUserSummaryContext {
   
   private final AtomicReference<ChessUser> reference;

   public ChessUserSummaryContext(){
      this.reference = new AtomicReference<ChessUser>();
   }
   
   public ChessUser getUser(){
      return reference.get();
   }
   
   public void setUser(ChessUser user) {
      reference.set(user);
   }
}
