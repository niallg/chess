package com.zuooh.chess.menu;

import java.util.Set;

import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.canvas.Flow;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;

@Component
public class ChessActionBarController {

   private final Set<String> icons;
   private final Flow flow;

   public ChessActionBarController(
         @Inject("actionBarIcons") Set<String> icons,
         @Inject("actionBar") Flow flow)
   {
      this.icons = icons;
      this.flow = flow;
   }
   
   @OnStart
   public void showActionBar() {
      if(flow != null) {
         for(String icon : icons) {
            flow.setVisible("icon");
         }
      }
   }
}
