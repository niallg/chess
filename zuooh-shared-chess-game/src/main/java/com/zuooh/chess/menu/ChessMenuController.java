package com.zuooh.chess.menu;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.canvas.Flow;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserType;

@Component
public class ChessMenuController {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessMenuController.class);

   private final ChessUserSource userSource;
   private final Screen screen;
   private final Toggle toggle;
   private final Flow flow;

   public ChessMenuController(
         @Inject("application") MobileApplication application,        
         @Inject("gameManager") ChessGameManager gameManager,
         @Inject("userSource") ChessUserSource userSource,
         @Inject("closeMenu") Toggle toggle,
         @Inject("actionBar") Flow flow)
   {
      this.screen = application.getScreen();
      this.userSource = userSource;
      this.toggle = toggle;
      this.flow = flow;
   }
   
   @OnStart
   public void showActionBar() {
      if(flow != null) {
         //flow.setVisible("menuIcon");
        // flow.setVisible("soundIcon");
        // flow.setVisible("homeIcon");
        // flow.setVisible("backIcon");
        // flow.setVisible("settingsIcon");
      }
   }

   @OnCall
   public void showPrevious() {
      screen.showPrevious();
   }

   @OnCall
   public void showPage(String page) {
      screen.showPage(page);
   }
   
   @OnCall
   public void showPage(String page, String function) {
      screen.showPage(page, function);
   }
   
   @OnCall
   public void showHighScores(){
      screen.showPage("score");
   }
   
   @OnCall
   public void showNewGames() {
      screen.showPage("createGame");
   }
   
   @OnCall
   public void showGameHistory() {
      screen.showPage("history");
   }   
   
   @OnCall
   public void showSavedGames() {
      screen.showPage("resumeGame");
   }
   
   @OnCall
   public void showAccountPage() {
      ChessUser user = userSource.currentUser();
      List<ChessUser> exstingUsers = userSource.listUsers(ChessUserType.NORMAL);
      
      if(user != null) {
         ChessUserType type = user.getType();
         
         if(type != ChessUserType.ANONYMOUS) {
            screen.showPage("accountSignOut"); // already logged in
         } else {
            if(exstingUsers.isEmpty()) {
               screen.showPage("accountRegister");
            } else {
               screen.showPage("accountSignIn"); 
            }
         }
      } else {
         if(exstingUsers.isEmpty()) {
            screen.showPage("accountRegister");
         } else {
            screen.showPage("accountSignIn"); 
         }
      }
   }
   
   @OnCall
   public void showRank() {
      screen.showPage("rank");
   }
   
   @OnCall
   public void showMenu() {
      screen.showMenu();
   }

   @OnCall
   public void hideMenu() {
      screen.hideMenu();
      
      if(toggle != null) {
         toggle.toggle();
      }
   }
}
