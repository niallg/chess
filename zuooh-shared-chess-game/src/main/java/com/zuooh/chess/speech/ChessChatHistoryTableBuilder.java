package com.zuooh.chess.speech;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Window;
import com.zuooh.application.canvas.Frame;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.TemplateList;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.timer.ChessTimeDrawer;
import com.zuooh.style.Dimension;
import com.zuooh.style.Font;
import com.zuooh.style.Rectangle;
import com.zuooh.style.Style;

@Component
public class ChessChatHistoryTableBuilder {

   private final Map<String, Panel> chatMessages;
   private final TemplateList chatList;
   private final Window window;
   
   public ChessChatHistoryTableBuilder(
         @Inject("application") MobileApplication application,       
         @Inject("chatList") TemplateList chatList)  
   {
      this.chatMessages = new ConcurrentHashMap<String, Panel>();
      this.window = application.getWindow();
      this.chatList = chatList;
   }
   
   public boolean createOpponentMessage(ChessChat message, ChessSide color) {
      return createMessage(message, color, true);
   }
   
   public boolean createPlayerMessage(ChessChat message, ChessSide color) {      
      return createMessage(message, color, false);
   }
   
   private boolean createMessage(ChessChat message, ChessSide color, boolean opponent) {
      String text = message.getMessage();
      String messageId = message.getChatId();
      Panel updatePanel = chatMessages.get(messageId);
      
      if(updatePanel == null) {
         Frame context = createFrame(message, color, opponent ? "opponent" : "player");        
         Panel textPanel = context.getCanvas("text"); 
         Panel timePanel = context.getCanvas("time");      
         Panel iconPanel = context.getCanvas("icon");         

         iconPanel.setStyleClass(color.name + "Pawn");
         textPanel.setText(text);
         
         chatMessages.put(messageId, timePanel);
         updateTime(message);
         
         if(!chatList.containsFrame("bottom")) {
            chatList.addLast("bottom", "empty");
         }
         return true;
      } 
      updateTime(message);
      return false;
   }
   
   private boolean updateTime(ChessChat message) {
      long currentTime = System.currentTimeMillis();
      long timeStamp = message.getTimeStamp();
      long duration = currentTime - timeStamp;
      String messageId = message.getChatId();
      Panel updatePanel = chatMessages.get(messageId);
      
      if(updatePanel != null) {
         if(duration > 2000) {
            String lastSeenTime = ChessTimeDrawer.convertToTextTime(duration, true);
            String timeText = lastSeenTime.trim();
            
            updatePanel.setText(timeText + " ago");
         } else {
            updatePanel.setText("Just now");
         }
         return true;
      }
      return false;
   }
   
   private Frame createFrame(ChessChat message, ChessSide color, String prefix) {
      String text = message.getMessage();
      String messageId = message.getChatId();      
      Frame context = chatList.addFirst(messageId, prefix + "Big");
      Rectangle totalBounds = context.getRectangle();
      Panel textPanel = context.getCanvas("text");
      Rectangle textBounds = textPanel.getRectangle();
      Style chatStyle = textPanel.getStyle();
      Font font = chatStyle.getFont();         
      int fontHeight = font.getSize();
      Dimension textSize = window.textDimensions(text, font, textBounds.width); // XXX this is kind of broken!!!
      int extraHeight = (textSize.height - textBounds.height) + fontHeight;      

      chatList.removeFrame(messageId); // remove frame as was used to measure only!   

      if(textBounds.width > textSize.width * 2.0f) {
         return chatList.addFirst(messageId, prefix + "Small", totalBounds.height + extraHeight);                    
      }           
      return chatList.addFirst(messageId, prefix + "Big", totalBounds.height + extraHeight);            
      
   }
    
}
