package com.zuooh.chess.speech;


import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.canvas.info.speech.SpeechBubbleCreator;
import com.zuooh.application.canvas.info.speech.SpeechBubbleDrawer;
import com.zuooh.application.canvas.info.speech.SpeechPointerMetrics;
import com.zuooh.application.canvas.info.speech.SpeechPointerSize;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.style.Color;
import com.zuooh.style.Orientation;
import com.zuooh.style.Quality;
import com.zuooh.style.Shadow;

@Component
public class ChessChatBubbleCreator {
   
   private final SpeechBubbleCreator wideSpeechCreator;  
   private final SpeechBubbleCreator speechCreator;
   private final Screen screen;
   private final Shadow shadow;
   private final Color bubbleColor;
   private final Color shadowColor;
   
   public ChessChatBubbleCreator(
         @Inject("application") MobileApplication application,
         @Inject("wideSpeechBubble") SpeechBubbleCreator speechCreator,
         @Inject("veryWideSpeechBubble") SpeechBubbleCreator wideSpeechCreator)  
   {
      this.shadowColor = new Color("#55000000");
      this.shadow = new Shadow(shadowColor, Quality.NORMAL, 10);
      //this.bubbleColor = new Color("#fcfdf8");
      this.bubbleColor = new Color("#ffffff");      
      this.screen = application.getScreen();
      this.wideSpeechCreator = wideSpeechCreator;
      this.speechCreator = speechCreator;
   }

   public SpeechBubbleDrawer createDrawer() {
      SpeechPointerMetrics metrics = SpeechPointerSize.SHORT.createMetrics(bubbleColor, Color.BLACK, shadow, 2);
      Orientation orientation = screen.orientation();
      
      return wideSpeechCreator.createDrawer(metrics, 20, 2);
   }
}
