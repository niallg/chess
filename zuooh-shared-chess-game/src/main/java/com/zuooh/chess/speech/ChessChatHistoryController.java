package com.zuooh.chess.speech;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Dialog;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.task.ChessTaskController;

@Component
public class ChessChatHistoryController extends ChessTaskController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessChatHistoryController.class);
   
   private static final int MAX_MESSAGES = 10;
   
   private final ChessMessagePublisher messagePublisher;
   private final ChessChatHistoryTableBuilder tableBuilder;
   private final ChessBoardContext boardContext;
   private final ChessGameManager gameManager;
   private final ChessDatabase gameDatabase;
   private final Window window;
   private final Screen screen;
   
   public ChessChatHistoryController(
         @Inject("application") MobileApplication application,
         @Inject("window") Window window,
         @Inject("gameManager") ChessGameManager gameManager,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("tableBuilder") ChessChatHistoryTableBuilder tableBuilder,
         @Inject("chatPublisher") ChessMessagePublisher messagePublisher)
   {
      this.screen = application.getScreen();
      this.messagePublisher = messagePublisher;
      this.tableBuilder = tableBuilder;
      this.gameManager = gameManager;
      this.boardContext = boardContext;
      this.gameDatabase = gameDatabase;
      this.window = window;
   }
   
   @Override
   public void onBeforeStart() throws Exception {
      ChessPlayer oppponent = boardContext.getOpponent();
      String gameId = oppponent.getAssociatedGame();
      ChessChatDatabase chatDatabase = gameDatabase.getChatDatabase();
      List<ChessChat> chatHistory = chatDatabase.listAllMessages(gameId);         
      
      if(chatHistory.isEmpty()) {
         Dialog dialog = screen.openDialog("warningDialog");
         
         dialog.setAttribute("warningTitle", "Chat");
         dialog.setAttribute("warningMessage", "There are no chat messages to display!");
         dialog.setAttribute("warningFunction", "dialog.hideDialog()");                  
         dialog.setAttribute("warningButton", "Okay");
         dialog.showDialog();  
      }
   }
   
   @Override
   public long onInterval() throws Exception {
      ChessUser playerProfile = gameManager.currentUser();
      ChessPlayer oppponent = boardContext.getOpponent();
      ChessSide opponentColor = boardContext.getOpponentSide();
      ChessSide playerColor = opponentColor.oppositeSide();
      String gameId = oppponent.getAssociatedGame();
      ChessChatDatabase chatDatabase = gameDatabase.getChatDatabase();
      List<ChessChat> chatHistory = chatDatabase.listAllMessages(gameId);
      String playerName = playerProfile.getKey();
      int size = chatHistory.size(); 
      int start = size - MAX_MESSAGES; 
      
      if(size > 0) {
         List<String> readMessages = new ArrayList<String>();
         
         for(ChessChat message : chatHistory) {
            String uniqueId = message.toString();
            String chatId = message.getChatId();
            String from = message.getFrom();
            
            if(start-- <= 0) {            
               if(from.equals(playerName)) {
                  if(tableBuilder.createPlayerMessage(message, playerColor)) {
                     readMessages.add(chatId);
                  }
               } else {
                  if(tableBuilder.createOpponentMessage(message, opponentColor)) {
                     readMessages.add(chatId);
                  }            
               }
            } else {
               LOG.debug("Ignoring message " + uniqueId + " from " + from);
            }
         }             
         for(String chatId : readMessages) {
            chatDatabase.markMessageAsRead(chatId);
         }
      }
      screen.scrollLock(false); // unlock just in case!
      window.invalidate(); // update timestamp              
      return 2000;
   }
}
