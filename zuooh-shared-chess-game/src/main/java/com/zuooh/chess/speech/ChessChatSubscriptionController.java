package com.zuooh.chess.speech;

import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Button;
import com.zuooh.application.ButtonTask;
import com.zuooh.application.Click;
import com.zuooh.application.Document;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.canvas.Canvas;
import com.zuooh.application.canvas.IconList;
import com.zuooh.application.canvas.ImageIcon;
import com.zuooh.application.canvas.info.Bubble;
import com.zuooh.application.canvas.info.speech.SpeechBubbleCreator;
import com.zuooh.application.canvas.info.speech.SpeechBubbleDrawer;
import com.zuooh.application.canvas.info.speech.SpeechPointerSize;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.control.ChessBoardController;
import com.zuooh.chess.control.ChessBoardEvent;
import com.zuooh.chess.control.ChessBoardEventScheduler;
import com.zuooh.chess.control.ChessBoardOperation;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.database.chat.ChessChatStatus;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.task.ChessTaskController;
import com.zuooh.chess.view.ChessBoardCellView;
import com.zuooh.chess.view.ChessBoardPanel;
import com.zuooh.style.Color;
import com.zuooh.style.Font;
import com.zuooh.style.Quality;
import com.zuooh.style.Rectangle;
import com.zuooh.style.Shadow;
import com.zuooh.style.Style;

@Component
public class ChessChatSubscriptionController extends ChessTaskController {

   private static final Logger LOG = LoggerFactory.getLogger(ChessChatSubscriptionController.class);
   
   private final ChessBoardEventScheduler eventScheduler;
   private final ChessBoardContext boardContext;
   private final SpeechBubbleCreator speechCreator;
   private final ChessMessagePublisher messagePublisher;
   private final ChessDatabase gameDatabase;
   private final Document document;
   private final Window window;
   private final Rectangle bounds;
   private final IconList whiteIcons;
   private final IconList blackIcons;
   private final Style style;   

   public ChessChatSubscriptionController(
         @Inject("window") Window window,
         @Inject("eventService") ChessBoardEventScheduler eventScheduler,
         @Inject("speechBubble") SpeechBubbleCreator speechCreator,
         @Inject("chatPublisher") ChessMessagePublisher messagePublisher,  
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("whiteIcons") IconList whiteIcons,
         @Inject("blackIcons") IconList blackIcons,
         @Inject("document") Document document,         
         @Inject("main") Rectangle bounds,
         @Inject("speech") Style style)        
   {
      this.messagePublisher = messagePublisher;
      this.eventScheduler = eventScheduler;
      this.speechCreator = speechCreator;
      this.boardContext = boardContext;
      this.gameDatabase = gameDatabase;
      this.blackIcons = blackIcons;
      this.whiteIcons = whiteIcons;
      this.document = document;
      this.bounds = bounds;
      this.window = window;
      this.style = style;
   }

   @Override
   public void onBeforeStart()  {
      LOG.info("Starting the chat controller");
   }   

   @Override
   public long onInterval() throws Exception {
      try {
         ChessPlayer opponent = boardContext.getOpponent();
         String gameId = opponent.getAssociatedGame();
         ChessSide opponentDirection = opponent.getPlayerSide();
         ChessUser profile = boardContext.getBoardUser();
         String userName = profile.getKey();
         final ChessChatDatabase chatDatabase = gameDatabase.getChatDatabase();
         List<ChessChat> mail = chatDatabase.listNewMessagesTo(gameId, userName);
         boolean newMail = !mail.isEmpty();        
         
         if(opponentDirection == ChessSide.WHITE) {
            ImageIcon icon = whiteIcons.getIcon("whiteMessage");
            
            if(icon != null) {
               icon.setVisible(newMail);
            }
         } else {
            ImageIcon icon = blackIcons.getIcon("blackMessage");
            
            if(icon != null) {
               icon.setVisible(newMail);
            }
         }
         //if(!newMail) {
         //   ChessChatHistoryEvent historyEvent = new ChessChatHistoryEvent(userName, gameId);         
         //   messagePublisher.executeRequest(historyEvent);            
         //}
      } catch(Exception e) {
         LOG.info("Exception while waiting for chat message", e);
      }
      return 5000;
   }   
   
   @OnCall
   public boolean showMail() throws Exception {
      final ChessPlayer opponent = boardContext.getOpponent();
      final String gameId = opponent.getAssociatedGame();
      final ChessSide opponentDirection = opponent.getPlayerSide();
      final ChessUser profile = boardContext.getBoardUser();
      final String userName = profile.getKey();
      final ChessChatDatabase chatDatabase = gameDatabase.getChatDatabase();
      final List<ChessChat> mail = chatDatabase.listNewMessagesTo(gameId, userName);
      
      if(!mail.isEmpty()) {
         eventScheduler.executeEvent(new ChessBoardEvent() {
   
            @Override
            public ChessBoardOperation executeEvent() {
               return new ChessBoardOperation() {
                  @Override
                  public void onOperation(ChessBoardController controller, ChessBoardPanel boardPanel, String currentGameId) {
                     try {
                        if(!currentGameId.equals(gameId)) {
                           LOG.info("Not rendering a chat message from previous game");
                        } else {
                           Iterator<ChessChat> newMails = mail.iterator();
                           ChessChat newMail = newMails.next();
                           ChessChatStatus status = newMail.getStatus();
                           String chatId = newMail.getChatId();
   
                           if(!status.isRead()) {
                              String message = newMail.getMessage();
                              ChessBoardCellView cellView = boardPanel.getKingCell(opponentDirection);
                              Rectangle rectangle = cellView.getRectangle();
                              
                              // do some crude scaling for now!!
                              float scale = document.getScale();
                              float width = rectangle.floatWidth;
                              float pad = (width / 2.5f) - (1f * scale);
                              int lineThickness = Math.round(2 * scale);
                              int rectPadding = Math.round(15 * scale); 
                              int screenPadding = Math.round(1 * scale);
                              int shadowDistance = Math.round(5 * scale);
                              Rectangle centerOfSpeech = rectangle.createInnerRectangle(pad);
                              Style scaledStyle = style.getScaled(scale);
                              Font font = scaledStyle.getFont();                              
                              Color shadowColor = new Color("#99000000"); 
                              Shadow shadow = new Shadow(shadowColor, Quality.NORMAL, shadowDistance);                                 
                              SpeechBubbleDrawer speechDrawer = speechCreator.createDrawer(SpeechPointerSize.SHORT, shadow, lineThickness, rectPadding, screenPadding);
                              final Bubble bubble = speechDrawer.drawBubble(bounds, message, font, centerOfSpeech);
                              bubble.setButton(new Button() {      
                                 @Override
                                 public ButtonTask click(Click click, Canvas canvas) {
                                    Canvas modalCanvas = window.getModal();
   
                                    if(canvas == bubble && canvas == modalCanvas) {
                                       window.setModal(null);
                                       window.invalidate();
                                    }
                                    return null;
                                 }
                              });
                              window.setModal(bubble);
                              chatDatabase.markMessageAsRead(chatId);
                              
                           }
                        }
                     } catch(Exception e) {
                        LOG.info("Could not display message", e);
                     }
                  }
               };
            }
         });
         return true;
      }
      return false;
   }
}
