package com.zuooh.chess.speech;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Tile;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.canvas.info.speech.SpeechBubbleCreator;
import com.zuooh.application.keypad.KeyPad;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.chat.ChessChatEvent;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.database.chat.ChessChatStatus;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.common.task.TaskScheduler;

@Component
public class ChessChatPublishController {

   private static final Logger LOG = LoggerFactory.getLogger(ChessChatPublishController.class);
   
   private final ChessDatabase gameDatabase;
   private final ChessBoardContext boardContext;
   private final ChessMessagePublisher publisher;
   private final MobileApplication application;
   private final Screen screen;

   public ChessChatPublishController(
         @Inject("application") MobileApplication application,
         @Inject("speechBubble") SpeechBubbleCreator speechCreator,
         @Inject("chatPublisher") ChessMessagePublisher publisher,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("document") Document document,
         @Inject("window") Window window,
         @Inject("chatTile") Tile chatTile)
   {   
      this.screen = application.getScreen();
      this.gameDatabase = gameDatabase;
      this.publisher = publisher;
      this.application = application;
      this. boardContext = boardContext;
   }   
  
   @OnCall
   public void cancelMessage() {
      //screen.showPrevious();
      screen.showPrevious();
   }

   @OnStart
   public void chatMessage() {
      KeyPad keyPad = application.getKeyPad();

      //chatButton.toggle();
      //screen.hideMenu();
      keyPad.showKeyPad();
   }   

   @OnCall
   public void sendChat(String text) {
      final TaskScheduler scheduler = application.getTaskScheduler();
      final ChessPlayer opponent = boardContext.getOpponent();
      final ChessChatDatabase chatDatabase = gameDatabase.getChatDatabase();
      final String gameId = opponent.getAssociatedGame();
      final String currentText = text;

      if(validChat(currentText)) {
         scheduler.scheduleRunnable(new Runnable() {
            public void run() {
               try {
                  long time = System.currentTimeMillis();
                  ChessUser playerProfile = boardContext.getBoardUser();
                  ChessUser opponentProfile = opponent.getUser();
                  
                  if(opponentProfile != null) {
                     String to = opponentProfile.getKey();
                     String from = playerProfile.getKey();
                     ChessChat mail = new ChessChat(ChessChatStatus.UNREAD, from + time, gameId, from, to, currentText, time);
                     ChessChatEvent event = new ChessChatEvent(mail);
      
                     if(boardContext.isGameOnline()) {
                        publisher.publishNotification(event);
                        chatDatabase.saveMessage(mail);
                     } else {
                        ChessChat response = new ChessChat(ChessChatStatus.READ, to + time, gameId, to, from, "I don't understand \"" + currentText + "\"", time);                     
                        chatDatabase.saveMessage(mail);
                        chatDatabase.saveMessage(response); // computer responds straight away                     
                     }
                  }
               } catch(Exception e) {
                  LOG.info("Could not send chat message", e);
               }
            }
         });
      }
      screen.showPrevious();
   }
      
   private boolean validChat(String text) {
      return text != null && text.trim().length() > 0;
   }      
}
