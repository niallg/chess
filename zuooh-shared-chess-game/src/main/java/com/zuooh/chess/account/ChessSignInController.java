package com.zuooh.chess.account;

import java.util.Iterator;
import java.util.List;

import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.canvas.FormInput;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.bind.annotation.Required;
import com.zuooh.chess.client.user.ChessUserDirectorySource;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserType;

@Component
public class ChessSignInController {

   @Required
   @Inject("userSource")
   private ChessUserDirectorySource userSource;

   @Required
   @Inject("password")
   public FormInput passwordBox;   

   @Required
   @Inject("name")
   public FormInput nameBox;   

   @OnCreate
   public void populateForm() {
      List<ChessUser> profile = userSource.listUsers(ChessUserType.NORMAL);
      Iterator<ChessUser> iterator = profile.iterator();
      
      if(iterator.hasNext()) {
         ChessUser user = iterator.next();
         String name = user.getName();
         String password = user.getPassword();
         
         if(nameBox != null && name != null) {
            nameBox.setText(name);
         }
         if(passwordBox != null && password != null) {
            passwordBox.setText(password);
         }
      }     
   }
}
