package com.zuooh.chess.account.form;

import java.util.Map;

import com.zuooh.chess.client.ChessClientController;
import com.zuooh.chess.client.event.operation.form.ChessFormAction;
import com.zuooh.chess.client.event.request.ChessRegisterUserEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserBuilder;
import com.zuooh.chess.client.user.ChessUserDirectorySource;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;

public class ChessRegisterUserAction implements ChessFormAction<ChessRegisterUserEvent, ChessUserValidationEvent> {

   private final ChessClientController gameController;  
   private final ChessUserDirectorySource userSource;    
   private final ChessUserBuilder userBuilder;
   private final ChessDatabase gameDatabase;
   
   public ChessRegisterUserAction(ChessUserBuilder userBuilder, ChessClientController gameController, ChessUserDirectorySource userSource, ChessDatabase gameDatabase) {
      this.gameController = gameController;
      this.userSource = userSource;
      this.gameDatabase = gameDatabase;      
      this.userBuilder = userBuilder;
   }   

   @Override
   public String executeResponse(ChessUserValidationEvent response) throws Exception {
      ChessUser user = response.getUser();               
      ChessUserDatabase userDatabase = gameDatabase.getUserDatabase();
      
      userSource.loginUser(user);
      userDatabase.saveUser(user);
      gameController.restart();
      
      return "accountWelcome";
   }

   @Override
   public String validateRequest(Map<String, String> attributes) throws Exception {
      String name = attributes.get("name");
      String password = attributes.get("password");
      String confirmPassword = attributes.get("confirmPassword");
      
      if(name == null || name.equals("")) {
         return "User name must not be blank!";
      }
      if(password == null || password.equals("")) {
         return "Password must not be blank!";
      }
      if(confirmPassword == null || confirmPassword.equals("")) {
         return "Password confirmation must not be blank!";
      }      
      if(!password.equals(confirmPassword)) {
         return "Passwords do not match, please try again!";
      }
      return null;
   }

   @Override
   public String validateResponse(ChessUserValidationEvent response) throws Exception {
      ChessUserValidation profileStatus = response.getValidation();
      
      if(profileStatus == ChessUserValidation.USER_NAME_REGISTERED) {
         return "An account already exists with that user name!";
      }
      return null;   
   }
   
   @Override
   public String requiresRedirect(ChessUserValidationEvent response) throws Exception {
      return null;
   }    

   @Override
   public ChessRegisterUserEvent createRequest(Map<String, String> attributes) throws Exception {
      String name = attributes.get("name");
      String password = attributes.get("password");
      ChessUser user = userBuilder.createNormalUser(name, password);
      
      return new ChessRegisterUserEvent(user);
   } 
}
