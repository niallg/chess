package com.zuooh.chess.account;

import java.util.List;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserType;

@Component
public class ChessRegisterCheckController {   

   private final ChessUserSource userSource;
   private final Screen screen;

   public ChessRegisterCheckController(
         @Inject("application") MobileApplication application,        
         @Inject("gameManager") ChessGameManager gameManager,
         @Inject("userSource") ChessUserSource userSource)
   {
      this.screen = application.getScreen();
      this.userSource = userSource;
   }
   
   @OnStart
   public void showAccountPage() {
      List<ChessUser> exstingUsers = userSource.listUsers(ChessUserType.NORMAL);
      
      if(exstingUsers.isEmpty()) {
         screen.showPage("accountCreate");
      } else {
         screen.showPage("home"); 
      }
   }
}
