package com.zuooh.chess.account;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.ResourceState;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.canvas.FormInput;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.bind.annotation.Required;
import com.zuooh.chess.client.user.ChessUserDirectorySource;

@Component
public class ChessVerifyPasswordController {

   @Required
   @Inject("application")
   private MobileApplication application;  
   
   @Required
   @Inject("userSource")
   private ChessUserDirectorySource userSource;  

   @Required
   @Inject("name")
   public FormInput nameBox;   

   @OnCreate
   public void populateForm() {
      ResourceState state = application.getResourceState("registerForm");
      Object value = state.getAttribute("name");
      
      if(value != null) { // this should not be blank!!
         String name = String.valueOf(value);
            
         if(nameBox != null && name != null) {
            nameBox.setText(name);
         }     
      }
   }
}
