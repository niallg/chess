package com.zuooh.chess.account.form;

import java.util.Map;

import com.zuooh.chess.client.ChessClientController;
import com.zuooh.chess.client.event.operation.form.ChessFormAction;
import com.zuooh.chess.client.event.request.ChessChangePasswordEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserDirectorySource;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;

public class ChessChangePasswordAction implements ChessFormAction<ChessChangePasswordEvent, ChessUserValidationEvent> {

   private final ChessClientController gameController;  
   private final ChessUserDirectorySource userSource;    
   private final ChessDatabase gameDatabase;
   
   public ChessChangePasswordAction(ChessClientController gameController, ChessUserDirectorySource userSource, ChessDatabase gameDatabase) {
      this.gameController = gameController;
      this.userSource = userSource;
      this.gameDatabase = gameDatabase;      
   }   

   @Override
   public String executeResponse(ChessUserValidationEvent response) throws Exception {
      ChessUser user = userSource.currentUser();               
      ChessUserDatabase userDatabase = gameDatabase.getUserDatabase();
      
      userSource.loginUser(user);
      userDatabase.saveUser(user);
      gameController.restart();
      
      return "accountWelcome";
   }

   @Override
   public String validateRequest(Map<String, String> attributes) throws Exception { 
      String password = attributes.get("password");
      String confirmPassword = attributes.get("confirmPassword");
      
      if(password == null || password.equals("")) {
         return "Password must not be blank!";
      }
      if(confirmPassword == null || confirmPassword.equals("")) {
         return "Password confirmation must not be blank!";
      }      
      if(!password.equals(confirmPassword)) {
         return "Passwords do not match, please try again!";
      }
      return null;
   }

   @Override
   public String validateResponse(ChessUserValidationEvent response) throws Exception {
      return null;
   }
   
   @Override
   public String requiresRedirect(ChessUserValidationEvent response) throws Exception {
      return null;
   }    

   @Override
   public ChessChangePasswordEvent createRequest(Map<String, String> attributes) throws Exception {
      ChessUser user = userSource.currentUser();  
      String userId = user.getKey();
      String password = attributes.get("password");

      return new ChessChangePasswordEvent(userId, password);
   }
}
