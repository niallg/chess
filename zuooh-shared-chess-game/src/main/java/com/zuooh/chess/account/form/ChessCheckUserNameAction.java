package com.zuooh.chess.account.form;

import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.zuooh.chess.client.event.operation.form.ChessFormAction;
import com.zuooh.chess.client.event.request.ChessCheckUserNameEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserValidation;

public class ChessCheckUserNameAction implements ChessFormAction<ChessCheckUserNameEvent, ChessUserValidationEvent> {
   
   private static final Pattern REQUIRED_PATTERN = Pattern.compile("^[A-Za-z0-9 ]*[A-Za-z0-9][A-Za-z0-9 ]*$");
   
   private final Set<String> illegalWords;
   private final Set<String> illegalNames;
   
   public ChessCheckUserNameAction(Set<String> illegalWords, Set<String> illegalNames) {
      this.illegalWords = illegalWords;
      this.illegalNames = illegalNames; 
   }

   @Override
   public String executeResponse(ChessUserValidationEvent response) throws Exception {
      ChessUserValidation validation = response.getValidation();
      
//      if(validation == ChessUserValidation.USER_NAME_REGISTERED) {
//         return "accountVerifyPassword";
//      }
//      if(validation == ChessUserValidation.USER_NAME_NOT_REGISTERED) {
//         return "accountRegisterPassword";
//      }  
      if(validation == ChessUserValidation.USER_NAME_REGISTERED) {
          return "accountCreateRetry";
       }
       if(validation == ChessUserValidation.USER_NAME_NOT_REGISTERED) {
          return "home";
       }       
      return "accountError";
   }

   @Override
   public String validateRequest(Map<String, String> attributes) throws Exception {
      String userName = attributes.get("name");
      
      if(userName == null || userName.equals("")) {
         return "User name must not be blank!";
      }
      String trimmedName = userName.trim();      
      int length = trimmedName.length();
      
      if(length > 16) {
         return "User name must be sixteen characters or less!";
      }
      String userText = userName.toLowerCase();     
      String userTokens[] = userText.split("\\s+");
      
      for(String userToken : userTokens) {
         for(String token : illegalWords) {         
            if(userToken.equals(token)) {
               return "User name contains an inappropriate word!";
            }
         }
      }
      for(String token : illegalNames) {
         if(userText.equals(token)) {
            return "User name is illegal, please choose another name!";
         }
      }
      Matcher matcher = REQUIRED_PATTERN.matcher(userText);
      
      if(!matcher.matches()) {
         return "User name can contain only letters, digits, and spaces!";
      }
      return null;
   }

   @Override
   public String validateResponse(ChessUserValidationEvent response) throws Exception {
      return null;
   }
   
   @Override
   public String requiresRedirect(ChessUserValidationEvent response) throws Exception {
      return null;
   }    

   public ChessCheckUserNameEvent createRequest(Map<String, String> attributes) throws Exception {
      String userName = attributes.get("name");      
      return new ChessCheckUserNameEvent(userName);
   }    
}
