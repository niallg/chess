package com.zuooh.chess.account.form;

import java.util.Map;

import com.zuooh.chess.client.ChessClientController;
import com.zuooh.chess.client.event.operation.form.ChessFormAction;
import com.zuooh.chess.client.event.request.ChessSignInEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserDirectorySource;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;

public class ChessSignInAction implements ChessFormAction<ChessSignInEvent, ChessUserValidationEvent> {

   private final ChessClientController gameController;  
   private final ChessUserDirectorySource userSource;    
   private final ChessDatabase gameDatabase;
   
   public ChessSignInAction(ChessClientController gameController, ChessUserDirectorySource userSource, ChessDatabase gameDatabase) {
      this.gameController = gameController;
      this.userSource = userSource;
      this.gameDatabase = gameDatabase;      
   }   

   @Override
   public String executeResponse(ChessUserValidationEvent response) throws Exception {
      ChessUser user = response.getUser();               
      ChessUserDatabase userDatabase = gameDatabase.getUserDatabase();
      
      userSource.loginUser(user);
      userDatabase.saveUser(user);
      gameController.restart();
      
      return "accountWelcome";
   }

   @Override
   public String validateRequest(Map<String, String> attributes) throws Exception {
      String user = attributes.get("name");      
      String password = attributes.get("password");
      
      if(password == null || password.equals("")) {
         return "Password must not be blank!";
      }
      if(user == null || user.equals("")) {
         return "User name must not be blank!";
      }      
      return null;
   }

   @Override
   public String validateResponse(ChessUserValidationEvent response) throws Exception {          
      ChessUserValidation profileStatus = response.getValidation();
      
      if(profileStatus == ChessUserValidation.PASSWORD_IS_INCORRECT) {
         return "Incorrect password, please try again!";
      } 
      if(profileStatus == ChessUserValidation.USER_NAME_NOT_REGISTERED) {
         return "No account with the provided user name has been registered!";
      }
      return null;      
   }
   
   @Override
   public String requiresRedirect(ChessUserValidationEvent response) throws Exception {
      ChessUserValidation profileStatus = response.getValidation();
      
      if(profileStatus == ChessUserValidation.USER_NAME_NOT_REGISTERED) {
         return "accountRegister";
      }
      return null;
   }    

   @Override
   public ChessSignInEvent createRequest(Map<String, String> attributes) throws Exception {
      String password = attributes.get("password");
      String name = attributes.get("name");
      
      return new ChessSignInEvent(name, password);
   } 
}
