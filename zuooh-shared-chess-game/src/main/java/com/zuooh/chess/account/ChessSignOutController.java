package com.zuooh.chess.account;

import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.canvas.FormInput;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.bind.annotation.Required;
import com.zuooh.chess.client.user.ChessUserDirectorySource;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;

@Component
public class ChessSignOutController {

   @Required
   @Inject("userSource")
   private ChessUserDirectorySource userSource;

   @Required
   @Inject("password")
   public FormInput passwordBox;
   
   @Required
   @Inject("skillPanel")
   public Context skillPanel;

   @Required
   @Inject("name")
   public FormInput nameBox;   

   @OnCreate
   public void populateForm() {
      ChessUser user = userSource.currentUser();
      ChessUserData data = user.getData();
      String name = user.getName();
      String password = user.getPassword();
      int skill = data.getSkill();
      
      for(int i = 0; i < skill; i++) {
         Panel panel = skillPanel.getCanvas("star" + (i+ 1));
         
         if(panel != null) {
            panel.setStyleClass("star");
         }
      }
      if(nameBox != null) {
         nameBox.setText(name);
      }
      if(passwordBox != null) {
         passwordBox.setText(password);
      }         
   }
   
   @OnCall
   public void signOut(){
      userSource.resetUser();
   }
}
