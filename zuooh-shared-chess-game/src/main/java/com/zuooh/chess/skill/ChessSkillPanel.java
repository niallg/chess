package com.zuooh.chess.skill;

import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessBoardMatch;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.style.Style;

@Component
public class ChessSkillPanel {

   private final ChessBoardContext boardContext;
   private final ChessDatabase gameDatabase;
   private final ChessBoardMatch boardMatch;
   private final Context blackSkill;
   private final Context whiteSkill;
   private final Style style;

   public ChessSkillPanel(
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("blackSkill") Context blackSkill,
         @Inject("whiteSkill") Context whiteSkill,
         @Inject("skill") Style style)
   {
      this.boardMatch = new ChessBoardMatch(boardContext, gameDatabase);
      this.gameDatabase = gameDatabase;
      this.boardContext = boardContext;
      this.blackSkill = blackSkill;
      this.whiteSkill = whiteSkill;
      this.style = style;
   }

   @OnStart
   public void refreshDetails() throws Exception {
      String gameId = boardContext.getGameId();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      ChessUser playerUser = boardContext.getBoardUser();
      ChessSide playerColor = boardGame.getUserSide();
      ChessUserData playerData = playerUser.getData();
      int playerSkill = playerData.getSkill();

      for(int i = 1; i <= playerSkill; i++) {
         Panel playerPanel = whiteSkill.getCanvas("whiteStar" + i);

         if(playerColor == ChessSide.BLACK) {
            playerPanel = blackSkill.getCanvas("blackStar" + i);
         }
         if(i <= playerSkill) {
            playerPanel.setStyle(style);
         }
      }
      ChessSide opponentSide = playerColor.oppositeSide();
      ChessUser opponentProfile = boardMatch.matchPlayer(opponentSide.name); // this trys to resolve from many places
      
      if(opponentProfile != null) { // perhaps not accepted yet?
         ChessUserData opponentData = opponentProfile.getData();
         int opponentSkill = opponentData.getSkill();
         
         for(int i = 1; i <= Math.max(opponentSkill, playerSkill); i++) {
            Panel opponentPanel = blackSkill.getCanvas("whiteStar" + i);
   
            if(playerColor == ChessSide.WHITE) {
               opponentPanel = whiteSkill.getCanvas("blackStar" + i);
            }
            if(i <= opponentSkill) {
               opponentPanel.setStyle(style);
            }
         }
      }
   }
}
