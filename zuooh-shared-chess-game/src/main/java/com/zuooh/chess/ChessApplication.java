package com.zuooh.chess;

import com.zuooh.chess.database.ChessDatabase;

public class ChessApplication {
   
   private final ChessDatabase gameDatabase;
   private final ChessGameManager gameManager;
   
   public ChessApplication(ChessGameManager gameManager, ChessDatabase gameDatabase) {
      this.gameManager = gameManager;
      this.gameDatabase = gameDatabase;
   }   
   
   public ChessGameManager getGameManager() {
      return gameManager;
   }
   
   public ChessDatabase getChessGameDatabase(){
      return gameDatabase;
   }
}
