package com.zuooh.chess.history;

import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.result.ChessResultRecord;

public class ChessResultHistoryItem {

   private final ChessResultRecord resultRecord;
   private final ChessBoardGame boardGame;

   public ChessResultHistoryItem(ChessBoardGame boardGame) {
      this(boardGame, null);
   }
   
   public ChessResultHistoryItem(ChessBoardGame boardGame, ChessResultRecord resultRecord) {
      this.resultRecord = resultRecord;
      this.boardGame = boardGame;
   }
   
   public ChessBoardGame getBoardGame() {
      return boardGame;
   }
   
   public ChessResultRecord getResultRecord() {
      return resultRecord;
   }   
   
   public long getResultTime(){
      if(resultRecord != null) {
         return resultRecord.getResultTime();
      }
      return boardGame.getCreationTime();
   }
}
