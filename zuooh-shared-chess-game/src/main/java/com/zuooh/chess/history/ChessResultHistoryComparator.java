package com.zuooh.chess.history;

import java.util.Comparator;

public class ChessResultHistoryComparator implements Comparator<ChessResultHistoryItem> {

   @Override
   public int compare(ChessResultHistoryItem left, ChessResultHistoryItem right) {
      Long rightTime = right.getResultTime();
      Long leftTime = left.getResultTime();
      
      return rightTime.compareTo(leftTime);
   }

}
