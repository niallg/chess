package com.zuooh.chess.history;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.result.ChessResultDatabase;
import com.zuooh.chess.database.result.ChessResultRecord;

public class ChessResultHistorySearcher {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessResultHistoryController.class);

   private final ChessResultHistoryComparator resultComparator;
   private final ChessDatabase gameDatabase;

   public ChessResultHistorySearcher(ChessDatabase gameDatabase) {
      this.resultComparator = new ChessResultHistoryComparator();   
      this.gameDatabase = gameDatabase;
   }
  
   public List<ChessResultHistoryItem> recentResults(String userId) {
      List<ChessResultHistoryItem> recentResults = new ArrayList<ChessResultHistoryItem>();
      Set<String> alreadyDone = new HashSet<String>();
   
      try {
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
         ChessResultDatabase resultDatabase = gameDatabase.getResultDatabase();      
         List<ChessResultRecord> resultRecords = resultDatabase.listResults(userId);

         for(ChessResultRecord resultRecord : resultRecords) {
            String gameId = resultRecord.getGameId();
            ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
            ChessBoardResult boardResult = boardGame.getBoardResult();    
            
            if(boardGame != null && boardResult != ChessBoardResult.NONE && boardResult != ChessBoardResult.REJECT) { // only show real results
               ChessResultHistoryItem historyRecord = new ChessResultHistoryItem(boardGame, resultRecord);
               
               alreadyDone.add(gameId);               
               recentResults.add(historyRecord);
            } else {
               resultDatabase.deleteResult(resultRecord); // no matching board game, no longer useful
            }
         }
         int resultCount = resultRecords.size();
         
         if(resultCount < 3) { // don't bother with older stuff if we have some data
            List<ChessBoardGame> boardGames = boardDatabase.loadFinishedBoardGames(userId);
            
            for(ChessBoardGame boardGame : boardGames) {
               String gameId = boardGame.getGameId();
               ChessBoardResult boardResult = boardGame.getBoardResult();    
               
               if(boardResult != ChessBoardResult.NONE && boardResult != ChessBoardResult.REJECT) { // only show real results
                  if(alreadyDone.add(gameId)) {         
                     ChessResultHistoryItem historyRecord = new ChessResultHistoryItem(boardGame);
                     
                     alreadyDone.add(gameId);               
                     recentResults.add(historyRecord);
                  }
               }
            }  
         } else {
            List<ChessBoardGame> boardGames = boardDatabase.loadFinishedBoardGames(userId);
            
            for(ChessBoardGame boardGame : boardGames) {
               String gameId = boardGame.getGameId();
               long creationTime = boardGame.getCreationTime();
               long gameDuration = boardGame.getGameDuration();
               long expiryTime = creationTime + (gameDuration * 3);
               long currentTime = System.currentTimeMillis();
               
               if(expiryTime < currentTime) {
                  if(!alreadyDone.contains(gameId)) {         
                     boardDatabase.deleteBoardGame(boardGame); // is no longer in the history anyway
                  }
               }
            } 
         }
      } catch(Exception e) {
         LOG.info("Could not list recent results for " + userId, e);
      }
      if(!recentResults.isEmpty()) {
         Collections.sort(recentResults, resultComparator);   
      }
      return recentResults;
   }
}
