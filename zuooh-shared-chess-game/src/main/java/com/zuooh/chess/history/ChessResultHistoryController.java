package com.zuooh.chess.history;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.TemplateList;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.computer.ChessComputerMockPlayer;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.result.ChessResultDatabase;
import com.zuooh.chess.database.result.ChessResultRecord;
import com.zuooh.chess.database.theme.ChessThemeDatabase;
import com.zuooh.chess.database.theme.ChessThemeDefinition;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.chess.database.user.ChessUserFormatter;
import com.zuooh.chess.timer.ChessTimeDrawer;
import com.zuooh.style.Rectangle;
import com.zuooh.style.Style;

@Component
public class ChessResultHistoryController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessResultHistoryController.class);

   private final ChessDatabase gameDatabase;
   private final ChessBoardContext boardContext;
   private final ChessResultHistorySearcher resultSearcher;
   private final MobileApplication application;
   private final TemplateList boardList;
   private final Context countPanel;
   private final Screen screen;
   private final Style star;

   public ChessResultHistoryController(
         @Inject("application") MobileApplication application,
         @Inject("gameDatabase") ChessDatabase gameDatabase,         
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("countPanel") Context countPanel,           
         @Inject("results") TemplateList boardList, 
         @Inject("star") Style star)
   {
      this.resultSearcher = new ChessResultHistorySearcher(gameDatabase);
      this.screen = application.getScreen();
      this.boardContext = boardContext;
      this.gameDatabase = gameDatabase;
      this.application = application;
      this.boardList = boardList;
      this.countPanel = countPanel;
      this.star = star;
   }
   
   @OnCreate
   public void buildHistory() {
      try {
         ChessUser user = boardContext.getBoardUser();
         String userId = user.getKey();
         List<ChessResultHistoryItem> resultItems = resultSearcher.recentResults(userId);
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
         ChessResultDatabase resultDatabase = gameDatabase.getResultDatabase();         
         ChessUserDatabase profileDatabase = gameDatabase.getUserDatabase();     
         int entryCount = 0;
         int whiteCount = 0;
         int blackCount = 0;
              
         for(ChessResultHistoryItem resultItem : resultItems) {
            ChessBoardGame boardGame = resultItem.getBoardGame();
            ChessSide userColor = boardGame.getUserSide();
            String gameId = boardGame.getGameId();               
            String opponentId = boardGame.getOpponentId();
            ChessUser opponent = null;
            
            try {
               opponent = profileDatabase.loadUser(opponentId);
               
               if(opponent != null) {
                  String actualId = opponent.getKey();
                  
                  if(opponentId == null || !opponentId.equals(actualId)) {
                     LOG.warn("Profile entry for " + opponentId + " has key of " + actualId);
                     profileDatabase.deleteUser(opponent);
                     continue;                           
                  }
               }
            } catch(Exception e) {
               LOG.warn("Could not find opponent " + opponentId);
            }               
            if(opponentId != null && opponent != null) { // ensure the opponent is valid
               boolean gameOnline = boardGame.isOnlineGame();
   
               if(userColor == ChessSide.WHITE) {
                  whiteCount++;
               } else {
                  blackCount++;
               }
               if(entryCount > 10) {
                  try {
                     ChessResultRecord resultRecord = resultItem.getResultRecord();
                     
                     if(resultRecord != null) {
                        resultDatabase.deleteResult(resultRecord);
                     }
                     boardDatabase.deleteBoardGame(boardGame); // clean up games!!
                  } catch(Exception e) {
                     LOG.warn("Could not delete board game", e);
                  }
               } else {
                  String templateName = userColor.name + (gameOnline ? "Online" : "Computer");
                  Context context = boardList.addLast(gameId, templateName);
                  
                  if(context != null) {
                     drawProfile(context, opponent, resultItem, star);
                     entryCount++;
                  }
               }
            }
         }         
         if(countPanel != null) {
            Panel whitePanel = countPanel.getCanvas("whiteCount");
            Panel blackPanel = countPanel.getCanvas("blackCount");
            
            whitePanel.setText("x " + whiteCount);
            blackPanel.setText("x " + blackCount);
         }
         Screen screen = application.getScreen();
         Rectangle size = boardList.getRectangle();
         int fullHeight = screen.screenHeight();
         int remainingSpace = fullHeight - size.height;
         
         if(remainingSpace > 0) {
            boardList.addLast("border", "border");
   
            if(remainingSpace > 5) {
               if(entryCount > 0) {
                  boardList.addLast("filler", "filler", remainingSpace - 5);
               } else {
                  boardList.addLast("empty", "empty", remainingSpace - 5);
               }
            }
         } else {
            boardList.addLast("border", "border");
            boardList.addLast("empty", "empty", 20);            
         }
      } catch(Exception e) {
         throw new IllegalStateException("Could not start history controller", e);
      }
   }
   
   private void drawProfile(Context context, ChessUser user, ChessResultHistoryItem resultItem, Style style) {
      Panel trophyPanel = context.getCanvas("trophyPanel");      
      Panel resultPanel = context.getCanvas("gameResult");
      Panel creationTime = context.getCanvas("lastSeenOnline");
      Panel userPanel = context.getCanvas("playerName");
      Context skillContext = context.getContext("skillPanel");
      
      if(userPanel != null) {
         drawPlayerName(context, user, userPanel);
      }
      if(skillContext != null) {
         drawSkill(skillContext, user, style);
      }
      drawPlayerPoints(context, user);
      
      if(creationTime != null) {
         drawCreationTime(context, user, creationTime);
      }
      if(resultPanel != null) {
         drawResult(context, user, resultItem, resultPanel, trophyPanel);
      }
      ChessBoardGame boardGame = resultItem.getBoardGame();
      
      context.addAttribute("boardGame", boardGame);
      context.addAttribute("player", user);
   }
   
   private void drawResult(Context context, ChessUser profile, ChessResultHistoryItem resultItem, Panel resultPanel, Panel trophyPanel) {
      try {
         ChessBoardGame boardGame = resultItem.getBoardGame();
         ChessBoardResult boardResult = boardGame.getBoardResult();    
         String resultText = null;
         
         if(boardResult == ChessBoardResult.WIN) {
            if(trophyPanel != null) {
               trophyPanel.setStyleClass("winner");
            }
            resultText = "You won";
         } else if(boardResult == ChessBoardResult.LOSE) {
            resultText = "You lost";
         } else if(boardResult == ChessBoardResult.DRAW){
            resultText = "Draw";
         }
         if(resultText != null) {
            long currentTime = System.currentTimeMillis();
            long resultTime = resultItem.getResultTime();
            long resultAge = currentTime - resultTime;
            
            if(resultAge > 1000) {
               String lastSeenTime = ChessTimeDrawer.convertToTextTime(resultAge, true);
               String trimmedTime = lastSeenTime.trim();
               
               resultText += " " + trimmedTime + " ago";         
            }  else {
               resultText += " just now";
            }
            resultPanel.setText(resultText);
         }
      } catch(Exception e) {
         LOG.info("Could not generate result", e);
      }
   }

   private void drawCreationTime(Context context, ChessUser user, Panel creationTime) {
      ChessUserData userData = user.getData();
      long timeStamp = userData.getLastSeenOnline();
      long currentTime = System.currentTimeMillis();
      long timeSinceActive = currentTime - timeStamp;
      long oneDay = 1000 * 60 * 60 * 24;
      
      if(timeSinceActive >= oneDay) {
         creationTime.setText("Not active today");
      } else if(timeSinceActive > 1000) {
         String lastSeenTime = ChessTimeDrawer.convertToTextTime(timeSinceActive);
         String timeTrimmed = lastSeenTime.trim();

         creationTime.setText("Active " + timeTrimmed + " ago");
      } else {
         creationTime.setText("Currently active");
      }
   }

   private void drawPlayerRank(Context context, ChessUser user, Panel playerRank) {
      ChessUserData userData = user.getData();
      int rank = userData.getRank();
      playerRank.setText("Rank " + rank);      
   }
   
   private void drawPlayerPoints(Context context, ChessUser profile) {
     // Panel playerPoints = context.getCanvas("playerPoints");
    //  int opponentPoints = profile.getPlayerPoints();

      //playerPoints.setText(String.valueOf(opponentPoints));
   }

   private void drawPlayerName(Context context,  ChessUser user, Panel playerPanel) {      
      String playerId = user.getKey();
      String playerName = ChessUserFormatter.formatUser(user);
      
      if(playerName != null) {
         playerName = playerName.trim();
         playerPanel.setText(playerName);         
      } else {
         playerPanel.setText(playerId);
      }
   }

   private void drawSkill(Context context,  ChessUser user, Style style) {
      ChessUserData userData = user.getData();
      int opponentSkillLevel = userData.getSkill();

      for(int i = 1; i <= opponentSkillLevel; i++) {
         Panel starPanel = context.getCanvas("star" + i);
         
         if(starPanel != null) {
            starPanel.setStyle(style);
         }
      }
   }

   @OnCall
   public void showGame(ChessBoardGame boardGame, ChessUser opponent) {
      try {
         String gameId = boardGame.getGameId();
         String themeColor = boardGame.getThemeColor();
         ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();
         ChessThemeDefinition themeDefinition = themeDatabase.loadThemeDefinition(themeColor);
         ChessSide userColor = boardGame.getUserSide();
         ChessSide opponentColor = userColor.oppositeSide();
         ChessBoardStatus boardStatus = boardGame.getBoardStatus();
         String screenName = themeDefinition.getScreen(userColor, boardStatus);
         
         boardContext.setThemeColor(themeColor);
         boardContext.setGameId(gameId);
         boardContext.setOpponentSide(opponentColor);

         if(opponent != null) {
            ChessPlayer opponentPlayer = new ChessComputerMockPlayer(opponentColor, opponent, gameId);
            boardContext.setOpponent(opponentPlayer);
         }
         screen.showPage(screenName);
      }catch(Exception e) {
         LOG.info("Could not start a specific game", e);
      }
   }
}
