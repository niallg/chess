package com.zuooh.chess.tray;

import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.speech.ChessChatSubscriptionController;
import com.zuooh.chess.timer.ChessTimer;

@Component
public class ChessTouchController {

   private final ChessChatSubscriptionController subscriptionController;
   private final ChessTimer chessTimer;

   public ChessTouchController(
         @Inject("chatSubscriptionController") ChessChatSubscriptionController subscriptionController,
         @Inject("chessTimer") ChessTimer chessTimer)
   {
      this.subscriptionController = subscriptionController;
      this.chessTimer = chessTimer;
   }
  
   @OnCall
   public void touchPlayer(String color) throws Exception {
      if(!subscriptionController.showMail()) {
         chessTimer.showPlayer(color);
      }
   }
   
}
