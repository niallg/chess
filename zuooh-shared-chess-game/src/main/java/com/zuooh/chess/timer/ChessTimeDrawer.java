package com.zuooh.chess.timer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.canvas.IconList;
import com.zuooh.application.canvas.TextIcon;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessBoardMatch;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.rank.ChessRankDialogBuilder;

public class ChessTimeDrawer {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessTimeDrawer.class);

   private final ChessGameManager gameManager;
   private final ChessBoardContext boardContext;
   private final ChessTimeCalculator timeCalculator;
   private final ChessBoardMatch boardMatch;
   private final ChessDatabase gameDatabase;
   private final IconList whiteIcons;
   private final IconList blackIcons;
   private final Window window;
   private final Screen screen;

   public ChessTimeDrawer(MobileApplication application, ChessBoardContext boardContext, ChessDatabase gameDatabase, ChessGameManager gameManager, IconList whiteIcons, IconList blackIcons) {
      this.boardMatch = new ChessBoardMatch(boardContext, gameDatabase);
      this.timeCalculator = new ChessTimeCalculator(gameDatabase);
      this.window = application.getWindow();
      this.screen = application.getScreen();
      this.gameDatabase = gameDatabase;
      this.boardContext = boardContext;
      this.gameManager = gameManager;
      this.whiteIcons = whiteIcons;
      this.blackIcons = blackIcons;
   }
   
   public ChessTimeCalculator getCalculator(){
      return timeCalculator;
   }

   public void drawTimer(ChessBoardGame boardGame) throws Exception {
      if(blackIcons != null && whiteIcons != null) {
         TextIcon blackTimer = blackIcons.getIcon("blackTimer");
         TextIcon whiteTimer = whiteIcons.getIcon("whiteTimer");
      
         if(blackTimer != null && whiteTimer != null) {
            long blackTime = calculateTimeRemaining(boardGame, ChessSide.BLACK);
            long whiteTime = calculateTimeRemaining(boardGame, ChessSide.WHITE);
            String whiteTimeText = convertToTime(whiteTime);              
            String blackTimeText = convertToTime(blackTime);          
            
            blackTimer.setVisible(true);
            whiteTimer.setVisible(true);
            
            if(whiteTimeText != null) {
               whiteTimer.setText(whiteTimeText);
            }
            if(blackTimeText != null) {
               blackTimer.setText(blackTimeText);
            }
            window.invalidate();
         }
      }    
   }
   
   public long calculateTimeRemaining(ChessBoardGame boardGame, ChessSide playDirection) throws Exception {
      String gameId = boardContext.getGameId();
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      
      if(boardGame != null) { // why is this null?
         ChessBoardStatus boardStatus = boardGame.getBoardStatus();
         
         if(boardStatus.isTimeOut()) {
            ChessHistoryItem lastMoveItem = historyDatabase.loadLastHistoryItem(gameId);
            ChessMove lastMove = lastMoveItem.getMove();
            ChessSide lastSide = lastMove.getSide();
            
            if(lastSide != playDirection) {
               return 0;
            }
         }
         boolean gameIsOver = boardStatus.isGameOver();
         long gameDuration = boardGame.getGameDuration();
         long timeElapsed = timeCalculator.calculateElapsedTime(gameId, playDirection, !gameIsOver);            
         long playerDuration = gameDuration - timeElapsed;               
         
         return playerDuration - 1;
      }
      return 0;
   }  
  
   public void showPlayer(String color) {        
      try{
         ChessUser user = boardMatch.matchPlayer(color);       
         
         if(user != null) {
            ChessUser currentUser = gameManager.currentUser();
            String currentUserId = currentUser.getKey();
            String userId = user.getKey();
            
            if(currentUserId.equals(userId)) {
               ChessRankDialogBuilder.showProfile(screen, user, "#66ffffff", "dialog.hideDialog()", false, true);
            } else {
               ChessRankDialogBuilder.showProfile(screen, user, "#66ffffff", "dialog.hideDialog()", false, false);
            }
         }
      } catch(Exception e) {
         LOG.info("Could nto show profile", e);
      }      
   }
   
   
   public static String convertToTextTime(long durationInMillis) {
      return convertToTextTime(durationInMillis, false);
   }
   
   public static String convertToTextTime(long durationInMillis, boolean shortTime) {
      StringBuilder builder = new StringBuilder();
      long seconds = (durationInMillis / 1000) % 60;
      long minutes = (durationInMillis / 60000) % 60;
      long hours = (durationInMillis / 3600000) % 24;
      long days = (durationInMillis / 86400000);

      if (days > 0) {
         builder.append(days);

         if(days != 1) {
            builder.append(" days ");
         } else {
            builder.append(" day ");
         }
         if(shortTime) {
            return builder.toString();
         }
      }
      if (hours > 0) {
         builder.append(hours);

         if(hours != 1) {
            builder.append(" hours ");
         } else {
            builder.append(" hour ");
         }
         if(shortTime) {
            return builder.toString();
         }
         if(days > 0) {
            return builder.toString();
         }
      }
      if (minutes > 0) {
         builder.append(minutes);

         if(minutes != 1) {
            builder.append(" minutes ");
         } else {
            builder.append(" minute ");
         }
         if(shortTime) {
            return builder.toString();
         }
         if(days > 0 || hours > 0) {
            return builder.toString();
         }
      }
      if(seconds > 0) {
         builder.append(seconds);

         if(seconds != 1) {
            builder.append(" seconds ");
         } else {
            builder.append(" second ");
         }
      }
      return builder.toString();
   }

   public static String convertToTime(long durationInMillis) {
      StringBuilder builder = new StringBuilder();

      if(durationInMillis <= 0) {
         return "00:00";
      }
      long seconds = (durationInMillis / 1000) % 60;
      long minutes = (durationInMillis / 60000) % 60;
      long hours = (durationInMillis / 3600000) % 24;
      long days = (durationInMillis / 86400000);

      //if (days > 0) {
      //   builder.append(days);
      //   builder.append(" days ");
      //}
      if (days > 0 || hours > 0) {
         //if(hours <= 9) {
         //   builder.append("0");
        // }
         builder.append(hours);
         builder.append(":");
      }
      //if (days > 0 || hours > 0 || minutes > 0) {
         if(minutes <= 9) {
            builder.append("0");
         }
         builder.append(minutes);
         builder.append(":");
      //}
      if(seconds <= 9) {
         builder.append("0");
      }
      builder.append(seconds);
      return builder.toString();
   }
}
