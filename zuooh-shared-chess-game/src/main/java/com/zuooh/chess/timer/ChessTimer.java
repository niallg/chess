package com.zuooh.chess.timer;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.canvas.IconList;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.task.ChessTaskController;

@Component
public class ChessTimer extends ChessTaskController {

   private final ChessTimeDrawer timeDrawer;
   private final ChessBoardContext boardContext;
   private final ChessDatabase gameDatabase;  

   public ChessTimer(
         @Inject("application") MobileApplication application,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("gameManager") ChessGameManager gameManager,       
         @Inject("whiteIcons") IconList whiteIcons,
         @Inject("blackIcons") IconList blackIcons)
   {
      this.timeDrawer = new ChessTimeDrawer(application, boardContext, gameDatabase, gameManager, whiteIcons, blackIcons);
      this.gameDatabase = gameDatabase;
      this.boardContext = boardContext;
   }

   @Override
   public void onBeforeStart() {}

   @Override
   public long onInterval() throws Exception {
      long startTime = System.currentTimeMillis();   
      String gameId = boardContext.getGameId();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      
      if(boardGame != null) {
         timeDrawer.drawTimer(boardGame);
      }
      long finishTime = System.currentTimeMillis();
      long timeElapsed = finishTime - startTime;
      
      if(timeElapsed < 1000) {
         return 1000 - timeElapsed; 
      }
      return 10; // schedule straight away 
   }
   
   @OnCall
   public void showPlayer(String color) {
      timeDrawer.showPlayer(color);
   }   
   
   public ChessTimeDrawer getDrawer() {
      return timeDrawer;
   }   
}
