package com.zuooh.chess.timer;

import java.util.Iterator;
import java.util.List;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;

public class ChessTimeCalculator {
   
   private final ChessDatabase database;
   
   public ChessTimeCalculator(ChessDatabase database) {
      this.database = database;
   }
   
   public long calculateElapsedTime(String gameId, ChessSide side) throws Exception {
      return calculateElapsedTime(gameId, side, true);
   }
   
   public long calculateElapsedTime(String gameId, ChessSide side, boolean timerActive) throws Exception {
      ChessHistoryDatabase historyDatabase = database.getHistoryDatabase();
      List<ChessHistoryItem> items = historyDatabase.loadAllHistoryItems(gameId);
      Iterator<ChessHistoryItem> iterator = items.iterator();
      long historicalTimeTaken = 0; 
      
      if(iterator.hasNext()) {
         ChessHistoryItem firstMoveMade = iterator.next(); // time starts on the second move
         ChessHistoryItem previousMoveMade = null;      
         
         if(firstMoveMade != null) {
            while(iterator.hasNext()) {
               ChessHistoryItem nextMoveMade = iterator.next();
               
               if(nextMoveMade.chessMove.side == side) { // was this move made by player
                  if(previousMoveMade != null) {
                     if(previousMoveMade.chessMove.side == side) {
                        throw new IllegalStateException("Time could not be calculated move made out of turn for " + gameId);
                     }
                     historicalTimeTaken += nextMoveMade.timeStamp - previousMoveMade.timeStamp;
                  }
               }
               previousMoveMade = nextMoveMade;
            }
            if(previousMoveMade != null && timerActive) {
               if(previousMoveMade.chessMove.side != side) {
                  long currentTime = System.currentTimeMillis();
                  long currentTimeTaken = currentTime - previousMoveMade.timeStamp;
                  
                  return currentTimeTaken + historicalTimeTaken;
               }
            }
         }
      }
      return historicalTimeTaken;
   }
}
