package com.zuooh.chess.status;

import com.zuooh.application.canvas.Panel;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.task.ChessTaskController;

@Component
public class ChessStatusPanel extends ChessTaskController {
   
   private final ChessSide opponentSide;
   private final ChessDatabase gameDatabase;
   private final ChessPlayer opponentPlayer;
   private final Panel throbber;
   private final Panel status;
   private final String gameId;

   public ChessStatusPanel(
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("throbber") Panel throbber,
         @Inject("status") Panel status)
   {
      this.opponentPlayer = boardContext.getOpponent();
      this.opponentSide = opponentPlayer.getPlayerSide();
      this.gameId = opponentPlayer.getAssociatedGame();
      this.gameDatabase = gameDatabase;
      this.throbber = throbber;
      this.status = status;
   }   

   @Override
   public synchronized long onInterval() throws Exception {
      ChessUser opponent = opponentPlayer.getUser();
      
      if(status != null) {
         if(opponentPlayer.isOnline()) {
            ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
            ChessHistoryItem lastWhiteMove = historyDatabase.loadLastHistoryItem(gameId, ChessSide.WHITE);
            ChessHistoryItem lastBlackMove = historyDatabase.loadLastHistoryItem(gameId, ChessSide.BLACK);             
            ChessHistoryItem lastOpponentMove = lastWhiteMove;
            ChessHistoryItem lastMove = lastBlackMove;         
            
            if(opponentSide == ChessSide.BLACK) {
               lastOpponentMove = lastBlackMove;
               lastMove = lastWhiteMove;
            }
            if(lastOpponentMove == null) {
               if(opponent == null) {                          
                  status.setText("Waiting for an opponent");
                  throbber.setVisible(true);               
                  status.setVisible(true);                
               } else {
                  if(lastMove == null && opponentSide == ChessSide.BLACK) {
                     status.setText("You have the first move");
                     status.setVisible(true);            
                     throbber.setVisible(true);
                  } else {
                     status.setText("Waiting for opponent to move");
                     status.setVisible(true);            
                     throbber.setVisible(true);
                  }
               }
            } else {
               if(lastMove == null) {
                  status.setText("Time starts on your move");
                  status.setVisible(true);            
                  throbber.setVisible(true);
               } else {
                  throbber.setVisible(false);
                  status.setVisible(false);
                  status.setText("");
                  return -1; // both opponent and user have moved
               }
            }
         }
         return 1000;
      }
      return -1;
   }
}
