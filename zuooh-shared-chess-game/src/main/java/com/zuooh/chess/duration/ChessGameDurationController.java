package com.zuooh.chess.duration;


import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Dialog;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.bind.annotation.OnMenu;
import com.zuooh.application.canvas.Deck;
import com.zuooh.application.store.Inventory;
import com.zuooh.application.store.ProductStore;
import com.zuooh.application.store.PurchaseStatus;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessGameQuota;
import com.zuooh.chess.control.state.ChessUpgradeProduct;
import com.zuooh.chess.select.ChessGameValidationController;

@Component
public class ChessGameDurationController {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessGameDurationController.class);

   private final  ChessGameValidationController validator;
   private final MobileApplication application;
   private final ChessBoardContext boardContext;
   private final AtomicBoolean showDialog;
   private final Deck buttonDeck;
   private final Screen screen;

   public ChessGameDurationController(
         @Inject("validationController") ChessGameValidationController validator,
         @Inject("boardContext") ChessBoardContext boardContext,         
         @Inject("application") MobileApplication application,
         @Inject("restrictedButtons") Deck buttonDeck)
   {
      this.showDialog = new AtomicBoolean();
      this.screen = application.getScreen();
      this.buttonDeck = buttonDeck;
      this.application = application;
      this.boardContext = boardContext;
      this.validator = validator;
   }  
   
   @OnCreate
   public void onCreate() {
      ChessUpgradeProduct product = boardContext.getUpgradeProduct();
      String productId = product.getProductId();
      ChessGameQuota quota = product.getUpgradeQuota();
      
      if(!product.isUpgraded()) {
         ProductStore store = application.getProductStore();
         Inventory inventory = store.loadInventory();
         PurchaseStatus status = inventory.getPurchaseStatus(productId);
         
         if(status.isPurchased()) {
            showDialog.set(true); // we have made purchase, so dialog must be shown
            product.setUpgraded(true); // ensure not to check again!!
         }
         if(product.isUpgraded()) {
            quota = product.getUpgradeQuota();
         } else {
            quota = product.getFreeQuota();
         }
      }
      buttonDeck.setTop("available"); // default to available!!
      
      try {
         int eightHours = 8 * 60 * 60 * 1000;
         int longGames = validator.countOnlineGamesToResume(eightHours); // greater or equal to eight hours
         int longGamesAllowed = quota.getLongGames();
         
         if(longGames >= longGamesAllowed) {
            buttonDeck.setTop("locked");
         } else {
            buttonDeck.setTop("available"); // default to available!!
         }
      } catch(Exception e) {
         LOG.info("Could not determine online games", e);
      }
   }
      

   @OnMenu
   public void showMenu() {
      screen.showPage("home");
   }

   @OnCall
   public void showUpgrade() {
      if(showDialog.get()) {
         Dialog dialog = screen.openDialog("warningDialog");
         
         dialog.setAttribute("warningTitle", "Quota");
         dialog.setAttribute("warningMessage", "You have exceeded your quota of long games!");
         dialog.setAttribute("warningFunction", "dialog.hideDialog()");
         dialog.setAttribute("warningButton", "Okay");
         dialog.showDialog();
      } else {
         boardContext.setUpgradePurchasePage("upgradeMoreTime");
         boardContext.setUpgradeFinishPage("durationOfGame");
         screen.showPage("upgrade");
      }
   }

   @OnCall
   public void gameDuration(int minutes, int hours, int days) {
      long duration = (minutes * 60 * 1000) + (hours * 60 * 60 * 1000) + (days * 24 * 60 * 60 * 1000);
      ChessSide opponentColor = boardContext.getOpponentSide();

      boardContext.setGameDuration(duration);
      boardContext.setUpgradePurchasePage(null);
      boardContext.setUpgradeFinishPage(null);

      if(boardContext.isGameRandom() && boardContext.isGameOnline()) {
         screen.showPage("matchGame");
      } else {
         if(opponentColor == ChessSide.WHITE) {
            screen.showPage("themeFlip");
         } else {
            screen.showPage("theme");
         }
      }
   }
}
