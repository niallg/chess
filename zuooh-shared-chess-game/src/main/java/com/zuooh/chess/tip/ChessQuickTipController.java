package com.zuooh.chess.tip;


import com.zuooh.application.Window;
import com.zuooh.application.canvas.Canvas;
import com.zuooh.application.canvas.info.tooltip.TooltipBubbleCreator;
import com.zuooh.application.canvas.info.tooltip.TooltipBubbleDrawer;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.style.Font;
import com.zuooh.style.Rectangle;
import com.zuooh.style.Style;

@Component
public class ChessQuickTipController {

   private final TooltipBubbleCreator builder;
   private final Window window;

   public ChessQuickTipController(
         @Inject("tooltipBubble") TooltipBubbleCreator builder,
         @Inject("window") Window window)
   {
      this.window = window;
      this.builder = builder;
   }

   public void tooltip(Context context, Canvas panel, Rectangle bounds, Style style, String text) throws Exception {
      Rectangle source = panel.getRectangle();
      Font font = style.getFont();
      TooltipBubbleDrawer drawer = builder.createDrawer(3);
      Canvas canvas = drawer.drawBubble(bounds, text, font, source);

      window.addForeground(canvas, 5000);
   }
}
