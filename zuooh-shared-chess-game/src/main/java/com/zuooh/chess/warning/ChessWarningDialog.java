package com.zuooh.chess.warning;

import com.zuooh.application.Sound;
import com.zuooh.application.Window;
import com.zuooh.application.canvas.Frame;

public class ChessWarningDialog {

   private final Window window;
   private final Frame frame;
   private final Sound sound;
   
   public ChessWarningDialog(Window window, Frame frame) {
      this(window, frame, null);
   }
   
   public ChessWarningDialog(Window window, Frame frame, Sound sound) {
      this.window = window;
      this.frame = frame;
      this.sound = sound;
   }
   
   public void addParameter(String name, String value) {
      frame.addAttribute(name, value);
   }

   public void showDialog() {
      showDialog(Long.MAX_VALUE);
   }
   
   public void showDialog(long duration) {      
      window.resetForeground();         
      window.addForeground(frame, duration);
      window.invalidate();
      
      if(sound != null) {
         sound.play(1.0f);
      }
   }
   
   public void hideDialog() {
      window.removeForeground(frame);
   }
}
