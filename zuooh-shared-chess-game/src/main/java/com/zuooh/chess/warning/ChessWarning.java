package com.zuooh.chess.warning;

public enum ChessWarning {
   WARNING,
   ERROR,
   INFO;
}
