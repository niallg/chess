package com.zuooh.chess.warning;

import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Sound;
import com.zuooh.application.SoundManager;
import com.zuooh.application.Tile;
import com.zuooh.application.Window;
import com.zuooh.application.canvas.Frame;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;

@Component
public class ChessWarningBuilder {

   private static final Logger LOG = LoggerFactory.getLogger(ChessWarningBuilder.class);
   
   private final Map<ChessWarning, String> warningTiles;
   private final Map<ChessWarning, String> warningSounds;
   private final MobileApplication application;
   private final Document document;
   private final Window window;

   public ChessWarningBuilder(
         @Inject("application") MobileApplication application,
         @Inject("document") Document document,
         @Inject("window") Window window,
         @Inject("warningTiles") Map<ChessWarning, String> warningTiles,
         @Inject("warningSounds") Map<ChessWarning, String> warningSounds)
   {
      this.warningTiles = warningTiles;
      this.warningSounds = warningSounds;
      this.application = application;
      this.document = document;
      this.window = window;
   }

   public ChessWarningDialog createWarning(ChessWarning warning) {
      try {
         String soundName = warningSounds.get(warning);
         SoundManager soundManager = application.getSoundManager();
         Sound sound = soundManager.getSound(soundName);
         String tileName = warningTiles.get(warning);
         Tile message = document.getTile(tileName);
         Frame messagePanel = message.createFrame();

         messagePanel.setFullScreen(true);
         messagePanel.setVisible(true);
         messagePanel.setDirty(true);
         
         return new ChessWarningDialog(window, messagePanel, sound);
      } catch(Exception e) {
         LOG.info("Could not display dialog", e);
      }
      return null;
   }
}
