package com.zuooh.chess;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.ChessGameController;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.user.ChessUser;

public class ChessGameManager {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessGameManager.class);

   private final ChessGamePlayerManager gameLoader;
   private final ChessGameController gameController;
   private final ChessDatabase gameDatabase;

   public ChessGameManager(ChessGameController gameController, ChessDatabase gameDatabase, ChessGamePlayerManager gameLoader) {
      this.gameController = gameController;
      this.gameDatabase = gameDatabase;
      this.gameLoader = gameLoader;
   }   
   
   public ChessUser currentUser() {
      return gameController.currentUser();
   }

   public Map<String, ChessGame> currentOnlineGames() {
      Map<String, ChessGame> gameMap = new HashMap<String, ChessGame>();
      
      try {
         List<ChessGame> onlineGames = gameController.statusUpdate();

         for(ChessGame challengeGame : onlineGames) {
            String gameId = challengeGame.getGameId();
            gameMap.put(gameId, challengeGame);
         }
      } catch(Exception e) {
         LOG.info("Could not acquire online games list", e);
      }
      return gameMap;
   }

   public Map<String, ChessGame> availableOnlineGames() {
      Map<String, ChessGame> gameMap = new HashMap<String, ChessGame>();
      
      try {
         List<ChessGame> onlineGames = gameController.availableGames();

         for(ChessGame challengeGame : onlineGames) {
            String gameId = challengeGame.getGameId();
            gameMap.put(gameId, challengeGame);
         }
      } catch(Exception e) {
         LOG.info("Could not acquire online games list", e);
      }
      return gameMap;
   }

   public ChessPlayer acceptOnlineGame(String gameId) {
      try {
         return gameController.acceptGame(gameId);
      } catch(Exception e) {
         LOG.info("Could not reject game " + gameId, e);
      }
      return null;
   }
   
   public void rejectOnlineGame(String gameId) {
      try {
         gameController.rejectGame(gameId);
      } catch(Exception e) {
         LOG.info("Could not reject game " + gameId, e);
      }
   }   

   public ChessPlayer loadGame(String gameId) {
      try {
         return gameLoader.loadChessPlayer(gameId);
      } catch(Exception e) {
         LOG.info("Could not load game " + gameId, e);
      }
      return null;
   }

   public void deleteGame(String gameId) {
      try {
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
         
         historyDatabase.deleteAllHistoryItems(gameId);
         boardDatabase.deleteBoardGame(boardGame);
      } catch(Exception e) {
         LOG.info("Could not delete board", e);
      }
   }
}
