package com.zuooh.chess.computer;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessComputerPlayer implements ChessPlayer {

   private final ChessMoveEngine chessEngine;
   private final ChessUser profile;
   private final String gameId;

   public ChessComputerPlayer(ChessMoveEngine chessEngine, ChessUser profile, String gameId) {
      this.chessEngine = chessEngine;
      this.profile = profile;
      this.gameId = gameId;
   }

   @Override
   public String getAssociatedGame() {
      return gameId;
   }

   @Override
   public ChessSide getPlayerSide() {
      return chessEngine.getSide();
   }

   @Override
   public ChessOpponentStatus getOpponentStatus() {
      return ChessOpponentStatus.ONLINE;
   }

   @Override
   public ChessMoveEngine getMoveEngine() {
      return chessEngine;
   }

   @Override
   public ChessUser getUser() {
      return profile;
   }

   @Override
   public boolean isOnline() {
      return false;
   }
}
