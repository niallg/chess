package com.zuooh.chess.computer;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessOpponentStatus;
import com.zuooh.chess.engine.ChessNoMoveEngine;

public class ChessComputerMockPlayer implements ChessPlayer {

   private final ChessMoveEngine chessEngine;
   private final ChessUser user;
   private final String gameId;

   public ChessComputerMockPlayer(ChessSide side, ChessUser user, String gameId) {
      this.chessEngine = new ChessNoMoveEngine(side);
      this.gameId = gameId;
      this.user = user;
   }

   @Override
   public String getAssociatedGame() {
      return gameId;
   }

   @Override
   public ChessSide getPlayerSide() {
      return chessEngine.getSide();
   }

   @Override
   public ChessOpponentStatus getOpponentStatus() {
      return ChessOpponentStatus.ONLINE;
   }

   @Override
   public ChessMoveEngine getMoveEngine() {
      return chessEngine;
   }

   @Override
   public ChessUser getUser() {
      return user;
   }

   @Override
   public boolean isOnline() {
      return false;
   }
}
