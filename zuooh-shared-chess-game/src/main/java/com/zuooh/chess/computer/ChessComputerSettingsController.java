package com.zuooh.chess.computer;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnMenu;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;

@Component
public class ChessComputerSettingsController {

   private final ChessDatabase gameDatabase;
   private final ChessBoardContext boardContext;
   private final Screen screen;

   public ChessComputerSettingsController(
         @Inject("application") MobileApplication application,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase)
   {
      this.screen = application.getScreen();
      this.boardContext = boardContext;
      this.gameDatabase = gameDatabase;
   }

   @OnMenu
   public void showMenu() {
      screen.showPage("home");
   }

   @OnCall
   public void selectOpponent(String opponentName) throws Exception {
      String gameId = boardContext.getGameId();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      
      boardGame.setOnlineGame(false);
      boardGame.setOpponentId(opponentName);
      boardContext.setGameOnline(false);
      boardDatabase.saveBoardGame(boardGame);
      screen.showPage("durationOfGame");
   }
}
