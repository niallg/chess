package com.zuooh.chess;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.common.time.DateTime;
import com.zuooh.container.annotation.ManagedAttribute;
import com.zuooh.container.annotation.ManagedOperation;
import com.zuooh.container.annotation.ManagedOperationParameter;
import com.zuooh.container.annotation.ManagedOperationParameters;
import com.zuooh.container.annotation.ManagedResource;

@ManagedResource("Monitor for existing games")
public class ChessGameMonitor {

   private final ChessSearchTracer searchTracer;
   private final ChessGameManager gameManager;
   private final ChessDatabase gameDatabase;
   
   public ChessGameMonitor(ChessGameManager gameManager, ChessDatabase gameDatabase, ChessSearchTracer searchTracer) {
      this.searchTracer = searchTracer;
      this.gameManager = gameManager;
      this.gameDatabase = gameDatabase;
   }
   
   @ManagedAttribute("Determine if search tracing is enabled")
   public boolean isTracingEnabled() {
      return searchTracer.isTracingEnabled();
   }   
   
   @ManagedOperation("Enable or disable search tracing")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="enabled", description="Enable or disable tracing")
   })
   public void setTracingEnabled(boolean enabled) {
      searchTracer.setTracingEnabled(enabled);
   }   
   
   @ManagedOperation("Show board games")   
   public String showBoardGames() throws Exception {
      StringBuilder builder = new StringBuilder();
      
      builder.append("<table border='1'>");
      builder.append("<th>gameId</th>");
      builder.append("<th>boardStatus</th>");
      builder.append("<th>playerColor</th>");
      builder.append("<th>playerId</th>");      
      builder.append("<th>opponentId</th>");
      
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessUser user = gameManager.currentUser();
      String userId = user.getKey();    
      List<ChessBoardGame> boardGames = boardDatabase.loadBoardGames(userId);
      
      for(ChessBoardGame boardGame :  boardGames) {
         String gameId = boardGame.getGameId();
         String opponentId = boardGame.getOpponentId(); 
         ChessSide playerColor = boardGame.getUserSide();
         ChessBoardStatus boardStatus = boardGame.getBoardStatus();
            
         builder.append("<tr>");
         builder.append("<td>");
         
         if(boardDatabase.containsBoardGame(gameId)) {
            builder.append("<a href='operationInvoked?component=ChessGameMonitor&operation=showBoardGame&signature=1&gameId=");
            builder.append(gameId);
            builder.append("'>");
            builder.append(gameId);
            builder.append("</a>");
         } else {
            builder.append(gameId);
         }
         builder.append("</td>");
         builder.append("<td>").append(boardStatus).append("</td>");
         builder.append("<td>").append(playerColor).append("</td>");
         builder.append("<td>").append(userId).append("</td>");
         builder.append("<td>").append(opponentId).append("</td>");
         builder.append("</tr>");
                  
      }
      builder.append("</table>");
      return builder.toString();
   }
   
   @ManagedOperation("Show board game")
   @ManagedOperationParameters({
      @ManagedOperationParameter(name="gameId", description="Show current board")
   })   
   public String showBoardGame(String gameId) throws Exception {
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      
      if(boardDatabase.containsBoardGame(gameId)) {
         ChessUserDatabase userDatabase = gameDatabase.getUserDatabase();
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
         ChessBoard chessBoard = historyDatabase.loadHistoryAsBoard(gameId);
         String opponentId = boardGame.getOpponentId();
         ChessUser opponent = userDatabase.loadUser(opponentId);
         String chessBoardText = ChessTextBoardDrawer.drawBoard(chessBoard);
         String opponentName = opponent.getName();
         ChessSide playerColor = boardGame.getUserSide();
         ChessSide opponentColor = playerColor.oppositeSide();
         List<ChessHistoryItem> historyItems = historyDatabase.loadAllHistoryItems(gameId);
         
         if(!historyItems.isEmpty()) {
            List<ChessHistoryItem> historyInReverse = new ArrayList<ChessHistoryItem>(historyItems);
            StringBuilder builder = new StringBuilder();
            
            builder.append("<h2>");
            builder.append(opponentName);
            builder.append(" (");
            builder.append(opponentColor);
            builder.append(")");
            builder.append("</h2>");
            builder.append("<table border='1'>");
            builder.append("<th>board</th>");
            builder.append("<th>history</th>");
            builder.append("<th>text</th>");            
            builder.append("<tr valign='top'>");
            builder.append("<td>");
            builder.append("<pre>");
            builder.append(chessBoardText);
            builder.append("<pre>");
            builder.append("</td>");
            builder.append("<td>");
            builder.append("<table border='1'>");
            
            Collections.reverse(historyInReverse);
      
            for (ChessHistoryItem historyItem : historyInReverse) {
               DateTime dateTime = DateTime.at(historyItem.timeStamp);
               ChessMove chessMove = historyItem.getMove();
               ChessSide moveDirection = chessMove.side;
      
               if (moveDirection == ChessSide.WHITE) {
                  builder.append("<tr bgcolor='#ffffff'>");
               } else {
                  builder.append("<tr bgcolor='#cccccc'>");
               }
               builder.append("<td>").append(chessMove).append("</td>");
               builder.append("<td>").append(moveDirection).append("</td>");
               builder.append("<td>").append(chessMove.change).append("</td>");               
               builder.append("<td>").append(dateTime).append("</td>");
               builder.append("<tr>");
            }
            
            builder.append("</table>");
            builder.append("</td>");
            builder.append("<td>");
            builder.append("<pre>");
      
            for (ChessHistoryItem historyItem : historyInReverse) {
               ChessMove chessMove = historyItem.getMove();              
               String moveText = String.format("%s:%s->%s\n", chessMove.side, chessMove.from, chessMove.to);
               String moveToken = moveText.toLowerCase();
               
               builder.append(moveToken);
            }            
            builder.append("</pre>");
            builder.append("</td>");
            builder.append("</tr>");
            builder.append("</table>");
      
            return builder.toString();
         }
      }
      return null;
   }   
}
