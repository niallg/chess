package com.zuooh.chess.splash;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.load.ResourceProgress;
import com.zuooh.application.canvas.ProgressBar;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.task.ChessTaskController;

@Component
public class ChessSplashProgressBar extends ChessTaskController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessSplashProgressBar.class);

   private final Set<String> descriptionsDone;
   private final AtomicInteger intervalCount;
   private final ProgressBar progressBar;
   private final ResourceProgress progress;
   private final AtomicBoolean finished;
   private final Window window;
   private final Screen screen;

   public ChessSplashProgressBar(
         @Inject("application") MobileApplication application,
         @Inject("window") Window window,
         @Inject("progressBar") ProgressBar progressBar,
         @Inject("pageProgress") ResourceProgress progress)
   {
      this.descriptionsDone = new CopyOnWriteArraySet<String>();
      this.intervalCount = new AtomicInteger();
      this.finished = new AtomicBoolean();
      this.screen = application.getScreen();
      this.progressBar = progressBar;
      this.progress = progress;
      this.window = window;
   }

   @Override
   public void onBeforeStart() throws Exception {
      LOG.info("Before start of splash screen");
   }

   @Override
   public long onInterval() throws Exception {
      if(finished.get()) {
         onStop();
      }
      float percentage = progress.getPercentage();
      float intervals = intervalCount.getAndIncrement();
      float minimumPercentage = (intervals / 10) * 100f;
      float progressPercentage = Math.min(minimumPercentage, percentage); // here we force longest wait!!!
      float roundedPercentage = Math.round(progressPercentage);

      if(roundedPercentage >= 90) {         
         String page = progressBar.getLandingPage();
         
         screen.showPage(page);
         finished.set(true);
         onStop();
      } else {
         Set<String> progressHistory = progress.getHistory();
         String currentMessage = progress.getCurrentDescription();
         String displayMessage = currentMessage;

         for(String progressMessage : progressHistory) {
            if(!descriptionsDone.contains(progressMessage)) {
               descriptionsDone.add(progressMessage);
               displayMessage = progressMessage;
               break;
            }
         }
         progressBar.setPercentage(roundedPercentage);
         progressBar.setText(displayMessage);
         window.invalidate();
      }
      if(percentage > 90) {
         return 100; // go superfast if all done
      }
      if(minimumPercentage < percentage) { 
         return 200; // go faster if pages already loaded
      }
      return 500;
   }
}
