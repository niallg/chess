package com.zuooh.chess.splash;


import com.zuooh.application.MobileApplication;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.common.task.TaskScheduler;

@Component
public class ChessSplashRedirectController {

   @Inject("application")
   private MobileApplication application;
   
   @OnCreate
   public void redirect() {
      TaskScheduler scheduler = application.getTaskScheduler();
      scheduler.scheduleRunnable(new Runnable() {
         @Override
         public void run() {
            application.getScreen().showPage("splash");            
         }         
      }, 6000);
   }
}
