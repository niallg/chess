package com.zuooh.chess.test;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.canvas.TemplateList;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.timer.ChessTimeDrawer;
import com.zuooh.style.Rectangle;

@Component
public class ChessTestController { // most recent first makes much more sense, think face book, email etc....

   private final Screen screen;

   public ChessTestController(
         @Inject("application") MobileApplication application,          
         @Inject("chats") TemplateList chatList)
   {
      try {              
         for(int i = 0; i < 20; i++){
            long time = System.currentTimeMillis();
            if(i == 2){
               Context context = chatList.addLast("chat"+i, (i % 2 == 0) ? "playerChat" : "opponentChat", 300);               
               drawChat(context, time, i);
            } else {
               Context context = chatList.addLast("chat"+i, (i % 2 == 0) ? "playerChat" : "opponentChat");               
               drawChat(context, time, i);
            }
         }
         Screen screen = application.getScreen();
         Rectangle size = chatList.getRectangle();
         int fullHeight = screen.screenHeight();
         int remainingSpace = fullHeight - size.height;
         
         if(remainingSpace > 0) {
            chatList.addLast("empty", "empty");              
         }
         this.screen = application.getScreen();
      } catch(Exception e) {
         e.printStackTrace();
         throw new IllegalStateException("Could not start history controller", e);
      }
   }
   
   private void drawChat(Context context, long time, int index) {
      String lastSeenTime = ChessTimeDrawer.convertToTextTime((index+1) * 60000, true);
      String trimmedTime = lastSeenTime.trim();
  
      context.addAttribute("text", "How are you this is chat " + index);
      context.addAttribute("time", trimmedTime + " ago");
   }
}
