package com.zuooh.chess.select;

import java.security.MessageDigest;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.user.ChessUser;

public class ChessGameKeyGenerator {
   
   private final ChessBoardContext boardContext;

   public ChessGameKeyGenerator(ChessBoardContext boardContext) { 
      this.boardContext = boardContext;
   }
   
   public String createGameId(ChessSide side, boolean online) {
      StringBuilder builder = new StringBuilder();
      
      try {
         MessageDigest digest = MessageDigest.getInstance("MD5");
         ChessUser currentProfile = boardContext.getBoardUser();
         String user = currentProfile.getKey();  
         long nanoTime = System.nanoTime();
         long hash = System.identityHashCode(this);
         
         builder.append(nanoTime);
         builder.append(side);
         builder.append(user);
         builder.append(hash);
         builder.append(online);         
         
         String key = builder.toString();
         byte[] octets = key.getBytes("UTF-8");
         
         builder.setLength(0);
         digest.update(octets);
         
         byte[] result = digest.digest();
         
         for(int i = 0; i < result.length;i++) {
            int value = 0xff & result[i];
            
            if(value <= 0xf) {
               builder.append("0");
            }
            builder.append(Integer.toHexString(value));
         }
         return builder.toString();         
      }catch(Exception e) {
         return builder.toString();
      }
   } 
}
