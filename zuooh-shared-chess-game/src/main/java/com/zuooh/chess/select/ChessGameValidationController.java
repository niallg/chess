package com.zuooh.chess.select;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameDatabase;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.timer.ChessTimeCalculator;

@Component
public class ChessGameValidationController {

   private static final Logger LOG = LoggerFactory.getLogger(ChessGameValidationController.class);

   private final ChessTimeCalculator timeCalculator;
   private final ChessGameManager gameManager;
   private final ChessDatabase database;

   public ChessGameValidationController(
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("gameManager") ChessGameManager gameManager)
   {
      this.timeCalculator = new ChessTimeCalculator(gameDatabase);
      this.database = gameDatabase;
      this.gameManager = gameManager;
   }

   public boolean isThereAnyGamesToResume() throws Exception {
      return countGamesToResume() > 0;
   }
   
   public boolean isThereAnyGamesToAccept() throws Exception {
      return countGamesToAccept() > 0;
   }
   
   public int countGamesToAccept() throws Exception {
      ChessUser playerProfile = gameManager.currentUser();
      ChessBoardGameDatabase boardDatabase = database.getBoardDatabase();
      Map<String, ChessGame> availableGames = gameManager.availableOnlineGames();
      Set<String> gameIds = availableGames.keySet();
      int count = 0;
      
      for(String gameId : gameIds) {
         ChessGame chessGame = availableGames.get(gameId);
         
         if(chessGame != null) {
            ChessGameCriteria chessChallenge = chessGame.getCriteria();
            ChessGameStatus gameStatus = chessGame.getStatus();
            String themeColor = chessChallenge.getThemeColor();
            long gameDuration = chessChallenge.getGameDuration(); 

            if(!boardDatabase.containsBoardGame(gameId) && !gameStatus.isGameOver()) {
               ChessUser opponentProfile = chessChallenge.getUser();
               ChessSide opponentColor = chessChallenge.getUserSide();
               ChessSide playerColor = opponentColor.oppositeSide();
               ChessBoardGame boardGame = new ChessBoardGame();
               String opponentId = opponentProfile.getKey();
               String playerId = playerProfile.getKey();
               
               boardGame.setOnlineGame(true);
               boardGame.setGameDuration(gameDuration);
               boardGame.setUserSide(playerColor);
               boardGame.setOpponentId(opponentId);
               boardGame.setUserId(playerId);
               boardGame.setThemeColor(themeColor);
               boardGame.setGameId(gameId);    

               if(ensureGameCanBeAccepted(boardGame)) {
                  count++;
               }
            }
         } 
      }
      return count;
   }
   
   public int countOnlineGamesToResume(long minimumDuration) throws Exception {
      ChessUser currentProfile = gameManager.currentUser();
      String playerId = currentProfile.getKey();         
      ChessBoardGameDatabase boardDatabase = database.getBoardDatabase();   
      List<ChessBoardGame> currentGames = boardDatabase.loadBoardGames(playerId);
      int count = 0;
      
      for(ChessBoardGame boardGame : currentGames) { 
         long gameDuration = boardGame.getGameDuration();
         
         if(gameDuration >= minimumDuration) {
            if(ensureGameCanBeResumed(boardGame)) {
               ChessBoardStatus status = boardGame.getBoardStatus();
               
               if(boardGame.isOnlineGame() && !status.isGameOver()) {
                  count++;
               }
            }           
         }
      }
      return count;
   }   
   
   public int countGamesToResume() throws Exception {
      return countGamesToResume(null);
   }
   
   public int countGamesToResume(ChessSide side) throws Exception {
      ChessUser currentProfile = gameManager.currentUser();
      String playerId = currentProfile.getKey();         
      ChessBoardGameDatabase boardDatabase = database.getBoardDatabase();
      List<ChessBoardGame> currentGames = boardDatabase.loadBoardGames(playerId);
      int count = 0;
      
      for(ChessBoardGame boardGame : currentGames) { 
         if(ensureGameCanBeResumed(boardGame)) {
            ChessSide gameSide = boardGame.getUserSide();
            
            if(side == null || side == gameSide) {
               count++;
            }
         }           
      }
      return count;
   }   
   
   private boolean ensureGameCanBeAccepted(ChessBoardGame boardGame) throws Exception {
      ChessBoardGameDatabase boardDatabase = database.getBoardDatabase();
      ChessHistoryDatabase historyDatabase = database.getHistoryDatabase();
      String themeColor = boardGame.getThemeColor();
      String opponentId = boardGame.getOpponentId();
      String gameId = boardGame.getGameId();
      boolean onlineGame = boardGame.isOnlineGame();
      
      if(themeColor == null) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.warn("No theme color specified for " + gameId);
         return false;
      }
      if(opponentId == null) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.warn("No opponent specified for " + gameId);
         return false;
      }
      if(!onlineGame) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.warn("Game was not online for " + gameId);
         return false;         
      }
      return true;
   }
   
   private boolean ensureGameCanBeResumed(ChessBoardGame boardGame) throws Exception {
      ChessBoardGameDatabase boardDatabase = database.getBoardDatabase();
      ChessHistoryDatabase historyDatabase = database.getHistoryDatabase();
      ChessGameDatabase gameDatabase = database.getGameDatabase();
      String gameId = boardGame.getGameId(); 
      String themeColor = boardGame.getThemeColor();      
      ChessBoardStatus status = boardGame.getBoardStatus();
      long gameDuration = boardGame.getGameDuration();
      long whiteTimeElapsed = timeCalculator.calculateElapsedTime(gameId, ChessSide.WHITE);
      long blackTimeElapsed = timeCalculator.calculateElapsedTime(gameId, ChessSide.BLACK);
      long whiteTimeRemaining = gameDuration - whiteTimeElapsed;
      long blackTimeRemaining = gameDuration - blackTimeElapsed;
      long timeRemaining = Math.min(blackTimeRemaining, whiteTimeRemaining);
      boolean knownOnlineGame = gameDatabase.containsGame(gameId);
      boolean onlineGame = boardGame.isOnlineGame();
      int historyCount = historyDatabase.countOfHistoryItems(gameId);

      if(status.isGameOver()) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("Game '" + gameId + "' is over so deleting it");
         return false;
      }
      if(!onlineGame && historyCount == 0) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("History count for '" + gameId + "' is zero so and it is not online so deleting it");         
         return false;
      }
      if(!onlineGame && timeRemaining < 0) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("Game '" + gameId + "' is not online and the time remaining is less than zero");         
         return false;
      }
      if(onlineGame && !knownOnlineGame) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);        
         LOG.info("Game '" + gameId + "' is online but no record of it exists");
         return false;
      } 
      if(themeColor == null) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("Game '" + gameId + "' seems to be only half created as it has no theme");         
         return false;
      }
      //if(opponentId == null) {
      //   LOG.info("Game '" + gameId + "' has not been accepted");
      //   return false;
      //}      
      return true;
   }     
}
