package com.zuooh.chess.select;

import java.util.LinkedList;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnBack;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.bind.annotation.OnMenu;
import com.zuooh.application.canvas.Card;
import com.zuooh.application.canvas.Deck;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameDatabase;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.theme.ChessThemeDatabase;
import com.zuooh.chess.database.theme.ChessThemeDefinition;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.timer.ChessTimeCalculator;
import com.zuooh.chess.timer.ChessTimeDrawer;
import com.zuooh.chess.timer.ChessTimer;
import com.zuooh.style.Orientation;
import com.zuooh.style.Style;

@Component
public class ChessAcceptGameScrollController extends ChessSelectGameController {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessAcceptGameScrollController.class);

   private final AtomicReference<ChessTimeDrawer> currentTimer;
   private final AtomicReference<ChessBoardGame> currentGame;
   private final LinkedList<ChessBoardGame> savedGames;
   private final ChessTimeCalculator timeCalculator;   
   private final ChessUserSource userSource;
   private final ChessDatabase gameDatabase;
   private final ChessBoardContext boardContext;
   private final ChessGameManager gameManager;
   private final Deck<Card> themeBoards;
   private final MobileApplication application;
   private final Document document;
   private final Window window;
   private final Screen screen;
   private final Style blank;
   private final Style star;

   public ChessAcceptGameScrollController(
         @Inject("application") MobileApplication application,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("document") Document document,
         @Inject("userSource") ChessUserSource userSource,
         @Inject("gameManager") ChessGameManager gameManager,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("themeBoards") Deck<Card> themeBoards,
         @Inject("blank") Style blank,          
         @Inject("star") Style star)
   {
      this.timeCalculator = new ChessTimeCalculator(gameDatabase);
      this.currentTimer = new AtomicReference<ChessTimeDrawer>();
      this.currentGame = new AtomicReference<ChessBoardGame>();
      this.savedGames = new LinkedList<ChessBoardGame>();
      this.window = application.getWindow();
      this.screen = application.getScreen();
      this.userSource = userSource;
      this.gameDatabase = gameDatabase;
      this.gameManager = gameManager;
      this.boardContext = boardContext;
      this.themeBoards = themeBoards;
      this.application = application;
      this.document = document;
      this.blank = blank;
      this.star = star;
   }
   
   @Override
   public long onInterval() throws Exception {
      ChessTimeDrawer timer = currentTimer.get();
      ChessBoardGame boardGame = currentGame.get();
      
      if(timer != null && boardGame != null) {
         timer.drawTimer(boardGame);
      }
      return 1000;
   }
   
   @Override
   public MobileApplication getApplication() {
      return application;
   }
   
   @Override
   public ChessTimeCalculator getTimeCalculator() {
      return timeCalculator;
   }     
   
   @Override
   public ChessUserSource getUserSource() {
      return userSource;
   }
   
   @Override
   public ChessGameManager getGameManager() {
      return gameManager;
   }
   
   @Override
   public ChessDatabase getGameDatabase() {
      return gameDatabase;
   }

   @OnMenu
   @OnBack
   public void showMenu() {
      screen.showPage("home");
   }

   @OnCreate
   public void loadSavedGames() throws Exception {
      ChessUser playerProfile = gameManager.currentUser();
      ChessUserDatabase profileDatabase = gameDatabase.getUserDatabase();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      Map<String, ChessGame> availableGames = gameManager.availableOnlineGames();
      Set<String> gameIds = availableGames.keySet();

      for(String gameId : gameIds) {
         ChessGame chessGame = availableGames.get(gameId);
         
         if(chessGame != null) {
            ChessGameCriteria chessChallenge = chessGame.getCriteria();
            ChessGameStatus gameStatus = chessGame.getStatus();
            String themeColor = chessChallenge.getThemeColor();
            long gameDuration = chessChallenge.getGameDuration(); 

            if(!boardDatabase.containsBoardGame(gameId) && !gameStatus.isGameOver()) {
               ChessUser opponentProfile = chessChallenge.getUser();
               ChessSide opponentColor = chessChallenge.getUserSide();
               ChessSide playerColor = opponentColor.oppositeSide();
               ChessBoardGame boardGame = new ChessBoardGame();
               String opponentId = opponentProfile.getKey();
               String playerId = playerProfile.getKey();
               
               boardGame.setOnlineGame(true);
               boardGame.setGameDuration(gameDuration);
               boardGame.setUserSide(playerColor);
               boardGame.setOpponentId(opponentId);
               boardGame.setUserId(playerId);
               boardGame.setThemeColor(themeColor);
               boardGame.setGameId(gameId);    
               profileDatabase.saveUser(opponentProfile);
               // saving this will cause problems when rotating, 
               // save after accept only
               //boardDatabase.saveBoardGame(boardGame);
               
               if(savedGames.isEmpty()) {
                  if(validGame(boardGame)) {                      
                     if(drawSlide(boardGame)) {
                        savedGames.add(boardGame);
                     }
                  }
               } else {
                  savedGames.add(boardGame);
               }
            }else {
               ChessMove chessMove = chessGame.getLastMove();
               
               if(gameStatus == ChessGameStatus.AWAITING_ACCEPTENCE) {
                  LOG.warn("Strange that a game of status " + gameStatus + " and last move " + chessMove + " is an existing board");
               } else {
                  LOG.warn("Game " + gameId + " of status " + gameStatus + " and last move " + chessMove + " is an existing board");
               }
            }
         } 
      }
      if(savedGames.isEmpty()) {
         screen.showPage("resumeGameContinue");
      }
      window.invalidate();
   }
   
   private boolean validGame(ChessBoardGame boardGame) throws Exception {
      String themeColor = boardGame.getThemeColor();
      String opponentId = boardGame.getOpponentId();
      String gameId = boardGame.getGameId();
      boolean onlineGame = boardGame.isOnlineGame();
      
      if(themeColor == null) {
         LOG.warn("No theme color specified for " + gameId);
         return false;
      }
      if(!onlineGame) {
         LOG.warn("Game was not online for " + gameId);
         return false;         
      }
      if(opponentId == null) {
         LOG.warn("No opponent specified for " + gameId);
         return false;
      }      
      return true;
   }
   
   private boolean drawSlide(ChessBoardGame boardGame) throws Exception {      
      String gameId = boardGame.getGameId();      
      Screen screen = application.getScreen();
      Orientation orientation = screen.orientation();
      ChessSide playerColor = boardGame.getUserSide();
      boolean youAreWhite = playerColor == ChessSide.WHITE;
      String themeColor = boardGame.getThemeColor();
      String cardName = themeColor + (youAreWhite ? "Card" : "FlipCard");
      Card card = themeBoards.getCanvas(cardName);           
      
      if(card == null) {
         throw new IllegalStateException("Could not find card "+ cardName + " for game " + gameId + " and theme " + themeColor);
      }
      if(card != null) {
         drawGame(boardGame, document, card);
         
         if(orientation == Orientation.PORTRAIT) {
            drawShortSummary(card, boardGame);
         }
         ChessTimer timer = card.getAttribute("chessTimer");
         
         if(timer != null) {
            ChessTimeDrawer drawer = timer.getDrawer();
            
            drawer.drawTimer(boardGame);
            currentTimer.set(drawer);
         }
         drawSkill(card, boardGame, star, blank);
         currentGame.set(boardGame);         

      }
      themeBoards.setTop(cardName);
      return true;
   }  
   
   @Override
   protected void drawShortSummary(Context context, ChessBoardGame boardGame) throws Exception {
      Panel shortSummaryPanel = context.getCanvas("description");
      
      if(shortSummaryPanel != null) {
         shortSummaryPanel.setText("Accept or reject game");
      }
   }   
   
   @OnCall
   public void showOpponent(Toggle toggle) throws Exception {
      ChessBoardGame boardGame = currentGame.get();
      ChessTimeDrawer boardTimer = currentTimer.get();
      ChessSide playerColor = boardGame.getUserSide();
      ChessSide opponentColor = playerColor.oppositeSide();

      if(boardTimer != null) {
         boardTimer.showPlayer(opponentColor.name);
      }
      toggle.toggle();
      window.invalidate();
   }     
   
   @OnCall
   public void rejectGame(Toggle toggle) throws Exception {      
      ChessBoardGame boardGame = currentGame.get();
      
      if(boardGame != null) {
         ChessGameDatabase database = gameDatabase.getGameDatabase();
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
         String gameId = boardGame.getGameId();
         ChessGame chessGame = database.loadGame(gameId);
         //ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
   
         if(chessGame != null) {
            gameManager.rejectOnlineGame(gameId); // reject it
            database.deleteGame(chessGame);
         }
         boardDatabase.deleteBoardGame(boardGame); // should not be in here, but just in case
      }
      nextGame(toggle);
   }

   @OnCall
   public void acceptGame(Toggle toggle) throws Exception { // go straight to the game!!     
      ChessBoardGame boardGame = currentGame.get();
      
      if(boardGame != null) {
         ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();
         String themeColor = boardGame.getThemeColor();
         ChessThemeDefinition themeDefinition = themeDatabase.loadThemeDefinition(themeColor);
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();      
         String gameId = boardGame.getGameId();
         ChessPlayer opponentPlayer = gameManager.acceptOnlineGame(gameId);
         ChessSide playerColor = boardGame.getUserSide();
         ChessSide opponentColor = playerColor.oppositeSide();
         String screenName = themeDefinition.getScreen(playerColor);
         boolean gameOnline = boardGame.isOnlineGame();
         long gameDuration = boardGame.getGameDuration();      
         
         boardDatabase.saveBoardGame(boardGame);
         boardContext.setGameDuration(gameDuration);
         boardContext.setGameOnline(gameOnline);
         boardContext.setOpponentSide(opponentColor);
         boardContext.setOpponent(opponentPlayer);
         boardContext.setPlayer(null);
         boardContext.setGameId(gameId);
         boardContext.setThemeColor(themeColor);
         boardContext.setGamePage(screenName);      
         screen.showPage(screenName);
         toggle.toggle();
      }
      nextGame(toggle);
   }      
   
   private void nextGame(Toggle toggle) throws Exception {
      while(!savedGames.isEmpty()) {
         ChessBoardGame savedGame = savedGames.removeFirst();

         if(savedGame != null) {
            ChessBoardGame boardGame = currentGame.get();

            if(!sameGame(savedGame, boardGame)) { // not the same then draw!!
               if(drawSlide(savedGame)) {
                  break;
               }
            }
         }
      }
      if(savedGames.isEmpty()) {           
         screen.showPage("resumeGameContinue");          
      }
      toggle.toggle();
      window.invalidate();
   }
   
   
   private boolean sameGame(ChessBoardGame savedGame, ChessBoardGame currentGame) {
      if(savedGame != null && currentGame != null) {
         String savedId = savedGame.getGameId();
         String currentId = currentGame.getGameId();
         
         if(savedId.equals(currentId)) {
            return true;
         }
      }
      return false;
   }
}

