package com.zuooh.chess.select.search;

import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.bind.annotation.Required;
import com.zuooh.chess.client.event.operation.ChessUserSearchCommand;

@Component
public class ChessUserSearchClient {

   @Required
   @Inject("searchCommand")
   private ChessUserSearchCommand searchCommand;
   
   @OnCall
   public void search(String searchTerm) {
      searchCommand.execute(searchTerm);
   }
}
