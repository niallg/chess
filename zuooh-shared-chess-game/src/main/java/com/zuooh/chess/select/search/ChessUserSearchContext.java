package com.zuooh.chess.select.search;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.user.ChessUser;

public class ChessUserSearchContext {

   private final AtomicReference<String> resultScreen;
   private final AtomicReference<ChessSide> userSide;
   private final AtomicReference<ChessUser> user;
   private final AtomicBoolean focus;

   public ChessUserSearchContext() {
      this.user = new AtomicReference<ChessUser>();
      this.userSide = new AtomicReference<ChessSide>();
      this.resultScreen = new AtomicReference<String>();      
      this.focus = new AtomicBoolean(false);
   }
   
   public String getResultScreen(){
      return resultScreen.get();
   }
   
   public void setResultScreen(String screenName) {
      resultScreen.set(screenName);
   }

   public boolean isFocusRequired() {
      return focus.get();
   }

   public void setFocusRequired(boolean focus) {
      this.focus.set(focus);
   }

   public ChessSide getUserSide() {
      return userSide.get();
   }

   public void setUserSide(ChessSide side) {
      userSide.set(side);
   }

   public ChessUser getChoosenUser() {
      return user.get();
   }

   public void setChoosenUser(ChessUser value){
      user.set(value);
   }
}
