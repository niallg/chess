package com.zuooh.chess.select.theme;

import java.util.List;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.Slide;
import com.zuooh.application.canvas.SlideShow;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.application.context.Context;
import com.zuooh.application.store.Inventory;
import com.zuooh.application.store.ProductStore;
import com.zuooh.application.store.PurchaseStatus;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.control.ChessBoardPlayerProvider;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessBoardStateModel;
import com.zuooh.chess.control.state.ChessBoardStateSet;
import com.zuooh.chess.control.state.ChessGameQuota;
import com.zuooh.chess.control.state.ChessUpgradeProduct;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.theme.ChessThemeDatabase;
import com.zuooh.chess.database.theme.ChessThemeDefinition;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.view.ChessBoardPanel;

@Component
public class ChessBoardThemeSelectController {

   private final ChessUserSource userSource;
   private final ChessBoardPlayerProvider playerProvider;
   private final ChessDatabase gameDatabase;
   private final ChessBoardContext boardContext;
   private final ProductStore productStore;
   private final SlideShow slideShow;
   private final Document document;
   private final Window window;
   private final Screen screen;
   private final Context themes;

   public ChessBoardThemeSelectController(
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("playerProvider") ChessBoardPlayerProvider playerProvider,
         @Inject("userSource") ChessUserSource userSource,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("application") MobileApplication application,
         @Inject("document") Document document,
         @Inject("slideShow") SlideShow slideShow,
         @Inject("themes") Context themes)
   {
      this.productStore = application.getProductStore();
      this.window = application.getWindow();
      this.screen = application.getScreen();
      this.userSource = userSource;
      this.gameDatabase = gameDatabase;
      this.slideShow = slideShow;
      this.playerProvider = playerProvider;
      this.boardContext = boardContext;
      this.document = document;
      this.themes = themes;
   }

   @OnCreate
   public void fillBoards() throws Exception {
      ChessUser boardPlayer = boardContext.getBoardUser();
      ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();
      List<ChessThemeDefinition> availableThemes = themeDatabase.loadThemeDefinitions();
      String gameId = boardContext.getGameId();
      
      for(ChessThemeDefinition availableTheme : availableThemes) {
         String themeColor = availableTheme.getColor();
         Context boardSetContext = themes.getContext(themeColor);

         if(boardSetContext != null) {
            ChessGameQuota quota = new ChessGameQuota(0, 0, 0);
            ChessUpgradeProduct product = new ChessUpgradeProduct(quota, quota, null);             
            ChessBoardContext boardContext = new ChessBoardContext(userSource, product, gameId, themeColor);
            ChessBoardStateModel boardModel = new ChessBoardStateModel(boardContext, gameDatabase);
            ChessBoardStateSet previewStateSet = new ChessBoardStateSet(boardModel, document);
            ChessBoardPanel boardPanel = new ChessBoardPanel(previewStateSet, null, boardSetContext);

            boardSetContext.addAttribute("boardPanel", boardPanel);
            boardSetContext.addAttribute("themeDefinition", availableTheme);
         }
      }
      Slide slide = slideShow.start();    
      Panel footerPanel = slide.getCanvas("footer");
      
      if(footerPanel != null) {    
         String name = slide.getName();
         int index = slideShow.getIndex(name);
         int count = slideShow.getCount();
         
         footerPanel.setText((index + 1) + " of " + count);
      }
      window.invalidate();
   }

   @OnCall
   public void nextSlide(Toggle toggle) throws Exception {
      Slide slide = slideShow.next();    
      Panel footerPanel = slide.getCanvas("footer");
      
      if(footerPanel != null) {    
         String name = slide.getName();
         int index = slideShow.getIndex(name);
         int count = slideShow.getCount();
         
         footerPanel.setText((index + 1) + " of " + count);
      }
      toggle.toggle();
      window.invalidate();
   }

   @OnCall
   public void previousSlide(Toggle toggle) throws Exception {
      Slide slide = slideShow.back();
      Panel footerPanel = slide.getCanvas("footer");
      
      if(footerPanel != null) {    
         String name = slide.getName();
         int index = slideShow.getIndex(name);
         int count = slideShow.getCount();
         
         footerPanel.setText((index + 1) + " of " + count);
      }
      toggle.toggle();
      window.invalidate();
   }
   
   @OnCall
   public void selectTheme(String themeColor) throws Exception {
      String gameId = boardContext.getGameId();
      Context boardSetContext = themes.getContext(themeColor);
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId); 
      ChessSide playerColor = boardGame.getUserSide();
      long gameDuration = boardContext.getGameDuration();
      
      boardContext.setThemeColor(themeColor);
      boardGame.setThemeColor(themeColor);
      boardGame.setGameDuration(gameDuration);
      boardDatabase.saveBoardGame(boardGame);
      
      ChessPlayer mockPlayer = playerProvider.createMockPlayer(boardGame);
      ChessPlayer opponentPlayer = playerProvider.createPlayer(gameId);
      ChessThemeDefinition themeDefinition = boardSetContext.getAttribute("themeDefinition");
      ChessUpgradeProduct product = boardContext.getUpgradeProduct();
      String screenName = themeDefinition.getScreen(playerColor);      
      ChessUser user = boardContext.getBoardUser();      
      String productId = product.getProductId();
      String userId = user.getKey();
      
      boardContext.setPlayer(mockPlayer);   
      boardContext.setOpponent(opponentPlayer);
      boardContext.setGamePage(screenName);
      
      if(!product.isUpgraded()) {
         Inventory inventory = productStore.loadInventory();
         PurchaseStatus status = inventory.getPurchaseStatus(productId);
         int finishedGames = boardDatabase.countFinishedBoardGames(userId); // start nagging after finished games
         
         if(status.isPurchased()) { 
            product.setUpgraded(true);
         } 
         if(!product.isUpgraded() && product.isPromptRequired() && finishedGames > 0) { // wait for at least one finished game before prompting
            boardContext.setUpgradeFinishPage(screenName);
            boardContext.setUpgradePurchasePage("upgradeAdvertFree");
            screen.showPage("upgrade");  
         } else {
            boardContext.setUpgradeFinishPage(null);
            boardContext.setUpgradePurchasePage(null);    
            screen.showPage(screenName);                                     
         }
      } else {
         screen.showPage(screenName);
      }
   }
}
