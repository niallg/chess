package com.zuooh.chess.select;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Dialog;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.client.event.operation.ChessReconcileGamesCommand;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameDatabase;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.task.ChessTaskController;
import com.zuooh.message.client.ResponseStatus;

@Component
public class ChessGameFocusController extends ChessTaskController {   
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessGameFocusController.class);

   private final ChessReconcileGamesCommand reconcileCommand;
   private final Map<String, ChessGame> reconcileGames;
   private final AtomicReference<ChessGame> savedGame;
   private final AtomicReference<String> requestId;
   private final AtomicLong expiryTime;
   private final ChessDatabase chessDatabase;
   private final ChessGameManager gameManager;
   private final Screen screen;

   public ChessGameFocusController(
         @Inject("reconcileCommand") ChessReconcileGamesCommand reconcileCommand,
         @Inject("gameDatabase") ChessDatabase chessDatabase,
         @Inject("gameManager") ChessGameManager gameManager,
         @Inject("screen") Screen screen)
   {
      this.reconcileGames = new ConcurrentHashMap<String, ChessGame>();
      this.savedGame = new AtomicReference<ChessGame>();
      this.requestId = new AtomicReference<String>();
      this.expiryTime = new AtomicLong();
      this.chessDatabase = chessDatabase;
      this.reconcileCommand = reconcileCommand;
      this.gameManager = gameManager;
      this.screen = screen;
   }
   
   @Override
   public void onBeforeStart() {
      long currentTime = System.currentTimeMillis();
      long timeLimit = currentTime + 60000;
      
      expiryTime.set(timeLimit);
   }

   @Override
   public long onInterval() throws Exception {
      String requestGameId = requestId.get(); 
      
      if(requestGameId != null) {
         checkReconcileResponse(requestGameId);
      } else {
         long currentTime = System.currentTimeMillis();
         long timeLimit = expiryTime.get();
         
         if(timeLimit < currentTime) {
            screen.showPage("resumeGame"); // something happened, we have had no showGame() request 
            return -1;
         }
      }
      return 1000;
   }     
   
   @Override
   public void onBeforeStop() {
      reconcileCommand.reset();
   }
   
   @OnCall
   public void showGame(String gameId) throws Exception {
      ChessGameDatabase gameDatabase = chessDatabase.getGameDatabase();

      try {
         if(requestId.compareAndSet(null, gameId)) { // first request only accepted
            ChessGame currentGame = gameDatabase.loadGame(gameId);
            ChessUser currentUser = gameManager.currentUser();
            String userId = currentUser.getKey();
            
            if(currentGame != null) {
               ChessGameStatus gameStatus = currentGame.getStatus();               
               
               savedGame.set(currentGame); // ensure we at least have the local copy
               
               if(gameStatus.isGameOver()) {
                  screen.showPage("history"); // game is over
               } else {
                  String ownerId = currentGame.getOwnerId();
                  
                  if(!ownerId.equals(userId) && gameStatus.isInvitation()) {
                     screen.showPage("resumeGameAcceptFocus", "selectController.showGame('" + gameId + "')"); // game invitation, no need to reconcile
                  } else {
                     reconcileCommand.execute(userId);; // ensure we are kept in sync
                  }
               }
            } else {
               reconcileCommand.execute(userId);// we don't have the game saved so fetch it
            }
         }
      } catch(Exception e) {
         LOG.info("Could not load games", e);
      }
   }
   
   private void showGame(ChessGame game) {
      try {
         if(game != null) {
            ChessGameStatus gameStatus = game.getStatus();
            ChessUser currentUser = gameManager.currentUser();
            String userId = currentUser.getKey();
            String gameId = game.getGameId();
            
            if(gameStatus.isGameOver()) {
               screen.showPage("history"); // game is over
            } else {
               String ownerId = game.getOwnerId();
               
               if(!ownerId.equals(userId) && gameStatus.isInvitation()) {
                  screen.showPage("resumeGameAcceptFocus", "selectController.showGame('" + gameId + "')"); // game invitation, no need to reconcile
               } else {
                  screen.showPage("resumeGameContinueFocus", "selectController.showGame('" + gameId + "')"); // show active game
               }
            }
         } else {
            screen.showPage("resumeGame"); // this should never happen
         }
      } catch(Exception e) {
         LOG.info("Could not show game", e);
      }
   }
   
   private void checkReconcileResponse(String requestId) {
      ResponseStatus status = reconcileCommand.getStatus();
      ChessGame currentGame = savedGame.get();
      
      if(status != ResponseStatus.NONE && status != ResponseStatus.SENDING) {
         if(status == ResponseStatus.TIMEOUT) {            
            if(currentGame != null) {
               showGame(currentGame); // if we have the data locally then use that
            } else {
               reconcileFailure("Error", "Unable to get game details due to network delays!");
            }
         } else if(status == ResponseStatus.NOT_CONNECTED) {
            if(currentGame != null) {
               showGame(currentGame);
            } else {
               reconcileFailure("Error", "Connection to internet is currently not available!"); 
            }                         
         } else if(status == ResponseStatus.ERROR) {
            if(currentGame != null) {
               showGame(currentGame);
            } else {
               reconcileFailure("Error", "Error occured downloading game details!");  
            }                     
         } else if(status == ResponseStatus.SUCCESS) {
            ChessListOfGamesEvent result = reconcileCommand.getListOfGamesResponse();
            
            if(result != null) {
               List<ChessGame> gameList = result.getGames();
               
               if(!gameList.isEmpty()) {
                  for(ChessGame game : gameList){
                     String gameId = game.getGameId();
                     reconcileGames.put(gameId, game);
                  }
                  ChessGame reconciledGame = reconcileGames.get(requestId);
                  
                  if(reconciledGame != null) {
                     ChessGameDatabase gameDatabase = chessDatabase.getGameDatabase();
                     
                     try {
                        gameDatabase.saveGame(reconciledGame);
                     } catch(Exception e) {
                        LOG.info("Try to save game before showing", e); // not really needed but best to be safe
                     }
                     showGame(reconciledGame);
                  } else {
                     if(currentGame != null) {
                        showGame(currentGame);
                     } else {
                        reconcileFailure("Not Found", "No record of requested game found!");
                     }
                  }
               } else {
                  if(currentGame != null) {
                     showGame(currentGame);
                  } else {
                     reconcileFailure("Not Found", "You have no invitations or games!");
                  }
               }
            } else {
               if(currentGame != null) {
                  showGame(currentGame);
               } else {
                  reconcileFailure("Not Found", "You have no invitations or games!");
               }
            }
         }
         reconcileCommand.reset();   
      }
   }
   
   private void reconcileFailure(String title, String message) {
      Dialog dialog = screen.openDialog("warningDialog");
      
      dialog.setAttribute("warningTitle", title);
      dialog.setAttribute("warningMessage", message);
      dialog.setAttribute("warningFunction", "screen.showPage('resumeGame')");
      dialog.setAttribute("warningButton", "Okay");
      dialog.showDialog();      
   }   
}