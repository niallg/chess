package com.zuooh.chess.select.search;

import java.util.List;

import com.zuooh.application.Dialog;
import com.zuooh.application.Screen;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.bind.annotation.Required;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.client.event.operation.ChessUserSearchCommand;
import com.zuooh.chess.client.event.response.ChessUserSearchResultEvent;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.task.ChessTaskController;
import com.zuooh.message.client.ResponseStatus;

@Component
public class ChessUserSearchResultChecker extends ChessTaskController {

   @Required
   @Inject("searchCommand")
   private ChessUserSearchCommand searchCommand;
   
   @Required
   @Inject("gameManager") 
   private ChessGameManager gameManager;
   
   
   @Required
   @Inject("screen")
   private Screen screen;

   @Override
   public long onInterval() throws Exception {
      ResponseStatus status = searchCommand.getStatus();
      
      if(status != ResponseStatus.NONE && status != ResponseStatus.SENDING) {
         if(status == ResponseStatus.TIMEOUT) {
            showWarning("Error", "Unable to complete search due to network delays!");                  
         } else if(status == ResponseStatus.NOT_CONNECTED) {
            showWarning("Error", "Connection to internet is currently not available!");             
         } else if(status == ResponseStatus.ERROR) {
            showWarning("Error", "Search result was an error, please try again!");                       
         } else if(status == ResponseStatus.SUCCESS) {
            ChessUserSearchResultEvent result = searchCommand.getSearchResponse();
            List<ChessUser> results = result.getUsers();
            ChessUser currentUser = gameManager.currentUser();
            String currentId = currentUser.getKey();
            int count = 0;
            
            for(ChessUser user : results) {
               String playerId = user.getKey();
               
               if(!playerId.equals(currentId)) { // no playing against yourself
                  count++;
               }
            }            
            if(count > 0) {
               screen.showPage("searchResults");
            } else {
               if(results.isEmpty()) {
                  showWarning("Not Found", "No player was found matching the search phrase!");
               } else {
                  showWarning("Not Found", "No player found that you can play against!");
               }
            }
         }
         searchCommand.reset();   
      }
      return 1000;
   }     
   
   @Override
   public void onBeforeStop() {
      searchCommand.reset();
   }
   
   public void showWarning(String title, String message) {
      Dialog dialog = screen.openDialog("warningDialog");
      
      dialog.setAttribute("warningTitle", title);
      dialog.setAttribute("warningMessage", message);
      dialog.setAttribute("warningFunction", "dialog.hideDialog()");
      dialog.setAttribute("warningButton", "Okay");
      dialog.showDialog();
   }
}
