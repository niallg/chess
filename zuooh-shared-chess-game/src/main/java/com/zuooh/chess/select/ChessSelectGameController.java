package com.zuooh.chess.select;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.canvas.Card;
import com.zuooh.application.canvas.IconList;
import com.zuooh.application.canvas.ImageIcon;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.computer.ChessComputerMockPlayer;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessBoardStateModel;
import com.zuooh.chess.control.state.ChessBoardStateSet;
import com.zuooh.chess.control.state.ChessGameQuota;
import com.zuooh.chess.control.state.ChessUpgradeProduct;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.database.game.ChessGameDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.chess.task.ChessTaskController;
import com.zuooh.chess.timer.ChessTimeCalculator;
import com.zuooh.chess.timer.ChessTimeDrawer;
import com.zuooh.chess.timer.ChessTimer;
import com.zuooh.chess.view.ChessBoardPanel;
import com.zuooh.style.Style;

@Component
public abstract class ChessSelectGameController extends ChessTaskController {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessSelectGameController.class);   
   
   protected void drawShortSummary(Context context, ChessBoardGame boardGame) throws Exception {
      Panel shortSummaryPanel = context.getCanvas("description");
      
      if(shortSummaryPanel != null) {
         ChessDatabase gameDatabase = getGameDatabase();
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
         String gameId = boardGame.getGameId();
         String opponentId = boardGame.getOpponentId();
         ChessSide playerColor = boardGame.getUserSide();
         ChessSide opponentColor = playerColor.oppositeSide();
         ChessHistoryItem lastMove = historyDatabase.loadLastHistoryItem(gameId);
         
         if(lastMove != null) {
            ChessSide currentSide = lastMove.chessMove.side.oppositeSide();         
                   
            if(currentSide != opponentColor) { // a move was made by opponent
               if(opponentId != null) {
                  shortSummaryPanel.setText("Your move");               
               } else {
                  shortSummaryPanel.setText("Your move in new game");
               }
            } else {
               if(opponentId != null) {
                  shortSummaryPanel.setText("Waiting for opponent to move");
               } else {
                  shortSummaryPanel.setText("No opponent yet");
               }
            }
         } else { // no move was made yet
            if(playerColor == ChessSide.WHITE) {
               shortSummaryPanel.setText("Your move in new game");
            } else {
               if(opponentId != null) {
                  shortSummaryPanel.setText("Waiting for opponent to move");
               } else {
                  shortSummaryPanel.setText("No opponent yet");
               }
            }
         }
      }
   }
   
   protected void drawLastMoveMade(Context context, ChessBoardGame boardGame) throws Exception {
      Panel description = context.getCanvas("description");
      ChessDatabase gameDatabase = getGameDatabase();
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      String gameId = boardGame.getGameId();
      ChessHistoryItem lastMove = historyDatabase.loadLastHistoryItem(gameId);
      long timeStamp = lastMove.timeStamp;
      long currentTime = System.currentTimeMillis();
      long timeSinceMove = currentTime - timeStamp;
      String lastMoveTime = ChessTimeDrawer.convertToTextTime(timeSinceMove);
      String timeTrimmed = lastMoveTime.trim();

      description.setText("Last move " + timeTrimmed + " ago");
   }

   protected void drawTimeRemaining(Context context, ChessBoardGame boardGame) throws Exception {
      Panel timeRemaining = context.getCanvas("description");
      ChessDatabase gameDatabase = getGameDatabase();
      ChessTimeCalculator timeCalculator = getTimeCalculator();
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      String gameId = boardGame.getGameId();
      ChessSide playerColor = boardGame.getUserSide();
      ChessSide opponentColor = playerColor.oppositeSide();
      long gameDuration = boardGame.getGameDuration();
      long whiteTimeElapsed = timeCalculator.calculateElapsedTime(gameId, ChessSide.WHITE);
      long blackTimeElapsed = timeCalculator.calculateElapsedTime(gameId, ChessSide.WHITE);
      long whiteTimeRemaining = gameDuration - whiteTimeElapsed;
      long blackTimeRemaining = gameDuration - blackTimeElapsed;
      long timeLeft = whiteTimeRemaining;

      if(whiteTimeRemaining > 500 && blackTimeRemaining > 500) {
         if(opponentColor == ChessSide.WHITE) {
            timeLeft = blackTimeRemaining;
         }
         String textTime = ChessTimeDrawer.convertToTime(timeLeft);
         timeRemaining.setText(textTime + " remaining");
      } else {
         timeRemaining.setText("Time's up!");
      }
   }

   protected void drawPlayerName(Context context, String playerName) {
      Panel person = context.getCanvas("person");
      person.setText(playerName);
   }

   protected void drawSkill(Card card, ChessBoardGame boardGame, Style style, Style blank) throws Exception {
      ChessDatabase gameDatabase = getGameDatabase();
      ChessGameManager gameManager = getGameManager();
      ChessUser currentProfile = gameManager.currentUser();
      ChessUserDatabase profileDatabase = gameDatabase.getUserDatabase();
      ChessSide playerColor = boardGame.getUserSide();
      ChessUserData currentData = currentProfile.getData();
      ChessSide opponentColor = playerColor.oppositeSide();
      String opponentId = boardGame.getOpponentId();
      
      for(int i = 1; i <= 6; i++) {
         Panel starPanel = card.getCanvas(opponentColor.name + "Star" + i);

         if(starPanel != null) {
            starPanel.setStyle(blank);
         }
      }
      if(opponentId != null) { // there may not be an opponent yet!
         ChessUser opponentProfile = profileDatabase.loadUser(opponentId);  
         
         if(opponentProfile != null) {
            ChessUserData opponentData = opponentProfile.getData();
            int opponentSkillLevel = opponentData.getSkill();
   
            for(int i = 1; i <= opponentSkillLevel; i++) {
               Panel starPanel = card.getCanvas(opponentColor.name + "Star" + i);
      
               if(starPanel != null) {
                  starPanel.setStyle(style);
               }
            }
         }
      }
      int playerSkill = currentData.getSkill();

      for(int i = 1; i <= playerSkill; i++) {
         Panel starPanel = card.getCanvas(playerColor.name + "Star" + i);

         if(starPanel != null) {
            starPanel.setStyle(style);
         }
      }
   }
   
   protected boolean acceptGame(ChessBoardGame boardGame) throws Exception {
      ChessDatabase database = getGameDatabase();
      ChessTimeCalculator timeCalculator = getTimeCalculator();
      ChessBoardGameDatabase boardDatabase = database.getBoardDatabase();
      ChessHistoryDatabase historyDatabase = database.getHistoryDatabase();
      ChessGameDatabase gameDatabase = database.getGameDatabase();      
      String gameId = boardGame.getGameId(); 
      String themeColor = boardGame.getThemeColor();      
      ChessBoardStatus status = boardGame.getBoardStatus();      
      long gameDuration = boardGame.getGameDuration();
      long whiteTimeElapsed = timeCalculator.calculateElapsedTime(gameId, ChessSide.WHITE);
      long blackTimeElapsed = timeCalculator.calculateElapsedTime(gameId, ChessSide.BLACK);
      long whiteTimeRemaining = gameDuration - whiteTimeElapsed;
      long blackTimeRemaining = gameDuration - blackTimeElapsed;
      long timeRemaining = Math.min(blackTimeRemaining, whiteTimeRemaining);
      boolean knownOnlineGame = gameDatabase.containsGame(gameId);
      boolean onlineGame = boardGame.isOnlineGame();
      int historyCount = historyDatabase.countOfHistoryItems(gameId);

      if(status.isGameOver()) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("Game '" + gameId + "' is over so deleting it");
         return false;
      }
      if(!onlineGame && historyCount == 0) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("History count for '" + gameId + "' is zero so and it is not online so deleting it");         
         return false;
      }
      if(!onlineGame && timeRemaining < 0) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("Game '" + gameId + "' is not online and the time remaining is less than zero");         
         return false;
      }
      if(onlineGame && !knownOnlineGame) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("Game '" + gameId + "' is online but no record of it exists");         
         return false;
      }      
      if(themeColor == null) {
         boardDatabase.deleteBoardGame(boardGame);
         historyDatabase.deleteAllHistoryItems(gameId);
         LOG.info("Game '" + gameId + "' seems to be only half created as it has no theme");         
         return false;
      }     
      //if(opponentId == null) {
      //   LOG.info("Game '" + gameId + "' has not been accepted");
      //   return false;
      //}      
      return true;
   }      

   protected void drawGame(ChessBoardGame boardGame, Document document, Card card) throws Exception {
      String gameId = boardGame.getGameId();
      String opponentId = boardGame.getOpponentId();               
      ChessSide playerColor = boardGame.getUserSide();
      MobileApplication application = getApplication();
      ChessUserSource userSource = getUserSource();
      ChessGameManager gameManager = getGameManager();
      ChessDatabase gameDatabase = getGameDatabase();
      ChessUserDatabase profileDatabase = gameDatabase.getUserDatabase();
      ChessSide opponentColor = playerColor.oppositeSide(); 
      String playerId = boardGame.getUserId();
      String themeColor = boardGame.getThemeColor();
      boolean onlineGame = boardGame.isOnlineGame();
      ChessUser opponentProfile = null;
      
      if(opponentId != null) {
         opponentProfile = profileDatabase.loadUser(opponentId); // null if not accepted  
      }      
      IconList blackIcons = card.getCanvas("blackIcons");
      IconList whiteIcons = card.getCanvas("whiteIcons");

      if(card != null) {
         ChessGameQuota quota = new ChessGameQuota(0, 0, 0);
         ChessUpgradeProduct product = new ChessUpgradeProduct(quota, quota, null);         
         ChessBoardContext previewContext = new ChessBoardContext(userSource, product, gameId, themeColor);
         ChessComputerMockPlayer mockPlayer = new ChessComputerMockPlayer(opponentColor, opponentProfile, gameId);
         
         previewContext.setGameId(gameId);
         previewContext.setThemeColor(themeColor);               
         previewContext.setOpponentSide(opponentColor);
         previewContext.setOpponent(mockPlayer);         
         
         ChessBoardStateModel boardModel = new ChessBoardStateModel(previewContext, gameDatabase);
         ChessBoardStateSet previewStateSet = new ChessBoardStateSet(boardModel, document);
         ChessBoardPanel boardPanel = new ChessBoardPanel(previewStateSet, null, card);
         ChessTimer timer = new ChessTimer(application, previewContext, gameDatabase, gameManager, whiteIcons, blackIcons);         
         
         if(onlineGame) {
            ChessChatDatabase chatDatabase = gameDatabase.getChatDatabase();
            List<ChessChat> mailList = chatDatabase.listNewMessagesTo(gameId, playerId);
            boolean newMail = mailList != null && !mailList.isEmpty();            
                          
            if(playerColor == ChessSide.BLACK) {
               ImageIcon icon = whiteIcons.getIcon("whiteMessage");
               
               if(icon != null) {
                  icon.setVisible(newMail);
               }
            } else {
               ImageIcon icon = blackIcons.getIcon("blackMessage");
               
               if(icon != null) {
                  icon.setVisible(newMail);
               }
            }
         }
         //timer.onInterval(); // draw straight away????
         card.addAttribute("chessTimer", timer);
         //card.addHandler("chessBoardContext", chessBoardContext);
         //card.addHandler("chessBoardGame", chessBoardGame);
         //drawLastMoveMade(card, chessBoardGame);
      }
   }   
   
   public abstract MobileApplication getApplication();
   public abstract ChessUserSource getUserSource();
   public abstract ChessGameManager getGameManager();
   public abstract ChessDatabase getGameDatabase();
   public abstract ChessTimeCalculator getTimeCalculator();
}
