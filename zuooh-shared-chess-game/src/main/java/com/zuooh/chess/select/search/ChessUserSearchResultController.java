package com.zuooh.chess.select.search;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnBack;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnMenu;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.TemplateList;
import com.zuooh.application.canvas.TextBox;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessGameController;
import com.zuooh.chess.client.event.operation.ChessUserSearchCommand;
import com.zuooh.chess.client.event.request.ChessUserSearchEvent;
import com.zuooh.chess.client.event.response.ChessUserSearchResultEvent;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserFormatter;
import com.zuooh.chess.database.user.ChessUserStatus;
import com.zuooh.chess.player.ChessUserSummaryContext;
import com.zuooh.chess.timer.ChessTimeDrawer;
import com.zuooh.chess.warning.ChessWarningBuilder;
import com.zuooh.style.Rectangle;
import com.zuooh.style.Style;

@Component
public class ChessUserSearchResultController {

   private static final Logger LOG = LoggerFactory.getLogger(ChessUserSearchResultController.class);

   private final ChessUserSummaryContext summaryContext;
   private final ChessUserSearchContext searchContext;
   private final ChessUserSearchCommand searchCommand;
   private final ChessDatabase gameDatabase;
   private final ChessGameController controller;
   private final ChessBoardContext boardContext;
   private final Screen screen;
   private final TextBox searchBox;

   public ChessUserSearchResultController(
         @Inject("application") MobileApplication application,
         @Inject("gameManager") ChessGameManager gameManager,
         @Inject("gameController") ChessGameController controller,
         @Inject("gameDatabase") ChessDatabase gameDatabase,         
         @Inject("searchCommand") ChessUserSearchCommand searchCommand,
         @Inject("searchContext") ChessUserSearchContext searchContext,
         @Inject("summaryContext") ChessUserSummaryContext summaryContext,
         @Inject("warningBuilder") ChessWarningBuilder warningBuilder,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("playerPanel") Context playerPanel,
         @Inject("results") TemplateList boardList,
         @Inject("searchBox") TextBox searchBox,   
         @Inject("star") Style style)
   {
      ChessUserSearchEvent request = searchCommand.getSearchRequest();
      ChessUserSearchResultEvent result = searchCommand.getSearchResponse();
      List<ChessUser> searchResults = result.getUsers();
      ChessUser currentUser = gameManager.currentUser();
      String currentId = currentUser.getKey();
      int entryCount = 0;
      
      if(playerPanel != null) {
         Panel playerRank = playerPanel.getCanvas("playerRank");
         
         if(playerRank != null) {
            drawPlayerRank(playerPanel, currentUser, playerRank);
         }
         drawProfile(playerPanel, currentUser, style);
      }      
      for(ChessUser user : searchResults) {
         if(user != null) {
            ChessUserData playerData = user.getData();
            ChessUserStatus status = playerData.getStatus();
            String playerId = user.getKey();
            
            if(!playerId.equals(currentId)) { // do not allow yourself to be played against
               Context context = boardList.addLast(playerId, status.statusName);
   
               if(context != null) {
                  drawProfile(context, user, style);
                  entryCount++;
               }
               if(entryCount >= 10) {
                  break; // limit to 10
               }
            }
         }
      }
      String searchTerm = request.getTerm();
      Screen screen = application.getScreen();
      Rectangle size = boardList.getRectangle();
      int fullHeight = screen.screenHeight();
      int remainingSpace = fullHeight - size.height;
      
      if(searchBox != null) {
         searchBox.setText(searchTerm);
      }
      if(remainingSpace > 0) {
         boardList.addLast("border", "border");

         if(remainingSpace > 5) {
            boardList.addLast("filler", "filler", remainingSpace - 5);
         }
      }
      this.searchContext = searchContext;
      this.summaryContext = summaryContext;
      this.boardContext = boardContext;
      this.searchCommand = searchCommand;
      this.gameDatabase = gameDatabase; 
      this.controller = controller;
      this.searchBox = searchBox;
      this.screen = screen;
   }
   
   private void drawProfile(Context context, ChessUser profile, Style style) {
      Panel creationTime = context.getCanvas("lastSeenOnline");
      Panel playerPanel = context.getCanvas("playerName");
      Context skillContext = context.getContext("skillPanel");
      
      if(playerPanel != null) {
         drawPlayerName(context, profile, playerPanel);
      }
      if(skillContext != null) {
         drawSkill(skillContext, profile, style);
      }
      drawPlayerPoints(context, profile);
      
      if(creationTime != null) {
         drawCreationTime(context, profile, creationTime);
      }
      context.addAttribute("player", profile);
   }

   private void drawCreationTime(Context context, ChessUser profile, Panel creationTime) {
      ChessUserData playerData = profile.getData();
      long timeStamp = playerData.getLastSeenOnline();
      long currentTime = System.currentTimeMillis();
      long timeSinceActive = currentTime - timeStamp;
      long oneDay = 1000 * 60 * 60 * 24;
      
      if(timeSinceActive >= oneDay) {
         creationTime.setText("Not active today");
      } else if(timeSinceActive > 1000) {
         String lastSeenTime = ChessTimeDrawer.convertToTextTime(timeSinceActive, true);
         String timeTrimmed = lastSeenTime.trim();

         creationTime.setText("Active " + timeTrimmed + " ago");
      } else {
         creationTime.setText("Currently active");
      }
   }

   private void drawPlayerRank(Context context, ChessUser profile, Panel playerRank) {
      ChessUserData playerData = profile.getData();
      int rank = playerData.getRank();
      playerRank.setText("Rank " + rank);      
   }
   
   private void drawPlayerPoints(Context context, ChessUser profile) {
     // Panel playerPoints = context.getCanvas("playerPoints");
    //  int opponentPoints = profile.getPlayerPoints();

      //playerPoints.setText(String.valueOf(opponentPoints));
   }

   private void drawPlayerName(Context context,  ChessUser profile, Panel playerPanel) {      
      String playerId = profile.getKey();
      String playerName = ChessUserFormatter.formatUser(profile);

      if(playerName != null) {
         playerName = playerName.trim();
         playerPanel.setText(playerName);
      } else {
         playerPanel.setText(playerId);
      }
   }

   private void drawSkill(Context context,  ChessUser profile, Style style) {
      ChessUserData playerData = profile.getData();
      int opponentSkillLevel = playerData.getSkill();

      for(int i = 1; i <= opponentSkillLevel; i++) {
         Panel starPanel = context.getCanvas("star" + i);
         
         if(starPanel != null) {
            starPanel.setStyle(style);
         }
      }
   }

   @OnMenu
   public void showMenu() {
      screen.showPage("home");
   }

   @OnBack
   public void showNewGames() {
      screen.showPage("createGame");
   }

   @OnCall
   public void focusSearch() {
      searchContext.setFocusRequired(true);
      searchCommand.reset();
      screen.showPage("search");
   }
   
   @OnCall
   public void showScoreProfile(ChessUser profile) {
      try {
         summaryContext.setUser(profile);
         screen.showPage("player");
      }catch(Exception e) {
         LOG.info("Could show player summary", e);
      }  
   }

   @OnCall
   public void startSpecificGame(ChessUser profile) {
      try {
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
         ChessSide playerColor = searchContext.getUserSide();
         ChessSide opponentsColor = playerColor.oppositeSide();
         String gameId = boardContext.getGameId();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
         String opponentId = profile.getKey();
        
         boardGame.setOnlineGame(true);
         boardGame.setOpponentId(opponentId);
         boardContext.setOpponentSide(opponentsColor);
         boardDatabase.saveBoardGame(boardGame);
         screen.showPage("durationOfGame");
      }catch(Exception e) {
         LOG.info("Could not start a specific game", e);
      }
   }
}
