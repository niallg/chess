package com.zuooh.chess.select;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.store.Inventory;
import com.zuooh.application.store.ProductStore;
import com.zuooh.application.store.PurchaseStatus;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessGameQuota;
import com.zuooh.chess.control.state.ChessUpgradeProduct;

@Component
public class ChessCreateGameQuotaController {

   private final ChessGameValidationController validator;
   private final ChessBoardContext boardContext;
   private final MobileApplication application;   
   private final Screen screen;

   public ChessCreateGameQuotaController(
         @Inject("validationController") ChessGameValidationController validator,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("application") MobileApplication application,
         @Inject("screen") Screen screen)
   {
      this.boardContext = boardContext;
      this.application = application;
      this.validator = validator;
      this.screen = screen;
   }
   
   @OnStart
   public void checkGames() throws Exception {
      ChessUpgradeProduct product = boardContext.getUpgradeProduct();
      String productId = product.getProductId();
      
      if(productId != null) {
         ProductStore store = application.getProductStore();
         Inventory inventory = store.loadInventory();
         PurchaseStatus status = inventory.getPurchaseStatus(productId); 
         ChessGameQuota gameQuota = null;
               
         if(status.isPurchased()) {
            product.setUpgraded(true);
         }
         if(product.isUpgraded()) {
            gameQuota = product.getUpgradeQuota();
         } else {
            gameQuota = product.getFreeQuota();            
         }
         int savedCount = validator.countGamesToResume();
         int allowedGames = gameQuota.getTotalGames();
         
         if(savedCount >= allowedGames) {
            screen.showPage("createGameSkip");
         } else {
            screen.showPage("createGameSelect");         
         }
      }
   } 
}
