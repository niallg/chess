package com.zuooh.chess.select;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Dialog;
import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnBack;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.bind.annotation.OnMenu;
import com.zuooh.application.canvas.Card;
import com.zuooh.application.canvas.Deck;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.theme.ChessThemeDatabase;
import com.zuooh.chess.database.theme.ChessThemeDefinition;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.timer.ChessTimeCalculator;
import com.zuooh.chess.timer.ChessTimer;
import com.zuooh.style.Orientation;
import com.zuooh.style.Style;

@Component
public class ChessResumeGameFocusController extends ChessSelectGameController {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessResumeGameFocusController.class);

   private final AtomicReference<ChessBoardGame> currentGame;
   private final AtomicReference<ChessTimer> currentTimer;
   private final Map<String, ChessBoardGame> savedGames;
   private final ChessTimeCalculator timeCalculator;
   private final ChessUserSource userSource;
   private final ChessDatabase gameDatabase;
   private final ChessBoardContext boardContext;
   private final ChessGameManager gameManager;  
   private final Deck<Card> themeBoards;
   private final MobileApplication application;
   private final Document document;
   private final Window window;
   private final Screen screen;
   private final Style blank;
   private final Style star;

   public ChessResumeGameFocusController(
         @Inject("application") MobileApplication application,
         @Inject("document") Document document,
         @Inject("userSource") ChessUserSource userSource,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("gameManager") ChessGameManager gameManager,
         @Inject("boardContext") ChessBoardContext boardContext,        
         @Inject("themeBoards") Deck<Card> themeBoards,
         @Inject("blank") Style blank,
         @Inject("star") Style star)
   {
      this.timeCalculator = new ChessTimeCalculator(gameDatabase);
      this.currentGame = new AtomicReference<ChessBoardGame>();
      this.currentTimer = new AtomicReference<ChessTimer>();
      this.savedGames = new HashMap<String, ChessBoardGame>();
      this.window = application.getWindow();
      this.screen = application.getScreen();   
      this.userSource = userSource;
      this.gameDatabase = gameDatabase;
      this.gameManager = gameManager;
      this.boardContext = boardContext;
      this.themeBoards = themeBoards;
      this.application = application;
      this.document = document;
      this.blank = blank;
      this.star = star;
   }
      
   
   @Override
   public long onInterval() throws Exception {
      ChessTimer timer = currentTimer.get();

      if(timer != null) {
         return timer.onInterval();
      }
      return 1000;
   }

   @OnMenu
   @OnBack
   public void showMenu() {
      screen.showPage("home");
   }
   
   @Override
   public MobileApplication getApplication() {
      return application;
   }
   
   @Override
   public ChessTimeCalculator getTimeCalculator() {
      return timeCalculator;
   }     
   
   @Override
   public ChessUserSource getUserSource() {
      return userSource;
   }
   
   @Override
   public ChessGameManager getGameManager() {
      return gameManager;
   }
   
   @Override
   public ChessDatabase getGameDatabase() {
      return gameDatabase;
   }

   @OnCreate
   public void loadSavedGames() throws Exception {
      ChessUser currentProfile = gameManager.currentUser();
      String playerId = currentProfile.getKey();         
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      List<ChessBoardGame> currentGames = boardDatabase.loadBoardGames(playerId);

      for(ChessBoardGame boardGame : currentGames) {
         String gameId = boardGame.getGameId();        
         long gameDuration = boardGame.getGameDuration();
         long whiteTimeElapsed = timeCalculator.calculateElapsedTime(gameId, ChessSide.WHITE);
         long blackTimeElapsed = timeCalculator.calculateElapsedTime(gameId, ChessSide.BLACK);
         long whiteTimeRemaining = gameDuration - whiteTimeElapsed;
         long blackTimeRemaining = gameDuration - blackTimeElapsed;
            
         if(whiteTimeRemaining > 0 && blackTimeRemaining > 9) { // is time up
            if(savedGames.isEmpty()) {
               if(acceptGame(boardGame)) {
                  savedGames.put(gameId, boardGame);
               }
            } else {
               savedGames.put(gameId, boardGame);
            }
         } else {
            if(gameDuration > 0) {
               LOG.info("Deleting game " + gameId + " as black time is " + blackTimeRemaining + " and white time is " + whiteTimeRemaining + " and game duration is " + gameDuration);                   
               gameManager.deleteGame(gameId); // game has expired
            }
         }      
      }
   }

   @OnCall
   public void showGame(String gameId) throws Exception {          
      ChessBoardGame boardGame = savedGames.get(gameId);
      
      if(boardGame != null) {
         if(acceptGame(boardGame)) {
            drawGame(boardGame);
         } else {
            screen.showPage("resumeGameContinue");               
         }
      } else {
         screen.showPage("resumeGameContinue");    
      }      
      currentGame.set(boardGame);
      window.invalidate();
   }

   private void drawGame(ChessBoardGame boardGame) throws Exception {      
      String gameId = boardGame.getGameId();      
      Screen screen = application.getScreen();
      Orientation orientation = screen.orientation();
      ChessSide playerColor = boardGame.getUserSide();
      boolean youAreWhite = playerColor == ChessSide.WHITE;
      String themeColor = boardGame.getThemeColor();
      String cardName = themeColor + (youAreWhite ? "Card" : "FlipCard");
      Card card = themeBoards.getCanvas(cardName);           
      
      if(card == null) {
         throw new IllegalStateException("Could not find card "+ cardName + " for game " + gameId + " and theme " + themeColor);
      }
      if(card != null) {
         drawGame(boardGame, document, card);
         
         if(orientation == Orientation.PORTRAIT) {
            drawShortSummary(card, boardGame);
         }
         ChessTimer timer = card.getAttribute("chessTimer");
         
         if(timer != null) {
            timer.onInterval();
         }
         drawSkill(card, boardGame, star, blank);
         currentGame.set(boardGame);
         currentTimer.set(timer);
      }
      themeBoards.setTop(cardName);      
   }
   
   @OnCall
   public void showOpponent(Toggle toggle) throws Exception {
      ChessBoardGame boardGame = currentGame.get();
      ChessTimer boardTimer = currentTimer.get();
      ChessSide playerColor = boardGame.getUserSide();
      ChessSide opponentColor = playerColor.oppositeSide();

      if(boardTimer != null) {
         boardTimer.showPlayer(opponentColor.name);
      }
      toggle.toggle();
      window.invalidate();
   }    

   @OnCall
   public void continueGame(Toggle toggle) throws Exception {
      ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();
      ChessBoardGame boardGame = currentGame.get();
      String themeColor = boardGame.getThemeColor();
      ChessThemeDefinition themeDefinition = themeDatabase.loadThemeDefinition(themeColor);
      String gameId = boardGame.getGameId();
      ChessPlayer opponentPlayer = gameManager.loadGame(gameId);
      ChessSide playerColor = boardGame.getUserSide();
      ChessSide opponentColor = playerColor.oppositeSide();
      String screenName = themeDefinition.getScreen(playerColor);
      boolean gameOnline = boardGame.isOnlineGame();
      long gameDuration = boardGame.getGameDuration();
      
      boardContext.setGameDuration(gameDuration);
      boardContext.setGameOnline(gameOnline);
      boardContext.setOpponentSide(opponentColor);
      boardContext.setOpponent(opponentPlayer);
      boardContext.setPlayer(null);
      boardContext.setGameId(gameId);
      boardContext.setThemeColor(themeColor);
      boardContext.setGamePage(screenName);
      screen.showPage(screenName); 
      toggle.toggle();
   }   
}
