package com.zuooh.chess.select;

import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;

@Component
public class ChessSkipGameController {

   private final ChessGameValidationController validator;
   private final Screen screen;

   public ChessSkipGameController(
         @Inject("validationController") ChessGameValidationController validator,
         @Inject("screen") Screen screen)
   {
      this.validator = validator;
      this.screen = screen;
   }
   
   @OnStart
   public void checkGames() throws Exception {
      if(validator.isThereAnyGamesToAccept()) {
         screen.showPage("resumeGameAccept");  
      } else if(validator.isThereAnyGamesToResume()) {
         screen.showPage("resumeGameContinue");
      } else {
         screen.showPage("resumeGameSkip");
      }
   } 
}
