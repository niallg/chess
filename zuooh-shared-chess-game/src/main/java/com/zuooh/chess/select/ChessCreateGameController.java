package com.zuooh.chess.select;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnBack;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnCreate;
import com.zuooh.application.bind.annotation.OnMenu;
import com.zuooh.application.canvas.Deck;
import com.zuooh.application.store.Inventory;
import com.zuooh.application.store.ProductStore;
import com.zuooh.application.store.PurchaseStatus;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessGameQuota;
import com.zuooh.chess.control.state.ChessUpgradeProduct;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.select.search.ChessUserSearchContext;

@Component
public class ChessCreateGameController {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessCreateGameController.class);   

   private final ChessGameValidationController validator;
   private final ChessUserSearchContext searchContext;
   private final ChessGameKeyGenerator keyGenerator;
   private final ChessBoardContext boardContext;   
   private final ChessDatabase gameDatabase;
   private final ChessBoardGame boardGame;
   private final MobileApplication application;
   private final Deck whiteRandomDeck;
   private final Deck whiteComputerDeck;     
   private final Deck whiteSearchDeck;        
   private final Deck blackRandomDeck;
   private final Deck blackComputerDeck;     
   private final Deck blackSearchDeck;     
   private final Screen screen;

   public ChessCreateGameController(
         @Inject("application") MobileApplication application,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase, 
         @Inject("searchContext") ChessUserSearchContext searchContext,
         @Inject("validationController") ChessGameValidationController validator,         
         @Inject("whiteRandomButtons") Deck whiteRandomDeck,
         @Inject("whiteComputerButtons") Deck whiteComputerDeck,         
         @Inject("whiteSearchButtons") Deck whiteSearchDeck,
         @Inject("blackRandomButtons") Deck blackRandomDeck,
         @Inject("blackComputerButtons") Deck blackComputerDeck,         
         @Inject("blackSearchButtons") Deck blackSearchDeck)         
   { 
      this.keyGenerator = new ChessGameKeyGenerator(boardContext);
      this.boardGame = new ChessBoardGame();
      this.screen = application.getScreen();
      this.gameDatabase = gameDatabase;
      this.searchContext = searchContext;
      this.boardContext = boardContext;
      this.application = application;
      this.whiteRandomDeck = whiteRandomDeck;
      this.whiteSearchDeck = whiteSearchDeck;
      this.whiteComputerDeck = whiteComputerDeck;
      this.blackRandomDeck = blackRandomDeck;
      this.blackSearchDeck = blackSearchDeck;
      this.blackComputerDeck = blackComputerDeck;      
      this.validator = validator;
   }
   
   @OnCreate
   public void onCreate() {
      ChessUpgradeProduct product = boardContext.getUpgradeProduct();
      String productId = product.getProductId();
      ChessGameQuota quota = product.getUpgradeQuota();
      
      if(!product.isUpgraded()) {
         ProductStore store = application.getProductStore();
         Inventory inventory = store.loadInventory();
         PurchaseStatus status = inventory.getPurchaseStatus(productId);
         
         if(status.isPurchased()) {
            product.setUpgraded(true); // ensure not to check again!!
         }
         if(product.isUpgraded()) {
            quota = product.getUpgradeQuota();
         } else {
            quota = product.getFreeQuota();
         }
      }
      try {
         int whiteGames = validator.countGamesToResume(ChessSide.WHITE);
         int blackGames = validator.countGamesToResume(ChessSide.BLACK);
         int allowedWhiteGames = quota.getWhiteGames();
         int allowedBlackGames = quota.getBlackGames();
         
         if(whiteGames >= allowedWhiteGames) {
            whiteSearchDeck.setTop("locked");
            whiteRandomDeck.setTop("locked");
            whiteComputerDeck.setTop("locked");         
         } else {
            whiteSearchDeck.setTop("available");
            whiteRandomDeck.setTop("available");
            whiteComputerDeck.setTop("available");
         }
         if(blackGames >= allowedBlackGames) {
            blackSearchDeck.setTop("locked");
            blackRandomDeck.setTop("locked");
            blackComputerDeck.setTop("locked");         
         } else {
            blackSearchDeck.setTop("available");
            blackRandomDeck.setTop("available");
            blackComputerDeck.setTop("available");
         }
      } catch(Exception e) {
         LOG.warn("Error counting games", e);        
      }
   }   

   @OnMenu
   @OnBack
   public void showMenu() throws Exception {
      screen.showPage("home");
   }

   @OnCall
   public void startWhiteGameAgainstComputer(boolean locked) throws Exception {
      if(continueGameCreation(locked)) {
         String gameId = createNewGame(ChessSide.WHITE, false);      
         
         boardContext.setGameId(gameId);
         boardContext.setGameRandom(false);
         boardContext.setGameOnline(false);
         boardContext.setOpponentSide(ChessSide.BLACK);
         screen.showPage("computerStrength");
      }
   }

   @OnCall
   public void startBlackGameAgainstComputer(boolean locked) throws Exception {
      if(continueGameCreation(locked)) {
         String gameId = createNewGame(ChessSide.BLACK, false);
         
         boardContext.setGameId(gameId);
         boardContext.setGameRandom(false);      
         boardContext.setGameOnline(false);
         boardContext.setOpponentSide(ChessSide.WHITE); 
         screen.showPage("computerStrength");
      }
   }

   @OnCall
   public void searchForOnlineWhiteGame(boolean locked) throws Exception {
      if(continueGameCreation(locked)) {
         String gameId = createNewGame(ChessSide.WHITE, true);
         
         boardContext.setGameId(gameId);
         boardContext.setGameRandom(false);
         boardContext.setGameOnline(true);
         boardContext.setOpponentSide(ChessSide.BLACK); 
         searchContext.setUserSide(ChessSide.WHITE);
         screen.showPage("search");
      }
   }

   @OnCall
   public void searchForOnlineBlackGame(boolean locked) throws Exception {
      if(continueGameCreation(locked)) {
         String gameId = createNewGame(ChessSide.BLACK, true);
         
         boardContext.setGameId(gameId);
         boardContext.setGameRandom(false);
         boardContext.setGameOnline(true);
         boardContext.setOpponentSide(ChessSide.WHITE); 
         searchContext.setUserSide(ChessSide.BLACK);
         screen.showPage("search");
      }
   }

   @OnCall
   public void createNewWhiteGame(boolean locked) throws Exception {
      if(continueGameCreation(locked)) {
         String gameId = createNewGame(ChessSide.WHITE, true);
         
         boardContext.setGameId(gameId);
         boardContext.setGameRandom(true);
         boardContext.setGameOnline(true);
         boardContext.setOpponentSide(ChessSide.BLACK);            
         screen.showPage("durationOfGame");
      }
   }

   @OnCall
   public void createNewBlackGame(boolean locked) throws Exception {
      if(continueGameCreation(locked)) {
         String gameId = createNewGame(ChessSide.BLACK, true);
         
         boardContext.setGameId(gameId);
         boardContext.setGameRandom(true);
         boardContext.setGameOnline(true);
         boardContext.setOpponentSide(ChessSide.WHITE);            
         screen.showPage("durationOfGame");
      }
   }
   
   private String createNewGame(ChessSide side, boolean online) throws Exception {
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessUser currentProfile = boardContext.getBoardUser();
      String playerId = currentProfile.getKey();     
      String gameId = keyGenerator.createGameId(side, online);
      long time = System.currentTimeMillis();
      
      boardGame.setCreationTime(time);
      boardGame.setUserSide(side);
      boardGame.setOnlineGame(online);
      boardGame.setUserId(playerId);
      boardGame.setGameId(gameId);
      boardDatabase.saveBoardGame(boardGame);
      
      return gameId;
   }   
   
   private boolean continueGameCreation(boolean locked) throws Exception {
      ChessUpgradeProduct product = boardContext.getUpgradeProduct();
      
      if(locked) {
         if(product.isUpgraded()) { /* just ran out of quota */
            boardContext.setUpgradeFinishPage(null);
            boardContext.setUpgradePurchasePage(null);
            screen.showPage("createGameSkip");            
         } else {
            boardContext.setUpgradeFinishPage("createGame");
            boardContext.setUpgradePurchasePage("upgradeAdvertFree");
            screen.showPage("upgradeMoreGames");  
         }        
         return false;
      }   
      return true;
   }
}
