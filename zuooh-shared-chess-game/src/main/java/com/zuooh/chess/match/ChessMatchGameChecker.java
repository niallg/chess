package com.zuooh.chess.match;

import com.zuooh.application.Dialog;
import com.zuooh.application.Screen;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.bind.annotation.Required;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.client.event.operation.ChessMatchGameCommand;
import com.zuooh.chess.client.event.response.ChessNewGameEvent;
import com.zuooh.chess.control.ChessBoardPlayerProvider;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.theme.ChessThemeDatabase;
import com.zuooh.chess.database.theme.ChessThemeDefinition;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.task.ChessTaskController;
import com.zuooh.message.client.ResponseStatus;

@Component
public class ChessMatchGameChecker extends ChessTaskController {
   
   @Required
   @Inject("gameDatabase")
   private ChessDatabase gameDatabase;

   @Required
   @Inject("matchCommand")
   private ChessMatchGameCommand matchCommand;
   
   @Required
   @Inject("playerProvider") 
   private ChessBoardPlayerProvider playerProvider;
   
   @Required
   @Inject("boardContext") 
   private ChessBoardContext boardContext;
   
   @Required
   @Inject("screen")
   private Screen screen;
   
   @Override
   public void onBeforeStart() throws Exception {
      long gameDuration = boardContext.getGameDuration();
      ChessSide opponentSide = boardContext.getOpponentSide();
      ChessSide userSide = opponentSide.oppositeSide();
      ChessUser user = boardContext.getBoardUser();
      ChessUserData userData = user.getData();
      String userId = user.getKey();
      int userSkill = userData.getSkill();
      
      matchCommand.execute(userSide, gameDuration, userId, userSkill);
   }

   @Override
   public long onInterval() throws Exception {
      ResponseStatus status = matchCommand.getStatus();
      
      if(status != ResponseStatus.NONE && status != ResponseStatus.SENDING) {
         if(status == ResponseStatus.TIMEOUT) {
            showWarning("Error", "Unable to create game due to network delays!");              
         } else if(status == ResponseStatus.NOT_CONNECTED) {
            showWarning("Error", "Connection to internet is currently not available!");               
         } else if(status == ResponseStatus.ERROR) {
            showWarning("Error", "Error occured trying to find opponent to play!");           
         } else if(status == ResponseStatus.SUCCESS) {
            ChessNewGameEvent result = matchCommand.getGameResponse();
            
            if(result == null) {
               ChessSide opponentSide = boardContext.getOpponentSide();
               
               if(opponentSide == ChessSide.WHITE) {
                  screen.showPage("themeFlip");
               } else {
                  screen.showPage("theme");
               }
            } else {
               ChessGame game = result.getGame();
               String opponentId = game.getOwnerId();
               String newId = game.getGameId();            
               String originalId = boardContext.getGameId();
               ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
               ChessBoardGame boardGame = boardDatabase.loadBoardGame(originalId); 
               
               boardDatabase.deleteBoardGame(boardGame); // delete from database               
               
               ChessGameCriteria gameCriteria = game.getCriteria();
               ChessSide opponentSide = boardContext.getOpponentSide();
               ChessSide userSide = opponentSide.oppositeSide();
               String themeColor = gameCriteria.getThemeColor();
               ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();               
               ChessThemeDefinition themeDefinition = themeDatabase.loadThemeDefinition(themeColor);
               long gameDuration = gameCriteria.getGameDuration();
               
               boardGame.setOpponentId(opponentId);
               boardGame.setGameId(newId);
               boardGame.setThemeColor(themeColor);
               boardGame.setGameDuration(gameDuration);
               boardDatabase.saveBoardGame(boardGame);
               
               ChessPlayer mockPlayer = playerProvider.createMockPlayer(boardGame);
               ChessPlayer opponentPlayer = playerProvider.createPlayer(newId);
               String screenName = themeDefinition.getScreen(userSide);
               
               boardContext.setGameId(newId);
               boardContext.setThemeColor(themeColor);
               boardContext.setPlayer(mockPlayer);                
               boardContext.setOpponent(opponentPlayer);
               boardContext.setGamePage(screenName);                  
               screen.showPage(screenName);               
            }
         }
         matchCommand.reset();   
      }
      return 1000;
   }     
   
   @Override
   public void onBeforeStop() {
      matchCommand.reset();
   }
   
   public void showWarning(String title, String message) {
      Dialog dialog = screen.openDialog("warningDialog");
      
      dialog.setAttribute("warningTitle", title);
      dialog.setAttribute("warningMessage", message);
      
      ChessSide opponentSide = boardContext.getOpponentSide();
      
      if(opponentSide == ChessSide.WHITE) {
         dialog.setAttribute("warningFunction", "screen.showPage('themeFlip')");
      } else {
         dialog.setAttribute("warningFunction", "screen.showPage('theme')");
      }      
      dialog.setAttribute("warningButton", "Okay");
      dialog.showDialog();
   }
}
