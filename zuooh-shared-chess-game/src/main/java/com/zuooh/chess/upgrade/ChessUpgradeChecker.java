package com.zuooh.chess.upgrade;

import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Dialog;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.store.Inventory;
import com.zuooh.application.store.InventoryResponse;
import com.zuooh.application.store.ProductStore;
import com.zuooh.application.store.ProductStoreResult;
import com.zuooh.application.store.ProductStoreState;
import com.zuooh.application.store.Purchase;
import com.zuooh.application.store.PurchaseType;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessUpgradeProduct;
import com.zuooh.chess.task.ChessTaskController;

@Component
public class ChessUpgradeChecker extends ChessTaskController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessUpgradeChecker.class);

   private final AtomicReference<InventoryResponse> inventoryResponse;
   private final ChessBoardContext boardContext;
   private final MobileApplication application;
   private final Panel messagePanel;
   private final Screen screen;

   public ChessUpgradeChecker(
         @Inject("application") MobileApplication application,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("message") Panel messagePanel)
   {
      this.inventoryResponse = new AtomicReference<InventoryResponse>();
      this.screen = application.getScreen();
      this.boardContext = boardContext;
      this.application = application;   
      this.messagePanel = messagePanel;
   }   
   
   @Override
   public void onBeforeStart() throws Exception {
      try {
         ChessUpgradeProduct product = boardContext.getUpgradeProduct();
         String productId = product.getProductId();
         ProductStore store = application.getProductStore();         
         InventoryResponse response = store.queryInventory(PurchaseType.SINGLE, productId);
         
         inventoryResponse.set(response);
      }catch(Exception e) {
         LOG.info("Could not upgrade", e);
      }
   }   
   
   @Override
   public long onInterval() throws Exception {
      InventoryResponse response = inventoryResponse.get();

      if(response != null) {
         ProductStoreState state = response.getState();
    
         if(state == ProductStoreState.ERROR) {
            onError(response);
         } else if(state == ProductStoreState.DISPOSED) {
            onError(response);
         } else if(state == ProductStoreState.READY){
            onReady(response);            
         } else if(state == ProductStoreState.REFRESHING_PRODUCTS) {
            onRefreshingInventory(response);
         }else if(state == ProductStoreState.REFRESHING_PURCHASES) {
            onRefreshingInventory(response);
         } else {
            onProcessing(response);
         }
      }
      return 1000;       
   }

   private void onError(InventoryResponse response) {
      onWarningDialog("Error", "Connection error checking product upgrade status, try again later!");  
   }
   
   private void onReady(InventoryResponse response) {
      Inventory inventory = response.getInventory();           
      ChessUpgradeProduct product = boardContext.getUpgradeProduct();
      String productId = product.getProductId();    
      Purchase purchase = inventory.getPurchase(productId);           
      
      if(purchase != null) {
         product.setUpgraded(true); // the product was purchased
         screen.showPage("upgradeSuccess");               
      } else {
         ProductStoreResult result = response.getResult();
         String purchasePage = boardContext.getUpgradePurchasePage();   
         
         if(result.isSuccess()) { // query was successful and you do not own it!!
            screen.showPage(purchasePage);
         } else {
            onWarningDialog("Warning", "Unable to get product upgrade status, try again later! ");                   
         }
      } 
   }
   
   private void onRefreshingInventory(InventoryResponse response) {  
      long elapsedTime = response.getTimeElapsed();
      
      if(elapsedTime > 60000) {
         onWarningDialog("Warning", "Response from store is taking too long, try again later!");            
      }
   }
   
   private void onProcessing(InventoryResponse response) {  
      long elapsedTime = response.getTimeElapsed();
      
      if(elapsedTime > 20000) {
         onWarningDialog("Warning", "Connecting to store is taking too long, try again later!");            
      }
   }      
   
   private void onWarningDialog(String title, String message) {
      Dialog dialog = screen.openDialog("warningDialog");
      
      dialog.setAttribute("warningTitle", title);
      dialog.setAttribute("warningMessage", message);
      dialog.setAttribute("warningButton", "Okay");
      
      String finishPage = boardContext.getUpgradeFinishPage();
      
      if(finishPage != null) {
         dialog.setAttribute("warningFunction", "screen.showPage('" + finishPage + "')");
      } else {
         dialog.setAttribute("warningFunction", "screen.showPrevious()");
      }
      if(messagePanel != null) {
         messagePanel.setText(null);
      }
      inventoryResponse.set(null); // stop checking!!!
      dialog.showDialog();
   }
}
