package com.zuooh.chess.upgrade;

import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Dialog;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.store.ProductStore;
import com.zuooh.application.store.ProductStoreResult;
import com.zuooh.application.store.ProductStoreState;
import com.zuooh.application.store.Purchase;
import com.zuooh.application.store.PurchaseResponse;
import com.zuooh.application.store.PurchaseType;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessUpgradeProduct;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.task.ChessTaskController;

@Component
public class ChessUpgradeController extends ChessTaskController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessUpgradeController.class);

   private final AtomicReference<PurchaseResponse> purchaseResponse;
   private final ChessBoardContext boardContext;
   private final MobileApplication application;
   private final Screen screen;

   public ChessUpgradeController(
         @Inject("application") MobileApplication application,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("message") Panel panel)
   {
      this.purchaseResponse = new AtomicReference<PurchaseResponse>();
      this.screen = application.getScreen();
      this.boardContext = boardContext;
      this.application = application;      
   }
   
   
   @Override
   public long onInterval() throws Exception {
      PurchaseResponse response = purchaseResponse.get();
      
      if(response != null) {
         ProductStoreState state = response.getState();
    
         if(state == ProductStoreState.ERROR) {
            onError(response);
         } else if(state == ProductStoreState.DISPOSED) {
            onError(response);
         } else if(state == ProductStoreState.READY){
            onReady(response);            
         } else if(state == ProductStoreState.REFRESHING_PRODUCTS) {
            onRefreshingInventory(response);
         }else if(state == ProductStoreState.REFRESHING_PURCHASES) {
            onRefreshingInventory(response);
         } else if(state == ProductStoreState.PURCHASE_PROMPT_VISIBLE){ // may take a long time
            onPurchasePromptVisible(response);
         } else {
            onProcessing(response); // this is just processing required
         }
      }
      return 1000;       
   }

   private void onError(PurchaseResponse response) {
      onWarningDialog("Warning", "Could not connect to store to perform upgrade, try again later!");  
   }
   
   private void onReady(PurchaseResponse response) {      
      ProductStoreResult result = response.getResult();
            
      if(result.isSuccess()) { // purchased!!
         screen.showPage("upgradeSuccess");               
      } else {
         onWarningDialog("Warning", "Upgrade was not successful at this time, please try again later!");  
      }            
   }
   
   private void onRefreshingInventory(PurchaseResponse response) {  
      long elapsedTime = response.getTimeElapsed();
      
      if(elapsedTime > 60000) {
         onWarningDialog("Warning", "Response from store is taking too long, please try again later!");            
      }
   }
   
   private void onPurchasePromptVisible(PurchaseResponse response) {
      long elapsedTime = response.getTimeElapsed();
      
      if(elapsedTime > 300000) {
         onWarningDialog("Warning", "Purchase is taking too long to complete, it will finish in the background!");
      }
   }   
   
   private void onProcessing(PurchaseResponse response) {  
      long elapsedTime = response.getTimeElapsed();
      
      if(elapsedTime > 60000) {
         onWarningDialog("Warning", "Processing upgrade is taking too long, please try again later!");            
      }
   } 
   
   private void onWarningDialog(String title, String message) {
      Dialog dialog = screen.openDialog("warningDialog");
      
      dialog.setAttribute("warningTitle", title);
      dialog.setAttribute("warningMessage", message);
      dialog.setAttribute("warningButton", "Okay");
      
      String finishPage = boardContext.getUpgradeFinishPage();
      
      if(finishPage != null) {
         dialog.setAttribute("warningFunction", "screen.showPage('" + finishPage + "')");
      } else {
         dialog.setAttribute("warningFunction", "screen.showPrevious()");
      }
      purchaseResponse.set(null); // stop checking!!!
      dialog.showDialog();
   }

   @OnCall
   public void upgradeNow() { 
      try {         
         PurchaseResponse response = purchaseResponse.get();
         
         if(response == null) {
            ChessUpgradeProduct product = boardContext.getUpgradeProduct();
            String productId = product.getProductId(); 
            ProductStore store = application.getProductStore();         
            
            response = store.makePurchase(PurchaseType.SINGLE, productId, "chess");
            product.promptShown();
            purchaseResponse.set(response);
         }
      }catch(Exception e) {
         LOG.info("Could not upgrade", e);
      }
   }

   @OnCall
   public void skipUpgrade() {
      ChessUpgradeProduct product = boardContext.getUpgradeProduct();
      String finishPage = boardContext.getUpgradeFinishPage();
      
      product.promptShown();
      boardContext.setUpgradePurchasePage(null);
      boardContext.setUpgradeFinishPage(null);
      screen.showPage(finishPage);
   }
}

