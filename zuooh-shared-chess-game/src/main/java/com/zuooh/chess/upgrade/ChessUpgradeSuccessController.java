package com.zuooh.chess.upgrade;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.canvas.Deck;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.control.state.ChessUpgradeProduct;

@Component
public class ChessUpgradeSuccessController {
   
   private final ChessBoardContext boardContext;
   private final Screen screen;

   public ChessUpgradeSuccessController(
         @Inject("application") MobileApplication application,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("iconDeck") Deck iconDeck)
   {
      String purchasePage = boardContext.getUpgradePurchasePage();      
     
      if(purchasePage != null && purchasePage.equals("upgradeAdvertFree")) {
         iconDeck.setTop("advertFree");
      } else {
         iconDeck.setTop("unlock");
      }
      this.screen = application.getScreen();
      this.boardContext = boardContext;   
   }

   @OnCall
   public void finishUpgrade(Toggle toggle) {
      String finishPage = boardContext.getUpgradeFinishPage();
      ChessUpgradeProduct product = boardContext.getUpgradeProduct();

      product.setUpgraded(true); // it should be upgraded now?????
      boardContext.setUpgradePurchasePage(null);
      boardContext.setUpgradeFinishPage(null);
      screen.showPage(finishPage);
      toggle.toggle();
   }
}

