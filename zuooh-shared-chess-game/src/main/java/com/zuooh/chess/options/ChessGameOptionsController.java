package com.zuooh.chess.options;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.bind.annotation.OnBack;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnMenu;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;

@Component
public class ChessGameOptionsController {

   private final ChessGameOptions options;
   private final ChessGameOptions edit;
   private final Screen screen;

   public ChessGameOptionsController(
         @Inject("application") MobileApplication application,
         @Inject("options") ChessGameOptions options)
   {
      this.edit = new ChessGameOptions();
      this.screen = application.getScreen();
      this.edit.setOptions(options);
      this.options = options;
   }

   @OnBack
   @OnMenu
   public void showMenu() {
      screen.showPrevious();
   }

   @OnCall
   public void saveOptions() {
      options.setOptions(edit);
      showMenu();
   }

   @OnCall
   public void showMoveHighlight(boolean show) {
      edit.setShowMoveHighlight(show);
   }

   @OnCall
   public void enableSound(boolean enable) {
      edit.setEnableSound(enable);
   }

   @OnCall
   public void enableChat(boolean enable) {
      edit.setEnableChat(enable);
   }

   @OnCall
   public void allowChatPopUp(boolean allow) {
      edit.setAllowChatPopUp(allow);
   }
}
