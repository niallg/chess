package com.zuooh.chess.options;

public class ChessGameOptions {

   private boolean enableSound;
   private boolean enableChat;
   private boolean allowChatPopUp;
   private boolean showMoveHighlight;

   public ChessGameOptions() {
      this.enableSound = true;
      this.enableChat = true;
      this.allowChatPopUp = true;
      this.showMoveHighlight = true;
   }

   public void setOptions(ChessGameOptions options) {
      this.enableSound = options.enableSound;
      this.enableChat = options.enableChat;
      this.allowChatPopUp = options.allowChatPopUp;
      this.showMoveHighlight = options.showMoveHighlight;
   }

   public boolean isEnableSound() {
      return enableSound;
   }

   public void setEnableSound(boolean enableSound) {
      this.enableSound = enableSound;
   }

   public boolean isEnableChat() {
      return enableChat;
   }

   public void setEnableChat(boolean enableChat) {
      this.enableChat = enableChat;
   }

   public boolean isAllowChatPopUp() {
      return allowChatPopUp;
   }

   public void setAllowChatPopUp(boolean allowChatPopUp) {
      this.allowChatPopUp = allowChatPopUp;
   }

   public boolean isShowMoveHighlight() {
      return showMoveHighlight;
   }

   public void setShowMoveHighlight(boolean showMoveHighlight) {
      this.showMoveHighlight = showMoveHighlight;
   }
}
