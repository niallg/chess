package com.zuooh.chess.control;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Window;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.theme.ChessThemePiece;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.options.ChessGameOptions;
import com.zuooh.chess.score.ChessScorePanel;
import com.zuooh.chess.skill.ChessSkillPanel;
import com.zuooh.chess.timer.ChessTimer;
import com.zuooh.chess.view.ChessBoardCellView;
import com.zuooh.chess.view.ChessBoardPanel;

@Component
public class ChessBoardEditController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardEditController.class);

   private final ChessBoardPanel boardPanel;
   private final ChessBoard chessBoard;
   private final ChessScorePanel scorePanel;
   private final ChessGameOptions options;
   private final Window window;

   public ChessBoardEditController(
         @Inject("eventService") ChessBoardEventScheduler eventScheduler,
         @Inject("boardPanel") ChessBoardPanel boardPanel,
         @Inject("scorePanel") ChessScorePanel scorePanel,
         @Inject("skillPanel") ChessSkillPanel skillPanel,
         @Inject("application") MobileApplication application,
         @Inject("options") ChessGameOptions options,
         @Inject("board") Context board,
         @Inject("window") Window window,
         @Inject("chessTimer") ChessTimer chessTimer)
   {
      this.chessBoard = boardPanel.getChessBoard();
      this.boardPanel = boardPanel;
      this.scorePanel = scorePanel;
      this.options = options;
      this.window = window;
   }

   public synchronized void onClick(String boardPosition) {
      ChessBoardCellView cell = boardPanel.getCell(boardPosition);

      if (cell == boardPanel.getSelectedCell()) {
         onSelectedPieceClicked(cell);
      } else if (boardPanel.getSelectedCell() != null) {
         onClearShadows();
         onCellClickedWithExistingSelection(cell);
      } else {
         ChessThemePiece chessPieceView = cell.getPieceView();

         if (chessPieceView != null) {
            onPieceSelected(cell);
         }
      }
   }

   private synchronized void onSelectedPieceClicked(ChessBoardCellView cell) {
      onClearShadows();
      boardPanel.setSelectedCell(null);
      window.invalidate();
   }

   private synchronized void onPieceSelected(ChessBoardCellView cell) {
      ChessPiece chessPiece = cell.getChessPiece();
      List<ChessBoardPosition> boardPositions = chessPiece.moveNormal(chessBoard);

      LOG.info("onPieceSelected() " + cell);

      onClearShadows();

      if(!boardPositions.isEmpty()) {
         for (ChessBoardPosition pos : boardPositions) {
            ChessBoardCellView posBoardCell = boardPanel.getCell(pos);
            ChessPiece posPiece = posBoardCell.getChessPiece();

            if (posPiece == null) {
               onHighlightWithShadow(posBoardCell, cell);
               
               if(chessPiece.moveWasEnPassent(chessBoard, pos)) {
                  ChessBoardCellView pawnTakeCell = null;
                  ChessBoardPosition pawnTakePosition = null;
                  
                  if(chessPiece.side == ChessSide.WHITE) {
                     pawnTakePosition = ChessBoardPosition.at(pos.x, pos.y + 1);
                  } else {
                     pawnTakePosition = ChessBoardPosition.at(pos.x, pos.y - 1);
                  }
                  pawnTakeCell = boardPanel.getCell(pawnTakePosition);
                  pawnTakeCell.underAttackPiece();                     
               }
            } else {
               if(chessPiece.canTake(posPiece)) {
                  posBoardCell.underAttackPiece();
               }
            }
         }
         cell.selectedPiece();
         boardPanel.setSelectedCell(cell);

         if (!boardPositions.isEmpty()) {
            window.invalidate();
         }
      } else {
         LOG.info("onPieceSelected() Board positions empty");
      }
   }

   private synchronized void onCellClickedWithExistingSelection(ChessBoardCellView clickedCell) {
      ChessBoardPosition clickedPosition = clickedCell.getPosition();
      ChessPiece clickedPiece = clickedCell.getChessPiece();
      ChessPiece chessPiece = boardPanel.getSelectedCell().getChessPiece();
      List<ChessBoardPosition> boardPositions = chessPiece.moveNormal(chessBoard);
      boolean isPositionLegal = boardPositions.contains(clickedPosition);
      boolean isPieceTakable = chessPiece.canTake(clickedPiece);
      boolean isCellEmpty = clickedCell.isCellEmpty();

      if (!isPositionLegal) {
         if (!isCellEmpty && !isPieceTakable) {
            onPieceSelected(clickedCell);
         } else {
            onInvalidCellClickedWithExistingSelection(clickedCell);
         }
      } else if (isCellEmpty) {
         onMovePieceToClickedCell(clickedCell, boardPanel.getSelectedCell());
      } else if (isPieceTakable) {
         onTakePieceInClickedCell(clickedCell, boardPanel.getSelectedCell());
      } else {
         onPieceSelected(clickedCell);
      }
      window.invalidate();
   }

   private synchronized void onTakePieceInClickedCell(ChessBoardCellView destinationCell, ChessBoardCellView currentCell) {
      ChessPiece chessPiece = currentCell.getChessPiece();
      ChessThemePiece takenPiece = destinationCell.getPieceView();
      ChessPiece removePiece = destinationCell.getChessPiece();
      ChessBoardPosition from = currentCell.getPosition();
      ChessBoardPosition to = destinationCell.getPosition();
      ChessSide side = chessPiece.getSide();
      int changeCount = chessBoard.getChangeCount();
      
      onClearShadows();

      if(removePiece.key.type != ChessPieceType.KING) {
         ChessMove chessMove = new ChessMove(from, to, side, changeCount);
         
         scorePanel.pieceTaken(removePiece.key, takenPiece);
         
         ChessBoardMove boardMove = chessBoard.getBoardMove(chessMove);
         
         boardMove.makeMove();
         destinationCell.normalPiece();         
         boardPanel.setSelectedCell(null);
         currentCell.resetCell();
      } else {
         boardPanel.setSelectedCell(null);
      }
      window.invalidate();
   }

   private synchronized void onMovePieceToClickedCell(ChessBoardCellView destinationCell, ChessBoardCellView currentCell) {
      ChessPiece chessPiece = currentCell.getChessPiece();

      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " +currentCell);
      }
      ChessSide side = chessPiece.getSide();      
      ChessBoardPosition from = currentCell.getPosition();
      ChessBoardPosition to = destinationCell.getPosition();
      int changeCount = chessBoard.getChangeCount();
      
      onClearShadows();

      ChessMove chessMove = new ChessMove(from, to, side, changeCount);
      ChessBoardMove boardMove = chessBoard.getBoardMove(chessMove);
      
      if(chessPiece.moveWasEnPassent(chessBoard, to)) {
         ChessBoardPosition pawnTakePosition = null;
         
         if(chessPiece.side == ChessSide.WHITE) {
            pawnTakePosition = ChessBoardPosition.at(to.x, to.y + 1);
         } else {
            pawnTakePosition = ChessBoardPosition.at(to.x, to.y - 1);
         }
         ChessBoardCellView pawnTakeCell = boardPanel.getCell(pawnTakePosition);
         ChessThemePiece takenPiece = pawnTakeCell.getPieceView();
         ChessPiece removePiece = pawnTakeCell.getChessPiece();
         
         scorePanel.pieceTaken(removePiece.key, takenPiece);   
         boardMove.makeMove();
      } else if(chessPiece.moveWasCastle(chessBoard, to)) {
         ChessBoardCellView castleFromCell = null;
         ChessBoardCellView castleToCell = null;

         if(chessPiece.side == ChessSide.BLACK) {
            if(to == ChessBoardPosition.G8) {
               castleFromCell = boardPanel.getCell(ChessBoardPosition.H8);
               castleToCell = boardPanel.getCell(ChessBoardPosition.F8);
            }else if(to == ChessBoardPosition.C8) {
               castleFromCell = boardPanel.getCell(ChessBoardPosition.A8);
               castleToCell = boardPanel.getCell(ChessBoardPosition.D8);
            }
         } else {
            if(to == ChessBoardPosition.G1) {
               castleFromCell = boardPanel.getCell(ChessBoardPosition.H1);
               castleToCell = boardPanel.getCell(ChessBoardPosition.F1);
            }else if(to == ChessBoardPosition.C1) {
               castleFromCell = boardPanel.getCell(ChessBoardPosition.A1);
               castleToCell = boardPanel.getCell(ChessBoardPosition.D1);
            }
         }
         boardMove.makeMove();
         castleToCell.normalPiece();
         castleFromCell.resetCell();
      } else {
         boardMove.makeMove();
      }
      destinationCell.normalPiece();
      currentCell.resetCell();
      boardPanel.setSelectedCell(null);
      window.invalidate();
   }

   private synchronized void onHighlightWithShadow(final ChessBoardCellView shadowCell, final ChessBoardCellView currentCell) {
      ChessPiece chessPiece = currentCell.getChessPiece();
      ChessPiece shadowPiece = shadowCell.getChessPiece();
      ChessBoardPosition pos = shadowCell.getPosition();

      LOG.info("onHighlightWithShadow() shadowCell " + shadowCell + " currentCell " + currentCell);

      if(options.isShowMoveHighlight()) {
         if(shadowPiece == null) {
            if(chessPiece.moveWasCastle(chessBoard, pos)) {
               ChessPiece castlePiece = null;

               if(chessPiece.side == ChessSide.BLACK) {
                  if(pos == ChessBoardPosition.G8) {
                     castlePiece = chessBoard.getPiece(ChessBoardPosition.H8);
                  }else if(pos == ChessBoardPosition.C8) {
                     castlePiece = chessBoard.getPiece(ChessBoardPosition.A8);
                  }
               } else {
                  if(pos == ChessBoardPosition.G1) {
                     castlePiece = chessBoard.getPiece(ChessBoardPosition.H1);
                  }else if(pos == ChessBoardPosition.C1) {
                     castlePiece = chessBoard.getPiece(ChessBoardPosition.A1);
                  }
               }
               shadowCell.shadowPiece(castlePiece);
            } else {
               shadowCell.shadowPiece(chessPiece);
            }
         }
      }
   }

   private synchronized void onInvalidCellClickedWithExistingSelection(ChessBoardCellView clickedCell) {
      onClearShadows();
      boardPanel.setSelectedCell(null);
   }

   private synchronized void onClearShadows() {
      Collection<ChessBoardCellView> boardCells = boardPanel.getBoardCells();

      for (ChessBoardCellView boardCell : boardCells) {
         if(boardCell.isCellShadow()) {
           boardCell.resetCell();
         }
         boardCell.clearPieceStatus();
      }
      LOG.info("Clearing shadows");
   }
}
