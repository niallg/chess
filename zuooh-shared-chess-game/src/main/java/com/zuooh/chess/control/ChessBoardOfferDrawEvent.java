package com.zuooh.chess.control;

import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;

public class ChessBoardOfferDrawEvent extends ChessBoardEvent {

   private final ChessBoardAlertNotifier boardNotifier;
   private final ChessDatabase gameDatabase;
   private final ChessBoardStatus boardStatus;
   private final ChessBoardResult boardResult;
   private final String gameId;
   
   public ChessBoardOfferDrawEvent(ChessDatabase gameDatabase, ChessBoardAlertNotifier boardNotifier, ChessBoardStatus boardStatus, ChessBoardResult boardResult, String gameId) {
      this.boardNotifier = boardNotifier;
      this.gameDatabase = gameDatabase;
      this.boardStatus = boardStatus;
      this.boardResult = boardResult;
      this.gameId = gameId;
   }
   
   @Override
   public boolean isCancelable() {
      return false;
   }
   
   @Override
   public ChessBoardOperation executeEvent() {
      try {
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);   
         ChessBoardStatus currentStatus = boardGame.getBoardStatus();  

         if(currentStatus != boardStatus) {
            boardGame.setBoardStatus(boardStatus);
            boardGame.setBoardResult(boardResult);
            boardNotifier.alertStatusChange(boardStatus, boardResult);
            boardDatabase.saveBoardGame(boardGame);
         }
         return ChessBoardOperation.NOOP;
      } catch(Exception e) {
         throw new IllegalStateException("Error resigning", e);
      }
   }
}
