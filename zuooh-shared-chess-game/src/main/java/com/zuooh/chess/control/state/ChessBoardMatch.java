package com.zuooh.chess.control.state;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;

public class ChessBoardMatch {

   private final ChessBoardContext boardContext;
   private final ChessDatabase gameDatabase;  

   public ChessBoardMatch(ChessBoardContext boardContext, ChessDatabase gameDatabase) {
      this.gameDatabase = gameDatabase;
      this.boardContext = boardContext;
   }
  
   public ChessUser matchPlayer(String color) throws Exception {
      ChessSide requestedColor = ChessSide.resolveSide(color);
      ChessSide opponentColor = boardContext.getOpponentSide();
      
      if(opponentColor == requestedColor) {
         ChessPlayer player = boardContext.getOpponent();
         ChessUser user = player.getUser(); 
         
         if(user == null) {            
            String gameId = boardContext.getGameId();
            ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
            ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);

            if(boardGame != null) {
               String opponentId = boardGame.getOpponentId();
               
               if(opponentId != null) {
                  ChessUserDatabase userDatabase = gameDatabase.getUserDatabase();
                  ChessUser opponent = userDatabase.loadUser(opponentId);
                  
                  return opponent;
               }
            }
         }
         return user;
      }
      return boardContext.getBoardUser();             
   }
}
