package com.zuooh.chess.control.state;

import java.util.Set;

import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.theme.ChessTheme;
import com.zuooh.chess.database.theme.ChessThemeDatabase;

@Component
public class ChessBoardStateModel {

   private final ChessDatabase gameDatabase;
   private final ChessBoard chessBoard;
   private final String themeColor;
   private final String gameId;

   public ChessBoardStateModel(         
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase) 
   {
      this(boardContext, gameDatabase, null);
   }
   
   public ChessBoardStateModel(         
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("chessBoard") ChessBoard chessBoard) 
   {
      this.gameId = boardContext.getGameId();
      this.themeColor = boardContext.getThemeColor();
      this.gameDatabase = gameDatabase;
      this.chessBoard = chessBoard;
   }   

   public String getGameId() {
      return gameId;
   }
   
   public String getThemeColor() {
      return themeColor;
   }
   
   public Set<ChessPieceKey> getPiecesTaken() {
      try {
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();   
         return historyDatabase.loadHistoryPiecesTaken(gameId);
      } catch(Exception e) {
         throw new IllegalStateException("Could not load state for gameId " + gameId + " and theme " + themeColor, e);
      }
   }
   
   public ChessPieceSet getChessPieceSet() {      
      try {
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();   
         return historyDatabase.loadHistoryAsPieceSet(gameId);
      } catch(Exception e) {
         throw new IllegalStateException("Could not load state for gameId " + gameId + " and theme " + themeColor, e);
      }
   }

   public ChessBoard getChessBoard() {
      try {
         if(chessBoard == null) {
            ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();   
            return historyDatabase.loadHistoryAsBoard(gameId);
         }
         return chessBoard;
      } catch(Exception e) {
         throw new IllegalStateException("Could not load state for gameId " + gameId + " and theme " + themeColor, e);
      }
   }

   public ChessTheme getTheme() {
      try {
         ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();
         return themeDatabase.loadTheme(themeColor);
      } catch(Exception e) {
         throw new IllegalStateException("Could not load state for gameId " + gameId + " and theme " + themeColor, e);
      }
   }
}
