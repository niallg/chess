package com.zuooh.chess.control;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.DelayQueue;
import java.util.concurrent.Delayed;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.common.thread.ThreadPoolFactory;

public class ChessBoardEventScheduler implements Runnable {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardEventScheduler.class);

   private final BlockingQueue<ChessBoardOperation> operationQueue;
   private final BlockingQueue<BoardFutureEvent> eventQueue;
   private final AtomicBoolean cancelAll;
   private final ThreadFactory threadFactory;
   private volatile Thread runningThread;
   private volatile boolean schedulerActive;

   public ChessBoardEventScheduler() {
      this.threadFactory = new ThreadPoolFactory(ChessBoardEventScheduler.class);
      this.eventQueue = new DelayQueue<BoardFutureEvent>();
      this.operationQueue = new LinkedBlockingQueue<ChessBoardOperation>();
      this.cancelAll = new AtomicBoolean();
   }

   public List<ChessBoardOperation> nextOperation(long timeInMillis) throws InterruptedException {
      List<ChessBoardOperation> operations = new ArrayList<ChessBoardOperation>();
      
      if(!operationQueue.isEmpty()) {
         operationQueue.drainTo(operations);
      }
      if(!operations.isEmpty()) {
         ChessBoardOperation operation = operationQueue.poll(timeInMillis, TimeUnit.MILLISECONDS);
         
         if(operation != null) {
            operations.add(operation);
         }
      }
      return operations;
   }

   public boolean isRunning() {
      return schedulerActive;
   }

   public void start() {
      if(!schedulerActive) {
         runningThread = threadFactory.newThread(this);
         schedulerActive = true;
         runningThread.start();
      }
   }

   public void stop() {
      if(runningThread != null) {
         try {
            schedulerActive = false;
            runningThread.interrupt();
            runningThread.join();
            runningThread = null;
         } catch(Exception e){
            e.printStackTrace();
         }
      }
   }

   public void cancelAll() {
      cancelAll.set(true);
   }

   public void executeEvent(ChessBoardEvent boardEvent) {
      executeEvent(boardEvent, 0);
   }

   public void executeEvent(ChessBoardEvent boardEvent, long delayBeforeExecute) {
      BoardFutureEvent futureEvent = new BoardFutureEvent(boardEvent, delayBeforeExecute);
      eventQueue.offer(futureEvent);
   }

   @Override
   public void run() {
      while(schedulerActive) {
         try {
            BoardFutureEvent boardEvent = eventQueue.poll(2000, MILLISECONDS);
            boolean cancelRequested = cancelAll.get();

            if(cancelRequested) {
               LinkedList<BoardFutureEvent> nonCancelable = new LinkedList<BoardFutureEvent>();

               if(boardEvent != null) {
                  if(!boardEvent.isCancelable()) {
                     nonCancelable.offer(boardEvent);
                  }
               }
               while(!eventQueue.isEmpty()) {
                  BoardFutureEvent nextEvent = eventQueue.peek();

                  if(nextEvent != null) {
                     eventQueue.remove(nextEvent);

                     if(!boardEvent.isCancelable()) {
                        nonCancelable.offer(nextEvent);
                     }
                  } else {
                     eventQueue.clear();
                  }
               }
               while(!nonCancelable.isEmpty()) {
                  BoardFutureEvent nextEvent = nonCancelable.poll();
                  eventQueue.offer(nextEvent);
               }
               cancelAll.set(false);
            } else {
               if(boardEvent != null) {
                  ChessBoardOperation boardOperation = boardEvent.executeEvent();

                  if(boardOperation != null && boardOperation != ChessBoardOperation.NOOP) {
                     operationQueue.offer(boardOperation);
                  }
               }
            }
         }catch(Exception e) {
            LOG.info("Error executing event", e);
         }
      }
   }

   private static class BoardFutureEvent extends ChessBoardEvent implements Delayed {

      private volatile ChessBoardEvent event;
      private volatile long time;

      public BoardFutureEvent(ChessBoardEvent event, long delayBeforeExecute) {
         this.time = System.currentTimeMillis() + delayBeforeExecute;
         this.event = event;
      }

      @Override
      public boolean isCancelable() {
         return event.isCancelable();
      }

      @Override
      public ChessBoardOperation executeEvent() {
         return event.executeEvent();
      }

      @Override
      public long getDelay(TimeUnit unit) {
         return unit.convert(time - System.currentTimeMillis(), MILLISECONDS);
      }

      @Override
      public int compareTo(Delayed other) {
         BoardFutureEvent entry = (BoardFutureEvent) other;

         if (other == this) {
            return 0;
         }
         return compareTo(entry);
      }

      private int compareTo(BoardFutureEvent entry) {
         long diff = time - entry.time;

         if (diff < 0) {
            return -1;
         } else if (diff > 0) {
            return 1;
         }
         return 0;
      }

      @Override
      public String toString() {
         return event.toString();
      }

   }
}
