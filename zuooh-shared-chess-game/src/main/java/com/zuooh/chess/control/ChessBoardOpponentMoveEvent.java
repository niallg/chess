package com.zuooh.chess.control;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.engine.ChessMove;

public class ChessBoardOpponentMoveEvent extends ChessBoardEvent {
   
   private final ChessMove chessMove;
   private final String gameId;
   
   public ChessBoardOpponentMoveEvent(ChessMove chessMove, String gameId) {
      this.chessMove = chessMove;
      this.gameId = gameId;
   }
   
   @Override
   public boolean isCancelable() {
      return false;
   }
   
   @Override
   public ChessBoardOperation executeEvent() { 
      ChessBoardPosition fromPosition = chessMove.getFrom();
      ChessBoardPosition toPosition = chessMove.getTo();
      int changeCount = chessMove.getChange();
      
      return new ChessBoardOpponentMoveOperation(fromPosition, toPosition, gameId, changeCount);
   }
}
