package com.zuooh.chess.control;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;

public class ChessBoardMakeMoveEvent extends ChessBoardEvent {
   
   private final ChessSide playDirection;
   private final ChessDatabase gameDatabase;
   private final ChessPlayer opponentPlayer;
   private final ChessPlayer mockPlayer;
   private final ChessBoard chessBoard;
   private final String gameId;
   
   public ChessBoardMakeMoveEvent(ChessDatabase gameDatabase, ChessBoard chessBoard, ChessSide playDirection, ChessPlayer opponentPlayer, ChessPlayer mockPlayer, String gameId) {
      this.playDirection = playDirection;
      this.gameDatabase = gameDatabase;
      this.opponentPlayer = opponentPlayer;
      this.mockPlayer = mockPlayer;
      this.chessBoard = chessBoard;
      this.gameId = gameId;
   }
   
   @Override
   public ChessBoardOperation executeEvent() {
      try {
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
         ChessSide opponentPlayDirection = opponentPlayer.getPlayerSide();
         ChessMoveEngine playerEngine = opponentPlayer.getMoveEngine();
         ChessHistoryItem lastItem = historyDatabase.loadLastHistoryItem(gameId);
         ChessMove chessMove = null;
         
         if(lastItem != null) {
            chessMove = lastItem.getMove();
         }         
         if(playDirection == opponentPlayDirection) {
            playerEngine.makeMove(chessBoard, chessMove);
         } else {
            if(mockPlayer != null) {         
               ChessMoveEngine mockEngine = mockPlayer.getMoveEngine();
            
               if(mockEngine != null) {
                  mockEngine.makeMove(chessBoard, chessMove);
               }
            }
         }
         return ChessBoardOperation.NOOP;
      } catch(Exception e) {
         throw new IllegalStateException("Could not process event", e);
      }
   }
}
