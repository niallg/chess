package com.zuooh.chess.control;

import java.util.Collection;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnBack;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.board.ChessDrawPhase;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.database.history.ChessHistoryMove;
import com.zuooh.chess.database.result.ChessResultDatabase;
import com.zuooh.chess.database.theme.ChessThemeDatabase;
import com.zuooh.chess.database.theme.ChessThemeDefinition;
import com.zuooh.chess.database.theme.ChessThemePiece;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessOpponentStatus;
import com.zuooh.chess.options.ChessGameOptions;
import com.zuooh.chess.score.ChessScorePanel;
import com.zuooh.chess.skill.ChessSkillPanel;
import com.zuooh.chess.task.ChessTaskController;
import com.zuooh.chess.timer.ChessTimeDrawer;
import com.zuooh.chess.timer.ChessTimer;
import com.zuooh.chess.view.ChessBoardCellView;
import com.zuooh.chess.view.ChessBoardPanel;

@Component
public class ChessBoardController extends ChessTaskController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardController.class);

   private final ChessBoardAlertNotifier boardNotifier;
   private final ChessBoardEventScheduler eventScheduler;
   private final ChessSide opponentSide;
   private final ChessDatabase gameDatabase;
   private final ChessBoardPanel boardPanel;
   private final ChessBoard chessBoard;
   private final ChessPlayer opponentPlayer;
   private final ChessPlayer mockPlayer;
   private final ChessScorePanel scorePanel;
   private final ChessSkillPanel skillPanel;
   private final ChessGameOptions options; // move this in to database also to remove all of the objects???
   private final ChessTimer chessTimer;
   private final Screen screen;
   private final Window window;
   private final String gameId;

   public ChessBoardController(
         @Inject("eventService") ChessBoardEventScheduler eventScheduler,
         @Inject("boardNotifier") ChessBoardAlertNotifier boardNotifier,
         @Inject("boardPanel") ChessBoardPanel boardPanel,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("scorePanel") ChessScorePanel scorePanel,
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("skillPanel") ChessSkillPanel skillPanel,
         @Inject("chessTimer") ChessTimer chessTimer,         
         @Inject("application") MobileApplication application,
         @Inject("options") ChessGameOptions options,
         @Inject("board") Context board,
         @Inject("window") Window window)
   {
      this.opponentPlayer = boardContext.getOpponent();
      this.mockPlayer = boardContext.getPlayer();
      this.screen = application.getScreen();
      this.opponentSide = opponentPlayer.getPlayerSide();
      this.gameId = opponentPlayer.getAssociatedGame();
      this.chessBoard = boardPanel.getChessBoard();
      this.gameDatabase = gameDatabase;
      this.boardNotifier = boardNotifier;
      this.eventScheduler = eventScheduler;
      this.chessTimer = chessTimer;
      this.boardPanel = boardPanel;
      this.scorePanel = scorePanel;
      this.skillPanel = skillPanel;
      this.options = options;
      this.window = window;
   }   
   
   @Override
   public synchronized void onBeforeStart() throws Exception {
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      int count = historyDatabase.countOfHistoryItems(gameId);      
      
      onClearShadows();
      
      if(count == 0) {
         if(opponentSide == ChessSide.WHITE) {
            onOpponentMove();
         } else {
            onRefresh();
         }
      } else {
         ChessHistoryItem lastItem = historyDatabase.loadLastHistoryItem(gameId);
         ChessHistoryMove lastMove = lastItem.chessMove;
         ChessSide lastMoveDirection = lastMove.side;

         if(opponentSide != lastMoveDirection) {
            onOpponentMove();
         } else {
            onRefresh();
         }
      }
   }   

   @Override
   public synchronized void onBeforeStop() {
      ChessMoveEngine playerEngine = opponentPlayer.getMoveEngine();

      if(opponentPlayer.isOnline()) {
         playerEngine.stopSearch();
      }
      if(mockPlayer != null) {
         ChessMoveEngine mockEngine = mockPlayer.getMoveEngine();
         
         if(mockEngine != null) {
            mockEngine.stopSearch();
         }
      }
   }   

   @Override
   public synchronized long onInterval() throws Exception {
      List<ChessBoardOperation> boardOperations = eventScheduler.nextOperation(100);

      for(ChessBoardOperation boardOperation : boardOperations) {
         boardOperation.onOperation(this, boardPanel, gameId);
      }
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      ChessBoardStatus status = boardGame.getBoardStatus();
      ChessBoardResult result = boardGame.getBoardResult();
      ChessDrawPhase drawPhase = boardGame.getDrawPhase();
      ChessTimeDrawer timeDrawer = chessTimer.getDrawer();
      long whiteTimeRemaining = timeDrawer.calculateTimeRemaining(boardGame, ChessSide.WHITE);
      long blackTimeRemaining = timeDrawer.calculateTimeRemaining(boardGame, ChessSide.BLACK);

      if(!status.isGameOver()) {
         if(blackTimeRemaining <= 0 || whiteTimeRemaining <= 0) {
            ChessSide playerSide = boardGame.getUserSide();

            if(playerSide == ChessSide.WHITE) {
               onGameOver(ChessBoardStatus.TIME_OUT, blackTimeRemaining > 0 ? ChessBoardResult.LOSE : ChessBoardResult.WIN);
            } else {
               onGameOver(ChessBoardStatus.TIME_OUT, blackTimeRemaining > 0 ? ChessBoardResult.WIN: ChessBoardResult.LOSE);
            }
         } else {
            if(drawPhase.isDrawRequestReceived()) {
               boardNotifier.alertDrawOffer(); 
               boardGame.setDrawPhase(ChessDrawPhase.REQUEST_ACKNOWLEDGED);
               boardDatabase.saveBoardGame(boardGame); // so not save!!
            }
            ChessMoveEngine playerEngine = opponentPlayer.getMoveEngine();
            ChessOpponentStatus opponentStatus = playerEngine.getEngineStatus();
            ChessMove moveResult = playerEngine.getLastMove();
            
            if(opponentStatus.isResign() || opponentStatus.isDraw()) {
               onMoveResult(null, opponentStatus);
            } else if(moveResult != null) {               
               onCheckOpponentProgress(moveResult, opponentStatus);
            } else {
               if(mockPlayer != null) {
                  onMakeMockMove();
               }
            }                  
         }        
      } else {
         onGameOver(status, result); // we should not be on this page
      }
      return 1000;
   }
   

   @OnBack
   public synchronized void onBack() throws Exception {
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      ChessHistoryItem lastItem = historyDatabase.loadLastHistoryItem(gameId);
      
      if(lastItem != null && !opponentPlayer.isOnline()) {
         ChessMoveEngine playerEngine = opponentPlayer.getMoveEngine();
         ChessSide playerSide = opponentSide.oppositeSide();
         int moveCount = historyDatabase.countOfHistoryItems(gameId);
         
         if((playerSide == ChessSide.WHITE && moveCount > 0) || (playerSide == ChessSide.BLACK && moveCount > 1)) { // don't allow very first move to be undone
            playerEngine.stopSearch();
            
            if(mockPlayer != null) {
               ChessMoveEngine mockEngine = mockPlayer.getMoveEngine();
               
               if(mockEngine != null) {
                  mockEngine.stopSearch();
               }
            }
            eventScheduler.executeEvent(new ChessBoardUndoHistoryEvent(eventScheduler, opponentPlayer, gameDatabase, scorePanel, playerSide, moveCount));
         }
      }
   }   
   
   private synchronized void onCheckOpponentProgress(ChessMove chessMove, ChessOpponentStatus opponentStatus) throws Exception {
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      ChessHistoryItem lastItem = historyDatabase.loadLastHistoryItem(gameId);

      if(lastItem == null) {
         if(opponentSide == ChessSide.WHITE) { 
            if(chessMove.side == opponentSide) { 
               onMoveResult(chessMove, opponentStatus);
            }
         }
      } else {
         ChessSide lastMoveDirection = lastItem.chessMove.side;
         
         if(lastMoveDirection != opponentSide) { // its opponents move                  
            if(chessMove.side == opponentSide) { // move was made by opponent
               onMoveResult(chessMove, opponentStatus);
            }
         } else {
            if(mockPlayer != null) {
               onMakeMockMove(); // mock players turn
            }
         }
      }
      onReconcileOpponentMove(chessMove, opponentStatus); // just in case the off line refresher makes the move
   }  
   
   private synchronized void onReconcileOpponentMove(ChessMove chessMove, ChessOpponentStatus opponentStatus) throws Exception {
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      ChessHistoryItem lastItem = historyDatabase.loadLastHistoryItem(gameId);    

      if(lastItem != null && lastItem.chessMove.side == opponentSide) { // was last move by opponent
         ChessMove lastMove = chessBoard.getLastMove(opponentSide);
         
         if(lastMove == null || lastMove.from != lastItem.chessMove.fromPosition) { // problem!! not from same square
            LOG.info("Reconciliation problem as last move is " + lastMove + " but history says " + lastItem.chessMove);
            onMoveResult(chessMove, opponentStatus);
         }         
      }
   }
   
   private synchronized void onMakeMockMove() throws Exception  {
      ChessMoveEngine mockEngine = mockPlayer.getMoveEngine();
      ChessSide side = mockEngine.getSide();
      ChessMove lastMove = mockEngine.getLastMove();
      
      if(lastMove == null) {
         if(side == ChessSide.WHITE) {
            onChessEngineMove(side);
         }
      } else if(lastMove.side != opponentSide) {
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
         ChessHistoryItem lastMockHistory = historyDatabase.loadLastHistoryItem(gameId, side);
         
         if(lastMockHistory != null) { 
            ChessMove lastMockMove = lastMockHistory.getMove();
         
            if(lastMockMove.change != lastMove.change) {
               ChessBoardCellView fromCell = boardPanel.getCell(lastMove.from);
               ChessBoardCellView toCell = boardPanel.getCell(lastMove.to);
         
               if(toCell.isCellEmpty()) {
                  onMovePieceToClickedCell(toCell, fromCell);
               } else {
                  onTakePieceInClickedCell(toCell, fromCell);;
               }
            }
         } else {
            ChessBoardCellView fromCell = boardPanel.getCell(lastMove.from);
            ChessBoardCellView toCell = boardPanel.getCell(lastMove.to);
      
            if(toCell.isCellEmpty()) {
               onMovePieceToClickedCell(toCell, fromCell);
            } else {
               onTakePieceInClickedCell(toCell, fromCell);;
            }
         }
      }
   }
   
   @OnCall
   public synchronized void onRefresh() throws Exception {
      onClearShadows();
      window.invalidate();
   }

   @OnCall
   public synchronized void onClick(String boardPosition) throws Exception {
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();   
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);   
      ChessBoardStatus status = boardGame.getBoardStatus();
      ChessSide playerSide = opponentSide.oppositeSide();
      ChessMove playerLastMove = chessBoard.getLastMove(playerSide);   
      ChessMove opponentLastMove = chessBoard.getLastMove(opponentSide);
      ChessHistoryItem lastItem = historyDatabase.loadLastHistoryItem(gameId);               
      boolean gameOver = status.isGameOver();
      boolean moveInProgress = false;
      boolean moveIsReconciled = true;      
      
      if(opponentLastMove == null) {
         moveInProgress = opponentSide == ChessSide.WHITE;
      } 
      if(lastItem != null) {
         if(lastItem.chessMove.side == opponentSide) { // was last move by opponent      
            moveIsReconciled = opponentLastMove != null && opponentLastMove.from == lastItem.chessMove.fromPosition;
         } else {
            moveIsReconciled = playerLastMove != null && playerLastMove.from == lastItem.chessMove.fromPosition;
            moveInProgress = moveIsReconciled;
         }
      }               
      if(mockPlayer == null && !moveInProgress && !gameOver && moveIsReconciled) {
         ChessBoardCellView cell = boardPanel.getCell(boardPosition);

         if (cell == boardPanel.getSelectedCell()) {
            onSelectedPieceClicked(cell);
         } else if (boardPanel.getSelectedCell() != null) {
            onClearShadows();
            onCellClickedWithExistingSelection(cell);
         } else {
            ChessThemePiece chessPieceView = cell.getPieceView();

            if (chessPieceView != null) {
               onPieceSelected(cell);
            }
         }
      } else {
         LOG.info("Move not possible as state is " + status);
      }
   }

   private synchronized void onSelectedPieceClicked(ChessBoardCellView cell) throws Exception {
      LOG.info("onSelectedPieceClicked() " + cell);
      onClearShadows();
      boardPanel.setSelectedCell(null);
      window.invalidate();
   }

   private synchronized void onGameOver(ChessBoardStatus boardStatus, ChessBoardResult boardResult) throws Exception {
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();
      ChessResultDatabase resultDatabase = gameDatabase.getResultDatabase();      
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      String themeColor = boardGame.getThemeColor();
      ChessThemeDefinition theme = themeDatabase.loadThemeDefinition(themeColor);
      ChessSide playerSide = boardGame.getUserSide();      
      String screenName = theme.getScreen(playerSide, boardStatus);
      
      boardNotifier.alertStatusChange(boardStatus, boardResult);
      boardGame.setBoardResult(boardResult);
      boardGame.setBoardStatus(boardStatus);
      resultDatabase.saveResult(boardGame);
      boardDatabase.saveBoardGame(boardGame);
      screen.showPage(screenName);
      onStop();      
   }

   private synchronized void onPieceSelected(ChessBoardCellView cell) throws Exception {
      ChessPiece chessPiece = cell.getChessPiece();
      List<ChessBoardPosition> boardPositions = chessPiece.moveNormal(chessBoard);

      LOG.info("onPieceSelected() " + cell);

      onClearShadows();

      if(chessPiece.side != opponentSide) {
         int moveCount = 0;
         
         if(!boardPositions.isEmpty()) {            
            for (ChessBoardPosition pos : boardPositions) {
               ChessBoardCellView posBoardCell = boardPanel.getCell(pos);
               ChessPiece posPiece = posBoardCell.getChessPiece();

               if(!chessPiece.checkIf(chessBoard, pos)) { // see if the move causes check
                  if (posPiece == null) {
                     onHighlightWithShadow(posBoardCell, cell);
                     
                     if(chessPiece.moveWasEnPassent(chessBoard, pos)) {
                        ChessBoardCellView pawnTakeCell = null;
                        ChessBoardPosition pawnTakePosition = null;
                        
                        if(chessPiece.side == ChessSide.WHITE) {
                           pawnTakePosition = ChessBoardPosition.at(pos.x, pos.y + 1);
                        } else {
                           pawnTakePosition = ChessBoardPosition.at(pos.x, pos.y - 1);
                        }
                        pawnTakeCell = boardPanel.getCell(pawnTakePosition);
                        pawnTakeCell.underAttackPiece();                     
                     }
                  } else {
                     if(chessPiece.canTake(posPiece)) {
                        posBoardCell.underAttackPiece();
                     }
                  }
                  moveCount++;
               }
            }
            if(moveCount > 0) {
               cell.selectedPiece();
               boardPanel.setSelectedCell(cell);
   
               if (!boardPositions.isEmpty()) {
                  window.invalidate();
               }
            }
         } else {
            LOG.info("onPieceSelected() Board positions empty");
         }
      } else {
         LOG.info("onPieceSelected() Opponents piece selected");
      }
   }

   private synchronized void onHighlightWithShadow(final ChessBoardCellView shadowCell, final ChessBoardCellView currentCell) {
      ChessPiece chessPiece = currentCell.getChessPiece();
      ChessPiece shadowPiece = shadowCell.getChessPiece();
      ChessBoardPosition pos = shadowCell.getPosition();

      LOG.info("onHighlightWithShadow() shadowCell " + shadowCell + " currentCell " + currentCell);

      if(options.isShowMoveHighlight()) {
         if(shadowPiece == null) {
            if(chessPiece.moveWasCastle(chessBoard, pos)) {
               ChessPiece castlePiece = null;

               if(chessPiece.side == ChessSide.BLACK) {
                  if(pos == ChessBoardPosition.G8) {
                     castlePiece = chessBoard.getPiece(ChessBoardPosition.H8);
                  }else if(pos == ChessBoardPosition.C8) {
                     castlePiece = chessBoard.getPiece(ChessBoardPosition.A8);
                  }
               } else {
                  if(pos == ChessBoardPosition.G1) {
                     castlePiece = chessBoard.getPiece(ChessBoardPosition.H1);
                  }else if(pos == ChessBoardPosition.C1) {
                     castlePiece = chessBoard.getPiece(ChessBoardPosition.A1);
                  }
               }
               shadowCell.shadowPiece(castlePiece);
            } else {
               shadowCell.shadowPiece(chessPiece);
            }
         }
      }
   }

   private synchronized void onCellClickedWithExistingSelection(ChessBoardCellView clickedCell) throws Exception {
      ChessBoardPosition clickedPosition = clickedCell.getPosition();
      ChessPiece clickedPiece = clickedCell.getChessPiece();
      ChessPiece chessPiece = boardPanel.getSelectedCell().getChessPiece();
      List<ChessBoardPosition> boardPositions = chessPiece.moveNormal(chessBoard);
      boolean isPositionLegal = boardPositions.contains(clickedPosition);
      boolean isPieceTakable = chessPiece.canTake(clickedPiece);
      boolean isCellEmpty = clickedCell.isCellEmpty();

      if (!isPositionLegal) {
         if (!isCellEmpty && !isPieceTakable) {
            onPieceSelected(clickedCell);
         } else {
            onInvalidCellClickedWithExistingSelection(clickedCell);
         }
      } else if (isCellEmpty) {
         onMovePieceToClickedCell(clickedCell, boardPanel.getSelectedCell());
      } else if (isPieceTakable) {
         onTakePieceInClickedCell(clickedCell, boardPanel.getSelectedCell());
      } else {
         onPieceSelected(clickedCell);
      }
      window.invalidate();
   }

   private synchronized void onTakePieceInClickedCell(ChessBoardCellView destinationCell, ChessBoardCellView currentCell) throws Exception {
      ChessPiece chessPiece = currentCell.getChessPiece();
      ChessThemePiece takenPiece = destinationCell.getPieceView();
      ChessSide side = chessPiece.getSide();
      ChessSide oppositeDirection = side.oppositeSide();
      ChessBoardPosition from = currentCell.getPosition();
      ChessBoardPosition to = destinationCell.getPosition();
      ChessPiece removePiece = destinationCell.getChessPiece();
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      int changeCount = chessBoard.getChangeCount();
      
      onClearShadows();

      if(!isMoveCheck(destinationCell, currentCell)) {
         ChessMove chessMove = new ChessMove(from, to, side, changeCount);
         
         historyDatabase.saveHistoryItem(gameId, chessMove);
         scorePanel.pieceTaken(removePiece.key, takenPiece);
         
         ChessBoardMove boardMove = chessBoard.getBoardMove(chessMove);
         
         boardMove.makeMove();
         destinationCell.normalPiece();
         boardPanel.setSelectedCell(null);
         currentCell.resetCell();
         onClearShadows();        
         onUpdateBoardStatus(oppositeDirection);
         boardNotifier.alertPieceTaken();
         window.invalidate();

         LOG.info("onTakePieceInClickedCell() Finished move " + to + " handling opponents move " + from);

         if(oppositeDirection == opponentSide) {
            onOpponentMove();
         } else if(mockPlayer != null){
            onChessEngineMove(opponentSide.oppositeSide());
         }
         window.invalidate();
      } else {
         LOG.info("onTakePieceInClickedCell() Cant move in check");
         boardPanel.setSelectedCell(null);
      }
   }

   private synchronized void onMovePieceToClickedCell(ChessBoardCellView destinationCell, ChessBoardCellView currentCell) throws Exception {
      ChessPiece chessPiece = currentCell.getChessPiece();

      if(chessPiece == null) {
         throw new NullPointerException("Current cell does not have a piece " +currentCell);
      }
      ChessSide side = chessPiece.getSide();
      ChessSide oppositeDirection = side.oppositeSide();      
      ChessBoardPosition from = currentCell.getPosition();
      ChessBoardPosition to = destinationCell.getPosition();
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      int changeCount = chessBoard.getChangeCount();
      boolean alertPieceTaken = false;
      
      onClearShadows();

      if(!isMoveCheck(destinationCell, currentCell)) {
         ChessMove chessMove = new ChessMove(from, to, side, changeCount);
         ChessBoardMove boardMove = chessBoard.getBoardMove(chessMove);
         
         historyDatabase.saveHistoryItem(gameId, chessMove);

         if(chessPiece.moveWasEnPassent(chessBoard, to)) {
            ChessBoardPosition pawnTakePosition = null;
            
            if(chessPiece.side == ChessSide.WHITE) {
               pawnTakePosition = ChessBoardPosition.at(to.x, to.y + 1);
            } else {
               pawnTakePosition = ChessBoardPosition.at(to.x, to.y - 1);
            }
            ChessBoardCellView pawnTakeCell = boardPanel.getCell(pawnTakePosition);
            ChessThemePiece takenPiece = pawnTakeCell.getPieceView();
            ChessPiece removePiece = pawnTakeCell.getChessPiece();
            
            scorePanel.pieceTaken(removePiece.key, takenPiece);           
            boardMove.makeMove();
            alertPieceTaken = true;
         } else if(chessPiece.moveWasCastle(chessBoard, to)) {
            ChessBoardCellView castleFromCell = null;
            ChessBoardCellView castleToCell = null;

            if(chessPiece.side == ChessSide.BLACK) {
               if(to == ChessBoardPosition.G8) {
                  castleFromCell = boardPanel.getCell(ChessBoardPosition.H8);
                  castleToCell = boardPanel.getCell(ChessBoardPosition.F8);
               }else if(to == ChessBoardPosition.C8) {
                  castleFromCell = boardPanel.getCell(ChessBoardPosition.A8);
                  castleToCell = boardPanel.getCell(ChessBoardPosition.D8);
               }
            } else {
               if(to == ChessBoardPosition.G1) {
                  castleFromCell = boardPanel.getCell(ChessBoardPosition.H1);
                  castleToCell = boardPanel.getCell(ChessBoardPosition.F1);
               }else if(to == ChessBoardPosition.C1) {
                  castleFromCell = boardPanel.getCell(ChessBoardPosition.A1);
                  castleToCell = boardPanel.getCell(ChessBoardPosition.D1);
               }
            }
            boardMove.makeMove();
            castleToCell.normalPiece();
            castleFromCell.resetCell();
         } else {
            boardMove.makeMove();
         }
         destinationCell.normalPiece();
         currentCell.resetCell();
         onClearShadows();
         boardPanel.setSelectedCell(null);
         onUpdateBoardStatus(oppositeDirection);
         
         if(alertPieceTaken) {
            boardNotifier.alertPieceTaken();
         } else {
            boardNotifier.alertMoveMade();
         }
         window.invalidate();

         LOG.info("onMovePieceToClickedCell() Finished move, handling opponents move");

         if(oppositeDirection == opponentSide) {
            onOpponentMove();
         } else if(mockPlayer != null){
            onChessEngineMove(opponentSide.oppositeSide());
         }
      } else {
         LOG.info("onMovePieceToClickedCell() Skipped everything as move is in check");
         boardPanel.setSelectedCell(null);
      }
   }

   private synchronized void onInvalidCellClickedWithExistingSelection(ChessBoardCellView clickedCell) throws Exception {
      onClearShadows();
      boardPanel.setSelectedCell(null);
   }    

   protected synchronized boolean isMoveCheck(ChessBoardCellView destinationCell, ChessBoardCellView currentCell) throws Exception {
      ChessPiece chessPiece = currentCell.getChessPiece();

      if(chessPiece != null) {
         ChessBoardPosition from = currentCell.getPosition();
         ChessBoardPosition to = destinationCell.getPosition();
         ChessMove move = chessPiece.side.createMove(from, to);
         ChessBoardMove boardMove = chessBoard.getBoardMove(move);
         
         try {
            boardMove.makeMove(false);
            return ChessCheckAnalyzer.check(chessBoard, chessPiece.side);
         } catch(Exception e) {
            LOG.info("Could not determine if board is in check", e);
         } finally {
            boardMove.revertMove();
         }         
      }
      return false;
   }

   private synchronized void onMoveResult(ChessMove chessMove, ChessOpponentStatus opponentStatus) throws Exception {
      if(opponentStatus.isThinking()) {
         eventScheduler.executeEvent(new ChessBoardMakeMoveEvent(gameDatabase, chessBoard, opponentSide, opponentPlayer, mockPlayer, gameId), 5000);
      } else if(opponentStatus.isWin()) {
         eventScheduler.executeEvent(new ChessBoardStatusChangeEvent(gameDatabase, boardNotifier, ChessBoardStatus.CHECK_MATE, ChessBoardResult.LOSE, gameId));
      } else if(opponentStatus.isLose()) {
         eventScheduler.executeEvent(new ChessBoardStatusChangeEvent(gameDatabase, boardNotifier, ChessBoardStatus.CHECK_MATE, ChessBoardResult.WIN, gameId));         
      } else if(opponentStatus.isResign()) {
         eventScheduler.executeEvent(new ChessBoardStatusChangeEvent(gameDatabase, boardNotifier, ChessBoardStatus.RESIGN, ChessBoardResult.WIN, gameId));
      } else if(opponentStatus.isDraw()) {
         eventScheduler.executeEvent(new ChessBoardStatusChangeEvent(gameDatabase, boardNotifier, ChessBoardStatus.DRAW, ChessBoardResult.DRAW, gameId));
      } else if(opponentStatus.isError()) {
         LOG.info("Error occured, try again later");
      } else if(opponentStatus.isOffline()) {
         LOG.info("Opponent is offline, try again later");
      } else if(opponentStatus.isOnline()) {
         eventScheduler.executeEvent(new ChessBoardOpponentMoveEvent(chessMove, gameId));
      }
      skillPanel.refreshDetails();
   }

   private synchronized void onOpponentMove() {
      onChessEngineMove(opponentSide);
   }

   private synchronized void onChessEngineMove(final ChessSide side) {
      eventScheduler.executeEvent(new ChessBoardMakeMoveEvent(gameDatabase, chessBoard, side, opponentPlayer, mockPlayer, gameId));
   }

   protected synchronized void onOpponentAboutToMakeMove(ChessBoardCellView destinationCell, ChessBoardCellView currentCell) {
      ChessPiece chessPiece = currentCell.getChessPiece();
      ChessPiece destinationPiece = destinationCell.getChessPiece();
      ChessBoardPosition toPosition = destinationCell.getPosition();
      
      if (destinationPiece == null) {
         onHighlightWithShadow(destinationCell, currentCell);
         
         if(chessPiece.moveWasEnPassent(chessBoard, toPosition)) {
            ChessBoardCellView pawnTakeCell = null;
            ChessBoardPosition pawnTakePosition = null;
            
            if(chessPiece.side == ChessSide.WHITE) {
               pawnTakePosition = ChessBoardPosition.at(toPosition.x, toPosition.y + 1);
            } else {
               pawnTakePosition = ChessBoardPosition.at(toPosition.x, toPosition.y - 1);
            }
            pawnTakeCell = boardPanel.getCell(pawnTakePosition);
            pawnTakeCell.underAttackPiece();                     
         }
      } else {
         if(chessPiece.canTake(destinationPiece)) {
            destinationCell.underAttackPiece();
         }
      }
      currentCell.selectedPiece();
      window.invalidate();
   }

   protected synchronized void onOpponentMove(ChessBoardCellView destinationCell, ChessBoardCellView currentCell) throws Exception {
      if(destinationCell.isCellEmpty() || destinationCell.isCellShadow()) {
         onMovePieceToClickedCell(destinationCell, currentCell);
      } else {
         onTakePieceInClickedCell(destinationCell, currentCell);
      }
      window.invalidate();
   }

   protected synchronized void onUpdateBoardStatus(ChessSide side) throws Exception {
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);   
      ChessBoardStatus status = boardGame.getBoardStatus();    

      if(!status.isGameOver()) {
         if(ChessCheckAnalyzer.checkMate(chessBoard, side)) {
            if(side == opponentSide) {
               onGameOver(ChessBoardStatus.CHECK_MATE, ChessBoardResult.WIN);
            } else {
               onGameOver(ChessBoardStatus.CHECK_MATE, ChessBoardResult.LOSE);
            }
         }else if(ChessCheckAnalyzer.staleMate(chessBoard, side)){
            onGameOver(ChessBoardStatus.STALE_MATE, ChessBoardResult.DRAW);
         } else if(ChessCheckAnalyzer.check(chessBoard, side)) {
            boardGame.setBoardStatus(ChessBoardStatus.CHECK);
            boardDatabase.saveBoardGame(boardGame);
         } else {
            if(status != ChessBoardStatus.NORMAL) {
               boardGame.setBoardStatus(ChessBoardStatus.NORMAL);
               boardDatabase.saveBoardGame(boardGame);
            }
         }
      }
      ChessBoardGame updatedGame = boardDatabase.loadBoardGame(gameId);  
      ChessBoardStatus currentStatus = updatedGame.getBoardStatus();
      ChessBoardResult currentResult = updatedGame.getBoardResult();      

      if(currentStatus != status) {
         boardNotifier.alertStatusChange(currentStatus, currentResult);
      }
   }   
   
   private synchronized void onHighlightLastMove() throws Exception {
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      ChessHistoryItem lastItem = historyDatabase.loadLastHistoryItem(gameId);
      
      if(lastItem != null) {
         ChessBoardCellView sourceCell = boardPanel.getCell(lastItem.chessMove.fromPosition);
         ChessBoardCellView destinationCell = boardPanel.getCell(lastItem.chessMove.toPosition);         
         ChessPiece lastPieceMoved = destinationCell.getChessPiece();
         
         if(lastPieceMoved != null) {
            destinationCell.showPieceHighlight();
            sourceCell.shadowPiece(lastPieceMoved);
         }
      }
   }   

   protected synchronized void onClearShadows() throws Exception {           
      Collection<ChessBoardCellView> boardCells = boardPanel.getBoardCells();
      
      for (ChessBoardCellView boardCell : boardCells) {
         if(boardCell.isCellShadow()) {
           boardCell.resetCell();
         }
         boardCell.clearPieceStatus();
      }
      onRefreshRecentCells(); // ensure we render the last moves made
      onHighlightLastMove();
   }
   
   protected synchronized void onRefreshRecentCells() throws Exception { // usually only required after starting the controller!!
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      ChessHistoryItem lastWhiteMove = historyDatabase.loadLastHistoryItem(gameId, ChessSide.WHITE);
      ChessHistoryItem lastBlackMove = historyDatabase.loadLastHistoryItem(gameId, ChessSide.BLACK);  
      
      if(lastWhiteMove != null) {
         ChessMove whiteMove = lastWhiteMove.getMove();
         ChessBoardPosition to = whiteMove.getTo();            
         ChessBoardCellView cellView = boardPanel.getCell(to);
         
         cellView.resetCell(); // refreshes it           
      }
      if(lastBlackMove != null) {
         ChessMove blackMove = lastBlackMove.getMove();
         ChessBoardPosition to = blackMove.getTo();            
         ChessBoardCellView cellView = boardPanel.getCell(to);
         
         cellView.resetCell(); // refreshes it  
      } 
   }
}
