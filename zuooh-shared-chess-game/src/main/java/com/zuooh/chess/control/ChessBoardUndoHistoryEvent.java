package com.zuooh.chess.control;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.score.ChessScorePanel;

public class ChessBoardUndoHistoryEvent extends ChessBoardEvent {
   
   private final ChessBoardEventScheduler eventScheduler;
   private final ChessDatabase gameDatabase;
   private final ChessScorePanel scorePanel;
   private final ChessPlayer chessPlayer;
   private final ChessSide playerSide;
   private final int moveCount;
   
   public ChessBoardUndoHistoryEvent(ChessBoardEventScheduler eventScheduler, ChessPlayer chessPlayer, ChessDatabase gameDatabase, ChessScorePanel scorePanel, ChessSide playerSide, int moveCount) {
      this.eventScheduler = eventScheduler;
      this.gameDatabase = gameDatabase;
      this.scorePanel = scorePanel;
      this.playerSide = playerSide;
      this.chessPlayer = chessPlayer;
      this.moveCount = moveCount;
   }
   
   @Override
   public boolean isCancelable() {
      return false;
   }
   
   @Override
   public ChessBoardOperation executeEvent() { 
      return new ChessBoardUndoHistoryOperation(eventScheduler, chessPlayer, gameDatabase, scorePanel, playerSide, moveCount);
   }
}
