package com.zuooh.chess.control;

public abstract class ChessBoardEvent {
   public boolean isCancelable() {
      return true;
   }
   public abstract ChessBoardOperation executeEvent();
}
