package com.zuooh.chess.control;

public enum ChessBoardAlert {
   CHECK(false, false, false, false),
   PIECE_TAKEN(false, false, false, false),
   MOVE_MADE(false, false, false, false),
   STALE_MATE(true, false, false, false),
   CHECK_MATE_YOU_WIN(true, true, false, false),
   TIMES_UP_YOU_WIN(true, true, false, false),
   RESIGN_YOU_WIN(true, true, false, false),
   CHECK_MATE_YOU_LOSE(true, false, true, false),
   TIMES_UP_YOU_LOSE(true, false, true, false),
   RESIGN_YOU_LOSE(true, false, true, false),
   DRAW_OFFERED(false, false, false, true),      
   DRAW_ACCEPTED(true, false, false, false),
   INVITE_REJECTED(true, false, false, false); 
   
   private final boolean gameOver;
   private final boolean youWin;
   private final boolean youLose;
   private final boolean choice;
   
   private ChessBoardAlert(boolean gameOver, boolean youWin, boolean youLose, boolean choice) {
      this.youLose = youLose;
      this.youWin = youWin;
      this.gameOver = gameOver;
      this.choice = choice;
   }
   
   public boolean isGameOver() {
      return gameOver;
   }
   
   public boolean isYouWin() {
      return youWin;
   }
   
   public boolean isYouLose() {
      return youLose;
   }
   
   public boolean isChoice() {
      return choice;
   }
}
