package com.zuooh.chess.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChessBoardNoOperationEvent extends ChessBoardEvent {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardNoOperationEvent.class);
   
   private final String  message;

   public ChessBoardNoOperationEvent() {
      this(null);
   }
   
   public ChessBoardNoOperationEvent(String message) {
      this.message =  message;
   }

   @Override
   public ChessBoardOperation executeEvent() {
      if(message != null) {
         LOG.info(message);
      }
      return ChessBoardOperation.NOOP;
   }

}
