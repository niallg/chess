package com.zuooh.chess.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.view.ChessBoardCellView;
import com.zuooh.chess.view.ChessBoardPanel;

public class ChessBoardOpponentMoveOperation implements ChessBoardOperation {   
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardOpponentMoveOperation.class);

   private final ChessBoardPosition fromPosition;
   private final ChessBoardPosition toPosition;
   private final String sourceGameId;
   private final int changeCount;
   
   public ChessBoardOpponentMoveOperation(ChessBoardPosition fromPosition, ChessBoardPosition toPosition, String sourceGameId, int changeCount) {
      this.changeCount = changeCount;
      this.sourceGameId = sourceGameId;
      this.fromPosition = fromPosition;
      this.toPosition = toPosition;
   }

   @Override
   public void onOperation(ChessBoardController boardController, ChessBoardPanel boardPanel, String gameId) {
      try {
         if(sourceGameId.equals(gameId)) {
            ChessBoardCellView fromCell = boardPanel.getCell(fromPosition);
            ChessBoardCellView toCell = boardPanel.getCell(toPosition);
            ChessBoard chessBoard = boardPanel.getChessBoard();
            int currentCount = chessBoard.getChangeCount();
            
            if(changeCount >= currentCount) {              
               onMove(boardController, fromCell, toCell); // just update the board view
            } else {
               LOG.info("Ignoring move " + fromPosition + " -> " + toPosition + " for " + gameId + "' as its change count " + changeCount +" <= " + currentCount);
            }
         }
      }catch(Exception e) {
         LOG.info("Error making move " + fromPosition + " -> " + toPosition, e);
      }
   }
      
   private void onMove(ChessBoardController boardController, ChessBoardCellView fromCell, ChessBoardCellView toCell) {
      try {
         boardController.onOpponentAboutToMakeMove(toCell, fromCell);
         Thread.sleep(1000);
         LOG.info("Moving from " + fromCell + " to " + toCell);
         boardController.onOpponentMove(toCell, fromCell);
      } catch(Exception e) {
         LOG.info("Error making move " + fromPosition + " -> " + toPosition, e);
      }
   }
   
}
