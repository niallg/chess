package com.zuooh.chess.control.state;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

public class ChessUpgradeProduct {

   private final ChessGameQuota freeQuota;
   private final ChessGameQuota upgradeQuota;
   private final String productId;
   private final AtomicBoolean upgraded;
   private final AtomicLong nextPrompt;
   
   public ChessUpgradeProduct(ChessGameQuota freeQuota, ChessGameQuota upgradeQuota, String productId) {
      this(freeQuota, upgradeQuota, productId, false);
   }
   
   public ChessUpgradeProduct(ChessGameQuota freeQuota, ChessGameQuota upgradeQuota, String productId, boolean upgraded) {
      this.upgraded = new AtomicBoolean(upgraded);
      this.nextPrompt = new AtomicLong();
      this.upgradeQuota = upgradeQuota;
      this.freeQuota = freeQuota;
      this.productId = productId;
   }   
   
   public void promptShown() {
      long currentTime = System.currentTimeMillis();
      long promptFreeInterval = 60 * 60 * 1000;
      
      nextPrompt.set(currentTime + promptFreeInterval);
   }
   
   public boolean isPromptRequired() {
      long currentTime = System.currentTimeMillis();
      long promptTime = nextPrompt.get();
      
      return promptTime < currentTime;
   }
   
   public boolean isUpgraded() {
      return upgraded.get();
   }
   
   public void setUpgraded(boolean status) {
      upgraded.set(status);
   }
   
   public String getProductId(){
      return productId;
   }
   
   public ChessGameQuota getFreeQuota(){
      return freeQuota;
   }  
   
   public ChessGameQuota getUpgradeQuota() {
      return upgradeQuota;
   }     
}
