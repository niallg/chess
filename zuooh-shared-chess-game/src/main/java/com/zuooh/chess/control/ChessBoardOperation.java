package com.zuooh.chess.control;

import com.zuooh.chess.view.ChessBoardPanel;

public interface ChessBoardOperation {
   public static final ChessBoardOperation NOOP = new ChessBoardOperation() {
      public void onOperation(ChessBoardController controller, ChessBoardPanel boardPanel, String gameId) {}
   };
   void onOperation(ChessBoardController controller, ChessBoardPanel boardPanel, String gameId);
}
