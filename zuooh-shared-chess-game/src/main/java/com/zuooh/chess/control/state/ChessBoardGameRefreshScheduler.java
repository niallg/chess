package com.zuooh.chess.control.state;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessGameManager;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameComparator;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.common.task.Task;

public class ChessBoardGameRefreshScheduler implements Task {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardGameRefreshScheduler.class);
   
   private final ChessBoardGameComparator gameComparator;
   private final ChessGameManager gameManager;
   private final ChessDatabase gameDatabase;
   private final int archiveCount;
   private final long archiveDuration;
   
   public ChessBoardGameRefreshScheduler(ChessGameManager gameManager, ChessDatabase gameDatabase) {
      this(gameManager, gameDatabase, 10);
   }
   
   public ChessBoardGameRefreshScheduler(ChessGameManager gameManager, ChessDatabase gameDatabase, int archiveCount) {
      this(gameManager, gameDatabase, archiveCount, 2 * 24 * 60 * 60 * 1000);
   }
   
   public ChessBoardGameRefreshScheduler(ChessGameManager gameManager, ChessDatabase gameDatabase, int archiveCount, long archiveDuration) {
      this.gameComparator = new ChessBoardGameComparator();
      this.archiveDuration = archiveDuration;      
      this.archiveCount = archiveCount;      
      this.gameDatabase = gameDatabase;
      this.gameManager = gameManager;
   }

   @Override
   public long executeTask() {
      try {
         refreshCurrentGames();
         clearExpiredGames();
      } catch(Exception e) {
         LOG.info("Error refreshing games", e);
      }
      return 5000;
   }
   
   public void clearExpiredGames() throws Exception {
      ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessUser user = gameManager.currentUser();   
      String userId = user.getKey();
      List<ChessBoardGame> boardGames = boardDatabase.loadFinishedBoardGames(userId);
      long currentTime = System.currentTimeMillis();
      int count = boardGames.size();
      
      if(count > archiveCount) {         
         Collections.sort(boardGames, gameComparator);
         
         for(int i = 0; i < archiveCount; i++) {
            boardGames.remove(0); // always keep most recent 10
         }
         for(ChessBoardGame boardGame : boardGames) {
            String gameId = boardGame.getGameId();
            long creationTime = boardGame.getCreationTime();
            long startTime = boardGame.getStartTime();
            long expiryTime = Math.max(startTime, creationTime) + archiveDuration;
            
            if(expiryTime < currentTime) { // don't delete games that may still be relevant
               historyDatabase.deleteAllHistoryItems(gameId);
               boardDatabase.deleteBoardGame(boardGame);
            }
         }
      }         
   }
   
   public void refreshCurrentGames() throws Exception {
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessUserDatabase userDatabase = gameDatabase.getUserDatabase();    
      List<ChessBoardGame> boardGames = boardDatabase.loadBoardGames();
      
      if(!boardGames.isEmpty()) {
         Map<String, ChessGame> currentOnlineGames = gameManager.currentOnlineGames();
         Map<String, ChessGame> availableOnlineGames = gameManager.availableOnlineGames();
         ChessUser user = gameManager.currentUser();      
   
         for(ChessBoardGame boardGame : boardGames) {
            String gameId = boardGame.getGameId();
            String opponentId = boardGame.getOpponentId();
            
            if(currentOnlineGames.containsKey(gameId) || availableOnlineGames.containsKey(gameId)) {               
               if(boardGame.isOnlineGame()) {
                  ChessBoardGameRefresher refresher = new ChessBoardGameRefresher(gameDatabase, gameId);              
                  ChessGame chessGame = currentOnlineGames.get(gameId);
                  
                  if(chessGame == null) {
                     chessGame = availableOnlineGames.get(gameId);
                  }
                  if(chessGame != null) {
                     String myId = user.getKey();
                     
                     if(opponentId == null) {                        
                        ChessGameCriteria chessChallenge = chessGame.getCriteria();
                        ChessUser challenger = chessChallenge.getUser();
                        
                        if(challenger != null) {
                           String challengerId = challenger.getKey(); 
                           
                           if(challengerId != null) { // might be a new game
                              if(!myId.equals(challengerId)) { // make sure not to set yourself
                                 userDatabase.saveUser(challenger);
                                 boardGame.setOpponentId(challengerId);
                                 boardDatabase.saveBoardGame(boardGame); // XXX should result/score be saved here????
                              }
                           }
                        }
                     }
                     refresher.onRefreshGame(chessGame);
                  }
               }            
            }
         }
      }
   }
}
