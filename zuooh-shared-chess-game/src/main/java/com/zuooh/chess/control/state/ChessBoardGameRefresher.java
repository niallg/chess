package com.zuooh.chess.control.state;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.engine.ChessMove;

public class ChessBoardGameRefresher {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardGameRefresher.class);

   private final ChessDatabase gameDatabase;
   private final String gameId;
   
   public ChessBoardGameRefresher(ChessDatabase gameDatabase, String gameId) {      
      this.gameDatabase = gameDatabase;
      this.gameId = gameId;
   }   
   
   public void onRefreshGame(ChessGame chessGame) { 
      ChessMove lastMove = chessGame.getLastMove();
      
      if(lastMove != null) {
         ChessSide playerToMove = chessGame.getSideToMove();
         ChessGameCriteria challenge = chessGame.getCriteria();
         ChessSide opponentColor = challenge.getUserSide();
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
         ChessGameStatus gameStatus = chessGame.getStatus();
         
         if(opponentColor != playerToMove) { // did the opponent make a move            
            try {
               ChessHistoryItem lastItem = historyDatabase.loadLastHistoryItem(gameId);
               
               if(lastItem != null) {
                  if(lastMove.change > lastItem.chessMove.changeCount) {
                     historyDatabase.saveHistoryItem(gameId, lastMove);
                  } else {
                     LOG.info("Ignoring move " + lastMove + " for game '" + gameId + "' its change count " + lastMove.change +" <= " + lastItem.chessMove.changeCount);
                  }
               } else {
                  if(lastMove.side == ChessSide.WHITE){               
                     historyDatabase.saveHistoryItem(gameId, lastMove);                     
                  } else {
                     LOG.info("Ignoring move " + lastMove + " for game '" + gameId + "' and status '" + gameStatus + "' as we have no history?");
                  }
               }
            } catch(Exception e) {
               LOG.info("Error refreshing game " + gameId, e);
            }
         }
      }
   }   
}
