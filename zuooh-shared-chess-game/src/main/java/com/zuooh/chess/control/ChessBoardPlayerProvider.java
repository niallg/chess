package com.zuooh.chess.control;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessGameController;
import com.zuooh.chess.client.ChessMoveEnginePlayer;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.game.ChessGameDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessMoveEngineProvider;

public class ChessBoardPlayerProvider {   
   
   private final Map<String, ChessPlayer> chessPlayers;
   private final ChessMoveEngineProvider moveEngineProvider;
   private final ChessGameController gameController;
   private final ChessDatabase gameDatabase;
   private final int mockSkill;
   
   public ChessBoardPlayerProvider(ChessGameController gameController, ChessDatabase gameDatabase, ChessMoveEngineProvider moveEngineProvider) {
      this(gameController, gameDatabase, moveEngineProvider, 0);
   }
   
   public ChessBoardPlayerProvider(ChessGameController gameController, ChessDatabase gameDatabase, ChessMoveEngineProvider moveEngineProvider, int mockSkill) {
      this.chessPlayers = new ConcurrentHashMap<String, ChessPlayer>();
      this.moveEngineProvider = moveEngineProvider;
      this.gameController = gameController;
      this.gameDatabase = gameDatabase;
      this.mockSkill = mockSkill;
   }   
   
   public synchronized ChessPlayer createPlayer(String gameId) throws Exception{
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      ChessUser user = gameController.currentUser();
      String userId = user.getKey();
      String playerGameId = userId + gameId;
      ChessPlayer chessPlayer = chessPlayers.get(playerGameId);
      
      if(chessPlayer == null) {         
         if(boardGame.isOnlineGame()) {
            chessPlayer = createOnlinePlayer(boardGame);
         } else {
            chessPlayer = createComputerPlayer(boardGame);
         }
         chessPlayers.put(playerGameId, chessPlayer);
      }
      return chessPlayer;
   }
   
   public synchronized ChessPlayer createMockPlayer(ChessBoardGame boardGame) throws Exception {
      if(mockSkill > 0) {
         String userId = boardGame.getUserId();
         ChessUserDatabase userDatabase = gameDatabase.getUserDatabase();
         ChessUser user = userDatabase.loadUser(userId);
         ChessSide userColor = boardGame.getUserSide();
         String gameId = boardGame.getGameId();
         ChessMoveEngine moveEngine = moveEngineProvider.createEngine(userColor, mockSkill);
         
         return new ChessMoveEnginePlayer(moveEngine, user, gameId);
      }
      return null;
   }
   
   private synchronized ChessPlayer createOnlinePlayer(ChessBoardGame boardGame) throws Exception {
      String gameId = boardGame.getGameId();
      ChessGameDatabase database = gameDatabase.getGameDatabase();
      
      if(!database.containsGame(gameId)) {
         String opponentId = boardGame.getOpponentId();
         ChessSide playerColor = boardGame.getUserSide();
         String themeColor = boardGame.getThemeColor();
         long gameDuration = boardGame.getGameDuration();
         
         return gameController.createNewGame(gameId, opponentId, playerColor, themeColor, gameDuration);           
      }
      return gameController.continueGame(gameId);
   }
   
   private synchronized ChessPlayer createComputerPlayer(ChessBoardGame boardGame) throws Exception {
      String opponentId = boardGame.getOpponentId();
      ChessUserDatabase userDatabase = gameDatabase.getUserDatabase();
      ChessUser opponent = userDatabase.loadUser(opponentId);
      ChessSide userColor = boardGame.getUserSide();
      ChessSide opponentColor = userColor.oppositeSide();
      ChessUserData opponentData = opponent.getData();
      String gameId = boardGame.getGameId();
      int skillLevel = opponentData.getSkill();
      ChessMoveEngine moveEngine = moveEngineProvider.createEngine(opponentColor, skillLevel);
      
      return new ChessMoveEnginePlayer(moveEngine, opponent, gameId);
   }
}
