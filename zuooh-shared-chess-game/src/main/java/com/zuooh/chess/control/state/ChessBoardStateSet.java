package com.zuooh.chess.control.state;

import java.util.Map;
import java.util.Set;

import com.zuooh.application.Document;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.database.theme.ChessTheme;
import com.zuooh.chess.database.theme.ChessThemePiece;
import com.zuooh.chess.database.theme.ChessThemePieceSet;
import com.zuooh.style.image.ImageManager;

@Component
public class ChessBoardStateSet implements ChessPieceSet {

   private final Set<ChessPieceKey> piecesTaken;
   private final ChessThemePieceSet boardPieceSet;
   private final ChessPieceSet pieceSet;
   private final ChessBoard chessBoard;

   public ChessBoardStateSet(
         @Inject("boardModel") ChessBoardStateModel boardModel,
         @Inject("document") Document document) 
   {
      ImageManager imageManager = document.getImageManager();
      ChessTheme chessTheme = boardModel.getTheme();
   
      this.boardPieceSet = chessTheme.getBoardPieceSet(imageManager);
      this.pieceSet = boardModel.getChessPieceSet();
      this.piecesTaken = boardModel.getPiecesTaken();
      this.chessBoard = boardModel.getChessBoard();
   }

   public Set<ChessPieceKey> getPiecesTaken() {
      return piecesTaken;
   }

   public ChessBoard getChessBoard() {
      return chessBoard;
   }

   public ChessThemePiece getThemePiece(ChessPieceKey pieceKey) {
      ChessSide playDirection = ChessSide.WHITE;

      if(pieceKey.side == ChessSide.BLACK) {
         playDirection = ChessSide.BLACK;
      }
      return boardPieceSet.getBoardPiece(pieceKey, playDirection);
   }

   @Override
   public Map<ChessBoardPosition, ChessPiece> getPieces(ChessSide playDirection) {
      return pieceSet.getPieces(playDirection);
   }
}
