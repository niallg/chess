package com.zuooh.chess.control.state;

public class ChessGameQuota {

   private final int whiteGames;
   private final int blackGames;
   private final int totalGames; // includes challenges  
   private final int longGames;

   public ChessGameQuota(int whiteGames, int blackGames) {
      this(whiteGames, blackGames, whiteGames + blackGames);
   }
   
   public ChessGameQuota(int whiteGames, int blackGames, int totalGames) {
      this(whiteGames, blackGames, totalGames, 2);
   }
   
   public ChessGameQuota(int whiteGames, int blackGames, int totalGames, int longGames) {
      this.longGames = longGames;
      this.whiteGames = whiteGames;
      this.blackGames = blackGames;
      this.totalGames = totalGames;      
   }   
   
   public int getLongGames() {
      return longGames;
   }

   public int getWhiteGames() {
      return whiteGames;
   }

   public int getBlackGames() {
      return blackGames;
   }

   public int getTotalGames() {
      return totalGames;
   }   
}
