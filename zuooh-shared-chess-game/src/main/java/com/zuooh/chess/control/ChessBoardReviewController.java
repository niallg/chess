package com.zuooh.chess.control;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.canvas.Flow;
import com.zuooh.application.canvas.Panel;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessBoardMoveIterator;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.database.theme.ChessThemePiece;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.score.ChessScorePanel;
import com.zuooh.chess.skill.ChessSkillPanel;
import com.zuooh.chess.timer.ChessTimer;
import com.zuooh.chess.view.ChessBoardCellView;
import com.zuooh.chess.view.ChessBoardPanel;

@Component
public class ChessBoardReviewController {   

   private List<ChessHistoryItem> historyItems;
   private ChessBoardPanel boardPanel;
   private ChessScorePanel scorePanel;
   private ChessBoardStatus status;
   private ChessBoard chessBoard;
   private Panel navigationText;
   private Window window;
   private Flow actionBar;
   private int currentMove;
   private int lastMove;
   private int moveCount;

   public ChessBoardReviewController(
         @Inject("boardPanel") ChessBoardPanel boardPanel,
         @Inject("scorePanel") ChessScorePanel scorePanel,
         @Inject("skillPanel") ChessSkillPanel skillPanel,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabase,         
         @Inject("application") MobileApplication application,
         @Inject("navigationText") Panel navigationText,
         @Inject("board") Context board,
         @Inject("window") Window window,
         @Inject("actionBar") Flow actionBar,         
         @Inject("chessTimer") ChessTimer chessTimer) 
   { 
      try {
         String gameId = boardContext.getGameId();
         ChessBoardGameDatabase boardGameDatabase = gameDatabase.getBoardDatabase();
         ChessBoardGame boardGame = boardGameDatabase.loadBoardGame(gameId);
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();             
         List<ChessHistoryItem> historyItems = historyDatabase.loadAllHistoryItems(gameId);
         
         this.historyItems = Collections.unmodifiableList(historyItems);
         this.chessBoard = boardPanel.getChessBoard();
         this.status = boardGame.getBoardStatus();
         this.moveCount = historyItems.size();
         this.lastMove = moveCount - 1;
         this.currentMove = lastMove; // last offset is the last move
         this.boardPanel = boardPanel;
         this.scorePanel = scorePanel;
         this.navigationText = navigationText;
         this.actionBar = actionBar;
         this.window = window;
      } catch(Exception e) {
         throw new IllegalStateException("Unable to load history", e);
      }    
   }
   
   @OnStart
   public void showCurrentMove() {
      if(status.isGameOver()) {
         actionBar.setHidden("menuIcon"); // don't provide this when game is over!!
         actionBar.setHidden("chatIcon");
         actionBar.setVisible("chatHistoryIcon");
      }      
      if(currentMove >= 0 && moveCount > 0) {
         ChessHistoryItem historyItem = historyItems.get(currentMove);
         ChessMove move = historyItem.getMove();
         
         showMove(move);
      } 
   }
   
   @OnCall
   public void checkStatus() {
      if(status.isGameOver()) {
         actionBar.setHidden("menuIcon"); // don't provide this when game is over!!
         actionBar.setHidden("chatIcon");
         actionBar.setVisible("chatHistoryIcon");
      }   
   }

   @OnCall
   public void nextMove(Toggle toggle) throws Exception {
      if(currentMove < lastMove) { 
         ChessHistoryItem item = historyItems.get(currentMove + 1); // what is the next move?
         ChessMove move = item.getMove();
         ChessBoardCellView fromCell= boardPanel.getCell(move.from);
         ChessBoardCellView toCell = boardPanel.getCell(move.to);
         ChessPiece chessPiece = fromCell.getChessPiece();
         ChessBoardMove boardMove = chessBoard.getBoardMove(move);
         ChessMove castleMove = item.getCastleMove();
         
         if(castleMove != null) { // moveCount is going to be wrong here!!
            ChessBoardCellView castleFromCell = null;
            ChessBoardCellView castleToCell = null;

            if(chessPiece.side == ChessSide.BLACK) {
               if(move.to == ChessBoardPosition.G8) {
                  castleFromCell = boardPanel.getCell(ChessBoardPosition.H8);
                  castleToCell = boardPanel.getCell(ChessBoardPosition.F8);
               }else if(move.to == ChessBoardPosition.C8) {
                  castleFromCell = boardPanel.getCell(ChessBoardPosition.A8);
                  castleToCell = boardPanel.getCell(ChessBoardPosition.D8);
               }
            } else {
               if(move.to == ChessBoardPosition.G1) {
                  castleFromCell = boardPanel.getCell(ChessBoardPosition.H1);
                  castleToCell = boardPanel.getCell(ChessBoardPosition.F1);
               }else if(move.to == ChessBoardPosition.C1) {
                  castleFromCell = boardPanel.getCell(ChessBoardPosition.A1);
                  castleToCell = boardPanel.getCell(ChessBoardPosition.D1);
               }
            }
            boardMove.makeMove();
            castleToCell.normalPiece();
            castleFromCell.resetCell();
         } else {
            if(item.chessMove.takePosition != null) {
               ChessBoardCellView takeCell = boardPanel.getCell(item.chessMove.takePosition); // covers all move types
               
               if(!takeCell.isCellEmpty()) {
                  ChessThemePiece takenPiece = takeCell.getPieceView();
                  ChessPiece removePiece = takeCell.getChessPiece();
                  
                  scorePanel.pieceTaken(removePiece.key, takenPiece);             
               } 
            }
            boardMove.makeMove();
         }         
         toCell.normalPiece();
         fromCell.resetCell();
         showMove(move);
         currentMove++;
      }
      toggle.toggle();
      window.invalidate();
   }

   @OnCall
   public void previousMove(Toggle toggle) throws Exception {
      if(currentMove >= 0) {
         ChessHistoryItem historyItem = historyItems.get(currentMove);
         ChessMove move = historyItem.getMove();
         ChessMove castleMove = historyItem.getCastleMove();
         
         if(castleMove != null) {
            revertCastleMove(historyItem);
         } else {                     
            ChessBoardPosition takePosition = historyItem.chessMove.takePosition;
                  
            if(takePosition != null && takePosition != move.to) {
               revertEnPassentMove(historyItem);
            } else {
               revertNormalMove(historyItem);               
            }
         }
         ChessMove previousMove = null;
         
         currentMove--; // we have just undone move     
         
         if(currentMove >= 0) {
            ChessHistoryItem previousItem = historyItems.get(currentMove);
            
            if(previousItem != null) {
               previousMove = previousItem.getMove();
            }
         }
         showMove(previousMove);            
      }
      toggle.toggle();
      window.invalidate();
   }
   
   private void revertEnPassentMove(ChessHistoryItem historyItem) {
      try {
         ChessMove pawnMove = historyItem.getMove();
         ChessBoardCellView pawnFromCell = boardPanel.getCell(pawnMove.from);
         ChessBoardCellView pawnToCell = boardPanel.getCell(pawnMove.to);
         ChessPiece pawnPiece = pawnToCell.getChessPiece();
         
         if(pawnPiece.key.type != ChessPieceType.PAWN) {
            throw new IllegalStateException("Piece in cell " + pawnMove.to + " is not a pawn for move " + pawnMove);
         }
         ChessBoardPosition pawnCapturePosition = historyItem.chessMove.takePosition;         
         ChessBoardCellView pawnCaptureCell = boardPanel.getCell(pawnCapturePosition);
         ChessPieceKey pawnCaptureKey = historyItem.chessMove.takePiece;
         
         if(pawnCaptureKey == null || pawnCaptureKey.type != ChessPieceType.PAWN) {
            throw new IllegalStateException("Capture piece at " + pawnCapturePosition + " was not a pawn for " + pawnMove);
         }
         ChessBoardMoveIterator moveIterator = chessBoard.getBoardMoves();
         
         if(!moveIterator.hasNext()) {
            throw new IllegalStateException("Move " + pawnMove + " is before the first move?");
         }
         ChessBoardMove boardMove = moveIterator.next();            
         
         scorePanel.pieceRecovered(pawnCaptureKey);
         boardMove.revertMove();
         pawnCaptureCell.normalPiece();
         pawnFromCell.normalPiece();
         pawnToCell.normalPiece();
      } catch(Exception e) {
         throw new IllegalStateException("Error occured trying to undo " + historyItem, e);
      }
   }     
   
   private void revertCastleMove(ChessHistoryItem historyItem) {
      try {
         ChessMove kingMove = historyItem.getMove();
         ChessMove castleMove = historyItem.getCastleMove();

         if(castleMove == null) {
            throw new IllegalStateException("Move was not a castle" + kingMove);
         }
         ChessBoardCellView kingFromCell = boardPanel.getCell(kingMove.from);
         ChessBoardCellView kingToCell = boardPanel.getCell(kingMove.to);
         ChessPiece kingPiece = kingToCell.getChessPiece();
         
         if(kingPiece.key.type != ChessPieceType.KING) {
            throw new IllegalStateException("Piece in cell " + kingMove.to + " is not a king for move " + kingMove);
         }
         ChessBoardCellView castleFromCell = boardPanel.getCell(castleMove.from);
         ChessBoardCellView castleToCell = boardPanel.getCell(castleMove.to);
         ChessPiece castlePiece = castleToCell.getChessPiece();
         
         if(castlePiece.key.type != ChessPieceType.ROOK) {
            throw new IllegalStateException("Piece in cell " + castleMove.to + " is not a rook for move " + kingMove); // exception should have original king move
         }
         ChessBoardMoveIterator moveIterator = chessBoard.getBoardMoves();
         
         if(!moveIterator.hasNext()) {
            throw new IllegalStateException("Move " + kingMove + " is before the first move?");
         }
         ChessBoardMove boardMove = moveIterator.next();            
         
         boardMove.revertMove();
         castleToCell.normalPiece();
         castleFromCell.normalPiece();         
         kingToCell.normalPiece();
         kingFromCell.normalPiece();
      } catch(Exception e) {
         throw new IllegalStateException("Error occured trying to revert " + historyItem, e);
      }      
   }   
   
   private void revertNormalMove(ChessHistoryItem historyItem) {
      try {
         ChessMove move = historyItem.getMove();
         ChessBoardCellView fromCell = boardPanel.getCell(move.from);
         ChessBoardCellView toCell = boardPanel.getCell(move.to);
         ChessPiece piece = toCell.getChessPiece();
   
         if(piece == null) {
            throw new IllegalStateException("No piece found in cell " + move.to + " for " + move);
         }        
         ChessPieceKey takePieceKey = historyItem.chessMove.takePiece;       
         
         if(takePieceKey != null) {
            scorePanel.pieceRecovered(takePieceKey);
         }
         ChessPieceKey pieceKey = historyItem.chessMove.fromPiece;
              
         if(piece.key != pieceKey) { // promotion
            piece = pieceKey.createPiece(move.side);            
         }         
         ChessBoardMoveIterator moveIterator = chessBoard.getBoardMoves();
         
         if(!moveIterator.hasNext()) {
            throw new IllegalStateException("Move " + move + " is before the first move?");
         }
         ChessBoardMove boardMove = moveIterator.next();            
         
         boardMove.revertMove();
         fromCell.normalPiece();
         toCell.normalPiece();              
      } catch(Exception e) {
         throw new IllegalStateException("Error occured trying to undo " + historyItem, e);
      }
   }
   
   private void showMove(ChessMove chessMove) {
      Collection<ChessBoardCellView> boardCells = boardPanel.getBoardCells();
      
      for (ChessBoardCellView boardCell : boardCells) {
         if(boardCell.isCellShadow()) {
           boardCell.resetCell();
         }
         boardCell.clearPieceStatus();
      }
      if(chessMove != null) {
         ChessBoardCellView fromCell= boardPanel.getCell(chessMove.from);
         ChessBoardCellView toCell = boardPanel.getCell(chessMove.to);
         ChessPiece chessPiece = toCell.getChessPiece();
         
         fromCell.shadowPiece(chessPiece);
         toCell.showPieceHighlight();
         
         if(navigationText != null) {
            navigationText.setText(chessMove.from + " - " + chessMove.to);
         }
      } else {
         if(navigationText != null) {
            navigationText.setText("");
         }
      }
   }
}
