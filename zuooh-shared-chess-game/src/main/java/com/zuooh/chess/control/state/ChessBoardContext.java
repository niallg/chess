package com.zuooh.chess.control.state;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.database.user.ChessUser;

public class ChessBoardContext {

   private ChessUpgradeProduct upgradeProduct;
   private ChessUserSource userSource;
   private ChessSide opponentSide;
   private ChessPlayer opponent;
   private ChessPlayer player;
   private String purchasePage;
   private String finishPage;
   private String page;
   private long lastTouch;
   private long gameDuration;
   private boolean gameRandom;
   private boolean gameOnline;
   private String themeColor;
   private String gameId;

   public ChessBoardContext(ChessUserSource userSource, ChessUpgradeProduct upgradeProduct) {
      this(userSource, upgradeProduct, null, null);
   }

   public ChessBoardContext(ChessUserSource userSource, ChessUpgradeProduct upgradeProduct, String gameId, String themeColor) {
      this.upgradeProduct = upgradeProduct;
      this.userSource = userSource;
      this.themeColor = themeColor;
      this.gameId = gameId;
   }
   
   public ChessUpgradeProduct getUpgradeProduct() {
      return upgradeProduct;
   }      

   public ChessUser getBoardUser() {
      return userSource.currentUser();
   }
   
   public String getThemeColor() {
      return themeColor;
   }
   
   public void setThemeColor(String themeColor) {
      this.themeColor = themeColor;
   }
   
   public String getUpgradePurchasePage() {
      return purchasePage;
   }
   
   public void setUpgradePurchasePage(String purchasePage) {
      this.purchasePage = purchasePage;
   }
   
   public String getUpgradeFinishPage() {
      return finishPage;
   }   
   
   public void setUpgradeFinishPage(String finishPage) {
      this.finishPage = finishPage;
   }

   public String getGameId() {
      return gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }   
   
   public boolean isGameRandom() {
      return gameRandom;
   }

   public void setGameRandom(boolean randomGame) {
      this.gameRandom = randomGame;
   }  
      
   public boolean isGameOnline() {
      return gameOnline;
   }
   
   public void setGameOnline(boolean gameOnline) {
      this.gameOnline = gameOnline;
   }
   
   public long getGameDuration() {
      return gameDuration;
   }
   
   public void setGameDuration(long gameDuration) {
      this.gameDuration = gameDuration;
   }

   public String getGamePage() {
      return page;
   }

   public void setGamePage(String page) {
      this.page = page;
   }

   public ChessSide getOpponentSide() {
      return opponentSide;
   }

   public void setOpponentSide(ChessSide opponentSide) {
      this.opponentSide = opponentSide;
   }

   public ChessPlayer getOpponent() {
      return opponent;
   }

   public void setOpponent(ChessPlayer opponent) {
      this.opponent = opponent;
   }

   public ChessPlayer getPlayer() {
      return player;
   }

   public void setPlayer(ChessPlayer player) {
      this.player = player;
   }  
}
