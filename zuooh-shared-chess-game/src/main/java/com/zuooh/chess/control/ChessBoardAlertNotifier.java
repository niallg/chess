package com.zuooh.chess.control;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.Document;
import com.zuooh.application.MobileApplication;
import com.zuooh.application.Sound;
import com.zuooh.application.SoundManager;
import com.zuooh.application.Tile;
import com.zuooh.application.Window;
import com.zuooh.application.canvas.Frame;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.board.ChessBoardStatus;

@Component
public class ChessBoardAlertNotifier {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardAlertNotifier.class);

   private final Map<ChessBoardAlert, String> alertTiles;
   private final Map<ChessBoardAlert, String> alertSounds;
   private final MobileApplication application;
   private final Document document;
   private final Window window;

   public ChessBoardAlertNotifier(
         @Inject("alertTiles") Map<ChessBoardAlert, String> alertTiles,
         @Inject("alertSounds") Map<ChessBoardAlert, String> alertSounds,         
         @Inject("application") MobileApplication application,
         @Inject("document") Document document,
         @Inject("window") Window window)
   {
      this.application = application;
      this.alertTiles = alertTiles;
      this.alertSounds = alertSounds;
      this.document = document;
      this.window = window;
   }
   
   public void alertDrawOffer() {
      alert(ChessBoardAlert.DRAW_OFFERED);      
   }

   public void alertMoveMade() {
      alert(ChessBoardAlert.MOVE_MADE);
   }

   public void alertPieceTaken() {
      alert(ChessBoardAlert.PIECE_TAKEN);
   }
   
   public void alertStatusChange(ChessBoardStatus boardStatus, ChessBoardResult result) {   
      alertStatusChange(boardStatus, result, false);
   }

   public void alertStatusChange(ChessBoardStatus boardStatus, ChessBoardResult result, boolean silent) {
      if(result == ChessBoardResult.WIN) {
         if(boardStatus == ChessBoardStatus.CHECK_MATE) {
            alert(ChessBoardAlert.CHECK_MATE_YOU_WIN, silent);
         } else if(boardStatus == ChessBoardStatus.TIME_OUT) {
            alert(ChessBoardAlert.TIMES_UP_YOU_WIN, silent);
         } else if(boardStatus == ChessBoardStatus.RESIGN) {
            alert(ChessBoardAlert.RESIGN_YOU_WIN, silent);            
         }
      } else if(result == ChessBoardResult.LOSE) {
         if(boardStatus == ChessBoardStatus.CHECK_MATE) {
            alert(ChessBoardAlert.CHECK_MATE_YOU_LOSE, silent);
         } else if(boardStatus == ChessBoardStatus.TIME_OUT) {
            alert(ChessBoardAlert.TIMES_UP_YOU_LOSE, silent);
         } else if(boardStatus == ChessBoardStatus.RESIGN) {
            alert(ChessBoardAlert.RESIGN_YOU_LOSE, silent);            
         }
      } else {
         if(boardStatus == ChessBoardStatus.CHECK) {
            alert(ChessBoardAlert.CHECK, silent);
         } else if(boardStatus == ChessBoardStatus.DRAW) {
            alert(ChessBoardAlert.DRAW_ACCEPTED, silent);
         } else if(boardStatus == ChessBoardStatus.STALE_MATE) {
            alert(ChessBoardAlert.STALE_MATE, silent);
         }
      }
   }

   public void alert(ChessBoardAlert alert) {
      alert(alert, false);
   }
   
   public void alert(ChessBoardAlert alert, boolean silent) {
      if(alert != null) {
         String tile = resolveTile(alert);
         String clip = resolveSound(alert);

         if(tile != null) {
            if(alert.isGameOver()) {
               showAlert(tile, 20000);
            } else if(alert.isChoice()) {
               showAlert(tile, 600000);               
            } else {
               showAlert(tile, 5000);
            }
         }
         if(clip != null && !silent) {
            SoundManager soundManager = application.getSoundManager();
            Sound sound = soundManager.getSound(clip);
            
            if(sound != null) {
               sound.play(1.0f);
            }
         }
      }
   }

   private void showAlert(String tileName, long duration) {
      try {
         Tile message = document.getTile(tileName);
         Frame messagePanel = message.createFrame();

         messagePanel.setFullScreen(true);
         messagePanel.setVisible(true);
         messagePanel.setDirty(true);
         
         window.resetForeground();         
         window.addForeground(messagePanel, duration);
         window.setModal(null);
         window.invalidate();
      } catch(Exception e) {
         LOG.info("Could not display dialog", e);
      }
   }
   
   private String resolveTile(ChessBoardAlert alert) {
      if(alertTiles != null) {
         return alertTiles.get(alert);
      }
      return null;
   }
   
   private String resolveSound(ChessBoardAlert alert) {
      if(alertSounds != null) {
         return alertSounds.get(alert);
      }
      return null;
   }
}
