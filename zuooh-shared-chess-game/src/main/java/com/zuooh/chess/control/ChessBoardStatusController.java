package com.zuooh.chess.control;


import com.zuooh.application.MobileApplication;
import com.zuooh.application.Screen;
import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.canvas.Toggle;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.board.ChessDrawPhase;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.result.ChessResultDatabase;
import com.zuooh.chess.database.result.ChessResultRecord;
import com.zuooh.chess.database.theme.ChessThemeDatabase;
import com.zuooh.chess.database.theme.ChessThemeDefinition;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.view.ChessBoardPanel;

@Component
public class ChessBoardStatusController {

   private final ChessDatabase gameDatabase;
   private final ChessBoardContext boardContext;
   private final ChessPlayer opponentPlayer;
   private final MobileApplication application;

   public ChessBoardStatusController(
         @Inject("boardPanel") ChessBoardPanel boardPanel,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("gameDatabase") ChessDatabase gameDatabse,         
         @Inject("application") MobileApplication application)
   {
      this.opponentPlayer = boardContext.getOpponent();
      this.boardContext = boardContext;
      this.gameDatabase = gameDatabse;
      this.application = application;
   }

   @OnCall
   public void resignGame(Toggle toggle) throws Exception {
      Screen screen = application.getScreen();
      String gameId = boardContext.getGameId();
      ChessMoveEngine playerEngine = opponentPlayer.getMoveEngine();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessResultDatabase resultDatabase = gameDatabase.getResultDatabase();      
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();
      String themeColor = boardGame.getThemeColor();
      ChessThemeDefinition theme = themeDatabase.loadThemeDefinition(themeColor);      
      ChessSide playerColor = boardGame.getUserSide();      
      String screenName = theme.getScreen(playerColor, ChessBoardStatus.RESIGN);
      
      playerEngine.resignGame();
      boardGame.setBoardResult(ChessBoardResult.LOSE);
      boardGame.setBoardStatus(ChessBoardStatus.RESIGN);
      boardDatabase.saveBoardGame(boardGame);
      resultDatabase.saveResult(boardGame);      
      screen.showPage(screenName);
      toggle.toggle();
   }
   
   @OnCall
   public void offerDraw(Toggle toggle) throws Exception {
      Screen screen = application.getScreen();
      String gameId = boardContext.getGameId();
      ChessMoveEngine playerEngine = opponentPlayer.getMoveEngine();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessResultDatabase resultDatabase = gameDatabase.getResultDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();
      String themeColor = boardGame.getThemeColor();
      ChessThemeDefinition theme = themeDatabase.loadThemeDefinition(themeColor);      
      ChessSide playerColor = boardGame.getUserSide();
      String screenName = theme.getScreen(playerColor, ChessBoardStatus.DRAW);
      ChessDrawPhase drawPhase = boardGame.getDrawPhase();
      
      playerEngine.drawGame(); // send message to remote player
      
      if(!opponentPlayer.isOnline() || drawPhase.isDrawPossible()) { // computer always accepts draw         
         boardGame.setBoardResult(ChessBoardResult.DRAW);
         boardGame.setBoardStatus(ChessBoardStatus.DRAW);
         boardDatabase.saveBoardGame(boardGame);
         resultDatabase.saveResult(boardGame);         
         screen.showPage(screenName);         
      } else {
         boardGame.setDrawPhase(ChessDrawPhase.REQUEST_SENT); // an initial request
         boardDatabase.saveBoardGame(boardGame);
         screen.hideMenu(); // hide the menu
      }
      toggle.toggle();
   }
   
   @OnCall
   public void acceptDraw(Toggle toggle) throws Exception {
      Screen screen = application.getScreen();
      String gameId = boardContext.getGameId();
      ChessMoveEngine playerEngine = opponentPlayer.getMoveEngine();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessResultDatabase resultDatabase = gameDatabase.getResultDatabase();      
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      ChessThemeDatabase themeDatabase = gameDatabase.getThemeDatabase();

      String themeColor = boardGame.getThemeColor();
      ChessThemeDefinition theme = themeDatabase.loadThemeDefinition(themeColor);      
      ChessSide playerColor = boardGame.getUserSide();
      String screenName = theme.getScreen(playerColor, ChessBoardStatus.DRAW);
      Window window = application.getWindow();
      
      playerEngine.drawGame(); // send message back
      boardGame.setDrawPhase(ChessDrawPhase.REQUEST_ACKNOWLEDGED); 
      boardGame.setBoardResult(ChessBoardResult.DRAW);
      boardGame.setBoardStatus(ChessBoardStatus.DRAW);
      boardDatabase.saveBoardGame(boardGame);      
      resultDatabase.saveResult(boardGame);
      window.resetForeground(); // for good measure!!
      screen.showPage(screenName);
      toggle.toggle();
   }
   
   @OnCall
   public void rejectDraw(Toggle toggle) throws Exception {
      String gameId = boardContext.getGameId();
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      Window window = application.getWindow();
      
      boardGame.setDrawPhase(ChessDrawPhase.REQUEST_ACKNOWLEDGED);     
      boardDatabase.saveBoardGame(boardGame);
      window.resetForeground(); // get rid of popup!
      toggle.toggle();
   }   
}
