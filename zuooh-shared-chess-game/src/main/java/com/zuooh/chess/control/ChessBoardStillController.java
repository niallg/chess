package com.zuooh.chess.control;

import com.zuooh.application.Window;
import com.zuooh.application.bind.annotation.OnCall;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.control.state.ChessBoardContext;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.result.ChessResultDatabase;
import com.zuooh.chess.database.result.ChessResultRecord;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.timer.ChessTimer;

@Component
public class ChessBoardStillController {   

   private final ChessDatabase gameDatabase;
   private final ChessBoardContext boardContext;
   private final ChessBoardAlertNotifier boardNotifier;
   private final Window window;

   public ChessBoardStillController(
         @Inject("gameDatabase") ChessDatabase gameDatabase,
         @Inject("boardContext") ChessBoardContext boardContext,
         @Inject("boardNotifier") ChessBoardAlertNotifier boardNotifier,
         @Inject("chessTimer") ChessTimer chessTimer,         
         @Inject("window") Window window)
   {
      this.boardNotifier = boardNotifier;
      this.boardContext = boardContext;
      this.gameDatabase = gameDatabase;
      this.window = window;
   }
   
   @OnStart
   public void onStart() throws Exception {
      ChessUser user = boardContext.getBoardUser();
      String userId = user.getKey();  
      String gameId = boardContext.getGameId();      
      ChessResultDatabase resultDatabase = gameDatabase.getResultDatabase();      
      ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
      ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
      ChessBoardStatus boardStatus = boardGame.getBoardStatus();
      ChessBoardResult boardResult = boardGame.getBoardResult();    
      ChessResultRecord resultRecord = resultDatabase.loadResult(userId, gameId);
      
      if(resultRecord != null) {
         long currentTime = System.currentTimeMillis();
         long resultTime = resultRecord.getResultTime();         
         long alertTime = resultRecord.getAlertTime();
         long expiryTime = resultTime + 10000;
         
         if(expiryTime > currentTime && alertTime <= 0) { // recent and not alerted yet
            resultRecord.setAlertTime(currentTime);
            resultDatabase.updateResult(resultRecord);;
            boardNotifier.alertStatusChange(boardStatus, boardResult);
         } else {
            boardNotifier.alertStatusChange(boardStatus, boardResult, true); // either old or has been alerted, no sound
         }
      } else {
         boardNotifier.alertStatusChange(boardStatus, boardResult, true); // silent alert!!
      }
      window.invalidate();
   }

   @OnCall
   public synchronized void onClick(String boardPosition) {
      window.resetForeground();
   }
}
