package com.zuooh.chess.control;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.zuooh.chess.view.ChessBoardPanel;

public class ChessBoardOpponentCancelOperation implements ChessBoardOperation {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardOpponentCancelOperation.class);

   private final String cancelForGameId;

   public ChessBoardOpponentCancelOperation(String cancelForGameId) {
      this.cancelForGameId = cancelForGameId;
   }

   @Override
   public void onOperation(ChessBoardController controller, ChessBoardPanel boardPanel, String gameId) {
      try {
         if(cancelForGameId.equals(gameId)) {
            Thread.sleep(2000);
            controller.onBeforeStart();
         }
      }catch(Exception e) {
         LOG.info("Could not restart opponent moves", e);
      }
   }
}
