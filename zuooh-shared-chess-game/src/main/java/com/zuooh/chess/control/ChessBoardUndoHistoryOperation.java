package com.zuooh.chess.control;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessBoardMoveIterator;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.history.ChessHistoryItem;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.score.ChessScorePanel;
import com.zuooh.chess.view.ChessBoardCellView;
import com.zuooh.chess.view.ChessBoardPanel;

public class ChessBoardUndoHistoryOperation implements ChessBoardOperation {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessBoardUndoHistoryOperation.class);
   
   private final ChessBoardEventScheduler eventScheduler;
   private final ChessDatabase gameDatabase;
   private final ChessScorePanel scorePanel;
   private final ChessBoardEvent boardEvent;
   private final ChessPlayer chessPlayer;
   private final ChessSide playerSide;
   private final int moveCount;
   
   public ChessBoardUndoHistoryOperation(ChessBoardEventScheduler eventScheduler, ChessPlayer chessPlayer, ChessDatabase gameDatabase, ChessScorePanel scorePanel, ChessSide playerSide, int moveCount) {
      this.boardEvent = new ChessBoardNoOperationEvent();
      this.eventScheduler = eventScheduler;
      this.gameDatabase = gameDatabase;
      this.chessPlayer = chessPlayer;
      this.scorePanel = scorePanel;
      this.playerSide = playerSide;
      this.moveCount = moveCount;
   }

   @Override
   public void onOperation(ChessBoardController boardController, ChessBoardPanel boardPanel, String gameId) { 
      try {        
         ChessHistoryDatabase historyDatabase = gameDatabase.getHistoryDatabase();
         int historyCount = historyDatabase.countOfHistoryItems(gameId);
         
         if(historyCount == moveCount) { // don't allow very first move to be undone
            List<ChessHistoryItem> lastItems = historyDatabase.deleteLastHistoryItems(gameId, playerSide);
            
            if(!lastItems.isEmpty()) {
               ChessSide opponentSide = playerSide.oppositeSide();
               ChessMoveEngine moveEngine = chessPlayer.getMoveEngine();
               
               boardPanel.setSelectedCell(null);
               boardController.onClearShadows();
               moveEngine.stopSearch(); // this is used to reset the last move
               eventScheduler.cancelAll();
               
               for(ChessHistoryItem historyItem : lastItems) {
                  onUndoHistory(boardPanel, historyItem);
               }
               boardController.onUpdateBoardStatus(playerSide);
               boardController.onUpdateBoardStatus(opponentSide);
               boardController.onRefresh();
               
               eventScheduler.executeEvent(boardEvent); // trip event scheduler!!!
            } else {
               boardPanel.setSelectedCell(null);
               boardController.onClearShadows();           
               eventScheduler.cancelAll();
            }
            boardController.onBeforeStart();         
         }
      } catch(Exception e) {
         LOG.info("Could not undo history for " + gameId, e);
      }
   }
   
   private void onUndoHistory(ChessBoardPanel boardPanel, ChessHistoryItem historyItem) {
      try {
         ChessMove move = historyItem.getMove();
         ChessMove castleMove = historyItem.getCastleMove();                
         
         if(castleMove != null) {
            onUndoCastleMove(boardPanel, historyItem);
         } else {                     
            ChessBoardPosition takePosition = historyItem.chessMove.takePosition;
                  
            if(takePosition != null && takePosition != move.to) {
               onUndoEnPassentMove(boardPanel, historyItem);
            } else {
               onUndoNormalMove(boardPanel, historyItem);               
            }
         }
         ChessBoardGameDatabase boardDatabase = gameDatabase.getBoardDatabase();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(historyItem.gameId);
         
         boardGame.setBoardStatus(historyItem.status);
         boardDatabase.saveBoardGame(boardGame);
      } catch(Exception e) {
         throw new IllegalStateException("Could not change history for " + historyItem.gameId, e);
      }
   }
   
   private void onUndoEnPassentMove(ChessBoardPanel boardPanel, ChessHistoryItem historyItem) {
      try {
         ChessMove pawnMove = historyItem.getMove();
         ChessBoardCellView pawnFromCell = boardPanel.getCell(pawnMove.from);
         ChessBoardCellView pawnToCell = boardPanel.getCell(pawnMove.to);
         ChessPiece pawnPiece = pawnToCell.getChessPiece();
         ChessBoard chessBoard = boardPanel.getChessBoard();
         
         if(pawnPiece.key.type != ChessPieceType.PAWN) {
            throw new IllegalStateException("Piece in cell " + pawnMove.to + " is not a pawn for move " + pawnMove);
         }
         ChessBoardPosition pawnCapturePosition = historyItem.chessMove.takePosition;         
         ChessBoardCellView pawnCaptureCell = boardPanel.getCell(pawnCapturePosition);
         ChessPieceKey pawnCaptureKey = historyItem.chessMove.takePiece;
         
         if(pawnCaptureKey == null || pawnCaptureKey.type != ChessPieceType.PAWN) {
            throw new IllegalStateException("Capture piece at " + pawnCapturePosition + " was not a pawn for " + pawnMove);
         }
         ChessBoardMoveIterator moveIterator = chessBoard.getBoardMoves();
         
         if(!moveIterator.hasNext()) {
            throw new IllegalStateException("Move " + pawnMove + " is before the first move?");
         }
         ChessBoardMove boardMove = moveIterator.next();            
         
         scorePanel.pieceRecovered(pawnCaptureKey);
         boardMove.revertMove();
         pawnToCell.normalPiece();
         pawnFromCell.normalPiece();
         pawnCaptureCell.normalPiece();
      } catch(Exception e) {
         throw new IllegalStateException("Error occured trying to undo " + historyItem, e);
      }
   }     
   
   private void onUndoCastleMove(ChessBoardPanel boardPanel, ChessHistoryItem historyItem) {
      try {
         ChessMove kingMove = historyItem.getMove();
         ChessMove castleMove = historyItem.getCastleMove();
         ChessBoard chessBoard = boardPanel.getChessBoard();
         
         if(castleMove == null) {
            throw new IllegalStateException("Move was not a castle" + kingMove);
         }
         ChessBoardCellView kingFromCell = boardPanel.getCell(kingMove.from);
         ChessBoardCellView kingToCell = boardPanel.getCell(kingMove.to);
         ChessPiece kingPiece = kingToCell.getChessPiece();
         
         if(kingPiece.key.type != ChessPieceType.KING) {
            throw new IllegalStateException("Piece in cell " + kingMove.to + " is not a king for move " + kingMove);
         }
         ChessBoardCellView castleFromCell = boardPanel.getCell(castleMove.from);
         ChessBoardCellView castleToCell = boardPanel.getCell(castleMove.to);
         ChessPiece castlePiece = castleToCell.getChessPiece();
         
         if(castlePiece.key.type != ChessPieceType.ROOK) {
            throw new IllegalStateException("Piece in cell " + castleMove.to + " is not a rook for move " + kingMove); // exception should have original king move
         }
         ChessBoardMoveIterator moveIterator = chessBoard.getBoardMoves();
         
         if(!moveIterator.hasNext()) {
            throw new IllegalStateException("Move " + kingMove + " is before the first move?");
         }
         ChessBoardMove boardMove = moveIterator.next();            
         
         boardMove.revertMove();
         castleToCell.normalPiece();
         castleFromCell.normalPiece();         
         kingToCell.normalPiece();
         kingFromCell.normalPiece();
      } catch(Exception e) {
         throw new IllegalStateException("Error occured trying to revert " + historyItem, e);
      }      
   }   
   
   private void onUndoNormalMove(ChessBoardPanel boardPanel, ChessHistoryItem historyItem) {
      try {
         ChessMove move = historyItem.getMove();
         ChessBoardCellView fromCell = boardPanel.getCell(move.from);
         ChessBoardCellView toCell = boardPanel.getCell(move.to);
         ChessPiece piece = toCell.getChessPiece();
         ChessBoard chessBoard = boardPanel.getChessBoard();
         
         if(piece == null) {
            throw new IllegalStateException("No piece found in cell " + move.to + " for " + move);
         }
         ChessPieceKey pieceKey = historyItem.chessMove.fromPiece;        
         ChessPieceKey takePieceKey = historyItem.chessMove.takePiece;       
         
         if(piece.key != pieceKey) { // promotion
            piece = pieceKey.createPiece(move.side);            
         }  
         ChessBoardMoveIterator moveIterator = chessBoard.getBoardMoves();
         
         if(!moveIterator.hasNext()) {
            throw new IllegalStateException("Move " + move + " is before the first move?");
         }
         ChessBoardMove boardMove = moveIterator.next();
         
         if(takePieceKey != null) {
            scorePanel.pieceRecovered(takePieceKey);
         }
         boardMove.revertMove();
         fromCell.normalPiece();
         toCell.normalPiece();            
      } catch(Exception e) {
         throw new IllegalStateException("Error occured trying to undo " + historyItem, e);
      }
   }
}
