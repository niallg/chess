package com.zuooh.chess.view;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.zuooh.application.canvas.Panel;
import com.zuooh.application.context.Context;
import com.zuooh.bind.annotation.Component;
import com.zuooh.bind.annotation.Inject;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.control.state.ChessBoardStateSet;
import com.zuooh.chess.database.theme.ChessThemePiece;
import com.zuooh.chess.score.ChessScorePanel;

@Component
public class ChessBoardPanel {

   private final Map<ChessBoardPosition, ChessBoardCellView> boardCells;
   private final ChessBoard chessBoard;

   private ChessBoardCellView selectedCell;

   public ChessBoardPanel(
         @Inject("boardStateSet") ChessBoardStateSet boardSet,
         @Inject("scorePanel") ChessScorePanel scorePanel,
         @Inject("board") Context board)
   {
      Map<ChessBoardPosition, ChessBoardCellView> boardCells = new HashMap<ChessBoardPosition, ChessBoardCellView>();
      ChessBoard chessBoard = boardSet.getChessBoard();

      for(ChessBoardPosition pos : ChessBoardPosition.values()) {
         ChessBoardCell boardCell = chessBoard.getBoardCell(pos);
         Panel area = board.getCanvas(pos.code);
         Panel background = board.getCanvas(pos.code + "B"); // e.g A1B         
         ChessBoardCellView cellView = new ChessBoardCellView(boardSet, boardCell, area, background);
         cellView.drawPiece();
         area.setText(pos.code);
         boardCells.put(pos, cellView);
      }
      Set<ChessPieceKey> piecesTaken = boardSet.getPiecesTaken();

      if(scorePanel != null) {
         for(ChessPieceKey pieceKey : piecesTaken) {
            ChessThemePiece pieceView = boardSet.getThemePiece(pieceKey);
            if(pieceView != null) {
               scorePanel.pieceTaken(pieceKey, pieceView);
            }
         }
      }
      this.boardCells = Collections.unmodifiableMap(boardCells);
      this.chessBoard = chessBoard;
   }

   public ChessBoardCellView getKingCell(ChessSide side) {
      ChessBoardPosition kingPosition = chessBoard.getKingPosition(side);
      return boardCells.get(kingPosition);
   }

   public ChessBoard getChessBoard() {
      return chessBoard;
   }

   public Collection<ChessBoardCellView> getBoardCells() {
      return boardCells.values();
   }

   public ChessBoardCellView getCell(ChessBoardPosition position) {
      return boardCells.get(position);
   }

   public ChessBoardCellView getCell(String position) {
      return getCell(ChessBoardPosition.at(position));
   }

   public ChessBoardCellView getSelectedCell() {
      return selectedCell;
   }

   public void setSelectedCell(ChessBoardCellView selectedCell) {
      this.selectedCell = selectedCell;
   }
}
