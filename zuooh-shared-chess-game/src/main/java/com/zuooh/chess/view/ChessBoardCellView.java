package com.zuooh.chess.view;

import java.util.concurrent.atomic.AtomicReference;

import com.zuooh.application.canvas.Panel;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessCellStatus;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.control.state.ChessBoardStateSet;
import com.zuooh.chess.database.theme.ChessThemePiece;
import com.zuooh.style.Point;
import com.zuooh.style.Rectangle;
import com.zuooh.style.Style;
import com.zuooh.style.image.Image;

public class ChessBoardCellView  {

   private final AtomicReference<ChessCellStatus> boardCellStatus;
   private final AtomicReference<ChessPiece> statusPiece;
   private final ChessBoardCell chessBoardCell;
   private final ChessBoardPosition boardPosition;
   private final ChessBoardStateSet boardSet;
   private final Rectangle panelArea;
   private final Style panelStyle;
   private final Panel background;
   private final Panel panel;

   public ChessBoardCellView(ChessBoardStateSet boardSet, ChessBoardCell chessBoardCell, Panel panel) {
      this(boardSet, chessBoardCell, panel, null);
   }
   
   public ChessBoardCellView(ChessBoardStateSet boardSet, ChessBoardCell chessBoardCell, Panel panel, Panel background) {
      this.boardCellStatus = new AtomicReference<ChessCellStatus>();
      this.statusPiece = new AtomicReference<ChessPiece>();
      this.boardPosition = chessBoardCell.getPosition();
      this.panelArea = panel.getRectangle();
      this.panelStyle = panel.getStyle();
      this.chessBoardCell = chessBoardCell;
      this.background = background;
      this.boardSet = boardSet;
      this.panel = panel;
   }

   public Rectangle getRectangle() {
      return panel.getRectangle();
   }

   public boolean isCellShadow() {
      return getCellStatus() == ChessCellStatus.SHADOW;
   }

   public boolean isCellEmpty() {
      return chessBoardCell.isCellEmpty();
   }

   public ChessBoardPosition getPosition() {
      return chessBoardCell.getPosition();
   }

   public ChessThemePiece getPieceView() {
      ChessPiece chessPiece = chessBoardCell.getPiece();

      if(isCellShadow()) {
         chessPiece = statusPiece.get();
      }
      if(chessPiece == null) {
         return null;
      }
      return boardSet.getThemePiece(chessPiece.key);
   }

   public ChessBoardCell getChessBoardCell() {
      return chessBoardCell;
   }

   public ChessPiece getChessPiece() {
      return chessBoardCell.getPiece();
   }

   public ChessCellStatus getCellStatus() {
      return boardCellStatus.get();
   }

   public void resetCell() {
      boardCellStatus.set(null);
      statusPiece.set(null);
      clearPieceHighlight();
      drawPiece();
   }

   public void underAttackPiece() {
      boardCellStatus.set(ChessCellStatus.UNDER_ATTACK);
      statusPiece.set(null);
      drawPiece();
   }

   public void selectedPiece() {
      boardCellStatus.set(ChessCellStatus.SELECTED);
      statusPiece.set(null);
      drawPiece();
   }

   public void shadowPiece(ChessPiece chessPiece) {
      boardCellStatus.set(ChessCellStatus.SHADOW);
      statusPiece.set(chessPiece);
      drawPiece();
   }
   
   public void normalPiece() {     
      boardCellStatus.set(ChessCellStatus.NORMAL);
      statusPiece.set(null);
      drawPiece();    
   }     

   public void clearPieceStatus() {
      boardCellStatus.set(null);
      statusPiece.set(null);
      clearPieceHighlight();
      drawPiece();
   }
   
   public void clearPieceHighlight() {
      if(background != null) {
         background.setVisible(false);
         background.setDirty(false);
      }
   }
   
   public void showPieceHighlight() {
      if(background != null) {
         background.setVisible(true);
         background.setDirty(true);
      }
   }

   public boolean isWithinCellBounds(int x, int y) {
      if (x >= panelArea.x && x <= panelArea.x + panelArea.width) {
         if (y >= panelArea.y && y <= panelArea.y + panelArea.height) {
            return true;
         }
      }
      return false;
   }

   public Point getCenterPoint() {
      return new Point(panelArea.x + (panelArea.width / 2), panelArea.y + (panelArea.height / 2));
   }

   public Point getCenterPointOnBottom() {
      return new Point(panelArea.x + panelArea.width, panelArea.y + (panelArea.height / 2));
   }

   public Point getTopRightPoint() {
      ChessThemePiece chessPiece = getPieceView();
      ChessCellStatus chessPieceStatus = getCellStatus();

      if (chessPiece != null) {
         int height = chessPiece.getHeight(chessPieceStatus, boardPosition.color);
         int width = chessPiece.getWidth(chessPieceStatus, boardPosition.color);
         int widthMiddle = width / 2;
         int heightMiddle = height / 2;
         int topRightCornerX = panelArea.x + (panelArea.width / 2) + widthMiddle;
         int topRightCornerY = panelArea.y + (panelArea.height / 2) - heightMiddle;

         return new Point(topRightCornerX, topRightCornerY);
      }
      return null;
   }

   public Point getTopLeftPoint() {
      ChessThemePiece chessPiece = getPieceView();
      ChessCellStatus chessPieceStatus = getCellStatus();

      if (chessPiece != null) {
         int height = chessPiece.getHeight(chessPieceStatus, boardPosition.color);
         int width = chessPiece.getWidth(chessPieceStatus, boardPosition.color);
         int widthMiddle = width / 2;
         int heightMiddle = height / 2;
         int topLeftCornerX = panelArea.x + (panelArea.width / 2) - widthMiddle;
         int topLeftCornerY = panelArea.y + (panelArea.height / 2) - heightMiddle;

         return new Point(topLeftCornerX, topLeftCornerY);
      }
      return null;
   }

   public void drawPiece() {
      ChessThemePiece chessPiece = getPieceView();
      ChessCellStatus chessPieceStatus = getCellStatus();

      if (chessPiece != null) {
         Image image = chessPiece.getImage(chessPieceStatus, boardPosition.color);

         if(image != null) {
            panel.setImage(image);
         }
      } else {
         panel.setImage(null);
      }
   }

   public String toString() {
      return String.format("%s=%s", boardPosition, chessBoardCell.getPiece());
   }
}
