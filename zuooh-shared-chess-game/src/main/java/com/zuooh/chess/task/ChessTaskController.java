package com.zuooh.chess.task;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.bind.annotation.OnStart;
import com.zuooh.application.bind.annotation.OnStop;
import com.zuooh.bind.annotation.Component;
import com.zuooh.common.task.Task;
import com.zuooh.common.task.TaskScheduler;

@Component
public abstract class ChessTaskController implements Task {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessTaskController.class);

   private volatile boolean schedulerActive;
   private volatile boolean started;
   
   public final long executeTask() {
      try {
         long sleepFrequency = 100;
         
         if(!started) {
            try {               
               onBeforeStart(); // keep actions of the main thread in android!!!
            } catch(Exception e) {
               LOG.info("Error before starting handler", e);
            } finally {
               started = true;
            }
         }
         while(schedulerActive) {
            synchronized(this) {
               if(schedulerActive) {
                  sleepFrequency = onInterval();
               }
            }
            if(sleepFrequency <= 0) {
               return -1;
            }
            if(sleepFrequency < 100) {
               sleepFrequency = 100;
            }
            return sleepFrequency;
         }
      }catch(Exception e) {
         LOG.info("Error while handler was running, stopping loop", e);
      }
      return -1;
   }

   @OnStart
   public final void onStart(MobileApplication application) {
      if(!schedulerActive) {
         TaskScheduler scheduler = application.getTaskScheduler();
         
         schedulerActive = true;
         scheduler.scheduleTask(this, 0);
      }
   }

   @OnStop
   public final void onStop() {
      if(schedulerActive) {
         synchronized(this) {
            schedulerActive = false;
            try {
               onBeforeStop();
            }catch(Exception e) {
               LOG.info("Error occured calling the stop method", e);
            }
         }
      }
   }

   public final boolean isStopped() {
      return schedulerActive == false;
   }

   public final boolean isActive() {
      return schedulerActive;
   }

   public void onBeforeStop() throws Exception {}
   public void onBeforeStart() throws Exception {}
   public abstract long onInterval() throws Exception;

}
