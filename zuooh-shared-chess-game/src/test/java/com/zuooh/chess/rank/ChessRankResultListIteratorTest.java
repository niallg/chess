package com.zuooh.chess.rank;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.database.user.ChessUserType;

public class ChessRankResultListIteratorTest extends TestCase {
     
   public void testBoundaries() throws Exception {
      List<ChessUser> profiles = new ArrayList<ChessUser>();
      ChessUserData playerData = new ChessUserData();
      
      for(int i = 0; i < 5; i++) {
         ChessUser profile = new ChessUser(playerData, ChessUserType.NORMAL, ""+i, "playerName-" + i, "mail-"+i, null);
         profiles.add(profile);
      }
      ChessUser profile = new ChessUser(playerData, ChessUserType.NORMAL, "3", "playerName-3", "mail-3", null);
      ChessRankResultListIterator iterator = new ChessRankResultListIterator(profile, profiles, 2);
      
      ChessUser first = iterator.nextResult().getProfile();
      
      assertNotNull(first);
      assertEquals(first.getName(), "playerName-3");
      assertEquals(first.getKey(), "mail-3");
      
      assertEquals(iterator.nextResult().getProfile().getKey(), "mail-4");
      assertEquals(iterator.nextResult().getProfile().getKey(), "mail-4");
      assertEquals(iterator.nextResult().getProfile().getKey(), "mail-4");
      
      assertEquals(iterator.previousResult().getProfile().getKey(), "mail-3");
      assertEquals(iterator.previousResult().getProfile().getKey(), "mail-2");
      assertEquals(iterator.previousResult().getProfile().getKey(), "mail-1");
      assertEquals(iterator.previousResult().getProfile().getKey(), "mail-0");
      assertEquals(iterator.previousResult().getProfile().getKey(), "mail-0");
      assertEquals(iterator.previousResult().getProfile().getKey(), "mail-0");  
      
      assertEquals(iterator.nextResult().getProfile().getKey(), "mail-1");
      assertEquals(iterator.nextResult().getProfile().getKey(), "mail-2");
      assertEquals(iterator.nextResult().getProfile().getKey(), "mail-3");
   }
   
   public void testIteration() throws Exception {
      List<ChessUser> profiles = new ArrayList<ChessUser>();
      ChessUserData playerData = new ChessUserData();
      
      for(int i = 0; i < 1000; i++) {
         ChessUser profile = new ChessUser(playerData, ChessUserType.NORMAL, "" + i, "playerName-" + i, "mail-"+i, null);
         profiles.add(profile);
      }
      ChessUser profile = new ChessUser(playerData, ChessUserType.NORMAL, "251", "playerName-251", "mail-251", null);
      ChessRankResultListIterator iterator = new ChessRankResultListIterator(profile, profiles);
      
      ChessUser first = iterator.nextResult().getProfile();
      
      assertNotNull(first);
      assertEquals(first.getName(), "playerName-251");
      assertEquals(first.getKey(), "mail-251");
      
      int expect = 251;
      
      for(int i = 0; i < 500; i++) {
         ChessUser next = iterator.nextResult().getProfile();
         
         expect++; // expect the next in sequence
         
         assertNotNull(next);
         assertEquals("At index " + i+ " expecting " + expect, next.getName(), "playerName-" + expect);
         assertEquals("At index " + i+ " expecting " + expect, next.getKey(), "" + expect);
         System.err.println(""+expect);
      }
      for(int i = 0; i < 500; i++) {
         ChessUser previous = iterator.previousResult().getProfile();
         
         expect--; // expect previous in sequence
         
         assertNotNull(previous);
         assertEquals("At index " + i+ " expecting " + expect, previous.getName(), "playerName-" + expect);
         assertEquals("At index " + i+ " expecting " + expect, previous.getKey(), "" + expect);
         System.err.println(""+expect);
      }
   }

}
