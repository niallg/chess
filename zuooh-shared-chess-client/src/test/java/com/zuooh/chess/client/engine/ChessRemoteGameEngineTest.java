package com.zuooh.chess.client.engine;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.client.event.ChessEvent;
import com.zuooh.chess.client.event.ChessMoveEvent;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessRemoteGameEngineTest extends TestCase {

   private static final String[][] SET = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};

   public void testGameEngine() throws Exception {
      MockController controller = new MockController();
      ChessRemoteGameMoveEngine engine = new ChessRemoteGameMoveEngine(controller, "me", "test-game-1");
      ChessPieceSet pieceSet = new ChessTextPieceSet(SET);
      ChessBoard board = new ChessPieceSetBoard(pieceSet);
      ChessMove move = new ChessMove(ChessBoardPosition.A1, ChessBoardPosition.A2, ChessSide.WHITE, 1);
      ChessMove opponentMove = new ChessMove(ChessBoardPosition.H1, ChessBoardPosition.H2, ChessSide.BLACK, 2);

      engine.makeMove(board, move);

      controller.statusOnline("test-game-1");
      controller.makeMove("test-game-1", move);

      assertEquals(engine.getLastMove().getSide(), ChessSide.WHITE);
      assertEquals(engine.getLastMove().getFrom(), ChessBoardPosition.A1);
      assertEquals(engine.getLastMove().getTo(), ChessBoardPosition.A2);

      controller.makeMove("test-game-1", opponentMove);

      assertEquals(engine.getEngineStatus(), ChessOpponentStatus.ONLINE);
      assertEquals(engine.getLastMove().getSide(), ChessSide.BLACK);
      assertEquals(engine.getLastMove().getFrom(), ChessBoardPosition.H1);
      assertEquals(engine.getLastMove().getTo(), ChessBoardPosition.H2);
   }

   public static class MockController implements ChessRemoteGameController {

      private final Map<String, ChessGameObserver> acceptObservers;
      private final Map<String, ChessMoveObserver> gameObservers;

      public MockController() {
         this.acceptObservers = new ConcurrentHashMap<String, ChessGameObserver>();
         this.gameObservers = new ConcurrentHashMap<String, ChessMoveObserver>();
      }

      public void statusOnline(String gameId) {
         gameObservers.get(gameId).onStatus(ChessOpponentStatus.ONLINE);
      }

      public void makeMove(String gameId, ChessMove move) {
         gameObservers.get(gameId).onMove(move);
      }

      public void resign(String gameId) {
         gameObservers.get(gameId).onResign();
      }

      public void removeGameObserver(String gameId) {
         acceptObservers.remove(gameId);
      }

      public void setGameObserver(String gameId, ChessGameObserver gameObserver) {
         acceptObservers.put(gameId, gameObserver);
      }

      public void removeActionObserver(String gameId) {
         gameObservers.remove(gameId);
      }

      public void setActionObserver(String gameId, ChessMoveObserver gameObserver) {
         gameObservers.put(gameId, gameObserver);
      }

      @Override
      public void executeRequest(ChessEvent chessEvent) {
         if(chessEvent instanceof ChessMoveEvent) {
            ChessMoveEvent moveEvent = (ChessMoveEvent)chessEvent;
            ChessMove move = moveEvent.getMove();
            String gameId = moveEvent.getGameId();
            ChessMoveObserver listener = gameObservers.get(gameId);

            listener.onMove(move);
         }
      }
   }
}
