package com.zuooh.chess.client;

import com.zuooh.chess.database.user.ChessUserStatus;

import junit.framework.TestCase;

public class ChessUserStatusTest extends TestCase {
   
   public void testStatus() {
      assertEquals(ChessUserStatus.BEGINNER, ChessUserStatus.calculateStatus(1, 90));
      assertEquals(ChessUserStatus.BEGINNER_GOOD, ChessUserStatus.calculateStatus(3, 90));
      assertEquals(ChessUserStatus.EXPERIENCED_GOOD, ChessUserStatus.calculateStatus(40, 90));        
   }

}
