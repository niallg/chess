package com.zuooh.chess.client.connect;

import com.zuooh.chess.database.user.ChessUser;

public class ChessQuery {

   private final ChessUser user;
   private final String protocol;
   
   public ChessQuery(ChessUser user, String protocol) {
      this.protocol = protocol;
      this.user = user;      
   }

   public ChessUser getUser() {
      return user;
   }

   public String getProtocol() {
      return protocol;
   }
   
   @Override
   public String toString() {
      return String.valueOf(user);
   }
}
