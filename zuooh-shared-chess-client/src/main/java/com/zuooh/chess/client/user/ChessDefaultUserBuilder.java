package com.zuooh.chess.client.user;

import static com.zuooh.chess.database.user.ChessUserType.ANONYMOUS;
import static com.zuooh.chess.database.user.ChessUserType.NORMAL;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;

public class ChessDefaultUserBuilder implements ChessUserBuilder {
   
   public static final String ANONYMOUS_NAME = "Anonymous";   

   private final String defaultUserMail;
   private final String defaultUserId;

   public ChessDefaultUserBuilder(String defaultUserId, String defaultUserMail) {
      this.defaultUserMail = defaultUserMail;
      this.defaultUserId = defaultUserId;
   }
   
   @Override
   public ChessUser createAnonymousUser() {
      ChessUserData data = createUserData();      
      return new ChessUser(data, ANONYMOUS, defaultUserId, ANONYMOUS_NAME, defaultUserMail, null);
   }
   
   @Override
   public ChessUser createNormalUser(String userName, String password) {
      ChessUserData data = createUserData();

      if(userName == null) {
         throw new IllegalArgumentException("User name must not be null");
      }
      if(userName.equals("")) {
         throw new IllegalArgumentException("User name must not be blank");
      }
      if(password == null) {
         throw new IllegalArgumentException("User password must not be null");
      }
      if(password.equals("")) {
         throw new IllegalArgumentException("User password cannot be blank");
      }
      return new ChessUser(data, NORMAL, defaultUserId, userName, defaultUserMail, password);
   }   
   
   private ChessUserData createUserData() {
      return new ChessUserData();
   }
}
