package com.zuooh.chess.client.event.chat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.database.chat.ChessChatStatus;

public class ChessChatController {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessChatController.class);

   private final ChessMessagePublisher messagePublisher;
   private final ChessDatabase gameDatabase;

   public ChessChatController(ChessDatabase gameDatabase, ChessMessagePublisher messagePublisher) {
      this.messagePublisher = messagePublisher;
      this.gameDatabase = gameDatabase;
   }

   public void updateChat(ChessChat chat) {
      if (chat != null) {
         try {
            ChessChatDatabase chatRoom = gameDatabase.getChatDatabase();
            
            chatRoom.saveMessage(chat);
            updateChatStatus(chat);
         } catch (Exception e) {
            LOG.info("Could not save chat", e);
         }
      }
   }
   
   public void updateChatStatus(ChessChat chat) {
      if(chat != null) {
         String chatId = chat.getChatId();
         ChessChatStatusEvent event = new ChessChatStatusEvent(ChessChatStatus.READ, chatId);
         
         try {
            messagePublisher.publishNotification(event);
         } catch (Exception e) {
            LOG.info("Could not update status of message " + chatId, e);
         }
      }
   }   
}
