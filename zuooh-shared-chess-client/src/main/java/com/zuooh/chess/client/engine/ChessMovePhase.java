package com.zuooh.chess.client.engine;

public enum ChessMovePhase {
   BEGIN_MOVE,
   MOVE_MADE,
   MOVE_CONFIRMED,
   OPPONENT_MOVE_MADE;
}
