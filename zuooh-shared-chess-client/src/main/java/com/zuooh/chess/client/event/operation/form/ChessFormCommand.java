package com.zuooh.chess.client.event.operation.form;

import java.util.Map;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.ChessRequestEvent;
import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.client.event.operation.ChessRequestCommand;
import com.zuooh.chess.client.event.operation.ChessRequestCommandClient;
import com.zuooh.message.client.ResponseStatus;

public class ChessFormCommand<A extends ChessRequestEvent, B extends ChessResponseEvent> {

   private final ChessRequestCommand<A, B> operation;
   private final ChessFormAction<A, B> action;
   
   public ChessFormCommand(ChessMessagePublisher publisher, ChessFormAction<A, B> action, Class<A> request, Class<B> response) {
      this.operation = new ChessRequestCommandClient<A, B>(publisher, request, response);
      this.action = action;
   }
   
   public ChessFormAction<A, B> getAction() {
      return action;
   }  
   
   public A getRequest() {
      return operation.getRequest();
   }
   
   public B getResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute(Map<String, String> form) {
      try {
         A request = action.createRequest(form);
         
         operation.update(request);
         operation.execute();
      } catch(Exception e) {
         throw new IllegalStateException("Unable to dispatch request", e);
      }
   }
   
   public void reset() {
      operation.clear();
   }
}
