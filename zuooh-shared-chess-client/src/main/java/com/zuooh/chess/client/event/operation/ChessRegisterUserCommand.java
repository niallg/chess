package com.zuooh.chess.client.event.operation;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.request.ChessRegisterUserEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.chess.client.user.ChessUserBuilder;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.message.client.ResponseStatus;

public class ChessRegisterUserCommand {

   private final ChessRequestCommand<ChessRegisterUserEvent, ChessUserValidationEvent> operation;
   private final ChessUserBuilder builder;
   
   public ChessRegisterUserCommand(ChessMessagePublisher publisher, ChessUserBuilder builder) {
      this.operation = new ChessRequestCommandClient<ChessRegisterUserEvent, ChessUserValidationEvent>(publisher, ChessRegisterUserEvent.class, ChessUserValidationEvent.class);
      this.builder = builder;
   }
   
   public ChessRegisterUserEvent getRegisterRequest() {
      return operation.getRequest();
   }
   
   public ChessUserValidationEvent getValidationResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute(String userName, String password) {
      ChessUser user = builder.createNormalUser(userName, password);
      ChessRegisterUserEvent event = new ChessRegisterUserEvent(user);
      
      operation.update(event);
      operation.execute();      
   }
   
   public void reset() {
      operation.clear();
   }
}
