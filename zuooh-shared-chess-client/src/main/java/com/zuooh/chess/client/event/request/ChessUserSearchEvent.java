package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessUserSearchEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;
   
   private String term;

   public ChessUserSearchEvent(String term) {
      this.term = term;
   }

   public String getTerm(){
      return term;
   }

   public void setTerm(String term) {
      this.term = term;
   }


}
