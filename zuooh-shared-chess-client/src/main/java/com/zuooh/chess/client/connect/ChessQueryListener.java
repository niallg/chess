package com.zuooh.chess.client.connect;

public interface ChessQueryListener {
   void update(ChessQuery query) throws Exception;
}
