package com.zuooh.chess.client.event.response;

import java.util.ArrayList;
import java.util.List;

import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.database.user.ChessUser;

public class ChessRankResultEvent extends ChessResponseEvent {

   private static final long serialVersionUID = 1L;

   private List<ChessUser> users;

   public ChessRankResultEvent() {
      this.users = new ArrayList<ChessUser>();
   }

   public List<ChessUser> getUsers() {
      return users;
   }

   public void addUser(ChessUser user) {
      users.add(user);
   }
}
