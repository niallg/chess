package com.zuooh.chess.client.event;

public class ChessResignEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;

   private final String opponentId;
   private final String userId;
   private final String gameId;

   public ChessResignEvent(String userId, String opponentId, String gameId) {
      this.opponentId = opponentId;
      this.userId = userId;
      this.gameId = gameId;
   }
   
   public String getOpponentId() {
      return opponentId;
   }

   public String getGameId() {
      return gameId;
   }

   public String getUserId() {
      return userId;
   }
}
