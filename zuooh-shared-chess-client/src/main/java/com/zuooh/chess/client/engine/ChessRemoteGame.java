package com.zuooh.chess.client.engine;

import java.util.concurrent.atomic.AtomicReference;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessRemoteGame implements ChessGameObserver {

   private final AtomicReference<ChessOpponentStatus> lastStatus;
   private final AtomicReference<ChessGameCriteria> challenge;
   private final ChessRemoteGameMoveEngine engine;
   private final String gameId;

   public ChessRemoteGame(String gameId, ChessRemoteGameMoveEngine engine, ChessGameCriteria challenge) {
      this.lastStatus = new AtomicReference<ChessOpponentStatus>(ChessOpponentStatus.OFFLINE);
      this.challenge = new AtomicReference<ChessGameCriteria>(challenge);
      this.engine = engine;
      this.gameId = gameId;
   }

   @Override
   public void onGameUpdate(ChessGame game) {
      ChessGameCriteria gameCriteria = game.getCriteria();
      String associatedId = game.getGameId();

      if(associatedId.equals(gameId)) {
         ChessUser opponent = gameCriteria.getUser();
         
         if(opponent != null) { // resigned before accept!
            String opponentId = opponent.getKey();
            
            challenge.set(gameCriteria);
            engine.setOpponent(opponentId);
         }
      }
   }   
   
   public ChessUser getUserProfile(){
      ChessGameCriteria challenge = getChallenge();
      ChessUser profile = challenge.getUser();
      
      return profile;
   }

   public ChessSide getUserSide() {
      ChessGameCriteria challenge = getChallenge();
      ChessSide userSide = challenge.getUserSide();
      
      return userSide;
   }

   public String getName() {
      ChessGameCriteria challenge = getChallenge();
      ChessUser user = challenge.getUser();
      
      if(user != null) {
         return user.getKey();
      }
      return null;
   }

   public int getSkill() {
      ChessGameCriteria challenge = getChallenge();
      ChessUser user = challenge.getUser();
      
      if(user != null) {
         ChessUserData userData = user.getData();
         return userData.getSkill();
      }
      return 0;
   }   

   public ChessOpponentStatus getStatus() {
      return lastStatus.get();
   }

   public ChessGameCriteria getChallenge() {
      return challenge.get();
   }

   public String getAssociatedGame() {
      return gameId;
   }
}
