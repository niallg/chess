package com.zuooh.chess.client.event.presence;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserType;
import com.zuooh.common.task.Task;

public class ChessPresenceNotificationPublisher implements Task {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessPresenceNotificationPublisher.class);
   
   private final ChessUserSource userSource;
   private final ChessMessagePublisher publisher;
   private final AtomicReference<String> presence;
   private final AtomicInteger counter;
   private final AtomicBoolean online;
   private final long frequency;
   private final long repeat;

   public ChessPresenceNotificationPublisher(ChessUserSource userSource, ChessMessagePublisher publisher) {
      this(userSource, publisher, 2 * 60 * 1000);
   }
   
   public ChessPresenceNotificationPublisher(ChessUserSource userSource, ChessMessagePublisher publisher, long frequency) {
      this(userSource, publisher, frequency, 3);
   }
   
   public ChessPresenceNotificationPublisher(ChessUserSource userSource, ChessMessagePublisher publisher, long frequency, int repeat) {
      this.presence = new AtomicReference<String>();
      this.online = new AtomicBoolean(false);
      this.counter = new AtomicInteger();
      this.frequency = frequency;
      this.publisher = publisher;
      this.userSource = userSource;
      this.repeat = repeat;
   }   

   @Override
   public long executeTask() {
      try {
         notifyOfPresence();
      } catch(Exception e) {
         LOG.info("Unable to notify of presence", e);
      }
      return frequency;
   }
      
   
   public boolean isOnline() {
      return online.get();
   }
   
   public void statusOnline() {
      if(!online.getAndSet(true)) {
         notifyOfPresence();
      } else {
         String current = presence.get();
         ChessUser user = userSource.currentUser();
         String userId = user.getKey();
         
         if(!userId.equals(current)) {
            notifyOfPresence(); // presence changed!!
         } else {
            int count = counter.getAndIncrement();
            
            if(count < repeat) { // make sure we send out presence a few times
               notifyOfPresence();
            }
         }
      }
   }
   
   public void statusOffline() {
      counter.set(0);
      online.set(false);
   }

   private void notifyOfPresence() {
      try {
         if(isOnline()) {
            ChessUser user = userSource.currentUser();
            ChessUserType type = user.getType();
            String userId = user.getKey();
            boolean anonymous = type.isAnonymous();
            
            if(userId != null) {
               ChessPresenceEvent event = new ChessPresenceEvent(userId, anonymous);
               
               publisher.publishNotification(event);
               presence.set(userId);
               counter.getAndIncrement();
            }
         }
      } catch(Exception e) {
         LOG.info("Unable to notify of presence", e);
      }      
   }
}
