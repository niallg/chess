package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessRejectGameEvent extends ChessRequestEvent {

   private static final long serialVersionUID = 1L;
   
   private String gameId;

   public ChessRejectGameEvent(String gameId) {
      this.gameId = gameId;
   }

   public String getGameId() {
      return gameId;
   }
}
