package com.zuooh.chess.client.engine;

import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessOpponentStatus;

public interface ChessMoveObserver {
   void onStatus(ChessOpponentStatus status);
   void onLastMove(ChessMove move);
   void onMove(ChessMove move);
   void onResign();
   void onDraw();
}
