package com.zuooh.chess.client.event;

import com.zuooh.chess.database.user.ChessUser;

public class ChessUserEvent extends ChessEvent {
   
   private static final long serialVersionUID = 1L;

   private final ChessUser user;

   public ChessUserEvent(ChessUser user) {
      this.user = user;
   }
   
   public ChessUser getUser() {
      return user;
   }
}
