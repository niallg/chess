package com.zuooh.chess.client.event.response;

import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.user.ChessUser;

public class ChessSignInResultEvent extends ChessResponseEvent {
   
   private static final long serialVersionUID = 1L;

   private final ChessUserValidation validation;
   private final ChessUser user;

   public ChessSignInResultEvent(ChessUserValidation validation, ChessUser user) {
      this.validation = validation;      
      this.user = user;
   }
   
   public ChessUserValidation getStatus() {
      return validation;
   }

   public ChessUser getUser() {
      return user;
   }
}
