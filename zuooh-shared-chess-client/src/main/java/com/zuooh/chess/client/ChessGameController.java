package com.zuooh.chess.client;

import java.util.List;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.user.ChessUser;

public interface ChessGameController {
   ChessUser currentUser();
   ChessServerStatus serverStatus() throws Exception;
   List<ChessGame> availableGames() throws Exception;
   List<ChessGame> statusUpdate() throws Exception;
   ChessPlayer continueGame(String gameId) throws Exception;
   ChessPlayer createNewGame(String gameId, String opponentId, ChessSide userSide, String boardSet, long gameDuration) throws Exception;
   ChessPlayer acceptGame(String gameId) throws Exception;
   void rejectGame(String gameId) throws Exception;
   void resignGame(String gameId) throws Exception;
}
