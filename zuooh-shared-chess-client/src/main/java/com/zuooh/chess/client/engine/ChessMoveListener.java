package com.zuooh.chess.client.engine;

import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveResult;

public interface ChessMoveListener {
   void onMoveMade(ChessMove moveMade);
   void onMoveResult(ChessMoveResult moveResult);
}
