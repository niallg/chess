package com.zuooh.chess.client;

import java.util.Properties;

import com.zuooh.application.MobileApplication;
import com.zuooh.application.ServiceLauncher;
import com.zuooh.common.process.ProcessMonitor;

public class ChessClientServiceLauncher implements ChessServiceLauncher {

   private final MobileApplication application;
   private final Properties properties;  
   
   public ChessClientServiceLauncher(MobileApplication application, Properties properties) {
      this.application = application;  
      this.properties = properties;       
   }
   
   public void update(String user, String protocol) throws Exception {
      properties.setProperty("user", user);
      properties.setProperty("protocol", protocol);
   }
   
   public void launch() {
      ServiceLauncher launcher = application.getServiceLauncher();
      ProcessMonitor monitor = application.getProcessMonitor();
      String platform = monitor.getDevicePlatform();
      String version = monitor.getDeviceVersion();
      String device = monitor.getDeviceName();      

      properties.setProperty("version", version);
      properties.setProperty("platform", platform);
      properties.setProperty("device", device);
      launcher.launch(properties, 0);
   }
}
