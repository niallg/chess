package com.zuooh.chess.client.event.response;

import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.engine.ChessMove;

public class ChessLastMoveEvent extends ChessResponseEvent {
   
   private static final long serialVersionUID = 1L;

   private String userId;
   private String gameId;
   private ChessMove move;

   public ChessLastMoveEvent(ChessMove move, String userId, String gameId) {
      this.userId = userId;
      this.move = move;
      this.gameId = gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }

   public String getGameId() {
      return gameId;
   }

   public String getUserId() {
      return userId;
   }

   public void setUserId(String userId) {
      this.userId = userId;
   }

   public ChessMove getMove() {
      return move;
   }

   public void setMove(ChessMove move) {
      this.move = move;
   }
}
