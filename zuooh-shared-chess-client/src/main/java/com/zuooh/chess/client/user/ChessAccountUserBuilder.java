package com.zuooh.chess.client.user;

import static com.zuooh.chess.database.user.ChessUserType.ANONYMOUS;
import static com.zuooh.chess.database.user.ChessUserType.NORMAL;

import com.zuooh.application.MobileApplication;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;

public class ChessAccountUserBuilder implements ChessUserBuilder {
   
   public static final String ANONYMOUS_NAME = "Anonymous";

   private final ChessUserMailResolver resolver;
   private final ChessUserKeyGenerator generator;

   public ChessAccountUserBuilder(MobileApplication application) {
      this.resolver = new ChessUserMailResolver(application);
      this.generator = new ChessUserKeyGenerator();
   }
   
   @Override
   public ChessUser createAnonymousUser() {
      String userMail = resolver.resolveBestMail();
      String userId = generator.generateKey(ANONYMOUS, userMail);
      ChessUserData data = createUserData();
      
      return new ChessUser(data, ANONYMOUS, userId, ANONYMOUS_NAME, userMail, null);
   }
   
   @Override
   public ChessUser createNormalUser(String userName, String password) {
      String userMail = resolver.resolveBestMail();
      String userId = generator.generateKey(NORMAL, userMail, userName);
      ChessUserData data = createUserData();

      if(userName == null) {
         throw new IllegalArgumentException("User name must not be null");
      }
      if(userName.equals("")) {
         throw new IllegalArgumentException("User name must not be blank");
      }
      if(password == null) {
         throw new IllegalArgumentException("User password must not be null");
      }
      if(password.equals("")) {
         throw new IllegalArgumentException("User password cannot be blank");
      }
      return new ChessUser(data, NORMAL, userId, userName, userMail, password);
   }   
   
   private ChessUserData createUserData() {
      return new ChessUserData();
   }
}
