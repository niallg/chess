package com.zuooh.chess.client.event.operation;

import com.zuooh.message.client.ResponseStatus;

public interface ChessRequestCommand<A, B> {
   A getRequest();
   B getResponse();
   ResponseStatus getStatus(); 
   // ChessRequestClient getClient();
   void update(A request);
   void execute();
   void clear();
}
