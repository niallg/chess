package com.zuooh.chess.client.event;

public abstract class ChessRequestEvent extends ChessEvent {
   
   private static final long serialVersionUID = 1L;
   
   protected ChessRequestEvent() {
      super();
   }
}
