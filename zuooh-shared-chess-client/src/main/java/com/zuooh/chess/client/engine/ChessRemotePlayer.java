package com.zuooh.chess.client.engine;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.ChessPlayer;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessRemotePlayer implements ChessPlayer {

   private final ChessRemoteGameMoveEngine onlineEngine;
   private final ChessRemoteGame onlineGame;

   public ChessRemotePlayer(ChessRemoteGameMoveEngine onlineEngine, ChessRemoteGame onlineGame) {
      this.onlineEngine = onlineEngine;
      this.onlineGame = onlineGame;
   }

   public ChessRemoteGame getGame() {
      return onlineGame;
   }
   
   @Override
   public String getAssociatedGame() {
      return onlineGame.getAssociatedGame();
   }

   @Override
   public ChessSide getPlayerSide() {
      return onlineEngine.getSide();
   }

   @Override
   public ChessOpponentStatus getOpponentStatus() {
      return onlineGame.getStatus();
   }

   @Override
   public ChessRemoteGameMoveEngine getMoveEngine() {
      return onlineEngine;
   }

   @Override
   public ChessUser getUser() {
      return onlineGame.getUserProfile();
   }

   @Override
   public boolean isOnline() {
      return true;
   }
}
