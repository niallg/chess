package com.zuooh.chess.client;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChessTraceServiceLauncher implements ChessServiceLauncher {
   
   public static final Logger LOG = LoggerFactory.getLogger(ChessTraceServiceLauncher.class);
   
   @Override
   public void update(String user, String protocol) {
      LOG.info("Service launch user changed " + user);
   }

   @Override
   public void launch() {
      LOG.info("Service launch request received");
   }
}
