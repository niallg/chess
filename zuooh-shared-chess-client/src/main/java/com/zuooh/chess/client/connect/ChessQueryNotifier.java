
package com.zuooh.chess.client.connect;

import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChessQueryNotifier {
   
   public static final Logger LOG = LoggerFactory.getLogger(ChessQueryNotifier.class);
   
   private final Set<ChessQueryListener> listeners; 
   private final ChessQueryDistributor distributor;
   
   public ChessQueryNotifier(Set<ChessQueryListener> listeners, ChessQueryDistributor distributor) {
      this.distributor = distributor;
      this.listeners = listeners;   
   }   
   
   public void register() {
      for(ChessQueryListener listener : listeners) {
         try {
            distributor.register(listener);
         } catch(Exception e) {
            LOG.info("Error registering listener", e);
         }
      }
   }
}
