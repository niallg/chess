package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessUpdateRankEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;
   
   private final long requestTime;
   
   public ChessUpdateRankEvent(long requestTime) {
      this.requestTime = requestTime;
   }
   
   public long getRequestTime() {
      return requestTime;
   }

}
