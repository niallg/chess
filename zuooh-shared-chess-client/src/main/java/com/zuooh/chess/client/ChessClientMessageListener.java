package com.zuooh.chess.client;

import java.util.List;

import com.zuooh.chess.client.event.ChessDrawEvent;
import com.zuooh.chess.client.event.ChessMoveEvent;
import com.zuooh.chess.client.event.ChessNotificationEvent;
import com.zuooh.chess.client.event.ChessResignEvent;
import com.zuooh.chess.client.event.ChessUserEvent;
import com.zuooh.chess.client.event.chat.ChessChatController;
import com.zuooh.chess.client.event.chat.ChessChatEvent;
import com.zuooh.chess.client.event.chat.ChessChatListEvent;
import com.zuooh.chess.client.event.presence.ChessPresenceNotificationPublisher;
import com.zuooh.chess.client.event.request.ChessRejectGameEvent;
import com.zuooh.chess.client.event.response.ChessLastMoveEvent;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.chess.client.event.response.ChessNewGameEvent;
import com.zuooh.chess.client.user.ChessUserDirectorySource;
import com.zuooh.chess.database.chat.ChessChat;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.message.Message;
import com.zuooh.message.MessageListener;
import com.zuooh.message.client.NotificationMessage;
import com.zuooh.message.client.ResponseMessage;

public class ChessClientMessageListener implements MessageListener {

   private final ChessPresenceNotificationPublisher presencePublisher;
   private final ChessClientController mainController;
   private final ChessChatController chatController;
   private final ChessUserDirectorySource userController;

   public ChessClientMessageListener(ChessClientController mainController, ChessUserDirectorySource userController, ChessPresenceNotificationPublisher presencePublisher, ChessChatController chatController) {
      this.presencePublisher = presencePublisher;
      this.userController = userController;
      this.mainController = mainController;
      this.chatController = chatController;
   }

   @Override
   public void onMessage(Message message) {
      Object value = message.getValue();
      
      if(value instanceof ResponseMessage) {
         onResponse((ResponseMessage)value);
      }
      if(value instanceof NotificationMessage) {
         onNotification((NotificationMessage)value);
      }   
   }   
   
   private void onNotification(NotificationMessage message) {
      Object value = message.getMessage();      
      
      if(value instanceof ChessChatEvent) {
         updateChat((ChessChatEvent)value);
      }
      if(value instanceof ChessChatListEvent) {
         updateChat((ChessChatListEvent)value);
      } 
      if(value instanceof ChessUserEvent) {
         updateProfile((ChessUserEvent)value);
      }
      if(value instanceof ChessNotificationEvent) {
         updateNotification((ChessNotificationEvent)value);
      }        
      if(value instanceof ChessListOfGamesEvent) {
         listOfGames((ChessListOfGamesEvent)value);
      }    
      if(value instanceof ChessMoveEvent) {
         moveMade((ChessMoveEvent)value);
      }
      if(value instanceof ChessResignEvent) {
         playerResign((ChessResignEvent)value);
      }
      if(value instanceof ChessDrawEvent) {
         playerDraw((ChessDrawEvent)value);
      }        
   }   
   
   private void onResponse(ResponseMessage message) {
      Object value = message.getResult();         
     
      if(value instanceof ChessNewGameEvent) {
         newGame((ChessNewGameEvent)value);
      }             
      if(value instanceof ChessMoveEvent) {
         moveMade((ChessMoveEvent)value);
      }       
      if(value instanceof ChessLastMoveEvent) {
         lastMoveMade((ChessLastMoveEvent)value);
      }       
      if(value instanceof ChessResignEvent) {
         playerResign((ChessResignEvent)value);
      }      
      if(value instanceof ChessRejectGameEvent) {
         rejectGame((ChessRejectGameEvent)value);
      }
   }

   @Override
   public void onException(Exception cause) {
      statusOffline();
   }

   @Override
   public void onHeartbeat() {
      statusOnline();
   }

   @Override
   public void onReset() {
      statusOffline();
   }

   public void updateChat(ChessChatEvent event) {
      ChessChat chat = event.getChat();

      if (chat != null) {
         chatController.updateChat(chat);
      }
   }

   public void updateChat(ChessChatListEvent event) {
      List<ChessChat> chats = event.getChats();

      for (ChessChat chat : chats) {
         if(chat != null) {
            chatController.updateChat(chat);
         }
      }
   }

   public void newGame(ChessNewGameEvent event) {
      ChessGame game = event.getGame();
      ChessGameCriteria challenge = game.getCriteria();
      ChessUser profile = challenge.getUser();

      if (profile != null) {
         userController.saveUser(profile);
      }
      mainController.updateGame(game);
   }

   public void listOfGames(ChessListOfGamesEvent event) {
      List<ChessGame> gameList = event.getGames();

      for (ChessGame game : gameList) {
         ChessGameCriteria challenge = game.getCriteria();
         ChessUser profile = challenge.getUser();

         if (profile != null) {
            userController.saveUser(profile);
         }
         mainController.updateGame(game);
      }
   }

   public void moveMade(ChessMoveEvent event) {
      String gameId = event.getGameId();
      ChessMove chessMove = event.getMove();

      mainController.moveMade(gameId, chessMove);
   }

   public void lastMoveMade(ChessLastMoveEvent event) {
      String gameId = event.getGameId();
      ChessMove chessMove = event.getMove();

      mainController.lastMoveMade(gameId, chessMove);
   }
   
   public void rejectGame(ChessRejectGameEvent event) {
      String gameId = event.getGameId();
      
      if(gameId != null) {
         mainController.deleteGame(gameId);
      }
   }   

   public void playerResign(ChessResignEvent event) {
      String gameId = event.getGameId();
      String resignUserId = event.getUserId();
      ChessUser profile = userController.currentUser();
      
      if(profile != null) {
         String userId = profile.getKey();
         
         if(!userId.equals(resignUserId)) {
            mainController.resignGame(gameId);
         }
      }
   }
   
   public void playerDraw(ChessDrawEvent event) {
      String gameId = event.getGameId();
      String offerUserId = event.getPlayerId();
      ChessUser profile = userController.currentUser();
      
      if(profile != null) {
         String userId = profile.getKey();
         
         if(!userId.equals(offerUserId)) {
            mainController.drawGame(gameId);
         }
      }
   }
   
   public void updateNotification(ChessNotificationEvent event) {
      String userId = event.getUserId();
      
      if(userId != null) {
         mainController.checkNotifications();
      }
   }   
   
   public void updateProfile(ChessUserEvent event) {
      ChessUser profile = event.getUser();
      
      if(profile != null) {
         userController.saveUser(profile);
      }
   }

   public void statusOnline() {
      presencePublisher.statusOnline();
      mainController.statusOnline();
   }

   public void statusOffline() {
      presencePublisher.statusOffline();
      mainController.statusOffline();
   }
}
