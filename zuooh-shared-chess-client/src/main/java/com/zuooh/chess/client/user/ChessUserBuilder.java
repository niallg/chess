package com.zuooh.chess.client.user;

import com.zuooh.chess.database.user.ChessUser;

public interface ChessUserBuilder {
   ChessUser createAnonymousUser();
   ChessUser createNormalUser(String userName, String password);     
}
