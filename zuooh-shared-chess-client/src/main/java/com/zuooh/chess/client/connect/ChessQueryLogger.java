package com.zuooh.chess.client.connect;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.user.ChessUser;

public class ChessQueryLogger implements ChessQueryListener {

   private static final Logger LOG = LoggerFactory.getLogger(ChessQueryLogger.class);
   
   @Override
   public void update(ChessQuery query) {
      ChessUser user = query.getUser();
      String userId = user.getKey();
      String userName = user.getName();
      String protocol = query.getProtocol();      
      
      LOG.info("Query for userId=["+userId+"] userName=["+userName+"] protocol=["+protocol+"]");
   }

}
