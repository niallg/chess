package com.zuooh.chess.client.event.operation;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.request.ChessUserSearchEvent;
import com.zuooh.chess.client.event.response.ChessUserSearchResultEvent;
import com.zuooh.message.client.ResponseStatus;

public class ChessUserSearchCommand {
   
   private final ChessRequestCommand<ChessUserSearchEvent, ChessUserSearchResultEvent> operation;
   
   public ChessUserSearchCommand(ChessMessagePublisher publisher) {
      this.operation = new ChessRequestCommandClient<ChessUserSearchEvent, ChessUserSearchResultEvent>(publisher, ChessUserSearchEvent.class, ChessUserSearchResultEvent.class);
   }
   
   public ChessUserSearchEvent getSearchRequest() {
      return operation.getRequest();
   }
   
   public ChessUserSearchResultEvent getSearchResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute(String term) {
      ChessUserSearchEvent event = new ChessUserSearchEvent(term);
      
      operation.update(event);
      operation.execute();      
   }
   
   public void reset() {
      operation.clear();
   }
}
