package com.zuooh.chess.client;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessOpponentStatus;

public interface ChessPlayer {
   String getAssociatedGame();
   ChessSide getPlayerSide();
   ChessOpponentStatus getOpponentStatus();
   ChessMoveEngine getMoveEngine();
   ChessUser getUser();
   boolean isOnline();

}
