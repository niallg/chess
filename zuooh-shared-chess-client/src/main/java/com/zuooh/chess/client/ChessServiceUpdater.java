package com.zuooh.chess.client;

import com.zuooh.chess.client.connect.ChessQuery;
import com.zuooh.chess.client.connect.ChessQueryListener;
import com.zuooh.chess.database.user.ChessUser;

public class ChessServiceUpdater implements ChessQueryListener {

   private final ChessServiceLauncher refresher;
   
   public ChessServiceUpdater(ChessServiceLauncher refresher) {
      this.refresher = refresher;         
   }

   @Override
   public void update(ChessQuery query) throws Exception {
      ChessUser user = query.getUser();
      String protocol = query.getProtocol();
      String key = user.getKey();
      
      refresher.update(key, protocol);
      refresher.launch();
   }
}
