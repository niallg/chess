package com.zuooh.chess.client.engine;

import java.util.concurrent.atomic.AtomicBoolean;

import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.client.event.ChessGameOverEvent;
import com.zuooh.chess.client.event.ChessMoveEvent;
import com.zuooh.chess.client.event.request.ChessLastMoveQueryEvent;
import com.zuooh.chess.client.event.request.ChessOpponentMoveQueryEvent;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessMoveResult;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessMakeMoveController {

   private final ChessRemoteGameController remoteController;
   private final ChessMoveListener moveListener;
   private final ChessMoveEngine engine;  
   private final AtomicBoolean gameOver;
   private final ChessMove moveToMake; 
   private final String playerName;
   private final String gameId;

   public ChessMakeMoveController(ChessRemoteGameController remoteController, ChessMoveListener moveListener, ChessMoveEngine engine, ChessMove moveToMake, AtomicBoolean gameOver, String playerName, String gameId) {
      this.remoteController = remoteController;
      this.moveListener = moveListener;
      this.moveToMake = moveToMake;
      this.playerName = playerName;
      this.engine = engine;
      this.gameOver = gameOver;
      this.gameId = gameId;
   }

   public void makeMove() {
      String opponentName = engine.getOpponent();
      
      if(moveToMake != null) {
         ChessMoveEvent moveEvent = new ChessMoveEvent(moveToMake, playerName, opponentName, gameId);
   
         try {
            remoteController.executeRequest(moveEvent);
         } catch(Exception e) {
            ChessMoveResult moveResult = new ChessMoveResult(ChessOpponentStatus.ERROR);
   
            if(!gameOver.get()) {
               moveListener.onMoveResult(moveResult);
            }
         }
      }
   }

   public void requestLastMove() {
      ChessLastMoveQueryEvent lastMoveEvent = new ChessLastMoveQueryEvent(playerName, gameId);

      try {
         remoteController.executeRequest(lastMoveEvent);
      } catch(Exception e) {
         ChessMoveResult moveResult = new ChessMoveResult(ChessOpponentStatus.ERROR);

         if(!gameOver.get()) {
            moveListener.onMoveResult(moveResult);
         }
      }
   }

   public void requestOpponentMove() {
      ChessOpponentMoveQueryEvent moveEvent = new ChessOpponentMoveQueryEvent(playerName, gameId);

      try {
         remoteController.executeRequest(moveEvent);
      } catch(Exception e) {
         ChessMoveResult moveResult = new ChessMoveResult(ChessOpponentStatus.ERROR);

         if(!gameOver.get()) {
            moveListener.onMoveResult(moveResult);
         }
      }
   }  

   public void gameOver(ChessBoardResult result) {
      ChessGameOverEvent gameOverEvent = new ChessGameOverEvent(result, playerName, gameId);

      try {
         remoteController.executeRequest(gameOverEvent);
      } catch(Exception e) {
         ChessMoveResult moveResult = new ChessMoveResult(ChessOpponentStatus.ERROR);

         if(!gameOver.get()) {
            moveListener.onMoveResult(moveResult);
         }
      }
      remoteController.removeActionObserver(gameId);
   }

   public void opponentAcceptDraw() {
      ChessMoveResult moveResult = new ChessMoveResult(ChessOpponentStatus.DRAW);
      moveListener.onMoveResult(moveResult);      
   }
   

   public void opponentResign() {
      ChessMoveResult moveResult = new ChessMoveResult(ChessOpponentStatus.RESIGN);
      moveListener.onMoveResult(moveResult);      
   } 
}
