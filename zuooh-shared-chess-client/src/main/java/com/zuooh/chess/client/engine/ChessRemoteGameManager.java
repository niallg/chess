package com.zuooh.chess.client.engine;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.board.ChessDrawPhase;
import com.zuooh.chess.client.ChessClientController;
import com.zuooh.chess.client.ChessGameController;
import com.zuooh.chess.client.ChessServerStatus;
import com.zuooh.chess.client.ChessServiceLauncher;
import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.ChessEvent;
import com.zuooh.chess.client.event.ChessGameOverEvent;
import com.zuooh.chess.client.event.request.ChessAcceptGameEvent;
import com.zuooh.chess.client.event.request.ChessCreateGameEvent;
import com.zuooh.chess.client.event.request.ChessRejectGameEvent;
import com.zuooh.chess.client.user.ChessUserSource;
import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.database.game.ChessGameCriteria;
import com.zuooh.chess.database.game.ChessGameDatabase;
import com.zuooh.chess.database.game.ChessGameStatus;
import com.zuooh.chess.database.result.ChessResultDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessRemoteGameManager implements ChessGameController, ChessRemoteGameController, ChessClientController {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessRemoteGameManager.class);

   private final Map<String, ChessRemoteGameMoveEngine> gameEngines;
   private final Map<String, ChessMoveObserver> actionNotifiers;
   private final Map<String, ChessGameObserver> gameNotifiers;   
   private final ChessMessagePublisher messagePublisher;
   private final ChessServiceLauncher serviceLauncher;
   private final ChessDatabase chessDatabase;
   private final ChessUserSource userSource;
   private final String serverName;

   public ChessRemoteGameManager(ChessUserSource userSource, ChessDatabase chessDatabase, ChessMessagePublisher messagePublisher, ChessServiceLauncher serviceLauncher, String serverName) {
      this.gameEngines = new ConcurrentHashMap<String, ChessRemoteGameMoveEngine>();
      this.gameNotifiers = new ConcurrentHashMap<String, ChessGameObserver>();
      this.actionNotifiers = new ConcurrentHashMap<String, ChessMoveObserver>();    
      this.messagePublisher = messagePublisher;
      this.serviceLauncher = serviceLauncher;
      this.chessDatabase = chessDatabase;
      this.userSource = userSource;
      this.serverName = serverName;
   }
   
   @Override
   public synchronized void restart() {
      ChessGameDatabase gameDatabase = chessDatabase.getGameDatabase();
      
      try {
         actionNotifiers.clear();
         gameNotifiers.clear();
         gameEngines.clear();
         gameDatabase.deleteAll();
      } catch(Exception e) {
         LOG.info("Could not execute delete statement", e);
      }
   }

   @Override
   public synchronized void setGameObserver(String gameId, ChessGameObserver gameObserver) {
      gameNotifiers.put(gameId, gameObserver);
   }

   @Override
   public synchronized void removeGameObserver(String gameId) {
      gameNotifiers.remove(gameId);
   }

   @Override
   public synchronized void setActionObserver(String gameId, ChessMoveObserver actionObserver) {
      actionNotifiers.put(gameId, actionObserver);
   }

   @Override
   public synchronized void removeActionObserver(String gameId) {
      actionNotifiers.remove(gameId);
   }
   
   @Override
   public synchronized ChessUser currentUser() {
      return userSource.currentUser();
   }

   @Override
   public synchronized ChessServerStatus serverStatus() {
      return ChessServerStatus.AVAILABLE;
   }

   @Override
   public synchronized void executeRequest(ChessEvent chessEvent) {
      ChessUser user = userSource.currentUser();
      String userId = user.getKey();
      
      if(userId != null) {         
         try {       
            messagePublisher.executeRequest(chessEvent);
         } catch(Exception e) {
            throw new IllegalStateException("Could not publish event", e);
         }
      }
   }

   @Override
   public synchronized void statusOnline() {
      Set<String> gameIds = actionNotifiers.keySet();

      for(String gameId : gameIds) {
         ChessMoveObserver observer = actionNotifiers.get(gameId);
         observer.onStatus(ChessOpponentStatus.ONLINE);
      }
   }

   @Override
   public synchronized void statusOffline() {
      Set<String> gameIds = actionNotifiers.keySet();

      for(String gameId : gameIds) {
         ChessMoveObserver observer = actionNotifiers.get(gameId);
         observer.onStatus(ChessOpponentStatus.OFFLINE);
      }
   }   

   @Override
   public void checkNotifications() {
      try {
         serviceLauncher.launch();
      } catch(Exception e) {
         LOG.info("Could not launch service to check notifications", e);
      }
   }   

   @Override
   public synchronized void updateGame(ChessGame chessGame) {
      String gameId = chessGame.getGameId();
      ChessGameObserver listener = gameNotifiers.get(gameId);
      ChessGameDatabase gameDatabase = chessDatabase.getGameDatabase();
      ChessBoardGameDatabase boardGameDatabase = chessDatabase.getBoardDatabase();
      ChessResultDatabase resultDatabase = chessDatabase.getResultDatabase();
      
      if(listener != null) {
         listener.onGameUpdate(chessGame);
      }
      try {
         ChessBoardGame boardGame = boardGameDatabase.loadBoardGame(gameId);
         
         if(boardGame != null) {
            ChessBoardStatus boardStatus = boardGame.getBoardStatus();
            ChessGameStatus gameStatus = chessGame.getStatus();
            
            if(gameStatus.isResigned() && !boardStatus.isGameOver()) {              
               boardGame.setBoardStatus(ChessBoardStatus.RESIGN);
               boardGame.setBoardResult(ChessBoardResult.WIN);
               boardGameDatabase.saveBoardGame(boardGame); // ensure to track opponent resignation
               resultDatabase.saveResult(boardGame);               
            }
            if(gameStatus.isRejected()) {
               boardGame.setBoardStatus(ChessBoardStatus.REJECT);
               boardGame.setBoardResult(ChessBoardResult.REJECT);
               boardGameDatabase.saveBoardGame(boardGame); // don't delete yet, it might cause issues
               resultDatabase.saveResult(boardGame);               
            }
         }
      } catch(Exception e) {
         LOG.info("Could not change game status of " + gameId, e);
      }
      try {
         gameDatabase.saveGame(chessGame);
      } catch(Exception e) {
         LOG.info("Problem saving game " + gameId, e);
      }
   }   

   @Override
   public synchronized void moveMade(String gameId, ChessMove chessMove) {
      ChessMoveObserver gameObserver = actionNotifiers.get(gameId);

      if(gameObserver != null) {
         gameObserver.onMove(chessMove);
      }
   }

   @Override
   public synchronized void lastMoveMade(String gameId, ChessMove chessMove) {
      ChessMoveObserver gameObserver = actionNotifiers.get(gameId);

      if(gameObserver != null) {
         gameObserver.onLastMove(chessMove);
      }
   }
   
   @Override
   public synchronized void deleteGame(String gameId) {
      try {
         ChessBoardGameDatabase boardDatabase = chessDatabase.getBoardDatabase();
         ChessGameDatabase gameDatabase = chessDatabase.getGameDatabase();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
         ChessGame game = gameDatabase.loadGame(gameId);
         
         if(boardGame != null) {
            boardDatabase.deleteBoardGame(boardGame);
         }
         if(game != null) {
            gameDatabase.deleteGame(game);
         }
         actionNotifiers.remove(gameId); // to we make a call here?         
      } catch(Exception e) {
         LOG.info("Could not delete game " + gameId, e);
      }
   }   

   @Override
   public synchronized void resignGame(String gameId) {
      try {
         ChessMoveObserver gameObserver = actionNotifiers.get(gameId);
         ChessBoardGameDatabase boardDatabase = chessDatabase.getBoardDatabase();
         ChessResultDatabase resultDatabase = chessDatabase.getResultDatabase();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
         
         if(gameObserver != null) {
            gameObserver.onResign();
         } else {
            if(boardGame != null) {
               boardGame.setBoardStatus(ChessBoardStatus.RESIGN);
               boardGame.setBoardResult(ChessBoardResult.WIN);
               boardDatabase.saveBoardGame(boardGame);
               resultDatabase.saveResult(boardGame);                
            }
         }
      } catch(Exception e) {
         LOG.info("Could not resign game " + gameId, e);
      }
   }
   
   @Override
   public synchronized void drawGame(String gameId) {
      try {
         ChessMoveObserver gameObserver = actionNotifiers.get(gameId);         
         ChessBoardGameDatabase boardDatabase = chessDatabase.getBoardDatabase();         
         ChessResultDatabase resultDatabase = chessDatabase.getResultDatabase();
         ChessBoardGame boardGame = boardDatabase.loadBoardGame(gameId);
         
         if(boardGame != null) {
            ChessDrawPhase drawPhase = boardGame.getDrawPhase();
            ChessUser user = currentUser();
            String userId = user.getKey();
            
            if(drawPhase.isDrawRequestSent()) { // this must be a confirmation!
               ChessGameOverEvent gameOver = new ChessGameOverEvent(ChessBoardResult.DRAW, userId, gameId);            
               
               if(gameObserver != null) {
                  gameObserver.onDraw();
               } else {
                  boardGame.setBoardStatus(ChessBoardStatus.DRAW);
                  boardGame.setBoardResult(ChessBoardResult.DRAW);
                  boardDatabase.saveBoardGame(boardGame);
                  resultDatabase.saveResult(boardGame);                  
               }
               executeRequest(gameOver);
            } else {
               boardGame.setDrawPhase(ChessDrawPhase.REQUEST_RECEIVED);
               boardDatabase.saveBoardGame(boardGame);
            } 
         }
      } catch(Exception e) {
         LOG.info("Could not offer draw " + gameId, e);
      }
   }   

   @Override
   public synchronized List<ChessGame> statusUpdate() {
      ChessGameDatabase gameDatabase = chessDatabase.getGameDatabase();
      
      try {
         return gameDatabase.loadInProgressGames();
      } catch(Exception e) {
         LOG.info("Problem loading in progress games", e);
      }
      return Collections.emptyList();
   }

   @Override
   public synchronized List<ChessGame> availableGames() {
      ChessGameDatabase gameDatabase = chessDatabase.getGameDatabase();
      
      try {
         return gameDatabase.loadAvailableGames();
      } catch(Exception e) {
         LOG.info("Problem loading in progress games", e);
      }
      return Collections.emptyList();
   }

   @Override
   public synchronized ChessRemotePlayer continueGame(String gameId) {    
      ChessGame existingGame = loadGame(gameId);

      if(existingGame != null) {
         ChessGameCriteria challenge = existingGame.getCriteria();      
         ChessRemotePlayer remotePlayer = createRemotePlayer(challenge, gameId);
         ChessRemoteGameMoveEngine moveEngine = remotePlayer.getMoveEngine();
         ChessMove chessMove = existingGame.getLastMove();
         ChessSide challengeSide = challenge.getUserSide();
         ChessUser opponentProfile = remotePlayer.getUser();
         String opponentId = opponentProfile.getKey();
   
         moveEngine.setOpponentStatus(ChessOpponentStatus.NONE);
         moveEngine.setSide(challengeSide);
         moveEngine.setOpponent(opponentId);
         moveEngine.setLastMove(chessMove);
   
         if(chessMove != null && chessMove.side != null) { // BUG here the move is created but is empty!!
            moveEngine.setLastMove(chessMove.side, chessMove);
         }
         return remotePlayer;
      }
      return null;
   }

   @Override
   public synchronized ChessRemotePlayer createNewGame(String gameId, String opponentId, ChessSide userSide, String boardSet, long gameDuration) {
      long creationTime = System.currentTimeMillis();
      ChessUser user = currentUser();
      ChessSide challengerSide = userSide.oppositeSide();
      ChessGameCriteria createChallenge = new ChessGameCriteria(user, userSide, boardSet, gameDuration, creationTime);
      ChessCreateGameEvent createEvent = new ChessCreateGameEvent(createChallenge, opponentId, gameId);
      ChessGameCriteria unacceptedChallenge = new ChessGameCriteria(null, challengerSide, boardSet, gameDuration, creationTime);
      
      executeRequest(createEvent);
      
      ChessRemotePlayer remotePlayer = createRemotePlayer(unacceptedChallenge, gameId);
      ChessRemoteGameMoveEngine moveEngine = remotePlayer.getMoveEngine();
      
      moveEngine.setOpponentStatus(ChessOpponentStatus.NONE);
      moveEngine.setSide(challengerSide);
      
      return remotePlayer;      
   }

   @Override
   public synchronized ChessRemotePlayer acceptGame(String gameId) {
      ChessGame acceptedGame = loadGame(gameId);

      if(acceptedGame == null) {
         throw new IllegalStateException("Could not accept game " + gameId + " as it does not exist");
      }
      ChessUser user = userSource.currentUser();
      ChessUserData playerData = user.getData();
      String userName = user.getKey();
      int userSkill = playerData.getSkill();
      ChessAcceptGameEvent acceptEvent = new ChessAcceptGameEvent(userName, userSkill, gameId);
      ChessGameCriteria challenge = acceptedGame.getCriteria();
      ChessSide challengerSide = challenge.getUserSide();
      
      executeRequest(acceptEvent);

      ChessRemotePlayer remotePlayer = createRemotePlayer(challenge, gameId);
      ChessRemoteGameMoveEngine moveEngine = remotePlayer.getMoveEngine();
      
      moveEngine.setOpponentStatus(ChessOpponentStatus.NONE);
      moveEngine.setSide(challengerSide);
      
      return remotePlayer;       
   }
   
   @Override
   public synchronized void rejectGame(String gameId) {
      ChessGame game = loadGame(gameId);

      if(game == null) {
         throw new IllegalStateException("Could not reject game " + gameId + " as it does not exist");
      }
      ChessRejectGameEvent acceptEvent = new ChessRejectGameEvent(gameId);
      
      executeRequest(acceptEvent);   
   }   
   
   private ChessRemotePlayer createRemotePlayer(ChessGameCriteria challenge, String gameId) {
      ChessRemoteGameMoveEngine gameEngine = createRemoteEngine(gameId);
      ChessRemoteGame opponentPlayer = new ChessRemoteGame(gameId, gameEngine, challenge);

      setGameObserver(gameId, opponentPlayer);
      
      ChessRemotePlayer onlinePlayer = new ChessRemotePlayer(gameEngine, opponentPlayer);

      return onlinePlayer;
   }
   
   private ChessRemoteGameMoveEngine createRemoteEngine(String gameId) {
      ChessUser user = currentUser();      
      String userId = user.getKey();
      ChessRemoteGameMoveEngine gameEngine = gameEngines.get(gameId);
      
      if(gameEngine == null) {
         gameEngine = new ChessRemoteGameMoveEngine(this, userId, gameId);         
         gameEngines.put(gameId, gameEngine);
      }
      return gameEngine;
   }   
   
   private synchronized ChessGame loadGame(String gameId) {
      ChessGameDatabase gameDatabase = chessDatabase.getGameDatabase();

      try {
         return gameDatabase.loadGame(gameId);
      } catch(Exception e) {
         LOG.info("Problem loading game " + gameId, e);
      }
      return null;
   }
}
