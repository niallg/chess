
package com.zuooh.chess.client.connect;

import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

public class ChessQueryDistributor implements ChessQueryListener {
   
   private final AtomicReference<ChessQuery> reference;  
   private final Set<ChessQueryListener> listeners; 

   public ChessQueryDistributor() {
      this.listeners = new CopyOnWriteArraySet<ChessQueryListener>();  
      this.reference = new AtomicReference<ChessQuery>();
   }   
   
   public synchronized void register(ChessQueryListener listener) throws Exception {
      ChessQuery query = reference.get();
      
      if(query != null) {
         listener.update(query);
      }
      listeners.add(listener);
   }
   
   public synchronized void remove(ChessQueryListener listener) throws Exception {
      listeners.remove(listener);
   }   

   @Override
   public synchronized void update(ChessQuery query) throws Exception {
      for(ChessQueryListener queryListener : listeners) {
         queryListener.update(query);
      }
      reference.set(query);
   }
}
