package com.zuooh.chess.client.user;

public enum ChessUserValidation {
   USER_NAME_NOT_REGISTERED("User name is available"),
   USER_NAME_REGISTERED("User name has been taken"),    
   PASSWORD_IS_INCORRECT("Password did not match"),
   PASSWORD_CHANGED("Password has been changed"),
   PASSWORD_VERIFIED("Password has been verified"),      
   USER_REGISTERED("User has been registered");
   
   public final String description;
   
   private ChessUserValidation(String description) {
      this.description = description;
   }
   
   public String getDescription() {
      return description;
   }
}
