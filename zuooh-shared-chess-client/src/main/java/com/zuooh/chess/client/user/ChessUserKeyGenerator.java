package com.zuooh.chess.client.user;

import java.security.MessageDigest;
import java.security.SecureRandom;
import java.util.Calendar;

import com.zuooh.chess.database.user.ChessUserType;

public class ChessUserKeyGenerator {
   
   private final SecureRandom random;
   
   public ChessUserKeyGenerator() {
      this.random = new SecureRandom();
   }
   
   public String generateKey(ChessUserType type, String userMail) {
      return generateKey(type, userMail, null);
   }

   public String generateKey(ChessUserType type, String userMail, String userName) {
      StringBuilder builder = new StringBuilder();
      
      builder.append(type.code);
      builder.append("-");
      
      if(userName != null) {
         String userKey = String.valueOf(userName);
         String userToken = userKey.trim();
         int length = userToken.length();
         
         if(length > 0) {
            char prev = userToken.charAt(0);
         
            for(int i = 0; i < length; i++) {
               char next = userToken.charAt(i);
            
               if(Character.isWhitespace(next)) {
                  if(!Character.isWhitespace(prev)) {            
                     builder.append(next);
                  }
               } else {
                  builder.append(next);
               }
               prev = next;
            }
            builder.append("-");
         }
      }
      long currentTime = System.currentTimeMillis();
      long randomNumber = random.nextLong();

      builder.append(userMail);
      builder.append(currentTime);
      builder.append(randomNumber);
      
      String uniqueKey = builder.toString();
      String uniqueToken = uniqueKey.toLowerCase();
      
      return createDigestKey(type, uniqueToken, currentTime);
   }
   
   private String createDigestKey(ChessUserType type, String key, long time) {      
      try {
         MessageDigest digest = MessageDigest.getInstance("MD5");         
         Calendar calendar = Calendar.getInstance();
         byte[] octets = key.getBytes("UTF-8");
         
         calendar.setTimeInMillis(time);
         digest.update(octets);
         
         byte[] result = digest.digest();
         StringBuilder builder = new StringBuilder();
         
         for(int i = 0; i < result.length;i++) {
            int value = 0xff & result[i];
            
            if(value <= 0xf) {
               builder.append("0");
            }
            String token = Integer.toHexString(value);            
            builder.append(token);
         }
         char suffix = Character.toLowerCase(type.code);
         int month = calendar.get(Calendar.MONTH);
         int day = calendar.get(Calendar.DAY_OF_MONTH);
         
         builder.append(suffix);
         builder.append("d");
         builder.append(day);
         builder.append("m");
         builder.append(month);
         
         return builder.toString();        
      }catch(Exception e) {
         return key;
      }
   }

}
