package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;
import com.zuooh.chess.database.user.ChessUser;

public class ChessRegisterUserEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;

   private ChessUser user;

   public ChessRegisterUserEvent(ChessUser user) {
      this.user = user;
   }

   public ChessUser getUser() {
      return user;
   }

   public void setUser(ChessUser user) {
      this.user = user;
   }
}
