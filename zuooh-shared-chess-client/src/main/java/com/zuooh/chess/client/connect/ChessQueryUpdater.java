package com.zuooh.chess.client.connect;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.message.client.MessageSource;
import com.zuooh.tuple.TupleAdapter;
import com.zuooh.tuple.query.Origin;
import com.zuooh.tuple.query.Query;
import com.zuooh.tuple.subscribe.SubscriptionConnection;

public class ChessQueryUpdater implements MessageSource {   

   public static final Logger LOG = LoggerFactory.getLogger(ChessQueryUpdater.class);

   private final SubscriptionConnection connection;   
   private final AtomicReference<String> source;
   private final ChessQueryListener listener;
   private final String protocol;
   private final Query query;
   
   public ChessQueryUpdater(ChessQueryListener listener, SubscriptionConnection connection, Query query, String protocol) {
      this.source = new AtomicReference<String>();
      this.connection = connection;
      this.listener = listener;
      this.protocol = protocol;
      this.query = query;
   }
   
   @Override
   public String getName() {
      return source.get();
   }
   
   public String getProtocol(){
      return protocol;
   }

   public void subscribe(ChessUser user) throws Exception {
      SubscriptionUpdater updater = new SubscriptionUpdater(user, protocol);
      
      if(user != null) {   
         String userId = user.getKey();
         String userName = user.getName();
         
         System.setProperty("account.name", userName);
         source.set(userId);
         connection.register(updater); 
      }
   }
   
   public void cancel() throws Exception {
      connection.close();
   }
   
   private class SubscriptionUpdater extends TupleAdapter {
      
      private final ChessQuery update;
      private final ChessUser user;
      
      public SubscriptionUpdater(ChessUser user, String protocol) {
         this.update = new ChessQuery(user, protocol);
         this.user = user;
      }
      
      @Override
      public void onHeartbeat() {
         try {
            Map<String, String> templates = query.getPredicates();
            Set<String> types = templates.keySet();
            Origin origin = query.getOrigin();
            String userId = user.getKey();     
            
            if(!types.isEmpty()) {
               Map<String, String> predicates = new LinkedHashMap<String, String>();
               Query query = new Query(origin, predicates);
               
               for(String type : types) {
                  String template = templates.get(type);
                  
                  template = template.replaceAll("%\\{userId\\}", userId);
                  template = template.replaceAll("%userId", userId);
                  
                  predicates.put(type, template);
               }
               connection.update(query);
            } else {
               connection.update(query);
            }        
            connection.remove(this); // only subscribe once!!
            listener.update(update);
         } catch(Exception e) {
            LOG.info("Error occured on heartbeat", e);
         }
      }
   }
}
