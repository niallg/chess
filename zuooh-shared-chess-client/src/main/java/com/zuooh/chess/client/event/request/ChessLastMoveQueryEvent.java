package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessLastMoveQueryEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;

   private final String userId;
   private final String gameId;

   public ChessLastMoveQueryEvent(String userId, String gameId) {
      this.userId = userId;
      this.gameId = gameId;
   }

   public String getGameId() {
      return gameId;
   }

   public String getUserId() {
      return userId;
   }
}
