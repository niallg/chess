package com.zuooh.chess.client.user;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.connect.ChessQueryUpdater;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserType;

public class ChessUserDirectorySource implements ChessUserSource {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessUserDirectorySource.class);

   private final ChessUserDirectory userDirectory;
   private final ChessQueryUpdater updater;
   private final ChessUser defaultUser;

   public ChessUserDirectorySource(ChessUserDirectory userDirectory, ChessQueryUpdater updater) {
      this.defaultUser = userDirectory.loadOrCreateAnonymousUser();
      this.userDirectory = userDirectory;
      this.updater = updater;
   }   

   @Override
   public ChessUser currentUser() {
      return userDirectory.loadOrCreateActiveUser();
   }
   
   @Override
   public void saveUser(ChessUser user) { // from presence event!     
      try {        
         ChessUser activeUser = userDirectory.loadOrCreateActiveUser();
         
         if(activeUser == null) {
            throw new IllegalStateException("Could not activate any user");
         }
         String activeId = activeUser.getKey();
         String userId = user.getKey();
         
         if(userId.equals(activeId)) {
            ChessUserType userType = user.getType();
            
            if(userType.isInvalid()) { // not registered
               ChessUser anonymousUser = userDirectory.loadOrCreateAnonymousUser();
               String anonymousId = anonymousUser.getKey();
               
               if(userId.equals(anonymousId)) { // it can't be invalid if its anonymous
                  LOG.warn("Presence things " + userId + " is invalid, however it is the anonymous user!!");
               }
               userDirectory.saveAndLoginUser(anonymousUser); // switch to anonymous if its invalid                  
            } else {
               userDirectory.saveAndLoginUser(user); // ensure to swap in updated profile
            }
         } else {         
            userDirectory.saveUser(user);
         }
      } catch(Exception e) {
         LOG.info("Could not save user", e);
      }
   }

   @Override
   public List<ChessUser> listUsers() {
      return userDirectory.listUsersByLoginTime();
   }

   @Override
   public List<ChessUser> listUsers(ChessUserType type) {
      return userDirectory.listUsers(type);
   }

   @Override
   public void loginUser(ChessUser user) {
      try {
         updater.subscribe(user); // change what we subscribe for
      } catch(Exception e) {
         LOG.info("Could not change user", e);
      }
      userDirectory.saveAndLoginUser(user); 
   }

   @Override
   public void resetUser() {
      loginUser(defaultUser);
   }
}
