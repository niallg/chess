package com.zuooh.chess.client.engine;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.client.event.ChessDrawEvent;
import com.zuooh.chess.client.event.ChessResignEvent;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessMoveResult;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessRemoteGameMoveEngine implements ChessMoveEngine {
   
   private final AtomicReference<ChessOpponentStatus> opponentStatus;
   private final AtomicReference<ChessMoveListener> moveObserver;
   private final AtomicReference<ChessSide> engineSide;
   private final Map<ChessSide, ChessMove> recentMoveHistory;
   private final AtomicReference<String> opponentPlayer;   
   private final AtomicReference<ChessMove> lastMoveMade;
   private final ChessRemoteGameController remoteController;
   private final ChessRemoteGameStateUpdater stateUpdater;
   private final AtomicBoolean stopSearch;
   private final String userName;
   private final String gameId;

   public ChessRemoteGameMoveEngine(ChessRemoteGameController remoteController, String userName, String gameId) {
      this.opponentStatus = new AtomicReference<ChessOpponentStatus>(ChessOpponentStatus.NONE);
      this.recentMoveHistory = new ConcurrentHashMap<ChessSide, ChessMove>();
      this.moveObserver = new AtomicReference<ChessMoveListener>();
      this.engineSide = new AtomicReference<ChessSide>();
      this.opponentPlayer = new AtomicReference<String>();
      this.lastMoveMade = new AtomicReference<ChessMove>();     
      this.stateUpdater = new ChessRemoteGameStateUpdater();
      this.stopSearch = new AtomicBoolean(false);
      this.remoteController = remoteController;
      this.userName = userName;
      this.gameId = gameId;
   }
   
   public void setOpponentStatus(ChessOpponentStatus newStatus) {
      ChessOpponentStatus currentStatus = opponentStatus.get();
      
      if(currentStatus != ChessOpponentStatus.RESIGN) {
         opponentStatus.set(newStatus);
      }
   }
   
   public void setMoveListener(ChessMoveListener moveListener) {
      moveObserver.set(moveListener);
   }
   
   public void setOpponent(String opponent) {
      opponentPlayer.set(opponent);
   }
   
   public void setLastMove(ChessMove chessMove) {
      lastMoveMade.set(chessMove);
   }
   
   public void setSide(ChessSide newDirection) {
      engineSide.set(newDirection);
   }
  
   public void setLastMove(ChessSide side, ChessMove chessMove) {
      recentMoveHistory.put(side, chessMove);
   }
   
   public ChessMove getLastMove(ChessSide side) {
      return recentMoveHistory.get(side);
   }
   
   public String getAssociatedGame() {
      return gameId;
   }
   
   @Override
   public String getOpponent() {
      return opponentPlayer.get();
   }
   
   @Override
   public ChessMove getLastMove() {
      return lastMoveMade.get();
   }
   
   @Override
   public ChessOpponentStatus getEngineStatus() {
      return opponentStatus.get();
   }
   
   @Override
   public ChessSide getSide() {
      return engineSide.get();
   }
  
   @Override
   public void makeMove(ChessBoard chessBoard, ChessMove myLastMove) {
      stopSearch.set(false);
      
      ChessMakeMoveController moveController = new ChessMakeMoveController(remoteController, stateUpdater, this, myLastMove, stopSearch, userName, gameId);
      ChessMoveValidator moveValidator = new ChessMoveValidator(chessBoard, myLastMove);
      ChessMoveTransaction moveTransaction = null;
      
      if(myLastMove == null) {
         moveTransaction = new ChessMoveTransaction(moveController, stateUpdater, moveValidator, stopSearch, ChessMovePhase.MOVE_CONFIRMED);
      } else {
         moveTransaction = new ChessMoveTransaction(moveController, stateUpdater, moveValidator, stopSearch, ChessMovePhase.BEGIN_MOVE);
      }
      remoteController.setActionObserver(gameId, moveTransaction);
   }
   
   @Override
   public void resignGame() {
      ChessOpponentStatus status = opponentStatus.get();
      String opponentName = opponentPlayer.get();
      
      if(status != ChessOpponentStatus.RESIGN && status != ChessOpponentStatus.LOSE || status == ChessOpponentStatus.WIN) {
         ChessResignEvent resignEvent = new ChessResignEvent(userName, opponentName, gameId);

         remoteController.executeRequest(resignEvent);
      }
   }
   
   @Override
   public void drawGame() {
      ChessOpponentStatus status = opponentStatus.get();
      String opponentName = opponentPlayer.get();
      
      if(status != ChessOpponentStatus.RESIGN && status != ChessOpponentStatus.LOSE || status == ChessOpponentStatus.WIN) {
         ChessDrawEvent offerDrawEvent = new ChessDrawEvent(userName, opponentName, gameId);

         remoteController.executeRequest(offerDrawEvent);
      }
   }   
   
   @Override
   public void stopSearch() {
      stopSearch.set(true);
   }
   
   private class ChessRemoteGameStateUpdater implements ChessMoveListener {

      @Override
      public void onMoveMade(ChessMove moveMade) {
         ChessSide side = moveMade.getSide();
         ChessMoveListener listener = moveObserver.get();
         
         recentMoveHistory.put(side, moveMade);  
         lastMoveMade.set(moveMade);
         
         if(listener != null) {
            listener.onMoveMade(moveMade);
         }
      }

      @Override
      public void onMoveResult(ChessMoveResult moveResult) {
         ChessOpponentStatus resultOpponentStatus = moveResult.getStatus();
         ChessOpponentStatus currentStatus = opponentStatus.get();
         ChessMove resultMove = moveResult.getMove();
         ChessMoveListener listener = moveObserver.get();         
         
         if(resultMove != null) {
            ChessSide resultPlayDirection = resultMove.getSide();
            
            recentMoveHistory.put(resultPlayDirection, resultMove);
            lastMoveMade.set(resultMove);
         }         
         if(currentStatus != ChessOpponentStatus.RESIGN) {
            opponentStatus.set(resultOpponentStatus);   
         }
         remoteController.removeActionObserver(gameId);
         
         if(listener != null) {
            listener.onMoveResult(moveResult);
         }
      }  
   }
}
