package com.zuooh.chess.client;

public interface ChessServiceLauncher {
   void update(String user, String protocol) throws Exception;
   void launch() throws Exception;
}
