package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessAcceptGameEvent extends ChessRequestEvent {

   private static final long serialVersionUID = 1L;
   
   private String userId;
   private String gameId;
   private int userSkill;

   public ChessAcceptGameEvent(String userId, int userSkill, String gameId) {
      this.userId = userId;
      this.userSkill = userSkill;
      this.gameId = gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }

   public String getGameId() {
      return gameId;
   }

   public String getUserId() {
      return userId;
   }

   public void setUserId(String userId) {
      this.userId = userId;
   }

   public int getUserSkill() {
      return userSkill;
   }

   public void setUserSkill(int userSkill) {
      this.userSkill = userSkill;
   }
}
