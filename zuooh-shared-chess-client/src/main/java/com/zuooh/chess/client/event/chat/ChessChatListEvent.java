package com.zuooh.chess.client.event.chat;

import java.util.List;

import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.database.chat.ChessChat;

public class ChessChatListEvent extends ChessResponseEvent {
   
   private static final long serialVersionUID = 1L;

   private final List<ChessChat> chats;
   private final String playerId;

   public ChessChatListEvent(String playerId, List<ChessChat> chats) {
      this.playerId = playerId;
      this.chats = chats;
   }
   
   public String getPlayerId(){
      return playerId;
   }

   public List<ChessChat> getChats() {
      return chats;
   }
}
