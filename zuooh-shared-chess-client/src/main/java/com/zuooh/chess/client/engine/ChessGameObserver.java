package com.zuooh.chess.client.engine;

import com.zuooh.chess.database.game.ChessGame;

public interface ChessGameObserver {
   void onGameUpdate(ChessGame game);
}
