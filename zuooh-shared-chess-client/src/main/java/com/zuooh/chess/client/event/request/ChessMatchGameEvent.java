package com.zuooh.chess.client.event.request;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessMatchGameEvent extends ChessRequestEvent {

   private static final long serialVersionUID = 1L;
   
   private final ChessSide gameSide;
   private final long gameDuration;
   private final String userId;
   private final int userSkill;

   public ChessMatchGameEvent(ChessSide gameSide, long gameDuration, String userId, int userSkill) {
      this.userId = userId;
      this.userSkill = userSkill;
      this.gameSide = gameSide;
      this.gameDuration = gameDuration;     
   }

   public ChessSide getGameSide() {
      return gameSide;
   }

   public long getGameDuration() {
      return gameDuration;
   }

   public String getUserId() {
      return userId;
   }

   public int getUserSkill() {
      return userSkill;
   }   
}
