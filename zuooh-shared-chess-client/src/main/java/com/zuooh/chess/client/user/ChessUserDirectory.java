package com.zuooh.chess.client.user;

import static com.zuooh.chess.database.user.ChessUserType.ANONYMOUS;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.database.ChessDatabase;
import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.chess.database.user.ChessUserLogin;
import com.zuooh.chess.database.user.ChessUserType;

public class ChessUserDirectory {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessUserDirectory.class);
   
   private final AtomicReference<ChessUser> activeUser;
   private final ChessUserBuilder builder;
   private final ChessDatabase database;
   
   public ChessUserDirectory(ChessDatabase database, ChessUserBuilder builder) {
      this.activeUser = new AtomicReference<ChessUser>();
      this.database = database;
      this.builder = builder;
   }
   
   public synchronized ChessUser loadUser(String userId) {
      try {
         if(userId != null) {
            ChessUserDatabase userDatabase = database.getUserDatabase();
            return userDatabase.loadUser(userId);
         }
      } catch(Exception e) {
         LOG.info("Could not load user '" + userId + "'", e);
      }
      return null;
   }
   
   public synchronized ChessUser loadOrCreateAnonymousUser() {
      ChessUser user = loadLastActiveUser(ANONYMOUS);
      
      if(user == null) {
         ChessUser newUser = builder.createAnonymousUser();
         
         if(newUser != null) {
            saveAndLoginUser(newUser);
         }
         return loadLastActiveUser();
      }
      return user;
   }
   
   public synchronized ChessUser loadOrCreateActiveUser() {
      ChessUser user = loadLastActiveUser();
      
      if(user == null) {
         ChessUser newUser = builder.createAnonymousUser();
         
         if(newUser != null) {
            saveAndLoginUser(newUser);
         }
         return loadLastActiveUser();
      }
      return user;
   }
   
   public synchronized ChessUser loadLastActiveUser() {
      ChessUser user = activeUser.get();
      
      if(user == null) {
         ChessUser lastActive = loadLastActiveUser(null);
         
         if(lastActive != null) {
            activeUser.set(lastActive);
         }
         return lastActive;
      }
      return user;
   }
   
   public synchronized ChessUser loadLastActiveUser(ChessUserType type) {
      List<ChessUser> users = listUsersByLoginTime();
      
      for(ChessUser user : users) {
         ChessUserType userType = user.getType();
         
         if(type == null || userType == type) {
            return user;
         }
      }
      return null;
   }
   
   public synchronized List<ChessUser> listUsers() {
      return listUsers(null);
   } 
   
   public synchronized List<ChessUser> listUsers(ChessUserType type) {
      try {         
         ChessUserDatabase userDatabase = database.getUserDatabase();
         return userDatabase.loadUsersByLoginTime(type);
      }catch(Exception e) {
         LOG.info("Could not list users", e);
      }
      return Collections.emptyList();
   } 
   
   public synchronized List<ChessUser> listUsersByLoginTime() {
      try {         
         ChessUserDatabase userDatabase = database.getUserDatabase();
         return userDatabase.loadUsersByLoginTime();
      }catch(Exception e) {
         LOG.info("Could not list users by login", e);
      }
      return Collections.emptyList();
   }
   
   public synchronized void saveUser(ChessUser user) {
      try {         
         ChessUserDatabase userDatabase = database.getUserDatabase();
         userDatabase.saveUser(user);
      }catch(Exception e) {
         LOG.info("Could not save user", e);
      }     
   }
   
   public synchronized void saveAndLoginUser(ChessUser user) {
      long currentTime = System.currentTimeMillis();    
      ChessUserType userType = user.getType();
      String userId = user.getKey();
      
      try {
         ChessUserDatabase userDatabase = database.getUserDatabase();
         
         if(userType.isAnonymous()) {
            List<ChessUser> localUsers = userDatabase.loadUsersByLoginTime();
            
            for(ChessUser localUser : localUsers) {
               ChessUserType localUserType = localUser.getType();
               String localUserId = localUser.getKey();
               
               if(localUserType.isAnonymous() && !localUserId.equals(userId)) {
                  throw new IllegalStateException("Anonymous login for " + userId + " but " + localUserId + " already exists");
               }
            }
         }
        ChessUserLogin userLogin = new ChessUserLogin(userType, userId, currentTime);
        
        activeUser.set(user);
        userDatabase.saveUser(user);
        userDatabase.saveUserLogin(userLogin);
      } catch(Exception e) {
         LOG.info("Could not save user", e);
      }      
   }
}
 