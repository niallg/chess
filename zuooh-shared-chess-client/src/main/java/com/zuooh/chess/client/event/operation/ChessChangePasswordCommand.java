package com.zuooh.chess.client.event.operation;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.request.ChessChangePasswordEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.message.client.ResponseStatus;

public class ChessChangePasswordCommand {

   private final ChessRequestCommand<ChessChangePasswordEvent, ChessUserValidationEvent> operation;
   
   public ChessChangePasswordCommand(ChessMessagePublisher publisher) {
      this.operation = new ChessRequestCommandClient<ChessChangePasswordEvent, ChessUserValidationEvent>(publisher, ChessChangePasswordEvent.class, ChessUserValidationEvent.class);
   }
   
   public ChessChangePasswordEvent getResetRequest() {
      return operation.getRequest();
   }
   
   public ChessUserValidationEvent getValidationResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute(String userId, String password) {
      ChessChangePasswordEvent event = new ChessChangePasswordEvent(userId, password);
      
      operation.update(event);
      operation.execute();      
   }
   
   public void reset() {
      operation.clear();
   }
}
