package com.zuooh.chess.client.event.operation;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.request.ChessSignInEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.message.client.ResponseStatus;

public class ChessSignInCommand {

   private final ChessRequestCommand<ChessSignInEvent, ChessUserValidationEvent> operation;
   
   public ChessSignInCommand(ChessMessagePublisher publisher) {
      this.operation = new ChessRequestCommandClient<ChessSignInEvent, ChessUserValidationEvent>(publisher, ChessSignInEvent.class, ChessUserValidationEvent.class);
   }
   
   public ChessSignInEvent getSignInRequest() {
      return operation.getRequest();
   }
   
   public ChessUserValidationEvent getValidationResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute(String user, String password) {
      ChessSignInEvent event = new ChessSignInEvent(user, password);
      
      operation.update(event);
      operation.execute();      
   }
   
   public void reset() {
      operation.clear();
   }
}
