package com.zuooh.chess.client.event.response;

import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.client.user.ChessUserValidation;
import com.zuooh.chess.database.user.ChessUser;

public class ChessUserValidationEvent extends ChessResponseEvent {
   
   private static final long serialVersionUID = 1L;

   private ChessUserValidation status;
   private ChessUser user;

   public ChessUserValidationEvent(ChessUserValidation status, ChessUser user) {
      this.status = status;
      this.user = user;
   }
   
   public ChessUserValidation getValidation() {
      return status;
   }

   public ChessUser getUser() {
      return user;
   }
}
