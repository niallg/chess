package com.zuooh.chess.client.event.chat;

import com.zuooh.chess.client.event.ChessEvent;
import com.zuooh.chess.database.chat.ChessChatStatus;

public class ChessChatStatusEvent extends ChessEvent {
   
   private static final long serialVersionUID = 1L;

   private final ChessChatStatus status;
   private final String chatId;
   
   public ChessChatStatusEvent(ChessChatStatus status, String chatId) {
      this.status = status;
      this.chatId = chatId;
   }

   public ChessChatStatus getStatus() {
      return status;
   }

   public String getChatId() {
      return chatId;
   }   
}
