package com.zuooh.chess.client.event;

public class ChessDrawEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;

   private final String opponentId;
   private final String playerId;
   private final String gameId;

   public ChessDrawEvent(String playerId, String opponentId, String gameId) {
      this.opponentId = opponentId;
      this.playerId = playerId;
      this.gameId = gameId;
   }
   
   public String getOpponentId() {
      return opponentId;
   }

   public String getGameId() {
      return gameId;
   }

   public String getPlayerId() {
      return playerId;
   }
}
