package com.zuooh.chess.client;

public enum ChessServerStatus {
   AVAILABLE,
   UNAVAILABLE;
}
