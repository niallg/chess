package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessReconcileGamesEvent extends ChessRequestEvent {

   private static final long serialVersionUID = 1L;
   
   private String userId;

   public ChessReconcileGamesEvent(String userId) {
      this.userId = userId;
   }

   public String getUserId() {
      return userId;
   }
}
