package com.zuooh.chess.client.event;

public abstract class ChessResponseEvent extends ChessEvent {
   
   private static final long serialVersionUID = 1L;
   
   protected ChessResponseEvent() {
      super();
   }
}
