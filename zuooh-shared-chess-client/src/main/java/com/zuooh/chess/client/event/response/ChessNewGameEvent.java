package com.zuooh.chess.client.event.response;

import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.database.game.ChessGame;

public class ChessNewGameEvent extends ChessResponseEvent {
   
   private static final long serialVersionUID = 1L;

   private ChessGame game;

   public ChessNewGameEvent(ChessGame game) {
      this.game = game;
   }

   public ChessGame getGame() {
      return game;
   }

   public void setGame(ChessGame game) {
      this.game = game;
   }
}
