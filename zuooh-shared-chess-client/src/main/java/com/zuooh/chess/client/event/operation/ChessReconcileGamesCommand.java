package com.zuooh.chess.client.event.operation;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.request.ChessReconcileGamesEvent;
import com.zuooh.chess.client.event.response.ChessListOfGamesEvent;
import com.zuooh.message.client.ResponseStatus;

public class ChessReconcileGamesCommand {
   
   private final ChessRequestCommand<ChessReconcileGamesEvent, ChessListOfGamesEvent> operation;
   
   public ChessReconcileGamesCommand(ChessMessagePublisher publisher) {
      this.operation = new ChessRequestCommandClient<ChessReconcileGamesEvent, ChessListOfGamesEvent>(publisher, ChessReconcileGamesEvent.class, ChessListOfGamesEvent.class);
   }
   
   public ChessReconcileGamesEvent getReconcileRequest() {
      return operation.getRequest();
   }
   
   public ChessListOfGamesEvent getListOfGamesResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute(String userId) {
      ChessReconcileGamesEvent event = new ChessReconcileGamesEvent(userId);
      
      operation.update(event);
      operation.execute();      
   }
   
   public void reset() {
      operation.clear();
   }
}
