package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessRankQueryEvent extends ChessRequestEvent {

   private static final long serialVersionUID = 1L;
   
   private final int from;
   private final int size;
   
   public ChessRankQueryEvent() {
      this(0, 10);
   }
   
   public ChessRankQueryEvent(int from, int size) {
      this.from = from;
      this.size = size;
   }
   
   public int getFrom(){
      return from;
   }
   
   public int getSize() {
      return size;
   }
}
