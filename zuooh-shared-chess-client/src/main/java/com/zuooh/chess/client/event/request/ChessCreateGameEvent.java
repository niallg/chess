package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;
import com.zuooh.chess.database.game.ChessGameCriteria;

public class ChessCreateGameEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;

   private ChessGameCriteria challenge;
   private String challengeUserId;
   private String gameId;
   
   public ChessCreateGameEvent(ChessGameCriteria challenge, String challengeUserId, String gameId) {
      this.challengeUserId = challengeUserId;
      this.challenge = challenge;
      this.gameId = gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }

   public String getGameId() {
      return gameId;
   }

   public ChessGameCriteria getChallenge() {
      return challenge;
   }

   public void setChallenge(ChessGameCriteria challenge) {
      this.challenge = challenge;
   }

   public String getChallengeUserId() {
      return challengeUserId;
   }

   public void setChallengeUserId(String challengeUserId) {
      this.challengeUserId = challengeUserId;
   }
}
