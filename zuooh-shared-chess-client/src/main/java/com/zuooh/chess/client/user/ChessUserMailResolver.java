package com.zuooh.chess.client.user;

import java.util.Set;

import com.zuooh.application.Address;
import com.zuooh.application.AddressBook;
import com.zuooh.application.AddressType;
import com.zuooh.application.MobileApplication;

public class ChessUserMailResolver {

   private final MobileApplication application;
   
   public ChessUserMailResolver(MobileApplication application) {     
      this.application = application;
   }

   public String resolveBestMail() {
      AddressBook book = application.getAddressBook();
      Set<Address> accounts = book.getAccounts(AddressType.EMAIL);

      if (!accounts.isEmpty()) {
         String facebookAccount = null;
         String googleAccount = null;
         String anyAccount = null;

         for (Address address : accounts) {
            String accountType = address.getCategory();
            String accountMail = address.getAddress();

            if (accountType != null) {
               if (accountType.equals("com.facebook.auth.login")) {
                  if (facebookAccount == null) {
                     facebookAccount = accountMail;
                  }
               } else if (accountType.equals("com.google")) {
                  if (googleAccount == null) {
                     googleAccount = accountMail;
                  }
               }
            } else if (anyAccount == null) {
               anyAccount = accountMail;
            }            
         }
         if(facebookAccount != null) {
            return facebookAccount;
         }
         if(googleAccount != null) {
            return googleAccount;
         }
         return anyAccount;
      }
      return null;
   }
}
