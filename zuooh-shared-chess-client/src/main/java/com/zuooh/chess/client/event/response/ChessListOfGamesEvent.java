package com.zuooh.chess.client.event.response;

import java.util.List;

import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.database.game.ChessGame;

public class ChessListOfGamesEvent extends ChessResponseEvent {
   
   private static final long serialVersionUID = 1L;

   private final List<ChessGame> games;

   public ChessListOfGamesEvent(List<ChessGame> games) {
      this.games = games;
   }

   public List<ChessGame> getGames() {
      return games;
   }
}
