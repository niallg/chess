package com.zuooh.chess.client.engine;

import com.zuooh.chess.client.event.ChessEvent;

public interface ChessRemoteGameController {
   void setGameObserver(String gameId, ChessGameObserver gameObserver);
   void removeGameObserver(String gameId);
   void setActionObserver(String gameId, ChessMoveObserver gameObserver);
   void removeActionObserver(String gameId);
   void executeRequest(ChessEvent chessEvent);
}
