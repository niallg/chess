package com.zuooh.chess.client.event;

import java.io.Serializable;

public abstract class ChessEvent implements Serializable {
   
   private static final long serialVersionUID = 1L;
   
   private long timeStamp;
   
   protected ChessEvent() {
      this.timeStamp = System.currentTimeMillis();
   }

   public long getTimeStamp() {
      return timeStamp;
   }

   public void setTimeStamp(long timeStamp) {
      this.timeStamp = timeStamp;
   }
}
