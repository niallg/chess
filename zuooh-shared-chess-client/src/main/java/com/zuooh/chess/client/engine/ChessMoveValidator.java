package com.zuooh.chess.client.engine;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;

public class ChessMoveValidator {

   private final ChessMove myMove;
   private final ChessBoard board;

   public ChessMoveValidator(ChessBoard board, ChessMove myMove) {
      this.myMove = myMove;
      this.board = board;
   }

   public boolean isWin() {
      if(myMove != null) {
         ChessSide mySide = myMove.getSide();
         ChessSide opponentSide = mySide.oppositeSide();
   
         if(ChessCheckAnalyzer.checkMate(board, opponentSide)) {
            return true;
         }
      }
      return false;
   }

   public boolean isLose() {
      if(myMove != null) {
         ChessSide mySide = myMove.getSide();
   
         if(ChessCheckAnalyzer.checkMate(board, mySide)) {
            return true;
         }
      }
      return false;
   }

   public boolean isDraw() {
      if(myMove != null) {
         ChessSide mySide = myMove.getSide();
         ChessSide opponentSide = mySide.oppositeSide();
         
         if(ChessCheckAnalyzer.staleMate(board, opponentSide)) { // did my move put opponent in stale mate?
            return true;
         }
         if(!ChessCheckAnalyzer.checkMatePossible(board)) { // insufficient material
            return true;
         }
      }
      return false;
   }

   public boolean isNewMove(ChessMove move) {
      if(myMove != null) {
         ChessSide side = move.getSide();
         ChessSide myDirection = myMove.getSide();
   
         if(myDirection != side) {
            int changeCount = board.getChangeCount();
            return changeCount == move.change;
         }
         return false;
      }
      return true;
   }

   public boolean isConfirmationMove(ChessMove move) {
      if(myMove != null) {
         ChessSide side = move.getSide();
         ChessSide mySide = myMove.getSide();
   
         if(side == mySide) {
            return move.from == myMove.from && move.to == myMove.to;
         }
      }
      return false;
   }
}
