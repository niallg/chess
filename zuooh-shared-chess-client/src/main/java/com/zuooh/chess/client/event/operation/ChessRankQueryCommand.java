package com.zuooh.chess.client.event.operation;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.request.ChessRankQueryEvent;
import com.zuooh.chess.client.event.response.ChessRankResultEvent;
import com.zuooh.message.client.ResponseStatus;

public class ChessRankQueryCommand {
   
   private final ChessRequestCommand<ChessRankQueryEvent, ChessRankResultEvent> operation;
   
   public ChessRankQueryCommand(ChessMessagePublisher publisher) {
      this.operation = new ChessRequestCommandClient<ChessRankQueryEvent, ChessRankResultEvent>(publisher, ChessRankQueryEvent.class, ChessRankResultEvent.class);
   }
   
   public ChessRankQueryEvent getRankRequest() {
      return operation.getRequest();
   }
   
   public ChessRankResultEvent getRankResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute() {
      ChessRankQueryEvent event = new ChessRankQueryEvent();
      
      operation.update(event);
      operation.execute();      
   }
   
   public void reset() {
      operation.clear();
   }
}
