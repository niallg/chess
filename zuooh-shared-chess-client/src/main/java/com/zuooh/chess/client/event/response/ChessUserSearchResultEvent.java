package com.zuooh.chess.client.event.response;

import java.util.ArrayList;
import java.util.List;

import com.zuooh.chess.client.event.ChessResponseEvent;
import com.zuooh.chess.database.user.ChessUser;

public class ChessUserSearchResultEvent extends ChessResponseEvent {
   
   private static final long serialVersionUID = 1L;

   private List<ChessUser> users;
   private String term;

   public ChessUserSearchResultEvent() {
      this(null);
   }

   public ChessUserSearchResultEvent(String term) {
      this.users = new ArrayList<ChessUser>();
      this.term = term;
   }

   public void setTerm(String term) {
      this.term = term;
   }

   public String getTerm() {
      return term;
   }

   public List<ChessUser> getUsers() {
      return users;
   }

   public void addUser(ChessUser user) {
      users.add(user);
   }
}
