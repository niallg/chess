package com.zuooh.chess.client.engine;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveResult;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessMoveTransaction implements ChessMoveObserver {

   private final AtomicReference<ChessMovePhase> currentPhase;
   private final ChessMakeMoveController moveController;
   private final ChessMoveValidator moveValidator;
   private final ChessMoveListener moveListener;
   private final AtomicBoolean gameOver;

   public ChessMoveTransaction(ChessMakeMoveController moveController, ChessMoveListener moveListener, ChessMoveValidator moveValidator, AtomicBoolean gameOver) {
      this(moveController, moveListener, moveValidator, gameOver, ChessMovePhase.BEGIN_MOVE);
   }
   
   public ChessMoveTransaction(ChessMakeMoveController moveController, ChessMoveListener moveListener, ChessMoveValidator moveValidator, AtomicBoolean gameOver, ChessMovePhase movePhase) {
      this.currentPhase = new AtomicReference<ChessMovePhase>(movePhase);
      this.moveController = moveController;
      this.moveListener = moveListener;
      this.moveValidator = moveValidator;
      this.gameOver = gameOver;
   }

   public void onStatus(ChessOpponentStatus status) {
      ChessMovePhase movePhase = currentPhase.get();

      if(movePhase == ChessMovePhase.BEGIN_MOVE) {
         moveController.makeMove();
         currentPhase.set(ChessMovePhase.MOVE_MADE);
      } else if(movePhase == ChessMovePhase.MOVE_MADE) {
         moveController.requestLastMove();
      } else if(movePhase == ChessMovePhase.MOVE_CONFIRMED) {
         if(moveValidator.isWin()) {
            moveController.gameOver(ChessBoardResult.WIN);
         } else if(moveValidator.isLose()) {
            moveController.gameOver(ChessBoardResult.LOSE);
         }else if(moveValidator.isDraw()) {
            moveController.gameOver(ChessBoardResult.DRAW);
         } else {
            moveController.requestOpponentMove();
         }
      }
   }

   public void onLastMove(ChessMove move) {
      ChessMovePhase movePhase = currentPhase.get();

      if(movePhase == ChessMovePhase.MOVE_MADE) {
         if(move == null) {
            moveController.makeMove();
         } else {
            if(moveValidator.isConfirmationMove(move)) {
               if(currentPhase.compareAndSet(ChessMovePhase.MOVE_MADE, ChessMovePhase.MOVE_CONFIRMED)) {
                  if(!gameOver.get()) {
                     moveListener.onMoveMade(move);
                  }
               }
            } else {
               moveController.makeMove();
            }
         }
      }
   }

   public void onMove(ChessMove move) {
      ChessMovePhase movePhase = currentPhase.get();

      if(movePhase == ChessMovePhase.MOVE_MADE) {
         if(moveValidator.isConfirmationMove(move)) {
            if(currentPhase.compareAndSet(ChessMovePhase.MOVE_MADE, ChessMovePhase.MOVE_CONFIRMED)) {
               if(!gameOver.get()) {
                  moveListener.onMoveMade(move);
               }
            }
         }
      } else if(movePhase == ChessMovePhase.MOVE_CONFIRMED) {
         if(moveValidator.isNewMove(move)) {
            ChessMoveResult moveResult = new ChessMoveResult(ChessOpponentStatus.ONLINE, move);

            if(currentPhase.compareAndSet(ChessMovePhase.MOVE_CONFIRMED, ChessMovePhase.OPPONENT_MOVE_MADE)) {
               if(!gameOver.get()) {
                  moveListener.onMoveResult(moveResult);
               }
            }
         }
      }
   }
   
   public void onDraw() {
      moveController.opponentAcceptDraw();
   }

   public void onResign() {
      moveController.opponentResign();
   }
}
