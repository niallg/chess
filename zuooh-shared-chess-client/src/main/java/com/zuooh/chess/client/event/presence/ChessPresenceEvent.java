package com.zuooh.chess.client.event.presence;

import com.zuooh.chess.client.event.ChessEvent;

public class ChessPresenceEvent extends ChessEvent {
   
   private static final long serialVersionUID = 1L;
   
   private final String userId;
   private final boolean anonymous;
   
   public ChessPresenceEvent(String userId, boolean anonymous) {
      this.anonymous = anonymous;
      this.userId = userId;
   }
   
   public String getUserId() {
      return userId;
   }
   
   public boolean isAnonymous() {
      return anonymous;
   }

}
