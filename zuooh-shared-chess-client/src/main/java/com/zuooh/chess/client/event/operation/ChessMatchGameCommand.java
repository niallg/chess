package com.zuooh.chess.client.event.operation;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.request.ChessMatchGameEvent;
import com.zuooh.chess.client.event.response.ChessNewGameEvent;
import com.zuooh.message.client.ResponseStatus;

public class ChessMatchGameCommand {

   private final ChessRequestCommand<ChessMatchGameEvent, ChessNewGameEvent> operation;
   
   public ChessMatchGameCommand(ChessMessagePublisher publisher) {
      this.operation = new ChessRequestCommandClient<ChessMatchGameEvent, ChessNewGameEvent>(publisher, ChessMatchGameEvent.class, ChessNewGameEvent.class);
   }
   
   public ChessMatchGameEvent getMatchRequest() {
      return operation.getRequest();
   }
   
   public ChessNewGameEvent getGameResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute(ChessSide gameSide, long gameDuration, String userId, int userSkill) {
      ChessMatchGameEvent event = new ChessMatchGameEvent(gameSide, gameDuration, userId, userSkill);
      
      operation.update(event);
      operation.execute();      
   }
   
   public void reset() {
      operation.clear();
   }
}
