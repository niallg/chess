package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessChangePasswordEvent extends ChessRequestEvent {

   private static final long serialVersionUID = 1L;   
   
   private final String password;
   private final String userId;

   public ChessChangePasswordEvent(String userId, String password) {
      this.password = password;
      this.userId = userId;
   }
   
   public String getUserId() {
      return userId;
   }
   
   public String getPassword() {
      return password;
   }
}
