package com.zuooh.chess.client.event.operation.form;

import java.util.Map;

import com.zuooh.chess.client.event.ChessRequestEvent;
import com.zuooh.chess.client.event.ChessResponseEvent;

public interface ChessFormAction<A extends ChessRequestEvent, B extends ChessResponseEvent> {
   String validateRequest(Map<String, String> attributes) throws Exception;
   A createRequest(Map<String, String> attributes) throws Exception;
   String validateResponse(B response) throws Exception;
   String requiresRedirect(B response) throws Exception;   
   String executeResponse(B response) throws Exception;
}
