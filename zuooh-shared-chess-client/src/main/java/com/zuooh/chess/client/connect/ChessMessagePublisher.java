package com.zuooh.chess.client.connect;

import static com.zuooh.message.client.ResponseStatus.NOT_CONNECTED;

import com.zuooh.chess.client.event.ChessEvent;
import com.zuooh.message.client.ResponseHandle;
import com.zuooh.message.client.ResponseMessage;
import com.zuooh.message.client.ResponseStatus;
import com.zuooh.message.client.publish.NotificationPublisher;
import com.zuooh.message.client.publish.RequestHandlerExecutor;

public class ChessMessagePublisher {
   
   private final RequestHandlerExecutor executor;
   private final NotificationPublisher publisher;
   private final ResponseHandle failure;
   private final boolean enable;
   
   public ChessMessagePublisher(RequestHandlerExecutor executor, NotificationPublisher publisher) {
      this(executor, publisher, true);
   }
   
   public ChessMessagePublisher(RequestHandlerExecutor executor, NotificationPublisher publisher, boolean enable) {
      this.failure = new ConnectionFailureHandle(NOT_CONNECTED);
      this.publisher = publisher;
      this.executor = executor;
      this.enable = enable;
   }   
   
   public void publishNotification(ChessEvent event) throws Exception {
      try {
         if(enable) {
            publisher.publish(event);
         }
      } catch(Exception e) {
         throw new IllegalStateException("Could not publish notification", e);
      }
   }
   
   public ResponseHandle executeRequest(ChessEvent event) throws Exception {
      try {
         if(enable) {                   
            return executor.execute(event);
         }
      } catch(Exception e) {
         throw new IllegalStateException("Could not publish message", e);
      }
      return failure;
   }
   
   private class ConnectionFailureHandle implements ResponseHandle {
      
      private final ResponseStatus status;
      
      public ConnectionFailureHandle(ResponseStatus status) {
         this.status = status;
      }

      @Override
      public ResponseStatus getStatus() throws Exception {
         return status;
      }

      @Override
      public ResponseMessage getResponse() throws Exception {
         return null;
      }
      
   }
}
