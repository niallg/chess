package com.zuooh.chess.client.connect;

import com.zuooh.message.client.publish.RequestMessageQueue;

public class ChessMessageConnector implements ChessQueryListener {
   
   private final RequestMessageQueue queue;
   
   public ChessMessageConnector(RequestMessageQueue queue) {
      this.queue = queue;
   }

   @Override
   public void update(ChessQuery query) throws Exception {
      queue.start();
   }

}
