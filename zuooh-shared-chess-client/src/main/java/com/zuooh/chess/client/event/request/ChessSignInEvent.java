package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessSignInEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;

   private String password;
   private String userName;

   public ChessSignInEvent(String userName, String password) {
      this.password = password;
      this.userName = userName;
   }
   
   public String getUserName(){
      return userName;
   }
   
   public String getPassword() {
      return password;
   }
}
