package com.zuooh.chess.client.event.request;

import com.zuooh.chess.client.event.ChessRequestEvent;

public class ChessCheckUserNameEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;

   private final String name;

   public ChessCheckUserNameEvent(String name) {
      this.name = name;
   }

   public String getUserName() {
      return name;
   }
}
