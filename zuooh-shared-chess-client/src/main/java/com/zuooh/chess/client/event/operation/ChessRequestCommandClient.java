package com.zuooh.chess.client.event.operation;

import java.util.concurrent.atomic.AtomicReference;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.ChessEvent;
import com.zuooh.message.client.ResponseHandle;
import com.zuooh.message.client.ResponseMessage;
import com.zuooh.message.client.ResponseStatus;

public class ChessRequestCommandClient<A extends ChessEvent, B> implements ChessRequestCommand<A, B> {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessRequestCommandClient.class);
   
   private final AtomicReference<ResponseHandle> reference;
   private final AtomicReference<ResponseStatus> override;
   private final ChessMessagePublisher publisher;
   private final AtomicReference<A> value;
   private final Class<B> expect;
   private final Class<A> send;
   
   public ChessRequestCommandClient(ChessMessagePublisher publisher, Class<A> send, Class<B> expect) {
      this.reference = new AtomicReference<ResponseHandle>();
      this.override = new AtomicReference<ResponseStatus>();
      this.value = new AtomicReference<A>();
      this.publisher = publisher;
      this.expect = expect;
      this.send = send;
   }   

   @Override
   public A getRequest() {
      return value.get();
   }   

   @Override
   public B getResponse() {
      ResponseHandle handle = reference.get();
      
      try {
         if(handle != null) {
            ResponseMessage response = handle.getResponse();
            Object result = response.getResult();
            
            if(result != null) {
               ResponseStatus status = handle.getStatus();
               
               if(status == ResponseStatus.ERROR) {
                  throw new IllegalStateException("Call to method resulted in exception", (Throwable)result);
               }               
               Class type = result.getClass();
               
               if(type != expect) {
                  throw new IllegalStateException("Response expected " + expect + " but got " + type);
               }
               return (B)result;
            }
         }
      } catch(Exception e) {
         LOG.info("Could not acquire response", e);         
      }
      return null;
   }   

   @Override
   public ResponseStatus getStatus() {
      ResponseStatus status = override.get();
      
      if(status != null && status != ResponseStatus.NONE) {
         return status;
      }
      ResponseHandle handle = reference.get();
      
      try {
         if(handle != null) {
            return handle.getStatus();
         }
      } catch(Exception e) {
         LOG.info("Could not determine status", e);
         return ResponseStatus.ERROR;
      }
      return ResponseStatus.NONE;
   }
   

   @Override
   public void update(A object) {
      if(object != null) {
         Class type = object.getClass();
         
         if(type != send) {
            reference.set(null);
            override.set(ResponseStatus.ERROR);
            throw new IllegalStateException("This operation sends " + send + " rather than " + type);
         }
      }
      reference.set(null);
      override.set(ResponseStatus.NONE);
      value.set(object);
   }   

   @Override
   public void execute() {
      A object = value.get();
      
      if(object == null) {
         throw new IllegalStateException("Unable to executed without a value of " + send);
      }
      try {
         ResponseHandle handle = publisher.executeRequest(object);
         
         if(handle != null) {
            override.set(ResponseStatus.NONE);
            reference.set(handle);
         }
      } catch(Exception e) {
         LOG.info("Could not execute request", e);
         reference.set(null);
         override.set(ResponseStatus.ERROR);
      }
   }
   
   public void clear() {
      override.set(ResponseStatus.FINISHED);
   }
}
