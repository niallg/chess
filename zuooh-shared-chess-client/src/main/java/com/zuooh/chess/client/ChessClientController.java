package com.zuooh.chess.client;

import com.zuooh.chess.database.game.ChessGame;
import com.zuooh.chess.engine.ChessMove;

public interface ChessClientController {
   void statusOnline();
   void statusOffline();
   void updateGame(ChessGame game);
   void lastMoveMade(String gameId, ChessMove move);
   void moveMade(String gameId, ChessMove move);
   void deleteGame(String gameId);
   void resignGame(String gameId);
   void drawGame(String gameId);
   void checkNotifications();
   void restart();
}
