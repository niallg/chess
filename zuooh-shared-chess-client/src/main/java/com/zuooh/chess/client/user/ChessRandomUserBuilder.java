package com.zuooh.chess.client.user;

import static com.zuooh.chess.database.user.ChessUserType.ANONYMOUS;
import static com.zuooh.chess.database.user.ChessUserType.NORMAL;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserData;

public class ChessRandomUserBuilder implements ChessUserBuilder {
   
   public static final String ANONYMOUS_NAME = "Anonymous";

   private final ChessUserKeyGenerator generator;

   public ChessRandomUserBuilder() {
      this.generator = new ChessUserKeyGenerator();
   }
   
   @Override
   public ChessUser createAnonymousUser() {
      String userId = generator.generateKey(ANONYMOUS, null);
      ChessUserData data = createUserData();
      
      return new ChessUser(data, ANONYMOUS, userId, ANONYMOUS_NAME, null, null);
   }
   
   @Override
   public ChessUser createNormalUser(String userName, String password) {
      String userId = generator.generateKey(NORMAL, null, userName);
      ChessUserData data = createUserData();

      if(userName == null) {
         throw new IllegalArgumentException("User name must not be null");
      }
      if(userName.equals("")) {
         throw new IllegalArgumentException("User name must not be blank");
      }
      if(password == null) {
         throw new IllegalArgumentException("User password must not be null");
      }
      if(password.equals("")) {
         throw new IllegalArgumentException("User password cannot be blank");
      }
      return new ChessUser(data, NORMAL, userId, userName, null, password);
   }   
   
   private ChessUserData createUserData() {
      return new ChessUserData();
   }
}
