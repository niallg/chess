package com.zuooh.chess.client.event.chat;

import com.zuooh.chess.client.event.ChessEvent;
import com.zuooh.chess.database.chat.ChessChat;

public class ChessChatEvent extends ChessEvent {
   
   private static final long serialVersionUID = 1L;

   private final ChessChat chat;

   public ChessChatEvent(ChessChat chat) {
      this.chat = chat;
   }

   public ChessChat getChat() {
      return chat;
   }
}
