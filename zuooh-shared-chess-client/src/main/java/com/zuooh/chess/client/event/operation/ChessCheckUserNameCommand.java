package com.zuooh.chess.client.event.operation;

import com.zuooh.chess.client.connect.ChessMessagePublisher;
import com.zuooh.chess.client.event.request.ChessCheckUserNameEvent;
import com.zuooh.chess.client.event.response.ChessUserValidationEvent;
import com.zuooh.message.client.ResponseStatus;

public class ChessCheckUserNameCommand {

   private final ChessRequestCommand<ChessCheckUserNameEvent, ChessUserValidationEvent> operation;
   
   public ChessCheckUserNameCommand(ChessMessagePublisher publisher) {
      this.operation = new ChessRequestCommandClient<ChessCheckUserNameEvent, ChessUserValidationEvent>(publisher, ChessCheckUserNameEvent.class, ChessUserValidationEvent.class);
   }
   
   public ChessCheckUserNameEvent getRequest() {
      return operation.getRequest();
   }
   
   public ChessUserValidationEvent getValidationResponse() {
      return operation.getResponse();
   }
   
   public ResponseStatus getStatus() {
      return operation.getStatus();
   }
   
   public void execute(String userName) {
      ChessCheckUserNameEvent event = new ChessCheckUserNameEvent(userName);
      
      operation.update(event);
      operation.execute();      
   }
   
   public void reset() {
      operation.clear();
   }
}
