package com.zuooh.chess.client.event;

import com.zuooh.chess.board.ChessBoardResult;

public class ChessGameOverEvent extends ChessRequestEvent {
   
   private static final long serialVersionUID = 1L;

   private final ChessBoardResult result;
   private final String playerId;
   private final String gameId;

   public ChessGameOverEvent(ChessBoardResult result, String playerId, String gameId) {
      this.playerId = playerId;
      this.gameId = gameId;
      this.result = result;
   }
   
   public ChessBoardResult getResult() {
      return result;
   }

   public String getGameId() {
      return gameId;
   }

   public String getPlayerId() {
      return playerId;
   }
}
