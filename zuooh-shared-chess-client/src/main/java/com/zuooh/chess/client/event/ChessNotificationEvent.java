package com.zuooh.chess.client.event;

import com.zuooh.chess.database.notify.ChessNotificationType;

public class ChessNotificationEvent extends ChessEvent {

   private static final long serialVersionUID = 1L;

   private final ChessNotificationType notificationType;
   private final String gameId;
   private final String userId;

   public ChessNotificationEvent(ChessNotificationType notificationType, String gameId, String userId) {
      this.notificationType = notificationType;
      this.gameId = gameId;
      this.userId = userId;
   }
   
   public ChessNotificationType getType() {
      return notificationType;
   }

   public String getGameId() {
      return gameId;
   }

   public String getUserId() {
      return userId;
   }   
}
