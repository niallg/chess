package com.zuooh.chess.client.event;

import com.zuooh.chess.engine.ChessMove;

public class ChessMoveEvent extends ChessEvent {
   
   private static final long serialVersionUID = 1L;

   private String opponentId;
   private String userId;
   private String gameId;
   private ChessMove move;

   public ChessMoveEvent(ChessMove move, String userId, String opponentId, String gameId) {
      this.opponentId = opponentId;
      this.userId = userId;
      this.move = move;
      this.gameId = gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }

   public String getGameId() {
      return gameId;
   }
   
   public String getOpponentId() {
      return opponentId;
   }
   
   public void setOpponentId(String opponentId) {
      this.opponentId = opponentId;
   }

   public String getUserId() {
      return userId;
   }

   public void setUserId(String userId) {
      this.userId = userId;
   }

   public ChessMove getMove() {
      return move;
   }

   public void setMove(ChessMove move) {
      this.move = move;
   }
}
