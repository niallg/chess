package com.zuooh.chess.client.user;

import java.util.List;

import com.zuooh.chess.database.user.ChessUser;
import com.zuooh.chess.database.user.ChessUserType;

public interface ChessUserSource {
   ChessUser currentUser();   
   List<ChessUser> listUsers();
   List<ChessUser> listUsers(ChessUserType type);
   void saveUser(ChessUser user);
   void loginUser(ChessUser user);
   void resetUser();
}
