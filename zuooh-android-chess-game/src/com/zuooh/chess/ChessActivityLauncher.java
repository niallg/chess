package com.zuooh.chess;

import com.zuooh.application.android.AndroidLauncher;

public class ChessActivityLauncher extends AndroidLauncher {

   @Override
   public String getStoreKey() {
      return "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEApzb4e5k4U2JV0yaTqA1AmWKRotjh/P6GFkpgXTJzDx18U7wdqVqrJgJfyYzx+vXVubfKzpAf70HMlHFjUDr9+S8XsPuJNGQWJof4dTnJodl2rSJj1p8rXYDKMCaU9mla6RcY0j0KFh/99GJSwz5YrNbFrHUhpfGR/eDppy4U2FIEFblcBNw6tEr7zu55eupyjkwjwZDstHmU2kBI7rBLzXObPrMzFlf4y98KF8nL/HpOZ8uuUHNmUAsSrRgDj0FBThTnk7+oUTWlucuSXNxuNRFSeWIWcOpHu+WoDxF+w653Rsip8s5V3PvezhSRB3BLfZBSMMhaxFSJRFzpEeUWpwIDAQAB";      		
   }
   
   @Override
   public String getPagePackageName() {
      return "com.zuooh.chess.page";
   }
   
   @Override
   public String getDatabasePackageName() {
      return "com.zuooh.chess.database";
   }

   @Override
   public String getContainerClassName() {
      return "com.zuooh.chess.container.ChessContainer";
   }
   
   @Override
   public String getName() {
      return "application";
   }

}
