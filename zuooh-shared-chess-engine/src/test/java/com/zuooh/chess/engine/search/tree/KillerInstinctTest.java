package com.zuooh.chess.engine.search.tree;

import java.util.Collections;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.score.ChessScoreEvaluation;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.score.ChessSimpleScoreEvaluation;
import com.zuooh.chess.engine.score.ChessTaperedScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchFeature;
import com.zuooh.chess.engine.search.ChessSearchResponse;
import com.zuooh.chess.engine.search.ChessSearchTracer;

public class KillerInstinctTest extends TestCase {   

   private static final String[][] BOARD = {
   { "bR", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "bP", "  ", "  ", "  ", "  ", "bP", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "bP", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "bP", "  ", "  ", "bP", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "wP", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }};


   public void testTranspositionTable() throws InterruptedException {
   /*   ChessSearchCriteria searchCriteria = new ChessSearchCriteria( Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, 5);
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessScoreStrategy scoreStrategy = new ChessTaperedScoreStrategy(scoreEvaluation);
      ChessSearchTracer searchTracer = new ChessSearchTracer();
      ChessQuiescenceSearchAlgorithm searchAlgorithm = new ChessQuiescenceSearchAlgorithm(searchCriteria, scoreStrategy, searchTracer);
      ChessPiece blackKing = new ChessPiece("" + ChessPieceKey.BLACK_KING.code + ChessPieceKey.BLACK_KING.index, ChessPieceType.KING, ChessSide.BLACK);
      ChessPiece whiteKing = new ChessPiece("" + ChessPieceKey.WHITE_KING.code + ChessPieceKey.WHITE_KING.index, ChessPieceType.KING, ChessSide.WHITE);
      
      chessBoard.getBoardCell(ChessBoardPosition.C2).placePiece(blackKing);
      chessBoard.getBoardCell(ChessBoardPosition.E1).placePiece(whiteKing);
      
      //showBoard(chessBoard);
      //Thread.sleep(200000);
      
      ChessSearchResponse moveResult = searchAlgorithm.searchBoard(chessBoard, ChessSide.BLACK);
      
      assertEquals(moveResult.chessMove.from, ChessBoardPosition.A8);
      assertEquals(moveResult.chessMove.to, ChessBoardPosition.A1);   */   
   }
}
