package com.zuooh.chess.engine.search.tree;

import java.util.Collections;

import junit.framework.TestCase;

import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.score.ChessScoreEvaluation;
import com.zuooh.chess.engine.score.ChessSimpleScoreEvaluation;
import com.zuooh.chess.engine.score.ChessTaperedScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchAlgorithm;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchFeature;
import com.zuooh.chess.engine.search.ChessSearchResponse;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.chess.engine.search.tree.ChessFutilityPruneSearchAlgorithm;
import com.zuooh.chess.engine.search.tree.ChessQuiescenceSearchAlgorithm;
import com.zuooh.chess.engine.search.tree.ChessTranspositionTableSearchAlgorithm;

/*
Rather than computing the hash for the entire board every time, as the pseudocode above does, the hash value of a 
board can be updated simply by XORing out the bitstring(s) for positions that have changed, and XORing in the bitstrings 
for the new positions. For instance, if a pawn on a chessboard square is replaced by a rook from another square, the 
resulting position would be produced by XORing the existing hash with the bitstrings for:

 'pawn at this square'      (XORing out the pawn at this square)
 'rook at this square'      (XORing in the rook at this square)
 'rook at source square'    (XORing out the rook at the source square)
 'nothing at source square' (XORing in nothing at the source square).
 
This makes Zobrist hashing very efficient for traversing a game tree.

In computer go, this technique is also used for superko detection.
 */
public class SearchAlgorithmDepthTest extends TestCase {   
   
   public static void main(String[] list) throws Exception {
      SearchAlgorithmDepthTest test = new SearchAlgorithmDepthTest();
      test.setUp();
      test.testTranspositionTable();
   }
   
   private static final String[][] BOARD = {
   { "  ", "  ", "  ", "  ", "bR", "  ", "b#", "  " },
   { "  ", "  ", "  ", "  ", "bB", "bP", "bP", "bP" },
   { "bP", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "bP", "bP", "  ", "wP", "  ", "wK", "  " },
   { "  ", "  ", "bK", "  ", "  ", "  ", "bB", "  " },
   { "  ", "  ", "wK", "bR", "  ", "  ", "  ", "  " },
   { "wP", "wP", "  ", "  ", "  ", "wP", "wP", "wP" },
   { "wR", "  ", "wB", "  ", "wR", "  ", "w#", "  " }};
   
   public void testTranspositionTable() throws InterruptedException {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      
      for(int i = 0; i < 2; i++) {
         //ChessSearchAlgorithm searchAlgo = createQuiescentSearch(30, 20000);
         //ChessSearchAlgorithm searchAlgo = createTranspositionTableSearch(30, 60000);
         ChessSearchAlgorithm searchAlgo = createFutilityPruneSearch(30, 60000);           
        // ChessSearchMove quiescentResult = quiescent.searchBoard(chessBoard, ChessPlayDirection.NORTH);
         ChessSearchResponse result = searchAlgo.searchBoard(chessBoard, ChessSide.WHITE); // <--- TRANSPOSITION IS TOO SLOW!!!
         
        // System.err.println(quiescentResult);
        // System.err.println();
        // System.err.println();
         System.err.println(result);
         System.err.println();
         System.err.println();
      }
   }
   
   public ChessSearchAlgorithm createQuiescentSearch(int searchDepth, long timeLimit) {
      ChessSearchCriteria searchCriteria = new ChessSearchCriteria(Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, searchDepth, timeLimit);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessTaperedScoreStrategy scoreStrategy = new ChessTaperedScoreStrategy(scoreEvaluation);
      ChessSearchTracer searchTracer = new ChessSearchTracer();
      
      return new ChessQuiescenceSearchAlgorithm(searchCriteria, scoreStrategy, searchTracer);
   }
   
   public ChessSearchAlgorithm createTranspositionTableSearch(int searchDepth, long timeLimit) {
      ChessSearchCriteria searchCriteria = new ChessSearchCriteria(Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, searchDepth, timeLimit);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessTaperedScoreStrategy scoreStrategy = new ChessTaperedScoreStrategy(scoreEvaluation);
      ChessSearchTracer searchTracer = new ChessSearchTracer();
      
      return new ChessTranspositionTableSearchAlgorithm(searchCriteria, scoreStrategy, searchTracer);
   }

   public ChessSearchAlgorithm createFutilityPruneSearch(int searchDepth, long timeLimit) {
      ChessSearchCriteria searchCriteria = new ChessSearchCriteria(Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, searchDepth, timeLimit);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessTaperedScoreStrategy scoreStrategy = new ChessTaperedScoreStrategy(scoreEvaluation);
      ChessSearchTracer searchTracer = new ChessSearchTracer();
      
      return new ChessFutilityPruneSearchAlgorithm(searchCriteria, scoreStrategy, searchTracer);
   }
}
