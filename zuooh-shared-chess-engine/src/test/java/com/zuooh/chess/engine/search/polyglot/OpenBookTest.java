package com.zuooh.chess.engine.search.polyglot;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class OpenBookTest extends TestCase {
   
   private static final String[][] BOARD = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};

   public void testOpenBook() throws Exception {
     /* File file = new File("C:\\Work\\development\\bitbucket\\chess\\zuooh-shared-chess-engine\\src\\test\\java\\com\\zuooh\\chess\\engine\\search\\polyglot\\performance.bin");
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ByteArrayOutputStream buffer = new ByteArrayOutputStream();
      FileInputStream input = new FileInputStream(file);
      byte[] chunk = new byte[8192];
      int count = 0;
      
      while((count = input.read(chunk)) !=-1) {
         buffer.write(chunk, 0, count);
      }
      input.close();
      byte[] bookData = buffer.toByteArray();
      ChessBookFile bookFile = new ChessMemoryBookFile(bookData);
      //RandomAccessFile raf = new RandomAccessFile(file, "r");
      //RandomAccessBookFile bookFile = new RandomAccessBookFile(raf);
      ChessBook openBook = new ChessBookReader(bookFile, false);
      
      ChessSide nextMove = ChessSide.WHITE;
      
      for(int i = 0; i < 100; i++) {
         ChessMove move = openBook.createMove(chessBoard, nextMove);
         
         if(move == null) {
            break;
         }
         ChessBoardCell fromCell = chessBoard.getBoardCell(move.from);
         ChessBoardCell toCell = chessBoard.getBoardCell(move.to);
         ChessPiece piece = fromCell.getPiece();
         ChessPiece taken = toCell.getPiece();
         
         if(piece.key.type == ChessPieceType.KING) {
            if(piece.moveWasCastle(chessBoard, move.to)) {
               ChessBoardPosition castleFrom = null;
               ChessBoardPosition castleTo = null;

               if(piece.side == ChessSide.BLACK) {
                  if(move.to == ChessBoardPosition.G8) {
                     castleFrom = ChessBoardPosition.H8;
                     castleTo = ChessBoardPosition.F8;
                  }else if(move.to == ChessBoardPosition.C8) {
                     castleFrom = ChessBoardPosition.A8;
                     castleTo = ChessBoardPosition.D8;
                  }
               } else {
                  if(move.to == ChessBoardPosition.G1) {
                     castleFrom = ChessBoardPosition.H1;
                     castleTo = ChessBoardPosition.F1;
                  }else if(move.to == ChessBoardPosition.C1) {
                     castleFrom = ChessBoardPosition.A1;
                     castleTo = ChessBoardPosition.D1;
                  }
               }
               ChessBoardCell castleFromCell = chessBoard.getBoardCell(castleFrom);
               ChessBoardCell castleToCell = chessBoard.getBoardCell(castleTo);
               ChessPiece rook = castleFromCell.getPiece();
               
               castleToCell.placePiece(rook); // do the castle!!               
            }
         }
         if(taken == null) {
            toCell.placePiece(piece);
         } else {
            toCell.takePiece();
            toCell.placePiece(piece);
         }
         nextMove = nextMove.oppositeSide();         
         String changeText = ChessTextBoardDrawer.drawBoard(chessBoard);
         System.err.println();
         System.err.println("MOVE COUNT: "+chessBoard.getChangeCount());         
         System.err.println(changeText);
         Thread.sleep(2000);
      }*/
   }

}
