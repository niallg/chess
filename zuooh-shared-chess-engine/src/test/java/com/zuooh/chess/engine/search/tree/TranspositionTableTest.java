package com.zuooh.chess.engine.search.tree;

import java.util.Collections;
import java.util.concurrent.atomic.AtomicInteger;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchFeature;
import com.zuooh.chess.engine.search.ChessTranspositionType;
import com.zuooh.chess.engine.search.tree.ChessRootSearchBoard;
import com.zuooh.chess.engine.search.tree.ChessSearchBoard;
import com.zuooh.chess.engine.search.tree.ChessSearchBoardBuilder;
import com.zuooh.chess.engine.search.tree.ChessSearchNode;
import com.zuooh.chess.engine.search.tree.ChessTranspositionTable;

public class TranspositionTableTest extends TestCase {   

   private static final String[][] BOARD = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};


   public void testTranspositionTable() throws InterruptedException {
      ChessSearchCriteria searchCriteria = new ChessSearchCriteria(Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, Integer.MAX_VALUE);
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessSearchBoard moveBoard = new ChessRootSearchBoard(chessBoard, ChessSide.WHITE, 1);
      ChessTranspositionTable table = new ChessTranspositionTable();
      AtomicInteger nodeCounter = new AtomicInteger();
      ChessMove firstMove = new ChessMove(ChessBoardPosition.A2, ChessBoardPosition.A3, ChessSide.WHITE, 1);
      ChessSearchBoard firstEvaluation = ChessSearchBoardBuilder.makeMove(moveBoard, firstMove);
      
      table.createEntry(new ChessSearchNode(null, firstEvaluation, ChessSide.WHITE, nodeCounter, 0, 1, 1), ChessTranspositionType.EXACT, firstMove, 12);
      //table.createEntry(new ChessSearchNode(null, firstEvaluation, ChessPlayDirection.SOUTH, nodeCounter, 0, 1, 1), ChessTranspositionType.EXACT, 44);
      
      assertEquals(table.findEntry(new ChessSearchNode(null, firstEvaluation, ChessSide.WHITE, nodeCounter, 0, 1, 1)).getValue(), 12);
      //assertEquals(table.findEntry(new ChessSearchNode(null, firstEvaluation, ChessPlayDirection.SOUTH, nodeCounter, 0, 1, 1)).getValue(), 44);      
      
      ChessMove secondMove = new ChessMove(ChessBoardPosition.A7, ChessBoardPosition.A6, ChessSide.BLACK, 1);
      ChessSearchBoard secondEvaluation = ChessSearchBoardBuilder.makeMove(firstEvaluation, secondMove);
      
      ChessTextBoardDrawer.drawBoard(secondEvaluation.getBoard());
      
      assertNull(table.findEntry(new ChessSearchNode(null, secondEvaluation, ChessSide.WHITE, nodeCounter, 0, 1, 1)));
      assertNull(table.findEntry(new ChessSearchNode(null, secondEvaluation, ChessSide.BLACK, nodeCounter, 0, 1, 1)));
      
      //table.createEntry(new ChessSearchNode(null, secondEvaluation, ChessPlayDirection.NORTH, nodeCounter, 0, 1, 1), ChessTranspositionType.EXACT, 55);
      table.createEntry(new ChessSearchNode(null, secondEvaluation, ChessSide.BLACK, nodeCounter, 0, 1, 1), ChessTranspositionType.EXACT, secondMove, 66);
      
      //assertEquals(table.findEntry(new ChessSearchNode(null, secondEvaluation, ChessPlayDirection.NORTH, nodeCounter, 0, 1, 1)).getValue(), 55);
      assertEquals(table.findEntry(new ChessSearchNode(null, secondEvaluation, ChessSide.BLACK, nodeCounter, 0, 1, 1)).getValue(), 66);
      
      ChessMove thirdMove = new ChessMove(ChessBoardPosition.C2, ChessBoardPosition.C3, ChessSide.WHITE, 1);
      ChessMove fourthAndBestMove = new ChessMove(ChessBoardPosition.D7, ChessBoardPosition.D6, ChessSide.BLACK, 1);
      ChessSearchBoard thirdEvaluation = ChessSearchBoardBuilder.makeMove(secondEvaluation, thirdMove);
      
      ChessTextBoardDrawer.drawBoard(thirdEvaluation.getBoard());
      
      table.createEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 2, 2), ChessTranspositionType.EXACT, fourthAndBestMove, 255);
      //table.createEntry(new ChessSearchNode(null, thirdEvaluation, ChessPlayDirection.SOUTH, nodeCounter, 0, 2, 2), ChessTranspositionType.EXACT, 36);
      
      assertEquals(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 1, 1)).getValue(), 255);
      assertEquals(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 1, 1)).getMove(thirdEvaluation.getBoard(), ChessSide.BLACK).getFrom(), ChessBoardPosition.D7);
      assertEquals(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 1, 1)).getMove(thirdEvaluation.getBoard(), ChessSide.BLACK).getTo(), ChessBoardPosition.D6);           
      //assertEquals(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessPlayDirection.SOUTH, nodeCounter, 0, 1, 1)).getValue(), 36);
      assertEquals(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 2, 2)).getValue(), 255);
      //assertEquals(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessPlayDirection.SOUTH, nodeCounter, 0, 2, 2)).getValue(), 36);
      assertNull(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 3, 3)));
      assertNull(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.BLACK, nodeCounter, 0, 3, 3)));
      assertNotNull(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 3, 3), 1));
      assertNotNull(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.BLACK, nodeCounter, 0, 3, 3), 1));
      assertEquals(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 3, 3), 1).getMove(thirdEvaluation.getBoard(), ChessSide.BLACK).getFrom(), ChessBoardPosition.D7);
      assertEquals(table.findEntry(new ChessSearchNode(null, thirdEvaluation, ChessSide.WHITE, nodeCounter, 0, 3, 3), 1).getMove(thirdEvaluation.getBoard(), ChessSide.BLACK).getTo(), ChessBoardPosition.D6);       
   }
}
