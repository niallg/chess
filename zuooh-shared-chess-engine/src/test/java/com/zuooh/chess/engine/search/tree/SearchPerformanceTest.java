package com.zuooh.chess.engine.search.tree;

import java.util.Collections;

import junit.framework.TestCase;

import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.score.ChessScoreEvaluation;
import com.zuooh.chess.engine.score.ChessSimpleScoreEvaluation;
import com.zuooh.chess.engine.score.ChessTaperedScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchFeature;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.chess.engine.search.tree.ChessQuiescenceSearchAlgorithm;
import com.zuooh.chess.engine.search.tree.ChessTimeLimitSearchAlgorithm;
import com.zuooh.chess.engine.search.tree.ChessTranspositionTableSearchAlgorithm;

public class SearchPerformanceTest extends TestCase  {   
   
   public static void main(String[] list) throws Exception {
      SearchPerformanceTest test = new SearchPerformanceTest();
      test.setUp();
      test.testPerformance();
   }

   private static final String[][] BOARD = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};
   
   public void testPerformance() throws InterruptedException {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      
      for(int i = 0; i < 7; i++) {
         ChessTimeLimitSearchAlgorithm quiescent = createQuiescentSearch(20, 20000);
         ChessTimeLimitSearchAlgorithm transpositionTable = createTranspositionTableSearch(20, 20000);
         long startTime = System.currentTimeMillis();
         //int quiescentNodeCount = quiescent.testPerformance(chessBoard, ChessSide.WHITE, i);
         long afterQuiescentTime = System.currentTimeMillis();
         int transpositionNodeCount = transpositionTable.testPerformance(chessBoard, ChessSide.WHITE, i);
         long afterTranspositionTime = System.currentTimeMillis();
         
         //System.err.println("depth="+i+" type=quiescent time=" +(afterQuiescentTime-startTime)+" nodes="+quiescentNodeCount);
         System.err.println("depth="+i+" type=transposition time=" +(afterTranspositionTime-afterQuiescentTime)+" nodes="+transpositionNodeCount);
         System.err.println();
         System.err.println();
      }
   }
   
   public ChessTimeLimitSearchAlgorithm createQuiescentSearch(int searchDepth, long timeLimit) {
      ChessSearchCriteria searchCriteria = new ChessSearchCriteria(Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, searchDepth, timeLimit);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessTaperedScoreStrategy scoreStrategy = new ChessTaperedScoreStrategy(scoreEvaluation);
      ChessSearchTracer searchTracer = new ChessSearchTracer();
      
      return new ChessQuiescenceSearchAlgorithm(searchCriteria, scoreStrategy, searchTracer);
   }
   
   public ChessTimeLimitSearchAlgorithm createTranspositionTableSearch(int searchDepth, long timeLimit) {
      ChessSearchCriteria searchCriteria = new ChessSearchCriteria(Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, searchDepth, timeLimit);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessTaperedScoreStrategy scoreStrategy = new ChessTaperedScoreStrategy(scoreEvaluation);
      ChessSearchTracer searchTracer = new ChessSearchTracer();
      
      return new ChessTranspositionTableSearchAlgorithm(searchCriteria, scoreStrategy, searchTracer);
   }

}
