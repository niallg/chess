package com.zuooh.chess.engine.search.tree;

import junit.framework.TestCase;

public class EnsureEnPassantIsEvaluatedTest extends TestCase {

   private static final String[][] BOARD = {
      // A     B     C     D     E     F     G    H
     { "wQ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 8
     { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 7
     { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 6
     { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },  // 5
     { "  ", "bP", "  ", "  ", "  ", "  ", "  ", "  " },  // 4
     { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "bP" },  // 3
     { "wP", "  ", "  ", "  ", "  ", "  ", "wP", "  " },  // 2
     { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }}; // 1

     public void testEnPassant() throws Exception {
       /* ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
        ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
        ChessMoveBoard moveEvaluation = new ChessNoMoveBoard(chessBoard);
        ChessPiece whitePawn = chessBoard.getPiece(ChessBoardPosition.A2);
        ChessPiece blackPawn = chessBoard.getPiece(ChessBoardPosition.B4);
        ChessBoardCell a4 = chessBoard.getBoardCell(ChessBoardPosition.A4);
        a4.placePiece(whitePawn);
        int changeCount = chessBoard.getChangeCount();
        assertEquals(chessBoard.getMoveCount(whitePawn), 1);

        ChessMove enPassent = new ChessMove(ChessBoardPosition.B4, ChessBoardPosition.A3, ChessSide.BLACK, changeCount+1);
        assertTrue("Move was enpasent by black", blackPawn.moveWasEnPassent(chessBoard, ChessBoardPosition.A3));*/
     }
}
