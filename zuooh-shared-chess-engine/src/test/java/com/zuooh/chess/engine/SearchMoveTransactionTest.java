package com.zuooh.chess.engine;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;

import junit.framework.TestCase;

public class SearchMoveTransactionTest extends TestCase {
   
   public void testTransaction() throws Exception {
      ChessSearchMoveTransaction whiteTransaction = new ChessSearchMoveTransaction(ChessSide.WHITE);
      ChessSearchMoveTransaction blackTransaction = new ChessSearchMoveTransaction(ChessSide.BLACK);      
      
      assertTrue(whiteTransaction.beginMove(null));
      assertFalse(whiteTransaction.beginMove(null));
      assertFalse(whiteTransaction.beginMove(null)); 
      assertTrue(whiteTransaction.abortMove());
      assertFalse(whiteTransaction.abortMove()); 
      assertTrue(whiteTransaction.beginMove(null));
      assertFalse(whiteTransaction.beginMove(null));
      assertNull(whiteTransaction.lastMove());
      assertTrue(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.A1, ChessBoardPosition.A2, ChessSide.WHITE, 1), null));
      assertFalse(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.A1, ChessBoardPosition.A2, ChessSide.WHITE, 1), null)); // move was already made
      assertFalse(whiteTransaction.beginMove(null));
      assertFalse(whiteTransaction.abortMove());  
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1))); // wrong direction
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1))); // wrong direction
      assertFalse(whiteTransaction.abortMove());
      assertTrue(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1)));
      assertTrue(whiteTransaction.abortMove());
      assertFalse(whiteTransaction.abortMove());
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1))); // wrong direction
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1))); // wrong direction
      assertTrue(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1), new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1), null));
      assertTrue(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1), new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1), new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.abortMove());
      assertFalse(whiteTransaction.abortMove());
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1))); // wrong direction
      assertTrue(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.H1, ChessBoardPosition.H1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1), new ChessMove(ChessBoardPosition.F1, ChessBoardPosition.F1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1), new ChessMove(ChessBoardPosition.C1, ChessBoardPosition.C1, ChessSide.BLACK, 1)));      
      assertTrue(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1), new ChessMove(ChessBoardPosition.H1, ChessBoardPosition.H1, ChessSide.BLACK, 1))); // correct move
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.D2, ChessBoardPosition.D2, ChessSide.WHITE, 1))); // wrong direction
      assertTrue(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.D1, ChessBoardPosition.D1, ChessSide.BLACK, 1)));
      assertFalse(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.D1, ChessBoardPosition.D1, ChessSide.BLACK, 1)));
      assertTrue(whiteTransaction.abortMove());
      assertFalse(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1), new ChessMove(ChessBoardPosition.D1, ChessBoardPosition.D1, ChessSide.BLACK, 1))); // aborted move
      assertTrue(whiteTransaction.beginMove(new ChessMove(ChessBoardPosition.D1, ChessBoardPosition.D1, ChessSide.BLACK, 1)));
      assertTrue(whiteTransaction.commitMove(new ChessMove(ChessBoardPosition.B1, ChessBoardPosition.B1, ChessSide.WHITE, 1), new ChessMove(ChessBoardPosition.D1, ChessBoardPosition.D1, ChessSide.BLACK, 1))); 
   }
   

}
