package com.zuooh.chess.engine;

import static com.zuooh.chess.ChessBoardPosition.A2;
import static com.zuooh.chess.ChessBoardPosition.A3;
import static com.zuooh.chess.ChessBoardPosition.A4;
import static com.zuooh.chess.ChessBoardPosition.A7;
import static com.zuooh.chess.ChessBoardPosition.B2;
import static com.zuooh.chess.ChessBoardPosition.B7;
import static com.zuooh.chess.ChessBoardPosition.C2;
import static com.zuooh.chess.ChessBoardPosition.C7;
import static com.zuooh.chess.ChessBoardPosition.D2;
import static com.zuooh.chess.ChessBoardPosition.D7;
import static com.zuooh.chess.ChessBoardPosition.D8;
import static com.zuooh.chess.ChessBoardPosition.E2;
import static com.zuooh.chess.ChessBoardPosition.E5;
import static com.zuooh.chess.ChessBoardPosition.E7;
import static com.zuooh.chess.ChessBoardPosition.F2;
import static com.zuooh.chess.ChessBoardPosition.F7;
import static com.zuooh.chess.ChessBoardPosition.G2;
import static com.zuooh.chess.ChessBoardPosition.G7;
import static com.zuooh.chess.ChessBoardPosition.H2;
import static com.zuooh.chess.ChessBoardPosition.H4;
import static com.zuooh.chess.ChessBoardPosition.H7;
import junit.framework.TestCase;

import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextMoveEvaluator;
import com.zuooh.chess.board.text.ChessTextPieceSet;

public class MoveEvaluationBoardTest extends TestCase {

   private static final String[][] BOARD = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};

   public void testEvaluationBoard() throws InterruptedException {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      
      System.err.println(chessSet);
      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));

      assertNotNull(chessBoard.getPiece(A7));
      assertNotNull(chessBoard.getPiece(B7));
      assertNotNull(chessBoard.getPiece(C7));
      assertNotNull(chessBoard.getPiece(D7));
      assertNotNull(chessBoard.getPiece(E7));
      assertNotNull(chessBoard.getPiece(F7));
      assertNotNull(chessBoard.getPiece(G7));
      assertNotNull(chessBoard.getPiece(H7));
      assertNotNull(chessBoard.getPiece(A2));
      assertNotNull(chessBoard.getPiece(B2));
      assertNotNull(chessBoard.getPiece(C2));
      assertNotNull(chessBoard.getPiece(D2));
      assertNotNull(chessBoard.getPiece(E2));
      assertNotNull(chessBoard.getPiece(F2));
      assertNotNull(chessBoard.getPiece(G2));
      assertNotNull(chessBoard.getPiece(H2));
      
      ChessBoard moveBoard1 = ChessTextMoveEvaluator.makeMove(chessBoard, "w:a2->a3");

      System.err.println(ChessTextBoardDrawer.drawBoard(moveBoard1));
      
      assertNull(moveBoard1.getPiece(A2)); 
      assertNotNull(moveBoard1.getPiece(A3));       
      assertNull(moveBoard1.getPiece(A2));
      assertNotNull(moveBoard1.getPiece(B2));
      assertNotNull(moveBoard1.getPiece(C2));
      assertNotNull(moveBoard1.getPiece(D2));
      assertNotNull(moveBoard1.getPiece(E2));
      assertNotNull(moveBoard1.getPiece(F2));
      assertNotNull(moveBoard1.getPiece(G2));
      assertNotNull(moveBoard1.getPiece(H2));     
      assertNotNull(moveBoard1.getPiece(A7));
      assertNotNull(moveBoard1.getPiece(B7));
      assertNotNull(moveBoard1.getPiece(C7));
      assertNotNull(moveBoard1.getPiece(D7));
      assertNotNull(moveBoard1.getPiece(E7));
      assertNotNull(moveBoard1.getPiece(F7));
      assertNotNull(moveBoard1.getPiece(G7));
      assertNotNull(moveBoard1.getPiece(H7));  
      

      ChessBoard moveBoard2 = ChessTextMoveEvaluator.makeMove(moveBoard1, "b:e7->e5");

      System.err.println(ChessTextBoardDrawer.drawBoard(moveBoard2));
      
      assertNull(moveBoard2.getPiece(E7)); 
      assertNotNull(moveBoard2.getPiece(E5));    
      assertNull(moveBoard2.getPiece(A2));
      assertNotNull(moveBoard2.getPiece(B2));
      assertNotNull(moveBoard2.getPiece(C2));
      assertNotNull(moveBoard2.getPiece(D2));
      assertNotNull(moveBoard2.getPiece(E2));
      assertNotNull(moveBoard2.getPiece(F2));
      assertNotNull(moveBoard2.getPiece(G2));
      assertNotNull(moveBoard2.getPiece(H2));    
      assertNotNull(moveBoard2.getPiece(A7));
      assertNotNull(moveBoard2.getPiece(B7));
      assertNotNull(moveBoard2.getPiece(C7));
      assertNotNull(moveBoard2.getPiece(D7));
      assertNull(moveBoard2.getPiece(E7));
      assertNotNull(moveBoard2.getPiece(F7));
      assertNotNull(moveBoard2.getPiece(G7));
      assertNotNull(moveBoard2.getPiece(H7));      

      ChessBoard moveBoard3 = ChessTextMoveEvaluator.makeMove(moveBoard2, "w:a3->a4");

      System.err.println(ChessTextBoardDrawer.drawBoard(moveBoard3));
      
      assertNull(moveBoard3.getPiece(A3)); 
      assertNotNull(moveBoard3.getPiece(A4)); 
      assertNull(moveBoard3.getPiece(A2));
      assertNotNull(moveBoard3.getPiece(B2));
      assertNotNull(moveBoard3.getPiece(C2));
      assertNotNull(moveBoard3.getPiece(D2));
      assertNotNull(moveBoard3.getPiece(E2));
      assertNotNull(moveBoard3.getPiece(F2));
      assertNotNull(moveBoard3.getPiece(G2));
      assertNotNull(moveBoard3.getPiece(H2));     
      assertNotNull(moveBoard3.getPiece(A7));
      assertNotNull(moveBoard3.getPiece(B7));
      assertNotNull(moveBoard3.getPiece(C7));
      assertNotNull(moveBoard3.getPiece(D7));
      assertNull(moveBoard3.getPiece(E7));
      assertNotNull(moveBoard3.getPiece(F7));
      assertNotNull(moveBoard3.getPiece(G7));
      assertNotNull(moveBoard3.getPiece(H7));
      assertNotNull(moveBoard3.getPiece(A7)); 
      
      ChessBoard moveBoard4 = ChessTextMoveEvaluator.makeMove(moveBoard3, "b:d8->h4");

      System.err.println(ChessTextBoardDrawer.drawBoard(moveBoard4));
      
      assertNull(moveBoard4.getPiece(D8)); 
      assertNotNull(moveBoard4.getPiece(H4)); 
      assertNull(moveBoard4.getPiece(A3)); 
      assertNotNull(moveBoard4.getPiece(A4)); 
      assertNull(moveBoard4.getPiece(A2));
      assertNotNull(moveBoard4.getPiece(B2));
      assertNotNull(moveBoard4.getPiece(C2));
      assertNotNull(moveBoard4.getPiece(D2));
      assertNotNull(moveBoard4.getPiece(E2));
      assertNotNull(moveBoard4.getPiece(F2));
      assertNotNull(moveBoard4.getPiece(G2));
      assertNotNull(moveBoard4.getPiece(H2));     
      assertNotNull(moveBoard4.getPiece(A7));
      assertNotNull(moveBoard4.getPiece(B7));
      assertNotNull(moveBoard4.getPiece(C7));
      assertNotNull(moveBoard4.getPiece(D7));
      assertNull(moveBoard4.getPiece(E7));
      assertNotNull(moveBoard4.getPiece(F7));
      assertNotNull(moveBoard4.getPiece(G7));
      assertNotNull(moveBoard4.getPiece(H7));
      assertNotNull(moveBoard4.getPiece(A7)); 
   }
}
