package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessNullMove;
import com.zuooh.chess.engine.search.tree.ChessSearchBoard;
import com.zuooh.chess.engine.search.tree.ChessSearchBoardBuilder;

public class ChessTextMoveEvaluator {

   public static ChessSearchBoard makeMove(ChessSearchBoard moveBoard, String move) {
      ChessBoard chessBoard = moveBoard.getBoard();
      ChessMove moveMade = parseMove(chessBoard, move);
      
      return ChessSearchBoardBuilder.makeMove(moveBoard, moveMade);      
   }
   
   public static ChessMove parseMove(ChessBoard chessBoard, String move) {
      ChessSide moveSide = ChessSide.WHITE;
      String moveToken = move.toUpperCase();
      
      if(moveToken.startsWith("BLACK:")) {
         moveSide = ChessSide.BLACK;
         moveToken = moveToken.substring(6);
      } else if(moveToken.startsWith("WHITE:")) {
         moveSide = ChessSide.WHITE;
         moveToken = moveToken.substring(6);
      } else if(moveToken.startsWith("B:")) {
         moveSide = ChessSide.BLACK;
         moveToken = moveToken.substring(2);
      } else if(moveToken.startsWith("W:")) {
         moveSide = ChessSide.WHITE;
         moveToken = moveToken.substring(2);
      } else {
         throw new IllegalArgumentException("Move '" + move + "' must be in the format color:from->to");
      }
      int changeCount = chessBoard.getChangeCount();
      
      if(!moveToken.equals("NULL")) {
         String[] movePositions = moveToken.split("->");
         
         if(movePositions.length != 2) {
            throw new IllegalArgumentException("Move '" + move + "' must be in the format color:from->to");
         }
         String fromCode = movePositions[0].trim();
         String toCode = movePositions[1].trim();
         ChessBoardPosition from = ChessBoardPosition.at(fromCode);
         ChessBoardPosition to = ChessBoardPosition.at(toCode);
         ChessPiece piece = chessBoard.getPiece(from);   
         
         if(piece.side != moveSide) {
            throw new IllegalStateException("Move '" + move + "' trying to move piece " + piece);
         }      
         return new ChessMove(from, to, piece.side, changeCount +  1);
      }
      return new ChessNullMove(moveSide, changeCount);
   }   
}
