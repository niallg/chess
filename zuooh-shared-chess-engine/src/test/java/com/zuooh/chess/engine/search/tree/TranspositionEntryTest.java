package com.zuooh.chess.engine.search.tree;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.engine.search.ChessTranspositionEntry;
import com.zuooh.chess.engine.search.ChessTranspositionType;

public class TranspositionEntryTest extends TestCase {
   
   public void testType() {
      ChessTranspositionEntry entry = new ChessTranspositionEntry(ChessTranspositionType.EXACT, ChessBoardPosition.A1, ChessBoardPosition.G2, 0xdddL, 2233445, 210, (byte)12);
      
      assertEquals(entry.getType(), ChessTranspositionType.EXACT);
      assertEquals(entry.getFrom(), ChessBoardPosition.A1);      
      assertEquals(entry.getTo(), ChessBoardPosition.G2);
      assertEquals(entry.getBoardHash(), 0xdddL);
      assertEquals(entry.getValue(),2233445);
      assertEquals(entry.getVersion(), (byte)12);
      
      entry = new ChessTranspositionEntry(ChessTranspositionType.LOWER_BOUND, ChessBoardPosition.H1, ChessBoardPosition.H2, 0xdddL, 1111, 0, (byte)0);
      
      assertEquals(entry.getType(), ChessTranspositionType.LOWER_BOUND);
      assertEquals(entry.getFrom(), ChessBoardPosition.H1);      
      assertEquals(entry.getTo(), ChessBoardPosition.H2);
      assertEquals(entry.getBoardHash(), 0xdddL);
      assertEquals(entry.getValue(), 1111);
      assertEquals(entry.getVersion(), (byte)0);
   }

}
