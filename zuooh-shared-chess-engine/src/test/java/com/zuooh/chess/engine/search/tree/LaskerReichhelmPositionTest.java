package com.zuooh.chess.engine.search.tree;
import java.util.Collections;

import junit.framework.TestCase;

import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.score.ChessScoreEvaluation;
import com.zuooh.chess.engine.score.ChessSimpleScoreEvaluation;
import com.zuooh.chess.engine.score.ChessSimpleScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchFeature;
import com.zuooh.chess.engine.search.ChessSearchResponse;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.chess.engine.search.tree.ChessTranspositionTableSearchAlgorithm;

//http://www.sigmachess.com/_usersmanual/ManualData/TransTables.html
//https://chessprogramming.wikispaces.com/Lasker-Reichhelm+Position
public class LaskerReichhelmPositionTest extends TestCase {
   
   private static final String[][] BOARD = {
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "b#", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "bP", "  ", "  ", "  ", "  " },
   { "bP", "  ", "  ", "wP", "  ", "bP", "  ", "  " },
   { "wP", "  ", "  ", "wP", "  ", "wP", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "w#", "  ", "  ", "  ", "  ", "  ", "  ", "  " }};


   public void testWhiteMoveToWin() throws InterruptedException {
      ChessSearchCriteria searchCriteria = new ChessSearchCriteria(Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, 30, 20000);
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessSimpleScoreStrategy scoreStrategy = new ChessSimpleScoreStrategy(scoreEvaluation);
      ChessSearchTracer searchTracer = new ChessSearchTracer();
      ChessTranspositionTableSearchAlgorithm searchAlgorithm = new ChessTranspositionTableSearchAlgorithm(searchCriteria, scoreStrategy, searchTracer);
      
      System.err.println(ChessTextBoardDrawer.drawBoard(chessBoard));
      
      ChessSearchResponse searchMove = searchAlgorithm.searchBoard(chessBoard, ChessSide.WHITE);
      System.err.println(searchMove);
   }

}
