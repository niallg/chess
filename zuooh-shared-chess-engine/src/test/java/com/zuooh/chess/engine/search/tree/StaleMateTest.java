package com.zuooh.chess.engine.search.tree;

import java.util.Collections;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.score.ChessScoreEvaluation;
import com.zuooh.chess.engine.score.ChessSimpleScoreEvaluation;
import com.zuooh.chess.engine.score.ChessTaperedScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchAlgorithm;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchFeature;
import com.zuooh.chess.engine.search.ChessSearchResponse;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.chess.engine.search.tree.ChessTranspositionTableSearchAlgorithm;

public class StaleMateTest extends TestCase {
   
   private static final String[][] BOARD = {
   { "  ", "  ", "  ", "  ", "wR", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "b#" },
   { "  ", "  ", "  ", "  ", "  ", "wR", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "wP", "wP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "wP", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "wP", "  ", "w#", "  ", "wP", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " }};   

   
   public void testStaleMate() throws InterruptedException {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessSearchCriteria searchCriteria = new ChessSearchCriteria(Collections.singleton(ChessSearchFeature.EVERYTHING), ChessPieceType.QUEEN, 10000, 20000);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessTaperedScoreStrategy scoreStrategy = new ChessTaperedScoreStrategy(scoreEvaluation);
      ChessSearchTracer searchTracer = new ChessSearchTracer();
      ChessSearchAlgorithm searchAlgorithm = new ChessTranspositionTableSearchAlgorithm(searchCriteria, scoreStrategy, searchTracer);      
      ChessSearchResponse searchMove = searchAlgorithm.searchBoard(chessBoard, ChessSide.WHITE); // <--- TRANSPOSITION IS TOO SLOW!!!
      
      System.err.println(searchMove.getResult().getMove());
      
      assertNotNull(searchMove.getResult().getMove());
      assertTrue(searchMove.getResult().getMove().getTo() != ChessBoardPosition.H6); // no stale mate   
   }
}
