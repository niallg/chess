package com.zuooh.chess.engine.score;

import junit.framework.TestCase;


import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.score.ChessScoreEvaluation;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.score.ChessSimpleScoreEvaluation;
import com.zuooh.chess.engine.score.ChessSimpleScoreStrategy;

public class SimpleScoreStrategyTest extends TestCase {

   private static final String[][] BOARD = {
   { "  ", "  ", "  ", "  ", "b#", "  ", "  ", "  " },
   { "  ", "  ", "bP", "bP", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "w#", "  ", "  ", "  " },
   };

   public void testScore() throws Exception {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      ChessScoreEvaluation scoreEvaluation = new ChessSimpleScoreEvaluation();
      ChessScoreStrategy scoreStrategy = new ChessSimpleScoreStrategy(scoreEvaluation);
      int blackScore = scoreStrategy.score(chessBoard, ChessSide.BLACK);
      int whiteScore = scoreStrategy.score(chessBoard, ChessSide.WHITE);

      System.err.printf("black=%s white=%s", blackScore, whiteScore);

      assertEquals(blackScore, -whiteScore);
   }
}
