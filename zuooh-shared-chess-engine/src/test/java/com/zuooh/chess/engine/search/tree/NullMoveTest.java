package com.zuooh.chess.engine.search.tree;

import junit.framework.TestCase;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardHashCalculator;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.board.text.ChessTextBoardDrawer;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.search.tree.ChessRootSearchBoard;
import com.zuooh.chess.engine.search.tree.ChessSearchBoard;

public class NullMoveTest extends TestCase {

   
   public static void main(String[] list) throws Exception {
      NullMoveTest test = new NullMoveTest();
      test.setUp();
      test.testNullMove();
   }

   private static final String[][] BOARD = {
   { "bR", "bK", "bB", "bQ", "b#", "bB", "bK", "bR" },
   { "bP", "bP", "bP", "bP", "bP", "bP", "bP", "bP" },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "  ", "  ", "  ", "  ", "  ", "  ", "  ", "  " },
   { "wP", "wP", "wP", "wP", "wP", "wP", "wP", "wP" },
   { "wR", "wK", "wB", "wQ", "w#", "wB", "wK", "wR" }};

   public void testNullMove() throws InterruptedException {
      ChessPieceSet chessSet = new ChessTextPieceSet(BOARD);
      ChessBoard chessBoard = new ChessPieceSetBoard(chessSet);
      long beginHash = ChessBoardHashCalculator.calculateBoardHash(chessBoard, ChessSide.BLACK);    
      ChessRootSearchBoard moveBoard = new ChessRootSearchBoard(chessBoard, ChessSide.WHITE, beginHash);
      
      assertEquals(beginHash, moveBoard.getBoardHash());
      
      System.err.print(ChessTextBoardDrawer.drawBoard(moveBoard.getBoard()));
      
      ChessSearchBoard moveBoard1 = ChessTextMoveEvaluator.makeMove(moveBoard, "w:a2->a3");
      ChessSearchBoard moveBoard2 = ChessTextMoveEvaluator.makeMove(moveBoard1, "b:g7->g6");
      ChessSearchBoard moveBoard3 = ChessTextMoveEvaluator.makeMove(moveBoard2, "w:d2->d3");
      ChessSearchBoard moveBoard4 = ChessTextMoveEvaluator.makeMove(moveBoard3, "b:h7->h5");      
      
      System.err.print(ChessTextBoardDrawer.drawBoard(moveBoard4.getBoard()));
      
      ChessSearchBoard moveBoard5 = ChessTextMoveEvaluator.makeMove(moveBoard4, "white:a3->a4"); 
      ChessSearchBoard moveBoard6 = ChessTextMoveEvaluator.makeMove(moveBoard5, "black:h5->h4");
      
      System.err.print(ChessTextBoardDrawer.drawBoard(moveBoard6.getBoard()));
      
      long whiteHash6 = ChessBoardHashCalculator.calculateBoardHash(moveBoard6.getBoard(), ChessSide.WHITE);
      long blackHash6 = ChessBoardHashCalculator.calculateBoardHash(moveBoard6.getBoard(), ChessSide.BLACK);
      
      System.err.println("BLACK:"+blackHash6);
      System.err.println("WHITE:"+whiteHash6);
      
      assertEquals(blackHash6, moveBoard6.getBoardHash()); // last move was made by black
      
      ChessSearchBoard moveBoard7 = ChessTextMoveEvaluator.makeMove(moveBoard6, "white:null");
      
      assertEquals(whiteHash6, moveBoard7.getBoardHash()); // last move was made by white and nothing else changed
      assertFalse(blackHash6 ==  moveBoard7.getBoardHash()); // last move was made by white and nothing else changed      
      
      System.err.print(ChessTextBoardDrawer.drawBoard(moveBoard7.getBoard()));
   }      
}
