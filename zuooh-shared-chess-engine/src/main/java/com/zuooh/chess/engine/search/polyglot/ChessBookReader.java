package com.zuooh.chess.engine.search.polyglot;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;

public class ChessBookReader implements ChessBook {

   /* How much space one entry takes, long+short+short+int */
   private static final int SPACE_FOR_ONE_ENTRY = 16;

   private final ChessBookEntryComparator entryComparator;
   private final ChessBookFile bookFile;
   private final SecureRandom random;
   private final boolean randomEntry;

   public ChessBookReader(ChessBookFile bookFile) {
      this(bookFile, true);
   }

   public ChessBookReader(ChessBookFile bookFile, boolean randomEntry) {
      this.entryComparator = new ChessBookEntryComparator();
      this.random = new SecureRandom();
      this.randomEntry = randomEntry;
      this.bookFile = bookFile;
   }

   @Override
   public synchronized ChessMove createMove(ChessBoard chessBoard, ChessSide nextMove) {
      long key = ChessBookKeyCalculator.computeKey(chessBoard, nextMove);      
      ChessBookEntry entry = searchForEntry(key);

      if (entry != null) {
         return convertToMove(chessBoard, nextMove, entry);
      }
      return null;
   }

   private synchronized ChessMove convertToMove(ChessBoard chessBoard, ChessSide nextMove, ChessBookEntry entry) {
      ChessBookPosition entryFrom = entry.getFromPosition();
      ChessBookPosition entryTo = entry.getToPosition();
      ChessBoardPosition toPosition = ChessBookConverter.convert(entryTo);
      ChessBoardPosition fromPosition = ChessBookConverter.convert(entryFrom);
      ChessPiece fromPiece = chessBoard.getPiece(fromPosition);
      int changeCount = chessBoard.getChangeCount();

      if (fromPiece.key.side == ChessSide.BLACK) {
         if (toPosition == ChessBoardPosition.H8 && fromPosition == ChessBoardPosition.E8) {
            return new ChessMove(ChessBoardPosition.E8, ChessBoardPosition.G8, ChessSide.BLACK, changeCount);
         }
         if (toPosition == ChessBoardPosition.A8 && fromPosition == ChessBoardPosition.E8) {
            return new ChessMove(ChessBoardPosition.E8, ChessBoardPosition.C8, ChessSide.BLACK, changeCount);
         }
      } else {
         if (toPosition == ChessBoardPosition.H1 && fromPosition == ChessBoardPosition.E1) {
            return new ChessMove(ChessBoardPosition.E1, ChessBoardPosition.G1, ChessSide.WHITE, changeCount);
         }
         if (toPosition == ChessBoardPosition.A1 && fromPosition == ChessBoardPosition.E1) {
            return new ChessMove(ChessBoardPosition.E1, ChessBoardPosition.C1, ChessSide.WHITE, changeCount);
         }
      }
      return new ChessMove(fromPosition, toPosition, nextMove, changeCount);
   }

   private synchronized ChessPieceType convertToPromotion(ChessBoard chessBoard, ChessSide nextMove, ChessBookEntry entry) {
      int promotion = (entry.move >> 12) & 0x7;

      if (promotion != 0) {
         if (promotion == 1) {
            return ChessPieceType.KNIGHT;
         } else if (promotion == 2) {
            return ChessPieceType.BISHOP;
         } else if (promotion == 3) {
            return ChessPieceType.ROOK;
         } else if (promotion == 4) {
            return ChessPieceType.QUEEN;
         }
      }
      return null;
   }

   private synchronized ChessBookEntry searchForEntry(long searchKey) {
      List<ChessBookEntry> potentialEntries = new ArrayList<ChessBookEntry>();
      long bookLength = bookFile.length();
      long seekPointLow = 0;
      long seekPointHigh = bookLength / SPACE_FOR_ONE_ENTRY;
      long seekPointMid = -1;
      int weightSum = 0;

      /*
       * Loop through the file, break when seekPointMid has already been used (which means the position was not found)
       */
      while (seekPointMid != ((seekPointLow + seekPointHigh) / 2) * SPACE_FOR_ONE_ENTRY) {

         /* Set the mid point */
         seekPointMid = ((seekPointLow + seekPointHigh) / 2) * SPACE_FOR_ONE_ENTRY;

         /* Start searching at the given mid point */
         ChessBookEntry entry = bookFile.read(seekPointMid);

         /*
          * A key was found, so use the seek point as base to find all moves connected to the same key (can be both after and before this seek point)
          */
         if (entry.hash == searchKey) {
            /* Start with adding the move at this seek point */
            potentialEntries.add(entry);
            weightSum += entry.weight;

            /* Loop backward and add moves */
            long tempSeek = seekPointMid;
            boolean moveFound = true;
            while (moveFound && tempSeek >= 0) {
               /* Move one entry back */
               tempSeek -= SPACE_FOR_ONE_ENTRY;
               entry = bookFile.read(tempSeek);

               /* Add if it is still the same key */
               if (entry.hash == searchKey) {
                  potentialEntries.add(entry);
                  weightSum += entry.weight;
               } else {
                  moveFound = false;
               }
            }

            /* Loop foward and add moves */
            tempSeek = seekPointMid;
            moveFound = true;
            while (moveFound && tempSeek < bookLength) {
               /* Move one entry forward */
               tempSeek += SPACE_FOR_ONE_ENTRY;
               entry = bookFile.read(tempSeek);

               /* Add if it is still the same key */
               if (entry.hash == searchKey) {
                  potentialEntries.add(entry);
                  weightSum += entry.weight;
               } else {
                  moveFound = false;
               }
            }

            /* All moves found so break out of the search */
            break;
         }

         /*
          * The key was not a match so calculate which way the search should go next. Since the long will go negative at too high numbers checks are made for this
          */
         if ((entry.hash < 0 && searchKey < 0) || (entry.hash > 0 && searchKey > 0)) {
            if (entry.hash < searchKey) {
               seekPointLow = seekPointMid / SPACE_FOR_ONE_ENTRY;
            } else {
               seekPointHigh = seekPointMid / SPACE_FOR_ONE_ENTRY;
            }
         } else if (entry.hash > 0 && searchKey < 0) {
            seekPointLow = seekPointMid / SPACE_FOR_ONE_ENTRY;
         } else {
            seekPointHigh = seekPointMid / SPACE_FOR_ONE_ENTRY;
         }
      }
      int matchCount = potentialEntries.size();

      /* Select a move and return it */
      if (matchCount > 0) {
         if (randomEntry) {
            return selectRandomEntry(potentialEntries, weightSum);
         }
         if (matchCount > 1) {
            Collections.sort(potentialEntries, entryComparator);
         }
         return potentialEntries.get(0);
      }
      return null;
   }

   private ChessBookEntry selectRandomEntry(List<ChessBookEntry> potentialEntries, int weightSum) { // http://hardy.uhasselt.be/Toga/book_format.html
      /* Get a random number between 0 and 1 */
      double randomNumber = random.nextDouble();
      double totalWeight = 0;

      /* Loop through all the moves */
      for (ChessBookEntry entry : potentialEntries) {
         double entryWeight = entry.weight;

         /* Each loop add the total weight */
         totalWeight += entryWeight / (double) weightSum;

         /*
          * If the randomnumber is lower than the current total weight pick this move by breaking and leaving i as the index
          */
         if (randomNumber < totalWeight) {
            return entry;
         }
      }
      return potentialEntries.get(0);
   }
}
