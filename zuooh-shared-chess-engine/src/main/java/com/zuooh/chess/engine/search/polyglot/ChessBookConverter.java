package com.zuooh.chess.engine.search.polyglot;

import static com.zuooh.chess.ChessBoardPosition.A1;
import static com.zuooh.chess.ChessBoardPosition.A2;
import static com.zuooh.chess.ChessBoardPosition.A3;
import static com.zuooh.chess.ChessBoardPosition.A4;
import static com.zuooh.chess.ChessBoardPosition.A5;
import static com.zuooh.chess.ChessBoardPosition.A6;
import static com.zuooh.chess.ChessBoardPosition.A7;
import static com.zuooh.chess.ChessBoardPosition.A8;
import static com.zuooh.chess.ChessBoardPosition.B1;
import static com.zuooh.chess.ChessBoardPosition.B2;
import static com.zuooh.chess.ChessBoardPosition.B3;
import static com.zuooh.chess.ChessBoardPosition.B4;
import static com.zuooh.chess.ChessBoardPosition.B5;
import static com.zuooh.chess.ChessBoardPosition.B6;
import static com.zuooh.chess.ChessBoardPosition.B7;
import static com.zuooh.chess.ChessBoardPosition.B8;
import static com.zuooh.chess.ChessBoardPosition.C1;
import static com.zuooh.chess.ChessBoardPosition.C2;
import static com.zuooh.chess.ChessBoardPosition.C3;
import static com.zuooh.chess.ChessBoardPosition.C4;
import static com.zuooh.chess.ChessBoardPosition.C5;
import static com.zuooh.chess.ChessBoardPosition.C6;
import static com.zuooh.chess.ChessBoardPosition.C7;
import static com.zuooh.chess.ChessBoardPosition.C8;
import static com.zuooh.chess.ChessBoardPosition.D1;
import static com.zuooh.chess.ChessBoardPosition.D2;
import static com.zuooh.chess.ChessBoardPosition.D3;
import static com.zuooh.chess.ChessBoardPosition.D4;
import static com.zuooh.chess.ChessBoardPosition.D5;
import static com.zuooh.chess.ChessBoardPosition.D6;
import static com.zuooh.chess.ChessBoardPosition.D7;
import static com.zuooh.chess.ChessBoardPosition.D8;
import static com.zuooh.chess.ChessBoardPosition.E1;
import static com.zuooh.chess.ChessBoardPosition.E2;
import static com.zuooh.chess.ChessBoardPosition.E3;
import static com.zuooh.chess.ChessBoardPosition.E4;
import static com.zuooh.chess.ChessBoardPosition.E5;
import static com.zuooh.chess.ChessBoardPosition.E6;
import static com.zuooh.chess.ChessBoardPosition.E7;
import static com.zuooh.chess.ChessBoardPosition.E8;
import static com.zuooh.chess.ChessBoardPosition.F1;
import static com.zuooh.chess.ChessBoardPosition.F2;
import static com.zuooh.chess.ChessBoardPosition.F3;
import static com.zuooh.chess.ChessBoardPosition.F4;
import static com.zuooh.chess.ChessBoardPosition.F5;
import static com.zuooh.chess.ChessBoardPosition.F6;
import static com.zuooh.chess.ChessBoardPosition.F7;
import static com.zuooh.chess.ChessBoardPosition.F8;
import static com.zuooh.chess.ChessBoardPosition.G1;
import static com.zuooh.chess.ChessBoardPosition.G2;
import static com.zuooh.chess.ChessBoardPosition.G3;
import static com.zuooh.chess.ChessBoardPosition.G4;
import static com.zuooh.chess.ChessBoardPosition.G5;
import static com.zuooh.chess.ChessBoardPosition.G6;
import static com.zuooh.chess.ChessBoardPosition.G7;
import static com.zuooh.chess.ChessBoardPosition.G8;
import static com.zuooh.chess.ChessBoardPosition.H1;
import static com.zuooh.chess.ChessBoardPosition.H2;
import static com.zuooh.chess.ChessBoardPosition.H3;
import static com.zuooh.chess.ChessBoardPosition.H4;
import static com.zuooh.chess.ChessBoardPosition.H5;
import static com.zuooh.chess.ChessBoardPosition.H6;
import static com.zuooh.chess.ChessBoardPosition.H7;
import static com.zuooh.chess.ChessBoardPosition.H8;
import static com.zuooh.chess.ChessSide.BLACK;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;

public class ChessBookConverter {
   
   private static final ChessBoardPosition[][] POSITIONS = {
   {A1,A2,A3,A4,A5,A6,A7,A8},
   {B1,B2,B3,B4,B5,B6,B7,B8},   
   {C1,C2,C3,C4,C5,C6,C7,C8},   
   {D1,D2,D3,D4,D5,D6,D7,D8},   
   {E1,E2,E3,E4,E5,E6,E7,E8},   
   {F1,F2,F3,F4,F5,F6,F7,F8},   
   {G1,G2,G3,G4,G5,G6,G7,G8},   
   {H1,H2,H3,H4,H5,H6,H7,H8}};
   
   public static ChessBookPosition convert(ChessBoardPosition position) {
      return new ChessBookPosition(position.x, 7 - position.y);
   }
   
   public static ChessBoardPosition convert(ChessBookPosition position) {
      return POSITIONS[position.file][position.row];
   }
   
   public static ChessBoardPosition convert(int file, int row) {
      return POSITIONS[file][row];
   }

   public static int convert(ChessPiece piece) {
      ChessPieceKey key = piece.key;
      ChessSide side = key.side;
      ChessPieceType type = key.type;
      
      if(side == BLACK) {
         return type.index * 2;
      }
      return (type.index * 2) + 1;
   }
         
}
