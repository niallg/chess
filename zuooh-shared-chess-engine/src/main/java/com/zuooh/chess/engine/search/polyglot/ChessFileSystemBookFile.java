package com.zuooh.chess.engine.search.polyglot;

import com.zuooh.common.FileSystem;

public class ChessFileSystemBookFile implements ChessBookFile {
   
   private FileSystem fileSystem;
   private ChessBookFile bookFile;
   private String fileName;
   
   public ChessFileSystemBookFile(FileSystem fileSystem, String fileName) {
      this.fileSystem = fileSystem;
      this.fileName = fileName;      
   }   

   @Override
   public synchronized ChessBookEntry read(long offset) {
      if(bookFile == null) {
         byte[] bookData = fileSystem.loadFile(fileName);
         
         if(bookData != null) {
            bookFile = new ChessMemoryBookFile(bookData);
         }         
      }
      return bookFile.read(offset);
   }

   @Override
   public synchronized long length() {
      if(bookFile == null) {
         byte[] bookData = fileSystem.loadFile(fileName);
         
         if(bookData != null) {
            bookFile = new ChessMemoryBookFile(bookData);
         }         
      }
      return bookFile.length();
   }

}
