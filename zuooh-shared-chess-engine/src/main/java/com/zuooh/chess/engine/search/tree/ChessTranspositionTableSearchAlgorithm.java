package com.zuooh.chess.engine.search.tree;

import static com.zuooh.chess.ChessMoveType.NORMAL;
import static com.zuooh.chess.engine.score.ChessScoreEvaluation.LOWEST_POSSIBLE_SCORE;
import static com.zuooh.chess.engine.search.ChessSearchPhase.SEARCH_CHECK_MATE;
import static com.zuooh.chess.engine.search.ChessSearchPhase.TRANSPOSITION_TABLE;
import static com.zuooh.chess.engine.search.ChessTranspositionType.EXACT;
import static com.zuooh.chess.engine.search.ChessTranspositionType.LOWER_BOUND;
import static com.zuooh.chess.engine.search.ChessTranspositionType.UPPER_BOUND;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchPhase;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.chess.engine.search.ChessTranspositionEntry;
import com.zuooh.chess.engine.search.ChessTranspositionType;

//http://en.wikipedia.org/wiki/Negamax
//http://en.wikipedia.org/wiki/Transposition_table
//http://en.wikipedia.org/wiki/Zobrist_hashing
public class ChessTranspositionTableSearchAlgorithm extends ChessQuiescenceSearchAlgorithm {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessTranspositionTableSearchAlgorithm.class);
   
   protected final ChessTranspositionTable moveTable;

   public ChessTranspositionTableSearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer) {
      this(searchCriteria, scoreStrategy, searchTracer, 600000);
   }
   
   public ChessTranspositionTableSearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer, long tableExpiry) {
      super(searchCriteria, scoreStrategy, searchTracer);
      this.moveTable = new ChessTranspositionTable(tableExpiry);// we should recycle this after every search!!!      
   }

   @Override
   protected ChessSearchNodeResult searchNode(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      int originalFloor = scoreFloor;
      
      if(checkNullMove(searchNode, scoreFloor, scoreCeiling)) { // allow a null move to be performed
         ChessSearchNode childNode = nodeBuilder.createChild(searchNode);
         ChessSearchNodeResult result = searchNode(childNode, -scoreCeiling, -scoreFloor);
         int score = -result.getScore();
         
         if(score >= scoreCeiling) {
            return new ChessSearchNodeResult(result.searchPhase, result.searchNode, null, score);           
         }         
      }  
      if(searchCriteria.isTranspositionTableEnabled()) {
         if(acceptNodeBefore(searchNode, scoreFloor, scoreCeiling)) {
            ChessTranspositionEntry transpositionEntry = moveTable.findEntry(searchNode);
            
            if(transpositionEntry != null) {
               ChessTranspositionType entryType = transpositionEntry.getType();
               int entryValue = transpositionEntry.getValue();
               
               if(entryType == EXACT) {
                  ChessBoard board = searchNode.moveEvaluation.getBoard();
                  ChessMove entryMove = transpositionEntry.getMove(board, searchNode.moveSide); // recover the move
                  
                  if(entryMove != null) { // dud move!!
                     return new ChessSearchNodeResult(TRANSPOSITION_TABLE, searchNode, entryMove, entryValue);
                  }
               } else if(entryType == LOWER_BOUND) {
                  scoreFloor = Math.max(scoreFloor, entryValue);
               } else if(entryType == UPPER_BOUND) {
                  scoreCeiling = Math.min(scoreCeiling, entryValue);
               }
               if(entryType != EXACT) { // if entry was exact it should have returned!! unless it was a dud
                  if(scoreFloor >= scoreCeiling) {
                     ChessBoard board = searchNode.moveEvaluation.getBoard();
                     ChessMove entryMove = transpositionEntry.getMove(board, searchNode.moveSide);
                     
                     if(entryMove != null) { // potential dud move
                        return new ChessSearchNodeResult(TRANSPOSITION_TABLE, searchNode, entryMove, entryValue);
                     }
                  }
               }
            }
         }
      }   
      if(!continueSearch(searchNode, scoreFloor, scoreCeiling)) {
         return evaluateNode(searchNode, scoreFloor, scoreCeiling); // leaf node of the search tree is where we get the score   
      }
      List<ChessSearchMove> availableMoves = availableMoves(searchNode, scoreFloor, scoreCeiling);
      
      if(!availableMoves.isEmpty()) {
         ChessSearchNodeResult bestResult = null;
      
         for(ChessSearchMove childMove : availableMoves){
            ChessSearchNode childNode = childMove.getNode(); // board with the move made!!!
            ChessSearchBoard childBoard = childNode.getBoard();
            ChessBoardMove boardMove = childBoard.getBoardMove();
            
            try {              
               boardMove.makeMove();
               
               if(!moveValidator.moveInCheck(childMove)) { // we need some way to stop the queen from doing stupid moves!!
                  ChessSearchNodeResult result = searchNode(childNode, -scoreCeiling, -scoreFloor);
                  ChessMove searchMove = childMove.getMove();                     
                  int score = -result.getScore();
                  
                  if(bestResult == null) {
                     bestResult = new ChessSearchNodeResult(result.searchPhase, result.searchNode, searchMove, score);
                  }
                  int bestScore = bestResult.getScore();
                  
                  if(score > bestScore) {
                     bestResult = new ChessSearchNodeResult(result.searchPhase, result.searchNode, searchMove, score);
                  } 
                  if(score > scoreFloor) {
                     scoreFloor = score;
                  }
                  if(scoreFloor >= scoreCeiling) {
                     break;
                  }            
               }    
            } catch(Exception e) {
               LOG.info("Error searching node", e);
            } finally {
               boardMove.revertMove();
            } 
         }
         // all moves tried and all are in check so its either
         // check mate or it is stale mate
         if(bestResult == null) {
            return new ChessSearchNodeResult(SEARCH_CHECK_MATE, searchNode, null, LOWEST_POSSIBLE_SCORE);
         }
         
         // search has finished and we now have a best score, when searchDepth == remainingDepth then we know
         // we have dived in searchDepth deep and come back up to the top to leave, so its really good then!!
         // so when REMAINING DEPTH IS SMALL the score is HIGH QUALITY at this point
         if(searchCriteria.isTranspositionTableEnabled()) { // XXX if time expires does this mean the score is not valid? probably not!
            if(acceptNodeAfter(searchNode, scoreFloor, scoreCeiling)) {
               ChessTranspositionType entryType = EXACT;
               int bestScore = bestResult.getScore();
               
               if(bestScore <= originalFloor) {
                  entryType = UPPER_BOUND;
               } else if(bestScore >= scoreCeiling) {
                  entryType = LOWER_BOUND;
               }
               moveTable.createEntry(searchNode, entryType, bestResult.rootMove, bestScore);
            }
         }
         return bestResult;
      }
      ChessSearchBoard searchBoard = searchNode.getBoard();
      ChessBoard board = searchBoard.getBoard();
      ChessSide moveSide = searchNode.getSide();
      
      if(ChessCheckAnalyzer.check(board, moveSide)) {
         return new ChessSearchNodeResult(ChessSearchPhase.SEARCH_CHECK_MATE, searchNode, null, LOWEST_POSSIBLE_SCORE);         
      }
      return new ChessSearchNodeResult(ChessSearchPhase.SEARCH_STALE_MATE, searchNode, null, 0); // stale mate game is a draw so score is 0 i.e (x - y) = 0 if x == y
   }

   protected ChessSearchMove findKillerMove(ChessSearchNode searchNode) {
      if(searchCriteria.isTranspositionTableEnabled()) {
         ChessTranspositionEntry transpositionEntry = moveTable.findEntry(searchNode, 0);
         
         if(transpositionEntry != null) {   
            ChessBoard board = searchNode.moveEvaluation.getBoard();
            ChessMove entryMove = transpositionEntry.getMove(board, searchNode.moveSide); // recover the move
            int entryValue = transpositionEntry.getValue();
            
            if(entryMove != null) { // dud move!!
               return searchNode.getMove(NORMAL, entryMove, entryValue);
            }              
         }
      }
      return super.findKillerMove(searchNode);
   }   
   
   // a depth of at least one is needed if there is to be a valid move to be chosen
   protected boolean acceptNodeBefore(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      int currentDepth = searchNode.getCurrentDepth();
      
      if(currentDepth == 0) {
         return false;
      }
      return true;
   }
   
   // only store if search was not stopped and the timeout has not expired
   // this ensures that the search was complete
   protected boolean acceptNodeAfter(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      long timeLimit = searchCriteria.getTimeLimit();
      long timeElapsed = stopWatch.elapsed();
      boolean searchStopped = isStopped();
      
      if(timeElapsed >= timeLimit) {
         return false;
      }
      if(searchStopped) {
         return false;
      }
      return true;
   }

}