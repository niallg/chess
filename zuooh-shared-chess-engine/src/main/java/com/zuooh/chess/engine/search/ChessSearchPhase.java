package com.zuooh.chess.engine.search;

public enum ChessSearchPhase {
   QUIESCENT_SCORE,
   QUIESCENT_NO_CHANGE,
   QUIESCENT_NO_CAPTURES,
   SEARCH_SCORE, 
   SEARCH_STALE_MATE,
   SEARCH_CHECK_MATE,  
   TRANSPOSITION_TABLE,
   OPEN_BOOK;
}
