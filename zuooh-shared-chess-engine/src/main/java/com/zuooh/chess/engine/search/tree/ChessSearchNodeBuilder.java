package com.zuooh.chess.engine.search.tree;

import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardHashCalculator;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessBoardValidator;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveMode;
import com.zuooh.chess.engine.search.ChessSearchCriteria;

public class ChessSearchNodeBuilder {

   private static final Logger LOG = LoggerFactory.getLogger(ChessSearchNodeBuilder.class);
   
   public final ChessSearchCriteria searchCriteria;
   public final ChessMoveMode moveMode;

   public ChessSearchNodeBuilder(ChessSearchCriteria searchCriteria) {
      this(searchCriteria, ChessMoveMode.SEARCH);
   }
   
   public ChessSearchNodeBuilder(ChessSearchCriteria searchCriteria, ChessMoveMode moveMode) {
      this.searchCriteria = searchCriteria;      
      this.moveMode = moveMode;
   }
   
   public ChessSearchNode createRoot(ChessBoard moveBoard, ChessSide moveSide, int searchDepth) {
      ChessSide oppositeDirection = moveSide.oppositeSide();
      long boardHash = ChessBoardHashCalculator.calculateBoardHash(moveBoard, oppositeDirection);            
      ChessSearchBoard moveEvaluation = new ChessRootSearchBoard(moveBoard, moveSide, boardHash);    
      AtomicInteger nodeCounter = new AtomicInteger();
      
      return new ChessSearchNode(this, moveEvaluation, moveSide, nodeCounter, 0, searchDepth, searchDepth);
   }
   
   public ChessSearchNode createChild(ChessSearchNode parentNode, ChessMove moveToMake) {
      ChessSearchBoard parentEvaluation = parentNode.getBoard();
      ChessSearchBoard childEvaluation = ChessSearchBoardBuilder.makeMove(parentEvaluation, moveToMake);
      ChessSide parentSide = parentNode.getSide();
      ChessSide childSide = parentSide.oppositeSide();
      long boardHash = childEvaluation.getBoardHash();
      long previousHash = childEvaluation.getPreviousHash();
      
      if(moveMode == ChessMoveMode.VALIDATION) {
         ChessBoard chessBoard = childEvaluation.getBoard();
         ChessBoardMove boardMove = childEvaluation.getBoardMove();
         
         try {
            ChessBoardValidator.validateBoardHash(chessBoard, childSide, previousHash);
            boardMove.makeMove();
            ChessBoardValidator.validateBoardCells(chessBoard);
            ChessBoardValidator.validateBoardOccupation(chessBoard);
            ChessBoardValidator.validateBoardHash(chessBoard, moveToMake.side, boardHash);
         } catch(Exception e) {
            LOG.info("Error during node validation", e);
         } finally {
            boardMove.revertMove();
         }
      }
      int startDepth = parentNode.getStartDepth();
      int remainingDepth = parentNode.getRemainingDepth();
      int moveCount = parentNode.getNextMoveCount(); 
      
      return new ChessSearchNode(this, childEvaluation, childSide, parentNode.moveCounter, moveCount, startDepth, remainingDepth-1);
   }
   
   public ChessSearchNode createChild(ChessSearchNode parentNode) {
      ChessSearchBoard parentEvaluation = parentNode.getBoard();
      ChessBoard parentBoard = parentEvaluation.getBoard();
      ChessSide parentSide = parentNode.getSide();     
      int changeCount = parentBoard.getChangeCount();    
      ChessMove nullMove = parentSide.createMove(changeCount);

      return createChild(parentNode, nullMove);
   }
}

