package com.zuooh.chess.engine.search;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;

public interface ChessSearchResult {
   ChessSide getSide();
   ChessMove getMove();
   ChessSearchPhase getPhase(); 
   int getScore();
   int getCount();
}
