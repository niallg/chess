package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;

public class ChessRootSearchBoard implements ChessSearchBoard {

   private final ChessBoard moveBoard;
   private final ChessSide moveSide;
   private final long boardHash;

   public ChessRootSearchBoard(ChessBoard moveBoard) {
      this(moveBoard, null);
   }
   
   public ChessRootSearchBoard(ChessBoard moveBoard, ChessSide moveSide) {
      this(moveBoard, moveSide, 0L);
   }
   
   public ChessRootSearchBoard(ChessBoard moveBoard, ChessSide moveSide, long boardHash) {
      this.moveSide = moveSide;
      this.moveBoard = moveBoard;
      this.boardHash = boardHash;
   } 
   
   @Override
   public ChessBoard getBoard() {   
      return moveBoard;
   }
   
   @Override
   public ChessBoardMove getBoardMove() {
      return null;
   } 

   @Override
   public ChessMove getFirstMove() {
      return null;
   }   
   
   @Override
   public ChessSide getFirstSide() {
      return moveSide;
   }   

   @Override
   public ChessMove getLastMove() {
      return null;
   }
   
   @Override
   public ChessSide getLastSide() {
      return null;
   }
   
   @Override
   public long getBoardHash() {     
      return boardHash;
   }

   @Override
   public long getPreviousHash() {
      return 0;
   }  
}
