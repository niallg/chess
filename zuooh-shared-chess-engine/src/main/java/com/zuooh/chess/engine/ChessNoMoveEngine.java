package com.zuooh.chess.engine;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveEngine;
import com.zuooh.chess.engine.ChessOpponentStatus;

public class ChessNoMoveEngine implements ChessMoveEngine {
   
   private final ChessSide playDirection;
   
   public ChessNoMoveEngine(ChessSide playDirection) {
      this.playDirection = playDirection;      
   }   

   @Override
   public String getOpponent() {
      return null;
   }
   
   @Override
   public ChessMove getLastMove() {
      return null;
   }
   
   @Override
   public ChessOpponentStatus getEngineStatus() {
      return ChessOpponentStatus.OFFLINE;
   }

   @Override
   public void makeMove(ChessBoard chessBoard,ChessMove moveToMake) {}

   @Override
   public ChessSide getSide() {
      return playDirection;
   }

   @Override
   public void stopSearch() {}

   @Override
   public void resignGame() {}
   
   @Override
   public void drawGame() {}
}
