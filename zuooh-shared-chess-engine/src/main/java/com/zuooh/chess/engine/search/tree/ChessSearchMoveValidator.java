package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.common.collections.Cache;

public class ChessSearchMoveValidator {   
   
   private final Cache<Long, ChessBoardHistory> recentHistory;
   
   public ChessSearchMoveValidator(Cache<Long, ChessBoardHistory> recentHistory) {
      this.recentHistory = recentHistory;    
   }   
   
   public boolean moveRepeat(ChessSearchMove searchMove) {
      return false;
   }  
   
   public boolean moveRecent(ChessSearchMove searchMove) { // call before making move
      return false;
   }     
   
   public boolean moveInCheck(ChessSearchMove searchMove) {
      ChessSearchNode searchNode = searchMove.getNode();
      ChessSearchBoard searchBoard = searchNode.getBoard(); 
      ChessBoard board = searchBoard.getBoard();
      ChessSide side = searchNode.getSide();
      ChessSide oppositeDirection = side.oppositeSide();
      ChessBoardPosition kingPosition = board.getKingPosition(oppositeDirection);
      
      if(kingPosition != null) { // can be null if "the null move" is used
         ChessPiece kingPiece = board.getPiece(kingPosition);
         
         if(kingPiece != null) {
            return kingPiece.check(board);
         }
      }
      return true;
   }
}
