package com.zuooh.chess.engine.search.tree;

import java.util.concurrent.atomic.AtomicInteger;

import com.zuooh.chess.ChessMoveType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;

public class ChessSearchNode {

   public final ChessSearchNodeBuilder nodeBuilder;
   public final ChessSearchBoard moveEvaluation;
   public final AtomicInteger moveCounter;
   public final ChessSide moveSide;
   public final int startDepth;
   public final int remainingDepth;
   public final int moveCount;
   
   public ChessSearchNode(ChessSearchNodeBuilder nodeBuilder, ChessSearchBoard moveEvaluation, ChessSide moveSide, AtomicInteger moveCounter, int moveCount, int startDepth, int remainingDepth) {
      this.remainingDepth = remainingDepth; 
      this.moveEvaluation = moveEvaluation;
      this.moveCounter = moveCounter;
      this.nodeBuilder = nodeBuilder;
      this.startDepth = startDepth; 
      this.moveCount = moveCount;
      this.moveSide = moveSide;
   }
   
   public ChessSearchMove getNullMove() {
      ChessSearchNode childNode = nodeBuilder.createChild(this);
      
      if(startDepth == remainingDepth) {
         throw new IllegalStateException("Null move should not be made on the first move");        
      }
      return new ChessSearchMove(childNode, ChessMoveType.NORMAL, null, 0);          
   }
   
   public ChessSearchMove getMove(ChessMoveType moveType, ChessMove moveToMake) {
      return getMove(moveType, moveToMake, 0);            
   }
   
   public ChessSearchMove getMove(ChessMoveType moveType, ChessMove moveToMake, int moveScore) {
      ChessSearchNode childNode = nodeBuilder.createChild(this, moveToMake);
      
      if(moveToMake.side != moveSide) {
         throw new IllegalStateException("Move " + moveToMake + " is not on same side as node " + moveSide);
      }
      return new ChessSearchMove(childNode, moveType, moveToMake, moveScore);      
   }

   public ChessSearchBoard getBoard() {
      return moveEvaluation;
   }

   public ChessSide getSide() {
      return moveSide;
   }
   
   public int getTotalMoveCount() {
      return moveCounter.get();
   }
   
   public int getNextMoveCount() {
      return moveCounter.getAndIncrement();
   }
   
   public int getMoveCount() {
      return moveCount;
   }

   public int getStartDepth() {
      return startDepth;
   }

   public int getRemainingDepth() {
      return remainingDepth;
   }
   
   public int getCurrentDepth() {
      return startDepth - remainingDepth;
   }

   
}
