package com.zuooh.chess.engine;

import java.util.Map;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.search.ChessSearchAlgorithm;
import com.zuooh.chess.engine.search.ChessSearchAlgorithmFactory;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.chess.engine.search.polyglot.ChessBook;
import com.zuooh.chess.engine.search.tree.ChessBookSearchAlgorithm;
import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.common.task.TaskScheduler;

public class ChessBestMatchMoveEngineProvider implements ChessMoveEngineProvider {

   private final Map<Integer, ChessSearchAlgorithmFactory> searchAlgorithms;
   private final Cache<String, ChessSearchAlgorithmFactory> searchCache;
   private final ChessSearchTracer searchTracer;
   private final TaskScheduler taskScheduler;
   private final ChessBook openBook;

   public ChessBestMatchMoveEngineProvider(Map<Integer, ChessSearchAlgorithmFactory> searchAlgorithms, ChessBook openBook) {
      this(searchAlgorithms, openBook, null);
   }
   
   public ChessBestMatchMoveEngineProvider(Map<Integer, ChessSearchAlgorithmFactory> searchAlgorithms, ChessBook openBook, TaskScheduler taskScheduler) {
      this.searchCache = new LeastRecentlyUsedCache<String, ChessSearchAlgorithmFactory>();
      this.searchTracer = new ChessSearchTracer(false);
      this.searchAlgorithms = searchAlgorithms;
      this.taskScheduler = taskScheduler;
      this.openBook = openBook;
   }   

   @Override
   public synchronized ChessMoveEngine createEngine(ChessSide engineSide, int skillLevel) {
      String engineKey = createEngineKey(engineSide, skillLevel);
      ChessSearchAlgorithmFactory searchAlgorithmFactory = searchCache.fetch(engineKey);
      
      if(searchAlgorithmFactory == null) {
         searchAlgorithmFactory = findBestAlgorithm(engineSide, skillLevel);
         searchCache.cache(engineKey, searchAlgorithmFactory);
      }
      ChessSearchAlgorithm searchAlgorithm = searchAlgorithmFactory.createAlgorithm();
      
      if(openBook != null) {
         searchAlgorithm = new ChessBookSearchAlgorithm(openBook, searchAlgorithm, searchTracer);
      }
      return new ChessSearchMoveEngine(searchAlgorithm, engineSide, taskScheduler);
   }
   
   private synchronized String createEngineKey(ChessSide engineSide, int skillLevel){
      return engineSide + ":" + skillLevel;
   }
   
   private synchronized ChessSearchAlgorithmFactory findBestAlgorithm(ChessSide engineSide, int skillLevel) {
      ChessSearchAlgorithmFactory searchAlgorithmFactory = searchAlgorithms.get(skillLevel);
      
      if(searchAlgorithmFactory == null) {
         int distance = 0;
         
         if(!searchAlgorithms.isEmpty()) {
            while(skillLevel - distance > 0) {
               searchAlgorithmFactory = searchAlgorithms.get(skillLevel - (distance+1));
               
               if(searchAlgorithmFactory == null) {
                  searchAlgorithmFactory = searchAlgorithms.get(skillLevel + (distance+1));
               }
               if(searchAlgorithmFactory != null) {
                  return searchAlgorithmFactory;
               }
               distance++;
            }
            while(distance < 10) {
               searchAlgorithmFactory = searchAlgorithms.get(skillLevel+distance);
               
               if(searchAlgorithmFactory != null) {
                  return searchAlgorithmFactory;
               }
            }
            return searchAlgorithms.values().iterator().next();
         }
      }
      return searchAlgorithmFactory;
   }

}
