package com.zuooh.chess.engine.search.tree;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchTracer;

//https://chessprogramming.wikispaces.com/Futility+Pruning
public class ChessFutilityPruneSearchAlgorithm extends ChessTranspositionTableSearchAlgorithm {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessFutilityPruneSearchAlgorithm.class);   
   
   private static final ChessPieceType[] FUTILITY_PIECES = {null, ChessPieceType.PAWN, ChessPieceType.ROOK};

   public ChessFutilityPruneSearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer) {
      super(searchCriteria, scoreStrategy, searchTracer);
   }
   
   @Override
   protected List<ChessSearchMove> pruneMoves(List<ChessSearchMove> bestMovesFirst, int scoreFloor, int scoreCeiling) {
      List<ChessSearchMove> possibleMoves = super.pruneMoves(bestMovesFirst, scoreFloor, scoreCeiling);
      
      if(!possibleMoves.isEmpty()) {
         List<ChessSearchMove> futileMoves = new ArrayList<ChessSearchMove>();
         
         for(ChessSearchMove searchMove : possibleMoves) {
            ChessSearchNode searchNode = searchMove.getNode();
            int currentDepth = searchNode.getCurrentDepth();
            int remainingDepth = searchNode.getRemainingDepth();
            
            if(currentDepth > 3 && remainingDepth > 0 && remainingDepth < FUTILITY_PIECES.length) { // search to a depth of 3 at least to open up the game
               ChessPieceType futilityPiece = FUTILITY_PIECES[remainingDepth];
               ChessMove moveMade = searchMove.getMove();
               ChessSide moveSide = moveMade.side;
               
               if(futilityPiece != null) {
                  ChessSearchBoard evaluation = searchNode.getBoard();
                  ChessBoard board = evaluation.getBoard();  
                  ChessBoardMove boardMove = evaluation.getBoardMove();
                  ChessPiece takenPiece = board.getPiece(moveMade.to);
                  
                  if(takenPiece == null) { // captures are never futile, so if no capture then check for futility
                     boardMove.makeMove();                     
                     
                     try {                
                        int boardScore = scoreStrategy.score(board, moveSide); // how expensive is this? dies if screw up zobrist hashing?
                        int pieceValue = scoreStrategy.valueOf(futilityPiece);
                        
                        if(boardScore + pieceValue <= scoreFloor) {
                           if(!moveValidator.moveInCheck(searchMove)) { 
                              futileMoves.add(searchMove);
                           }
                        }
                     } catch(Exception e) {
                        LOG.info("Error checking for futile moves", e);
                     } finally {
                        boardMove.revertMove();
                     }                                                                  
                  }
               }
            }
         }
         int availableCount = possibleMoves.size();
         int futilityCount = futileMoves.size();
         
         if(futilityCount > 0 && availableCount - futilityCount > 0) { // there are futile moves and also non futile moves
            possibleMoves.removeAll(futileMoves);
         }        
      }
      return possibleMoves;
   }
}
