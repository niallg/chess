package com.zuooh.chess.engine.search.tree;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessMoveType;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardCellIterator;
import com.zuooh.chess.board.ChessBoardHashCalculator;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveMode;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchAlgorithm;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchResponse;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.common.time.StopWatch;

public abstract class ChessHistorySearchAlgorithm implements ChessSearchAlgorithm {

   private static final Logger LOG = LoggerFactory.getLogger(ChessHistorySearchAlgorithm.class);
   
   protected final Cache<Long, ChessBoardHistory> recentHistory;
   protected final ChessSearchMoveValidator moveValidator;
   protected final ChessSearchCriteria searchCriteria;
   protected final ChessScoreStrategy scoreStrategy;
   protected final ChessSearchNodeBuilder nodeBuilder;
   protected final ChessSearchTracer searchTracer;
   protected final SecureRandom secureRandom;
   protected final AtomicBoolean stopSearch;
   protected final AtomicBoolean searching;
   protected final StopWatch stopWatch;
   
   public ChessHistorySearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer) {
      this.recentHistory = new LeastRecentlyUsedCache<Long, ChessBoardHistory>(100); // 100 concurrent games
      this.nodeBuilder = new ChessSearchNodeBuilder(searchCriteria, ChessMoveMode.SEARCH); 
      this.moveValidator = new ChessSearchMoveValidator(recentHistory); 
      this.secureRandom = new SecureRandom();
      this.stopSearch = new AtomicBoolean();
      this.searching = new AtomicBoolean();
      this.stopWatch = new StopWatch();
      this.searchTracer = searchTracer;
      this.searchCriteria = searchCriteria;
      this.scoreStrategy = scoreStrategy;      
   }   
   
   public long timeElapsed() {
      return stopWatch.elapsed();
   }

   public boolean isStopped() {
      return stopSearch.get();
   }

   public boolean isSearching() {
      return searching.get();
   }

   public void stopSearch() {
      recentHistory.clear();
      stopSearch.set(true);
   }
   
   public final ChessSearchResponse searchBoard(ChessBoard chessBoard, ChessSide side) {      
      stopWatch.start();
      stopSearch.set(false);
      searching.set(true);
      
      try {
         long boardKey = chessBoard.getBoardKey();
         int limitDepth = searchCriteria.getSearchDepth();
         ChessBoardHistory boardHistory = recentHistory.fetch(boardKey);
         ChessSide oppositeDirection = side.oppositeSide();
         
         if(boardHistory == null) {
            boardHistory = new ChessBoardHistory(chessBoard);
            recentHistory.cache(boardKey, boardHistory);
         }
         long beforeMoveHash = ChessBoardHashCalculator.calculateBoardHash(chessBoard, oppositeDirection); // hash of board before move made 
         ChessSearchResponse searchMove = searchBoard(chessBoard, side, limitDepth);
         
         if(searchMove != null && searchMove.chessMove != null) {
            ChessMove moveMade = searchMove.chessMove;
            int repeatCount = boardHistory.countRepeatMoves(chessBoard, moveMade, beforeMoveHash);
            boolean recentMove = boardHistory.recentMove(chessBoard, moveMade);            
            
            if(repeatCount > 0 || recentMove) {
               LOG.info("The move " + moveMade + " has been repeated " + repeatCount + " times and is recent " + recentMove);
            }
            boardHistory.saveMove(chessBoard, moveMade, beforeMoveHash);
         }
         return searchMove;
      } finally {
         searching.set(false);
         stopSearch.set(false);
         stopWatch.stop();
      }
   }

   protected List<ChessSearchMove> availableMoves(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      ChessSearchBoard evaluation = searchNode.getBoard(); 
      ChessBoard board = evaluation.getBoard();
      ChessSide side = searchNode.getSide();
      ChessBoardCellIterator boardCells = board.getBoardCells(side); 
      
      if(boardCells.hasNext()) {
         List<ChessSearchMove> availableMoves = new ArrayList<ChessSearchMove>();
         List<ChessSearchMove> availableMovesNoRepeats = new ArrayList<ChessSearchMove>();            
      
         while(boardCells.hasNext()) {            
            ChessBoardCell boardCell = boardCells.next();
            ChessPiece piece = boardCell.getPiece();
            ChessBoardPosition fromPosition = boardCell.getPosition();
            ChessPieceType pieceType = piece.key.type;    
            List<ChessBoardPosition> boardPositions = pieceType.moveNormal(board, piece);
            int count = board.getChangeCount();
            
            for(ChessBoardPosition toPosition : boardPositions) {
               ChessMove moveToMake = side.createMove(fromPosition, toPosition, count);
               ChessSearchMove moveProjection = searchNode.getMove(ChessMoveType.NORMAL, moveToMake, 0);             
                           
               availableMoves.add(moveProjection);
            }              
         }
         if(!availableMovesNoRepeats.isEmpty()) {
            return availableMovesNoRepeats;
         }
         return availableMoves;
      }
      return Collections.emptyList();     
   }
   
   protected abstract ChessSearchResponse searchBoard(ChessBoard chessBoard, ChessSide side, int searchDepth);
}
