package com.zuooh.chess.engine;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.search.ChessSearchAlgorithm;
import com.zuooh.chess.engine.search.ChessSearchResponse;

public class ChessSearchMoveTask implements Runnable {   

   private static final Logger LOG = LoggerFactory.getLogger(ChessSearchMoveTask.class);

   private final ChessSearchMoveTransaction moveTransaction;
   private final ChessSearchAlgorithm searchAlgorithm;
   private final ChessSide playDirection;
   private final ChessBoard chessBoard;
   private final ChessMove originalMove;
   
   public ChessSearchMoveTask(ChessSearchAlgorithm searchAlgorithm, ChessSearchMoveTransaction moveTransaction, ChessBoard chessBoard, ChessSide playDirection, ChessMove originalMove) {
      this.searchAlgorithm = searchAlgorithm;
      this.moveTransaction = moveTransaction;
      this.playDirection = playDirection;
      this.chessBoard = chessBoard;
      this.originalMove = originalMove;
   }
   
   @Override
   public void run() {
      try {
         ChessMove chessMove = search();
   
         if(chessMove != null) {
            moveTransaction.commitMove(chessMove, originalMove);      
         } else {
            if(ChessCheckAnalyzer.staleMate(chessBoard, playDirection)) {
               moveTransaction.updateStatus(ChessOpponentStatus.DRAW);
            } else {
               moveTransaction.updateStatus(ChessOpponentStatus.LOSE);
            }
         }
      } catch(Exception e) {
         LOG.info("Could not commit move", e);
      }
   }
   
   private ChessMove search() {   
      try {
         ChessSearchResponse searchResult = searchAlgorithm.searchBoard(chessBoard, playDirection);

         if(searchResult !=  null && searchResult.chessMove != null) {
            ChessMove chessMove = searchResult.getMove();
            ChessBoardPosition fromPosition = chessMove.getFrom();
            ChessBoardPosition toPosition = chessMove.getTo();
            ChessSide playDirection = chessMove.getSide();
            int boardCount = chessBoard.getChangeCount();
            int moveCount = chessMove.getChange();
            
            if(boardCount != moveCount) {
               throw new IllegalStateException("Move " + chessMove + " has a count of " + moveCount + " not " + boardCount);
            }
            if(originalMove != null) {
               int originalCount = originalMove.getChange();
               
               if(moveCount <= originalCount) {
                  throw new IllegalStateException("Move " + chessMove + " is not greater than " + originalCount + " for " + originalMove);
               }
            }
            return new ChessMove(fromPosition, toPosition, playDirection, moveCount);
         }
      } catch(Exception e) {
         LOG.info("Could not make move", e);
      }
      return null;
      
   }
}
