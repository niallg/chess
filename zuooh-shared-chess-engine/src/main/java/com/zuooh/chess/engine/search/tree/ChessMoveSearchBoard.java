package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;

public class ChessMoveSearchBoard implements ChessSearchBoard {

   private final ChessSearchBoard moveBoard;
   private final ChessBoardMove boardMove;
   private final ChessBoard board;
   private final ChessMove move;
   private final long boardHash;
   private final long previousHash;
   
   public ChessMoveSearchBoard(ChessSearchBoard moveBoard, ChessBoardMove boardMove, ChessBoard board, ChessMove move, long boardHash, long previousHash) {
      this.previousHash = previousHash;
      this.boardHash = boardHash;
      this.moveBoard = moveBoard;
      this.boardMove = boardMove;
      this.board = board;
      this.move = move;     
   }
   
   @Override
   public ChessBoard getBoard() {   
      return board;
   }
   
   @Override
   public ChessBoardMove getBoardMove() {
      if(boardMove == null) {
         return new ChessNullBoardMove();
      }
      return boardMove;
   }
   
   @Override
   public ChessMove getLastMove() {
      return move;
   }
   
   @Override
   public ChessSide getLastSide() {
      return move.side;
   }
   
   @Override
   public ChessMove getFirstMove() {
      ChessMove rootMove = moveBoard.getFirstMove();
      
      if(rootMove == null) {
         return move;
      }
      return rootMove;
   }
   
   @Override
   public ChessSide getFirstSide() {
      ChessSide rootSide = moveBoard.getFirstSide();
      
      if(rootSide == null) {
         return move.side;
      }
      return rootSide;
   } 
   
   @Override
   public long getBoardHash() {     
      return boardHash;
   }
   
   @Override
   public long getPreviousHash() {     
      return previousHash;
   }   
   
   @Override
   public String toString() {
      return String.valueOf(move);
   }
}
