package com.zuooh.chess.engine.score;

import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardCellIterator;

public class ChessSimpleScoreStrategy implements ChessScoreStrategy {

   protected final ChessScoreEvaluation scoreEvaluation;

   public ChessSimpleScoreStrategy(ChessScoreEvaluation scoreEvaluation) {
      this.scoreEvaluation = scoreEvaluation;
   }

   public int score(ChessBoard chessBoard, ChessSide playDirection) {
      ChessSide oppositeDirection = playDirection.oppositeSide();
      ChessBoardCellIterator myCells = chessBoard.getBoardCells(playDirection);
      ChessBoardCellIterator opponentsCells = chessBoard.getBoardCells(oppositeDirection);
      int totalScore = 0;

      while(myCells.hasNext()) {         
         ChessBoardCell myCell = myCells.next();
         ChessPiece myPiece = myCell.getPiece();
         
         totalScore += scoreEvaluation.valueOf(myPiece.key.type);         
      }
      while(opponentsCells.hasNext()) {         
         ChessBoardCell opponentsCell = opponentsCells.next();
         ChessPiece opponentsPiece = opponentsCell.getPiece();
         
         totalScore -= scoreEvaluation.valueOf(opponentsPiece.key.type);         
      }
      return totalScore;
   }   
   
   public int valueOf(ChessPieceType type) {
      return scoreEvaluation.valueOf(type);
   }   
}
