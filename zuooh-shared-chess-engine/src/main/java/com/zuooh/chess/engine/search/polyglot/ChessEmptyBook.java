package com.zuooh.chess.engine.search.polyglot;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;

public class ChessEmptyBook implements ChessBook {

   @Override
   public ChessMove createMove(ChessBoard chessBoard, ChessSide nextMove) {
      return null;
   }

}
