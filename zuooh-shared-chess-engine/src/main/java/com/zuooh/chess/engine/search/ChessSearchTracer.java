package com.zuooh.chess.engine.search;

import java.util.concurrent.atomic.AtomicBoolean;

public class ChessSearchTracer {

   public final AtomicBoolean traceEnabled;

   public ChessSearchTracer() {
      this(false);
   }
   
   public ChessSearchTracer(boolean traceEnabled) {
      this.traceEnabled = new AtomicBoolean(traceEnabled);
   }
  
   public boolean isTracingEnabled() {
      return traceEnabled.get();
   }   
   
   public void setTracingEnabled(boolean enabled) {
      traceEnabled.set(enabled);
   }
}
