package com.zuooh.chess.engine.search.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessMoveType;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardCellIterator;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchPhase;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.common.collections.GreatestFirstQueue;

//http://chessprogramming.wikispaces.com/CPW-Engine_search
//http://en.wikipedia.org/wiki/Quiescence_search
public class ChessQuiescenceSearchAlgorithm extends ChessTimeLimitSearchAlgorithm {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessQuiescenceSearchAlgorithm.class);

   public ChessQuiescenceSearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer) {
      super(searchCriteria, scoreStrategy, searchTracer);
   }

   @Override
   protected ChessSearchNodeResult evaluateNode(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      if(searchCriteria.isQuiescentSearchEnabled()) {
         long timeLimit = searchCriteria.getTimeLimit();
         long timeElapsed = stopWatch.elapsed();
         boolean forceStop = isStopped();
         
         if(timeElapsed < timeLimit && !forceStop) { // do not do this in the event of a timeout or stop
            return searchNodeCaptures(searchNode, scoreFloor, scoreCeiling);  // are scoreFloor and scoreCeiling valid for this part of the search!!!!???
         }
      }
      return super.evaluateNode(searchNode, scoreFloor, scoreCeiling); // fall back on standing pat
   }
   
   protected ChessSearchNodeResult searchNodeCaptures(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      ChessSearchBoard searchBoard = searchNode.getBoard();
      ChessBoard board = searchBoard.getBoard();
      ChessSide side = searchNode.getSide();
      int standingPat = scoreStrategy.score(board, side); // get the best score so far      
      
      if(standingPat >= scoreCeiling) {
         return new ChessSearchNodeResult(ChessSearchPhase.QUIESCENT_SCORE, searchNode, null, scoreCeiling); // null move means horizon node
      }      
      if(standingPat > scoreFloor) {
         scoreFloor = standingPat;
      }
      List<ChessSearchMove> availableCaptures = availableCaptureMoves(searchNode);
      
      if(!availableCaptures.isEmpty()) { 
         ChessSearchNodeResult bestResult = new ChessSearchNodeResult(ChessSearchPhase.QUIESCENT_NO_CHANGE, searchNode, null, scoreFloor); // scoreFloor and scoreCeiling might be confusing!!
         
         for(ChessSearchMove childMove : availableCaptures){
            ChessSearchNode childNode = childMove.getNode(); // node with move made!
            ChessSearchBoard childBoard = childNode.getBoard();
            ChessBoardMove boardMove = childBoard.getBoardMove();
            
            try {
               boardMove.makeMove();
               
               if(!moveValidator.moveInCheck(childMove)) { // will this move take me in to check????              
                  ChessSearchNodeResult result = searchNodeCaptures(childNode, -scoreCeiling, -scoreFloor);
                  ChessMove searchMove = childMove.getMove();
                  int score = -result.getScore();
                  
                  if(score >= scoreCeiling) {
                     return new ChessSearchNodeResult(result.searchPhase, result.searchNode, searchMove, scoreCeiling);
                  }
                  if(score > scoreFloor) {
                     bestResult = new ChessSearchNodeResult(result.searchPhase, result.searchNode, searchMove, score);
                     scoreFloor = score;
                  }
               }    
            } catch(Exception e) {
               LOG.info("Error searching for captures", e);
            } finally {
               boardMove.revertMove();
            }
         }
         return bestResult;
      }
      return new ChessSearchNodeResult(ChessSearchPhase.QUIESCENT_NO_CAPTURES, searchNode, null, scoreFloor); // scoreFloor and scoreCeiling might be confusing!!
   }
   
   protected List<ChessSearchMove> availableCaptureMoves(ChessSearchNode searchNode) {
      ChessSearchBoard searchBoard = searchNode.getBoard();
      ChessBoard board = searchBoard.getBoard();
      ChessSide side = searchNode.getSide();
      ChessBoardCellIterator boardCells = board.getBoardCells(side); 
      
      if(boardCells.hasNext()) {
         GreatestFirstQueue<ChessSearchMove> capturesInOrder = new GreatestFirstQueue<ChessSearchMove>();         
         List<ChessSearchMove> captureMoves = new ArrayList<ChessSearchMove>();
      
         while(boardCells.hasNext()) {            
            ChessBoardCell boardCell = boardCells.next();
            ChessPiece piece = boardCell.getPiece();
            ChessBoardPosition fromPosition = boardCell.getPosition();
            ChessPieceType pieceType = piece.key.type;            
            List<ChessBoardPosition> capturePositions = pieceType.moveCapture(board, piece); // will give king takes also                 

            for(ChessBoardPosition toPosition : capturePositions) {
               ChessPiece opponentsPiece = board.getPiece(toPosition);

               if(opponentsPiece != null) {
                  int changeCount = board.getChangeCount();
                  int opponentWeight = scoreStrategy.valueOf(opponentsPiece.key.type);
                  ChessMove move = side.createMove(fromPosition, toPosition, changeCount);
                  ChessSearchMove searchMove = searchNode.getMove(ChessMoveType.CAPTURE, move, opponentWeight);
                  
                  if(searchMove != null) {
                     capturesInOrder.offer(searchMove);
                  }
               } 
            }
         }
         while(!capturesInOrder.isEmpty()) {
            ChessSearchMove move = capturesInOrder.poll();
            captureMoves.add(move);
         }
         return captureMoves;
      }
      return Collections.emptyList();
   }
}
