package com.zuooh.chess.engine.search.tree;

import static com.zuooh.chess.engine.score.ChessScoreEvaluation.HIGHEST_POSSIBLE_SCORE;
import static com.zuooh.chess.engine.score.ChessScoreEvaluation.LOWEST_POSSIBLE_SCORE;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardHashCalculator;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveMode;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchResponse;
import com.zuooh.chess.engine.search.ChessSearchPhase;
import com.zuooh.chess.engine.search.ChessSearchResult;
import com.zuooh.chess.engine.search.ChessSearchTracer;

//http://en.wikipedia.org/wiki/Negamax
public class ChessNegaMaxSearchAlgorithm extends ChessHistorySearchAlgorithm {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessNegaMaxSearchAlgorithm.class);

   public ChessNegaMaxSearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer) {
      super(searchCriteria, scoreStrategy, searchTracer);
   }

   protected ChessSearchResponse searchBoard(ChessBoard board, ChessSide side, int searchDepth) {
      return searchBoard(board, side, searchDepth, LOWEST_POSSIBLE_SCORE, HIGHEST_POSSIBLE_SCORE);
   }
   
   protected ChessSearchResponse searchBoard(ChessBoard board, ChessSide side, int searchDepth, int scoreFloor, int scoreCeiling) {
      ChessSearchNode rootNode = nodeBuilder.createRoot(board,  side,  searchDepth);          
      long startTime = System.currentTimeMillis();
      ChessSearchResult searchResult = searchNode(rootNode, scoreFloor, scoreCeiling);
      ChessMove resultMove = searchResult.getMove();
      long finishTime = System.currentTimeMillis();
      long searchDuration = finishTime - startTime;
      
      return new ChessSearchResponse(searchResult, board, resultMove, searchDepth, searchDuration);
   }

   protected ChessSearchNodeResult searchNode(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      if(checkNullMove(searchNode, scoreFloor, scoreCeiling)) { // allow a null move to be performed
         ChessSearchNode childNode = nodeBuilder.createChild(searchNode); // will always be in the opposite direction
         ChessSearchNodeResult result = searchNode(childNode, -scoreCeiling, -scoreFloor);
         int score = -result.getScore();
         
         if(score >= scoreCeiling) {            
            return new ChessSearchNodeResult(result.searchPhase, result.searchNode, null, score);
         }         
      } 
      if(!continueSearch(searchNode, scoreFloor, scoreCeiling)) {
         return evaluateNode(searchNode, scoreFloor, scoreCeiling); // leaf node of the search tree is where we get the score
      }
      List<ChessSearchMove> availableMoves = availableMoves(searchNode, scoreFloor, scoreCeiling);
      
      if(!availableMoves.isEmpty()) {
         ChessSearchNodeResult bestResult = null;
         
         for(ChessSearchMove childMove : availableMoves){
            ChessSearchNode childNode = childMove.getNode(); // node with the move made!!!
            ChessSearchBoard moveBoard = childNode.getBoard();
            ChessBoardMove boardMove = moveBoard.getBoardMove();
            
            try {
               boardMove.makeMove();
               
               if(!moveValidator.moveInCheck(childMove)) { 
                  ChessSearchNodeResult result = searchNode(childNode, -scoreCeiling, -scoreFloor);
                  ChessMove move = childMove.getMove();                     
                  int score = -result.getScore();
                  
                  if(bestResult == null) {
                     bestResult = new ChessSearchNodeResult(result.searchPhase, result.searchNode, move, score);
                  }               
                  int bestScore = bestResult.getScore();
                  
                  if(score > bestScore) {
                     bestResult = new ChessSearchNodeResult(result.searchPhase, result.searchNode, move, score);
                  }
                  if(score > scoreFloor) {
                     scoreFloor = score;
                  }
                  if(scoreFloor >= scoreCeiling) {
                     break;
                  }            
               }    
            } catch(Exception e) {
               LOG.info("Error searching node", e);
            } finally {
               boardMove.revertMove();
            }
         }
         if(bestResult == null) {
            return new ChessSearchNodeResult(ChessSearchPhase.SEARCH_CHECK_MATE, searchNode, null, LOWEST_POSSIBLE_SCORE);
         }
         return bestResult; 
      }
      ChessSearchBoard searchBoard = searchNode.getBoard();
      ChessBoard board = searchBoard.getBoard();
      ChessSide moveSide = searchNode.getSide();
      
      if(ChessCheckAnalyzer.check(board, moveSide)) {
         return new ChessSearchNodeResult(ChessSearchPhase.SEARCH_CHECK_MATE, searchNode, null, LOWEST_POSSIBLE_SCORE);         
      }
      return new ChessSearchNodeResult(ChessSearchPhase.SEARCH_STALE_MATE, searchNode, null, 0); // stale mate is dead even at score of 0      
   }
   
   protected boolean checkNullMove(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      if(searchCriteria.isNullMoveEnabled()) {
         ChessSearchBoard searchBoard = searchNode.getBoard();
         ChessSide rootDirection = searchBoard.getFirstSide();
         int currentDepth = searchNode.getCurrentDepth();
         
         if(rootDirection == searchNode.moveSide) { // give your opponent the null move
            ChessSearchBoard moveBoard = searchNode.getBoard();
            ChessBoard board = moveBoard.getBoard();
            
            if(currentDepth == 2) { // me = 0, you = 1, me = 2
               long occupationMask = board.getBoardOccupation(searchNode.moveSide);
               int remainingPieces = 0;
               
               for(int i = 0; i < 64; i++) {
                  remainingPieces += occupationMask & 1L; // if its occupied + 1
                  occupationMask >>>= 1;
               }
               if(remainingPieces > 10) {
                  return !ChessCheckAnalyzer.check(board); // don't skip if the board is in check
               }
            }
         }
      }
      return false;
   }
   
   // possibility to continue search here!!
   protected ChessSearchNodeResult evaluateNode(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      ChessSearchBoard moveBoard = searchNode.getBoard();
      ChessBoard board = moveBoard.getBoard();
      ChessSide side = searchNode.getSide();            
      int boardScore = scoreStrategy.score(board, side);
      
      return new ChessSearchNodeResult(ChessSearchPhase.SEARCH_SCORE, searchNode, null, boardScore);
   }   

   protected boolean continueSearch(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      ChessSearchBoard searchBoard = searchNode.getBoard();
      ChessBoard board = searchBoard.getBoard();
      ChessSide side = searchNode.getSide();  
      ChessBoardPosition kingPosition = board.getKingPosition(side);
      int startDepth = searchNode.getStartDepth();
      int remainingDepth = searchNode.getRemainingDepth(); 

      if(kingPosition == null) {
         return false;
      }
      if(startDepth == remainingDepth) { // must go at least one deep
         return true;
      }      
      if(remainingDepth <= 0) {
         return false;
      }     
      return !isStopped();
   }
}
