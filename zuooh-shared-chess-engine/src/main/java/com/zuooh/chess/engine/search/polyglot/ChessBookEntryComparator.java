package com.zuooh.chess.engine.search.polyglot;

import java.util.Comparator;

public class ChessBookEntryComparator implements Comparator<ChessBookEntry> {
   
   private final boolean bestFirst;
   
   public ChessBookEntryComparator() {
      this(true);
   }
   
   public ChessBookEntryComparator(boolean bestFirst) {
      this.bestFirst = bestFirst;
   }

   @Override
   public int compare(ChessBookEntry left, ChessBookEntry right) {
      Short leftWeight = left.weight;
      Short rightWeight = right.weight;
      
      if(bestFirst) {
         return rightWeight.compareTo(leftWeight);
      }
      return leftWeight.compareTo(rightWeight);
   }

}
