package com.zuooh.chess.engine.score;

import com.zuooh.chess.ChessPieceType;

public class ChessSimpleScoreEvaluation implements ChessScoreEvaluation{

   @Override
   public int valueOf(ChessPieceType pieceType) {
      if(pieceType == ChessPieceType.PAWN) {
         return PAWN_VALUE;
      }
      if(pieceType == ChessPieceType.KNIGHT) {
         return KNIGHT_VALUE;
      }
      if(pieceType == ChessPieceType.BISHOP) {
         return BISHOP_VALUE;
      }
      if(pieceType == ChessPieceType.ROOK) {
         return ROOK_VALUE;
      }
      if(pieceType == ChessPieceType.QUEEN) {
         return QUEEN_VALUE;
      }
      if(pieceType == ChessPieceType.KING) {
         return KING_VALUE;
      }
      throw new IllegalArgumentException("No value for piece type " + pieceType);
   }
}
