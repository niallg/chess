package com.zuooh.chess.engine.score;

import static com.zuooh.chess.ChessPieceType.BISHOP;
import static com.zuooh.chess.ChessPieceType.KING;
import static com.zuooh.chess.ChessPieceType.KNIGHT;
import static com.zuooh.chess.ChessPieceType.PAWN;
import static com.zuooh.chess.ChessPieceType.QUEEN;
import static com.zuooh.chess.ChessPieceType.ROOK;
import static com.zuooh.chess.ChessSide.BLACK;
import static com.zuooh.chess.ChessSide.WHITE;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardCellIterator;

/*
 * Evaluator that uses
 * 
 * Material weights
 * Positioning weights
 * Bonuses/penalties for:
 * 2 bishops, 2 knights, 2 rooks, no pawns, tempo
 * Number of pawns effects knights/rooks/dual bishops
 * 
 * Uses a tapered evaluation function: http://chessprogramming.wikispaces.com/Tapered+Eval
 * 
 * http://chessprogramming.wikispaces.com/Simplified+evaluation+function
 */
public class ChessTaperedScoreStrategy implements ChessScoreStrategy {

   private static final int PHASE_CONSTANT = 256;

   // Penalties for having 2 knights, rooks, or no pawns
   private static final int KNIGHT_PENALTY = -10;
   private static final int ROOK_PENALTY = -20;
   private static final int NO_PAWNS_PENALTY = -20;

   // Bonuses depending on how many pawns are left
   private static final int[] KNIGHT_PAWN_ADJUSTMENT =
      {-30, -20, -15, -10, -5, 0, 5, 10, 15}; 

   private static final int[] ROOK_PAWN_ADJUSTMENT =
      {25, 20, 15, 10, 5, 0, -5, -10, -15}; 

   private static final int[] DUAL_BISHOP_PAWN_ADJUSTMENT =
      {40, 40, 35, 30, 25, 20, 20, 15, 15};

   private static final int TEMPO_BONUS = 10;

   /*
    * Piece value tables modify the value of each piece according to where it
    * is on the board.
    * 
    * To orient these tables, each row of 8 represents one row (rank) of the
    * chess board.
    * 
    * !!! The first row is where white's pieces start !!!
    * 
    * So, for example
    * having a pawn at d2 is worth -20 for white. Having it at d7 is worth
    * 50. Note that these have to be flipped over to evaluate black's pawns
    * since pawn values are not symmetric.
    * 
    * Values based on:
    * http://chessprogramming.wikispaces.com/Simplified+evaluation+function
    * http://chessprogramming.wikispaces.com/CPW-Engine_eval
    */

   private static final int PAWN_POS_WHITE[][] =
      {
      //a1,b1,c1,d1,e1,f1,g1,h1
      {0,   0,   0,   0,   0,   0,  0,  0},
      {5,  10,  10, -20, -20,  10, 10,  5},
      {5,  -5, -10,   0,   0, -10, -5,  5},
      {0,   0,   0,  20,  20,   0,  0,  0},
      {5,   5,  10,  25,  25,  10,  5,  5},     
      {10, 10,  20,  30,  30,  20, 10, 10},
      {50, 50,  50,  50,  50,  50, 50, 50},
      {0,   0,   0,   0,   0,   0,  0,  0}
      };

   private static final int PAWN_POS_BLACK[][] =
      {
      {0,   0,   0,   0,   0,   0,  0,  0},
      {50, 50,  50,  50,  50,  50, 50, 50},
      {10, 10,  20,  30,  30,  20, 10, 10},
      {5,   5,  10,  25,  25,  10,  5,  5},     
      {0,   0,   0,  20,  20,   0,  0,  0},
      {5,  -5, -10,   0,   0, -10, -5,  5},
      {5,  10,  10, -20, -20,  10, 10,  5},
      {0,   0,   0,   0,   0,   0,  0,  0}
      };

   private static final int KNIGHT_POS_WHITE[][] =
      {
      {-50, -40, -30, -30, -30, -30, -40, -50},
      {-40, -20,   0,   5,   5,   0, -20, -40},
      {-30,   5,  10,  15,  15,  10,   5, -30},
      {-30,   0,  15,  20,  20,  15,   0, -30},
      {-30,   5,  15,  20,  20,  15,   5, -30},  
      {-30,   0,  10,  15,  15,  10,   0, -30},  
      {-40, -20,   0,   0,   0,   0, -20, -40},
      {-50, -40, -30, -30, -30, -30, -40, -50}
      };

   private static final int KNIGHT_POS_BLACK[][] =
      {
      {-50, -40, -30, -30, -30, -30, -40, -50},
      {-40, -20,   0,   0,   0,   0, -20, -40},
      {-30,   0,  10,  15,  15,  10,   0, -30},  
      {-30,   5,  15,  20,  20,  15,   5, -30},
      {-30,   0,  15,  20,  20,  15,   0, -30},
      {-30,   5,  10,  15,  15,  10,   5, -30},
      {-40, -20,   0,   5,   5,   0, -20, -40},
      {-50, -40, -30, -30, -30, -30, -40, -50}
      };

   private static final int BISHOP_POS_WHITE[][] =
      {
      {-20, -10, -10, -10, -10, -10, -10, -20},
      {-10,   5,   0,   0,   0,   0,   5, -10},
      {-10,  10,  10,  10,  10,  10,  10, -10},     
      {-10,   0,  10,  10,  10,  10,   0, -10},     
      {-10,   5,   5,  10,  10,   5,   5, -10},     
      {-10,   0,   5,  10,  10,   5,   0, -10},     
      {-10,   0,   0,   0,   0,   0,   0, -10},     
      {-20, -10, -10, -10, -10, -10, -10, -20}
      };

   private static final int BISHOP_POS_BLACK[][] =
      {
      {-20, -10, -10, -10, -10, -10, -10, -20},
      {-10,   0,   0,   0,   0,   0,   0, -10},
      {-10,   0,   5,  10,  10,   5,   0, -10},
      {-10,   5,   5,  10,  10,   5,   5, -10},     
      {-10,   0,  10,  10,  10,  10,   0, -10},     
      {-10,  10,  10,  10,  10,  10,  10, -10},     
      {-10,   5,   0,   0,   0,   0,   5, -10},
      {-20, -10, -10, -10, -10, -10, -10, -20}
      };

   private static final int ROOK_POS_WHITE[][] =
      {
      {0,  0,  0,  5,  5,  0,  0,  0},
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {5, 10, 10, 10, 10, 10, 10,  5},
      {0,  5,  5,  5,  5,  5,  5,  0}
      };

   private static final int ROOK_POS_BLACK[][] =
      {
      {0,  5,  5,  5,  5,  5,  5,  0},
      {5, 10, 10, 10, 10, 10, 10,  5},
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {-5, 0,  0,  0,  0,  0,  0, -5},  
      {0,  0,  0,  5,  5,  0,  0,  0}
      };

   private static final int QUEEN_POS_WHITE[][] =
      {
      {-20, -10, -10, -5, -5, -10, -10, -20},
      {-10,   0,   5,  0,  0,   0,   0, -10},    
      {-10,   5,   5,  5,  5,   5,   0, -10},    
      {0,     0,   5,  5,  5,   5,   0, -5},  
      {-5,    0,   5,  5,  5,   5,   0, -5},  
      {-10,   0,   5,  5,  5,   5,   0, -10},         
      {-10,   0,   0,  0,  0,   0,   0, -10},
      {-20, -10, -10, -5, -5, -10, -10, -20}
      };

   private static final int QUEEN_POS_BLACK[][] =
      {
      {-20, -10, -10, -5, -5, -10, -10, -20},
      {-10,   0,   0,  0,  0,   0,   0, -10},
      {-10,   0,   5,  5,  5,   5,   0, -10},    
      {-5,    0,   5,  5,  5,   5,   0, -5},  
      {0,     0,   5,  5,  5,   5,   0, -5},  
      {-10,   5,   5,  5,  5,   5,   0, -10},    
      {-10,   0,   5,  0,  0,   0,   0, -10},    
      {-20, -10, -10, -5, -5, -10, -10, -20}
      };

   private static final int KING_POS_WHITE[][] =
      {
      {20,   30,  10,   0,   0,  10,  30,  20},
      {20,   20,   0,   0,   0,   0,  20,  20},  
      {-10, -20, -20, -20, -20, -20, -20, -10},       
      {-20, -30, -30, -40, -40, -30, -30, -20},  
      {-30, -40, -40, -50, -50, -40, -40, -30},    
      {-30, -40, -40, -50, -50, -40, -40, -30},
      {-30, -40, -40, -50, -50, -40, -40, -30},
      {-30, -40, -40, -50, -50, -40, -40, -30}
      };

   private static final int KING_POS_BLACK[][] =
      {
      {-30, -40, -40, -50, -50, -40, -40, -30},
      {-30, -40, -40, -50, -50, -40, -40, -30},
      {-30, -40, -40, -50, -50, -40, -40, -30},
      {-30, -40, -40, -50, -50, -40, -40, -30},
      {-20, -30, -30, -40, -40, -30, -30, -20},  
      {-10, -20, -20, -20, -20, -20, -20, -10},  
      {20,   20,   0,   0,   0,   0,  20,  20},  
      {20,   30,  10,   0,   0,  10,  30,  20}
      };

   private static final int KING_POS_WHITE_END[][] =
      {
      {-50, -30, -30, -30, -30, -30, -30, -50},
      {-30, -30,   0,   0,   0,   0, -30, -30},  
      {-30, -10,  20,  30,  30,  20, -10, -30},             
      {-30, -10,  30,  40,  40,  30, -10, -30},       
      {-30, -10,  30,  40,  40,  30, -10, -30},          
      {-30, -10,  20,  30,  30,  20, -10, -30},    
      {-30, -20, -10,   0,   0, -10, -20, -30},
      {-50, -40, -30, -20, -20, -30, -40, -50}
      };

   private static final int KING_POS_BLACK_END[][] =
      {
      {-50, -40, -30, -20, -20, -30, -40, -50},
      {-30, -20, -10,   0,   0, -10, -20, -30},
      {-30, -10,  20,  30,  30,  20, -10, -30},    
      {-30, -10,  30,  40,  40,  30, -10, -30},    
      {-30, -10,  30,  40,  40,  30, -10, -30},       
      {-30, -10,  20,  30,  30,  20, -10, -30},       
      {-30, -30,   0,   0,   0,   0, -30, -30},  
      {-50, -30, -30, -30, -30, -30, -30, -50}
      };

   protected final ChessScoreEvaluation scoreEvaluation;
   protected final int[] scoreCache;

   public ChessTaperedScoreStrategy(ChessScoreEvaluation scoreEvaluation) {
      this.scoreEvaluation = scoreEvaluation;
      this.scoreCache = new int[6];
   }
   
   /*
    * This is the evaluator. It simply returns a score for the board position
    * with respect to the player to move.
    * 
    * The evaluation function gives a score for each piece according to the
    * pieceValue array below, and an additional amount for each piece depending
    * on where it is (see comment below).
    * 
    * The eval of a position is the value of the pieces of the player whose
    * turn it is, minus the value of the pieces of the other player (plus the
    * castling points thrown in).
    * 
    * If it's WHITE's turn, and white is up a queen, then the value will be
    * roughly 900. If it's BLACK's turn and white is up a queen, then the value
    * returned should be about -900.
    */

   @Override
   public int score(ChessBoard chessBoard, ChessSide playerSide) {
      // Get current player
      ChessSide opponentSide = playerSide.oppositeSide();   
      ChessBoardCellIterator whiteCells = chessBoard.getBoardCells(ChessSide.WHITE);
      ChessBoardCellIterator blackCells = chessBoard.getBoardCells(ChessSide.BLACK);      
      int[][] pieceCounts = new int[2][6];
      
      while(whiteCells.hasNext()) {
         ChessBoardCell whiteCell = whiteCells.next();      
         ChessPiece whitePiece = whiteCell.getPiece();
         
         pieceCounts[WHITE.index][whitePiece.key.type.index]++;         
      }
      while(blackCells.hasNext()) {
         ChessBoardCell blackCell = blackCells.next(); 
         ChessPiece blackPiece = blackCell.getPiece();
         
         pieceCounts[BLACK.index][blackPiece.key.type.index]++;         
      }      
      int openingPlayerValue = materialScore(chessBoard, playerSide, pieceCounts, false);
      int endPlayerValue = materialScore(chessBoard, playerSide, pieceCounts, true);

      int openingOpponentValue = materialScore(chessBoard, opponentSide, pieceCounts, false);
      int endOpponentValue = materialScore(chessBoard, opponentSide, pieceCounts, true);

      // Weigh the two evaluation functions based on the current phase
      int phase = currentPhase(chessBoard, pieceCounts);
      
      int playerValue = ((openingPlayerValue * (PHASE_CONSTANT - phase)) + (endPlayerValue * phase)) / PHASE_CONSTANT;
      int opponentValue = ((openingOpponentValue * (PHASE_CONSTANT - phase)) + (endOpponentValue * phase)) / PHASE_CONSTANT;

      // Return the difference between our current score and opponents
      return playerValue - opponentValue;
   } 
   
   public int valueOf(ChessPieceType type) {
      return scoreEvaluation.valueOf(type);
   } 
   
   private int materialScore(ChessBoard board, ChessSide playerSide, int[][] pieceCounts, boolean endGame) {

      ChessBoardCellIterator boardCells = board.getBoardCells(playerSide);
      ChessSide opponentSide = playerSide.oppositeSide();

      // Determine which arrays to use
      int[][] pawnPos, knightPos, bishopPos, rookPos, queenPos, kingPos;
      
      int pawnCount = pieceCounts[playerSide.index][PAWN.index];
      int opponentPawnCount = pieceCounts[opponentSide.index][PAWN.index];
      int bishopCount = pieceCounts[playerSide.index][BISHOP.index];
      int knightCount = pieceCounts[playerSide.index][KNIGHT.index];
      int rookCount = pieceCounts[playerSide.index][ROOK.index];
      int queenCount = pieceCounts[playerSide.index][QUEEN.index];

      if (playerSide == ChessSide.WHITE) {
         pawnPos = PAWN_POS_WHITE;
         knightPos = KNIGHT_POS_WHITE;
         bishopPos = BISHOP_POS_WHITE;
         rookPos = ROOK_POS_WHITE;
         queenPos = QUEEN_POS_WHITE;
         
         if (endGame) {
            kingPos = KING_POS_WHITE_END;
         } else {         
            kingPos = KING_POS_WHITE;
         }        
      } else {
         pawnPos = PAWN_POS_BLACK;
         knightPos = KNIGHT_POS_BLACK;
         bishopPos = BISHOP_POS_BLACK;
         rookPos = ROOK_POS_BLACK;
         queenPos = QUEEN_POS_BLACK;
         
         if (endGame) {
            kingPos = KING_POS_BLACK_END;
         } else {         
            kingPos = KING_POS_BLACK;
         }    
      }
      int pawnValue = scoreCache[ChessPieceType.PAWN.index];
      int knightValue = scoreCache[ChessPieceType.KNIGHT.index];
      int bishopValue = scoreCache[ChessPieceType.BISHOP.index];
      int rookValue = scoreCache[ChessPieceType.ROOK.index];
      int queenValue = scoreCache[ChessPieceType.QUEEN.index];
      int kingValue = scoreCache[ChessPieceType.KING.index];      
      int value = 0;    
      
      if(kingValue == 0) {
         pawnValue = scoreCache[ChessPieceType.PAWN.index] = scoreEvaluation.valueOf(ChessPieceType.PAWN);
         knightValue = scoreCache[ChessPieceType.KNIGHT.index] = scoreEvaluation.valueOf(ChessPieceType.KNIGHT);
         bishopValue = scoreCache[ChessPieceType.BISHOP.index] = scoreEvaluation.valueOf(ChessPieceType.BISHOP);
         rookValue = scoreCache[ChessPieceType.ROOK.index] = scoreEvaluation.valueOf(ChessPieceType.ROOK);
         queenValue = scoreCache[ChessPieceType.QUEEN.index] = scoreEvaluation.valueOf(ChessPieceType.QUEEN);
         kingValue = scoreCache[ChessPieceType.KING.index] = scoreEvaluation.valueOf(ChessPieceType.KING);
      }
      while(boardCells.hasNext()) {         
         ChessBoardCell boardCell = boardCells.next();
         ChessPiece piece = boardCell.getPiece();
         ChessBoardPosition position = boardCell.getPosition();
         ChessPieceType pieceType = piece.key.type;
         int column = position.x;
         int row = 7 - position.y;
         
         if(pieceType == PAWN) {
            value += pawnValue + pawnPos[row][column];
         } else if(pieceType == KNIGHT) {
            value += knightValue + knightPos[row][column] + KNIGHT_PAWN_ADJUSTMENT[pawnCount];               
         } else if(pieceType == BISHOP) {
            value += bishopValue + bishopPos[row][column];
         } else if(pieceType == ROOK) {
            value += rookValue + rookPos[row][column] + ROOK_PAWN_ADJUSTMENT[pawnCount];               
         } else if(pieceType == QUEEN) {
            value += queenValue + queenPos[row][column];
         } else if(pieceType == KING) {
            value += kingValue + kingPos[row][column];               
         }         
      }

      // Give two bishops a bonus depending on pawns
      if (bishopCount > 1) {
         value += DUAL_BISHOP_PAWN_ADJUSTMENT[pawnCount];
      }
      if (knightCount > 1) {
         value += KNIGHT_PENALTY;
      }
      if (rookCount > 1) {
         value += ROOK_PENALTY;
      }
      if (pawnCount == 0) {
         value += NO_PAWNS_PENALTY;
      }

      // Bonus if it's our turn: https://chessprogramming.wikispaces.com/Tempo
      //if (player == board.toPlay()) {
      //   value += tempoBonus;
     // }


      /*******************************************************************
       *  Low material correction - guarding against an illusory material *
       *  advantage.  Program  will not not  expect to  win  having  only *
       *  a single minor piece and no pawns.                              *
       *******************************************************************/
      if ((pawnCount == 0) && (value < bishopValue) && (value > 0)) {
         return 0;
      }

      /*******************************************************************
       *  Program will not expect to win having only two knights in case  *
       *  neither  side  has pawns.                                       *
       *******************************************************************/
      if (value > 0 && pawnCount == 0 && opponentPawnCount == 0 && knightCount == 2 && bishopCount == 0 && rookCount == 0 && queenCount == 0) {
         return 0;
      }

      return value;
   }

   // Calculate what part of the game we're in
   // From http://chessprogramming.wikispaces.com/Tapered+Eval
   private int currentPhase(ChessBoard chessBoard, int[][] pieceCounts) {    
      int knightPhase = 1;
      int bishopPhase = 1;
      int rookPhase = 2;
      int queenPhase = 4;
      int totalPhase = knightPhase*4 + bishopPhase*4 + rookPhase*4 + queenPhase*2;
      int phase = totalPhase;
      
      int knightCount = pieceCounts[WHITE.index][KNIGHT.index] + pieceCounts[BLACK.index][KNIGHT.index];      
      int bishopCount = pieceCounts[WHITE.index][BISHOP.index] + pieceCounts[BLACK.index][BISHOP.index];      
      int rookCount = pieceCounts[WHITE.index][ROOK.index] + pieceCounts[BLACK.index][ROOK.index];      
      int queenCount = pieceCounts[WHITE.index][QUEEN.index] + pieceCounts[BLACK.index][QUEEN.index];      

      phase -= knightCount * knightPhase;
      phase -= bishopCount * bishopPhase;
      phase -= rookCount   * rookPhase;
      phase -= queenCount  * queenPhase;
      
      return (phase * PHASE_CONSTANT + (totalPhase / 2)) / totalPhase;
   }
}