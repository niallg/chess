package com.zuooh.chess.engine.search.tree;

import static com.zuooh.chess.engine.score.ChessScoreEvaluation.HIGHEST_POSSIBLE_SCORE;
import static com.zuooh.chess.engine.score.ChessScoreEvaluation.LOWEST_POSSIBLE_SCORE;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchPhase;
import com.zuooh.chess.engine.search.ChessSearchResponse;
import com.zuooh.chess.engine.search.ChessSearchResult;
import com.zuooh.chess.engine.search.ChessSearchTracer;

//https://chessprogramming.wikispaces.com/Iterative+Deepening
public class ChessTimeLimitSearchAlgorithm extends ChessKillerMoveSearchAlgorithm {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessTimeLimitSearchAlgorithm.class);
   
   protected final int startDepth;

   public ChessTimeLimitSearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer) {
      this(searchCriteria, scoreStrategy, searchTracer, 1);
   }
   
   public ChessTimeLimitSearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer, int startDepth) {
      super(searchCriteria, scoreStrategy, searchTracer);
      this.startDepth = startDepth;
   }   
   
   @Override
   protected ChessSearchResponse searchBoard(ChessBoard chessBoard, ChessSide playDirection, int searchDepth) {
      int aspirationCeiling = HIGHEST_POSSIBLE_SCORE;
      int aspirationFloor = LOWEST_POSSIBLE_SCORE;
      ChessSearchResponse bestResult = null;
  
      for(int currentDepth = startDepth; currentDepth < Math.max(searchDepth, startDepth + 1); currentDepth++) { // eventually move to search depth                  
         long startTime = System.currentTimeMillis();

         if(searchTracer.isTracingEnabled()) {
            LOG.info("Searching with aspiration window of " + aspirationFloor + "/" + aspirationCeiling + " at depth of "+ (currentDepth + 1) + 
                " current static score is white/black " + scoreStrategy.score(chessBoard, ChessSide.WHITE)+"/"+scoreStrategy.score(chessBoard, ChessSide.BLACK));
         }
         
         ChessSearchResponse bestResultThisDepth = super.searchBoard(chessBoard, playDirection, currentDepth + 1, aspirationFloor, aspirationCeiling); // always search from a depth of 1
         ChessSearchResult searchResult = bestResultThisDepth.getResult();
         ChessSearchPhase searchPhase = searchResult.getPhase();         
         int score = searchResult.getScore();
         
         if(score <= aspirationFloor || score >= aspirationCeiling) {
            if(searchTracer.isTracingEnabled()) {
               LOG.info("Aspiration window " + aspirationFloor + "/" + aspirationCeiling + " out of bounds at depth of "+ (currentDepth + 1) +" with score " +score);
            }
            
            if(aspirationCeiling != HIGHEST_POSSIBLE_SCORE && aspirationFloor != LOWEST_POSSIBLE_SCORE) { // if we were already at the limits don't reset
               aspirationCeiling = HIGHEST_POSSIBLE_SCORE;
               aspirationFloor = LOWEST_POSSIBLE_SCORE;
               currentDepth--; // do it again
            }
         } else {            
            // Aspiration windows are a way to reduce the search space in an alpha-beta search. The technique is to use a guess 
            // of the expected value (usually from the last iteration in iterative deepening), and use a window around this as 
            // the alpha-beta bounds. Because the window is narrower, more beta cutoffs are achieved, and the search takes a shorter time. 
            // The drawback is that if the true score is outside this window, then a costly re-search must be made. Typical window sizes are 
            // 1/2 to 1/4 of a pawn on either side of the guess.
            if(searchTracer.isTracingEnabled()) {
               LOG.info("Updating aspiration window at depth of "+ (currentDepth + 1) +" with score " +score);
            }
            aspirationCeiling = score + scoreStrategy.valueOf(ChessPieceType.PAWN) / 3; 
            aspirationFloor = score - scoreStrategy.valueOf(ChessPieceType.PAWN) / 3;  
         }
         Class algorithmClass = getClass();
         String algorithmName = algorithmClass.getSimpleName();
         long timeElapsed = stopWatch.elapsed();  
         long timeLimit = searchCriteria.getTimeLimit();
         long finishTime = System.currentTimeMillis();
         long searchDuration = finishTime - startTime;       
         
         if(bestResult == null) {
            bestResult = bestResultThisDepth;
         }         
         if(isStopped()) {
            if(searchTracer.isTracingEnabled()) {
               LOG.info("["+algorithmName+"] Search has been stopped at " + (currentDepth + 1) + " returning null");
            }
            return null;
         }
         if(searchPhase == ChessSearchPhase.SEARCH_CHECK_MATE) {
            if(searchTracer.isTracingEnabled()) {
               LOG.info("["+algorithmName+"] Check mate at depth of " + (currentDepth + 1) + " after " + timeElapsed + ": " + bestResult);
            }
            return bestResult; // win!!
         } 
         if(searchPhase == ChessSearchPhase.SEARCH_STALE_MATE) {
            if(searchTracer.isTracingEnabled()) {
               LOG.info("["+algorithmName+"] Stale mate at depth of " + (currentDepth + 1) + " after " + timeElapsed + ": " + bestResult);
            }
            return bestResult; // draw!!
         }      
         if(timeElapsed >= timeLimit) { // we did not finish this search cleanly
            if(searchTracer.isTracingEnabled()) {
               LOG.info("["+algorithmName+"] Time limit expired on depth " + (currentDepth + 1) + " after " + timeElapsed + ": " + bestResult);
            }
            return bestResult;
         }
         long searchFinishTimeEstimate = searchDuration + searchDuration + timeElapsed; // do we have enough time to search even deeper 
         
         if(searchFinishTimeEstimate >= timeLimit) { // no point in starting a search that will timeout
            if(searchTracer.isTracingEnabled()) {
               LOG.info("["+algorithmName+"] Not enough time to try depth " + (currentDepth + 2) + " as " + timeElapsed + " has already elapsed: " + bestResultThisDepth);
            }
            return bestResultThisDepth;
         }    
         bestResult = bestResultThisDepth;
      }
      return bestResult;
   } 
   
   
   protected int testPerformance(ChessBoard chessBoard, ChessSide moveSide, int searchDepth) {
      ChessSearchNode rootNode = nodeBuilder.createRoot(chessBoard, moveSide, searchDepth);
      
      if(searchDepth > 0) {
         return countNodes(rootNode);
      }
      return 0;
   }
   
   protected int countNodes(ChessSearchNode searchNode) {
      int remainingDepth = searchNode.getRemainingDepth();
   
      if(remainingDepth <= 0) {
         return 1;
      }
      List<ChessSearchMove> availableMoves = availableMoves(searchNode, LOWEST_POSSIBLE_SCORE, HIGHEST_POSSIBLE_SCORE); // do not narrow   
      int nodeCount = 0;
    
      if(!availableMoves.isEmpty()) {
         for(ChessSearchMove childMove : availableMoves){
            ChessSearchNode childNode = childMove.getNode(); // node with the move made!!!
            ChessSearchBoard moveBoard = childNode.getBoard();
            ChessBoardMove boardMove = moveBoard.getBoardMove();
            
            try {
               boardMove.makeMove();
               
               if(!moveValidator.moveInCheck(childMove)) { 
                  nodeCount += countNodes(childNode);
               }
            } catch(Exception e) {
               LOG.info("Error counting nodes", e);
            } finally {
               boardMove.revertMove();
            }            
         }
      }      
      return nodeCount;
   }   
   
   @Override
   protected boolean continueSearch(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) { 
      int currentDepth = searchNode.getCurrentDepth();
      
      if(currentDepth > 0) {
         long timeLimit = searchCriteria.getTimeLimit();
         long timeElapsed = stopWatch.elapsed();
         
         if(timeElapsed > timeLimit) {
            return false;
         }
         return super.continueSearch(searchNode, scoreFloor, scoreCeiling);
      }
      return true; // must search to at least one
   }
}
