package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardHashCalculator;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;

public class ChessSearchBoardBuilder {
 
   public static ChessSearchBoard makeMove(ChessSearchBoard searchBoard, ChessMove move) {      
      ChessSide lastSide = searchBoard.getLastSide();

      if(lastSide != null && lastSide == move.side) {
         throw new IllegalStateException("Move made by " + move.side + " also made the last move?");
      }   
      if(move.from == null) { // null move 
         return makeNullMove(searchBoard, move);
      }
      ChessBoard board = searchBoard.getBoard();
      ChessPiece piece = board.getPiece(move.from);
      
      if(piece == null) {
         throw new IllegalStateException("No piece located at " + move.from + " for move " + move);
      }      
      if(piece.side != move.side) {
         throw new IllegalStateException("Move made by " + move.side + " but piece is " + piece);
      }            
      if(piece.key.type == ChessPieceType.PAWN) {
         if(piece.moveWasEnPassent(board, move.to)) {
            return makeEnPassentMove(searchBoard, move);
         } 
         if(piece.moveWasPromotion(board, move.to)) {
            return makePromotionMove(searchBoard, move);
         }
      } else if(piece.key.type == ChessPieceType.KING) {
         if(piece.moveWasCastle(board, move.to)) {
            return makeCastleMove(searchBoard, move);
         }
      } 
      return makeNormalMove(searchBoard, move);
   }
   
   private static ChessSearchBoard makeNullMove(ChessSearchBoard searchBoard, ChessMove move) {
      ChessBoard board = searchBoard.getBoard();
      long boardHash = searchBoard.getBoardHash();
      long moveHash = boardHash;
            
      if(moveHash != 0) {
         moveHash ^= ChessBoardHashCalculator.calculateChangeTurnHash(board); // changed sides as basically its my move, but I do nothing      
      }
      return new ChessMoveSearchBoard(searchBoard, null, board, move, moveHash, boardHash);
   }
   
   private static ChessSearchBoard makeNormalMove(ChessSearchBoard searchBoard, ChessMove move) {      
      ChessBoard board = searchBoard.getBoard();
      ChessBoardMove boardMove = board.getBoardMove(move);    
      long boardHash = searchBoard.getBoardHash();
      long moveHash = boardHash;
      
      if(moveHash != 0) {
         moveHash ^= ChessBoardHashCalculator.calculateChangeTurnHash(board);            
         moveHash ^= ChessBoardHashCalculator.calculateChangePositionHash(board, move.from, move.to, ChessPieceType.QUEEN);
         moveHash ^= ChessBoardHashCalculator.calculateTakePieceHash(board, move.to);
      } 
      return new ChessMoveSearchBoard(searchBoard, boardMove, board, move, moveHash, boardHash);
   }
   
   private static ChessSearchBoard makePromotionMove(ChessSearchBoard searchBoard, ChessMove move) {
      ChessBoard board = searchBoard.getBoard();
      ChessBoardMove boardMove = board.getBoardMove(move);   
      long boardHash = searchBoard.getBoardHash();
      long moveHash = boardHash;
      
      if(moveHash != 0) {
         moveHash ^= ChessBoardHashCalculator.calculateChangeTurnHash(board);            
         moveHash ^= ChessBoardHashCalculator.calculateChangePositionHash(board, move.from, move.to, ChessPieceType.QUEEN);
         moveHash ^= ChessBoardHashCalculator.calculateTakePieceHash(board, move.to);
      } 
      return new ChessMoveSearchBoard(searchBoard, boardMove, board, move, moveHash, boardHash);
   }
   
   private static ChessSearchBoard makeEnPassentMove(ChessSearchBoard searchBoard, ChessMove move) {
      ChessBoard board = searchBoard.getBoard();
      ChessBoardMove boardMove = board.getBoardMove(move);  
      ChessBoardPosition takePosition = null;
      
      if(move.side == ChessSide.WHITE) {
         takePosition = ChessBoardPosition.at(move.to.x, move.to.y + 1);
      } else {
         takePosition = ChessBoardPosition.at(move.to.x, move.to.y - 1);
      }
      long boardHash = searchBoard.getBoardHash();
      long moveHash = boardHash;

      if(moveHash != 0) {
         moveHash ^= ChessBoardHashCalculator.calculateChangeTurnHash(board);              
         moveHash ^= ChessBoardHashCalculator.calculateChangePositionHash(board, move.from, move.to,  ChessPieceType.QUEEN);
         moveHash ^= ChessBoardHashCalculator.calculateTakePieceHash(board, takePosition);
      }     
      return new ChessMoveSearchBoard(searchBoard, boardMove, board, move, moveHash, boardHash);
   }
   
   private static ChessSearchBoard makeCastleMove(ChessSearchBoard searchBoard, ChessMove move) {
      ChessBoardPosition castleFrom = null;
      ChessBoardPosition castleTo = null;
      
      if(move.side == ChessSide.BLACK) {
         if(move.to == ChessBoardPosition.G8) {
            castleFrom = ChessBoardPosition.H8;
            castleTo = ChessBoardPosition.F8;
         }else if(move.to == ChessBoardPosition.C8) {
            castleFrom = ChessBoardPosition.A8;
            castleTo = ChessBoardPosition.D8;
         }
      } else {
         if(move.to == ChessBoardPosition.G1) {
            castleFrom = ChessBoardPosition.H1;
            castleTo = ChessBoardPosition.F1;
         }else if(move.to == ChessBoardPosition.C1) {
            castleFrom = ChessBoardPosition.A1;
            castleTo = ChessBoardPosition.D1;
         }
      }  
      ChessBoard board = searchBoard.getBoard(); 
      ChessBoardMove boardMove = board.getBoardMove(move);  
      long boardHash = searchBoard.getBoardHash();
      long moveHash = boardHash;
      
      if(moveHash != 0) {
         moveHash ^= ChessBoardHashCalculator.calculateChangeTurnHash(board);     
         moveHash ^= ChessBoardHashCalculator.calculateChangePositionHash(board, castleFrom, castleTo,  ChessPieceType.QUEEN);
         moveHash ^= ChessBoardHashCalculator.calculateChangePositionHash(board, move.from, move.to,  ChessPieceType.QUEEN);         
      }
      return new ChessMoveSearchBoard(searchBoard, boardMove, board, move, moveHash, boardHash);
   }
}

