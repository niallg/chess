package com.zuooh.chess.engine.search.polyglot;

public interface ChessBookFile {
   ChessBookEntry read(long offset);
   long length();
}
