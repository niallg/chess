package com.zuooh.chess.engine.search.tree;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessMoveType;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardCellIterator;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.score.ChessScoreStrategy;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.common.collections.GreatestFirstQueue;

public class ChessKillerMoveSearchAlgorithm extends ChessNegaMaxSearchAlgorithm {
   
   private static final Logger LOG = LoggerFactory.getLogger(ChessKillerMoveSearchAlgorithm.class);

   public ChessKillerMoveSearchAlgorithm(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer) {
      super(searchCriteria, scoreStrategy, searchTracer);
   }
   
   @Override
   protected List<ChessSearchMove> availableMoves(ChessSearchNode searchNode, int scoreFloor, int scoreCeiling) {
      ChessSearchBoard searchBoard = searchNode.getBoard(); 
      ChessBoard board = searchBoard.getBoard();
      ChessSide nodeSide = searchNode.getSide();
      ChessBoardCellIterator boardCells = board.getBoardCells(nodeSide);     
      
      if(boardCells.hasNext()) {
         ChessSearchMove killerNode = findKillerMove(searchNode);  
         ChessMove killerMove = null;
         
         if(killerNode != null) {
            killerMove = killerNode.getMove();
         }
         GreatestFirstQueue<ChessSearchMove> movesInOrder = new GreatestFirstQueue<ChessSearchMove>();       
         List<ChessSearchMove> bestMovesFirst = new ArrayList<ChessSearchMove>();
         
         while(boardCells.hasNext()) {            
            ChessBoardCell boardCell = boardCells.next();
            ChessPiece piece = boardCell.getPiece();
            ChessBoardPosition fromPosition = boardCell.getPosition();
            ChessPieceType pieceType = piece.key.type;
            List<ChessBoardPosition> boardPositions = pieceType.moveNormal(board, piece);
            int changeCount = board.getChangeCount();
            
            for(ChessBoardPosition toPosition : boardPositions) {
               if(killerMove != null && killerMove.from == fromPosition && killerMove.to == toPosition) {
                  bestMovesFirst.add(killerNode); // if the killer is valid make sure it is first
               } else {
                  ChessMove move = nodeSide.createMove(fromPosition, toPosition, changeCount);
                  ChessPiece takenPiece = board.getPiece(toPosition);
                  int score = 0;
                  
                  if(takenPiece != null) {
                     score = scoreStrategy.valueOf(takenPiece.key.type); // kind of a killer move!!??
                  }               
                  ChessSearchMove searchMove = searchNode.getMove(ChessMoveType.NORMAL, move, score);

                  if(searchMove != null) {
                     movesInOrder.offer(searchMove);
                  }
               }
            }             
         }
         while(!movesInOrder.isEmpty()) {
            ChessSearchMove moveProjection = movesInOrder.poll();
            bestMovesFirst.add(moveProjection);
         }
         return pruneMoves(bestMovesFirst, scoreFloor, scoreCeiling);
      }
      return Collections.emptyList();
   }
   
   protected List<ChessSearchMove> pruneMoves(List<ChessSearchMove> bestMovesFirst, int scoreFloor, int scoreCeiling) {
      return bestMovesFirst;
   }
   
   protected ChessSearchMove findKillerMove(ChessSearchNode searchNode) {
      return null;
   }
}
