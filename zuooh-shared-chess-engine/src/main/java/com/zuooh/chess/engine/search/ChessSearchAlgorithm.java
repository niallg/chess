package com.zuooh.chess.engine.search;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;

public interface ChessSearchAlgorithm {
   ChessSearchResponse searchBoard(ChessBoard chessBoard, ChessSide playDirection);
   void stopSearch();
}
