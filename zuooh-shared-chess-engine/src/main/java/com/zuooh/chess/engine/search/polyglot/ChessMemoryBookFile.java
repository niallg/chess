package com.zuooh.chess.engine.search.polyglot;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.InputStream;

public class ChessMemoryBookFile implements ChessBookFile {
   
   private final byte[] bookFile;
   
   public ChessMemoryBookFile(byte[] bookFile) {
      this.bookFile = bookFile;
   }
   
   @Override
   public ChessBookEntry read(long offset) {
      try {
         InputStream rawInput = new ByteArrayInputStream(bookFile);
         DataInputStream dataInput = new DataInputStream(rawInput);
         
         dataInput.reset();
         dataInput.skip(offset);
         
         long hash = dataInput.readLong();
         short move = dataInput.readShort();
         short weight = dataInput.readShort();
         int learn = dataInput.readInt();
         
         return new ChessBookEntry(hash, move, weight, learn);
      } catch(Exception e) {
         throw new IllegalStateException("Error occured reading book file", e);
      }
   }
   
   @Override
   public long length() {
      return bookFile.length;
   }

}
