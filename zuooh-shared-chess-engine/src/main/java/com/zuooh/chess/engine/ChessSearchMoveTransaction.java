package com.zuooh.chess.engine;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;

public class ChessSearchMoveTransaction {     
   
   private final Map<ChessSide, ChessMove> lastMoves;
   private final AtomicReference<ChessOpponentStatus> searchStatus;
   private final AtomicReference<ChessMove> currentMove;
   private final ChessSide playDirection;
   private final AtomicInteger moveCount;
   
   public ChessSearchMoveTransaction(ChessSide playDirection) {
      this.searchStatus = new AtomicReference<ChessOpponentStatus>(ChessOpponentStatus.ONLINE);
      this.lastMoves = new LinkedHashMap<ChessSide, ChessMove>();
      this.currentMove = new AtomicReference<ChessMove>();
      this.moveCount = new AtomicInteger();
      this.playDirection = playDirection;
   }
   
   public synchronized ChessOpponentStatus currentStatus(){
      return searchStatus.get();
   }
   
   public synchronized void updateStatus(ChessOpponentStatus opponentStatus) {
      searchStatus.set(opponentStatus);
   }
   
   public synchronized ChessMove lastMove() {
      return currentMove.get();
   }
   
   public synchronized boolean abortMove() {
      ChessMove lastMoveMade = currentMove.get();
      
      currentMove.set(null);
      lastMoves.clear();
      
      if(lastMoveMade == null) {
         if(moveCount.compareAndSet(1, 0)) { // first move abort
            searchStatus.set(ChessOpponentStatus.ONLINE);
            return true;
         }
         return false;
      }
      ChessSide moveDirection = lastMoveMade.getSide();

      if(moveDirection != playDirection) {
         searchStatus.set(ChessOpponentStatus.ONLINE);         
         return true;
      }
      return false; // too late move committed
   }
   
   public synchronized boolean beginMove(ChessMove moveToMake) {
      ChessMove lastMoveMade = currentMove.get();
      
      if(moveToMake == null && lastMoveMade == null) {
         if(playDirection != ChessSide.WHITE) { // white makes first move!!              
            throw new IllegalStateException("Attempt to make first move out of turn");
         }
         if(moveCount.compareAndSet(0, 1)) { // make first move
            searchStatus.set(ChessOpponentStatus.THINKING);
            return true;            
         }
         return false;
      }             
      if(moveToMake != null && lastMoveMade != null) {
         ChessSide moveDirection = moveToMake.getSide();
         ChessSide lastDirection = lastMoveMade.getSide();
      
         if(moveDirection != lastDirection) {
            ChessMove historicalMove = lastMoves.get(moveDirection);
            
            if(historicalMove != null) {
               if(sameMove(moveToMake, historicalMove)) { // do not make same move again
                  return false;
               }
            }
            searchStatus.set(ChessOpponentStatus.THINKING);
            currentMove.set(moveToMake);
            return true;
         }
      }
      if(lastMoveMade == null) { // aborted move
         ChessSide moveDirection = moveToMake.getSide();
         
         if(moveDirection != playDirection) {
            searchStatus.set(ChessOpponentStatus.THINKING);
            currentMove.set(moveToMake);
            return true;
         }
      }
      return false;
   }
   
   public synchronized boolean commitMove(ChessMove nextMove, ChessMove previousMove) {
      ChessMove lastMoveMade = currentMove.get();
      
      if(previousMove == null && lastMoveMade == null) {
         if(playDirection != ChessSide.WHITE) { // white makes first move!!              
            throw new IllegalStateException("Attempt to make first move out of turn");
         }
         searchStatus.set(ChessOpponentStatus.ONLINE);
         moveCount.getAndIncrement();
         currentMove.set(nextMove);
         return true;
      }
      if(sameMove(lastMoveMade, previousMove)) {            
         ChessSide nextDirection = nextMove.getSide();
         ChessSide previousDirection = previousMove.getSide();
         
         if(nextDirection != previousDirection) {
            lastMoves.put(previousDirection, lastMoveMade); // store the confirmed move
            searchStatus.set(ChessOpponentStatus.ONLINE);
            moveCount.getAndIncrement();
            currentMove.set(nextMove);
            return true;
         }           
      } 
      return false; // some async race condition!!
   }
   
   private synchronized boolean sameMove(ChessMove leftMove, ChessMove rightMove) {
      if(leftMove == rightMove) {
         return true;
      }
      if(leftMove == null && rightMove == null) {
         return true;
      }
      if(leftMove != null && rightMove != null) {
         ChessSide leftDirection = leftMove.getSide();
         ChessSide rightDirection = rightMove.getSide();
         
         if(leftDirection != rightDirection) {
            return false;
         }
         ChessBoardPosition leftFrom = leftMove.getFrom();
         ChessBoardPosition rightFrom = rightMove.getFrom();
         
         if(leftFrom != rightFrom) {
            return false;
         }
         ChessBoardPosition leftTo = leftMove.getTo();
         ChessBoardPosition rightTo = rightMove.getTo();
         
         if(leftTo != rightTo) {
            return false;
         }
         int leftCount = leftMove.getChange();
         int rightCount = rightMove.getChange();
         
         if(leftCount != rightCount) {
            return false;
         } 
         return true;
      }
      return false;
   }
}