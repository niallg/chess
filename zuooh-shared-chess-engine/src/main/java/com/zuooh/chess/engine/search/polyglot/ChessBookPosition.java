package com.zuooh.chess.engine.search.polyglot;

public class ChessBookPosition {
   
   public final int row;
   public final int file;
   
   public ChessBookPosition(int file, int row) {
      this.row = row;
      this.file = file;
   }
   
   @Override
   public String toString() {
      return "row=" + row + " file=" + file;
   }
}
