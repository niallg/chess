package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.board.ChessBoardMove;

public class ChessNullBoardMove implements ChessBoardMove {

   @Override
   public void makeMove() {}

   @Override
   public void makeMove(boolean validate) {}

   @Override
   public void revertMove() {}

}
