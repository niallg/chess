package com.zuooh.chess.engine.search;

import java.util.Set;

public enum ChessSearchFeature {
   NULL_MOVE(0x00000001),
   ITERATIVE_DEEPENING(0x00000002),
   TRANSPOSITION_TABLE(0x00000004),
   OPENING_BOOK(0x00000008),
   MOVE_ORDERING(0x00000010),
   QUIESCENT_SEARCH(0x00000020),
   FUTILITY_PRUNE(0x00000040), 
   EVERYTHING(0xffffffff);   
   
   public final int mask;
   
   private ChessSearchFeature(int mask) {
      this.mask = mask;      
   }
   
   public boolean containsFeature(int value){
      return (mask & value) == mask;
   }
   
   public static int createFeatureMask(Set<ChessSearchFeature> searchFeatures) {
      int value = 0;
      
      for(ChessSearchFeature searchFeature : searchFeatures){
         if(searchFeature != null) {
            value |= searchFeature.mask;
         }
      }
      return value;
      
   }
}
