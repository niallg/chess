
package com.zuooh.chess.engine.search;

import java.lang.reflect.Constructor;

import com.zuooh.chess.engine.score.ChessScoreStrategy;

public class ChessSearchAlgorithmFactory<T extends ChessSearchAlgorithm> {

   private final ChessSearchCriteria searchCriteria;
   private final ChessScoreStrategy scoreStrategy;
   private final ChessSearchTracer searchTracer;
   private final Class<T> searchAlgorithm;
   
   public ChessSearchAlgorithmFactory(ChessSearchCriteria searchCriteria, ChessScoreStrategy scoreStrategy, ChessSearchTracer searchTracer, Class<T> searchAlgorithm) {
      this.searchTracer = searchTracer;
      this.searchCriteria = searchCriteria;
      this.scoreStrategy = scoreStrategy;
      this.searchAlgorithm = searchAlgorithm;
   }
   
   public T createAlgorithm() {
      try {
         Constructor<T> algorithmFactory = searchAlgorithm.getConstructor(ChessSearchCriteria.class, ChessScoreStrategy.class, ChessSearchTracer.class);
         return algorithmFactory.newInstance(searchCriteria, scoreStrategy, searchTracer);
      } catch(Exception e) {
         throw new IllegalStateException("Could not instantiate " + searchAlgorithm, e);
      }
   }
   
}
