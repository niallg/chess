package com.zuooh.chess.engine;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.search.ChessSearchAlgorithm;
import com.zuooh.common.task.TaskScheduler;

public class ChessSearchMoveEngine implements ChessMoveEngine {  
 
   private final ChessSearchMoveTransaction moveTransaction;
   private final ChessSearchAlgorithm searchAlgorithm;
   private final ChessSide playDirection;
   private final TaskScheduler taskScheduler;

   public ChessSearchMoveEngine(ChessSearchAlgorithm searchAlgorithm, ChessSide playDirection) {
      this(searchAlgorithm, playDirection, null);
   }
   
   public ChessSearchMoveEngine(ChessSearchAlgorithm searchAlgorithm, ChessSide playDirection, TaskScheduler taskScheduler) {
      this.moveTransaction = new ChessSearchMoveTransaction(playDirection);
      this.searchAlgorithm = searchAlgorithm;
      this.taskScheduler = taskScheduler;
      this.playDirection = playDirection;
   }   

   @Override
   public String getOpponent() {
      return null;
   }   

   @Override
   public ChessSide getSide(){
      return playDirection;
   }
   
   @Override
   public ChessOpponentStatus getEngineStatus() {
      return moveTransaction.currentStatus();
   }
   
   @Override
   public ChessMove getLastMove() {
      return moveTransaction.lastMove();
   }

   @Override
   public void stopSearch() {
      moveTransaction.abortMove();
      searchAlgorithm.stopSearch();     
   }

   @Override
   public void resignGame() {
      moveTransaction.abortMove();
      searchAlgorithm.stopSearch();     
   }
   
   @Override
   public void drawGame() {
      moveTransaction.abortMove();
      searchAlgorithm.stopSearch(); 
   }   

   @Override
   public void makeMove(ChessBoard chessBoard, ChessMove moveToMake) { // this method is called repeatedly, like a poll
      if(moveTransaction.beginMove(moveToMake)) {
         ChessSearchMoveTask searchTask = new ChessSearchMoveTask(searchAlgorithm, moveTransaction, chessBoard, playDirection, moveToMake);         
            
         if(taskScheduler != null) {
            taskScheduler.execute(searchTask);
         } else {
            searchTask.run();
         }         
      }
   }
}
