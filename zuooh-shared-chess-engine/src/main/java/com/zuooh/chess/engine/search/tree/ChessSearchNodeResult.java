package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.search.ChessSearchPhase;
import com.zuooh.chess.engine.search.ChessSearchResult;

public class ChessSearchNodeResult implements ChessSearchResult {
   
   public final ChessSearchBoard searchBoard;
   public final ChessSearchNode searchNode;
   public final ChessSearchPhase searchPhase;
   public final ChessMove rootMove;
   public final int score;   

   public ChessSearchNodeResult(ChessSearchPhase searchPhase, ChessSearchNode searchNode, ChessMove rootMove, int score) {
      this.searchBoard = searchNode.getBoard();
      this.searchNode = searchNode;
      this.searchPhase = searchPhase;
      this.rootMove = rootMove;
      this.score = score;      
   }

   @Override
   public ChessSide getSide(){
      return searchNode.getSide();
   }
   
   @Override
   public ChessSearchPhase getPhase() {
      return searchPhase;
   }
   
   public ChessMove getRootMove() {
      return rootMove; // this is null on the horizon
   }
   
   @Override
   public ChessMove getMove() {
      return searchBoard.getFirstMove(); 
   }
   
   @Override
   public int getScore() {
      return score;
   }   
   
   @Override
   public int getCount(){
      return searchNode.getMoveCount();
   }
   
   @Override
   public String toString() {
      StringBuilder builder = new StringBuilder();
         
      builder.append("phase: ");
      builder.append(searchPhase);
      builder.append("\n");
      builder.append("direction: ");
      builder.append(searchNode.moveSide);
      builder.append("\n");      
      builder.append("score: ");
      builder.append(score);
      builder.append("\n");      
      builder.append("node: ");
      builder.append(searchNode.moveCount);                
      builder.append("\n");      
      builder.append("count: ");
      builder.append(searchNode.moveCounter);
      
      return builder.toString();
   }
}
