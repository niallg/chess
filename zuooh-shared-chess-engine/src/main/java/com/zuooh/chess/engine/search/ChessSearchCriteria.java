package com.zuooh.chess.engine.search;

import java.util.Collections;
import java.util.Set;

import com.zuooh.chess.ChessPieceType;

public class ChessSearchCriteria {
   
   public final Set<ChessSearchFeature> searchFeatures;
   public final ChessPieceType promotionType;
   public final long timeLimit;
   public final int featureMask;
   public final int searchDepth;
   
   public ChessSearchCriteria(Set<ChessSearchFeature> searchFeatures, ChessPieceType promotionType, int searchDepth) {
      this(searchFeatures, promotionType, searchDepth, Long.MAX_VALUE);
   }
   
   public ChessSearchCriteria(Set<ChessSearchFeature> searchFeatures,  ChessPieceType promotionType, int searchDepth, long timeLimit) {
      this.featureMask = ChessSearchFeature.createFeatureMask(searchFeatures);
      this.searchFeatures = Collections.unmodifiableSet(searchFeatures);
      this.promotionType = promotionType;
      this.searchDepth = searchDepth;
      this.timeLimit = timeLimit;
   }
   
   public boolean isTranspositionTableEnabled() {
      return ChessSearchFeature.TRANSPOSITION_TABLE.containsFeature(featureMask);
   }
   
   public boolean isNullMoveEnabled() {
      return ChessSearchFeature.NULL_MOVE.containsFeature(featureMask);
   }   
   
   public boolean isQuiescentSearchEnabled() {
      return ChessSearchFeature.QUIESCENT_SEARCH.containsFeature(featureMask);      
   }
   
   public boolean isOpeningBookEnabled() {
      return ChessSearchFeature.OPENING_BOOK.containsFeature(featureMask);      
   }   
   
   public boolean isIterativeDeepeningEnabled() {
      return ChessSearchFeature.ITERATIVE_DEEPENING.containsFeature(featureMask);  
   }   
   
   public boolean isMoveOrderingEnabled() {
      return ChessSearchFeature.MOVE_ORDERING.containsFeature(featureMask);  
   }       

   public ChessPieceType getPromotionType() {
      return promotionType;
   }   

   public int getSearchFeatures() {
      return featureMask;
   }

   public int getSearchDepth() {
      return searchDepth;
   }   

   public long getTimeLimit() {
      return timeLimit;
   }
   
   @Override
   public String toString() {
      return String.format("%s @ (%s %sms)", searchFeatures, searchDepth, timeLimit);
   }
}
