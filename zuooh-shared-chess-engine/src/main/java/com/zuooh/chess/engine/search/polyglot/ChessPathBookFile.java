package com.zuooh.chess.engine.search.polyglot;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ChessPathBookFile implements ChessBookFile {
   
   private final static Logger LOG = LoggerFactory.getLogger(ChessPathBookFile.class);
   
   private ChessBookFile bookFile;
   private File path;
   
   public ChessPathBookFile(String path) {
      this.path = new File(path);      
   }   

   @Override
   public synchronized ChessBookEntry read(long offset) {
      if(bookFile == null) {
         bookFile = loadBook();
      }
      if(bookFile != null) {
         return bookFile.read(offset);
      }
      return null;
   }

   @Override
   public synchronized long length() {
      if(bookFile == null) {
         bookFile = loadBook();
      }
      if(bookFile != null) {
         return bookFile.length();
      }
      return 0;
   }
   
   private synchronized ChessBookFile loadBook() {      
      try {
         FileInputStream source = new FileInputStream(path);
         
         try {
            ByteArrayOutputStream buffer = new ByteArrayOutputStream();
            byte[] chunk = new byte[1024];
            int count = 0;         
            
            while((count = source.read(chunk)) != -1) {
               buffer.write(chunk, 0, count);
            }
            byte[] bookData = buffer.toByteArray();
            return  new ChessMemoryBookFile(bookData);
         } finally {
            source.close();
         }
      } catch(Exception e) {
         LOG.info("Could not load book " + path, e);
      }
      return null;
   }

}
