package com.zuooh.chess.engine.search;

public enum ChessTranspositionType {
   LOWER_BOUND(0x01),
   EXACT(0x02),
   UPPER_BOUND(0x03);
   
   public final int mask;
   
   private ChessTranspositionType(int mask){
      this.mask = mask;
   }   
   
   public static ChessTranspositionType resolveType(int mask) {
      if((mask & LOWER_BOUND.mask) == LOWER_BOUND.mask) {
         return LOWER_BOUND;
      }
      if((mask & EXACT.mask) == EXACT.mask) {
         return EXACT;
      }
      if((mask & UPPER_BOUND.mask) == UPPER_BOUND.mask) {
         return UPPER_BOUND;
      }
      throw new IllegalStateException("Could not determine type for " + mask);
   }
}
