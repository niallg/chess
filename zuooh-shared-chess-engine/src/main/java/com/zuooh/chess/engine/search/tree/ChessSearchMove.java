package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.ChessMoveType;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.common.collections.Weight;

public class ChessSearchMove implements Weight {

   private final ChessSearchNode moveNode;
   private final ChessMoveType moveType;  
   private final ChessMove move;
   private final int score;

   public ChessSearchMove(ChessSearchNode moveNode, ChessMoveType moveType, ChessMove move, int score) {
      this.moveNode = moveNode;
      this.moveType = moveType;
      this.score = score;   
      this.move = move;
   }
   
   public ChessSearchNode getNode() {
      return moveNode;
   }
   
   public ChessMoveType getMoveType() {
      return moveType;
   }
   
   public ChessMove getMove() {
      return move;
   }

   public int getScore() {
      return score;
   }

   @Override
   public long getWeight() {
      return score;
   }

   @Override
   public String toString() {
      return String.format("%s -> %s (%s %s): %s", move.from, move.to, moveType, move.side, score);
   }
}
