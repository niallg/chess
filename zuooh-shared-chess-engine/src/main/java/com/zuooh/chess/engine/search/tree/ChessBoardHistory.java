package com.zuooh.chess.engine.search.tree;

import java.util.ArrayList;
import java.util.List;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;

public class ChessBoardHistory {

   private final List<ChessMoveRecord> moveHistory;
   private final long moveExpiryTime;
   private final int historyCapacity;

   public ChessBoardHistory(ChessBoard board) {
      this(board, 600000, 200);
   }
   
   public ChessBoardHistory(ChessBoard board, long moveExpiryTime, int historyCapacity) {
      this.moveHistory = new ArrayList<ChessMoveRecord>(historyCapacity + 2);
      this.moveExpiryTime = moveExpiryTime;
      this.historyCapacity = historyCapacity;
   }

   public synchronized void saveMove(ChessBoard beforeMove, ChessMove moveMade, long beforeMoveHash) { // hash of board move will be applied to
      if (repeatableMove(beforeMove, moveMade)) {
         ChessMoveRecord moveRecord = new ChessMoveRecord(moveMade, beforeMoveHash);
         int moveLimit = Math.max(2, historyCapacity);
         int moveCount = moveHistory.size();

         if (moveCount >= moveLimit) {
            moveHistory.remove(moveCount - 1); // remove oldest entry
         }
         moveHistory.add(0, moveRecord); // shift everything up
      }
   }
   
   public synchronized boolean recentMove(ChessBoard beforeMove, ChessMove moveToMake) {
      long currentTime = System.currentTimeMillis();
      int moveCount = moveHistory.size();

      for (int i = 0; i < moveCount; i++) {
         ChessMoveRecord moveRecord = moveHistory.get(i);
         long moveAge = currentTime - moveRecord.moveTime;
         
         if (moveAge < moveExpiryTime) { // check is it a valid move
            if (sameMove(moveRecord.moveMade, moveToMake)) {
               return i <= 5; // if only 5 moves have passed its recent
            }
         }
      }
      return false;
   }   

   public synchronized int countRepeatMoves(ChessBoard beforeMove, ChessMove moveToMake, long beforeMoveHash) { // hash of board move will be applied to
      long currentTime = System.currentTimeMillis();
      int moveCount = moveHistory.size();
      int repeatCount = 0;

      for (int i = 0; i < moveCount; i++) {
         ChessMoveRecord moveRecord = moveHistory.get(i);
         long moveAge = currentTime - moveRecord.moveTime;
         
         if (moveRecord.boardHash == beforeMoveHash && moveAge < moveExpiryTime) { // check is it a valid move
            if (sameMove(moveRecord.moveMade, moveToMake)) {
               repeatCount++;
            }
         }
      }
      return repeatCount;
   }

   private synchronized boolean repeatableMove(ChessBoard beforeMove, ChessMove moveMade) {
      ChessPiece pieceTaken = beforeMove.getPiece(moveMade.to);
      ChessPiece pieceMoved = beforeMove.getPiece(moveMade.from);
      int moveCount = beforeMove.getMoveCount(pieceMoved);

      if (pieceTaken != null) {
         return false;
      }
      if (moveCount <= 1) {
         return false;
      }
      return true;
   }

   private synchronized boolean sameMove(ChessMove leftMove, ChessMove rightMove) {
      if (leftMove == rightMove) {
         return true;
      }
      if (leftMove == null && rightMove == null) {
         return true;
      }
      if (leftMove != null && rightMove != null) {
         ChessSide leftDirection = leftMove.getSide();
         ChessSide rightDirection = rightMove.getSide();

         if (leftDirection != rightDirection) {
            return false;
         }
         ChessBoardPosition leftFrom = leftMove.getFrom();
         ChessBoardPosition rightFrom = rightMove.getFrom();

         if (leftFrom != rightFrom) {
            return false;
         }
         ChessBoardPosition leftTo = leftMove.getTo();
         ChessBoardPosition rightTo = rightMove.getTo();

         if (leftTo != rightTo) {
            return false;
         }
         return true;
      }
      return false;
   }

   public synchronized void clearMoves() {
      moveHistory.clear();
   }

   private static class ChessMoveRecord {

      private final ChessMove moveMade;
      private final long moveTime;
      private final long boardHash;

      public ChessMoveRecord(ChessMove moveMade, long boardHash) {
         this.moveTime = System.currentTimeMillis();
         this.boardHash = boardHash;
         this.moveMade = moveMade;
      }
      
      @Override
      public String toString() {
         return String.format("%s (%s@%s)", moveMade, boardHash, moveTime);
      }
   }
}
