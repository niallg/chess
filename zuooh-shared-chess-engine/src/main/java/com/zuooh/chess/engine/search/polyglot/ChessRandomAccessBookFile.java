package com.zuooh.chess.engine.search.polyglot;

import java.io.RandomAccessFile;

public class ChessRandomAccessBookFile implements ChessBookFile {
   
   private final RandomAccessFile file;
   
   public ChessRandomAccessBookFile(RandomAccessFile file) {
      this.file = file;
   }

   @Override
   public ChessBookEntry read(long offset) {
      try {
         file.seek(offset);
         
         long hash = file.readLong();
         short move = file.readShort();
         short weight = file.readShort();
         int learn = file.readInt();
         
         return new ChessBookEntry(hash, move, weight, learn);
      } catch(Exception e) {
         throw new IllegalStateException("Error occured reading book file", e);
      }
   }

   @Override
   public long length() {
      try {
         return (int)file.length();
      } catch(Exception e) {
         throw new IllegalStateException("Error occured reading book file", e);
      }
   }

}
