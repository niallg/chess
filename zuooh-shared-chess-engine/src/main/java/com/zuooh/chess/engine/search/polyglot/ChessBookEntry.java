package com.zuooh.chess.engine.search.polyglot;

public class ChessBookEntry {
   
   public final long hash;
   public final short move;
   public final short weight;
   public final int learn;
   
   public ChessBookEntry(long hash, short move, short weight, int learn) {
      this.hash = hash;
      this.move = move;
      this.weight = weight;
      this.learn = learn;
   }
   
   public ChessBookPosition getFromPosition() {
      return new ChessBookPosition((move>>6) &0x7, (move>>9) &0x7);
   }
   
   public ChessBookPosition getToPosition() {
      return new ChessBookPosition(move & 0x7, (move>>3) & 0x7);
   }
   
   @Override
   public String toString() {
      return "hash=" + hash + " move="+move + " weight="+weight;
   }
}
