package com.zuooh.chess.engine.search;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;

public class ChessTranspositionEntry {

   private final long boardHash;
   private final int bestScore;
   private final int entryData; // [depth-10-bits][type=2-bits][from=6-bits][to=6-bits][version=8-bits]

   public ChessTranspositionEntry(ChessTranspositionType entryType, ChessBoardPosition fromPosition, ChessBoardPosition toPosition, long boardHash, int bestScore, int achievedDepth, byte entryVersion) {
      this.entryData = (achievedDepth << 22) | (entryType.mask << 20) | (fromPosition.index << 14) | (toPosition.index << 8) | entryVersion;
      this.boardHash = boardHash;
      this.bestScore = bestScore;     
   }
   
   public ChessMove getMove(ChessBoard chessBoard, ChessSide playDirection) {
      ChessBoardPosition toPosition = getTo();
      ChessBoardPosition fromPosition = getFrom();    
      ChessSide fromOccupation = chessBoard.getSide(fromPosition);
      ChessSide toOccupation = chessBoard.getSide(toPosition);      
      int changeCount = chessBoard.getChangeCount();
      
      if(fromOccupation == playDirection && toOccupation != playDirection) { // do a small bit of validation
         return new ChessMove(fromPosition, toPosition, playDirection, changeCount);
      }
      return null;
   }

   public ChessTranspositionType getType() {
      return ChessTranspositionType.resolveType(entryData >> 20);
   }
   
   public ChessBoardPosition getFrom() {
      return ChessBoardPosition.at((entryData >> 14) & 0x3F);
   } 
   
   public ChessBoardPosition getTo() {
      return ChessBoardPosition.at((entryData >> 8) & 0x3F);
   } 

   public long getBoardHash() {
      return boardHash;
   }
   
   public int getValue() {
      return bestScore;
   }

   public int getDepth() {
      return entryData >>> 22;
   }   
   
   public byte getVersion(){
      return (byte)(entryData & 0xff);
   }   
}
