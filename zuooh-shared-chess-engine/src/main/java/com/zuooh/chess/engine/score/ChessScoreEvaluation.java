package com.zuooh.chess.engine.score;

import com.zuooh.chess.ChessPieceType;

public interface ChessScoreEvaluation {    
   int STALE_MATE = 0;
   int CHECK_MATE = 300000; 
   int INFINITY  = 10000000; // Integer.MIN_VALUE * -1 == Integer.MIN_VALUE so it does not work                                              
   int LOWEST_POSSIBLE_SCORE = -INFINITY; // initial alpha
   int HIGHEST_POSSIBLE_SCORE = INFINITY; // initial beta   
   int PAWN_VALUE = 100;
   int KNIGHT_VALUE = 320;
   int BISHOP_VALUE = 330;
   int ROOK_VALUE = 500;
   int QUEEN_VALUE = 1050; // value from Crafty, override 900 default assumption
   int KING_VALUE = CHECK_MATE;   
   
   int valueOf(ChessPieceType pieceType);
}