package com.zuooh.chess.engine.search;

import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.common.collections.Weight;

public class ChessSearchResponse implements Weight {

   public final ChessSearchResult searchResult;
   public final ChessBoard startBoard;
   public final ChessMove chessMove;
   public final long searchDuration;
   public final int searchDepth;
   
   public ChessSearchResponse(ChessSearchResult searchResult, ChessBoard startBoard, ChessMove chessMove, int searchDepth, long searchDuration) {
      this.searchDuration = searchDuration;
      this.searchResult = searchResult;
      this.searchDepth = searchDepth;
      this.startBoard = startBoard;
      this.chessMove = chessMove;
   }

   public ChessSearchResult getResult() {
      return searchResult;
   }

   public ChessMove getMove() {
      return chessMove;
   }

   public int getDepth() {
      return searchDepth;
   }

   @Override
   public long getWeight() {    
      return searchResult.getScore();
   }
   
   @Override
   public String toString() {
      StringBuilder builder = new StringBuilder();
      
      builder.append("\n");
      builder.append(startBoard);    
      builder.append("\n");
      builder.append("depth: ");
      builder.append(searchDepth);      
      builder.append("\n");
      builder.append(searchResult);
      builder.append("\n");
      builder.append("duration: ");
      builder.append(searchDuration);
      builder.append("\n");       
      
      if(chessMove != null) {
         builder.append("perspective: ");
         builder.append(chessMove.side);
         builder.append("\n");
         builder.append("result: ");
         builder.append(chessMove);
         builder.append("\n");
      }
      return builder.toString(); 
   }
}
