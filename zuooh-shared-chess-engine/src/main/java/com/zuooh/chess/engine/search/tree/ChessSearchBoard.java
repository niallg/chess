package com.zuooh.chess.engine.search.tree;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.engine.ChessMove;

public interface ChessSearchBoard {
   ChessBoard getBoard();  
   ChessBoardMove getBoardMove(); 
   ChessMove getLastMove();
   ChessSide getLastSide();   
   ChessMove getFirstMove();
   ChessSide getFirstSide();
   long getBoardHash();
   long getPreviousHash();
}

