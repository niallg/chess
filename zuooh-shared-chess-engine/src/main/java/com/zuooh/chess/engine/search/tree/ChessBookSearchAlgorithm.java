package com.zuooh.chess.engine.search.tree;

import java.util.Collections;
import java.util.Set;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.ChessMoveMode;
import com.zuooh.chess.engine.search.ChessSearchAlgorithm;
import com.zuooh.chess.engine.search.ChessSearchCriteria;
import com.zuooh.chess.engine.search.ChessSearchFeature;
import com.zuooh.chess.engine.search.ChessSearchResponse;
import com.zuooh.chess.engine.search.ChessSearchPhase;
import com.zuooh.chess.engine.search.ChessSearchResult;
import com.zuooh.chess.engine.search.ChessSearchTracer;
import com.zuooh.chess.engine.search.polyglot.ChessBook;

public class ChessBookSearchAlgorithm implements ChessSearchAlgorithm {
   
   private final ChessSearchAlgorithm search;
   private final AtomicBoolean bookExhausted;
   private final AtomicInteger moveCounter;
   private final ChessBook openBook;
   private final int limitMoves;
   
   public ChessBookSearchAlgorithm(ChessBook openBook, ChessSearchAlgorithm search, ChessSearchTracer searchTracer) {
      this(openBook, search, searchTracer, 5);
   }
   
   public ChessBookSearchAlgorithm(ChessBook openBook, ChessSearchAlgorithm search, ChessSearchTracer searchTracer, int limitMoves) {
      this.bookExhausted = new AtomicBoolean();
      this.moveCounter = new AtomicInteger();
      this.limitMoves = limitMoves;
      this.openBook = openBook;
      this.search = search;
   }

   @Override
   public void stopSearch() {
      moveCounter.set(0);
      bookExhausted.set(false); // in case back was hit
      search.stopSearch();
   }

   @Override
   public ChessSearchResponse searchBoard(ChessBoard chessBoard, ChessSide playDirection) {
      int currentCount = moveCounter.getAndIncrement();
      
      if(!bookExhausted.get() && currentCount < limitMoves) {
         ChessMove chessMove = openBook.createMove(chessBoard, playDirection);
         Set<ChessSearchFeature> searchFeatures = Collections.singleton(ChessSearchFeature.EVERYTHING);
         long startTime = System.currentTimeMillis();
         
         if(chessMove != null) {
            AtomicInteger nodeCounter = new AtomicInteger();
            ChessSearchBoard moveBoard = new ChessRootSearchBoard(chessBoard, playDirection);
            ChessSearchCriteria searchCriteria = new ChessSearchCriteria(searchFeatures, ChessPieceType.QUEEN, 0);
            ChessSearchNodeBuilder nodeBuilder = new ChessSearchNodeBuilder(searchCriteria, ChessMoveMode.BASIC);
            ChessSearchNode searchNode = new ChessSearchNode(nodeBuilder, moveBoard, playDirection, nodeCounter, 0, 0, 0);
            ChessSearchResult result = new ChessSearchNodeResult(ChessSearchPhase.OPEN_BOOK, searchNode, null, 0);
            long finishTime = System.currentTimeMillis();
            long searchDuration = finishTime - startTime;
                  
            return new ChessSearchResponse(result, chessBoard, chessMove, 0, searchDuration);
         }
         bookExhausted.set(true);
      }
      return search.searchBoard(chessBoard, playDirection);
   }
   
   
   
   
   
   

}
