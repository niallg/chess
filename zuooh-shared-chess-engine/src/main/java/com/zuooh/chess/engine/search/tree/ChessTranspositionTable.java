package com.zuooh.chess.engine.search.tree;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;

import com.zuooh.chess.engine.ChessMove;
import com.zuooh.chess.engine.search.ChessTranspositionEntry;
import com.zuooh.chess.engine.search.ChessTranspositionType;

public class ChessTranspositionTable {
   
   private final ChessTranspositionEntry[] entryTable;
   private final AtomicInteger currentVersion;
   private final AtomicLong previousTime;
   private final long expiryTime;

   public ChessTranspositionTable() {
      this(600000);
   }
   
   public ChessTranspositionTable(long expiryTime) {
      this.entryTable = new ChessTranspositionEntry[100000];
      this.currentVersion = new AtomicInteger();
      this.previousTime = new AtomicLong();
      this.expiryTime = expiryTime;
   }
   
   public synchronized ChessTranspositionEntry createEntry(ChessSearchNode searchNode, ChessTranspositionType entryType, ChessMove bestMove, int bestScore) {
      ChessSearchBoard searchBoard = searchNode.getBoard();
      
      if(bestMove == null) {
         throw new IllegalStateException("Node does not have a move for " + searchNode);
      }
      int achievedDepth = searchNode.getRemainingDepth(); // how deep has this node dived?
      long boardHash = searchBoard.getBoardHash();
      
      if(boardHash == 0) {
         throw new IllegalStateException("Board does not have a hash for " + searchBoard);
      }
      long currentTime = System.currentTimeMillis();
      long timeStamp = previousTime.get();
      
      if(currentTime - timeStamp >= expiryTime) {
         currentVersion.getAndIncrement(); // gradually increase this version
         previousTime.set(currentTime);
      }  
      int tableVersion = currentVersion.get();
      byte tableEntryVersion = (byte)(tableVersion % 255); // a byte that tracks time, helps us replace old transposition entries
      int tableIndex = (int)Math.abs(boardHash % entryTable.length);
      ChessTranspositionEntry entry = entryTable[tableIndex];
      
      if(entry != null) {
         long currentHash = entry.getBoardHash();
         byte currentVersion = entry.getVersion();
         int currentDepth = entry.getDepth(); 
         
         if(currentHash != boardHash) {
            entry = null; // replace different hash
         } else if(achievedDepth >= currentDepth) {
            entry = null; // replace low quality entry
         } else if(tableEntryVersion > currentVersion + 1) {
            entry = null; // replace old/expired entry
         }
      } 
      if(entry == null) {
         ChessTranspositionEntry newEntry = new ChessTranspositionEntry(entryType, bestMove.from, bestMove.to, boardHash, bestScore, achievedDepth, tableEntryVersion);
         entryTable[tableIndex] = newEntry; // store or replace entry
         return entry;
      }
      return entry; // table had a fresh entry that had a higher quality score (i.e deeper)
   }
   
   public synchronized ChessTranspositionEntry findEntry(ChessSearchNode searchNode) {      
      int requiredDepth = searchNode.getRemainingDepth(); // how deep will it dive
      
      if(requiredDepth >= 0) {
         return findEntry(searchNode, requiredDepth);
      }
      return null;
   }
   
   public synchronized ChessTranspositionEntry findEntry(ChessSearchNode searchNode, int minimumDepth) {
      ChessSearchBoard searchBoard = searchNode.getBoard();     
      long boardHash = searchBoard.getBoardHash();
      
      if(boardHash == 0) {
         throw new IllegalStateException("Board does not have a hash for " + searchBoard);
      }
      int tableIndex = (int)Math.abs(boardHash % entryTable.length);
      ChessTranspositionEntry entry = entryTable[tableIndex];
      
      if(entry != null) {        
         int achievedDepth = entry.getDepth(); // how deep did it dive when stored
         
         if(achievedDepth < minimumDepth) {
            return null; // result is not high enough quality
         }
         long entryHash = entry.getBoardHash();
         
         if(entryHash != boardHash) {           
            return null;
         }  
      }
      return entry;
   }
}
