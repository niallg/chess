package com.zuooh.chess.engine.score;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;

public interface ChessScoreStrategy extends ChessScoreEvaluation {
   int score(ChessBoard chessBoard, ChessSide playDirection);
}
