package com.zuooh.chess.database.theme;

import java.io.Serializable;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardStatus;

public class ChessThemeDefinition implements Serializable {

   private static final long serialVersionUID = 1L;
   
   private final String finishFlipScreen;
   private final String finishScreen;
   private final String flipScreen;   
   private final String screen;
   private final String theme;
   private final String color;
   
   public ChessThemeDefinition(String color, String screen, String flipScreen, String finishScreen, String finishFlipScreen, String theme) {
      this.finishFlipScreen = finishFlipScreen;
      this.finishScreen = finishScreen;
      this.flipScreen = flipScreen;
      this.screen = screen;
      this.theme = theme;
      this.color = color;
   }
   
   public String getScreen(ChessSide side) {
      return getScreen(side, ChessBoardStatus.NORMAL);
   }
   
   public String getScreen(ChessSide side, ChessBoardStatus status) {
      if(status.isGameOver()) {
         if(side == ChessSide.WHITE) {
            return finishScreen;
         }
         return finishFlipScreen;         
      }
      if(side == ChessSide.WHITE) {
         return screen;
      }
      return flipScreen;
      
   }
   
   public String getTheme() {
      return theme;
   }
   
   public String getColor() {
      return color;
   }
   
   @Override
   public String toString() {
      return String.format("%s: %s %s", screen, color, theme);
   }
}
