package com.zuooh.chess.database.notify;

import java.util.List;

import com.zuooh.database.bind.DatabaseBinder;

public class ChessNotificationDatabase {
   
   private final DatabaseBinder binder;
   
   public ChessNotificationDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   public synchronized List<ChessNotification> listNotifications() throws Exception {
      return binder.withTable(ChessNotification.class)
               .select()
               .execute()
               .fetchAll();
   }
   
   public synchronized List<ChessNotification> listUserNotifications(String userId) throws Exception {
      return binder.withTable(ChessNotification.class)
            .select()
            .where("userId == :userId")
            .set("userId", userId)
            .execute()
            .fetchAll();
   }      
   
   public synchronized List<ChessNotification> listUserNotifications(String userId, long sendTime) throws Exception {
      return binder.withTable(ChessNotification.class)
            .select()
            .where("userId == :userId and sendTime <= :sendTime and received == :received")
            .set("received", false)
            .set("userId", userId) 
            .set("sendTime", sendTime)   
            .execute()
            .fetchAll();
   }   
   
   public synchronized List<ChessNotification> listUserNotifications(String userId, String gameId) throws Exception {
      return binder.withTable(ChessNotification.class)
            .select()
            .where("userId == :userId and gameId == :gameId and received == :received")
            .set("received", false)
            .set("userId", userId) 
            .set("gameId", gameId)   
            .execute()
            .fetchAll();
   }
   
   public synchronized List<ChessNotification> listOpponentNotifications(String userId, String gameId) throws Exception {
      return binder.withTable(ChessNotification.class)
            .select()
            .where("userId != :userId and gameId == :gameId and received == :received")
            .set("received", false)
            .set("userId", userId) 
            .set("gameId", gameId)   
            .execute()
            .fetchAll();
   }   
   
   public synchronized void markUserNotificationsAsReceived(String userId, String gameId) throws Exception {
      List<ChessNotification> notifications = binder.withTable(ChessNotification.class)
            .select()
            .where("userId == :userId and gameId == :gameId and received == :received")
            .set("received", false)
            .set("userId", userId) 
            .set("gameId", gameId)   
            .execute()
            .fetchAll();
      
      for(ChessNotification notification : notifications) {
         notification.setReceived(true);
         binder.withTable(ChessNotification.class)
            .updateOrInsert()
            .execute(notification);
      }
   }  
   
   public synchronized void markOpponentNotificationsAsReceived(String userId, String gameId) throws Exception {
      List<ChessNotification> notifications = binder.withTable(ChessNotification.class)
            .select()
            .where("userId != :userId and gameId == :gameId and received == :received")
            .set("received", false)
            .set("userId", userId) 
            .set("gameId", gameId)   
            .execute()
            .fetchAll();
      
      for(ChessNotification notification : notifications) {
         notification.setReceived(true);
         binder.withTable(ChessNotification.class)
            .updateOrInsert()
            .execute(notification);
      }
   }   
   
   public synchronized void saveNotification(ChessNotification notification) throws Exception {
      binder.withTable(ChessNotification.class)
            .updateOrInsert()
            .execute(notification);
   }
   
   public synchronized void deleteNotifications(String gameId) throws Exception {
      List<ChessNotification> notifications = binder.withTable(ChessNotification.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)   
            .execute()
            .fetchAll();
      
      for(ChessNotification notification : notifications) {      
         binder.withTable(ChessNotification.class)
            .delete()
            .execute(notification);
      }      
   }     
}

