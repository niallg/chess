package com.zuooh.chess.database.notify;

public class ChessNotificationBuilder {

   private final String smallIcon;
   private final String normalIcon;
   private final boolean vibrate;
   private final long retry;   

   public ChessNotificationBuilder(String normalIcon, String smallIcon){
      this(normalIcon, smallIcon, 5 * 60 * 1000);
   }
   
   public ChessNotificationBuilder(String normalIcon, String smallIcon, long retry){
      this(normalIcon, smallIcon, retry, true);
   }
   
   public ChessNotificationBuilder(String normalIcon, String smallIcon, long retry, boolean vibrate){
      this.normalIcon = normalIcon;
      this.smallIcon = smallIcon; 
      this.vibrate = vibrate;    
      this.retry = retry;
   } 

   public ChessNotification createNotification(ChessNotificationType notificationType, String gameId, String userId) {
      ChessNotification notification = new ChessNotification(notificationType, gameId, userId);      
      
      notification.setSequence(1);
      notification.setRetry(retry);
      notification.setVibrate(vibrate);
      notification.setSmallIcon(smallIcon);           
      notification.setNormalIcon(normalIcon);
          
      long currentTime = System.currentTimeMillis();      
      
      notification.setSendTime(currentTime);
      notification.setCreationTime(currentTime);     
      
      return notification;
   }
}
