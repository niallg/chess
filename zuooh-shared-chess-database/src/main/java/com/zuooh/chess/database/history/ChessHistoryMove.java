package com.zuooh.chess.database.history;

import java.io.Serializable;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;

public class ChessHistoryMove implements Serializable {   

   private static final long serialVersionUID = 1L;

   public final ChessBoardPosition takePosition;
   public final ChessBoardPosition fromPosition;
   public final ChessBoardPosition toPosition;
   public final ChessPieceKey fromPiece;
   public final ChessPieceKey takePiece;
   public final ChessSide side;
   public final int changeCount;

   public ChessHistoryMove(ChessBoardPosition fromPosition, ChessBoardPosition toPosition, ChessBoardPosition takePosition, ChessPieceKey fromPiece, ChessPieceKey takePiece, ChessSide side, int changeCount) {
      this.takePosition = takePosition;
      this.fromPosition = fromPosition;
      this.toPosition = toPosition;
      this.changeCount = changeCount;
      this.fromPiece = fromPiece;
      this.takePiece = takePiece;
      this.side = side;      
 
   }
   
   public ChessMove getMove() {
      return new ChessMove(fromPosition, toPosition, side, changeCount);
   }

   @Override
   public String toString() {
      if(takePiece != null) {
         return String.format("%s[%s] -> %s taken=%s@%s", fromPiece, fromPosition, toPosition, takePiece, takePosition);
      }
      return String.format("%s[%s] -> %s", fromPiece, fromPosition, toPosition); 
   }
}
