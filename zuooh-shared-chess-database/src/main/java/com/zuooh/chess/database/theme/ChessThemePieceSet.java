package com.zuooh.chess.database.theme;

import java.util.Map;

import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;

public class ChessThemePieceSet {

   private Map<ChessThemePieceKey, ChessThemePiece> iconChessSet;

   public ChessThemePieceSet(Map<ChessThemePieceKey, ChessThemePiece> iconChessSet) {
      this.iconChessSet = iconChessSet;
   }

   public ChessThemePiece getBoardPiece(ChessPieceKey pieceKey, ChessSide playDirection) {
      ChessThemePieceKey key = new ChessThemePieceKey(pieceKey.type, playDirection);
      return iconChessSet.get(key);
   }

   public ChessThemePiece getBoardPiece(ChessPiece chessPiece) {
      if(chessPiece == null) {
         return null;
      }
      ChessThemePieceKey key = new ChessThemePieceKey(chessPiece);
      return iconChessSet.get(key);
   }
}
