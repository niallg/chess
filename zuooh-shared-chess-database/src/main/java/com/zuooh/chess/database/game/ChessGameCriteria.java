package com.zuooh.chess.database.game;

import java.io.Serializable;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.user.ChessUser;

public class ChessGameCriteria implements Serializable {

   private static final long serialVersionUID = 1L;
   
   private final ChessSide userSide;
   private final ChessUser user;
   private final String themeColor;
   private final long creationTime;
   private final long gameDuration;

   public ChessGameCriteria(ChessUser user, ChessSide userSide, String themeColor, long gameDuration, long creationTime) {
      this.creationTime = creationTime;
      this.gameDuration = gameDuration;
      this.themeColor = themeColor;
      this.userSide = userSide;
      this.user = user;      
   }   

   public ChessUser getUser() {
      return user;
   }
   
   public ChessSide getUserSide() {
      return userSide;
   }
   
   public String getThemeColor() {
      return themeColor;
   }
   
   public long getGameDuration() {
      return gameDuration;
   }

   public long getCreationTime() {
      return creationTime;
   }
   
   @Override
   public String toString() {
      return userSide + ": " +themeColor;
   }
}
