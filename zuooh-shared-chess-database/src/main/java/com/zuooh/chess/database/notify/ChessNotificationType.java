package com.zuooh.chess.database.notify;

public enum ChessNotificationType {
   GAME_INVITE("Game Invite", "Game invitation from %{text}", "resumeGameFocus", "splash,home", "selectController.showGame('%{game}')", 1),
   GAME_START("Game Started", "Game has started against %{text}", "resumeGameFocus", "splash,home", "selectController.showGame('%{game}')", 1),
   MOVE_MADE("Move Made", "Move made by %{text}", "resumeGameFocus", "splash,home", "selectController.showGame('%{game}')", 2),
   GAME_OVER_WIN("Game Over", "You won against %{text}", "resumeGameFocus", "splash,home", "selectController.showGame('%{game}')", 3),
   GAME_OVER_LOSE("Game Over", "You lost to %{text}", "resumeGameFocus", "splash,home", "selectController.showGame('%{game}')", 3),
   GAME_OVER_DRAW("Game Over", "Draw between you and %{text}", "resumeGameFocus", "splash,home", "selectController.showGame('%{game}')", 3);
   
   public final String function;
   public final String template;
   public final String title;
   public final String page;
   public final String home;
   public final int type;
   
   private ChessNotificationType(String title, String template, String page, String home, String function, int type) {
      this.function = function;
      this.template = template;
      this.title = title; 
      this.page = page;
      this.home = home;
      this.type = type;
   }
   
   public boolean isInvite() {
      return this == GAME_INVITE;
   }
   
   public boolean isStart() {
      return this == GAME_START;
   }
   
   public boolean isMove() {
      return this == MOVE_MADE;
   }
   
   public boolean isWin() {
      return this == GAME_OVER_WIN;
   }

   public boolean isLose() {
      return this == GAME_OVER_WIN;
   }
   
   public boolean isDraw() {
      return this == GAME_OVER_WIN;
   }
   
   public boolean isFinish() {
      return this == GAME_OVER_DRAW || this == GAME_OVER_LOSE || this == GAME_OVER_WIN;
   }
}
