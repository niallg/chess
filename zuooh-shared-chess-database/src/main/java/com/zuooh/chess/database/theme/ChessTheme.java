package com.zuooh.chess.database.theme;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.style.image.ImageManager;

public class ChessTheme {

   private final Map<ChessBoardPosition, ChessPiece> bottomToTop;
   private final Map<ChessBoardPosition, ChessPiece> topToBottom;
   private final List<ChessThemePieceImage> pieces;
   private final List<ChessThemePiecePosition> positions;

   public ChessTheme(List<ChessThemePieceImage> pieces, List<ChessThemePiecePosition> positions) {
      this.bottomToTop = new HashMap<ChessBoardPosition, ChessPiece>();      
      this.topToBottom = new HashMap<ChessBoardPosition, ChessPiece>();   
      this.positions = positions;
      this.pieces = pieces;  
   }
  
   public ChessPieceSet getPieceImageSet() {
      if(topToBottom.isEmpty() && bottomToTop.isEmpty()) {
         for(ChessThemePiecePosition position : positions) {
            ChessBoardPosition boardPosition = position.getPosition();
            String pieceName = position.getPieceName();
            ChessThemePieceImage piece = getPiece(pieceName);
   
            if(piece == null) {
               throw new IllegalStateException("Could not find piece for '" + pieceName + "' at position " + position);
            }
            ChessPiece chessPiece  = piece.getChessPiece(boardPosition);
            ChessSide playDirection = chessPiece.getSide();
   
            if(playDirection == ChessSide.WHITE) {
               bottomToTop.put(boardPosition, chessPiece);
            } else {
               topToBottom.put(boardPosition, chessPiece);
            }
         }
      }
      return new DelegatePieceSet(bottomToTop, topToBottom);
   } 

   public ChessThemePieceSet getBoardPieceSet(ImageManager imageManager) {
      Map<ChessThemePieceKey, ChessThemePiece> iconChessSet = new HashMap<ChessThemePieceKey, ChessThemePiece>();

      for(ChessThemePiecePosition position : positions) {
         ChessBoardPosition boardPosition = position.getPosition();
         String pieceName = position.getPieceName();
         ChessThemePieceImage piece = getPiece(pieceName);
         ChessThemePiece iconChessPiece = piece.getPiece(imageManager);
         ChessPiece chessPiece  = piece.getChessPiece(boardPosition);

         if(chessPiece != null) {
            ChessThemePieceKey pieceKey = new ChessThemePieceKey(chessPiece);            
            iconChessSet.put(pieceKey, iconChessPiece);
         }
      }
      return new ChessThemePieceSet(iconChessSet);
   }   
   
   public ChessBoard getChessBoard() {
      ChessPieceSet pieceSet = getPieceImageSet();
      
      if(pieceSet != null) {
         return new ChessPieceSetBoard(pieceSet);
      }
      return null;
   }  
   
   public ChessThemePieceImage getPiece(String name) {
      for(ChessThemePieceImage piece : pieces) {
         String key = piece.getName();
         
         if(key.equals(name)) {
            return piece;
         }         
      }
      return null;
   }
   
   private static class DelegatePieceSet implements ChessPieceSet {

      private final Map<ChessBoardPosition, ChessPiece> bottomToTop;
      private final Map<ChessBoardPosition, ChessPiece> topToBottom;
      
      public DelegatePieceSet(Map<ChessBoardPosition, ChessPiece> bottomToTop, Map<ChessBoardPosition, ChessPiece> topToBottom) {
         this.bottomToTop = bottomToTop;
         this.topToBottom = topToBottom;      
      }
      
      @Override
      public Map<ChessBoardPosition, ChessPiece> getPieces(ChessSide playDirection) {
         if(playDirection == ChessSide.WHITE) {
            return bottomToTop;
         }
         return topToBottom;
      } 
   }
}
