package com.zuooh.chess.database.user;

import java.io.Serializable;

public class ChessUserData implements Serializable {   

   private static final long serialVersionUID = 1L;

   private final ChessUserStatus status;
   private final long lastSeenOnline;   
   private final int blackGames;
   private final int whiteGames;
   private final int rank;
   private final int skill;
   
   public ChessUserData() {
      this(ChessUserStatus.BEGINNER, 0, 0, 0, 0, 0);
   }
   
   public ChessUserData(ChessUserStatus status, int rank, int skill, int blackGames, int whiteGames, long lastSeenOnline) {
      this.lastSeenOnline = lastSeenOnline;
      this.blackGames = blackGames;
      this.whiteGames = whiteGames;
      this.status = status;
      this.skill = skill;
      this.rank = rank;
   }

   public ChessUserStatus getStatus() {
      return status;
   }

   public int getRank() {
      return rank;
   }
   
   public int getSkill() {
      return skill;
   }

   public int getBlackGames() {
      return blackGames;
   }

   public int getWhiteGames() {
      return whiteGames;
   }

   public long getLastSeenOnline() {
      return lastSeenOnline;
   }
}
