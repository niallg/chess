package com.zuooh.chess.database.state;

import java.io.Serializable;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;

public class ChessMoveState implements Serializable {

   private static final long serialVersionUID = 1L;

   private ChessBoardPosition fromPosition;
   private ChessBoardPosition toPosition;
   private ChessSide userSide;
   private String userId;
   private String gameId;
   private long timeStamp;
   private boolean acknowledged;
   private int changeCount;   

   public ChessMoveState(String gameId, String userId, ChessSide userSide, ChessBoardPosition fromPosition, ChessBoardPosition toPosition, long timeMoveMade, int changeCount) {
      this.gameId = gameId;
      this.userSide = userSide;
      this.userId = userId;
      this.fromPosition = fromPosition;
      this.toPosition = toPosition;
      this.timeStamp = timeMoveMade;
      this.changeCount = changeCount;
   }   
   
   public ChessMove getMove() {
      return new ChessMove(fromPosition, toPosition, userSide, changeCount);
   }
   
   public String getMoveId() {
      return gameId + "@" + changeCount;
   }
   
   public int getChangeCount() {
      return changeCount;
   }

   public String getGameId() {
      return gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }

   public ChessSide getUserSide() {
      return userSide;
   }

   public String getUserId() {
      return userId;
   }

   public ChessBoardPosition getFromPosition() {
      return fromPosition;
   }

   public ChessBoardPosition getToPosition() {
      return toPosition;
   }

   public long getTimeMoveMade() {
      return timeStamp;
   }

   public boolean isAcknowledged() {
      return acknowledged;
   }

   public void setAcknowledged(boolean acknowledged) {
      this.acknowledged = acknowledged;
   }
   
   @Override
   public String toString() {
      return String.format("%s -> %s (%s %s)", fromPosition, toPosition, userSide, changeCount);
   }
}
