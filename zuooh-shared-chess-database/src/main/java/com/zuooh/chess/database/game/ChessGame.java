package com.zuooh.chess.database.game;

import java.io.Serializable;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;

public class ChessGame implements Serializable {

   private static final long serialVersionUID = 1L;
   
   private ChessGameCriteria criteria;   
   private ChessGameStatus status;
   private ChessSide sideToMove;
   private ChessMove lastMove;
   private String gameId;
   private String ownerId;

   public ChessGame(String gameId, ChessGameStatus status, ChessGameCriteria criteria, ChessMove lastMove, ChessSide sideToMove, String ownerId) {
      this.sideToMove = sideToMove;
      this.status = status;
      this.criteria = criteria;
      this.ownerId = ownerId;
      this.lastMove = lastMove;
      this.gameId = gameId;
   }

   public ChessMove getLastMove() {
      return lastMove;
   }
   
   public ChessGameStatus getStatus() {
      return status;
   }

   public ChessSide getSideToMove() {
      return sideToMove;
   }

   public ChessGameCriteria getCriteria(){
      return criteria;
   }

   public String getOwnerId() {
      return ownerId;
   }
   
   public String getGameId() {
      return gameId;
   }
   
   @Override
   public String toString() {
      return gameId + ": (" + status + ")";
   }
}
