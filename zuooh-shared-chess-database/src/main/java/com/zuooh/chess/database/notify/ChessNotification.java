package com.zuooh.chess.database.notify;

import java.io.Serializable;

public class ChessNotification implements Serializable { 
   
   private static final long serialVersionUID = 1L;

   private ChessNotificationType notificationType;
   private String notificationText;
   private String gameId;
   private String userId;   
   private String smallIcon;
   private String normalIcon;
   private boolean received;
   private boolean vibrate;
   private long retryDelay; 
   private long creationTime;
   private long sendTime;
   private int sequence;
   
   public ChessNotification(ChessNotificationType notificationType, String gameId, String userId){
      this.notificationType = notificationType;
      this.gameId = gameId;
      this.userId = userId;
   }   

   public ChessNotificationType getType() {
      return notificationType;
   }

   public void setType(ChessNotificationType notificationType) {
      this.notificationType = notificationType;
   } 
   
   public String getText() {
      return notificationText;
   }
   
   public void setText(String notificationText) {
      this.notificationText = notificationText;
   } 
   
   public String getGameId() {
      return gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }
   
   public String getUserId() {
      return userId;
   }

   public void setUserId(String userId) {
      this.userId = userId;
   }  

   public String getSmallIcon() {
      return smallIcon;
   }

   public void setSmallIcon(String smallIcon) {
      this.smallIcon = smallIcon;
   }

   public String getNormalIcon() {
      return normalIcon;
   }

   public void setNormalIcon(String normalIcon) {
      this.normalIcon = normalIcon;
   }   

   public boolean isVibrate() {
      return vibrate;
   }

   public void setVibrate(boolean vibrate) {
      this.vibrate = vibrate;
   }
   
   public boolean isReceived() {
      return received;
   }

   public void setReceived(boolean received) {
      this.received = received;
   }   
   
   public long getCreationTime() {
      return creationTime;
   }

   public void setCreationTime(long creationTime) {
      this.creationTime = creationTime;
   }
   
   public long getSendTime() {
      return sendTime;
   }

   public void setSendTime(long sendTime) {
      this.sendTime = sendTime;
   }   

   public long getRetry() {
      return retryDelay;
   }

   public void setRetry(long retry) {
      this.retryDelay = retry;
   }

   public int getSequence() {
      return sequence;
   }

   public void setSequence(int sequence) {
      this.sequence = sequence;
   }

   @Override
   public String toString() {
      return String.format("type=%s, text=%s, retry=%s", notificationType, notificationText, retryDelay);
   }
}
