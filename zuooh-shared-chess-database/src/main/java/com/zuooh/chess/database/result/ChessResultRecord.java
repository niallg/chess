package com.zuooh.chess.database.result;

import java.io.Serializable;

import com.zuooh.chess.board.ChessBoardResult;

public class ChessResultRecord implements Serializable {
   
   private static final long serialVersionUID = 1L;
   
   private ChessBoardResult result;
   private String gameId;
   private String userId;
   private long resultTime;
   private long alertTime;
   
   public ChessResultRecord(ChessBoardResult result, String gameId, String userId) {
      this.result = result;
      this.gameId = gameId;
      this.userId = userId;
   }
   
   public ChessBoardResult getResult() {
      return result;
   }
   
   public String getGameId() {
      return gameId;
   }

   public String getUserId() {
      return userId;
   }

   public long getResultTime() {
      return resultTime;
   }

   public void setResultTime(long resultTime) {
      this.resultTime = resultTime;
   }

   public long getAlertTime() {
      return alertTime;
   }

   public void setAlertTime(long alertTime) {
      this.alertTime = alertTime;
   }   
}
