package com.zuooh.chess.database.theme;

import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;

public class ChessThemePieceKey {

   private final ChessPieceType pieceType;
   private final ChessSide playDirection;

   public ChessThemePieceKey(ChessPiece chessPiece) {
      this(chessPiece.key.type, chessPiece.side);
   }

   public ChessThemePieceKey(ChessPieceType pieceType, ChessSide playDirection) {
      this.pieceType = pieceType;
      this.playDirection = playDirection;
   }

   @Override
   public boolean equals(Object other) {
      if(other instanceof ChessThemePieceKey) {
         return equals((ChessThemePieceKey)other);
      }
      return false;
   }
   
   private boolean equals(ChessThemePieceKey other) {
      return other.pieceType == pieceType && other.playDirection == playDirection;
   }

   @Override
   public int hashCode() {
      return pieceType.hashCode() ^ playDirection.hashCode();
   }
}
