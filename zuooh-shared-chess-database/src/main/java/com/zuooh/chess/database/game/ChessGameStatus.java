package com.zuooh.chess.database.game;

public enum ChessGameStatus {
   NEW("Game created with no specific challenger"),
   AWAITING_ACCEPTENCE("A specific person has been challenged to a new game"),
   IN_PROGRESS("A game has two players and is in progress"),
   REJECTED("Game invitation was rejected"),   
   RESIGNED("One player has decided to resign and the game is over"),
   GAME_OVER("Game has ended due to a check mate or a stale mate");

   public final String description;

   private ChessGameStatus(String description) {
      this.description = description;
   }
   
   public boolean isAccepted() {
      return this == IN_PROGRESS;
   }
   
   public boolean isInvitation() {
      return this == AWAITING_ACCEPTENCE;
   }
   
   public boolean isRejected() {
      return this == REJECTED;
   }   
      
   public boolean isResigned() {
      return this == RESIGNED;
   }
   
   public boolean isGameOver() {
      return this == GAME_OVER || this == RESIGNED || this == REJECTED;
   }

   public static ChessGameStatus resolveStatus(String token) {
      if(token != null) {
         for(ChessGameStatus gameStatus : values()) {
            String enumName = gameStatus.name();

            if(token.equals(enumName)) {
               return gameStatus;
            }
         }
      }
      return null;
   }

}
