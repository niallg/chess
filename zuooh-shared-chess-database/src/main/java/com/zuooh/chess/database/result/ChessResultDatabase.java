package com.zuooh.chess.database.result;

import java.util.List;

import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.database.board.ChessBoardGame;
import com.zuooh.database.bind.DatabaseBinder;

public class ChessResultDatabase {

   private final DatabaseBinder binder;
   
   public ChessResultDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   public synchronized List<ChessResultRecord> listResults(String userId) throws Exception {
      return binder.withTable(ChessResultRecord.class)
         .select()
         .where("userId == :userId")
         .set("userId", userId)
         .orderBy("resultTime desc")            
         .execute()
         .fetchAll();
   }   
   
   public synchronized List<ChessResultRecord> listResultsOlderThan(String userId, long duration) throws Exception {
      long currentTime = System.currentTimeMillis();
      long limitTime = currentTime - duration;
      
      return binder.withTable(ChessResultRecord.class)
         .select()
         .where("userId == :userId and resultTime < :limitTime")
         .set("userId", userId)         
         .set("limitTime", limitTime)
         .orderBy("resultTime desc")            
         .execute()
         .fetchAll();
   }  
   
   public synchronized List<ChessResultRecord> listResultsNewerThan(String userId, long duration) throws Exception {
      long currentTime = System.currentTimeMillis();
      long limitTime = currentTime - duration;
      
      return binder.withTable(ChessResultRecord.class)
         .select()
         .where("userId == :userId and resultTime >= :limitTime")
         .set("userId", userId)              
         .set("limitTime", limitTime)
         .orderBy("resultTime desc")            
         .execute()
         .fetchAll();
   }      
   
   public synchronized ChessResultRecord loadResult(String userId, String gameId) throws Exception {
      return binder.withTable(ChessResultRecord.class)
         .select()
         .where("userId == :userId and gameId == :gameId")
         .set("userId", userId)
         .set("gameId", gameId)                            
         .execute()
         .fetchFirst();
   }
   
   public synchronized void updateResult(ChessResultRecord resultRecord) throws Exception {
      String gameId = resultRecord.getGameId();
      String userId = resultRecord.getUserId();
      ChessResultRecord existingRecord = loadResult(userId, gameId);
      
      if(existingRecord != null) {
         binder.withTable(ChessResultRecord.class)
            .update()
            .execute(resultRecord); 
      }
   }  
   
   public synchronized void saveResult(ChessBoardGame boardGame) throws Exception {
      ChessResultRecord resultRecord = boardGame.getResultRecord();
      ChessBoardResult boardResult = resultRecord.getResult();
      
      if(boardResult.isFinal()) {
         long currentTime = System.currentTimeMillis();
         
         resultRecord.setResultTime(currentTime);
         binder.withTable(ChessResultRecord.class)
            .insertOrIgnore()
            .execute(resultRecord); 
      }
   }
   
   public synchronized void saveResult(ChessResultRecord resultRecord) throws Exception {
      ChessBoardResult boardResult = resultRecord.getResult();
      
      if(boardResult.isFinal()) {
         long currentTime = System.currentTimeMillis();
         
         resultRecord.setResultTime(currentTime);
         binder.withTable(ChessResultRecord.class)
            .insertOrIgnore()
            .execute(resultRecord);  
      }
   }
   
   public synchronized void deleteResult(ChessResultRecord resultRecord) throws Exception {
      binder.withTable(ChessResultRecord.class)
         .delete()
         .execute(resultRecord);  
   }   
}

