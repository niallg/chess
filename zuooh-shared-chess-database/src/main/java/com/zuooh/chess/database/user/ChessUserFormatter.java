package com.zuooh.chess.database.user;

public class ChessUserFormatter {
   
   private static final String ANONYMOUS_USER = "Anonymous";
   private static final String UNKNOWN_USER = "Unknown";
   private static final String YOU_USER = "You";     
   
   public static String formatUser(ChessUser user) {
      return formatUser(user, false);
   }
   
   public static String formatUser(ChessUser user, boolean you) {
      if(!you) {
         if(user != null) {
            ChessUserType type = user.getType();
            String name = user.getName();
            
            if(type.isAnonymous()) {
               return ANONYMOUS_USER;
            }
            return formatName(name);
         }
         return UNKNOWN_USER;
      }
      return YOU_USER;
   }
   
   public static String formatName(String name) {
      return formatName(name, false);
   }
   
   public static String formatName(String name, boolean you) {
      if(!you) {
         StringBuilder builder = new StringBuilder();
         
         if(name != null) {
            String text = name.replace("_", " ");
            String[] tokens = text.split("\\s+");
            String separator = "";
            
            for(String token : tokens) {      
               int length = token.length();
               
               if(length > 0) {
                  String end = token.substring(1);
                  String start = token.substring(0, 1);
                  String capital = start.toUpperCase();
                  
                  builder.append(separator);
                  builder.append(capital);
                  builder.append(end);
                  separator = " ";
               }
            }
            return builder.toString();
         }
         return UNKNOWN_USER;
      }
      return YOU_USER;
   }   
}
