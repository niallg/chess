package com.zuooh.chess.database.user;

import java.io.Serializable;

import com.zuooh.chess.database.user.ChessUserType;

public class ChessUserLogin implements Serializable {
   
   private static final long serialVersionUID = 1L;
   
   private ChessUserType type;
   private String key;
   private long time;

   public ChessUserLogin(ChessUserType type, String key, long time) {
      this.time = time;
      this.key = key;
      this.type = type;
   }
   
   public ChessUserType getUserType() {
      return type;
   }
   
   public void setUserType(ChessUserType type) {
      this.type = type;
   }
   
   public String getUserKey() {
      return key;
   }
   
   public void setUserKey(String key) {
      this.key = key;
   }
   
   public long getTime() {
      return time;
   }
   
   public void setTime(long time) {
      this.time = time;
   }
}