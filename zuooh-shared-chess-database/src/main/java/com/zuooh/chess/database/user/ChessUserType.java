package com.zuooh.chess.database.user;

public enum ChessUserType {
   TRANSIENT('T'),
   COMPUTER('C'),
   ANONYMOUS('A'),
   INVALID('I'),   
   NORMAL('N');
   
   public final char code;
   
   private ChessUserType(char code) {
      this.code = code;
   }
   
   public boolean isAnonymous() {
      return this == ANONYMOUS || this == TRANSIENT;
   }
   
   public boolean isInvalid() {
      return this == INVALID;
   }   
   
   public boolean isComputer() {
      return this == COMPUTER;
   }
   
   public boolean isNormal() {
      return this == NORMAL;
   }
}
