package com.zuooh.chess.database.theme;

import java.io.Serializable;

import com.zuooh.chess.ChessBoardPosition;

public class ChessThemePiecePosition implements Serializable {

   private static final long serialVersionUID = 1L;

   private final String square;
   private final String name;
   
   public ChessThemePiecePosition(String name, String square) {
      this.name = name;
      this.square = square;
   }

   public String getPieceName() {
      return name;
   }

   public ChessBoardPosition getPosition() {
      return ChessBoardPosition.at(square);
   }
   
   @Override
   public String toString() {
      return String.format("%s: %s", name, square);
   }
}
