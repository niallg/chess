package com.zuooh.chess.database.theme;

import java.util.List;

import com.zuooh.database.bind.DatabaseBinder;

public class ChessThemeDatabase {
   
   private final DatabaseBinder binder;
   
   public ChessThemeDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   public synchronized ChessThemeDefinition loadThemeDefinition(String color) throws Exception {
      return binder.withTable(ChessThemeDefinition.class)
         .select()
         .where("color == :color")
         .set("color", color)
         .execute()
         .next();
   }
   
   public synchronized List<ChessThemeDefinition> loadThemeDefinitions() throws Exception {
      return binder.withTable(ChessThemeDefinition.class)
            .select()
            .execute()
            .fetchAll();
   }
   
   public synchronized ChessTheme loadTheme(String color) throws Exception {
      String name = binder.withTable(ChessThemeDefinition.class)
            .select()
            .where("color == :color")
            .set("color", color)
            .execute()
            .next()
            .getTheme();
   
      List<ChessThemePieceImage> images = binder.withTable(ChessThemePieceImage.class)
            .select()
            .where("theme == :theme")
            .set("theme", name)
            .execute()
            .fetchAll();        
      
      List<ChessThemePiecePosition> positions = binder.withTable(ChessThemePiecePosition.class)
            .select()
            .execute()
            .fetchAll();  

      return new ChessTheme(images, positions);
   }
}
