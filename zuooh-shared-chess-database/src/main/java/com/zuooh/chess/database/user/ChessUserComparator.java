package com.zuooh.chess.database.user;

import java.util.Comparator;

public class ChessUserComparator implements Comparator<ChessUser> {

   @Override
   public int compare(ChessUser left, ChessUser right) {
      ChessUserData leftData = left.getData();
      ChessUserData rightData = right.getData();
      Integer leftRank = leftData.getRank();
      Integer rightRank = rightData.getRank();
      
      if(leftRank == 0) {
         leftRank = Integer.MAX_VALUE;
      }
      if(rightRank == 0) {
         rightRank = Integer.MAX_VALUE;
      }
      return leftRank.compareTo(rightRank);
   }
}
