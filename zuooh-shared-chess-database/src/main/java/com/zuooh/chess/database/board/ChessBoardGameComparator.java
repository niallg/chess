package com.zuooh.chess.database.board;

import java.util.Comparator;

public class ChessBoardGameComparator implements Comparator<ChessBoardGame> {

   @Override
   public int compare(ChessBoardGame left, ChessBoardGame right) {
      Long rightTimeStamp = right.getCreationTime();
      Long leftTimeStamp = left.getCreationTime();
      Long rightDuration = right.getGameDuration();
      Long leftDuration = left.getGameDuration();
      Long rightAge = rightTimeStamp + rightDuration;
      Long leftAge = leftTimeStamp + leftDuration;
      
      return leftAge.compareTo(rightAge);
   }

}
