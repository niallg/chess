package com.zuooh.chess.database.user;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.zuooh.database.bind.DatabaseBinder;

public class ChessUserDatabase {
   
   private final DatabaseBinder binder;
   
   public ChessUserDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }   
   
   public synchronized ChessUser loadUser(String key) throws Exception {
      validateUserId(key);
      return binder.withTable(ChessUser.class)
            .select()
            .where("key == :key")
            .set("key", key)
            .execute()
            .fetchFirst();
   }
   
   public synchronized List<ChessUser> loadUsers() throws Exception {
      return binder.withTable(ChessUser.class)
            .select()
            .execute()
            .fetchAll();  
   }
   
   public synchronized List<ChessUser> loadUsers(ChessUserType type) throws Exception {
      return binder.withTable(ChessUser.class)
            .select()
            .where("type == :type")
            .set("type", type)         
            .execute()
            .fetchAll();     
   }    
   
   public synchronized List<ChessUser> loadUsersWithBestRank(int count) throws Exception {
      return binder.withTable(ChessUser.class)
            .select()
            .orderBy("rank desc")
            .execute()
            .fetchNext(count);  
   }
   
   public synchronized List<ChessUser> loadUsersMatching(String term) throws Exception {
      return loadUsersMatching(term, false);
   }    
   
   public synchronized List<ChessUser> loadUsersMatching(String term, boolean includeKeys) throws Exception {
      String token = term.toLowerCase();
      List<ChessUser> users = binder.withTable(ChessUser.class)
            .select()
            .execute()
            .fetchAll();
      
      if(!users.isEmpty()) {
         Map<String, ChessUser> userMatches = new LinkedHashMap<String, ChessUser>();  
         
         for(ChessUser profile : users) {
            String userKey = profile.getKey();
            String userName = profile.getName();
            String userMail = profile.getMail();
            
            if(userName != null) {
               String userSearchText = userName.toLowerCase();
               
               if(userSearchText.indexOf(token) != -1) {
                  userMatches.put(userKey, profile);
               }
            }           
            if(userMail != null) {
               if(userMail.equalsIgnoreCase(token)) {
                  userMatches.put(userKey, profile);
               }
            }
            if(includeKeys) {
               String userSearchText = userKey.toLowerCase();
               
               if(userSearchText.indexOf(token) != -1) {
                  userMatches.put(userKey, profile);
               }
            }             
         }
         Collection<ChessUser> matchedUsers = userMatches.values();
         
         if(!matchedUsers.isEmpty()) {
            return new ArrayList<ChessUser>(matchedUsers);
         }
      }
      return Collections.emptyList();
   }    
   
   public synchronized List<ChessUser> loadUsersByLoginTime() throws Exception {      
      List<ChessUserLogin> loginTimes = binder.withTable(ChessUserLogin.class)
            .select()
            .orderBy("time desc")
            .execute()
            .fetchAll();     
      
      if(!loginTimes.isEmpty()) {
         List<ChessUser> users = new ArrayList<ChessUser>();
         
         for(ChessUserLogin login : loginTimes) {
            String userId = login.getUserKey();
            ChessUser user = loadUser(userId);
            
            if(user != null) {
               users.add(user);
            }
         }
         return users;
      }
      return Collections.emptyList();
   }        
   
   public synchronized List<ChessUser> loadUsersByLoginTime(ChessUserType type) throws Exception {
      List<ChessUserLogin> loginTimes = binder.withTable(ChessUserLogin.class)
            .select()
            .where("type == :type")
            .set("type", type)
            .orderBy("time desc")
            .execute()
            .fetchAll();     
      
      if(!loginTimes.isEmpty()) {
         List<ChessUser> users = new ArrayList<ChessUser>();
         
         for(ChessUserLogin login : loginTimes) {
            String userId = login.getUserKey();
            ChessUser user = loadUser(userId);
            
            if(user != null) {
               users.add(user);
            }
         }
         return users;
      }
      return Collections.emptyList(); 
   }
   
   public synchronized void deleteUser(ChessUser user) throws Exception {
      validateUser(user);
      binder.withTable(ChessUser.class)
         .delete()           
         .execute(user);
   }
   
   public synchronized boolean containsUser(String key) throws Exception {
      validateUserId(key);
      return !binder.withTable(ChessUser.class)
            .select()
            .where("key == :key")
            .set("key", key)
            .execute()
            .isEmpty();
   }
   
   public synchronized void saveUser(ChessUser user) throws Exception {
      validateUser(user);
      binder.withTable(ChessUser.class)
         .updateOrInsert()
         .execute(user);     
   }

   public synchronized void saveUserLogin(ChessUserLogin login) throws Exception {
      validateUserLogin(login);
      binder.withTable(ChessUserLogin.class)
         .updateOrInsert()
         .execute(login);     
   }
   
   public synchronized void deleteUserLogin(ChessUserLogin activity) throws Exception {
      validateUserLogin(activity);
      binder.withTable(ChessUserLogin.class)
         .delete()
         .execute(activity);     
   }    
   
   public synchronized void validateUserId(String key) throws Exception {
      if(key == null) {
         throw new IllegalArgumentException("User key must not be null");
      }
      if(!key.toLowerCase().equals(key)) {
         throw new IllegalArgumentException("User key '" + key + "' must be in lower case");
      }
      if(key.equals("")) {
         throw new IllegalArgumentException("User key '" + key + "' is not valid");
      }
   }
   
   public synchronized void validateUser(ChessUser user) throws Exception {
      String userName = user.getName();
      String key = user.getKey();
      
      if(key == null) {
         throw new IllegalArgumentException("User " + user + " has a null key");
      }
      if(userName == null) {
         throw new IllegalArgumentException("User " + user + " has a null user name");
      }     
      if(userName.equals("")) {
         throw new IllegalArgumentException("User " + user + " has an empty user name");
      }
   }
   
   public synchronized void validateUserLogin(ChessUserLogin login) throws Exception {
      String key = login.getUserKey();
      
      if(key == null) {
         throw new IllegalArgumentException("User login " + login + " has a null key");
      }
      ChessUser user = loadUser(key);
      
      if(user == null) {
         throw new IllegalArgumentException("User login " + login + " references an unknown user");
      }     
   }   
}
