package com.zuooh.chess.database.state;

import java.util.List;

import com.zuooh.database.bind.DatabaseBinder;

public class ChessGameStateDatabase {
   
   private final DatabaseBinder binder;
   
   public ChessGameStateDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   public synchronized List<ChessGameState> listGames() throws Exception {
      return binder.withTable(ChessGameState.class)
               .select()
               .execute()
               .fetchAll();
   }
   
   public synchronized List<ChessGameState> listGamesPlayedBy(String userId) throws Exception {
      return binder.withTable(ChessGameState.class)
            .select()
            .where("challengerId == :userId or ownerId == :userId")
            .set("userId", userId)
            .execute()
            .fetchAll();
   }
   
   public synchronized List<ChessGameState> listGamesCreatedBy(String userId) throws Exception {
      return binder.withTable(ChessGameState.class)
            .select()
            .where("ownerId == :ownerId")
            .set("ownerId", userId)
            .execute()
            .fetchAll();
   }
   
   public synchronized List<ChessGameState> listGamesInvitedTo(String userId) throws Exception {
      return binder.withTable(ChessGameState.class)
            .select()
            .where("challengerId == :challengerId")
            .set("challengerId", userId)
            .execute()
            .fetchAll();
   }
   
   public synchronized ChessGameState loadGame(String gameId) throws Exception {      
      return binder.withTable(ChessGameState.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)             
            .execute()
            .fetchFirst();  
   }
   
   public synchronized List<ChessMoveState> listMoves(String gameId) throws Exception {
      return binder.withTable(ChessMoveState.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .orderBy("timeStamp asc")  // oldest first          
            .execute()
            .fetchAll();
   }
   
   public synchronized List<ChessMoveState> listMoves(String gameId, int limit) throws Exception {
      return binder.withTable(ChessMoveState.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .orderBy("timeStamp asc")  // oldest first
            .limit(limit)
            .execute()
            .fetchAll();
   }     

   public synchronized List<ChessMoveState> listMovesLatestFirst(String gameId) throws Exception {
      return binder.withTable(ChessMoveState.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .orderBy("timeStamp desc")  // latest first
            .execute()
            .fetchAll();
   }
   
   public synchronized List<ChessMoveState> listMovesLatestFirst(String gameId, int limit) throws Exception {
      return binder.withTable(ChessMoveState.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .orderBy("timeStamp desc")  // latest first
            .limit(limit)
            .execute()
            .fetchAll();
   }    
   
   public synchronized ChessMoveState lastMoveMadeBy(String gameId, String userId) throws Exception {
      return binder.withTable(ChessMoveState.class)
            .select()
            .where("gameId == :gameId and userId == :userId")
            .set("gameId", gameId)
            .set("userId", userId)
            .orderBy("timeStamp desc")            
            .execute()
            .fetchFirst();
   }
   
   public synchronized ChessMoveState lastMoveMadeByOpponent(String gameId, String userId) throws Exception {
      return binder.withTable(ChessMoveState.class)
            .select()
            .where("gameId == :gameId and userId != :userId")
            .set("gameId", gameId)
            .set("userId", userId)   
            .orderBy("timeStamp desc")
            .execute()
            .fetchFirst();
   }
   
   public synchronized ChessMoveState nextMove(String gameId, String userId) throws Exception {
      return binder.withTable(ChessMoveState.class)
            .select()
            .where("gameId == :gameId and userId != :userId and acknowledged == :acknowledged")
            .set("gameId", gameId)
            .set("userId", userId)   
            .set("acknowledged", false)
            .orderBy("timeStamp desc")
            .execute()
            .fetchFirst();
   }
   
   public synchronized ChessMoveState lastMove(String gameId) throws Exception {
      return binder.withTable(ChessMoveState.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .orderBy("timeStamp desc")
            .execute()
            .fetchFirst();            
   }
   
   public synchronized void deleteGame(ChessGameState game) throws Exception {
      String gameId = game.getGameId();
      List<ChessMoveState> moveStates = listMoves(gameId);
      
      for(ChessMoveState moveState : moveStates) {
         binder.withTable(ChessMoveState.class)
            .delete()
            .execute(moveState);
      }
      binder.withTable(ChessGameState.class)
            .delete()
            .execute(game);
   }
   
   public synchronized void deleteMove(ChessMoveState move) throws Exception {
      binder.withTable(ChessMoveState.class)
            .delete()
            .execute(move);
   }        

   public synchronized void saveGame(ChessGameState game) throws Exception {
      binder.withTable(ChessGameState.class)
            .updateOrInsert()
            .execute(game);
   }
   
   public synchronized void saveMove(ChessMoveState move) throws Exception {
      binder.withTable(ChessMoveState.class)
            .updateOrInsert()
            .execute(move);
   }   
}
