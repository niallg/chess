package com.zuooh.chess.database;

import com.zuooh.chess.database.board.ChessBoardGameDatabase;
import com.zuooh.chess.database.chat.ChessChatDatabase;
import com.zuooh.chess.database.game.ChessGameDatabase;
import com.zuooh.chess.database.history.ChessHistoryDatabase;
import com.zuooh.chess.database.result.ChessResultDatabase;
import com.zuooh.chess.database.theme.ChessThemeDatabase;
import com.zuooh.chess.database.user.ChessUserDatabase;
import com.zuooh.database.bind.DatabaseBinder;

public class ChessDatabase {

   private final ChessBoardGameDatabase boardDatabase;
   private final ChessHistoryDatabase historyDatabase;
   private final ChessResultDatabase resultDatabase;   
   private final ChessThemeDatabase themeDatabase;
   private final ChessGameDatabase gameDatabase;
   private final ChessChatDatabase chatDatabase;
   private final ChessUserDatabase userDatabase;
   
   public ChessDatabase(DatabaseBinder binder) {
      this.userDatabase = new ChessUserDatabase(binder);
      this.boardDatabase = new ChessBoardGameDatabase(binder);
      this.historyDatabase = new ChessHistoryDatabase(binder);
      this.resultDatabase = new ChessResultDatabase(binder);
      this.themeDatabase = new ChessThemeDatabase(binder);
      this.gameDatabase = new ChessGameDatabase(binder);
      this.chatDatabase = new ChessChatDatabase(binder); 
   }
   
   public ChessChatDatabase getChatDatabase() {
      return chatDatabase;
   }

   public ChessUserDatabase getUserDatabase() {
      return userDatabase;
   }
   
   public ChessResultDatabase getResultDatabase() {
      return resultDatabase;
   }   

   public ChessBoardGameDatabase getBoardDatabase() {
      return boardDatabase;
   }

   public ChessHistoryDatabase getHistoryDatabase() {
      return historyDatabase;
   }

   public ChessThemeDatabase getThemeDatabase() {
      return themeDatabase;
   }
   
   public ChessGameDatabase getGameDatabase() {
      return gameDatabase;
   }
}
