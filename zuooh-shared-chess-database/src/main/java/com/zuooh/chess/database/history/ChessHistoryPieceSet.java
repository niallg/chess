package com.zuooh.chess.database.history;

import static com.zuooh.chess.ChessBoardPosition.A1;
import static com.zuooh.chess.ChessBoardPosition.A2;
import static com.zuooh.chess.ChessBoardPosition.A7;
import static com.zuooh.chess.ChessBoardPosition.A8;
import static com.zuooh.chess.ChessBoardPosition.B1;
import static com.zuooh.chess.ChessBoardPosition.B2;
import static com.zuooh.chess.ChessBoardPosition.B7;
import static com.zuooh.chess.ChessBoardPosition.B8;
import static com.zuooh.chess.ChessBoardPosition.C1;
import static com.zuooh.chess.ChessBoardPosition.C2;
import static com.zuooh.chess.ChessBoardPosition.C7;
import static com.zuooh.chess.ChessBoardPosition.C8;
import static com.zuooh.chess.ChessBoardPosition.D1;
import static com.zuooh.chess.ChessBoardPosition.D2;
import static com.zuooh.chess.ChessBoardPosition.D7;
import static com.zuooh.chess.ChessBoardPosition.D8;
import static com.zuooh.chess.ChessBoardPosition.E1;
import static com.zuooh.chess.ChessBoardPosition.E2;
import static com.zuooh.chess.ChessBoardPosition.E7;
import static com.zuooh.chess.ChessBoardPosition.E8;
import static com.zuooh.chess.ChessBoardPosition.F1;
import static com.zuooh.chess.ChessBoardPosition.F2;
import static com.zuooh.chess.ChessBoardPosition.F7;
import static com.zuooh.chess.ChessBoardPosition.F8;
import static com.zuooh.chess.ChessBoardPosition.G1;
import static com.zuooh.chess.ChessBoardPosition.G2;
import static com.zuooh.chess.ChessBoardPosition.G7;
import static com.zuooh.chess.ChessBoardPosition.G8;
import static com.zuooh.chess.ChessBoardPosition.H1;
import static com.zuooh.chess.ChessBoardPosition.H2;
import static com.zuooh.chess.ChessBoardPosition.H7;
import static com.zuooh.chess.ChessBoardPosition.H8;
import static com.zuooh.chess.ChessPieceKey.BLACK_BISHOP_C;
import static com.zuooh.chess.ChessPieceKey.BLACK_BISHOP_F;
import static com.zuooh.chess.ChessPieceKey.BLACK_KING;
import static com.zuooh.chess.ChessPieceKey.BLACK_KNIGHT_B;
import static com.zuooh.chess.ChessPieceKey.BLACK_KNIGHT_G;
import static com.zuooh.chess.ChessPieceKey.BLACK_PAWN_A;
import static com.zuooh.chess.ChessPieceKey.BLACK_PAWN_B;
import static com.zuooh.chess.ChessPieceKey.BLACK_PAWN_C;
import static com.zuooh.chess.ChessPieceKey.BLACK_PAWN_D;
import static com.zuooh.chess.ChessPieceKey.BLACK_PAWN_E;
import static com.zuooh.chess.ChessPieceKey.BLACK_PAWN_F;
import static com.zuooh.chess.ChessPieceKey.BLACK_PAWN_G;
import static com.zuooh.chess.ChessPieceKey.BLACK_PAWN_H;
import static com.zuooh.chess.ChessPieceKey.BLACK_QUEEN;
import static com.zuooh.chess.ChessPieceKey.BLACK_ROOK_A;
import static com.zuooh.chess.ChessPieceKey.BLACK_ROOK_H;
import static com.zuooh.chess.ChessPieceKey.WHITE_BISHOP_C;
import static com.zuooh.chess.ChessPieceKey.WHITE_BISHOP_F;
import static com.zuooh.chess.ChessPieceKey.WHITE_KING;
import static com.zuooh.chess.ChessPieceKey.WHITE_KNIGHT_B;
import static com.zuooh.chess.ChessPieceKey.WHITE_KNIGHT_G;
import static com.zuooh.chess.ChessPieceKey.WHITE_PAWN_A;
import static com.zuooh.chess.ChessPieceKey.WHITE_PAWN_B;
import static com.zuooh.chess.ChessPieceKey.WHITE_PAWN_C;
import static com.zuooh.chess.ChessPieceKey.WHITE_PAWN_D;
import static com.zuooh.chess.ChessPieceKey.WHITE_PAWN_E;
import static com.zuooh.chess.ChessPieceKey.WHITE_PAWN_F;
import static com.zuooh.chess.ChessPieceKey.WHITE_PAWN_G;
import static com.zuooh.chess.ChessPieceKey.WHITE_PAWN_H;
import static com.zuooh.chess.ChessPieceKey.WHITE_QUEEN;
import static com.zuooh.chess.ChessPieceKey.WHITE_ROOK_A;
import static com.zuooh.chess.ChessPieceKey.WHITE_ROOK_H;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class ChessHistoryPieceSet implements ChessPieceSet {
   
   private final List<ChessHistoryItem> historyItems;

   public ChessHistoryPieceSet() {
      this(Collections.EMPTY_LIST);
   }
   
   public ChessHistoryPieceSet(List<ChessHistoryItem> historyItems) {
      this.historyItems = historyItems;     
   }

   @Override
   public Map<ChessBoardPosition, ChessPiece> getPieces(ChessSide playSide) {
      Map<ChessBoardPosition, ChessPieceKey> chessSet = getCurrentPieces();    
      Set<ChessBoardPosition> boardPositions = chessSet.keySet();
      
      if(!boardPositions.isEmpty()) {
         Map<ChessBoardPosition, ChessPiece> chessPieces = new LinkedHashMap<ChessBoardPosition, ChessPiece>();
         
         for(ChessBoardPosition boardPosition : boardPositions) {
            ChessPieceKey chessPieceKey = chessSet.get(boardPosition);
            
            if(chessPieceKey.side == playSide) {
               ChessPiece chessPiece = new ChessPiece(chessPieceKey, playSide);
               chessPieces.put(boardPosition, chessPiece);               
            }
         }
         return chessPieces;
      }
      return Collections.emptyMap();
   }
   
   private Map<ChessBoardPosition, ChessPieceKey> getCurrentPieces() {
      Map<ChessBoardPosition, ChessPieceKey> chessSet = getDefaultPieces();
      
      for(ChessHistoryItem historyItem : historyItems) {
         ChessMove chessMove = historyItem.getMove();
         ChessMove castleMove = historyItem.getCastleMove();         
         ChessPieceKey chessPiece = chessSet.remove(chessMove.from);
         
         chessSet.put(chessMove.to, chessPiece);
         
         if(castleMove != null) {
            ChessPieceKey castlePiece = chessSet.remove(castleMove.from);
            
            if(castlePiece.type != ChessPieceType.ROOK) {
               throw new IllegalStateException("Castle move is not a rook for " + castleMove);
            }
            chessSet.put(chessMove.to, chessPiece);
         }
      }
      return chessSet;
   }
   
   private Map<ChessBoardPosition, ChessPieceKey> getDefaultPieces() {
      Map<ChessBoardPosition, ChessPieceKey> chessSet = new LinkedHashMap<ChessBoardPosition, ChessPieceKey>();
      chessSet.put(A7,BLACK_PAWN_A);
      chessSet.put(B7,BLACK_PAWN_B);
      chessSet.put(C7,BLACK_PAWN_C);
      chessSet.put(D7,BLACK_PAWN_D);
      chessSet.put(E7,BLACK_PAWN_E);
      chessSet.put(F7,BLACK_PAWN_F);
      chessSet.put(G7,BLACK_PAWN_G);
      chessSet.put(H7,BLACK_PAWN_H);
      chessSet.put(A8,BLACK_ROOK_A);
      chessSet.put(H8,BLACK_ROOK_H);
      chessSet.put(B8,BLACK_KNIGHT_B);
      chessSet.put(G8,BLACK_KNIGHT_G);
      chessSet.put(C8,BLACK_BISHOP_C);
      chessSet.put(F8,BLACK_BISHOP_F);
      chessSet.put(D8,BLACK_QUEEN);
      chessSet.put(E8,BLACK_KING);
      chessSet.put(A2,WHITE_PAWN_A);
      chessSet.put(B2,WHITE_PAWN_B);
      chessSet.put(C2,WHITE_PAWN_C);
      chessSet.put(D2,WHITE_PAWN_D);
      chessSet.put(E2,WHITE_PAWN_E);
      chessSet.put(F2,WHITE_PAWN_F);
      chessSet.put(G2,WHITE_PAWN_G);
      chessSet.put(H2,WHITE_PAWN_H);
      chessSet.put(A1,WHITE_ROOK_A);
      chessSet.put(H1,WHITE_ROOK_H);
      chessSet.put(B1,WHITE_KNIGHT_B);
      chessSet.put(G1,WHITE_KNIGHT_G);
      chessSet.put(C1,WHITE_BISHOP_C);
      chessSet.put(F1,WHITE_BISHOP_F);
      chessSet.put(D1,WHITE_QUEEN);
      chessSet.put(E1,WHITE_KING);
      return chessSet;
   }

}
