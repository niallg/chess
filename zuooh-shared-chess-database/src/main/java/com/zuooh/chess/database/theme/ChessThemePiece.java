package com.zuooh.chess.database.theme;

import com.zuooh.chess.ChessCellColor;
import com.zuooh.chess.ChessCellStatus;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.style.image.Image;

public class ChessThemePiece {

   public final ChessPieceType pieceType;
   public final ChessSide pieceSide;
   public final Image imageTaken;
   public final Image imageOnWhite;
   public final Image imageOnBlack;
   public final Image imageOnDarkGrey;
   public final Image imageShadow;
   public final Image imageUnderAttack;
   public final Image imageSelected;

   public ChessThemePiece(ChessPieceType pieceType, ChessSide pieceSide, Image imageOnWhite, Image imageOnBlack, Image imageOnDarkGrey, Image imageShadow, Image imageUnderAttack, Image imageSelected, Image imageTaken) {
      this.imageUnderAttack = imageUnderAttack;
      this.imageSelected = imageSelected;
      this.imageOnWhite = imageOnWhite;
      this.imageOnBlack = imageOnBlack;
      this.imageOnDarkGrey = imageOnDarkGrey;
      this.imageShadow = imageShadow;
      this.imageTaken = imageTaken;
      this.pieceType = pieceType;
      this.pieceSide = pieceSide;
   }

   public ChessPieceType getPieceType() {
      return pieceType;
   }

   public ChessSide getPieceSide() {
      return pieceSide;
   }

   public Image getImage(ChessCellStatus cellStatus, ChessCellColor cellColor) {
      if(cellStatus == ChessCellStatus.UNDER_ATTACK) {
         return imageUnderAttack;
      } else if(cellStatus == ChessCellStatus.SELECTED) {
         return imageSelected;
      } else if(cellStatus == ChessCellStatus.SHADOW) {
         return imageShadow;
      } else if(cellStatus == ChessCellStatus.TAKEN) {
         return imageTaken;
      } else {
         if(cellColor == ChessCellColor.LIGHT) {
            return imageOnWhite;
         }
         if(cellColor == ChessCellColor.DARK) {
            return imageOnBlack;
         }
         return imageOnDarkGrey;
      }
   }

   public int getHeight(ChessCellStatus cellStatus, ChessCellColor cellColor) {
      return getImage(cellStatus, cellColor).getHeight();
   }

   public int getWidth(ChessCellStatus cellStatus, ChessCellColor cellColor) {
      return getImage(cellStatus, cellColor).getWidth();
   }

   public String toString() {
      return pieceType.toString();
   }
}
