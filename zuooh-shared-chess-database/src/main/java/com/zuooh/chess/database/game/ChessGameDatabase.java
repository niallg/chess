package com.zuooh.chess.database.game;

import java.util.List;

import com.zuooh.database.bind.DatabaseBinder;

public class ChessGameDatabase {

   private final DatabaseBinder binder;
   
   public ChessGameDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   public synchronized void deleteGame(ChessGame game) throws Exception {
      binder.withTable(ChessGame.class)
         .delete()           
         .execute(game);
   }
   
   public synchronized boolean containsGame(String gameId) throws Exception {
      return !binder.withTable(ChessGame.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .execute()
            .isEmpty();
   }
   
   public synchronized void saveGame(ChessGame game) throws Exception {
      binder.withTable(ChessGame.class)
         .updateOrInsert()
         .execute(game);     
   }
   
   public synchronized ChessGame loadGame(String gameId) throws Exception {
      return binder.withTable(ChessGame.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .execute()
            .next();  
   }
   
   public synchronized List<ChessGame> loadInProgressGames() throws Exception {
      return binder.withTable(ChessGame.class)
            .select()
            .where("status == :progress or status == :waiting")
            .set("progress", ChessGameStatus.IN_PROGRESS)
            .set("waiting", ChessGameStatus.AWAITING_ACCEPTENCE)
            .execute()
            .fetchAll();  
   }
   
   public synchronized List<ChessGame> loadAvailableGames() throws Exception {
      return binder.withTable(ChessGame.class)
            .select()
            .where("status == :new or status == :waiting")
            .set("new", ChessGameStatus.NEW)
            .set("waiting", ChessGameStatus.AWAITING_ACCEPTENCE)
            .execute()
            .fetchAll();  
   }
   
   public synchronized void deleteAll() throws Exception {
      binder.withTable(ChessGame.class)
            .truncate()
            .execute();
   }
}
