package com.zuooh.chess.database.user;

public enum ChessUserStatus {
   BEGINNER("beginner", "New", 0, 0), 
   BEGINNER_LOSER("beginnerLoser", "Beginner", 2, 0), 
   BEGINNER_OK("beginnerOk", "Beginner", 2, 30), // can this ever be achieved?
   BEGINNER_GOOD("beginnerGood", "Good", 2, 70),
   EXPERIENCED_LOSER("experiencedLoser", "Veteran", 10, 0),
   EXPERIENCED_OK("experiencedOk", "Advanced", 15, 30),
   EXPERIENCED_GOOD("experiencedGood", "Expert", 30, 70),
   EXPERIENCED_EXCELLENT("experiencedExcellent", "Master", 50, 80);
   
   public final String statusDescription;
   public final String statusName;
   public final int gamesRequired;
   public final int abovePercentage;
   
   private ChessUserStatus(String statusName, String statusDescription, int gamesRequired, int abovePercentage) {
      this.statusDescription = statusDescription;
      this.gamesRequired = gamesRequired;
      this.abovePercentage = abovePercentage;
      this.statusName = statusName;
   }
   
   public static int calculateSkill(int gamesPlayed, int percentage) {
      return Math.round((percentage / 100.0f) * 5.0f);
   }
   
   public static ChessUserStatus calculateStatus(int gamesPlayed, int percentage) {
      ChessUserStatus result = BEGINNER;
      
      for(ChessUserStatus status : values()) {
         if(status.gamesRequired <= gamesPlayed && status.abovePercentage <= percentage) {
            result = status;
         }
      }
      return result;
   }
}
