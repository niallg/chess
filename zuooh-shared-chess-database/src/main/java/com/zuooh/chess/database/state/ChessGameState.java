package com.zuooh.chess.database.state;

import java.io.Serializable;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.database.game.ChessGameStatus;

public class ChessGameState implements Serializable {

   private static final long serialVersionUID = 1L;
   
   private ChessGameStatus gameStatus;
   private ChessSide lastMoveMadeBy;
   private ChessSide ownerSide;
   private String boardSet;
   private String gameId;
   private String ownerId;
   private String challengerId;  
   private String winnerId;
   private String declareId;
   private boolean userNotified;
   private long creationTime;
   private long gameDuration;

   public ChessGameState(String gameId, String ownerId, ChessSide ownerSide, String boardSet, long gameDuration, long creationTime) {
      this.gameStatus = ChessGameStatus.NEW;     
      this.ownerId = ownerId;
      this.ownerSide = ownerSide;
      this.creationTime = creationTime;
      this.gameDuration = gameDuration;
      this.boardSet = boardSet;
      this.gameId = gameId;
   }
   
   public long getGameDuration() {
      return gameDuration;
   }
   
   public String getBoardSet() {
      return boardSet;
   }

   public long getCreationTime() {
      return creationTime;
   }

   public ChessGameStatus getGameStatus() {
      return gameStatus;
   }

   public void setGameStatus(ChessGameStatus gameStatus) {
      this.gameStatus = gameStatus;
   }

   public String getGameId() {
      return gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }

   public String getOwnerId() {
      return ownerId;
   }

   public ChessSide getOwnerSide() {
      return ownerSide;
   }
   
   public String getDeclareId(){
      return declareId;
   }
   
   public void setDeclareId(String declareId) {
      this.declareId = declareId;
   }     

   public String getWinnerId(){
      return winnerId;
   }
   
   public void setWinnerId(String winnerId) {
      this.winnerId = winnerId;
   }   
   
   public String getChallengerId() {
      return challengerId;
   }

   public void setChallengerId(String challengerId) {
      this.challengerId = challengerId;
   }

   public ChessSide getLastMoveMadeBy() {
      return lastMoveMadeBy;
   }

   public void setLastMoveMadeBy(ChessSide lastMoveMadeBy) {
      this.lastMoveMadeBy = lastMoveMadeBy;
   }
   
   public boolean isUserNotified() {
      return userNotified;
   }
   
   public void setUserNotified(boolean userNotifed) {
      this.userNotified = userNotifed;
   }
}
