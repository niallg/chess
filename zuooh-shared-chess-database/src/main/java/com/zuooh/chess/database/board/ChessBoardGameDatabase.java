package com.zuooh.chess.database.board;

import java.util.List;

import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.database.bind.DatabaseBinder;

public class ChessBoardGameDatabase {
   
   private final DatabaseBinder binder;
   
   public ChessBoardGameDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   public synchronized List<ChessBoardGame> loadBoardGames() throws Exception {
      return binder.withTable(ChessBoardGame.class)
            .select()
            .orderBy("creationTime asc")
            .execute()
            .fetchAll();  

   }
   
   public synchronized List<ChessBoardGame> loadBoardGames(String userId) throws Exception {
      return binder.withTable(ChessBoardGame.class)
            .select()
            .where("userId == :userId and boardResult == :boardResult")
            .set("userId", userId)
            .set("boardResult", ChessBoardResult.NONE)
            .orderBy("creationTime asc")
            .execute()
            .fetchAll();  
   }
   
   public synchronized int countFinishedBoardGames(String userId) throws Exception {
      return binder.withTable(ChessBoardGame.class)
            .selectCount()
            .where("userId == :userId and boardResult != :boardResult")
            .set("userId", userId)
            .set("boardResult", ChessBoardResult.NONE)
            .execute();  
   }    
   
   public synchronized List<ChessBoardGame> loadFinishedBoardGames(String userId) throws Exception {
      return binder.withTable(ChessBoardGame.class)
            .select()
            .where("userId == :userId and boardResult != :boardResult")
            .set("userId", userId)
            .set("boardResult", ChessBoardResult.NONE)
            .orderBy("creationTime desc")
            .execute()
            .fetchAll();  
   }   
   
   public synchronized ChessBoardGame loadBoardGame(String gameId) throws Exception {
      return binder.withTable(ChessBoardGame.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .execute()
            .next();  
   }

   public synchronized void deleteBoardGame(ChessBoardGame game) throws Exception {
      binder.withTable(ChessBoardGame.class)
         .delete()           
         .execute(game);
   }
   
   public synchronized boolean containsBoardGame(String gameId) throws Exception {
      return !binder.withTable(ChessBoardGame.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .execute()
            .isEmpty();
   }
   
   public synchronized void saveBoardGame(ChessBoardGame game) throws Exception {
      validateBoardGame(game);
      binder.withTable(ChessBoardGame.class)
         .updateOrInsert()
         .execute(game);  
   }
   
   public synchronized void validateBoardGame(ChessBoardGame game) throws Exception {
      String gameId = game.getGameId();
      String userId = game.getUserId();      
      
      if(gameId == null || gameId.equals("")) {
         throw new IllegalArgumentException("Game key cannot be null or blank");
      }
      if(userId == null || userId.equals("")) {
         throw new IllegalArgumentException("User key for '" + gameId + "' cannot be null or blank");
      }
   }
}
