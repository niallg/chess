package com.zuooh.chess.database.board;

import java.io.Serializable;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardResult;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.board.ChessDrawPhase;
import com.zuooh.chess.database.result.ChessResultRecord;

public class ChessBoardGame implements Serializable {

   private static final long serialVersionUID = 1L;

   private ChessBoardResult boardResult;
   private ChessBoardStatus boardStatus;
   private ChessDrawPhase drawPhase;
   private ChessSide userSide;
   private String themeColor;
   private String opponentId;
   private String userId;
   private String gameId;  
   private boolean onlineGame;
   private long gameDuration;
   private long moveLatency;
   private long startTime;
   private long creationTime;

   public ChessBoardGame() {
      this.drawPhase = ChessDrawPhase.NOT_POSSIBLE;
      this.boardStatus = ChessBoardStatus.NORMAL;
      this.boardResult = ChessBoardResult.NONE;
      this.userSide = ChessSide.WHITE;
      this.gameDuration = 60 * 60 * 1000;
   }   
   
   public ChessResultRecord getResultRecord() {
      return new ChessResultRecord(boardResult, gameId, userId);
   }

   public long getMoveLatency() {
      return moveLatency;
   }

   public void setMoveLatency(long moveLatency) {
      this.moveLatency = moveLatency;
   }

   public long getStartTime() {
      return startTime;
   }

   public void setStartTime(long startTime) {
      this.startTime = startTime;
   }

   public long getCreationTime() {
      return creationTime;
   }

   public void setCreationTime(long creationTime) {
      this.creationTime = creationTime;
   }

   public ChessSide getUserSide() {
      return userSide;
   }

   public void setUserSide(ChessSide selectedSide) {
      this.userSide = selectedSide;
   }

   public long getGameDuration() {
      return gameDuration;
   }

   public void setGameDuration(long gameDuration) {
      this.gameDuration = gameDuration;
   }

   public String getOpponentId(){
      return opponentId;
   }
   
   public void setOpponentId(String opponentId) {
      this.opponentId = opponentId;
   }
   
   public String getUserId(){
      return userId;
   }
   
   public void setUserId(String userId) {
      this.userId = userId;
   } 

   public String getGameId() {
      return gameId;
   }

   public void setGameId(String gameId) {
      this.gameId = gameId;
   }

   public void setOnlineGame(boolean onlineGame) {
      this.onlineGame = onlineGame;
   }

   public boolean isOnlineGame() {
      return onlineGame;
   }
   
   public void setDrawPhase(ChessDrawPhase drawPhase) {
      this.drawPhase = drawPhase;
   }
   
   public ChessDrawPhase getDrawPhase() {
      return drawPhase;
   } 

   public String getThemeColor() {
      return themeColor;
   }

   public void setThemeColor(String themeColor) {
      this.themeColor = themeColor;
   }

   public ChessBoardResult getBoardResult() {
      return boardResult;
   }

   public void setBoardResult(ChessBoardResult boardResult) {
      this.boardResult = boardResult;      
   }

   public ChessBoardStatus getBoardStatus() {
      return boardStatus;
   }
   
   public void setBoardStatus(ChessBoardStatus boardStatus) {
      this.boardStatus = boardStatus;
   }
}
