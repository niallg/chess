package com.zuooh.chess.database.history;

import java.io.Serializable;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.engine.ChessMove;

public class ChessHistoryItem implements Serializable {

   private static final long serialVersionUID = 1L;

   public final ChessHistoryMove chessMove;
   public final ChessBoardStatus status;
   public final String gameId;
   public final long timeStamp;
   
   public ChessHistoryItem(ChessHistoryMove chessMove, ChessBoardStatus status, String gameId, long timeStamp) {
      this.chessMove = chessMove;
      this.timeStamp = timeStamp;
      this.status = status;
      this.gameId = gameId;
   }
   
   public ChessMove getMove() {
      return chessMove.getMove();
   }
   
   public ChessMove getCastleMove() {
      ChessPieceKey fromPiece = chessMove.fromPiece;
      ChessBoardPosition from = chessMove.fromPosition;
      ChessBoardPosition to = chessMove.toPosition;
      
      if(fromPiece.side == ChessSide.BLACK) {
         if(to == ChessBoardPosition.G8 && from == ChessBoardPosition.E8) {
            return new ChessMove(ChessBoardPosition.H8, ChessBoardPosition.F8, ChessSide.BLACK, chessMove.changeCount + 1);
         }
         if(to == ChessBoardPosition.C8 && from == ChessBoardPosition.E8) {
            return new ChessMove(ChessBoardPosition.A8, ChessBoardPosition.D8, ChessSide.BLACK, chessMove.changeCount + 1);
         }              
      } else {
         if(to == ChessBoardPosition.G1 && from == ChessBoardPosition.E1) {
            return new ChessMove(ChessBoardPosition.H1, ChessBoardPosition.F1, ChessSide.WHITE, chessMove.changeCount + 1);
         }
         if(to == ChessBoardPosition.C1 && from == ChessBoardPosition.E1) {
            return new ChessMove(ChessBoardPosition.A1, ChessBoardPosition.D1, ChessSide.WHITE, chessMove.changeCount + 1);
         }              
      }            
      return null;
   }
   
   @Override
   public String toString() {
      return String.format("%s: %s", status, chessMove);
   }
}
