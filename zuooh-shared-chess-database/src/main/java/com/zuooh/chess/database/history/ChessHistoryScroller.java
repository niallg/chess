package com.zuooh.chess.database.history;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.zuooh.chess.ChessSide;
import com.zuooh.chess.engine.ChessMove;

public class ChessHistoryScroller {

   private final ChessHistory history;
   
   public ChessHistoryScroller(ChessHistory history) {
      this.history = history;
   }
   
   public synchronized List<ChessMove> scrollBack(String gameId, ChessSide side) throws Exception {
      int historyCount = history.countOfHistoryItems(gameId);
      
      if(historyCount > 0) {
         ChessHistoryItem lastItem = history.deleteLastHistoryItem(gameId);
         ChessMove lastMove = lastItem.getMove();
         
         if(historyCount > 1) {
            if(lastItem.chessMove.side == side) {
               ChessHistoryItem beforeLastItem = history.deleteLastHistoryItem(gameId);
               ChessMove beforeLastMove = beforeLastItem.getMove();
               
               return Arrays.asList(lastMove, beforeLastMove);
            } 
         }
         return Arrays.asList(lastMove);
      }
      return Collections.emptyList();
   }
}
