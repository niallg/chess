package com.zuooh.chess.database.chat;

import java.util.List;

import com.zuooh.database.bind.DatabaseBinder;

public class ChessChatDatabase {
   
   private final DatabaseBinder binder;
   
   public ChessChatDatabase(DatabaseBinder binder) {
      this.binder = binder;
   }
   
   public synchronized List<ChessChat> listAllMessages(String gameId) throws Exception {
      return binder.withTable(ChessChat.class)
         .select()
         .where("gameId == :gameId")
         .set("gameId", gameId)
         .orderBy("timeStamp asc")
         .execute()
         .fetchAll();
   }
   
   public synchronized List<ChessChat> listNewMessages(String gameId) throws Exception {
      return binder.withTable(ChessChat.class)
            .select()
            .where("gameId == :gameId and status == :status")            
            .set("gameId", gameId)            
            .set("status", ChessChatStatus.UNREAD)
            .orderBy("timeStamp asc")            
            .execute()
            .fetchAll();
   }
   
   public synchronized List<ChessChat> listNewMessagesTo(String gameId, String toId) throws Exception {
      return binder.withTable(ChessChat.class)
            .select()
            .where("gameId == :gameId and status == :status and toId == :toId")
            .set("gameId", gameId)
            .set("status", ChessChatStatus.UNREAD)
            .set("toId", toId)
            .orderBy("timeStamp asc")            
            .execute()
            .fetchAll();
   }
   
   public synchronized List<ChessChat> listNewMessagesTo(String toId) throws Exception {
      return binder.withTable(ChessChat.class)
            .select()
            .where("status == :status and toId == :toId")
            .set("status", ChessChatStatus.UNREAD)
            .set("toId", toId)
            .orderBy("timeStamp asc")            
            .execute()
            .fetchAll();
   }
   
   public synchronized void markMessageAsRead(String chatId) throws Exception {
      ChessChat chat = binder.withTable(ChessChat.class)
            .select()
            .where("chatId == :chatId")
            .set("chatId", chatId)
            .execute()
            .fetchFirst();
      
      if(chat != null) {
         chat.setStatus(ChessChatStatus.READ);
         binder.withTable(ChessChat.class)
            .updateOrInsert()
            .execute(chat);
      }
   }
   
   public synchronized void deleteMessage(String chatId) throws Exception {
      ChessChat chat = binder.withTable(ChessChat.class)
            .select()
            .where("chatId == :chatId")
            .set("chatId", chatId)
            .execute()
            .fetchFirst();
      
      if(chat != null) {
         binder.withTable(ChessChat.class)
            .delete()
            .execute(chat);
      }
   }
   
   public synchronized void deleteAllMessages(String gameId) throws Exception {
      List<ChessChat> messages = binder.withTable(ChessChat.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .execute()
            .fetchAll();
      
      for(ChessChat message : messages) {
         binder.withTable(ChessChat.class)
            .delete()
            .execute(message);
      }
   }
   
   public synchronized void saveMessage(ChessChat chat) throws Exception {
      long currentTime = System.currentTimeMillis();
      
      chat.setTimeStamp(currentTime); // hack job, but needed!!
      binder.withTable(ChessChat.class)
         .updateOrInsert()
         .execute(chat);
   }
}
