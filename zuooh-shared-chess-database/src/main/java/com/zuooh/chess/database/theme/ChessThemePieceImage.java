package com.zuooh.chess.database.theme;

import java.io.Serializable;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceType;
import com.zuooh.chess.ChessSide;
import com.zuooh.style.image.Image;
import com.zuooh.style.image.ImageManager;

public class ChessThemePieceImage implements Serializable {

   private static final long serialVersionUID = 1L;

   private ChessPieceType pieceType;
   private ChessSide pieceSide;
   private String imageDefault;
   private String imageOnWhite;
   private String imageOnBlack;
   private String imageOnDarkGrey;
   private String imageShadow;
   private String imageUnderAttack;
   private String imageSelected;
   private String imageTaken;
   private String theme;
   private String name;

   public ChessThemePieceImage(ChessPieceType pieceType, ChessSide pieceSide, String imageDefault, String name, String theme) {
      this.imageDefault = imageDefault;
      this.imageOnWhite = imageDefault;
      this.imageOnBlack = imageDefault;
      this.imageUnderAttack = imageDefault;
      this.imageOnDarkGrey = imageDefault;
      this.imageShadow = imageDefault;
      this.imageTaken = imageDefault;
      this.imageSelected = imageDefault;      
      this.pieceSide = pieceSide;
      this.pieceType = pieceType;
      this.theme = theme;
      this.name = name;
   }
   
   public ChessPiece getChessPiece(ChessBoardPosition boardPosition) {
      return new ChessPiece(boardPosition, pieceType, pieceSide);
   }

   public ChessThemePiece getPiece(ImageManager imageManager) {
      Image onWhiteImage = imageManager.getImage(imageOnWhite);
      Image onBlackImage = imageManager.getImage(imageOnBlack);
      Image onDarkGreyImage = imageManager.getImage(imageOnDarkGrey);
      Image onShadowImage = imageManager.getImage(imageShadow);
      Image underAttackImage = imageManager.getImage(imageUnderAttack);
      Image selectedImage = imageManager.getImage(imageSelected);
      Image takenImage = imageManager.getImage(imageTaken);
      
      return new ChessThemePiece(pieceType, pieceSide, onWhiteImage, onBlackImage, onDarkGreyImage, onShadowImage, underAttackImage, selectedImage, takenImage);
   }
   
   public String getImageDefault() {
      return imageDefault;
   }

   public void setImageDefault(String imageDefault) {
      this.imageDefault = imageDefault;
   }

   public String getImageOnWhite() {
      return imageOnWhite;
   }

   public void setImageOnWhite(String imageOnWhite) {
      this.imageOnWhite = imageOnWhite;
   }

   public String getImageOnBlack() {
      return imageOnBlack;
   }

   public void setImageOnBlack(String imageOnBlack) {
      this.imageOnBlack = imageOnBlack;
   }

   public String getImageOnDarkGrey() {
      return imageOnDarkGrey;
   }

   public void setImageOnDarkGrey(String imageOnDarkGrey) {
      this.imageOnDarkGrey = imageOnDarkGrey;
   }

   public String getImageShadow() {
      return imageShadow;
   }

   public void setImageShadow(String imageShadow) {
      this.imageShadow = imageShadow;
   }

   public String getImageUnderAttack() {
      return imageUnderAttack;
   }

   public void setImageUnderAttack(String imageUnderAttack) {
      this.imageUnderAttack = imageUnderAttack;
   }

   public String getImageSelected() {
      return imageSelected;
   }

   public void setImageSelected(String imageSelected) {
      this.imageSelected = imageSelected;
   }

   public String getImageTaken() {
      return imageTaken;
   }

   public void setImageTaken(String imageTaken) {
      this.imageTaken = imageTaken;
   }

   public ChessSide getSide() {
      return pieceSide;
   }

   public ChessPieceType getPieceType() {
      return pieceType;
   }
   
   public String getTheme() {
      return theme;
   }

   public String getName() {
      return name;
   }
   
   @Override
   public String toString() {
      return String.format("%s: %s", name, theme);
   }
}
