package com.zuooh.chess.database.chat;

public enum ChessChatStatus {
   READ,
   UNREAD,
   DELETED;
   
   public boolean isRead() {
      return this == READ || this == DELETED;
   }
}
