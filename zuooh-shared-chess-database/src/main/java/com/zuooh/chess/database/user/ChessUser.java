package com.zuooh.chess.database.user;

import java.io.Serializable;

public class ChessUser implements Serializable {
   
   private static final long serialVersionUID = 1L;

   private final ChessUserData data;
   private final ChessUserType type;
   private final String mail;
   private final String name;
   private final String key;
   private final String password;
   
   public ChessUser(ChessUserData data, ChessUserType type, String key, String name, String mail, String password) {
      this.key = key.toLowerCase();
      this.password = password;      
      this.mail = mail;
      this.data = data;
      this.type = type;
      this.name = name;      
   }
   
   public ChessUserType getType() {
      return type;
   }
   
   public ChessUserData getData() {
      return data;
   }

   public String getKey() {
      return key;
   }

   public String getName() {
      return name;
   }
   
   public String getMail() {
      return mail;
   }

   public String getPassword() {
      return password;
   }
   
   @Override
   public String toString() {
      return String.format("userId=%s userName=%s", key, name);
   }
}
