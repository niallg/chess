package com.zuooh.chess.database.history;

import java.util.List;
import java.util.Set;

import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.engine.ChessMove;

public interface ChessHistory {
   int countOfHistoryItems(String gameId) throws Exception;
   boolean containsHistory(String gameId) throws Exception;
   ChessBoard loadHistoryAsBoard(String gameId) throws Exception;
   ChessPieceSet loadHistoryAsPieceSet(String gameId) throws Exception; 
   Set<ChessPieceKey> loadHistoryPiecesTaken(String gameId) throws Exception; 
   List<ChessHistoryItem> loadAllHistoryItems(String gameId) throws Exception;
   ChessHistoryItem loadLastHistoryItem(String gameId) throws Exception;
   ChessHistoryItem loadLastHistoryItem(String gameId, ChessSide direction) throws Exception;   
   ChessHistoryItem deleteLastHistoryItem(String gameId) throws Exception;
   List<ChessHistoryItem> deleteLastHistoryItems(String gameId, ChessSide playDirection) throws Exception;  
   void saveHistoryItem(ChessHistoryItem item) throws Exception;   
   void saveHistoryItem(String gameId, ChessMove chessMove) throws Exception;
   void deleteAllHistoryItems(String gameId) throws Exception;
}
