package com.zuooh.chess.database.chat;

import java.io.Serializable;

public class ChessChat implements Serializable {
   
   private static final long serialVersionUID = 1L;
   
   private ChessChatStatus status;
   private String gameId;
   private String chatId;
   private String message;
   private String fromId;
   private String toId;
   private long timeStamp;
   
   public ChessChat(ChessChatStatus status, String chatId, String gameId, String fromId, String toId, String message, long timeStamp) {
      this.timeStamp = timeStamp;
      this.status = status;
      this.gameId = gameId;
      this.message = message;
      this.fromId = fromId;
      this.chatId = chatId;
      this.toId = toId;
   }   

   public long getTimeStamp() {
      return timeStamp;
   }   
   
   public void setTimeStamp(long timeStamp) {
      this.timeStamp = timeStamp;
   }

   public ChessChatStatus getStatus() {
      return status;
   }
   
   public void setStatus(ChessChatStatus status) {
      this.status = status;
   }
   
   public String getChatId() {
      return chatId;
   }   

   public String getMessage() {
      return message;
   }

   public String getFrom() {
      return fromId;
   }

   public String getTo() {
      return toId;
   }

   public String getGameId() {
      return gameId;
   }

   @Override
   public String toString() {
      return String.format("[%s]%s->%s@%s", gameId, fromId, toId, timeStamp);
   }
}
