package com.zuooh.chess.database.history;

import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPiece;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.analyze.ChessCheckAnalyzer;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.ChessBoardCell;
import com.zuooh.chess.board.ChessBoardMove;
import com.zuooh.chess.board.ChessBoardStatus;
import com.zuooh.chess.board.ChessPieceSet;
import com.zuooh.chess.board.ChessPieceSetBoard;
import com.zuooh.chess.engine.ChessMove;
import com.zuooh.common.collections.Cache;
import com.zuooh.common.collections.LeastRecentlyUsedCache;
import com.zuooh.database.bind.DatabaseBinder;

public class ChessHistoryDatabase implements ChessHistory {
   
   private final Cache<String, List<ChessHistoryItem>> fullHistoryCache;
   private final Cache<String, ChessHistoryItem> lastHistoryCache;
   private final Cache<String, Integer> countCache;
   private final DatabaseBinder binder;
   
   public ChessHistoryDatabase(DatabaseBinder binder) {
      this.fullHistoryCache = new LeastRecentlyUsedCache<String, List<ChessHistoryItem>>(10);
      this.lastHistoryCache = new LeastRecentlyUsedCache<String, ChessHistoryItem>(10);
      this.countCache = new LeastRecentlyUsedCache<String, Integer>(10);
      this.binder = binder;
   }      

   @Override
   public synchronized int countOfHistoryItems(String gameId) throws Exception {
      Integer historyCount = countCache.fetch(gameId);
      
      if(historyCount == null) {
         historyCount = binder.withTable(ChessHistoryItem.class)
            .selectCount()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .execute();
         
         countCache.cache(gameId,  historyCount);
      }
      return historyCount;
   }
   
   @Override
   public synchronized boolean containsHistory(String gameId) throws Exception {
      return countOfHistoryItems(gameId) > 0;
   }

   @Override
   public synchronized List<ChessHistoryItem> loadAllHistoryItems(String gameId) throws Exception {
      List<ChessHistoryItem> historyItems = fullHistoryCache.fetch(gameId);
      
      if(historyItems == null) {
         historyItems = binder.withTable(ChessHistoryItem.class)
            .select()
            .where("gameId == :gameId")
            .set("gameId", gameId)
            .orderBy("chessMoveChangeCount asc")
            .execute()
            .fetchAll();
         
         fullHistoryCache.cache(gameId,  historyItems);
      }
      return Collections.unmodifiableList(historyItems);
   }

   @Override
   public synchronized ChessHistoryItem loadLastHistoryItem(String gameId) throws Exception {
      ChessHistoryItem historyItem = lastHistoryCache.fetch(gameId);
      
      if(historyItem == null) {
         int count = countOfHistoryItems(gameId);
         
         if(count > 0) {
            historyItem = binder.withTable(ChessHistoryItem.class)
                  .select()
                  .where("gameId == :gameId")
                  .set("gameId", gameId)
                  .orderBy("chessMoveChangeCount desc")
                  .execute()
                  .next();
            
            lastHistoryCache.cache(gameId, historyItem);
         }
      }
      return historyItem;
   }
   
   @Override
   public synchronized ChessHistoryItem loadLastHistoryItem(String gameId, ChessSide side) throws Exception {
      int count = countOfHistoryItems(gameId);
      
      if(count > 1) {
         return binder.withTable(ChessHistoryItem.class)
               .select()
               .where("gameId == :gameId and chessMoveSide == :chessMoveSide")
               .set("gameId", gameId)
               .set("chessMoveSide", side)            
               .orderBy("chessMoveChangeCount desc")
               .execute()
               .next(); 
      } else {
         ChessHistoryItem lastItem = loadLastHistoryItem(gameId);  
         
         if(lastItem != null) {
            if(lastItem.chessMove.side == side) {
               return lastItem;
            }
         }
      }
      return null;
   }
   
   @Override
   public synchronized ChessHistoryItem deleteLastHistoryItem(String gameId) throws Exception {
      ChessHistoryItem lastItem = loadLastHistoryItem(gameId);    
      
      countCache.take(gameId);
      fullHistoryCache.take(gameId);
      lastHistoryCache.take(gameId);
      binder.withTable(ChessHistoryItem.class)
         .delete()
         .execute(lastItem);
      
      return lastItem;
   }
   
   
   @Override
   public synchronized List<ChessHistoryItem> deleteLastHistoryItems(String gameId, ChessSide side) throws Exception {
      int historyCount = countOfHistoryItems(gameId);
      
      if(historyCount > 0) {
         ChessHistoryItem lastItem = deleteLastHistoryItem(gameId);
         
         if(historyCount > 1) {
            if(lastItem.chessMove.side != side) {
               ChessHistoryItem beforeLastItem = deleteLastHistoryItem(gameId);
               
               return Arrays.asList(lastItem, beforeLastItem);
            } 
         }
         return Arrays.asList(lastItem);
      }
      return Collections.emptyList();
   }   

   @Override
   public synchronized void deleteAllHistoryItems(String gameId) throws Exception {
      List<ChessHistoryItem> items = loadAllHistoryItems(gameId);    
      
      for(ChessHistoryItem item : items) {
         binder.withTable(ChessHistoryItem.class)
            .delete()
            .execute(item);
      }
      countCache.take(gameId);
      fullHistoryCache.take(gameId);
      lastHistoryCache.take(gameId);
   }

   @Override
   public synchronized Set<ChessPieceKey> loadHistoryPiecesTaken(String gameId) throws Exception { 
      Set<ChessPieceKey> piecesTaken = new LinkedHashSet<ChessPieceKey>();
      
      if(containsHistory(gameId)) {
         List<ChessHistoryItem> items = loadAllHistoryItems(gameId);
         
         for(ChessHistoryItem historyItem : items) {
            if(historyItem.chessMove.takePiece != null) {
               piecesTaken.add(historyItem.chessMove.takePiece);
            }                     
         }         
      }
      return piecesTaken;
   }
   
   @Override
   public synchronized ChessBoard loadHistoryAsBoard(String gameId) throws Exception { 
      ChessPieceSet defaultSet = new ChessHistoryPieceSet();
      ChessPieceSetBoard chessBoard = new ChessPieceSetBoard(defaultSet);
      
      if(containsHistory(gameId)) {
         List<ChessHistoryItem> items = loadAllHistoryItems(gameId);
         
         for(ChessHistoryItem historyItem : items) {
            ChessMove chessMove = historyItem.getMove(); 
            ChessBoardMove boardMove = chessBoard.getBoardMove(chessMove);
            
            boardMove.makeMove();                       
         }         
      }
      return chessBoard;
   }

   @Override
   public synchronized ChessPieceSet loadHistoryAsPieceSet(String gameId) throws Exception {
      List<ChessHistoryItem> items = loadAllHistoryItems(gameId);      
      return new ChessHistoryPieceSet(items);
   }
   
   @Override
   public synchronized void saveHistoryItem(ChessHistoryItem item) throws Exception {
      int historyCount = countOfHistoryItems(item.gameId);
      
      if(historyCount > 0) {
         ChessHistoryItem lastItem = loadLastHistoryItem(item.gameId);
         
         if(lastItem.chessMove.side != item.chessMove.side) {
            if(lastItem.status.isGameOver()) {
               throw new IllegalStateException("Illegal histor item for " + item.gameId + ", game is over as " + lastItem.status);
            }
            countCache.take(item.gameId);
            fullHistoryCache.take(item.gameId);
            lastHistoryCache.take(item.gameId);
            binder.withTable(ChessHistoryItem.class)
               .insert()
               .execute(item);         
         }
      } else {
         if(item.chessMove.side != ChessSide.WHITE) { // first move   
            throw new IllegalStateException("Illegal history item for new game " + item.gameId + ", white must make first move");
         }
         countCache.take(item.gameId);
         fullHistoryCache.take(item.gameId);
         lastHistoryCache.take(item.gameId);
         binder.withTable(ChessHistoryItem.class)
            .insert()
            .execute(item);         
      }
   }
   
   @Override
   public synchronized void saveHistoryItem(String gameId, ChessMove chessMove) throws Exception {
      int historyCount = countOfHistoryItems(gameId);
      
      if(historyCount > 0) {
         ChessHistoryItem lastItem = loadLastHistoryItem(gameId);
         
         if(lastItem != null && lastItem.chessMove.side != chessMove.side) { 
            if(lastItem.status.isGameOver()) {
               throw new IllegalStateException("Illegal history item for " + gameId + ", game is over as " + lastItem.status);
            }            
            ChessBoard chessBoard = loadHistoryAsBoard(gameId);
            ChessHistoryItem nextItem = createHistoryItem(chessBoard, chessMove, gameId);
            
            countCache.take(gameId);
            fullHistoryCache.take(gameId);
            lastHistoryCache.take(gameId);
            binder.withTable(ChessHistoryItem.class)
               .insert()
               .execute(nextItem);                  
         }
      } else {
         if(chessMove.side != ChessSide.WHITE) { // first move
            throw new IllegalStateException("Illegal history item for new game " + gameId + ", white must make first move");
         }
         ChessBoard chessBoard = loadHistoryAsBoard(gameId);
         ChessHistoryItem nextItem = createHistoryItem(chessBoard, chessMove, gameId);
         
         countCache.take(gameId);
         fullHistoryCache.take(gameId);
         lastHistoryCache.take(gameId);
         binder.withTable(ChessHistoryItem.class)
            .insert()
            .execute(nextItem);             
      }
   }
   
   private synchronized ChessHistoryItem createHistoryItem(ChessBoard chessBoard, ChessMove chessMove, String gameId) {
      if(chessMove == null) {
         throw new IllegalStateException("Chess move is null for " + gameId);
      }
      ChessBoardCell destinationCell = chessBoard.getBoardCell(chessMove.to);
      ChessBoardCell currentCell = chessBoard.getBoardCell(chessMove.from);
      
      if(currentCell.isCellEmpty()) {
         throw new IllegalStateException("Game move " + chessMove + " for game " + gameId + " is invalid");
      }
      ChessPiece chessPiece = currentCell.getPiece();
      
      if(chessPiece == null) {
         throw new IllegalStateException("Invalid move " + chessMove + " for " + gameId + " cell " + chessMove.from + " has no piece");
      }
      ChessBoardPosition from = currentCell.getPosition();
      ChessBoardPosition to = destinationCell.getPosition();
      ChessHistoryMove move = createHistoryMove(chessBoard, from, to);
      ChessBoardMove boardMove = chessBoard.getBoardMove(chessMove);
      ChessSide oppositeSide = chessMove.side.oppositeSide();
      long timeStamp = System.currentTimeMillis();
      
      try {
         boardMove.makeMove();
         return createHistoryItem(gameId, move, chessBoard, oppositeSide, timeStamp);
      } finally {
         boardMove.revertMove();
      }
   }

   private ChessHistoryItem createHistoryItem(String gameId, ChessHistoryMove move, ChessBoard board, ChessSide oppositeSide, long timeStamp) {
      if(ChessCheckAnalyzer.checkMate(board)) { // did the move end the game
         return new ChessHistoryItem(move, ChessBoardStatus.CHECK_MATE, gameId, timeStamp);
      }
      if(ChessCheckAnalyzer.check(board)) {
         return new ChessHistoryItem(move, ChessBoardStatus.CHECK, gameId, timeStamp);
      }
      if(ChessCheckAnalyzer.staleMate(board, oppositeSide)) { // did the move put the opponent in stale mate?
         return new ChessHistoryItem(move, ChessBoardStatus.STALE_MATE, gameId, timeStamp);
      }      
      return new ChessHistoryItem(move, ChessBoardStatus.NORMAL, gameId, timeStamp);
   }   
   
   
   private synchronized ChessHistoryMove createHistoryMove(ChessBoard chessBoard, ChessBoardPosition from, ChessBoardPosition to) {
      ChessPiece fromPiece = chessBoard.getPiece(from);
      ChessPiece toPiece = chessBoard.getPiece(to);
      int changeCount = chessBoard.getChangeCount();
      
      if(toPiece != null) {
         return new ChessHistoryMove(from, to, to, fromPiece.key, toPiece.key, fromPiece.side, changeCount);
      } else {
         if(fromPiece.moveWasEnPassent(chessBoard, to)) {
            ChessPiece takePiece = null;
            ChessBoardCell takeCell = null;
            ChessBoardPosition takePosition = null;
            
            if(fromPiece.side == ChessSide.WHITE) {
               takePosition = ChessBoardPosition.at(to.x, to.y + 1);
            } else {
               takePosition = ChessBoardPosition.at(to.x, to.y - 1);
            }
            takeCell = chessBoard.getBoardCell(takePosition);
            takePiece = takeCell.getPiece();
            
            return new ChessHistoryMove(from, to, takePosition, fromPiece.key, takePiece.key, fromPiece.side, changeCount);            
         }
      }
      return new ChessHistoryMove(from, to, null, fromPiece.key, null, fromPiece.side, changeCount); 
   }
}

