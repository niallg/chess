package com.zuooh.chess.database.user;

import com.zuooh.chess.database.user.ChessUserStatus;

import junit.framework.TestCase;

public class ChessUserStatusTest extends TestCase {
   
   public void testStatus() {
      assertEquals(ChessUserStatus.calculateSkill(10, 0), 0);
      assertEquals(ChessUserStatus.calculateStatus(10, 0), ChessUserStatus.EXPERIENCED_LOSER);      
   }

}
