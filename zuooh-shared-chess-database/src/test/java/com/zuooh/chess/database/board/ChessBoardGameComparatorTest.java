package com.zuooh.chess.database.board;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import junit.framework.TestCase;

public class ChessBoardGameComparatorTest extends TestCase{
   
   public void testSortOldestLast() throws Exception {
      List<ChessBoardGame> list = new ArrayList<ChessBoardGame>();
      ChessBoardGameComparator comparator = new ChessBoardGameComparator();
      ChessBoardGame game1 = new ChessBoardGame();
      ChessBoardGame game2 = new ChessBoardGame();
      ChessBoardGame game3 = new ChessBoardGame();
      ChessBoardGame game4 = new ChessBoardGame();      
      
      game1.setCreationTime(6);
      game2.setCreationTime(2);
      game3.setCreationTime(3);
      game4.setCreationTime(1);
      
      list.add(game1);
      list.add(game2);      
      list.add(game3);      
      list.add(game4);
      
      Collections.sort(list, comparator);
      
      assertEquals(list.get(0), game4);
      assertEquals(list.get(1), game2);
      assertEquals(list.get(2), game3);
      assertEquals(list.get(3), game1);
   }

}
