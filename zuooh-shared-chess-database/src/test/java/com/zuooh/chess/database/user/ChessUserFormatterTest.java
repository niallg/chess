package com.zuooh.chess.database.user;

import junit.framework.TestCase;

public class ChessUserFormatterTest extends TestCase {
   
   public void testFormatter() throws Exception {
      assertEquals(ChessUserFormatter.formatUser(new ChessUser(null, ChessUserType.NORMAL, "", "tom",  null,  null)), "Tom");
      assertEquals(ChessUserFormatter.formatUser(new ChessUser(null, ChessUserType.NORMAL, "", " bob sachamano",  null,  null)), "Bob Sachamano");
      assertEquals(ChessUserFormatter.formatUser(new ChessUser(null, ChessUserType.NORMAL, "", " Aliza_chesspro",  null,  null)), "Aliza Chesspro");      
      assertEquals(ChessUserFormatter.formatUser(new ChessUser(null, ChessUserType.NORMAL, "", " 112",  null,  null)), "112"); 
      assertEquals(ChessUserFormatter.formatUser(new ChessUser(null, ChessUserType.NORMAL, "", "A a1 1",  null,  null)), "A A1 1"); 
      assertEquals(ChessUserFormatter.formatUser(new ChessUser(null, ChessUserType.NORMAL, "", " Jerry mc   graw ",  null,  null)), "Jerry Mc Graw");
      assertEquals(ChessUserFormatter.formatUser(new ChessUser(null, ChessUserType.NORMAL, "", "horizon+",  null,  null)), "Horizon+");       
   }

}
