@echo off
echo Please wait...

cd ..

echo %CLASSPATH%

set CLASSPATH=obfuscate/obfuscated/.
set CLASSPATH=%CLASSPATH%;target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Work/development/bitbucket/mobile/zuooh-standard-application/target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Work/development/bitbucket/mobile/zuooh-shared-code-generator/target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Work/development/bitbucket/mobile/zuooh-shared-application-builder/target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Work/development/bitbucket/database/zuooh-standard-database/target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Work/development/bitbucket/database/zuooh-shared-database-builder/target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Work/development/bitbucket/database/zuooh-shared-database-generator/target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Work/development/bitbucket/container/zuooh-shared-container/target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Work/development/bitbucket/container/zuooh-shared-container-builder/target/classes/.
set CLASSPATH=%CLASSPATH%;c:/Users/Niall/.m2/repository/org/simpleframework/simple-xml/2.7.1/simple-xml-2.7.1.jar
set CLASSPATH=%CLASSPATH%;lib/*
set CLASSPATH=%CLASSPATH%;~1%
set CLASSPATH=%CLASSPATH%;.

echo %CLASSPATH%

rem if you wish to use a specific java version then prefix 'java' with the full path below

set JAVA_OPTS="-Xrunjdwp:transport=dt_socket,server=y,address=8745,suspend=n -Xms512m -Xmx512m -XX:MaxPermSize=128m -XX:+CMSClassUnloadingEnabled -XX:+UseConcMarkSweepGC -XX:+CMSPermGenSweepingEnabled"

rem java "%JAVA_OPTS%" -classpath %CLASSPATH% com.yieldbroker.marketdata.client.ClientApplication etc\spring.xml etc\common.properties 
java "%JAVA_OPTS%" -classpath %CLASSPATH% com.zuooh.chess.ChessActivityLauncher tom.properties
