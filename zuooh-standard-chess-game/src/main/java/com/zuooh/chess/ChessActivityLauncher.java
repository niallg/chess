package com.zuooh.chess;

import com.zuooh.application.standard.StandardLauncher;
import com.zuooh.style.Dimension;

public class ChessActivityLauncher extends StandardLauncher {
   
   private static final String WORKSPACE_PATH = "c:\\Work\\development\\bitbucket\\";

   public ChessActivityLauncher(String rootPath, String configFile, String[] propertiesFiles) throws Exception {
      super(rootPath, configFile, propertiesFiles);
   }

   @Override
   public String getPackageName() {
      return "com.zuooh.chess";
   }

   @Override
   public String getContainerClassName() {
      return "com.zuooh.chess.container.ChessContainer";
   }

   @Override
   public Dimension getDeviceSize() {
      String width = getProperty("width");
      String height = getProperty("height");
      
      if(width != null && height != null) {         
         return new Dimension(Integer.parseInt(width), Integer.parseInt(height));
      }
      //return new Dimension(240, 400);
      //return new Dimension(600, 1024);
      //return new Dimension(768, 976);
      return new Dimension(480, 800);
   }    

   @Override
   public String getKeyPadImage() {
      return WORKSPACE_PATH + "mobile\\zuooh-standard-application\\keypad.png";
   }   
   
   @Override
   public String getDatabaseLibrary() {
      return WORKSPACE_PATH + "chess\\zuooh-standard-chess-game\\lib";
   }     
   
   @Override
   public String getDatabasePath() {
      return WORKSPACE_PATH + "chess\\zuooh-standard-chess-game\\resources\\temp\\database";
   }
   
   @Override
   public String getIconImage() {
      return WORKSPACE_PATH + "chess\\zuooh-standard-chess-game\\resources\\image\\art\\chess_icon_globe.png";
   }
   
   @Override
   public String[] getImageCachePaths() {
      return new String[]{
           WORKSPACE_PATH + "chess\\zuooh-standard-chess-game\\assets",
           WORKSPACE_PATH + "chess\\zuooh-android-chess-game\\assets"
      };
   }
   
   @Override
   public String[] getIconImagePaths() {
      return new String[]{
           WORKSPACE_PATH + "chess\\zuooh-standard-chess-game\\res",
           WORKSPACE_PATH + "chess\\zuooh-android-chess-game\\res"
      };
   }

   @Override
   public String[] getCodeGenertionPaths() {
      return new String[]{
           WORKSPACE_PATH + "chess\\zuooh-android-chess\\src",
           WORKSPACE_PATH + "chess\\zuooh-standard-chess\\src\\main\\java"
      };
   }

   public static void main(String[] list) throws Exception {
      ChessActivityLauncher launcher = new ChessActivityLauncher(
           WORKSPACE_PATH + "chess\\zuooh-standard-chess-game\\resources", 
            "spring.xml", 
            new String[]{"common.properties", list[0]});
      
      launcher.launchApplication();      
   }
}

