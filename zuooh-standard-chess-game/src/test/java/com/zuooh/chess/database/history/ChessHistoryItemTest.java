package com.zuooh.chess.database.history;

import junit.framework.TestCase;

import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessPieceKey;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoardStatus;

public class ChessHistoryItemTest extends TestCase {
   
   public void testHistoryItem() throws Exception {
      ChessHistoryMove move = new ChessHistoryMove(ChessBoardPosition.E8, ChessBoardPosition.G8, ChessBoardPosition.G8, ChessPieceKey.BLACK_KING, ChessPieceKey.BLACK_ROOK_H, ChessSide.BLACK, 1);
      ChessHistoryItem item = new ChessHistoryItem(move, ChessBoardStatus.NORMAL, "game.1", 0);
      
      assertNotNull(item.getCastleMove());
      assertEquals(item.getCastleMove().from, ChessBoardPosition.H8);
      assertEquals(item.getCastleMove().to, ChessBoardPosition.F8);
   }

}
