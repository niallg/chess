package com.zuooh.chess.database;

import com.zuooh.database.Database;
import com.zuooh.database.bind.DatabaseBinder;
import com.zuooh.database.bind.TableBinder;

public class SingleTableDatabaseBinder implements DatabaseBinder {
   
   private final Database database;
   private final TableBinder table;   
   private final Class type;
   
   public SingleTableDatabaseBinder(Database database, TableBinder table, Class type) {
      this.database = database;
      this.table = table;
      this.type = type;
   }   

   @Override
   public <T> TableBinder<T> withTable(String name) {
      return null;
   }

   @Override
   public <T> TableBinder<T> withTable(Class<T> query) {
      if(type == query) {
         return table;
      }
      return null;
   }

   @Override
   public Database withDatabase() {     
      return database;
   }
   
}
