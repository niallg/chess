package com.zuooh.chess.database.user;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

import junit.framework.TestCase;

import org.simpleframework.xml.core.Persister;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.chess.database.SingleTableDatabaseBinder;
import com.zuooh.database.Database;
import com.zuooh.database.DatabaseConnection;
import com.zuooh.database.bind.DatabaseBinder;
import com.zuooh.database.bind.TableBinder;
import com.zuooh.database.bind.load.TableDefinition;
import com.zuooh.database.bind.table.TableBuilder;
import com.zuooh.database.bind.table.attribute.AttributeTableBuilder;
import com.zuooh.database.bind.table.statement.CreateStatement;
import com.zuooh.database.bind.table.statement.DropStatement;
import com.zuooh.database.bind.table.statement.InsertStatement;
import com.zuooh.database.build.dom.element.TableBinderElement;
import com.zuooh.database.build.dom.schema.TableBinderBuilder;
import com.zuooh.database.standard.StandardDatabase;

public class ChessUserDatabaseTest extends TestCase {
   
   private static final String SOURCE =
   "<table name='playerProfile' type='com.zuooh.chess.database.user.ChessUser'>\r\n"+
   "  <schema>\r\n"+
   "    <key>\r\n"+
   "      <column name='key' />\r\n"+
   "    </key>\r\n"+
   "    <column name='name' constraint='required'/>\r\n"+
   "    <column name='mail' constraint='required'/>\r\n"+ 
   "  </schema>\r\n"+
   "  <insert>\r\n"+
   "    <row>\r\n"+
   "      <value column='password'>required</value>\r\n"+     
   "      <value column='dataStatus'>BEGINNER</value>\r\n"+   
   "      <value column='dataSkill'>2</value>\r\n"+
   "      <value column='dataRank'>1500</value>\r\n"+
   "      <value column='dataBlackGames'>1</value>\r\n"+
   "      <value column='dataWhiteGames'>1</value>\r\n"+
   "      <value column='dataLastSeenOnline'>0</value>\r\n"+   
   "      <value column='type'>NORMAL</value>\r\n"+     
   "      <value column='key'>jdoeid</value>\r\n"+   
   "      <value column='name'>John</value>\r\n"+
   "      <value column='mail'>jdoe@gmail.com</value>\r\n"+  
   "      <value column='password'>password12</value>\r\n"+
   "    </row>\r\n"+
   "  </insert>\r\n"+
   "</table>\r\n";
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll");  
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }   
   
   public void testProfileDatabase() throws Exception {
      Persister persister = new Persister();
      TableBinderBuilder builder = persister.read(TableBinderBuilder.class, SOURCE);
      Database database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      TableBuilder binder = new AttributeTableBuilder(database);
      List<String> script = new LinkedList<String>();
      
      TableBinderElement tableElement = builder.createElement();
      TableDefinition tableDefinition = tableElement.createTable(binder);
      TableBinder table = tableDefinition.createBinder(binder, script);
      DropStatement drop = table.dropIfExists();
      String dropStatement = drop.compile();
      
      System.err.println(dropStatement); 
      
      CreateStatement create = table.create();
      String createStatement = create.compile();
      
      System.err.println(createStatement);
      
      drop.execute();
      create.execute();
      
      System.err.println(SOURCE);
      System.err.println();
      
      for(String line : script) {
         DatabaseConnection conn = database.getConnection();
         try {
            conn.executeStatement(line);
         }finally {
            conn.closeConnection();
         }
      }
      
      DatabaseBinder databaseBinder = new SingleTableDatabaseBinder(database, table, ChessUser.class);
      ChessUserDatabase userDatabase = new ChessUserDatabase(databaseBinder);      
      
      assertTrue(userDatabase.containsUser("jdoeid"));
      assertFalse(userDatabase.containsUser("tomid"));
      assertEquals(userDatabase.loadUser("jdoeid").getName(), "John");
      
      ChessUser profile = userDatabase.loadUser("jdoeid");
      
      assertEquals(profile.getName(), "John");
      assertEquals(profile.getPassword(), "password12");
      assertEquals(profile.getKey(), "jdoeid");
      assertEquals(profile.getMail(), "jdoe@gmail.com");      
      assertEquals(profile.getType(), ChessUserType.NORMAL);
      assertEquals(profile.getData().getRank(), 1500);
      assertEquals(profile.getData().getSkill(), 2);
   }
   
   public void testPerformance() throws Exception {
      Persister persister = new Persister();
      TableBinderBuilder builder = persister.read(TableBinderBuilder.class, SOURCE);
      Database database = new StandardDatabase("c:\\work\\temp\\database", "C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      TableBuilder binder = new AttributeTableBuilder(database);
      List<String> script = new LinkedList<String>();
      
      TableBinderElement tableElement = builder.createElement();
      TableDefinition tableDefinition = tableElement.createTable(binder);
      TableBinder table = tableDefinition.createBinder(binder, script);
      DropStatement drop = table.dropIfExists();
      String dropStatement = drop.compile();
      
      System.err.println(dropStatement); 
      
      CreateStatement create = table.create();
      String createStatement = create.compile();
      
      System.err.println(createStatement);
      
      drop.execute();
      create.execute();
      
      System.err.println(SOURCE);
      System.err.println();
      
      for(String line : script) {
         DatabaseConnection conn = database.getConnection();
         try {
            conn.executeStatement(line);
         }finally {
            conn.closeConnection();
         }
      }
      long start = System.currentTimeMillis();
      for(int i = 0; i < 1000; i++) {
         ChessUserData data = new ChessUserData();
         ChessUser profile = new ChessUser(data, ChessUserType.NORMAL, ""+i, "Tom", "mail"+i, "password" +i);
         InsertStatement insertStatement = table.insertOrIgnore();
         String insertSQL = insertStatement.execute(profile);
         System.err.println(insertSQL);
      }
      long end = System.currentTimeMillis();
      long duration = end - start;
      
      System.err.println("Time taken was " + duration);
   }
  
}
