package com.zuooh.chess.database.history;

import java.io.File;
import java.io.FileNotFoundException;

import junit.framework.TestCase;

import com.almworks.sqlite4java.SQLite;
import com.zuooh.chess.ChessBoardPosition;
import com.zuooh.chess.ChessSide;
import com.zuooh.chess.board.ChessBoard;
import com.zuooh.chess.board.text.ChessTextPieceSet;
import com.zuooh.chess.engine.ChessMove;

public class ChessHistoryUpdaterTest extends TestCase {
   
   public void setUp() throws Exception {
      File file = new File("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib\\sqlite4java-win32-x86.dll"); 
      
      if(!file.exists()) {
         throw new FileNotFoundException("Unable to find library " + file);
      }
      SQLite.setLibraryPath("C:\\Work\\development\\bitbucket\\database\\zuooh-standard-database\\lib");
      SQLite.loadLibrary();
   }   
   
   public void testHistoryUpdater() throws Exception {
      ChessHistory history = ChessHistoryDatabaseTest.createEmptyDatabase();
      int counter = 0;
      
      dumpHistory(history, "game.1");
      makeMove(history, "F2", "F4", "WHITE", "game.1", counter++);
      makeMove(history, "H7", "H6", "BLACK", "game.1", counter++);
      
      makeMove(history, "E2", "E3", "WHITE", "game.1", counter++);
      makeMove(history, "H6", "H5", "BLACK", "game.1", counter++);
      
      makeMove(history, "G1", "H3", "WHITE", "game.1", counter++);
      makeMove(history, "H5", "H4", "BLACK", "game.1", counter++);

      makeMove(history, "F1", "A6", "WHITE", "game.1", counter++);
      makeMove(history, "H8", "H5", "BLACK", "game.1", counter++);
      
      makeMove(history, "E1", "G1", "WHITE", "game.1", counter++);
      makeMove(history, "B7", "A6", "BLACK", "game.1", counter++);
      
      makeMove(history, "A2", "A4", "WHITE", "game.1", counter++);
      makeMove(history, "H5", "G5", "BLACK", "game.1", counter++);
      
      makeMove(history, "C2", "C4", "WHITE", "game.1", counter++);
      makeMove(history, "G5", "G2", "BLACK", "game.1", counter++);
      
      makeMove(history, "G1", "G2", "WHITE", "game.1", counter++);
      makeMove(history, "G8", "H6", "BLACK", "game.1", counter++); 
   
      ChessHistoryScroller scroller = new ChessHistoryScroller(history);
      System.err.println("BEFORE");
      dumpHistory(history, "game.1"); 
      scroller.scrollBack("game.1", ChessSide.WHITE);
      System.err.println("AFTER");
      dumpHistory(history, "game.1");
      
      makeMove(history, "G8", "H6", "BLACK", "game.1", counter++);
      
      System.err.println("BEFORE");
      dumpHistory(history, "game.1"); 
      scroller.scrollBack("game.1", ChessSide.BLACK);
      System.err.println("AFTER");
      dumpHistory(history, "game.1");
   }
   
   public void makeMove(ChessHistory history, String from, String to, String color, String gameId, int count) throws Exception {
      ChessMove move = new ChessMove(ChessBoardPosition.at(from), ChessBoardPosition.at(to), ChessSide.resolveSide(color), count);
      
      history.saveHistoryItem("game.1", move);
      dumpHistory(history, "game.1");   
   }
   
   public void dumpHistory(ChessHistory history, String gameId) throws Exception {
      ChessBoard currentBoard = history.loadHistoryAsBoard(gameId);
      ChessTextPieceSet textPieceSet = new ChessTextPieceSet(currentBoard);
      
      System.err.println(textPieceSet.drawBoard());
      
      if(history.containsHistory(gameId)) {
         System.err.println("NORTH "+history.loadLastHistoryItem(gameId, ChessSide.WHITE));
         System.err.println("SOUTH "+history.loadLastHistoryItem(gameId, ChessSide.BLACK));
         System.err.println(currentBoard.getKingPosition(history.loadLastHistoryItem(gameId).chessMove.side));
         System.err.println(currentBoard.getKingPosition(history.loadLastHistoryItem(gameId).chessMove.side.oppositeSide()));
      }
   }
}
